<?php

function check_login() { //function to check user is logged in
    $ci = & get_instance();
    if ($ci->session->userdata('userinfo') == false) {
        redirect('login');
    }
}

function server_date_time() { //get server date time
    date_default_timezone_set('Asia/Kolkata');
    $current_date = date('Y-m-d H:i:s');
    return $current_date;
}

function sendSms($mob, $msg) { //send sms
    //$user_name = "demociscohttp2";//demo
    $user_name = "cisohttp"; //production
    //$pwd = "demo1234";//demo
    $pwd = "ciso1234"; //production
    $msg_type = "text";
    //$from = "9910048418";//demo
    //$from = "DIGIPR";//demo
    $from = "CISODP"; //production
    $url = "http://www.myvaluefirst.com/smpp/sendsms?username=$user_name&password=$pwd&from=$from&to=$mob&text=" . urlencode($msg) . "";

    $send = file_get_contents($url);
    return $send;
}

function sendEmail($to, $sub, $msg) {
    $ci = & get_instance();
    $config = Array(
        'protocol' => 'smtp',
        'smtp_host' => 'ssl://smtp.googlemail.com',
        'smtp_port' => 465,
        'smtp_user' => 'sightsavers33@gmail.com',
        'smtp_pass' => 'Default@123',
        'mailtype' => 'html',
        'charset' => 'iso-8859-1'
    );
    $ci->load->library('email', $config);
    $ci->email->set_newline("\r\n");

    $ci->email->from('mygmail@gmail.com', 'SightSaver');
    $ci->email->to($to);

    $ci->email->subject($sub);
    $ci->email->message($msg);

    $result = $ci->email->send();
	//echo $ci->email>print_debugger();die;
}

function sendAttachEmail($to, $sub, $msg, $filepath) {
    $ci = & get_instance();
    $config = Array(
        'protocol' => 'smtp',
        'smtp_host' => 'ssl://smtp.googlemail.com',
        'smtp_port' => 465,
        'smtp_user' => 'digiproctor@gmail.com',
        'smtp_pass' => 'DIGIPROCTOR@123',
        'mailtype' => 'html',
        'charset' => 'iso-8859-1'
    );
    $ci->load->library('email', $config);
    $ci->email->set_newline("\r\n");

    $ci->email->from('mygmail@gmail.com', 'DigiProctor');
    $ci->email->to($to);

    $ci->email->subject($sub);
    $ci->email->message($msg);
    $ci->email->attach($filepath);

    $result = $ci->email->send();
}

function unique16digit() {
    $milliseconds = microtime_float();
    $values = explode(".", $milliseconds);
    $unique_val = $values[0] . $values[1];
    //echo $milliseconds."<br/>";
    //echo $unique_val."<br/>";
    $length = strlen($unique_val);
    if ($length == 12) {
        $rand_no = rand(1000, 9999);
    }
    if ($length == 13) {
        $rand_no = rand(100, 999);
    }
    if ($length == 14) {
        $rand_no = rand(10, 99);
    }
    return $unique_val . $rand_no;
}

function microtime_float() {
    list($usec, $sec) = explode(" ", microtime());
    return ((float) $usec + (float) $sec);
}

function upload_image($input_name, $image_name) {
    $ci = & get_instance();
    $ci->load->library('upload');
    $_FILES['userfile']['name'] = $input_name['name'];
    $_FILES['userfile']['type'] = $input_name['type'];
    $_FILES['userfile']['tmp_name'] = $input_name['tmp_name'];
    $_FILES['userfile']['error'] = $input_name['error'];
    $_FILES['userfile']['size'] = $input_name['size'];

    $config['file_name'] = $image_name;
    $config['upload_path'] = './uploads/question_image/';
    $config['allowed_types'] = 'gif|jpg|png|jpeg';
    $config['max_size'] = '1024000';
    $config['max_width'] = '5000';
    $config['max_height'] = '4000';
    $ci->upload->initialize($config);
    $ci->load->library('upload', $config);
    $ci->upload->do_upload();
    $data = $ci->upload->data();
    $name_array = $data['file_name'];
    return $name_array;
}
function arr_unique($arr) {
  sort($arr);
  $curr = $arr[0];
  $uni_arr[] = $arr[0];
  for($i=0; $i<count($arr);$i++){
      if($curr != $arr[$i]) {
        $uni_arr[] = $arr[$i];
        $curr = $arr[$i];
      }
  }
  return $uni_arr;
}
function getStateCode(){
	return ['IN-BR'=>'Bihar',
'IN-PY'=>'Puducherry',
'IN-DD'=>'Daman and Diu',
'IN-DN'=>'Dadra and Nagar Haveli',
'IN-DL'=>'Delhi',
'IN-NL'=>'Nagaland',
'IN-WB'=>'West Bengal',
'IN-HR'=>'Haryana',
'IN-HP'=>'Himachal Pradesh',
'IN-AS'=>'Assam',
'IN-UT'=>'Uttaranchal',
'IN-JH'=>'Jharkhand',
'IN-JK'=>'Jammu and Kashmir',
'IN-UP'=>'Uttar Pradesh',
'IN-SK'=>'Sikkim',
'IN-MZ'=>'Mizoram',
'IN-CT'=>'Chhattisgarh',
'IN-CH'=>'Chandigarh',
'IN-GA'=>'Goa',
'IN-GJ'=>'Gujarat',
'IN-RJ'=>'Rajasthan',
'IN-MP'=>'Madhya Pradesh',
'IN-OR'=>'Orissa',
'IN-TN'=>'Tamil Nadu',
'IN-AN'=>'Andaman and Nicobar',
'IN-AP'=>'Andhra Pradesh',
'IN-TR'=>'Tripura',
'IN-AR'=>'Arunachal Pradesh',
'IN-KA'=>'Karnataka',
'IN-PB'=>'Punjab',
'IN-ML'=>'Meghalaya',
'IN-MN'=>'Manipur',
'IN-MH'=>'Maharashtra',
'IN-KL'=>'Kerala'];
}

function getQuaterMonth($quater){
	if($quater==1){
		return ['1','2','3'];
	}
	else if($quater==2){
		return ['4','5','6'];
	}
	else if($quater==3){
		return ['7','8','9'];
	}
	else if($quater==4){
		return ['10','11','12'];
	}
}