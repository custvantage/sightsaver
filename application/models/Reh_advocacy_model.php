<?php

Class Reh_advocacy_model extends CI_Model {
	protected $advocacy = "ss_reh_advocacy";
    
	public function createAdvocacy($data) {
		$this->db->trans_start();
        $this->db->insert($this->advocacy,$data);
		$this->db->trans_complete();
		return $this->db->insert_id();
    }
	
	public function updateAdvocacy($data,$id) 
	{
		$this->db->where('ss_reh_advocacy_id', $id);
		$this->db->update($this->advocacy, $data);
    }
	
	
	
	
	
	public function getAdvocacy($month_from,$partner_id)
	{		
		$query = $this->db->query("Select ss_reh_advocacy_id,ss_reh_advocacy_block,ss_reh_advocacy_event_type,ss_reh_advocacy_event_detail,ss_reh_advocacy_meeting_purpose,ss_reh_advocacy_priorities,ss_reh_advocacy_inclusion_eye_health,ss_reh_advocacy_meeting_level,ss_reh_advocacy_meeting_organised,ss_reh_advocacy_meeting_organised_detail,ss_reh_advocacy_meeting_with,ss_reh_advocacy_issue From $this->advocacy Where ss_reh_advocacy_month = '".$month_from."' AND ss_partner_id = '".$partner_id."'");
		return $query->result();
	}
	
	  public function deleteHehAdvocacy($id)
	     {		
		$query = $this->db->query("delete  From $this->advocacy Where ss_reh_advocacy_id = '".$id."'");
		return true;
        }
		
	  public function editFetchData($id)
		{		
			$query = $this->db->query("Select *, DATE_FORMAT(`ss_reh_advocacy_month`,'%m-%Y') as date_month From $this->advocacy Where ss_reh_advocacy_id = '".$id."'");
			return $query->result();
		}

}
