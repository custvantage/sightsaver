<?php

Class Reh_vhsnc_model extends CI_Model {
	protected $vhsnc = "ss_reh_vhsnc";
    
	public function createVhsnc($data) {
		$this->db->trans_start();
        $this->db->insert($this->vhsnc,$data);
		$this->db->trans_complete();
		return $this->db->insert_id();
    }
	
	public function updateVhsnc($data,$id) 
	{
		$this->db->where('ss_reh_vhsnc_id', $id);
		$this->db->update($this->vhsnc, $data);
    }

	
	public function getVhsnc($month_from,$partner_id)
	{		
		$query = $this->db->query("Select  ss_reh_vhsnc_id,ss_reh_vhsnc_name,DATE_FORMAT(ss_reh_vhsnc_meeting_date,'%d-%m-%Y')as meeting_date,ss_reh_vhsnc_meeting_attend_by,ss_reh_vhsnc_issue_discussed From $this->vhsnc Where ss_reh_vhsnc_month = '".$month_from."' AND ss_partner_id = '".$partner_id."'");
		return $query->result();
	}
	
    public function deleteHehVhsnc($id)
	     {		
		$query = $this->db->query("delete  From $this->vhsnc Where ss_reh_vhsnc_id = '".$id."'");
		return true;
        }
		
	public function editFetchData($id)
			{		
				$query = $this->db->query("Select *, DATE_FORMAT(`ss_reh_vhsnc_month`,'%m-%Y') as date_month From $this->vhsnc Where ss_reh_vhsnc_id = '".$id."'");
				return $query->result();
			}

}
