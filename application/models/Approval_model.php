<?php

Class Approval_model extends CI_Model {
	protected $mpr_summ = "ss_mpr_summary";
		
	public function getAllPartner()
	{
		$query = $this->db->query("Select ss_partners_id,ss_partners_name From ss_partners");
		return $query->result();
	}
	public function getAllState()
	{
		$query = $this->db->query("Select ss_states_name From ss_states");
		return $query->result();
	}
	public function getAllDistrict()
	{
		$query = $this->db->query("Select ss_district_name From ss_district");
		return $query->result();
	}
	public function getFilterSummary($month_from)
	{
		$query = ("Select DATE_FORMAT(ms.ss_mpr_modified_on,'%d-%m-%Y %H:%i') as last_modified,ms.ss_mpr_modified_by,ms.status,ms.ss_mpr_project_name,ms.ss_mpr_report_month,ms.ss_mpr_summary_partner_id,partnertbl.ss_partners_name,districttbl.ss_district_name,statetbl.ss_states_name From $this->mpr_summ as ms INNER JOIN ss_partners as partnertbl ON partnertbl.ss_partners_id=ms.ss_mpr_summary_partner_id INNER JOIN ss_district as districttbl ON districttbl.ss_district_id=partnertbl.ss_partners_districts INNER JOIN ss_states as statetbl ON statetbl.ss_states_id=districttbl.ss_states_ss_states_id");
		if(!empty($month_from))
		{
			$query .= " Where ms.ss_mpr_report_month='".$month_from."'";
		}		
		$query .= " ORDER BY ms.ss_mpr_summary_id DESC";		
		$result = $this->db->query($query);	
		return $result->result();
	}
	public function getPartnerFilterSummary($month_from, $partner_id = 0)
	{
		$query = ("Select DATE_FORMAT(ms.ss_mpr_modified_on,'%d-%m-%Y %H:%i') as last_modified,ms.status,ms.ss_mpr_project_name,ms.ss_mpr_report_month,ms.ss_mpr_summary_partner_id,partnertbl.ss_partners_name,districttbl.ss_district_name,statetbl.ss_states_name From $this->mpr_summ as ms INNER JOIN ss_partners as partnertbl ON partnertbl.ss_partners_id=ms.ss_mpr_summary_partner_id INNER JOIN ss_district as districttbl ON districttbl.ss_district_id=partnertbl.ss_partners_districts INNER JOIN ss_states as statetbl ON statetbl.ss_states_id=districttbl.ss_states_ss_states_id ");
		if(!empty($month_from))
		{
			if($partner_id !=0) {
			$query .= " Where ms.ss_mpr_report_month='".$month_from."' and ms.ss_mpr_summary_partner_id= '".$partner_id."'";
		} else
		{
			$query .= " Where ms.ss_mpr_report_month='".$month_from."'";
			}
		}		
		$query .= " ORDER BY ms.ss_mpr_summary_id DESC";	
		
		$result = $this->db->query($query);	
		//echo $this->db->last_query(); exit;
		return $result->result();
	}
	
	public function getFilterSummaryadmin($month_from)
	{
		$query = ("Select DATE_FORMAT(ms.ss_mpr_modified_on,'%d-%m-%Y %H:%i') as last_modified,ms.ss_mpr_modified_by,ms.status,ms.ss_mpr_project_name,ms.ss_mpr_report_month,ms.ss_mpr_summary_partner_id,partnertbl.ss_partners_name,districttbl.ss_district_name,statetbl.ss_states_name From $this->mpr_summ as ms INNER JOIN ss_partners as partnertbl ON partnertbl.ss_partners_id=ms.ss_mpr_summary_partner_id INNER JOIN ss_district as districttbl ON districttbl.ss_district_id=partnertbl.ss_partners_districts INNER JOIN ss_states as statetbl ON statetbl.ss_states_id=districttbl.ss_states_ss_states_id ");
		if(!empty($month_from))
		{
			$query .= " Where ms.ss_mpr_report_month='".$month_from."' && ms.status='3' ||  ms.status='5'";
		}		
		$query .= " ORDER BY ms.ss_mpr_summary_id DESC";		
		$result = $this->db->query($query);	
		return $result->result();
	}
	
	
	
}
