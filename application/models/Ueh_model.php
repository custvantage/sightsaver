<?php

Class Ueh_model extends CI_Model {
	protected $iec = "ss_ueh_iec";
	protected $vc = "ss_ueh_vc";
	protected $bh = "ss_ueh_base_hospital";
	protected $bcc = "ss_ueh_bcc";
	protected $advocacy = "ss_ueh_advocacy";
	protected $oc = "ss_ueh_outreachcamp";
	protected $training = "ss_ueh_training";
    
	public function createIec($data) {
		$this->db->trans_start();
        $this->db->insert($this->iec,$data);
		$this->db->trans_complete();
		return $this->db->insert_id();
    }
	
	public function updateIec($data,$id) 
	{
		//echo "<pre>"; print_r($data); die;
		$this->db->where('ss_ueh_iec_id', $id);
		$this->db->update($this->iec, $data);
    }
	   
	public function getIec($month_from,$partner_id)
	{		
		$query = $this->db->query("Select ss_ueh_iec_id,ss_ueh_iec_city,ss_ueh_iec_posters,ss_ueh_iec_booklet,ss_ueh_iec_pamplets,ss_ueh_iec_wall,ss_ueh_iec_others From $this->iec Where ss_ueh_iec_month = '".$month_from."' AND ss_partner_id = '".$partner_id."'");
		return $query->result();
	}
	public function deleteUehIec($id)
	     {		
		$query = $this->db->query("delete  From $this->iec  Where ss_ueh_iec_id = '".$id."'");
		return true;
        }
		
	public function editFetchDataIec($id)
			{		
				$query = $this->db->query("Select *, DATE_FORMAT(`ss_ueh_iec_month`,'%m-%Y') as date_month From $this->iec Where ss_ueh_iec_id = '".$id."'");
				return $query->result();
			}	
	
	
	
	
/////////////////////////////////////////////////////////////////	
	public function createBcc($data) 
	{
		$this->db->trans_start();
        $this->db->insert($this->bcc,$data);
		$this->db->trans_complete();
		return $this->db->insert_id();
    }
	 public function updatebcc($data,$id) 
	{
		$this->db->where('ss_ueh_bcc_id', $id);
		$this->db->update($this->bcc, $data);
    }
	
	
	public function getBcc($month_from,$partner_id)
	{		
		$query = $this->db->query("Select ss_ueh_bcc_id,ss_ueh_bcc_city_name,ss_ueh_bcc_activity_type,ss_activity_detail,ss_ueh_bcc_sansitize,ss_ueh_bcc_sansitize_detail,ss_ueh_bcc_participant_no,ss_ueh_bcc_topic From $this->bcc Where ss_ueh_bcc_month = '".$month_from."' AND ss_partner_id = '".$partner_id."'");
		return $query->result();
	}
	public function deleteUehBcc($id)
	     {		
		$query = $this->db->query("delete  From $this->bcc  Where ss_ueh_bcc_id = '".$id."'");
		return true;
        }
		
	public function editFetchDataBcc($id)
			{		
				$query = $this->db->query("Select *, DATE_FORMAT(`ss_ueh_bcc_month`,'%m-%Y') as date_month From $this->bcc Where ss_ueh_bcc_id = '".$id."'");
				return $query->result();
			}	
	
	
	///////////////// end of the bcc /////////
	public function createAdvocacy($data) {
		$this->db->trans_start();
        $this->db->insert($this->advocacy,$data);
		$this->db->trans_complete();
		return $this->db->insert_id();
    }
	
	 public function updateAdvocacy($data,$id) 
	{
		$this->db->where('ss_ueh_advocacy_id', $id);
		$this->db->update($this->advocacy, $data);
    }	
	
	
	public function getAdvocacy($month_from,$partner_id)
	{		
		$query = $this->db->query("Select ss_ueh_advocacy_id,ss_reh_city_name,ss_ueh_event_type,ss_ueh_event_detail,ss_ueh_meeting_purpose,ss_meeting_purpose_detail,ss_meeting_level,ss_meeting_held_with,ss_meeting_held_with_detail,ss_topic_event From $this->advocacy Where ss_ueh_month = '".$month_from."' AND ss_partner_id = '".$partner_id."'");
		return $query->result();
	}
   public function deleteUehAdvocay($id)
	     {		
		$query = $this->db->query("delete  From $this->advocacy  Where ss_ueh_advocacy_id = '".$id."'");
		return true;
        }
		
	public function editFetchDataAdvocacy($id)
			{		
				$query = $this->db->query("Select *, DATE_FORMAT(`ss_ueh_month`,'%m-%Y') as date_month From $this->advocacy Where ss_ueh_advocacy_id = '".$id."'");
				return $query->result();
			}	

////////////////start of the training section model //////	
	
   public function createTraining($data) {
		$this->db->trans_start();
        $this->db->insert($this->training,$data);
		$this->db->trans_complete();
		return $this->db->insert_id();
    }
	
   public function updateTraining($data,$id) 
	{
		$this->db->where('ss_ueh_training_id', $id);
		$this->db->update($this->training, $data);
    }	

   public function getTraining($month_from,$partner_id)
	{
		$query = $this->db->query("Select ss_ueh_training_id,ss_ueh_city_name,ss_ueh_person,ss_ueh_gender,ss_person_category,ss_person_category_detail,ss_location_activity,ss_location_activity_detail,ss_activity,ss_activity_details,ss_participants_from,ss_partcipants_from_detail,ss_topic From $this->training Where ss_ueh_month = '".$month_from."' AND ss_partner_id = '".$partner_id."'");
		return $query->result();
	}
	
	 public function deleteUehTraining($id)
	    {		
		$query = $this->db->query("delete  From $this->training Where ss_ueh_training_id = '".$id."'");
		return true;
        }
		
	public function editFetchDataTraining($id)
			{		
				$query = $this->db->query("Select *, DATE_FORMAT(`ss_ueh_month`,'%m-%Y') as date_month From $this->training Where ss_ueh_training_id = '".$id."'");
				return $query->result();
			}
	
	
	
//////////////////// vc model  ///////////////////////	
	public function createVc($data)
	{
		$this->db->trans_start();		
        $this->db->insert($this->vc,$data);		
		$this->db->trans_complete();		
		return $this->db->insert_id();
	}
	
	public function updateVc($data,$id) 
	{
		$this->db->where('ss_ueh_vc', $id);
		$this->db->update($this->vc, $data);
    }
	
	public function getVcData($month_from,$partner_id)
	{
		$query = $this->db->query("Select ss_ueh_vc,ss_vc_name,ss_vc_open_days,ss_vc_support,ss_screen_male,ss_screen_female,ss_screen_trans,ss_screen_boy,ss_screen_girl,ss_refra_person_male,ss_refra_person_female,ss_refra_person_trans,ss_refra_person_boy,ss_refra_person_girl,ss_refra_spectacles_male,ss_refra_spectacles_female,ss_refra_spectacles_trans,ss_refra_spectacles_boy,ss_refra_spectacles_girl,ss_refra_dispensed_spectacles_male,ss_refra_dispensed_spectacles_female,ss_refra_dispensed_spectacles_trans,ss_refra_dispensed_spectacles_boy,ss_refra_dispensed_spectacles_girl,ss_refra_purchage_male,ss_refra_purchage_female,ss_refra_purchage_trans,ss_refra_purchage_boy,ss_refra_purchage_girls,ss_refer_persons_male,ss_refer_persons_female,ss_refer_persons_trans,ss_refer_persons_boy,ss_refer_persons_girl,ss_refer_cataract_male,ss_refer_cataract_female,ss_refer_cataract_trans,ss_refer_cataract_boy,ss_refer_cataract_girl,ss_treat_male,ss_treat_female,ss_treat_trans,ss_treat_boy,ss_treat_girl,ss_dr_diabetes_male,ss_dr_diabetes_female,ss_dr_diabetes_trans,ss_dr_diabetes_boy,ss_dr_diabetes_girl,ss_dr_dr_male,ss_dr_dr_female,ss_dr_dr_trans,ss_dr_dr_boy,ss_dr_dr_girl,ss_methods_male,ss_methods_female,ss_dr_methods_trans,ss_methods_boy,ss_methods_girl From $this->vc Where ss_ueh_vc_month = '".$month_from."' AND ss_partner_id = '".$partner_id."'");
		return $query->result();
	}
	
	 public function deleteUehVc($id)
	     {		
		$query = $this->db->query("delete  From $this->vc Where ss_ueh_vc = '".$id."'");
		return true;
        }
		
	public function editFetchData($id)
			{		
				$query = $this->db->query("Select *, DATE_FORMAT(`ss_ueh_vc_month`,'%m-%Y') as date_month From $this->vc Where ss_ueh_vc = '".$id."'");
				return $query->result();
			}
	
//////////////////// end of the vc model //////////////////////	
	public function createBhHos($data)
	{
		$this->db->trans_start();
        $this->db->insert($this->bh,$data);		
		$this->db->trans_complete();		
		return $this->db->insert_id();
	}
	public function updateBhHos($data,$id) 
	{
		$this->db->where('ss_ueh_base_id', $id);
		$this->db->update($this->bh, $data);
    }
	
	public function getBhData($month_from,$partner_id)
	{
		$query = $this->db->query("Select ss_ueh_base_id,ss_agency_name,ss_exam_opd_male,ss_exam_opd_female,ss_exam_opd_boy,ss_exam_opd_girl,ss_refer_vc_male,ss_refer_vc_female,ss_refer_vc_boy,ss_refer_vc_girl,ss_refer_camp_male,ss_refer_camp_female,ss_refer_camp_boy,ss_refer_camp_girl,ss_refer_walk_male,ss_refer_walk_female,ss_refer_walk_boy,ss_refer_walk_girl,ss_refra_refra_male,ss_refra_refra_female,ss_refra_refra_boy,ss_refra_refra_girl,ss_refra_pres_male,ss_refra_pres_female,ss_refra_pres_boy,ss_refra_pres_girl,ss_refra_disp_male,ss_refra_disp_female,ss_refra_disp_boy,ss_refra_disp_girl,ss_refra_purc_male,ss_refra_purc_female,ss_refra_purc_boy,ss_refra_purc_girl,ss_ptreat_sur_male,ss_ptreat_sur_female,ss_ptreat_sur_boy,ss_ptreat_sur_girl,ss_treat_subdfree_male,ss_treat_subdfree_female,ss_treat_subdfree_boy,ss_treat_subdfree_girl,ss_treat_subd_male,ss_treat_subd_female,ss_treat_subd_boy,ss_treat_subd_girl,ss_treat_catara_male,ss_treat_catara_female,ss_treat_catara_boy,ss_treat_catara_girl,ss_treat_glau_male,ss_treat_glau_female,ss_treat_glau_boy,ss_treat_glau_girl,ss_treat_sur_male,ss_treat_sur_female,ss_treat_sur_boy,ss_treat_sur_girl From $this->bh Where ss_ueh_base_month = '".$month_from."' AND ss_partner_id = '".$partner_id."'");
		return $query->result();
	}
	
	 public function deleteUehBase($id)
	    {		
		$query = $this->db->query("delete From $this->bh Where ss_ueh_base_id = '".$id."'");
		return true;
        }
		
	 public function editFetchDataBase($id)
		{		
			$query = $this->db->query("Select *, DATE_FORMAT(`ss_ueh_base_month`,'%m-%Y') as date_month From $this->bh Where ss_ueh_base_id = '".$id."'");
			return $query->result();
		}
	
	
	
////////////  end of the base hospital  ///////////////////	
	
	public function createOc($data)
	{
		$this->db->trans_start();
        $this->db->insert($this->oc,$data);		
		$this->db->trans_complete();		
		return $this->db->insert_id();
	}
	
	
	public function updateOc($data,$id) 
	{
		$this->db->where('ss_ueh_outreachcamp_id', $id);
		$this->db->update($this->oc, $data);
    }
	
	public function getOcData($month_from,$partner_id)
	{
		$query = $this->db->query("Select ss_ueh_outreachcamp_id,ss_name_cs,ss_camp_support,ss_fund_agency,ss_loc_type,ss_screen_male,ss_screen_female,ss_screen_boy,ss_screen_girl,ss_refra_refra_male,ss_refra_refra_female,ss_refra_refra_boy,ss_refra_refra_girl,ss_refra_pres_male,ss_refra_pres_female,ss_refra_pres_boy,ss_refra_pres_girl,ss_refra_disp_male,ss_refra_disp_female,ss_refra_disp_boy,ss_refra_disp_girl,ss_refra_purc_male,ss_refra_purc_female,ss_refra_purc_child,ss_refra_purc_girl,ss_refer_refer_male,ss_refer_refer_female,ss_refer_refer_boy,ss_refer_refer_girl,ss_refer_catar_male,ss_refer_catar_female,ss_refer_catar_boy,ss_refer_catar_girl,ss_treat_male,ss_treat_female,ss_treat_boy,ss_treat_girl,ss_dr_diabe_male,ss_dr_diabe_female,ss_dr_diabe_boy,ss_dr_diabe_girl,ss_dr_screen_male,ss_dr_screen_female,ss_dr_screen_boy,ss_dr_screen_girl,ss_dr_method_male,ss_dr_method_female,ss_dr_method_boy,ss_dr_method_girl From $this->oc Where ss_ueh_month = '".$month_from."' AND ss_partner_id = '".$partner_id."'");
		return $query->result();
	}
	public function createBaseHospitalExcel($data)
	{
		$this->db->trans_start();
        $this->db->insert_batch($this->bh,$data);
		$this->db->trans_complete();
		return $this->db->insert_id();
	}
	
	public function deleteUehOc($id)
	    {		
		$query = $this->db->query("delete From $this->oc Where ss_ueh_outreachcamp_id = '".$id."'");
		return true;
        }
		
	 public function editFetchDataOc($id)
		{		
			$query = $this->db->query("Select *, DATE_FORMAT(`ss_ueh_month`,'%m-%Y') as date_month From $this->oc Where ss_ueh_outreachcamp_id = '".$id."'");
			return $query->result();
		}
	
	
	
	
	
}
