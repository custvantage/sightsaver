<?php

Class Dashboard_model extends CI_Model {
	protected $mpr_summ = "ss_mpr_summary";
	
	public function getSummary() {
		$query = $this->db->query("Select DATE_FORMAT(ms.ss_mpr_modified_on,'%d-%m-%Y %H:%i') as last_modified,ms.status,DATE_FORMAT(ms.ss_mpr_report_month,'%d-%m-%Y') as report_month,partnertbl.ss_partners_name,districttbl.ss_district_name,statetbl.ss_states_name From $this->mpr_summ as ms INNER JOIN ss_partners as partnertbl ON partnertbl.ss_partners_id=ms.ss_mpr_summary_partner_id INNER JOIN ss_district as districttbl ON districttbl.ss_district_id=partnertbl.ss_partners_districts INNER JOIN ss_states as statetbl ON statetbl.ss_states_id=districttbl.ss_states_ss_states_id ORDER BY ms.ss_mpr_summary_id DESC");
		return $query->result();
    }
	public function getAllPartner()
	{
		$query = $this->db->query("Select ss_partners_id,ss_partners_name From ss_partners");
		return $query->result();
	}
	public function getAllState()
	{
		$query = $this->db->query("Select ss_states_name From ss_states");
		return $query->result();
	}
	public function getAllDistrict()
	{
		$query = $this->db->query("Select ss_district_name From ss_district");
		return $query->result();
	}
	public function getFilterSummary($month_from)
	{
		$query = ("Select DATE_FORMAT(ms.ss_mpr_modified_on,'%d-%m-%Y %H:%i') as last_modified,ms.status,partnertbl.ss_partners_name,districttbl.ss_district_name,statetbl.ss_states_name From $this->mpr_summ as ms INNER JOIN ss_partners as partnertbl ON partnertbl.ss_partners_id=ms.ss_mpr_summary_partner_id INNER JOIN ss_district as districttbl ON districttbl.ss_district_id=partnertbl.ss_partners_districts INNER JOIN ss_states as statetbl ON statetbl.ss_states_id=districttbl.ss_states_ss_states_id");
		if(!empty($month_from))
		{
			$query .= " Where ms.ss_mpr_report_month='".$month_from."'";
		}		
		$query .= " ORDER BY ms.ss_mpr_summary_id DESC";		
		$result = $this->db->query($query);
		return $result->result();
	}
	public function getYearMprDetail() {
		$query = $this->db->query("Select DISTINCT(DATE_FORMAT(ms.ss_mpr_report_month,'%Y')) as report_year From $this->mpr_summ as ms order by report_year asc");
		return $query->result();
    }
	public function getMonthMprDetail() {
		$query = $this->db->query("Select DISTINCT(DATE_FORMAT(ms.ss_mpr_report_month,'%M')) as report_month From $this->mpr_summ as ms order by report_month desc");
		return $query->result();
    }
	public function getMetricId($project_name,$module_name,$section_name)
	{
		$query = $this->db->query("Select ss_section_layout_id From ss_section_layout Where ss_section_layout_project_name='".$project_name."' AND ss_section_layout_module_name='".$module_name."' AND ss_section_layout_section_name='".$section_name."'");
		$layout_id = $query->row('ss_section_layout_id');
		//echo $this->db->last_query(); die;
		$query_graph = $this->db->query("Select metric_id From ss_graph Where module_id='".$layout_id."'");
				
		return $query_graph->result();
	}
  public function graph_first($year,$metrec_id)
	{
		$query = $this->db->query("
			SELECT sum(four.ss_four_column_data_value_men) as 'four_column_men',
			sum(four.ss_four_column_data_value_women) as 'four_column_data_women',
			sum(four.ss_four_column_data_value_boys) as 'four_column_data_boys',
			sum(four.ss_four_column_data_value_girls) as 'four_column_data_girls' 
			FROM ss_four_column_data as four 
			LEFT JOIN ss_mpr_summary AS su 
			ON four.ss_partner_id = su.ss_mpr_summary_partner_id
			WHERE four.ss_four_column_data_month<='".$year."' AND four.ss_metric_master_id IN ($metrec_id) AND su.status='3'
		");
	    return $query->row();
	}
	
	 public function graph_first1($year,$metrec_ids)
	{
		/*$query = $this->db->query("
			SELECT sum(four.ss_four_column_data_value_men) as 'four_column_men',
			sum(four.ss_four_column_data_value_women) as 'four_column_data_women',
			sum(four.ss_four_column_data_value_boys) as 'four_column_data_boys',
			sum(four.ss_four_column_data_value_girls) as 'four_column_data_girls' 
			FROM ss_four_column_data as four 
			LEFT JOIN ss_mpr_summary AS su 
			ON four.ss_partner_id = su.ss_mpr_summary_partner_id
			WHERE four.ss_four_column_data_month<='".$year."' AND four.ss_metric_master_id IN ($metrec_id) AND su.status='3'
		");*/
		$my_metrec_ids = array();
		foreach($metrec_ids as $mid){
			$my_metrec_ids[] = $mid->metric_id ;
		}
		$metrec_ids = implode(',',@$my_metrec_ids);
		
		$query = $this->db->query("SELECT DISTINCT(four.ss_four_column_data_id),four.ss_four_column_data_value_men,four.ss_four_column_data_value_women,four.ss_four_column_data_value_boys,four.ss_four_column_data_value_girls
			FROM ss_four_column_data as four 
			JOIN ss_mpr_summary AS su 
			ON four.ss_partner_id = su.ss_mpr_summary_partner_id
			WHERE four.ss_four_column_data_month<='".$year."' AND year(four.ss_four_column_data_month) = year('".$year."')  AND four.ss_metric_master_id IN ($metrec_ids) AND su.status='3'");
		//echo $this->db->last_query(); die;	
	    return $query->result();
	}
	
	public function graph_first_yearly($year,$metrec_ids)
	{
		$my_metrec_ids = array();
		foreach($metrec_ids as $mid){
			$my_metrec_ids[] = $mid->metric_id ;
		}
		$metrec_ids = implode(',',@$my_metrec_ids);
		$query = $this->db->query(
		    "SELECT 
			sum(four.ss_four_column_data_value_men) as 'four_column_men1',
			sum(four.ss_four_column_data_value_women) as 'four_column_data_women1',
			sum(four.ss_four_column_data_value_boys) as 'four_column_data_boys1',
			sum(four.ss_four_column_data_value_girls) as 'four_column_data_girls1' 
			FROM ss_four_column_data as four  
			WHERE year(four.ss_four_column_data_month) = year('".$year."') AND four.ss_four_column_data_status='1' AND four.ss_metric_master_id IN($metrec_ids)"	
		);	
        //echo $this->db->last_query(); die;		
	    return $query->row();
	}
	public function getStateWiseOpdMain($year,$metrec_id)
	{
		$query = $this->db->query(
			"SELECT state.ss_states_name, 
			sum(four.ss_four_column_data_value_men) as four_column_men, 
			sum(four.ss_four_column_data_value_women) as four_column_women,
			sum(four.ss_four_column_data_value_boys)as four_column_boys, 
			sum(four.ss_four_column_data_value_girls)as four_column_girl 
			FROM ss_four_column_data AS four 
			LEFT JOIN ss_partners AS part ON four.ss_partner_id = part.ss_partners_id 
			LEFT JOIN ss_district AS dist ON part.ss_partners_districts = dist.ss_district_id 
			LEFT JOIN ss_states AS state ON dist.ss_states_ss_states_id = state.ss_states_id 
			LEFT JOIN ss_mpr_summary AS su ON four.ss_partner_id = su.ss_mpr_summary_partner_id 
			WHERE four.ss_four_column_data_month<='".$year."' AND four.ss_metric_master_id ='".$metrec_id."' 
			AND su.status='3' 
			GROUP BY state.ss_states_name"
		); 
		return $query->result();
	}
	
	public function getStateWiseOpdMain1($year,$metrec_ids)
	{
		$my_metrec_ids = array();
		foreach($metrec_ids as $mid){
			$my_metrec_ids[] = $mid->metric_id ;
		}
		$metrec_ids = implode(',',@$my_metrec_ids);
		$query = $this->db->query("SELECT 
			state.ss_states_name, 
			part.ss_partners_id,
			dist.ss_district_id,
			sum(four.ss_four_column_data_value_men) as four_column_men, 
			sum(four.ss_four_column_data_value_women) as four_column_women,
			sum(four.ss_four_column_data_value_boys)as four_column_boys, 
			sum(four.ss_four_column_data_value_girls)as four_column_girl
			FROM ss_four_column_data AS four 
			LEFT JOIN ss_partners AS part ON four.ss_partner_id = part.ss_partners_id 
			LEFT JOIN ss_district AS dist ON part.ss_partners_districts = dist.ss_district_id 
			LEFT JOIN ss_states AS state ON dist.ss_states_ss_states_id = state.ss_states_id 
			LEFT JOIN ss_mpr_summary AS su ON four.ss_partner_id = su.ss_mpr_summary_partner_id 
			WHERE four.ss_four_column_data_month<='".$year."' AND four.ss_metric_master_id IN ($metrec_ids)
			AND su.status='3'
			AND su.ss_mpr_report_month = four.ss_four_column_data_month 
			AND year(four.ss_four_column_data_month) = year('".$year."') GROUP BY state.ss_states_name");
			//echo $this->db->last_query(); die;
			return $query->result();
	}
	
	public function getStateWiseOpdYearly($year,$metrec_id)
	{
		$query = $this->db->query("SELECT state.ss_states_name,sum(four.ss_four_column_data_value_men) as four_column_men, sum(four.ss_four_column_data_value_women) as four_column_women,sum(four.ss_four_column_data_value_boys)as four_column_boys, sum(four.ss_four_column_data_value_girls)as four_column_girl FROM ss_four_column_data AS four LEFT JOIN ss_partners AS part ON four.ss_partner_id = part.ss_partners_id LEFT JOIN ss_district AS dist ON part.ss_partners_districts = dist.ss_district_id LEFT JOIN ss_states AS state ON dist.ss_states_ss_states_id = state.ss_states_id LEFT JOIN ss_mpr_summary AS su ON four.ss_partner_id = su.ss_mpr_summary_partner_id WHERE four.ss_four_column_data_month='".$year."' AND four.ss_metric_master_id ='".$metrec_id."' AND su.status='3' GROUP BY state.ss_states_name");
		return $query->result();
	}
	
	
	public function getStateWiseOpdYearly1($year,$metrec_ids)
	{
		$my_metrec_ids = array();
		foreach($metrec_ids as $mid){
			$my_metrec_ids[] = $mid->metric_id ;
		}
		$metrec_ids = implode(',',@$my_metrec_ids);
		$query = $this->db->query("SELECT 
			state.ss_states_name,
			sum(four.ss_four_column_data_value_men) as four_column_men, 
			sum(four.ss_four_column_data_value_women) as four_column_women,
			sum(four.ss_four_column_data_value_boys)as four_column_boys, 
			sum(four.ss_four_column_data_value_girls)as four_column_girl
			FROM ss_four_column_data AS four 
			LEFT JOIN ss_partners AS part ON four.ss_partner_id = part.ss_partners_id 
			LEFT JOIN ss_district AS dist ON part.ss_partners_districts = dist.ss_district_id 
			LEFT JOIN ss_states AS state ON dist.ss_states_ss_states_id = state.ss_states_id 
			WHERE year(four.ss_four_column_data_month) = year('".$year."') AND four.ss_four_column_data_status='1' AND four.ss_metric_master_id IN ($metrec_ids) GROUP BY state.ss_states_name");
			//echo $this->db->last_query(); die;
			return $query->result();
	}
	
	public function getMonthlyMain($year,$metrec_ids)
	{
		$my_metrec_ids = array();
		foreach($metrec_ids as $mid){
			$my_metrec_ids[] = $mid->metric_id ;
		}
		$metrec_ids = implode(',',@$my_metrec_ids);
		$query = $this->db->query("SELECT state.ss_states_name, DATE_FORMAT(four.ss_four_column_data_month,'%M') AS 
		month,SUM( four.ss_four_column_data_value_men) AS four_column_data_value_men,
		SUM(four.ss_four_column_data_value_women) AS four_column_data_value_women,
		SUM(four.ss_four_column_data_value_boys) AS four_column_data_value_boys,
		SUM(four.ss_four_column_data_value_girls) AS four_column_data_value_girls
		FROM ss_four_column_data AS four 
		LEFT JOIN ss_partners AS part ON four.ss_partner_id = part.ss_partners_id 
		LEFT JOIN ss_district AS dist ON part.ss_partners_districts = dist.ss_district_id 
		LEFT JOIN ss_states AS state ON dist.ss_states_ss_states_id = state.ss_states_id 
		WHERE four.ss_four_column_data_month<='".$year."' AND year(four.ss_four_column_data_month) = year('".$year."') AND ss_metric_master_id = '".$metrec_ids."'
		GROUP BY state.ss_states_name, four.ss_four_column_data_month ORDER BY four.ss_four_column_data_month ASC");		
		return $query->result();
	}
	

	
	
	/*Speedometer Dashboard graph functions  */
	public function getMetricId_speedometer($project_name,$module_name,$section_name)
	{

		$query = $this->db->query("Select ss_section_layout_id From ss_section_layout Where ss_section_layout_project_name='".$project_name."' AND ss_section_layout_module_name='".$module_name."' AND ss_section_layout_section_name='".$section_name."'");
		$layout_id = $query->row('ss_section_layout_id');
		
		$query_graph = $this->db->query("Select metric_id From ss_graph Where module_id='".$layout_id."'");

		return $query_graph->result();
	}
	
	public function graph_realdata($year,$metrec_ids)
	{
		
		$my_metrec_ids = array();
		foreach($metrec_ids as $mid){
			$my_metrec_ids[] = $mid->metric_id ;
		}
		$metrec_ids = implode(',',@$my_metrec_ids);

		if($year==0){
			$query = $this->db->query("
				SELECT sum(four.ss_four_column_data_value_men) as 'four_column_data_men',
				sum(four.ss_four_column_data_value_women) as 'four_column_data_women',
				sum(four.ss_four_column_data_value_boys) as 'four_column_data_boys',
				sum(four.ss_four_column_data_value_girls) as 'four_column_data_girls' 
				FROM ss_four_column_data as four 
				LEFT JOIN ss_mpr_summary AS su 
				ON four.ss_partner_id = su.ss_mpr_summary_partner_id
				WHERE four.ss_metric_master_id IN ($metrec_ids) AND su.status='3'
			");
			
		}else{
			
			$query = $this->db->query("
				SELECT DISTINCT(four.ss_four_column_data_id),
				four.ss_four_column_data_value_men,
				four.ss_four_column_data_value_women,
				four.ss_four_column_data_value_boys,
				four.ss_four_column_data_value_girls
				FROM ss_four_column_data as four 
				JOIN ss_mpr_summary AS su 
				ON four.ss_partner_id = su.ss_mpr_summary_partner_id
				WHERE four.ss_four_column_data_month<='".$year."' AND year(four.ss_four_column_data_month) = year('".$year."')  AND four.ss_metric_master_id IN ($metrec_ids) AND su.status='3' ");
			
		}
		//echo $this->db->last_query(); die;
	    return $query->result();
	}
	
	public function graph_targetdata($year,$metrec_ids)
	{
		$my_metrec_ids = array();
		foreach($metrec_ids as $mid){
			$my_metrec_ids[] = $mid->metric_id ;
		}
		$metrec_ids = implode(',',@$my_metrec_ids);
		
		if($year==0){
			
			$query = $this->db->query(
				"SELECT 
				sum(four.ss_four_column_data_value_men) as 'four_column_data_men1',
				sum(four.ss_four_column_data_value_women) as 'four_column_data_women1',
				sum(four.ss_four_column_data_value_boys) as 'four_column_data_boys1',
				sum(four.ss_four_column_data_value_girls) as 'four_column_data_girls1' 
				FROM ss_four_column_data as four 
				LEFT JOIN ss_mpr_summary AS su ON four.ss_partner_id = su.ss_mpr_summary_partner_id 
				WHERE four.ss_metric_master_id IN ($metrec_ids) AND su.status='3'AND four.ss_four_column_data_status='1'"
			);	
		}else{
			
			$query = $this->db->query(
				"SELECT 
			sum(four.ss_four_column_data_value_men) as 'four_column_data_men1',
			sum(four.ss_four_column_data_value_women) as 'four_column_data_women1',
			sum(four.ss_four_column_data_value_boys) as 'four_column_data_boys1',
			sum(four.ss_four_column_data_value_girls) as 'four_column_data_girls1' 
			FROM ss_four_column_data as four  
			WHERE year(four.ss_four_column_data_month) = year('".$year."') AND four.ss_metric_master_id IN ($metrec_ids) AND four.ss_four_column_data_status='1'"
			);	
			
			
		} 
		//echo $this->db->last_query(); die;
	    return $query->row();
	}
	
	
}
