<?php

Class Reh_base_hospital_model extends CI_Model {
	protected $base_hospital = "ss_reh_base_hospital";
    
	public function createBaseHospital($data)
	{
		$this->db->trans_start();
        $this->db->insert($this->base_hospital,$data);
		$this->db->trans_complete();
		return $this->db->insert_id();
    }
	public function UpdateBase($data,$id) 
	{
		$this->db->where('ss_reh_base_hospital_id', $id);
		$this->db->update($this->base_hospital, $data);
    }
	
	
	
	
	
	public function getBaseHopital($month_from,$partner_id)
	{		
		$query = $this->db->query("Select ss_reh_base_hospital_id, ss_sno, ss_opd_no,ss_patient_name,ss_kin_name,ss_village,ss_block,ss_districts,ss_age,ss_gender,ss_ref_from_ref_by,ss_eye_oper,ss_pre_oper,ss_post_oper,ss_surgery_type,ss_surgery_support From $this->base_hospital Where ss_reh_base_month = '".$month_from."' AND ss_partner_id = '".$partner_id."'");
		return $query->result();
	}
	
		public function deleteHehBase($id)
	     {		
		$query = $this->db->query("delete  From  $this->base_hospital Where  ss_reh_base_hospital_id = '".$id."'");
		return true;
        }
	
		public function editFetchData($id)
			{		
				$query = $this->db->query("Select *, DATE_FORMAT(`ss_reh_base_month`,'%m-%Y') as date_month From $this->base_hospital Where ss_reh_base_hospital_id = '".$id."'");
				return $query->result();
			}
	
	
	public function createBaseHospitalExcel($data) {
		$this->db->trans_start();
        $this->db->insert_batch($this->base_hospital,$data);
		$this->db->trans_complete();
		return $this->db->insert_id();
    }
	
}
