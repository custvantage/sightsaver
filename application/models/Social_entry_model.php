<?php

Class Social_entry_model extends CI_Model {	
	

	// mpr get all metric data end //
	public function create_four_column($data)
	{
		$this->db->insert_batch('ss_four_column_data', $data);
		return $this->db->insert_id();
	}
	public function create_one_column($data)
	{
		$this->db->insert_batch('ss_one_column_data', $data);
		return $this->db->insert_id();
	}
	public function create_analysis($data)
	{
		$this->db->insert_batch('ss_analysis_data', $data);
		return $this->db->insert_id();
	}
	/* public function getInsertedIdOneColumn($start,$end)
	{
		$query = $this->db->query("Select ss_one_column_data_id From ss_one_column_data Where ss_one_column_data_id BETWEEN ".$start." AND ".$end."");
		return $query->result();
	} */
	public function getPartnerInfo($partner_id)
	{
		$query = $this->db->query("Select ss_partners_created_on From ss_partners Where ss_partners_id='".$partner_id."'");
		return $query->row();
	}
	public function createMprSummary($data)
	{
		$this->db->trans_start();
        $this->db->insert("ss_mpr_summary",$data);
		$this->db->trans_complete();
		return $this->db->insert_id();
	}
	public function checkMprExist($partner_id,$project_name,$module_name,$data_month)
	{
		$query = $this->db->query("Select ss_mpr_summary_id From ss_mpr_summary Where ss_mpr_summary_partner_id='".$partner_id."' AND ss_mpr_project_name='".$project_name."' AND ss_mpr_module_name='".$module_name."' AND ss_mpr_report_month='".$data_month."'");		
		return $query->row();
		
	}
	public function checkMprExistArr($partner_id,$project_name,$module_name,$data_month)
	{
		$query = $this->db->query("Select ss_mpr_summary_id,status,ss_mpr_report_month From ss_mpr_summary Where ss_mpr_summary_partner_id='".$partner_id."' AND ss_mpr_project_name='".$project_name."' AND ss_mpr_module_name='".$module_name."' AND ss_mpr_report_month='".$data_month."'");
		if($query->num_rows() > 0)
		{
			return $query->row_array();
		}else
		{
			$data['status'] = "";
			return $data;
		}
	}
	public function updateMprSummary($data,$where)
	{
		$query = $this->db->update('ss_mpr_summary',$data,$where);
		return 1;
	}
	public function getSectionId($project_name,$module_name)
	{
		$query = $this->db->query("Select ss_section_layout_id From ss_section_layout Where ss_section_layout_project_name='".$project_name."' AND ss_section_layout_module_name='".$module_name."'");		
		return $query->result();
	}
	public function getMetricId($ids,$year)
	{
		$query = $this->db->query("Select ss_metric_master_id From ss_metric_master Where ss_section_layout_ss_section_layout_id IN($ids) AND DATE_FORMAT(ss_metric_master_year,'%Y')='".$year."'");		
		return $query->result();
	}
	public function getOneColumnData($partner_id,$month_from,$metrics_ids)
	{
		$query = $this->db->query("Select ss_one_column_data_id From ss_one_column_data Where ss_partner_id='".$partner_id."' AND ss_one_column_data_month='".$month_from."' AND ss_metric_master_id IN ($metrics_ids)");
		return $query->result();
	}
	public function getFourColumnData($partner_id,$month_from,$metrics_ids)
	{
		$query = $this->db->query("Select ss_four_column_data_id From ss_four_column_data Where ss_partner_id='".$partner_id."' AND ss_four_column_data_month='".$month_from."' AND ss_metric_master_id IN ($metrics_ids)");		
		return $query->result();
	}
	public function getAnalysisData($partner_id,$month_from,$metrics_ids)
	{
		$query = $this->db->query("Select ss_analysis_data_id From ss_analysis_data Where ss_partner_id='".$partner_id."' AND ss_analysis_data_month='".$month_from."' AND ss_metric_master_id IN ($metrics_ids)");		
		return $query->result();
	}
	public function deleteOneColumnData($ids)
	{
		$query = $this->db->query("Delete From ss_one_column_data Where ss_one_column_data_id IN ($ids)");		
		return 1;
	}
	public function deleteFourColumnData($ids)
	{
		$query = $this->db->query("Delete From ss_four_column_data Where ss_four_column_data_id IN ($ids)");		
		return 1;
	}
	public function deleteAnalysisData($ids)
	{
		$query = $this->db->query("Delete From ss_analysis_data Where ss_analysis_data_id IN ($ids)");
		return 1;
	}
	
	public function getAllMetricDataMprFourDepends($month,$master_matric_depends_id)
	{   
	    $partner_id=$this->session->userdata('userinfo')['partner_id'];
		$query = $this->db->query("select sum(ss_four_column_data_value_men) as 'ss_four_column_data_value_men',sum(ss_four_column_data_value_women) as 'ss_four_column_data_value_women',sum(ss_four_column_data_value_boys) as 'ss_four_column_data_value_boys',sum(ss_four_column_data_value_girls) as 'ss_four_column_data_value_girls' from ss_four_column_data where ss_partner_id='".$partner_id."' AND ss_four_column_data_month='".$month."' AND ss_metric_master_id IN ($master_matric_depends_id)");
		return $query->result();
	}
	
	public function getAllMetricDataMprOneDepends($month,$master_matric_depends_id)
	{   
	    $partner_id=$this->session->userdata('userinfo')['partner_id'];
		$query = $this->db->query("select sum(ss_one_column_data_value) as 'ss_one_column_data_value' from ss_one_column_data where ss_partner_id='".$partner_id."' AND ss_one_column_data_month='".$month."' AND ss_metric_master_id IN ($master_matric_depends_id)");
		return $query->result();
	}
	
	public function getAllMetricDataMprAnalysisDepends($month,$master_matric_depends_id)
	{   
	    $partner_id=$this->session->userdata('userinfo')['partner_id'];
		$query = $this->db->query("select sum(ss_analysis_data_value) as 'ss_analysis_data_value' from ss_analysis_data where ss_partner_id='".$partner_id."' AND ss_analysis_data_month='".$month."' AND ss_metric_master_id IN ($master_matric_depends_id)");
		return $query->result();
	}
	
	public function create_four_column_mpr($data)
	{
		$this->db->insert_batch('ss_mpr_four_column_data', $data);
		return $this->db->insert_id();
	}
	
	public function create_one_column_mpr($data)
	{
		$this->db->insert_batch('ss_mpr_one_column_data', $data);
		return $this->db->insert_id();
	}
	
	public function create_analysis_column_mpr($data)
	{
		$this->db->insert_batch('ss_mpr_analysis_data', $data);
		return $this->db->insert_id();
	}

	public function traing()
	{ 
	            $partner_id = $this->session->userdata['userinfo']['partner_id'];
				$created_on = server_date_time();
				$name_block = $this->input->post('name_block');
				$name_person = $this->input->post('name_person');
				$gender = $this->input->post('gender');
				$cagtegory = $this->input->post('cagtegory');
				$category_detail = $this->input->post('category_detail');
				
				$topic = $this->input->post('topic');
				$combine_day = "01-".$this->input->post('month_data');
				$data_month = date("Y-m-d",strtotime($combine_day));
				
				
			   $this->db->query("insert into ss_training(ss_partner_id,ss_training_name_block,ss_training_name,ss_training_gender,ss_training_category,ss_training_category_details,ss_training_topic_covers,ss_month_data,ss_training_created_on)values($partner_id,'$name_block','$name_person','$gender','$cagtegory','$category_detail','$topic','$data_month','$created_on')");
	           return true;
	}
	
		public function training_get($month_from,$partner_id)
	{
		$query = $this->db->query("Select * From ss_training Where ss_month_data = '".$month_from."' AND ss_partner_id = '".$partner_id."'");
		return $query->result();
	}
	
    public function access_aidit_sii($data)
	  {
        $this->db->insert('ss_access_audit', $data);		
		return $this->db->insert_id();
     }
	 
	public function access_get($month_from,$partner_id)
	{
		$query = $this->db->query("Select * From ss_access_audit Where ss_month_data = '".$month_from."' AND ss_partner_id = '".$partner_id."'");
		return $query->result();
	} 
	 
	public function agencies_sii($data)
	  {
        $this->db->insert('ss_agencies_si', $data);		
		return $this->db->insert_id();
     } 
	public function agencies_get($month_from,$partner_id)
	{
		$query = $this->db->query("Select * From ss_agencies_si Where ss_month_data = '".$month_from."' AND ss_partner_id = '".$partner_id."'");
		return $query->result();
	}  
	
	public function shg_si($data)
	  {
        $this->db->insert('ss_ssg', $data);		
		return $this->db->insert_id();
     } 
	public function shg_get($month_from,$partner_id)
	{
		$query = $this->db->query("Select * From ss_ssg Where month_data = '".$month_from."' AND ss_partner_id = '".$partner_id."'");
		return $query->result();
	}
	
   public function bpo_si($data)
	  {
        $this->db->insert('ss_bpo_dpo_entry_si', $data);		
		return $this->db->insert_id();
      } 
	  public function bpo_get($month_from,$partner_id)
	{
		$query = $this->db->query("Select * From ss_bpo_dpo_entry_si Where ss_month_data = '".$month_from."' AND ss_partner_id = '".$partner_id."'");
		return $query->result();
	}
	 
	   public function advocacy_si($data)
	  {
        $this->db->insert('ss_advocacy_si', $data);		
		return $this->db->insert_id();
      } 
	  
	   public function advocacy_get($month_from,$partner_id)
	{
		$query = $this->db->query("Select * From ss_advocacy_si Where ss_month_data = '".$month_from."' AND ss_partner_id = '".$partner_id."'");
		return $query->result();
	}
	
	   public function iec_si($data)
	  {
        $this->db->insert('ss_iec_activity_si', $data);		
		return $this->db->insert_id();
      } 
	  
	     public function iec_get($month_from,$partner_id)
	{
		$query = $this->db->query("Select * From ss_iec_activity_si Where ss_month_data = '".$month_from."' AND ss_partner_id = '".$partner_id."'");
		return $query->result();
	}
	  
	     public function bcc_si($data)
	  {
        $this->db->insert('ss_bcc_si', $data);		
		return $this->db->insert_id();
      } 
	
	     public function bcc_get($month_from,$partner_id)
	{
		$query = $this->db->query("Select * From ss_bcc_si Where ss_month_data = '".$month_from."' AND ss_partner_id = '".$partner_id."'");
		return $query->result();
	}
	 
}