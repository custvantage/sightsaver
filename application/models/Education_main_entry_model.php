<?php

Class Education_main_entry_model extends CI_Model {	
	
    public function getAllSections($project_name, $module_name,$year) {
        $query = $this->db->query("SELECT section.ss_section_layout_section_name,master_desc.ss_section_layout_ss_section_layout_id FROM ss_section_layout as section INNER JOIN ss_metric_master as master_desc ON master_desc.ss_section_layout_ss_section_layout_id=section.ss_section_layout_id WHERE section.ss_section_layout_active_status = 1 AND section.ss_section_layout_project_name = '" . $project_name . "' AND section.ss_section_layout_module_name = '" . $module_name . "' AND DATE_FORMAT(master_desc.ss_metric_master_year,'%Y')='".$year."' GROUP BY section.ss_section_layout_id ORDER BY section.ss_section_layout_section_order ASC");
        return $query->result();
    }

    public function getAllMetricData($project_name, $module_name, $year) {
        $query = $this->db->query("Select master_desc.ss_metric_master_description,master_desc.ss_tooltip_description,section.ss_section_layout_section_col_count,section.ss_section_layout_analysis_description,section.ss_section_layout_section_name,master_desc.ss_metric_master_id,master_desc.ss_section_layout_ss_section_layout_id From ss_section_layout as section INNER JOIN ss_metric_master as master_desc ON master_desc.ss_section_layout_ss_section_layout_id=section.ss_section_layout_id WHERE master_desc.ss_metric_master_active=1 AND section.ss_section_layout_project_name='" . $project_name . "' AND section.ss_section_layout_module_name='" . $module_name . "' AND master_desc.ss_metric_master_active=1 AND DATE_FORMAT(master_desc.ss_metric_master_year,'%Y')='".$year."' ORDER BY master_desc.ss_section_layout_ss_section_layout_id asc");
        return $query->result();
    }
	public function create_four_column($data)
	{
		$this->db->insert_batch('ss_four_column_data', $data);
		return $this->db->insert_id();
	}
	public function create_one_column($data)
	{
		$this->db->insert_batch('ss_one_column_data', $data);
		return $this->db->insert_id();
	}
	public function create_eight_column($data)
	{
		$this->db->insert_batch('ss_eight_data', $data);
		return $this->db->insert_id();
	}
	/* public function getInsertedIdOneColumn($start,$end)
	{
		$query = $this->db->query("Select ss_one_column_data_id From ss_one_column_data Where ss_one_column_data_id BETWEEN ".$start." AND ".$end."");
		return $query->result();
	} */
	public function getPartnerInfo($partner_id)
	{
		$query = $this->db->query("Select ss_partners_created_on From ss_partners Where ss_partners_id='".$partner_id."'");
		return $query->row();
	}
	public function createMprSummary($data)
	{
		$this->db->trans_start();
        $this->db->insert("ss_mpr_summary",$data);
		$this->db->trans_complete();
		return $this->db->insert_id();
	}
	public function checkMprExist($partner_id,$project_name,$module_name,$data_month)
	{
		$query = $this->db->query("Select ss_mpr_summary_id From ss_mpr_summary Where ss_mpr_summary_partner_id='".$partner_id."' AND ss_mpr_project_name='".$project_name."' AND ss_mpr_module_name='".$module_name."' AND ss_mpr_report_month='".$data_month."'");		
		return $query->row();
		
	}
	public function checkMprExistArr($partner_id,$project_name,$module_name,$data_month)
	{
		$query = $this->db->query("Select ss_mpr_summary_id,status,ss_mpr_report_month From ss_mpr_summary Where ss_mpr_summary_partner_id='".$partner_id."' AND ss_mpr_project_name='".$project_name."' AND ss_mpr_module_name='".$module_name."' AND ss_mpr_report_month='".$data_month."'");
		if($query->num_rows() > 0)
		{
			return $query->row_array();
		}else
		{
			$data['status'] = "";
			return $data;
		}
	}
	public function updateMprSummary($data,$where)
	{
		$query = $this->db->update('ss_mpr_summary',$data,$where);
		return 1;
	}
	public function getSectionId($project_name,$module_name)
	{
		$query = $this->db->query("Select ss_section_layout_id From ss_section_layout Where ss_section_layout_project_name='".$project_name."' AND ss_section_layout_module_name='".$module_name."'");		
		return $query->result();
	}
	public function getMetricId($ids,$year)
	{
		$query = $this->db->query("Select ss_metric_master_id From ss_metric_master Where ss_section_layout_ss_section_layout_id IN($ids) AND DATE_FORMAT(ss_metric_master_year,'%Y')='".$year."'");		
		return $query->result();
	}
	public function getOneColumnData($partner_id,$month_from,$metrics_ids)
	{
		$query = $this->db->query("Select ss_one_column_data_id From ss_one_column_data Where ss_partner_id='".$partner_id."' AND ss_one_column_data_month='".$month_from."' AND ss_metric_master_id IN ($metrics_ids)");
		return $query->result();
	}
	public function getFourColumnData($partner_id,$month_from,$metrics_ids)
	{
		$query = $this->db->query("Select ss_four_column_data_id From ss_four_column_data Where ss_partner_id='".$partner_id."' AND ss_four_column_data_month='".$month_from."' AND ss_metric_master_id IN ($metrics_ids)");		
		return $query->result();
	}
	public function getAnalysisData($partner_id,$month_from,$metrics_ids)
	{
		$query = $this->db->query("Select ss_analysis_data_id From ss_analysis_data Where ss_partner_id='".$partner_id."' AND ss_analysis_data_month='".$month_from."' AND ss_metric_master_id IN ($metrics_ids)");		
		return $query->result();
	}
	public function deleteOneColumnData($ids)
	{
		$query = $this->db->query("Delete From ss_one_column_data Where ss_one_column_data_id IN ($ids)");		
		return 1;
	}
	public function deleteFourColumnData($ids)
	{
		$query = $this->db->query("Delete From ss_four_column_data Where ss_four_column_data_id IN ($ids)");		
		return 1;
	}
	public function deleteAnalysisData($ids)
	{
		$query = $this->db->query("Delete From ss_analysis_data Where ss_analysis_data_id IN ($ids)");
		return 1;
	}
	
}
