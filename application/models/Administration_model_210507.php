<?php

Class Administration_model extends CI_Model
 {

// insert the data for partner //////
public function partner_data($data1)
	{  
       $this->db->insert_batch('ss_partners', $data1);
		return $this->db->insert_id();
    }
    
// fetch the data to show partner //////	
   public function partner_data_fetch()
	{
       $query1 = $this->db->query("select p.ss_partners_id,p.ss_partners_name,p.ss_partners_project_reh,p.ss_partners_project_ueh,p.ss_partners_project_si,ss_partners_project_ie,d.ss_district_name from ss_partners as p join ss_district as d on p.ss_partners_districts=d.ss_district_id");
	   return $query1->result();
    }

	// fetch the value to show the district value //////
	public function district()
	{
       $query1 = $this->db->query("select ss_district_id,ss_district_name from ss_district ORDER BY ss_district_name");
		return $query1->result();
    }
	
	public function parnter_name($id)
	{
       $query1 = $this->db->query("select ss_partners_name from ss_partners where ss_partners_id=$id");
		return $query1->result();
    }
	
	// insert the data for partner //////
    public function district_insert($data1)
	{  
       $this->db->insert_batch('ss_district', $data1);
		return $this->db->insert_id();
    }
	
	
	public function user_partner_program($data1)
	{  
       $this->db->insert_batch('ss_partner_user', $data1);
	   return $this->db->insert_id();
    }
	
	public function partner_id_save($name,$date1)
	{  
        $this->db->query("insert into ss_partners(ss_partners_name,ss_partners_created_on)values('$name','$date1')");
		return $this->db->insert_id(); 
    }
	
	// fetch the value to show the state value //////
	public function state_fetch()
	{
       $query1 = $this->db->query("select ss_states_id, ss_states_name from ss_states ORDER BY ss_states_name");
		return $query1->result();
    }
	// fetch the data to show district_partner //////	
   public function district_partner_fetch()
	{
		$query1 = $this->db->query("SELECT  d.ss_district_name,s.ss_states_name
		FROM ss_district AS d JOIN ss_states AS s ON d.ss_states_ss_states_id = s.ss_states_id 
		ORDER BY d.ss_district_name");
	   return $query1->result();
    }

	// fetch the value to show the partner value in usr form  //////
	public function partner_name()
	{
       $query1 = $this->db->query("select ss_partners_id,ss_partners_name from ss_partners order by ss_partners_name");
		return $query1->result();
    }	
	// fetch the value to show the role value in user form  //////
	public function role()
	{
       $query1 = $this->db->query("select ss_role_id,ss_role_name from ss_role");
		return $query1->result();
    }
	
// insert the data for partner //////
public function user($data1)
	{  
       $this->db->insert_batch('ss_user', $data1);
		return $this->db->insert_id();
    }	
	
	// fetch the data to show partner //////	
 public function user_data_fetch()
	{
       $query1 = $this->db->query("SELECT u.ss_user_id, u.ss_user_fname, u.ss_user_lname, u.ss_user_email_id, u.ss_user_status, p.ss_partners_name, r.ss_role_name
FROM ss_user AS u
LEFT OUTER JOIN ss_partners AS p ON u.ss_partner_id = p.ss_partners_id
JOIN ss_role AS r ON ss_role_id = u.ss_user_role ");
	   return $query1->result();
    }

	public function login($email,$passward1)
	{
       $query1 = $this->db->query("SELECT u.ss_user_id, u.ss_user_fname, u.ss_partner_id,p.ss_partners_name,p.ss_partners_project_reh,p.ss_partners_project_ueh,p.ss_partners_project_si,p.ss_partners_project_ie ,u.ss_user_email_id, u.ss_user_role, r.ss_role_name,dis.ss_district_name,sta.ss_states_name
			FROM ss_user AS u
			left JOIN ss_role AS r ON u.ss_user_role = r.ss_role_id left join ss_partners as p on p.ss_partners_id=u.ss_partner_id left join ss_district as dis on dis.ss_district_id=p.ss_partners_districts left join ss_states as sta on sta.ss_states_id=dis.ss_states_ss_states_id 
			WHERE u.ss_user_email_id =  '$email' && u.ss_user_pwd =  '$passward1'");
	   return $query1->row();
    }
	
}
