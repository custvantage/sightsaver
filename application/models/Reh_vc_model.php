<?php

Class Reh_vc_model extends CI_Model {
	protected $vc = "ss_reh_vc";
    
	public function createVc($data) {
		$this->db->trans_start();
        $this->db->insert($this->vc,$data);
		$this->db->trans_complete();
		return $this->db->insert_id();
    }
	
	public function updateVc($data,$id) 
	{
		$this->db->where('ss_reh_vc_id', $id);
		$this->db->update($this->vc, $data);
    }
	
	
	
	
	
	public function getVc($month_from,$partner_id)
	{		
		$query = $this->db->query("Select ss_reh_vc_id,ss_vc_name,ss_vc_open,ss_vc_support,ss_subsidized_male,ss_subsidized_female,ss_subsidized_trans,ss_subsidized_boy,ss_subsidized_girl,ss_spectacles_male,ss_spectacles_female,ss_spectacles_trans,ss_spectacles_boy,ss_spectacles_girl,ss_screen_male,ss_screen_female,ss_screen_trans,ss_screen_boy,ss_screen_girl,ss_refra_male,ss_refra_female,ss_refra_trans,ss_refra_boy,ss_refra_girl,ss_refra_pres_male,ss_refra_pres_female,ss_refra_pres_trans,ss_refra_pres_boy,ss_refra_pres_girl,ss_refra_disp_male,ss_refra_disp_female,ss_refra_disp_trans,ss_refra_disp_boy,ss_refra_disp_girl,ss_refer_refer_male,ss_refer_refer_female,ss_refer_refer_trans,ss_refer_refer_boy,ss_refer_refer_girl,ss_refer_cat_male,ss_refer_cat_female,ss_refer_cat_trans,ss_refer_cat_boy,ss_refer_cat_girl From $this->vc Where ss_reh_vc_month = '".$month_from."' AND ss_partner_id = '".$partner_id."'");
		return $query->result();
	}
	
	   public function deleteHehVc($id)
	     {		
		$query = $this->db->query("delete  From  $this->vc Where ss_reh_vc_id = '".$id."'");
		return true;
        }
		
		public function editFetchData($id)
			{		
				$query = $this->db->query("Select *, DATE_FORMAT(`ss_reh_vc_month`,'%m-%Y') as date_month From $this->vc Where ss_reh_vc_id = '".$id."'");
				return $query->result();
			}
	
}
