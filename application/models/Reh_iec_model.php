<?php

Class Reh_iec_model extends CI_Model {
	protected $iec = "ss_reh_iec";
    
	public function createIec($data) {
		$this->db->trans_start();
        $this->db->insert($this->iec,$data);
		$this->db->trans_complete();
		return $this->db->insert_id();
    }
	
	public function updateIec($data,$id) 
	{
		$this->db->where('ss_reh_iec_id', $id);
		$this->db->update($this->iec, $data);
    }

	
	
	
	public function getIec($month_from,$partner_id)
	{		
		$query = $this->db->query("Select  ss_reh_iec_id,ss_reh_iec_block,ss_reh_iec_posters,ss_reh_iec_booklet,ss_reh_iec_pamplets,ss_reh_iec_wall,ss_reh_iec_others,ss_reh_through_iec From $this->iec Where ss_reh_iec_month = '".$month_from."' AND ss_partner_id = '".$partner_id."'");
		return $query->result();
	}

   public function deleteHehIec($id)
	     {		
		$query = $this->db->query("delete  From $this->iec Where ss_reh_iec_id = '".$id."'");
		return true;
        }
		
	public function editFetchData($id)
			{		
				$query = $this->db->query("Select *, DATE_FORMAT(`ss_reh_iec_month`,'%m-%Y') as date_month From $this->iec Where ss_reh_iec_id = '".$id."'");
				return $query->result();
			}
			
	
	
	
	
}
