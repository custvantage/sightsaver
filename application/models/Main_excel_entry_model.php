<?php

Class Main_excel_entry_model extends CI_Model 
  {
	public function four_column_excel_upload($cal_data)
	{
		 if ( $this->db->insert_batch('ss_four_column_data', $cal_data))
		 {
            return true;
         }
        return false; 
    }
	public function analysis_excel_upload($analysis_data)
	{
		 if ( $this->db->insert_batch('ss_analysis_data', $analysis_data))
		 {
            return true;
         }
        return false; 
    }
	
	public function one_column_excel_upload($cal_one_data)
	{
		 if ( $this->db->insert_batch('ss_one_column_data', $cal_one_data))
		 {
            return true;
         }
        return false; 
    }
   public function metric_map_detail($year)
	{
	 $query1 = $this->db->query("select * from ss_excel_metric_map where ss_excel_metric_map_project_name='rural eye health' && ss_excel_metric_map_field_count=4 && ss_excel_metric_map_year like '$year%'");
	 return $query1->result();
	
    }


	public function metric_map_detail_one_column($year)
	{
	 $query1 = $this->db->query("select * from ss_excel_metric_map where ss_excel_metric_map_project_name='rural eye health' && ss_excel_metric_map_field_count=1 && ss_excel_metric_map_section_name!='Analysis 1' && ss_excel_metric_map_section_name!='Analysis 2' && ss_excel_metric_map_section_name!='Analysis 3' && ss_excel_metric_map_section_name!='Analysis 4' && ss_excel_metric_map_year like '$year%'");
     return $query1->result();
		 
    }

	public function metric_map_detail_analysis($year)
	{
	 $query1 = $this->db->query("SELECT * FROM ss_excel_metric_map
      WHERE ss_excel_metric_map_project_name =  'rural eye health' && ss_excel_metric_map_field_count =1 && ( ss_excel_metric_map_section_name =  'Analysis 1' || ss_excel_metric_map_section_name =  'Analysis 2' || ss_excel_metric_map_section_name =  'Analysis 3' || ss_excel_metric_map_section_name =  'Analysis 4' && ss_excel_metric_map_year like '$year%')");
     return $query1->result();
    }
	
	
	public function metric_map_detail_ueh_four_column($year)
	{
	 $query1 = $this->db->query("select * from ss_excel_metric_map where ss_excel_metric_map_project_name='urban eye health' && ss_excel_metric_map_field_count=4 && ss_excel_metric_map_year like '$year%'");
	 return $query1->result();
	 
    }
	
	
	public function metric_map_detail_ueh_one_column($year)
	{
	 $query1 = $this->db->query("select * from ss_excel_metric_map where ss_excel_metric_map_project_name='urban eye health' && ss_excel_metric_map_field_count=1 && ss_excel_metric_map_section_name!='Analysis 1' && ss_excel_metric_map_section_name!='Analysis 2' && ss_excel_metric_map_section_name!='Analysis 3' && ss_excel_metric_map_section_name!='Analysis 4' && ss_excel_metric_map_year like '$year%'");
     return $query1->result();
		 
    }
	
	public function metric_map_detail_analysis_ueh($year)
	{
	 $query1 = $this->db->query("SELECT * FROM ss_excel_metric_map
      WHERE ss_excel_metric_map_project_name =  'urban eye health' && ss_excel_metric_map_field_count =1 && ( ss_excel_metric_map_section_name =  'Analysis 1' || ss_excel_metric_map_section_name =  'Analysis 2' || ss_excel_metric_map_section_name =  'Analysis 3' || ss_excel_metric_map_section_name =  'Analysis 4' && ss_excel_metric_map_year like '$year%')");
     return $query1->result();
    }
	
}


