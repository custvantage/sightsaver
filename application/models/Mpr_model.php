<?php

Class Mpr_model extends CI_Model 
{
	public function partner_fetch_data()
	{		
		$query = $this->db->query("Select ss_partners_id,ss_partners_name From ss_partners");
		return $query->result();
	}
	
	public function partner_summary_fatch($partner_id,$month_data)
	{		
		$query = $this->db->query("Select su.ss_mpr_project_name,su.ss_mpr_report_month,su.status,su.ss_mpr_district_cover_hospital,su.ss_mpr_module_name,su.ss_mpr_district_sightsaver,p.ss_partners_name,d.ss_district_name,s.ss_states_name From ss_mpr_summary as su left join ss_partners as p on su.ss_mpr_summary_partner_id=p.ss_partners_id  left join ss_district as d on p.ss_partners_districts=d.ss_district_id left join ss_states as s on s.ss_states_id=d.ss_states_ss_states_id where ss_mpr_summary_partner_id='$partner_id' && ss_mpr_report_month='$month_data' && ss_mpr_project_name='rural eye health'");
		//echo $this->db->last_query(); exit;
		return $query->row();
	}
	
	public function partner_summary_fetch_redirect($partner_id,$month_data,$project_name)
	{		
		$query = $this->db->query("Select su.ss_mpr_project_name,su.ss_mpr_report_month,su.status,su.ss_mpr_district_cover_hospital,su.ss_mpr_module_name,su.ss_mpr_district_sightsaver,p.ss_partners_name,d.ss_district_name,s.ss_states_name From ss_mpr_summary as su left join ss_partners as p on su.ss_mpr_summary_partner_id=p.ss_partners_id  left join ss_district as d on p.ss_partners_districts=d.ss_district_id left join ss_states as s on s.ss_states_id=d.ss_states_ss_states_id where ss_mpr_summary_partner_id='$partner_id' AND ss_mpr_report_month='$month_data' AND ss_mpr_project_name='$project_name'");
		//echo $this->db->last_query(); exit;
		return $query->row();
	}
	
	public function partner_summary_fatch_ueh($partner_id,$month_data)
	{		
		$query = $this->db->query("Select su.ss_mpr_project_name,su.ss_mpr_report_month,su.status,su.ss_mpr_district_cover_hospital,su.ss_mpr_module_name,su.ss_mpr_district_sightsaver,p.ss_partners_name,d.ss_district_name,s.ss_states_name From ss_mpr_summary as su left join ss_partners as p on su.ss_mpr_summary_partner_id=p.ss_partners_id  left join ss_district as d on p.ss_partners_districts=d.ss_district_id left join ss_states as s on s.ss_states_id=d.ss_states_ss_states_id where ss_mpr_summary_partner_id='$partner_id' && ss_mpr_report_month='$month_data' && ss_mpr_project_name='urban eye health'");
		return $query->row();
	}
	
	
	
	public function getAllMetricsData($project_name, $module_name, $year)
	{ 
		$this->db->simple_query('SET SESSION group_concat_max_len=15000');
		/*"SELECT lay.ss_section_layout_section_name,lay.ss_section_layout_section_col_count,lay.ss_section_layout_analysis_description,group_concat(m.ss_metric_master_description) as 'ss_metric_master_description',m.ss_metric_master_id,m.ss_section_layout_ss_section_layout_id  FROM `ss_section_layout` as lay left join ss_metric_master as m on lay.ss_section_layout_id=m.ss_section_layout_ss_section_layout_id
WHERE m.ss_metric_master_active=1 AND lay.ss_section_layout_project_name='" . $project_name . "' AND lay.ss_section_layout_module_name='" . $module_name . "' AND m.ss_metric_master_active=1 group by lay.ss_section_layout_section_name  ORDER BY m.ss_section_layout_ss_section_layout_id asc"  */

/* $query = $this->db->query("SELECT lay.ss_section_layout_section_name,lay.ss_section_layout_section_col_count,lay.ss_section_layout_analysis_description,group_concat(m.ss_metric_master_description SEPARATOR '#') as 'ss_metric_master_description',group_concat(m.ss_metric_master_id SEPARATOR '#') as 'ss_metric_master_id',m.ss_section_layout_ss_section_layout_id  FROM `ss_section_layout` as lay left join ss_metric_master as m on lay.ss_section_layout_id=m.ss_section_layout_ss_section_layout_id
        WHERE m.ss_metric_master_active=1 AND lay.ss_section_layout_project_name='" . $project_name . "' AND lay.ss_section_layout_module_name='" . $module_name . "' AND m.ss_metric_master_active=1 group by lay.ss_section_layout_section_name ORDER BY m.ss_section_layout_ss_section_layout_id asc");
        return $query->result(); */
      // SET SESSION group_concat_max_len:=4294967295;
		$query = $this->db->query("SELECT lay.ss_section_layout_section_name,lay.ss_section_layout_section_col_count,lay.ss_section_layout_analysis_description,group_concat(m.ss_metric_master_description SEPARATOR '#') as 'ss_metric_master_description',group_concat(m.ss_metric_master_id SEPARATOR '#') as 'ss_metric_master_id',group_concat(m.ss_metric_master_active SEPARATOR '#') as ss_metric_master_active ,m.ss_section_layout_ss_section_layout_id  FROM `ss_section_layout` as lay left join ss_metric_master as m on lay.ss_section_layout_id = m.ss_section_layout_ss_section_layout_id
        WHERE lay.ss_section_layout_project_name = '" . $project_name . "' AND lay.ss_section_layout_module_name='" . $module_name . "' AND m.ss_metric_master_year like '$year%' group by lay.ss_section_layout_section_name ORDER BY m.ss_section_layout_ss_section_layout_id asc");
        return $query->result();
	}
	public function state_fetch()
	{
		$query = $this->db->query("select ss_states_id,ss_states_name from ss_states");
		return $query->result();
	}

	public function partner_fetch($state,$project_name)
		{
		$query = $this->db->query("SELECT part.ss_partners_id,part.ss_partners_name
		FROM ss_partners AS part
		left join ss_mpr_summary as sum on sum.ss_mpr_summary_partner_id=part.ss_partners_id
		LEFT JOIN ss_district AS dist ON part.ss_partners_districts = dist.ss_district_id
		LEFT JOIN ss_states AS state ON dist.ss_states_ss_states_id = state.ss_states_id
		WHERE state.ss_states_name ='$state' && sum.ss_mpr_project_name='$project_name' && sum.status='5' group by part.ss_partners_id,part.ss_partners_name");
		return $query->result();
		}
		
	public function partner_fetch_all()
	{
		$query = $this->db->query("select * from ss_partners");
		return $query->result();
	}
	public function getPartnerState($partner_id)
	{
		$query = $this->db->query('Select s.ss_states_name From ss_partners as p INNER JOIN ss_district as d ON p.ss_partners_districts=d.ss_district_id INNER JOIN ss_states as s ON s.ss_states_id=d.ss_states_ss_states_id');
		return $query->row();
	}
	
	
}
