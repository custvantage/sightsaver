<?php

Class Reh_training_model extends CI_Model {
	protected $training = "ss_reh_training";
    
	public function createTraining($data) {
		$this->db->trans_start();
        $this->db->insert($this->training,$data);
		$this->db->trans_complete();
		return $this->db->insert_id();
    }
	
	public function updateTraining($data,$id) 
	{
		$this->db->where('ss_reh_training_id', $id);
		$this->db->update($this->training, $data);
    }

	public function getTraining($month_from,$partner_id)//($month_from,$partner_id,$limit)
	{
	
		$query = $this->db->query("Select ss_reh_training_id,ss_sno,ss_reh_training_block,ss_reh_training_person,ss_reh_training_gender,ss_reh_training_designation,ss_reh_training_designation_detail,ss_reh_training_loc_activity,ss_reh_training_loc_activity_detail,ss_reh_training_activity,ss_reh_training_activity_detail,ss_reh_training_participant_from,ss_reh_training_participant_from_detail From $this->training Where ss_reh_training_month = '".$month_from."' AND ss_partner_id = '".$partner_id."'");
		return $query->result();
	}
	
	 public function deleteHehTraining($id)
	     {		
		$query = $this->db->query("delete  From $this->training Where ss_reh_training_id = '".$id."'");
		return true;
        }
		
	public function editFetchData($id)
		{		
			$query = $this->db->query("Select *, DATE_FORMAT(`ss_reh_training_month`,'%m-%Y') as date_month From $this->training Where ss_reh_training_id = '".$id."'");
			return $query->result();
		}
	public function createTrainingCBExcel($data) {
		$this->db->trans_start();
        $this->db->insert_batch($this->training,$data);
		$this->db->trans_complete();
		return $this->db->insert_id();
    }
  }
