<?php
class Graph_model extends CI_Model {
	public function getProgram() {
		$query = $this->db->query ( "SELECT DISTINCT(ss_section_layout_project_name) AS ss_section_layout_project_name FROM ss_section_layout" );
		return $query->result ();
	}
	public function getStates() {
		$query = $this->db->query ( "SELECT ss_states_id,ss_states_name FROM ss_states WHERE status='Active' ORDER BY ss_states_name " );
		return $query->result ();
	}
	public function getDistrict($states) {
		$query = $this->db->query ( "SELECT ss_district_id,ss_district_name FROM ss_district WHERE ss_states_ss_states_id='$states'  ORDER BY ss_district_name" );
		return $query->result ();
	}

	public function getrehchart($chart_type, $chart_subtype) {
		switch ($chart_type) {
			case 'OPD_Examined' :
				switch ($chart_subtype) {
					case 'no_of_person_examined' :
						$where = '';
						$where1 = '';
						$input = $this->input->post ();
						if ($input ['quaterly_type'] == 'monthly') {
							$where .= " (MONTH(ss_mpr_four_column_data_month)='$input[month]' AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
							$where1 .= " (MONTH(ss_mpr_four_column_data_month)='$input[month]' AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						} else {
							$where .= " (MONTH(ss_mpr_four_column_data_month) in (" . implode ( ',', getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
							$where1 .= " (MONTH(ss_mpr_four_column_data_month) in (" . implode ( ',', getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						}
						if (isset ( $input ['states'] ) && $input ['states'] != '') {
							$where .= " AND ss_states.ss_states_id='$input[states]' ";
							$where1 .= " AND ss_states.ss_states_id='$input[states]' ";
						}
						if (isset ( $input ['districts'] ) && $input ['districts'] != '') {
							$where .= " AND ss_district.ss_partners_districts='$input[districts]' ";
							$where1 .= " AND ss_district.ss_partners_districts='$input[districts]' ";
						}
						$query = $this->db->query ( "SELECT IFNULL(SUM(ss_mpr_four_column_data_value_men+ss_mpr_four_column_data_value_women+ss_mpr_four_column_data_value_trans+ss_mpr_four_column_data_value_boys+ss_mpr_four_column_data_value_girls),0) as opd_count FROM ss_mpr_four_column_data,ss_partners,ss_district,ss_states
								 WHERE ss_mpr_metric_master_id=438 AND $where1 AND ss_mpr_four_column_data.ss_mpr_partner_id =ss_partners.ss_partners_id AND
								ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id " );
						$actual_result = $query->row ();
						
						$query = $this->db->query ( "SELECT IFNULL(SUM(ss_mpr_four_column_data_value_men+ss_mpr_four_column_data_value_women+ss_mpr_four_column_data_value_trans+ss_mpr_four_column_data_value_boys+ss_mpr_four_column_data_value_girls),0) as opd_count FROM ss_mpr_four_column_data,ss_partners,ss_district,ss_states
								 WHERE ss_mpr_metric_master_id=828 AND $where1 AND ss_mpr_four_column_data.ss_mpr_partner_id =ss_partners.ss_partners_id AND
								ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id " );
						$target_result = $query->row ();
						return [ 
								'actual_result' => $actual_result,
								'target_result' => $target_result 
						];
						break;
					
					case 'no_of_person_examined_state_wise' :
						$where = '';
						$where1 = '';
						$input = $this->input->post ();
						if ($input ['quaterly_type'] == 'monthly') {
							$where .= " (MONTH(ss_mpr_four_column_data_month)='$input[month]' AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
							$where1 .= " (MONTH(ss_mpr_four_column_data_month)='$input[month]' AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						} else {
							$where .= " (MONTH(ss_mpr_four_column_data_month) in (" . implode ( ',', getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
							$where1 .= " (MONTH(ss_mpr_four_column_data_month) in (" . implode ( ',', getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						}
						
						$query = $this->db->query ( "SELECT IFNULL(SUM(ss_mpr_four_column_data_value_men+ss_mpr_four_column_data_value_women+ss_mpr_four_column_data_value_trans+ss_mpr_four_column_data_value_boys+ss_mpr_four_column_data_value_girls),0) as opd_count,ss_states.ss_states_id FROM ss_mpr_four_column_data,ss_partners,ss_district,ss_states  WHERE ss_mpr_metric_master_id=438 AND $where1 AND ss_mpr_four_column_data.ss_mpr_partner_id =ss_partners.ss_partners_id 
									 AND ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id GROUP BY ss_states.ss_states_id" );
						$actual_result = $query->result ();
						
						$query = $this->db->query ( "SELECT IFNULL(SUM(ss_mpr_four_column_data_value_men+ss_mpr_four_column_data_value_women+ss_mpr_four_column_data_value_trans+ss_mpr_four_column_data_value_boys+ss_mpr_four_column_data_value_girls),0) as opd_count,ss_states.ss_states_id FROM ss_mpr_four_column_data,ss_partners,ss_district,ss_states  WHERE ss_mpr_metric_master_id=828 AND $where1 AND ss_mpr_four_column_data.ss_mpr_partner_id =ss_partners.ss_partners_id 
									 AND ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id GROUP BY ss_states.ss_states_id" );
						$target_result = $query->result ();
						
						$query = $this->db->query ( "SELECT ss_states_id,ss_states_name FROM ss_states WHERE status='Active' ORDER BY ss_states_name " );
						$states = $query->result ();
						return [ 
								'actual_result' => $actual_result,
								'target_result' => $target_result,
								'states' => $states 
						];
						break;
					
					case 'no_of_person_examined_state_wise_trend' :
						$where = '';
						$input = $this->input->post ();
						if ($input ['quaterly_type'] == 'monthly') {
							$where .= " ((MONTH(ss_mpr_four_column_data_month) BETWEEN 1 AND $input[month]) AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						} else {
							
							$where .= " ((MONTH(ss_mpr_four_column_data_month) BETWEEN 1 AND " . max ( getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						}
						$query = $this->db->query (
								"SELECT IFNULL(SUM(ss_mpr_four_column_data_value_men+ss_mpr_four_column_data_value_women+ss_mpr_four_column_data_value_trans+ss_mpr_four_column_data_value_boys+ss_mpr_four_column_data_value_girls),0) as opd_count,ss_states.ss_states_id,MONTH(ss_mpr_four_column_data_month) as month FROM ss_mpr_four_column_data,ss_partners,ss_district,ss_states  WHERE ss_mpr_metric_master_id=438 AND $where AND ss_mpr_four_column_data.ss_mpr_partner_id =ss_partners.ss_partners_id
								AND ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id GROUP BY ss_states.ss_states_id,MONTH(ss_mpr_four_column_data_month) "
								 );
						$actual_result = $query->result ();
						
						$query = $this->db->query ( "SELECT ss_states_id,ss_states_name FROM ss_states WHERE  status='Active' ORDER BY ss_states_name " );
						$states = $query->result ();
						return [ 
								'actual_result' => $actual_result,
								'states' => $states,
								'month' => $input ['month'] 
						];
						break;
				}
				break;
			
			// catract
			
			case 'Catract_Examined' :
				switch ($chart_subtype) {
					case 'no_of_person_examined' :
						$where = '';
						$where1 = '';
						$input = $this->input->post ();
						if ($input ['quaterly_type'] == 'monthly') {
							$where .= " (MONTH(ss_mpr_four_column_data_month)='$input[month]' AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
							$where1 .= " (MONTH(ss_mpr_four_column_data_month)='$input[month]' AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						} else {
							$where .= " (MONTH(ss_mpr_four_column_data_month) in (" . implode ( ',', getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
							$where1 .= " (MONTH(ss_mpr_four_column_data_month) in (" . implode ( ',', getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						}
						if (isset ( $input ['states'] ) && $input ['states'] != '') {
							$where .= " AND ss_states.ss_states_id='$input[states]' ";
							$where1 .= " AND ss_states.ss_states_id='$input[states]' ";
						}
						if (isset ( $input ['districts'] ) && $input ['districts'] != '') {
							$where .= " AND ss_district.ss_partners_districts='$input[districts]' ";
							$where1 .= " AND ss_district.ss_partners_districts='$input[districts]' ";
						}
						$query = $this->db->query ( "SELECT IFNULL(SUM(ss_mpr_four_column_data_value_men+ss_mpr_four_column_data_value_women+ss_mpr_four_column_data_value_trans+ss_mpr_four_column_data_value_boys+ss_mpr_four_column_data_value_girls),0) as opd_count FROM ss_mpr_four_column_data,ss_partners,ss_district,ss_states
									WHERE ss_mpr_metric_master_id=494 AND $where1 AND ss_mpr_four_column_data.ss_mpr_partner_id =ss_partners.ss_partners_id AND
									ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id " );
						$actual_result = $query->row ();
						
						$query = $this->db->query ( "SELECT IFNULL(SUM(ss_mpr_four_column_data_value_men+ss_mpr_four_column_data_value_women+ss_mpr_four_column_data_value_trans+ss_mpr_four_column_data_value_boys+ss_mpr_four_column_data_value_girls),0) as opd_count FROM ss_mpr_four_column_data,ss_partners,ss_district,ss_states
									WHERE ss_mpr_metric_master_id=867 AND $where1 AND ss_mpr_four_column_data.ss_mpr_partner_id =ss_partners.ss_partners_id AND
									ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id " );
						$target_result = $query->row ();
						return [ 
								'actual_result' => $actual_result,
								'target_result' => $target_result 
						];
						break;
					
					case 'no_of_person_examined_state_wise' :
						$where = '';
						$where1 = '';
						$input = $this->input->post ();
						if ($input ['quaterly_type'] == 'monthly') {
							$where .= " (MONTH(ss_mpr_four_column_data_month)='$input[month]' AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
							$where1 .= " (MONTH(ss_mpr_four_column_data_month)='$input[month]' AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						} else {
							$where .= " (MONTH(ss_mpr_four_column_data_month) in (" . implode ( ',', getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
							$where1 .= " (MONTH(ss_mpr_four_column_data_month) in (" . implode ( ',', getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						}
						
						$query = $this->db->query ( "SELECT IFNULL(SUM(ss_mpr_four_column_data_value_men+ss_mpr_four_column_data_value_women+ss_mpr_four_column_data_value_trans+ss_mpr_four_column_data_value_boys+ss_mpr_four_column_data_value_girls),0) as opd_count,ss_states.ss_states_id FROM ss_mpr_four_column_data,ss_partners,ss_district,ss_states  WHERE ss_mpr_metric_master_id=494 AND $where1 AND ss_mpr_four_column_data.ss_mpr_partner_id =ss_partners.ss_partners_id
									AND ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id GROUP BY ss_states.ss_states_id" );
						$actual_result = $query->result ();
						
						$query = $this->db->query ( "SELECT IFNULL(SUM(ss_mpr_four_column_data_value_men+ss_mpr_four_column_data_value_women+ss_mpr_four_column_data_value_trans+ss_mpr_four_column_data_value_boys+ss_mpr_four_column_data_value_girls),0) as opd_count,ss_states.ss_states_id FROM ss_mpr_four_column_data,ss_partners,ss_district,ss_states  WHERE ss_mpr_metric_master_id=828 AND $where1 AND ss_mpr_four_column_data.ss_mpr_partner_id =ss_partners.ss_partners_id
									AND ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id GROUP BY ss_states.ss_states_id" );
						$target_result = $query->result ();
						
						$query = $this->db->query ( "SELECT ss_states_id,ss_states_name FROM ss_states WHERE  status='Active' ORDER BY ss_states_name " );
						$states = $query->result ();
						return [ 
								'actual_result' => $actual_result,
								'target_result' => $target_result,
								'states' => $states 
						];
						break;
					
					case 'no_of_person_examined_state_wise_trend' :
						$where1 = '';
						$input = $this->input->post ();
						if ($input ['quaterly_type'] == 'monthly') {
							$where1 .= " ((MONTH(ss_mpr_four_column_data_month) BETWEEN 1 AND $input[month]) AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						} else {
							$where1 .= " ((MONTH(ss_mpr_four_column_data_month) BETWEEN 1 AND  " . max ( getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						}
						$query = $this->db->query ( "SELECT IFNULL(SUM(ss_mpr_four_column_data_value_men+ss_mpr_four_column_data_value_women+ss_mpr_four_column_data_value_trans+ss_mpr_four_column_data_value_boys+ss_mpr_four_column_data_value_girls),0) as opd_count,
								MONTH(ss_mpr_four_column_data_month) as month,ss_states.ss_states_id
								FROM ss_mpr_four_column_data,ss_partners,ss_district,ss_states  WHERE ss_mpr_metric_master_id=494 AND $where1 AND ss_mpr_four_column_data.ss_mpr_partner_id =ss_partners.ss_partners_id
								AND ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id GROUP BY MONTH(ss_mpr_four_column_data_month) ,ss_states.ss_states_id" );
						
						$actual_result = $query->result ();
						
						$query = $this->db->query ( "SELECT ss_states_id,ss_states_name FROM ss_states WHERE  status='Active' ORDER BY ss_states_name " );
						$states = $query->result ();
						return [ 
								'actual_result' => $actual_result,
								'states' => $states,
								'month' => $input ['month'] 
						];
						break;
				}
				break;
			
			// refraction
			
			case 'Refraction' :
				switch ($chart_subtype) {
					case 'no_of_person_examined' :
						$where = '';
						$where1 = '';
						$input = $this->input->post ();
						if ($input ['quaterly_type'] == 'monthly') {
							$where .= " (MONTH(ss_mpr_four_column_data_month)='$input[month]' AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
							$where1 .= " (MONTH(ss_mpr_four_column_data_month)='$input[month]' AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						} else {
							$where .= " (MONTH(ss_mpr_four_column_data_month) in (" . implode ( ',', getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
							$where1 .= " (MONTH(ss_mpr_four_column_data_month) in (" . implode ( ',', getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						}
						if (isset ( $input ['states'] ) && $input ['states'] != '') {
							$where .= " AND ss_states.ss_states_id='$input[states]' ";
							$where1 .= " AND ss_states.ss_states_id='$input[states]' ";
						}
						if (isset ( $input ['districts'] ) && $input ['districts'] != '') {
							$where .= " AND ss_district.ss_partners_districts='$input[districts]' ";
							$where1 .= " AND ss_district.ss_partners_districts='$input[districts]' ";
						}
						$query = $this->db->query ( "SELECT IFNULL(SUM(ss_mpr_four_column_data_value_men+ss_mpr_four_column_data_value_women+ss_mpr_four_column_data_value_trans+ss_mpr_four_column_data_value_boys+ss_mpr_four_column_data_value_girls),0) as opd_count FROM ss_mpr_four_column_data,ss_partners,ss_district,ss_states
									WHERE ss_mpr_metric_master_id=465 AND $where1 AND ss_mpr_four_column_data.ss_mpr_partner_id =ss_partners.ss_partners_id AND
									ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id " );
						$actual_result = $query->row ();
						
						$query = $this->db->query ( "SELECT IFNULL(SUM(ss_mpr_four_column_data_value_men+ss_mpr_four_column_data_value_women+ss_mpr_four_column_data_value_trans+ss_mpr_four_column_data_value_boys+ss_mpr_four_column_data_value_girls),0) as opd_count FROM ss_mpr_four_column_data,ss_partners,ss_district,ss_states
									WHERE ss_mpr_metric_master_id=842 AND $where1 AND ss_mpr_four_column_data.ss_mpr_partner_id =ss_partners.ss_partners_id AND
									ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id " );
						$target_result = $query->row ();
						return [ 
								'actual_result' => $actual_result,
								'target_result' => $target_result 
						];
						break;
					
					case 'no_of_person_examined_state_wise' :
						$where = '';
						$where1 = '';
						$input = $this->input->post ();
						if ($input ['quaterly_type'] == 'monthly') {
							$where .= " (MONTH(ss_mpr_four_column_data_month)='$input[month]' AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
							$where1 .= " (MONTH(ss_mpr_four_column_data_month)='$input[month]' AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						} else {
							$where .= " (MONTH(ss_mpr_four_column_data_month) in (" . implode ( ',', getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
							$where1 .= " (MONTH(ss_mpr_four_column_data_month) in (" . implode ( ',', getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						}
						
						$query = $this->db->query ( "SELECT IFNULL(SUM(ss_mpr_four_column_data_value_men+ss_mpr_four_column_data_value_women+ss_mpr_four_column_data_value_trans+ss_mpr_four_column_data_value_boys+ss_mpr_four_column_data_value_girls),0) as opd_count,ss_states.ss_states_id FROM ss_mpr_four_column_data,ss_partners,ss_district,ss_states  WHERE ss_mpr_metric_master_id=465 AND $where1 AND ss_mpr_four_column_data.ss_mpr_partner_id =ss_partners.ss_partners_id
												AND ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id GROUP BY ss_states.ss_states_id" );
						$actual_result = $query->result ();
						
						$query = $this->db->query ( "SELECT IFNULL(SUM(ss_mpr_four_column_data_value_men+ss_mpr_four_column_data_value_women+ss_mpr_four_column_data_value_trans+ss_mpr_four_column_data_value_boys+ss_mpr_four_column_data_value_girls),0) as opd_count,ss_states.ss_states_id FROM ss_mpr_four_column_data,ss_partners,ss_district,ss_states  WHERE ss_mpr_metric_master_id=842 AND $where1 AND ss_mpr_four_column_data.ss_mpr_partner_id =ss_partners.ss_partners_id
														AND ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id GROUP BY ss_states.ss_states_id" );
						$target_result = $query->result ();
						
						$query = $this->db->query ( "SELECT ss_states_id,ss_states_name FROM ss_states WHERE  status='Active' ORDER BY ss_states_name " );
						$states = $query->result ();
						return [ 
								'actual_result' => $actual_result,
								'target_result' => $target_result,
								'states' => $states 
						];
						break;
					
					case 'no_of_person_examined_state_wise_trend' :
						$where1 = '';
						$input = $this->input->post ();
						if ($input ['quaterly_type'] == 'monthly') {
							$where1 .= " ((MONTH(ss_mpr_four_column_data_month) BETWEEN 1 AND $input[month]) AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						} else {
							$where1 .= " ((MONTH(ss_mpr_four_column_data_month) BETWEEN 1 AND  " . max ( getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						}
						$query = $this->db->query ( "SELECT IFNULL(SUM(ss_mpr_four_column_data_value_men+ss_mpr_four_column_data_value_women+ss_mpr_four_column_data_value_trans+ss_mpr_four_column_data_value_boys+ss_mpr_four_column_data_value_girls),0) as opd_count,
												MONTH(ss_mpr_four_column_data_month) as month,ss_states.ss_states_id
												FROM ss_mpr_four_column_data,ss_partners,ss_district,ss_states  WHERE ss_mpr_metric_master_id=465 AND $where1 AND ss_mpr_four_column_data.ss_mpr_partner_id =ss_partners.ss_partners_id
												AND ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id GROUP BY MONTH(ss_mpr_four_column_data_month) ,ss_states.ss_states_id" );
						
						$actual_result = $query->result ();
						
						$query = $this->db->query ( "SELECT ss_states_id,ss_states_name FROM ss_states WHERE  status='Active' ORDER BY ss_states_name " );
						$states = $query->result ();
						return [ 
								'actual_result' => $actual_result,
								'states' => $states,
								'month' => $input ['month'] 
						];
						break;
				}
				break;
			
			case 'Spectacles' :
				switch ($chart_subtype) {
					case 'no_of_person_examined' :
						$where = '';
						$where1 = '';
						$input = $this->input->post ();
						if ($input ['quaterly_type'] == 'monthly') {
							$where .= " (MONTH(ss_mpr_four_column_data_month)='$input[month]' AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
							$where1 .= " (MONTH(ss_mpr_four_column_data_month)='$input[month]' AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						} else {
							$where .= " (MONTH(ss_mpr_four_column_data_month) in (" . implode ( ',', getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
							$where1 .= " (MONTH(ss_mpr_four_column_data_month) in (" . implode ( ',', getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						}
						if (isset ( $input ['states'] ) && $input ['states'] != '') {
							$where .= " AND ss_states.ss_states_id='$input[states]' ";
							$where1 .= " AND ss_states.ss_states_id='$input[states]' ";
						}
						if (isset ( $input ['districts'] ) && $input ['districts'] != '') {
							$where .= " AND ss_district.ss_partners_districts='$input[districts]' ";
							$where1 .= " AND ss_district.ss_partners_districts='$input[districts]' ";
						}
						$query = $this->db->query ( "SELECT IFNULL(SUM(ss_mpr_four_column_data_value_men+ss_mpr_four_column_data_value_women+ss_mpr_four_column_data_value_trans+ss_mpr_four_column_data_value_boys+ss_mpr_four_column_data_value_girls),0) as opd_count FROM ss_mpr_four_column_data,ss_partners,ss_district,ss_states
									WHERE ss_mpr_metric_master_id in (483,484,485) AND $where1 AND ss_mpr_four_column_data.ss_mpr_partner_id =ss_partners.ss_partners_id AND
									ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id " );
						$actual_result = $query->row ();
						
						$query = $this->db->query ( "SELECT IFNULL(SUM(ss_mpr_four_column_data_value_men+ss_mpr_four_column_data_value_women+ss_mpr_four_column_data_value_trans+ss_mpr_four_column_data_value_boys+ss_mpr_four_column_data_value_girls),0) as opd_count FROM ss_mpr_four_column_data,ss_partners,ss_district,ss_states
									WHERE ss_mpr_metric_master_id in (851,852,853) AND $where1 AND ss_mpr_four_column_data.ss_mpr_partner_id =ss_partners.ss_partners_id AND
									ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id " );
						$target_result = $query->row ();
						return [ 
								'actual_result' => $actual_result,
								'target_result' => $target_result 
						];
						break;
					
					case 'no_of_person_examined_state_wise' :
						$where = '';
						$where1 = '';
						$input = $this->input->post ();
						if ($input ['quaterly_type'] == 'monthly') {
							$where .= " (MONTH(ss_mpr_four_column_data_month)='$input[month]' AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
							$where1 .= " (MONTH(ss_mpr_four_column_data_month)='$input[month]' AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						} else {
							$where .= " (MONTH(ss_mpr_four_column_data_month) in (" . implode ( ',', getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
							$where1 .= " (MONTH(ss_mpr_four_column_data_month) in (" . implode ( ',', getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						}
						
						$query = $this->db->query ( "SELECT IFNULL(SUM(ss_mpr_four_column_data_value_men+ss_mpr_four_column_data_value_women+ss_mpr_four_column_data_value_trans+ss_mpr_four_column_data_value_boys+ss_mpr_four_column_data_value_girls),0) as opd_count,ss_states.ss_states_id FROM ss_mpr_four_column_data,ss_partners,ss_district,ss_states  WHERE ss_mpr_metric_master_id in (483,484,485) AND $where1 AND ss_mpr_four_column_data.ss_mpr_partner_id =ss_partners.ss_partners_id
												AND ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id GROUP BY ss_states.ss_states_id" );
						$actual_result = $query->result ();
						
						$query = $this->db->query ( "SELECT IFNULL(SUM(ss_mpr_four_column_data_value_men+ss_mpr_four_column_data_value_women+ss_mpr_four_column_data_value_trans+ss_mpr_four_column_data_value_boys+ss_mpr_four_column_data_value_girls),0) as opd_count,ss_states.ss_states_id FROM ss_mpr_four_column_data,ss_partners,ss_district,ss_states  WHERE ss_mpr_metric_master_id in (851,852,853) AND $where1 AND ss_mpr_four_column_data.ss_mpr_partner_id =ss_partners.ss_partners_id
														AND ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id GROUP BY ss_states.ss_states_id" );
						$target_result = $query->result ();
						
						$query = $this->db->query ( "SELECT ss_states_id,ss_states_name FROM ss_states WHERE  status='Active' ORDER BY ss_states_name " );
						$states = $query->result ();
						return [ 
								'actual_result' => $actual_result,
								'target_result' => $target_result,
								'states' => $states 
						];
						break;
					
					case 'no_of_person_examined_state_wise_trend' :
						$where1 = '';
						$input = $this->input->post ();
						if ($input ['quaterly_type'] == 'monthly') {
							$where1 .= " ((MONTH(ss_mpr_four_column_data_month) BETWEEN 1 AND $input[month]) AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						} else {
							$where1 .= " ((MONTH(ss_mpr_four_column_data_month) BETWEEN 1 AND  " . max ( getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						}
						$query = $this->db->query ( "SELECT IFNULL(SUM(ss_mpr_four_column_data_value_men+ss_mpr_four_column_data_value_women+ss_mpr_four_column_data_value_trans+ss_mpr_four_column_data_value_boys+ss_mpr_four_column_data_value_girls),0) as opd_count,
														MONTH(ss_mpr_four_column_data_month) as month,ss_states.ss_states_id
														FROM ss_mpr_four_column_data,ss_partners,ss_district,ss_states  WHERE ss_mpr_metric_master_id in (483,484,485) AND $where1 AND ss_mpr_four_column_data.ss_mpr_partner_id =ss_partners.ss_partners_id
														AND ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id GROUP BY MONTH(ss_mpr_four_column_data_month) ,ss_states.ss_states_id" );
						
						$actual_result = $query->result ();
						
						$query = $this->db->query ( "SELECT ss_states_id,ss_states_name FROM ss_states WHERE  status='Active'   ORDER BY ss_states_name " );
						$states = $query->result ();
						return [ 
								'actual_result' => $actual_result,
								'states' => $states,
								'month' => $input ['month'] 
						];
						break;
				}
				break;
		}
	}
	public function getuehchart($chart_type, $chart_subtype) {
		switch ($chart_type) {
			case 'OPD_Examined' :
				switch ($chart_subtype) {
					case 'no_of_person_examined' :
						$where = '';
						$where1 = '';
						$input = $this->input->post ();
						if ($input ['quaterly_type'] == 'monthly') {
							$where .= " (MONTH(ss_reh_base_month)='$input[month]' AND YEAR(ss_reh_base_month)='$input[year]')";
							$where1 .= " (MONTH(ss_mpr_four_column_data_month)='$input[month]' AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						} else {
							$where .= " (MONTH(ss_reh_base_month) in (" . implode ( ',', getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_reh_base_month)='$input[year]')";
							$where1 .= " (MONTH(ss_mpr_four_column_data_month) in (" . implode ( ',', getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						}
						if (isset ( $input ['states'] ) && $input ['states'] != '') {
							$where .= " AND ss_states.ss_states_id='$input[states]' ";
							$where1 .= " AND ss_states.ss_states_id='$input[states]' ";
						}
						if (isset ( $input ['districts'] ) && $input ['districts'] != '') {
							$where .= " AND ss_district.ss_partners_districts='$input[districts]' ";
							$where1 .= " AND ss_district.ss_partners_districts='$input[districts]' ";
						}
						$query = $this->db->query ( "SELECT COUNT(ss_reh_base_hospital.ss_reh_base_hospital_id) as opd_count FROM ss_reh_base_hospital,ss_partners,ss_district,ss_states WHERE $where AND ss_reh_base_hospital.ss_partner_id =ss_partners.ss_partners_id AND
								ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id " );
						$actual_result = $query->row ();
						
						$query = $this->db->query ( "SELECT IFNULL(SUM(ss_mpr_four_column_data_value_men+ss_mpr_four_column_data_value_women+ss_mpr_four_column_data_value_trans+ss_mpr_four_column_data_value_boys+ss_mpr_four_column_data_value_girls),0) as opd_count FROM ss_mpr_four_column_data,ss_partners,ss_district,ss_states
									 WHERE ss_mpr_metric_master_id=705 AND $where1 AND ss_mpr_four_column_data.ss_mpr_partner_id =ss_partners.ss_partners_id AND
										ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id " );
						$target_result = $query->row ();
						return [ 
								'actual_result' => $actual_result,
								'target_result' => $target_result 
						];
						break;
					
					case 'no_of_person_examined_state_wise' :
						$where = '';
						$where1 = '';
						$input = $this->input->post ();
						if ($input ['quaterly_type'] == 'monthly') {
							$where .= " (MONTH(ss_reh_base_month)='$input[month]' AND YEAR(ss_reh_base_month)='$input[year]')";
							$where1 .= " (MONTH(ss_mpr_four_column_data_month)='$input[month]' AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						} else {
							$where .= " (MONTH(ss_reh_base_month) in (" . implode ( ',', getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_reh_base_month)='$input[year]')";
							$where1 .= " (MONTH(ss_mpr_four_column_data_month) in (" . implode ( ',', getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						}
						
						$query = $this->db->query ( "SELECT COUNT(ss_reh_base_hospital.ss_reh_base_hospital_id) as opd_count,ss_states.ss_states_id FROM ss_reh_base_hospital,ss_partners,ss_district,ss_states WHERE $where AND ss_reh_base_hospital.ss_partner_id =ss_partners.ss_partners_id AND ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id GROUP BY ss_states.ss_states_id" );
						$actual_result = $query->result ();
						
						$query = $this->db->query ( "SELECT IFNULL(SUM(ss_mpr_four_column_data_value_men+ss_mpr_four_column_data_value_women+ss_mpr_four_column_data_value_trans+ss_mpr_four_column_data_value_boys+ss_mpr_four_column_data_value_girls),0) as opd_count,ss_states.ss_states_id FROM ss_mpr_four_column_data,ss_partners,ss_district,ss_states  WHERE ss_mpr_metric_master_id=705 AND $where1 AND ss_mpr_four_column_data.ss_mpr_partner_id =ss_partners.ss_partners_id
								AND ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id GROUP BY ss_states.ss_states_id" );
						$target_result = $query->result ();
						
						$query = $this->db->query ( "SELECT ss_states_id,ss_states_name FROM ss_states WHERE  status='Active' ORDER BY ss_states_name " );
						$states = $query->result ();
						return [ 
								'actual_result' => $actual_result,
								'target_result' => $target_result,
								'states' => $states 
						];
						break;
					
					case 'no_of_person_examined_state_wise_trend' :
						$where = '';
						$input = $this->input->post ();
						if ($input ['quaterly_type'] == 'monthly') {
							$where .= " ((MONTH(ss_reh_base_month) BETWEEN 1 AND $input[month]) AND YEAR(ss_reh_base_month)='$input[year]')";
						} else {
							
							$where .= " ((MONTH(ss_reh_base_month) BETWEEN 1 AND " . max ( getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_reh_base_month)='$input[year]')";
						}
						
						$query = $this->db->query ( "SELECT COUNT(ss_reh_base_hospital.ss_reh_base_hospital_id) as opd_count,MONTH(ss_reh_base_month) as month,ss_states.ss_states_id FROM ss_reh_base_hospital,ss_partners,ss_district,ss_states WHERE $where AND ss_reh_base_hospital.ss_partner_id =ss_partners.ss_partners_id AND ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id GROUP BY ss_states.ss_states_id" );
						$actual_result = $query->result ();
						
						$query = $this->db->query ( "SELECT ss_states_id,ss_states_name FROM ss_states WHERE  status='Active'  ORDER BY ss_states_name " );
						$states = $query->result ();
						return [ 
								'actual_result' => $actual_result,
								'states' => $states,
								'month' => $input ['month'] 
						];
						break;
				}
				break;
			
			// catract
			
			case 'Catract_Examined' :
				switch ($chart_subtype) {
					case 'no_of_person_examined' :
						$where = '';
						$where1 = '';
						$input = $this->input->post ();
						if ($input ['quaterly_type'] == 'monthly') {
							$where .= " (MONTH(ss_mpr_four_column_data_month)='$input[month]' AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
							$where1 .= " (MONTH(ss_mpr_four_column_data_month)='$input[month]' AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						} else {
							$where .= " (MONTH(ss_mpr_four_column_data_month) in (" . implode ( ',', getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
							$where1 .= " (MONTH(ss_mpr_four_column_data_month) in (" . implode ( ',', getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						}
						if (isset ( $input ['states'] ) && $input ['states'] != '') {
							$where .= " AND ss_states.ss_states_id='$input[states]' ";
							$where1 .= " AND ss_states.ss_states_id='$input[states]' ";
						}
						if (isset ( $input ['districts'] ) && $input ['districts'] != '') {
							$where .= " AND ss_district.ss_partners_districts='$input[districts]' ";
							$where1 .= " AND ss_district.ss_partners_districts='$input[districts]' ";
						}
						$query = $this->db->query ( "SELECT IFNULL(SUM(ss_mpr_four_column_data_value_men+ss_mpr_four_column_data_value_women+ss_mpr_four_column_data_value_trans+ss_mpr_four_column_data_value_boys+ss_mpr_four_column_data_value_girls),0) as opd_count FROM ss_mpr_four_column_data,ss_partners,ss_district,ss_states
									WHERE ss_mpr_metric_master_id=622 AND $where1 AND ss_mpr_four_column_data.ss_mpr_partner_id =ss_partners.ss_partners_id AND
									ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id " );
						$actual_result = $query->row ();
						
						$query = $this->db->query ( "SELECT IFNULL(SUM(ss_mpr_four_column_data_value_men+ss_mpr_four_column_data_value_women+ss_mpr_four_column_data_value_trans+ss_mpr_four_column_data_value_boys+ss_mpr_four_column_data_value_girls),0) as opd_count FROM ss_mpr_four_column_data,ss_partners,ss_district,ss_states
											WHERE ss_mpr_metric_master_id=726 AND $where1 AND ss_mpr_four_column_data.ss_mpr_partner_id =ss_partners.ss_partners_id AND
											ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id " );
						$target_result = $query->row ();
						return [ 
								'actual_result' => $actual_result,
								'target_result' => $target_result 
						];
						break;
					
					case 'no_of_person_examined_state_wise' :
						$where = '';
						$where1 = '';
						$input = $this->input->post ();
						if ($input ['quaterly_type'] == 'monthly') {
							$where .= " (MONTH(ss_mpr_four_column_data_month)='$input[month]' AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
							$where1 .= " (MONTH(ss_mpr_four_column_data_month)='$input[month]' AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						} else {
							$where .= " (MONTH(ss_mpr_four_column_data_month) in (" . implode ( ',', getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
							$where1 .= " (MONTH(ss_mpr_four_column_data_month) in (" . implode ( ',', getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						}
						
						$query = $this->db->query ( "SELECT IFNULL(SUM(ss_mpr_four_column_data_value_men+ss_mpr_four_column_data_value_women+ss_mpr_four_column_data_value_trans+ss_mpr_four_column_data_value_boys+ss_mpr_four_column_data_value_girls),0) as opd_count,ss_states.ss_states_id FROM ss_mpr_four_column_data,ss_partners,ss_district,ss_states  WHERE ss_mpr_metric_master_id=622 AND $where1 AND ss_mpr_four_column_data.ss_mpr_partner_id =ss_partners.ss_partners_id
					AND ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id GROUP BY ss_states.ss_states_id" );
						$actual_result = $query->result ();
						
						$query = $this->db->query ( "SELECT IFNULL(SUM(ss_mpr_four_column_data_value_men+ss_mpr_four_column_data_value_women+ss_mpr_four_column_data_value_trans+ss_mpr_four_column_data_value_boys+ss_mpr_four_column_data_value_girls),0) as opd_count,ss_states.ss_states_id FROM ss_mpr_four_column_data,ss_partners,ss_district,ss_states  WHERE ss_mpr_metric_master_id=726 AND $where1 AND ss_mpr_four_column_data.ss_mpr_partner_id =ss_partners.ss_partners_id
							AND ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id GROUP BY ss_states.ss_states_id" );
						$target_result = $query->result ();
						
						$query = $this->db->query ( "SELECT ss_states_id,ss_states_name FROM ss_states WHERE  status='Active' ORDER BY ss_states_name " );
						$states = $query->result ();
						return [ 
								'actual_result' => $actual_result,
								'target_result' => $target_result,
								'states' => $states 
						];
						break;
					
					case 'no_of_person_examined_state_wise_trend' :
						$where1 = '';
						$input = $this->input->post ();
						if ($input ['quaterly_type'] == 'monthly') {
							$where1 .= " ((MONTH(ss_mpr_four_column_data_month) BETWEEN 1 AND $input[month]) AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						} else {
							$where1 .= " ((MONTH(ss_mpr_four_column_data_month) BETWEEN 1 AND  " . max ( getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						}
						$query = $this->db->query ( "SELECT IFNULL(SUM(ss_mpr_four_column_data_value_men+ss_mpr_four_column_data_value_women+ss_mpr_four_column_data_value_trans+ss_mpr_four_column_data_value_boys+ss_mpr_four_column_data_value_girls),0) as opd_count,
										MONTH(ss_mpr_four_column_data_month) as month,ss_states.ss_states_id
										FROM ss_mpr_four_column_data,ss_partners,ss_district,ss_states  WHERE ss_mpr_metric_master_id=622 AND $where1 AND ss_mpr_four_column_data.ss_mpr_partner_id =ss_partners.ss_partners_id
										AND ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id GROUP BY MONTH(ss_mpr_four_column_data_month) ,ss_states.ss_states_id" );
						
						$actual_result = $query->result ();
						
						$query = $this->db->query ( "SELECT ss_states_id,ss_states_name FROM ss_states WHERE  status='Active' ORDER BY ss_states_name " );
						$states = $query->result ();
						return [ 
								'actual_result' => $actual_result,
								'states' => $states,
								'month' => $input ['month'] 
						];
						break;
				}
				break;
			
			// refraction
			
			case 'Refraction' :
				switch ($chart_subtype) {
					case 'no_of_person_examined' :
						$where = '';
						$where1 = '';
						$input = $this->input->post ();
						if ($input ['quaterly_type'] == 'monthly') {
							$where .= " (MONTH(ss_mpr_four_column_data_month)='$input[month]' AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
							$where1 .= " (MONTH(ss_mpr_four_column_data_month)='$input[month]' AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						} else {
							$where .= " (MONTH(ss_mpr_four_column_data_month) in (" . implode ( ',', getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
							$where1 .= " (MONTH(ss_mpr_four_column_data_month) in (" . implode ( ',', getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						}
						if (isset ( $input ['states'] ) && $input ['states'] != '') {
							$where .= " AND ss_states.ss_states_id='$input[states]' ";
							$where1 .= " AND ss_states.ss_states_id='$input[states]' ";
						}
						if (isset ( $input ['districts'] ) && $input ['districts'] != '') {
							$where .= " AND ss_district.ss_partners_districts='$input[districts]' ";
							$where1 .= " AND ss_district.ss_partners_districts='$input[districts]' ";
						}
						$query = $this->db->query ( "SELECT IFNULL(SUM(ss_mpr_four_column_data_value_men+ss_mpr_four_column_data_value_women+ss_mpr_four_column_data_value_trans+ss_mpr_four_column_data_value_boys+ss_mpr_four_column_data_value_girls),0) as opd_count FROM ss_mpr_four_column_data,ss_partners,ss_district,ss_states
					WHERE ss_mpr_metric_master_id in (614,615) AND $where1 AND ss_mpr_four_column_data.ss_mpr_partner_id =ss_partners.ss_partners_id AND
					ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id " );
						$actual_result = $query->row ();
						
						$query = $this->db->query ( "SELECT IFNULL(SUM(ss_mpr_four_column_data_value_men+ss_mpr_four_column_data_value_women+ss_mpr_four_column_data_value_trans+ss_mpr_four_column_data_value_boys+ss_mpr_four_column_data_value_girls),0) as opd_count FROM ss_mpr_four_column_data,ss_partners,ss_district,ss_states
							WHERE ss_mpr_metric_master_id in (712,713) AND $where1 AND ss_mpr_four_column_data.ss_mpr_partner_id =ss_partners.ss_partners_id AND
							ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id " );
						$target_result = $query->row ();
						return [ 
								'actual_result' => $actual_result,
								'target_result' => $target_result 
						];
						break;
					
					case 'no_of_person_examined_state_wise' :
						$where = '';
						$where1 = '';
						$input = $this->input->post ();
						if ($input ['quaterly_type'] == 'monthly') {
							$where .= " (MONTH(ss_mpr_four_column_data_month)='$input[month]' AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
							$where1 .= " (MONTH(ss_mpr_four_column_data_month)='$input[month]' AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						} else {
							$where .= " (MONTH(ss_mpr_four_column_data_month) in (" . implode ( ',', getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
							$where1 .= " (MONTH(ss_mpr_four_column_data_month) in (" . implode ( ',', getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						}
						
						$query = $this->db->query ( "SELECT IFNULL(SUM(ss_mpr_four_column_data_value_men+ss_mpr_four_column_data_value_women+ss_mpr_four_column_data_value_trans+ss_mpr_four_column_data_value_boys+ss_mpr_four_column_data_value_girls),0) as opd_count,ss_states.ss_states_id FROM ss_mpr_four_column_data,ss_partners,ss_district,ss_states  WHERE ss_mpr_metric_master_id in (614,615) AND $where1 AND ss_mpr_four_column_data.ss_mpr_partner_id =ss_partners.ss_partners_id
					AND ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id GROUP BY ss_states.ss_states_id" );
						$actual_result = $query->result ();
						
						$query = $this->db->query ( "SELECT IFNULL(SUM(ss_mpr_four_column_data_value_men+ss_mpr_four_column_data_value_women+ss_mpr_four_column_data_value_trans+ss_mpr_four_column_data_value_boys+ss_mpr_four_column_data_value_girls),0) as opd_count,ss_states.ss_states_id FROM ss_mpr_four_column_data,ss_partners,ss_district,ss_states  WHERE ss_mpr_metric_master_id in (712,713) AND $where1 AND ss_mpr_four_column_data.ss_mpr_partner_id =ss_partners.ss_partners_id
							AND ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id GROUP BY ss_states.ss_states_id" );
						$target_result = $query->result ();
						
						$query = $this->db->query ( "SELECT ss_states_id,ss_states_name FROM ss_states WHERE  status='Active' ORDER BY ss_states_name " );
						$states = $query->result ();
						return [ 
								'actual_result' => $actual_result,
								'target_result' => $target_result,
								'states' => $states 
						];
						break;
					
					case 'no_of_person_examined_state_wise_trend' :
						$where1 = '';
						$input = $this->input->post ();
						if ($input ['quaterly_type'] == 'monthly') {
							$where1 .= " ((MONTH(ss_mpr_four_column_data_month) BETWEEN 1 AND $input[month]) AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						} else {
							$where1 .= " ((MONTH(ss_mpr_four_column_data_month) BETWEEN 1 AND  " . max ( getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						}
						$query = $this->db->query ( "SELECT IFNULL(SUM(ss_mpr_four_column_data_value_men+ss_mpr_four_column_data_value_women+ss_mpr_four_column_data_value_trans+ss_mpr_four_column_data_value_boys+ss_mpr_four_column_data_value_girls),0) as opd_count,
										MONTH(ss_mpr_four_column_data_month) as month,ss_states.ss_states_id
										FROM ss_mpr_four_column_data,ss_partners,ss_district,ss_states  WHERE ss_mpr_metric_master_id in (614,615) AND $where1 AND ss_mpr_four_column_data.ss_mpr_partner_id =ss_partners.ss_partners_id
										AND ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id GROUP BY MONTH(ss_mpr_four_column_data_month) ,ss_states.ss_states_id" );
						
						$actual_result = $query->result ();
						
						$query = $this->db->query ( "SELECT ss_states_id,ss_states_name FROM ss_states WHERE  status='Active' ORDER BY ss_states_name " );
						$states = $query->result ();
						return [ 
								'actual_result' => $actual_result,
								'states' => $states,
								'month' => $input ['month'] 
						];
						break;
				}
				break;
			
			case 'Spectacles' :
				switch ($chart_subtype) {
					case 'no_of_person_examined' :
						$where = '';
						$where1 = '';
						$input = $this->input->post ();
						if ($input ['quaterly_type'] == 'monthly') {
							$where .= " (MONTH(ss_mpr_four_column_data_month)='$input[month]' AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
							$where1 .= " (MONTH(ss_mpr_four_column_data_month)='$input[month]' AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						} else {
							$where .= " (MONTH(ss_mpr_four_column_data_month) in (" . implode ( ',', getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
							$where1 .= " (MONTH(ss_mpr_four_column_data_month) in (" . implode ( ',', getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						}
						if (isset ( $input ['states'] ) && $input ['states'] != '') {
							$where .= " AND ss_states.ss_states_id='$input[states]' ";
							$where1 .= " AND ss_states.ss_states_id='$input[states]' ";
						}
						if (isset ( $input ['districts'] ) && $input ['districts'] != '') {
							$where .= " AND ss_district.ss_partners_districts='$input[districts]' ";
							$where1 .= " AND ss_district.ss_partners_districts='$input[districts]' ";
						}
						$query = $this->db->query ( "SELECT IFNULL(SUM(ss_mpr_four_column_data_value_men+ss_mpr_four_column_data_value_women+ss_mpr_four_column_data_value_trans+ss_mpr_four_column_data_value_boys+ss_mpr_four_column_data_value_girls),0) as opd_count FROM ss_mpr_four_column_data,ss_partners,ss_district,ss_states
										WHERE ss_mpr_metric_master_id in (618,619,620) AND $where1 AND ss_mpr_four_column_data.ss_mpr_partner_id =ss_partners.ss_partners_id AND
										ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id " );
						$actual_result = $query->row ();
						
						$query = $this->db->query ( "SELECT IFNULL(SUM(ss_mpr_four_column_data_value_men+ss_mpr_four_column_data_value_women+ss_mpr_four_column_data_value_trans+ss_mpr_four_column_data_value_boys+ss_mpr_four_column_data_value_girls),0) as opd_count FROM ss_mpr_four_column_data,ss_partners,ss_district,ss_states
												WHERE ss_mpr_metric_master_id in (718,719) AND $where1 AND ss_mpr_four_column_data.ss_mpr_partner_id =ss_partners.ss_partners_id AND
												ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id " );
						$target_result = $query->row ();
						return [ 
								'actual_result' => $actual_result,
								'target_result' => $target_result 
						];
						break;
					
					case 'no_of_person_examined_state_wise' :
						$where = '';
						$where1 = '';
						$input = $this->input->post ();
						if ($input ['quaterly_type'] == 'monthly') {
							$where .= " (MONTH(ss_mpr_four_column_data_month)='$input[month]' AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
							$where1 .= " (MONTH(ss_mpr_four_column_data_month)='$input[month]' AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						} else {
							$where .= " (MONTH(ss_mpr_four_column_data_month) in (" . implode ( ',', getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
							$where1 .= " (MONTH(ss_mpr_four_column_data_month) in (" . implode ( ',', getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						}
						
						$query = $this->db->query ( "SELECT IFNULL(SUM(ss_mpr_four_column_data_value_men+ss_mpr_four_column_data_value_women+ss_mpr_four_column_data_value_trans+ss_mpr_four_column_data_value_boys+ss_mpr_four_column_data_value_girls),0) as opd_count,ss_states.ss_states_id FROM ss_mpr_four_column_data,ss_partners,ss_district,ss_states  WHERE ss_mpr_metric_master_id in (618,619,620) AND $where1 AND ss_mpr_four_column_data.ss_mpr_partner_id =ss_partners.ss_partners_id
									AND ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id GROUP BY ss_states.ss_states_id" );
						$actual_result = $query->result ();
						
						$query = $this->db->query ( "SELECT IFNULL(SUM(ss_mpr_four_column_data_value_men+ss_mpr_four_column_data_value_women+ss_mpr_four_column_data_value_trans+ss_mpr_four_column_data_value_boys+ss_mpr_four_column_data_value_girls),0) as opd_count,ss_states.ss_states_id FROM ss_mpr_four_column_data,ss_partners,ss_district,ss_states  WHERE ss_mpr_metric_master_id in (718,719) AND $where1 AND ss_mpr_four_column_data.ss_mpr_partner_id =ss_partners.ss_partners_id
											AND ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id GROUP BY ss_states.ss_states_id" );
						$target_result = $query->result ();
						
						$query = $this->db->query ( "SELECT ss_states_id,ss_states_name FROM ss_states WHERE  status='Active' ORDER BY ss_states_name " );
						$states = $query->result ();
						return [ 
								'actual_result' => $actual_result,
								'target_result' => $target_result,
								'states' => $states 
						];
						break;
					
					case 'no_of_person_examined_state_wise_trend' :
						$where1 = '';
						$input = $this->input->post ();
						if ($input ['quaterly_type'] == 'monthly') {
							$where1 .= " ((MONTH(ss_mpr_four_column_data_month) BETWEEN 1 AND $input[month]) AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						} else {
							$where1 .= " ((MONTH(ss_mpr_four_column_data_month) BETWEEN 1 AND  " . max ( getQuaterMonth ( $input ['quater'] ) ) . ") AND YEAR(ss_mpr_four_column_data_month)='$input[year]')";
						}
						$query = $this->db->query ( "SELECT IFNULL(SUM(ss_mpr_four_column_data_value_men+ss_mpr_four_column_data_value_women+ss_mpr_four_column_data_value_trans+ss_mpr_four_column_data_value_boys+ss_mpr_four_column_data_value_girls),0) as opd_count,
					MONTH(ss_mpr_four_column_data_month) as month,ss_states.ss_states_id
					FROM ss_mpr_four_column_data,ss_partners,ss_district,ss_states  WHERE ss_mpr_metric_master_id in (618,619,620) AND $where1 AND ss_mpr_four_column_data.ss_mpr_partner_id =ss_partners.ss_partners_id
							AND ss_district.ss_district_id=ss_partners.ss_partners_districts AND ss_states.ss_states_id=ss_district.ss_states_ss_states_id GROUP BY MONTH(ss_mpr_four_column_data_month) ,ss_states.ss_states_id" );
						
						$actual_result = $query->result ();
						
						$query = $this->db->query ( "SELECT ss_states_id,ss_states_name FROM ss_states WHERE  status='Active' ORDER BY ss_states_name " );
						$states = $query->result ();
						return [ 
								'actual_result' => $actual_result,
								'states' => $states,
								'month' => $input ['month'] 
						];
						break;
				}
				break;
		}
	}
}
