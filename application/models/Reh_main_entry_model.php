<?php

Class Reh_main_entry_model extends CI_Model {	
	
    public function getAllSections($project_name, $module_name,$year) {
        $query = $this->db->query("SELECT section.ss_section_layout_section_name,master_desc.ss_section_layout_ss_section_layout_id FROM ss_section_layout as section INNER JOIN ss_metric_master as master_desc ON master_desc.ss_section_layout_ss_section_layout_id=section.ss_section_layout_id WHERE section.ss_section_layout_active_status = 1 AND section.ss_section_layout_project_name = '" . $project_name . "' AND section.ss_section_layout_module_name = '" . $module_name . "' AND DATE_FORMAT(master_desc.ss_metric_master_year,'%Y')='".$year."' GROUP BY section.ss_section_layout_id ORDER BY section.ss_section_layout_section_order ASC");
        return $query->result();
    }
	
	public function getAllSectionsUeh($project_name, $module_name,$year) {
        $query = $this->db->query("SELECT section.ss_section_layout_section_name,master_desc.ss_section_layout_ss_section_layout_id FROM ss_section_layout as section INNER JOIN ss_metric_master as master_desc ON master_desc.ss_section_layout_ss_section_layout_id=section.ss_section_layout_id WHERE section.ss_section_layout_active_status = 1 AND section.ss_section_layout_project_name = '" . $project_name . "' AND section.ss_section_layout_module_name = '" . $module_name . "' AND DATE_FORMAT(master_desc.ss_metric_master_year,'%Y')='".$year."' AND section.ss_section_layout_section_name!='OPD/Screening' GROUP BY section.ss_section_layout_id ORDER BY section.ss_section_layout_section_order ASC");
        return $query->result();
    }
	
	
	

    public function getAllMetricData($project_name, $module_name, $year) {
        $query = $this->db->query("Select master_desc.ss_metric_master_description,master_desc.ss_tooltip_description,section.ss_section_layout_section_col_count,section.ss_section_layout_analysis_description,section.ss_section_layout_section_name,master_desc.ss_metric_master_id,master_desc.ss_section_layout_ss_section_layout_id From ss_section_layout as section INNER JOIN ss_metric_master as master_desc ON master_desc.ss_section_layout_ss_section_layout_id=section.ss_section_layout_id WHERE master_desc.ss_metric_master_active=1 AND section.ss_section_layout_project_name='" . $project_name . "' AND section.ss_section_layout_module_name='" . $module_name . "' AND DATE_FORMAT(master_desc.ss_metric_master_year,'%Y')='".$year."' ORDER BY master_desc.ss_section_layout_ss_section_layout_id asc");
        return $query->result();
    }
	
	 public function getAllMetricDataInsert($project_name,$module_name,$year) 
	 {
        $query = $this->db->query("Select master_desc.ss_metric_master_id,master_desc.ss_metric_master_description From ss_section_layout as section INNER JOIN ss_metric_master as master_desc ON master_desc.ss_section_layout_ss_section_layout_id=section.ss_section_layout_id WHERE master_desc.ss_metric_master_active=0 AND section.ss_section_layout_project_name='" . $project_name . "' AND section.ss_section_layout_module_name='" . $module_name . "' AND DATE_FORMAT(master_desc.ss_metric_master_year,'%Y')='".$year."' ORDER BY master_desc.ss_section_layout_ss_section_layout_id asc");
        return $query->result();
    }
	// for the ueh one column data black row insert 
	 public function getOneMetricDataInsertUeh($project_name,$module_name,$year) 
	 {
        $query = $this->db->query("Select master_desc.ss_metric_master_id,master_desc.ss_metric_master_description From ss_section_layout as section INNER JOIN ss_metric_master as master_desc ON master_desc.ss_section_layout_ss_section_layout_id=section.ss_section_layout_id WHERE master_desc.ss_metric_master_active=0 AND section.ss_section_layout_project_name='" . $project_name . "' AND section.ss_section_layout_module_name='" . $module_name . "' AND DATE_FORMAT(master_desc.ss_metric_master_year,'%Y')='".$year."' AND ss_section_layout_section_name IN('Vision Centres /Camps','Human Resource','Advocacy / Networking / Liasioning','IEC / BCC Activity') ORDER BY master_desc.ss_section_layout_ss_section_layout_id asc");
        return $query->result();
    }
	 public function getAllMetricDataInsertUehFour($project_name,$module_name,$year) 
	 {
        $query = $this->db->query("Select master_desc.ss_metric_master_id,master_desc.ss_metric_master_description From ss_section_layout as section INNER JOIN ss_metric_master as master_desc ON master_desc.ss_section_layout_ss_section_layout_id=section.ss_section_layout_id WHERE master_desc.ss_metric_master_active=0 AND section.ss_section_layout_project_name='" . $project_name . "' AND section.ss_section_layout_module_name='" . $module_name . "' AND DATE_FORMAT(master_desc.ss_metric_master_year,'%Y')='".$year."' AND ss_section_layout_section_name IN('Referral','Refraction','Spectacles','Cataract','Glaucoma','Diabetic Retinopathy','Low Vision / Irreversible Blindness','Other Treatments') ORDER BY master_desc.ss_section_layout_ss_section_layout_id asc");
        return $query->result();
    }
	
	
	
	
	
	
	

	public function getAllMetricDataSunder($project_name, $module_name, $year) {
        $query = $this->db->query("Select master_desc.ss_metric_master_description,master_desc.ss_tooltip_description,section.ss_section_layout_section_col_count,section.ss_section_layout_analysis_description,section.ss_section_layout_section_name,master_desc.ss_metric_master_id,master_desc.ss_section_layout_ss_section_layout_id From ss_section_layout as section INNER JOIN ss_metric_master as master_desc ON master_desc.ss_section_layout_ss_section_layout_id=section.ss_section_layout_id WHERE master_desc.ss_metric_master_active=1 AND section.ss_section_layout_project_name='" . $project_name . "' AND section.ss_section_layout_module_name='" . $module_name . "' AND master_desc.ss_metric_master_active=1 AND DATE_FORMAT(master_desc.ss_metric_master_year,'%Y')='".$year."' AND ss_metric_master_id!=435 AND ss_metric_master_id!=436 AND ss_metric_master_id!=437 ORDER BY master_desc.ss_section_layout_ss_section_layout_id asc");
        return $query->result();
    }
	
	
	
	
	
	
	
	
	//one column mpr get all metric data////
	/*  public function getAllMetricDataMprOne($project_name, $module_name, $year, $month) {
		$partner_id=$_SESSION["partner_id"]; 
        $query = $this->db->query("Select master_desc.ss_metric_master_description, master_desc.ss_metric_master_id,master_desc.ss_metric_master_active,one.ss_one_column_data_value From ss_section_layout as section INNER JOIN ss_metric_master as master_desc ON master_desc.ss_section_layout_ss_section_layout_id=section.ss_section_layout_id left join ss_one_column_data as one on master_desc.ss_metric_master_id=one.ss_metric_master_id WHERE  section.ss_section_layout_project_name='" . $project_name . "' AND section.ss_section_layout_module_name='" . $module_name . "' AND DATE_FORMAT(master_desc.ss_metric_master_year,'%Y')='".$year."' AND one.ss_one_column_data_month='".$month."' AND one.ss_partner_id='".$partner_id."' ORDER BY master_desc.ss_section_layout_ss_section_layout_id asc");
        return $query->result(); 
    }*/
	
public function getAllMetricDataMprOne($project_name, $module_name, $year, $month) 
{
	
$partner_id=$_SESSION["partner_id"];
$query = $this->db->query("Select 
master_desc.ss_metric_master_description,
master_desc.ss_metric_master_id as 'master_matric_id',
one.ss_metric_master_id as 'data_matric_id',
master_desc.ss_section_layout_ss_section_layout_id,
master_desc.ss_metric_master_active,
one.ss_one_column_data_value,
'' as 'master_matric_formula_id',
'' as 'master_matric_depends_id' 
From ss_section_layout as section 
Left JOIN ss_metric_master as master_desc 
ON master_desc.ss_section_layout_ss_section_layout_id=section.ss_section_layout_id 
left outer join ss_one_column_data as one 
on master_desc.ss_metric_master_id=one.ss_metric_master_id 
left join ss_matric_formula as formula on master_desc.ss_metric_master_id=formula.ss_main_matric_id
WHERE section.ss_section_layout_project_name='" . $project_name . "' 
AND section.ss_section_layout_module_name='" . $module_name . "'
AND DATE_FORMAT(master_desc.ss_metric_master_year,'%Y')='".$year."'
AND one.ss_one_column_data_month='".$month."'
AND one.ss_partner_id='".$partner_id."'
UNION
Select master_desc.ss_metric_master_description,
master_desc.ss_metric_master_id as 'master_matric_id',
'' as 'data_matric_id', 
master_desc.ss_section_layout_ss_section_layout_id,
master_desc.ss_metric_master_active,
'',formula.ss_main_matric_id,formula.ss_related_matric_id
From ss_section_layout as section 
Left JOIN ss_metric_master as master_desc 
ON master_desc.ss_section_layout_ss_section_layout_id=section.ss_section_layout_id 
left join ss_matric_formula as formula on master_desc.ss_metric_master_id=formula.ss_main_matric_id
WHERE section.ss_section_layout_project_name='" . $project_name . "'
AND section.ss_section_layout_module_name='" . $module_name . "' 
AND DATE_FORMAT(master_desc.ss_metric_master_year,'%Y')='".$year."' 
AND master_desc.ss_metric_master_active ='0'
ORDER BY 4,2 asc");
return $query->result();
}		
	
	
	

//four column mpr get all metric data////
public function getAllMetricDataMprFour($project_name, $module_name, $year, $month) 
{
$partner_id=$_SESSION["partner_id"];
$query = $this->db->query("Select master_desc.ss_metric_master_description,
master_desc.ss_metric_master_id as 'master_matric_id',
four.ss_metric_master_id as 'data_matric_id',
master_desc.ss_section_layout_ss_section_layout_id,
master_desc.ss_metric_master_active,
four.ss_four_column_data_value_men,
four.ss_four_column_data_value_women,
four.ss_four_column_data_value_adult,
four.ss_four_column_data_value_trans,
four.ss_four_column_data_value_boys,
four.ss_four_column_data_value_girls,
four.ss_four_column_data_value_child,
formula.ss_main_matric_id as 'master_matric_formula_id',
formula.ss_related_matric_id as 'master_matric_depends_id'
From ss_section_layout as section 
Left JOIN ss_metric_master as master_desc 
ON master_desc.ss_section_layout_ss_section_layout_id=section.ss_section_layout_id 
left outer join ss_four_column_data as four 
on master_desc.ss_metric_master_id=four.ss_metric_master_id 
left join ss_matric_formula as formula on master_desc.ss_metric_master_id=formula.ss_main_matric_id
WHERE section.ss_section_layout_project_name='" . $project_name . "' 
AND section.ss_section_layout_module_name='" . $module_name . "'
AND DATE_FORMAT(master_desc.ss_metric_master_year,'%Y')='".$year."'
AND four.ss_four_column_data_month='".$month."'
AND four.ss_partner_id='".$partner_id."'
ORDER BY 4,2 asc");

/*
UNION
Select master_desc.ss_metric_master_description,
master_desc.ss_metric_master_id as 'master_matric_id',
'' as 'data_matric_id', 
master_desc.ss_section_layout_ss_section_layout_id,
master_desc.ss_metric_master_active,
'','','','','','','',formula.ss_main_matric_id,formula.ss_related_matric_id
From ss_section_layout as section 
Left JOIN ss_metric_master as master_desc 
ON master_desc.ss_section_layout_ss_section_layout_id=section.ss_section_layout_id 
left join ss_matric_formula as formula on master_desc.ss_metric_master_id=formula.ss_main_matric_id
WHERE section.ss_section_layout_project_name='" . $project_name . "'
AND section.ss_section_layout_module_name='" . $module_name . "' 
AND DATE_FORMAT(master_desc.ss_metric_master_year,'%Y')='".$year."' 
AND master_desc.ss_metric_master_active ='0'
ORDER BY 4,2 asc");
*/

return $query->result();
}

public function getAllMetricDataMprAnalysis($project_name, $module_name, $year, $month) 
{
$partner_id=$_SESSION["partner_id"];
$query = $this->db->query("Select 
master_desc.ss_metric_master_description,
master_desc.ss_metric_master_id as 'master_matric_id',
analysis.ss_metric_master_id as 'data_matric_id',
master_desc.ss_section_layout_ss_section_layout_id,
master_desc.ss_metric_master_active,
analysis.ss_analysis_data_value,
'' as 'master_matric_formula_id',
'' as 'master_matric_depends_id' 
From ss_section_layout as section 
Left JOIN ss_metric_master as master_desc 
ON master_desc.ss_section_layout_ss_section_layout_id=section.ss_section_layout_id 
left outer join ss_analysis_data as analysis 
on master_desc.ss_metric_master_id=analysis.ss_metric_master_id 
left join ss_matric_formula as formula on master_desc.ss_metric_master_id=formula.ss_main_matric_id
WHERE section.ss_section_layout_project_name='" . $project_name . "' 
AND section.ss_section_layout_module_name='" . $module_name . "'
AND DATE_FORMAT(master_desc.ss_metric_master_year,'%Y')='".$year."'
AND analysis.ss_analysis_data_month='".$month."'
AND analysis.ss_partner_id='".$partner_id."'
UNION
Select master_desc.ss_metric_master_description,
master_desc.ss_metric_master_id as 'master_matric_id',
'' as 'data_matric_id', 
master_desc.ss_section_layout_ss_section_layout_id,
master_desc.ss_metric_master_active,
'',formula.ss_main_matric_id,formula.ss_related_matric_id
From ss_section_layout as section 
Left JOIN ss_metric_master as master_desc 
ON master_desc.ss_section_layout_ss_section_layout_id=section.ss_section_layout_id 
left join ss_matric_formula as formula on master_desc.ss_metric_master_id=formula.ss_main_matric_id
WHERE section.ss_section_layout_project_name='" . $project_name . "'
AND section.ss_section_layout_module_name='" . $module_name . "' 
AND DATE_FORMAT(master_desc.ss_metric_master_year,'%Y')='".$year."' 
AND master_desc.ss_metric_master_active ='0'
ORDER BY 4,2 asc");
return $query->result();
}
////////////////////////////////////////////////
public function getAllMetricDataMprEight($project_name, $module_name, $year, $month) 
{
$partner_id=$_SESSION["partner_id"];
$query = $this->db->query("Select master_desc.ss_metric_master_description,
master_desc.ss_metric_master_id as 'master_matric_id',
eight.ss_metric_master_id as 'data_matric_id',
master_desc.ss_section_layout_ss_section_layout_id,
master_desc.ss_metric_master_active,
eight.ss_elementory_sanctioned,
eight.ss_elementory_boys,
eight.ss_elementory_girls,
eight.ss_elementory_vacant,
eight.ss_secondary_sanctioned,
eight.ss_secondary_boys,
eight.ss_secondary_girls,
eight.ss_secondary_vacant,
'' as 'master_matric_formula_id','' as 'master_matric_depends_id' 
 
From ss_section_layout as section 
Left JOIN ss_metric_master as master_desc 
ON master_desc.ss_section_layout_ss_section_layout_id=section.ss_section_layout_id 
left outer join ss_eight_data as eight 
on master_desc.ss_metric_master_id=eight.ss_metric_master_id 
left join ss_matric_formula as formula on master_desc.ss_metric_master_id=formula.ss_main_matric_id
WHERE section.ss_section_layout_project_name='" . $project_name . "' 
AND section.ss_section_layout_module_name='" . $module_name . "'
AND DATE_FORMAT(master_desc.ss_metric_master_year,'%Y')='".$year."'
AND eight.ss_data_month='".$month."'
AND eight.ss_partner_id='".$partner_id."'
UNION
Select master_desc.ss_metric_master_description,
master_desc.ss_metric_master_id as 'master_matric_id',
'' as 'data_matric_id', 
master_desc.ss_section_layout_ss_section_layout_id,
master_desc.ss_metric_master_active,
'','','','','','','','',formula.ss_main_matric_id,formula.ss_related_matric_id
From ss_section_layout as section 
Left JOIN ss_metric_master as master_desc 
ON master_desc.ss_section_layout_ss_section_layout_id=section.ss_section_layout_id 
left join ss_matric_formula as formula on master_desc.ss_metric_master_id=formula.ss_main_matric_id
WHERE section.ss_section_layout_project_name='" . $project_name . "'
AND section.ss_section_layout_module_name='" . $module_name . "' 
AND DATE_FORMAT(master_desc.ss_metric_master_year,'%Y')='".$year."' 
AND master_desc.ss_metric_master_active ='0'
ORDER BY 4,2 asc");
return $query->result();
}

////////////////////////////////////////////
public function getAllMetricDataMprFourDepends($month,$master_matric_depends_id)
	{   
	    $partner_id=$_SESSION["partner_id"];
		$query = $this->db->query("select sum(ss_four_column_data_value_men) as 'ss_four_column_data_value_men',sum(ss_four_column_data_value_women) as 'ss_four_column_data_value_women',sum(ss_four_column_data_value_boys) as 'ss_four_column_data_value_boys',sum(ss_four_column_data_value_girls) as 'ss_four_column_data_value_girls',sum(ss_four_column_data_value_adult) as 'ss_four_column_data_value_adult',sum(ss_four_column_data_value_child) as 'ss_four_column_data_value_child',sum(ss_four_column_data_value_trans) as 'ss_four_column_data_value_trans' from ss_four_column_data where ss_partner_id='".$partner_id."' AND ss_four_column_data_month='".$month."' AND ss_metric_master_id IN ($master_matric_depends_id)");
		return $query->result();
	}
	
	public function getAllMetricDataMprOneDepends($month,$master_matric_depends_id)
	{   
	    $partner_id=$_SESSION["partner_id"];
		$query = $this->db->query("select sum(ss_one_column_data_value) as 'ss_one_column_data_value' from ss_one_column_data where ss_partner_id='".$partner_id."' AND ss_one_column_data_month='".$month."' AND ss_metric_master_id IN ($master_matric_depends_id)");
		return $query->result();
	}
	
	public function getAllMetricDataMprAnalysisDepends($month,$master_matric_depends_id)
	{   
	    $partner_id=$_SESSION["partner_id"];
		$query = $this->db->query("select sum(ss_analysis_data_value) as 'ss_analysis_data_value' from ss_analysis_data where ss_partner_id='".$partner_id."' AND ss_analysis_data_month='".$month."' AND ss_metric_master_id IN ($master_matric_depends_id)");
		return $query->result();
	}
public function create_four_column_mpr($data)
	{
		$this->db->insert_batch('ss_mpr_four_column_data', $data);
		return $this->db->insert_id();
	}
	
   public function create_eight_column_mpr($data)
	{
		$this->db->insert_batch('ss_mpr_eight_column_data', $data);
		return $this->db->insert_id();
	}	
	
	public function create_one_column_mpr($data)
	{
		$this->db->insert_batch('ss_mpr_one_column_data',$data);
		return $this->db->insert_id();
	}
	
	public function create_analysis_column_mpr($data)
	{
		$this->db->insert_batch('ss_mpr_analysis_data', $data);
		return $this->db->insert_id();
	}
	
	// mpr get all metric data end //
	public function create_four_column($data)
	{  
		$this->db->insert_batch('ss_four_column_data', $data);
		return $this->db->insert_id();
	}
	public function create_one_column($data)
	{  //echo "<pre>"; print_R($data); die;
		$this->db->insert_batch('ss_one_column_data', $data);
		return $this->db->insert_id();
	}
	public function create_analysis($data)
	{
		$this->db->insert_batch('ss_analysis_data', $data);
		return $this->db->insert_id();
	}
	public function create_eight_column($data)
	{
		$this->db->insert_batch('ss_eight_data', $data);
		return $this->db->insert_id();
	}
	

	public function getPartnerInfo($partner_id)
	{
		$query = $this->db->query("Select ss_partners_created_on From ss_partners Where ss_partners_id='".$partner_id."'");
		return $query->row();
	}
	public function createMprSummary($data)
	{
		$this->db->trans_start();
        $this->db->insert("ss_mpr_summary",$data);
		$this->db->trans_complete();
		return $this->db->insert_id();
	}
	public function checkMprExist($partner_id,$project_name,$module_name,$data_month)
	{
		$query = $this->db->query("Select ss_mpr_summary_id From ss_mpr_summary Where ss_mpr_summary_partner_id='".$partner_id."' AND ss_mpr_project_name='".$project_name."' AND ss_mpr_module_name='".$module_name."' AND ss_mpr_report_month='".$data_month."'");		
		return $query->row();
		
	}
	public function checkMprExistArr($partner_id,$project_name,$module_name,$data_month)
	{  // echo $partner_id, $project_name, $module_name, $data_month;  die;
		$query = $this->db->query("Select ss_mpr_summary_id,status,ss_mpr_report_month From ss_mpr_summary Where ss_mpr_summary_partner_id='".$partner_id."' AND ss_mpr_project_name='".$project_name."' AND ss_mpr_module_name='".$module_name."' AND ss_mpr_report_month='".$data_month."'");
		if($query->num_rows() > 0)
		{
			return $query->row_array();
		} else
		{
			$data['status'] = "";
			return $data;
		}
	}
	
	public function checkMprExistArr11($partner_id,$project_name,$module_name,$data_month)
	{  // echo $partner_id, $project_name, $module_name, $data_month;  die;
		$query = $this->db->query("Select ss_mpr_summary_id,status,ss_mpr_report_month From ss_mpr_summary Where ss_mpr_summary_partner_id='".$partner_id."' AND ss_mpr_project_name='".$project_name."' AND ss_mpr_module_name='".$module_name."' AND ss_mpr_report_month='".$data_month."'");
		return $query->result();
	}
	
	public function updateMprSummary($data,$where)
	{
		$query = $this->db->update('ss_mpr_summary',$data,$where);
		return 1;
	}
	public function getSectionId($project_name,$module_name)
	{  //echo $project_name; echo $module_name; die;
		$query = $this->db->query("Select ss_section_layout_id From ss_section_layout Where ss_section_layout_project_name='".$project_name."' AND ss_section_layout_module_name='".$module_name."'");		
		return $query->result();
	}
	public function getMetricId($ids,$year)
	{
		$query = $this->db->query("Select ss_metric_master_id From ss_metric_master Where ss_section_layout_ss_section_layout_id IN($ids) AND DATE_FORMAT(ss_metric_master_year,'%Y')='".$year."'");		
		return $query->result();
	}
	public function getOneColumnData($partner_id,$month_from,$metrics_ids)
	{  //echo $partner_id; echo $month_from; echo $metrics_ids; die;
		$query = $this->db->query("Select ss_one_column_data_id From ss_one_column_data Where ss_partner_id='".$partner_id."' AND ss_one_column_data_month='".$month_from."' AND ss_metric_master_id IN ($metrics_ids)");
		return $query->result();
	}
	public function getFourColumnData($partner_id,$month_from,$metrics_ids)
	{
		$query = $this->db->query("Select ss_four_column_data_id From ss_four_column_data Where ss_partner_id='".$partner_id."' AND ss_four_column_data_month='".$month_from."' AND ss_metric_master_id IN ($metrics_ids)");		
		return $query->result();
	}
	public function getAnalysisData($partner_id,$month_from,$metrics_ids)
	{ // echo $partner_id; echo $month_from; echo $metrics_ids; die;
		$query = $this->db->query("Select ss_analysis_data_id From ss_analysis_data Where ss_partner_id='".$partner_id."' AND ss_analysis_data_month='".$month_from."' AND ss_metric_master_id IN ($metrics_ids)");		
		return $query->result();
	}
	public function getEightColumnData($partner_id,$month_from,$metrics_ids)
	{
		//echo $partner_id;  echo $month_from; echo $metrics_ids; die;
		$query = $this->db->query("Select ss_eight_data_id From ss_eight_data Where ss_partner_id='".$partner_id."' AND ss_data_month='".$month_from."' AND ss_metric_master_id IN ($metrics_ids)");		
		return $query->result();
	}
	
	
	
	
	
	public function deleteOneColumnData($ids)
	{ 
		$query = $this->db->query("Delete From ss_one_column_data Where ss_one_column_data_id IN ($ids)");		
		return 1;
	}
	public function deleteFourColumnData($ids)
	{   
		$query = $this->db->query("Delete From ss_four_column_data Where ss_four_column_data_id IN ($ids)");		
		return 1;
	}
	public function deleteAnalysisData($ids)
	{  
		$query = $this->db->query("Delete From ss_analysis_data Where ss_analysis_data_id IN ($ids)");
		return 1;
	}
	public function deleteEightColumnData($ids)
	{   //echo "<pre>"; die; print_R($ids); die;
		$query = $this->db->query("Delete From ss_eight_data Where ss_eight_data_id IN ($ids)");
		return 1;
	}
	public function user_wise_partner($userid)
	{  
		// Start: 26-05-17: Changed to replace Partner + District name in dropdown by only District name
		// $query1 = $this->db->query("select p.ss_partners_id,p.ss_partners_name from ss_partner_user as ps left join ss_partners as p on ps.ss_partner_id=p.ss_partners_id where ps.ss_user_id='$userid'");
		// End: 26-05-17 
		$query1 = $this->db->query("select p.ss_partners_id, d.ss_district_name as 'ss_partners_name' from ss_partner_user as ps left join ss_partners as p on ps.ss_partner_id=p.ss_partners_id left join ss_district as d on p.ss_partners_districts = d.ss_district_id where ps.ss_user_id='$userid'");
		return $query1->result();
    }
	
	public function partner_program($user_id)
	{  
       $query1 = $this->db->query("select ss_partners_project_reh,ss_partners_project_ueh,ss_partners_project_si,ss_partners_project_ie from ss_partners where ss_partners_id='$user_id'");
	   return $query1->result();
    }
	
}
