<?php

Class Reh_bcc_model extends CI_Model {
	protected $bcc = "ss_reh_bcc";
    
	public function createBcc($data) {
		$this->db->trans_start();
        $this->db->insert($this->bcc,$data);
		$this->db->trans_complete();
		return $this->db->insert_id();
    }
	
	public function updateBcc($data,$id) 
	{
		$this->db->where('ss_reh_bcc_id', $id);
		$this->db->update($this->bcc, $data);
    }

	public function getBcc($month_from,$partner_id)
	{		
		$query = $this->db->query("Select ss_reh_bcc_id,ss_reh_bcc_block,ss_reh_bcc_activity_type,ss_reh_bcc_sansitize,ss_reh_bcc_sansitize_detail,ss_reh_bcc_participant_no,ss_reh_bcc_topic From $this->bcc Where ss_reh_bcc_month = '".$month_from."' AND ss_partner_id = '".$partner_id."'");
		return $query->result();
	}
	
	 public function deleteHehBcc($id)
	     {		
		$query = $this->db->query("delete  From $this->bcc Where ss_reh_bcc_id = '".$id."'");
		return true;
        }
		
	public function editFetchData($id)
			{		
				$query = $this->db->query("Select *, DATE_FORMAT(`ss_reh_bcc_month`,'%m-%Y') as date_month From $this->bcc Where ss_reh_bcc_id = '".$id."'");
				return $query->result();
			}
	
}
