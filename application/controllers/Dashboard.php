<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	 public function __construct() {
        parent::__construct();       
		check_login();		
		$this->load->model('Dashboard_model');
		$this->load->model('Reh_main_entry_model');
		 $this->load->model('Approval_model');
		$this->load->model('Graph_model');
		
    }
	
    public function index()
	{
		$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid); 
		if($this->session->userdata('userinfo')['default_role']==6)
		{
			$partner_id = 0;//$_SESSION["partner_id"];
			$created_on = server_date_time();
			$userid=$this->session->userdata('userinfo')['user_id'];
			$date_only = date("Y-m",strtotime($created_on));
			$create_date = $date_only."-01";
			$today_date = date("Y-m-d",strtotime($create_date));
			$data['summary_data'] = $this->Approval_model->getPartnerFilterSummary($today_date, 0);
			$data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
			//echo "<pre>"; print_r($data); exit;
			$data['include'] = "mpr/partner_report";
			$this->load->view('container_login_dis', $data);
		}
		else
		{
			$data['include'] = "dashboard";
        	$this->load->view('container_login_dis', $data);
		}
		
    }
	
	 public function program_district()
	{ 
	    $data = array();
		if($this->input->post('id'))
		{
		   $user_id=$this->input->post('id');
		  
		   $data['menu']=$this->Reh_main_entry_model->partner_program($user_id); 
		   //echo "<pre>"; print_R($data['menu']); die;
		}
		//$data['include'] = "dashboard";
        $this->load->view('common/program_district', $data);
    }

	 public function tab_active()
	{ 
	    
		if($this->input->post('id') and $this->input->post('section'))
		{
		   $_SESSION['tab_id_'.$this->input->post('section')] = $this->input->post('id');
		}
     else {
		 
		 $_SESSION['tab_id']=1;
		 
	 }		

    }
	
	/*---------custom code------*/
	public function dashboard_speedometer()
	{
		$data = array();
		
		
		$data['mpr_summary_year'] = $this->Dashboard_model->getYearMprDetail();
		$data['mpr_summary_month'] = $this->Dashboard_model->getMonthMprDetail();
		$data['program'] = $this->Graph_model->getProgram();
		$data['states'] = $this->Graph_model->getStates();
		$data['input']  = $this->input->post ();
		
		
		
		$this->load->view('common/header_login');
		$this->load->view('dashboard/speedometer',$data);		
	}
	
	
	
	public function speedo_graph_data($project_name,$section,$year)
	{
		
		//--Actual data--//
		$module_name = "main entry";
		$metric_ids_fetch_main = $this->Dashboard_model->getMetricId_speedometer($project_name,$module_name,$section);		
		
		if(!empty($metric_ids_fetch_main)) {
		$data['graph_actual'] = $this->Dashboard_model->graph_realdata($year,$metric_ids_fetch_main);
		
		$man = 0;
		$women = 0;
		$boys = 0;
		$girl = 0;
		foreach($data['graph_actual'] as $key => $values) {
		$man += $values->ss_four_column_data_value_men;
		$women += $values->ss_four_column_data_value_women;
		$boys += $values->ss_four_column_data_value_boys;
		$girl += $values->ss_four_column_data_value_girls;
		}
		$data['totaldata_actual']=($man+$women+$boys+$girl);
		}
		
		//--Target data--// 
		$module_name = "yearly target";
		$metric_ids_fetch_target = $this->Dashboard_model->getMetricId_speedometer($project_name,$module_name,$section);		
		$data['graph_target'] = $this->Dashboard_model->graph_targetdata($year,$metric_ids_fetch_target);
		$selecteddate = $year;
		$data['selectedmonth'] =  date("m", strtotime($selecteddate)); 
	    $man1=$data['graph_target']->four_column_data_men1;
		$women1=$data['graph_target']->four_column_data_women1;
		$boys1=$data['graph_target']->four_column_data_boys1;
		$girl1=$data['graph_target']->four_column_data_girls1;
		$data['totaldatayearly']=($man1+$women1+$boys1+$girl1); 
		
		$data['totaldata_target'] =  round(($data['totaldatayearly']/12)* $data['selectedmonth']);
		
		return $data;
		
	}
	
	public function speedo_graph_data_multiple_metric_id_refraction($project_name,$section,$year)
	{
		
		//--Actual data--//
		$module_name = "main entry";
		$metric_ids_fetch_main = $this->Dashboard_model->getMetricId($project_name,$module_name,$section);
		
		$data['graph_refraction_actual'] = $this->Dashboard_model->graph_realdata($year,$metric_ids_fetch_main);
			
		$man = 0;
		$women = 0;
		$boys = 0;
		$girl = 0;
		foreach($data['graph_refraction_actual'] as $key => $values) {
		$man += $values->ss_four_column_data_value_men;
		$women += $values->ss_four_column_data_value_women;
		$boys += $values->ss_four_column_data_value_boys;
		$girl += $values->ss_four_column_data_value_girls;
		}
		$data['totaldata_refraction_actual']=($man+$women+$boys+$girl);
		 
        
		//--Target data--// 
		$module_name = "yearly target";
		
		$metric_ids_fetch_target = $this->Dashboard_model->getMetricId($project_name,$module_name,$section);		
		$data['graph_refraction_target'] = $this->Dashboard_model->graph_targetdata($year,$metric_ids_fetch_target);	
		$selecteddate = $year;
		$data['selectedmonth'] =  date("m", strtotime($selecteddate)); 
	    $man1=$data['graph_refraction_target']->four_column_data_men1;
		$women1=$data['graph_refraction_target']->four_column_data_women1;
		$boys1=$data['graph_refraction_target']->four_column_data_boys1;
		$girl1=$data['graph_refraction_target']->four_column_data_girls1;
		$data['totaldata_refraction_target_yearly']=($man1+$women1+$boys1+$girl1); 
		
		$data['totaldata_refraction_target'] =  round(($data['totaldata_refraction_target_yearly']/12)* $data['selectedmonth']);
	   	
		return $data;
	}
	
	public function speedo_graph_data_multiple_metric_id_spectacles($project_name,$section,$year)
	{
		
		//--Actual data--//
		$module_name = "main entry";
		$metric_ids_fetch_main = $this->Dashboard_model->getMetricId($project_name,$module_name,$section);
					
		$data['graph_spectacles_actual'] = $this->Dashboard_model->graph_realdata($year,$metric_ids_fetch_main);
		
		$man = 0;
		$women = 0;
		$boys = 0;
		$girl = 0;
		foreach($data['graph_spectacles_actual'] as $key => $values) {
		$man += $values->ss_four_column_data_value_men;
		$women += $values->ss_four_column_data_value_women;
		$boys += $values->ss_four_column_data_value_boys;
		$girl += $values->ss_four_column_data_value_girls;
		}
		$data['totaldata_spectacles_actual']=($man+$women+$boys+$girl);
		
		//--Target data--// 
		$module_name = "yearly target";
		
		$metric_ids_fetch_target = $this->Dashboard_model->getMetricId($project_name,$module_name,$section);		
		$data['graph_spectacles_target'] = $this->Dashboard_model->graph_targetdata($year,$metric_ids_fetch_target);	
		$selecteddate = $year;
		$data['selectedmonth'] =  date("m", strtotime($selecteddate));
	    $man1=$data['graph_spectacles_target']->four_column_data_men1;
		$women1=$data['graph_spectacles_target']->four_column_data_women1;
		$boys1=$data['graph_spectacles_target']->four_column_data_boys1;
		$girl1=$data['graph_spectacles_target']->four_column_data_girls1;
		$data['totaldata_spectacles_target_yearly']=($man1+$women1+$boys1+$girl1); 
	   	
		$data['totaldata_spectacles_target'] =  round(($data['totaldata_spectacles_target_yearly']/12)* $data['selectedmonth']);
		return $data;
	}
	/*---------custom code------*/
	
	
	
	
	
	
	
	public function dashboard_graph()
	{
		 $data['mpr_summary_year'] = $this->Dashboard_model->getYearMprDetail();
		$data['mpr_summary_month'] = $this->Dashboard_model->getMonthMprDetail();
		$data['program'] = $this->Graph_model->getProgram();
		$data['states'] = $this->Graph_model->getStates();
		$data['input']  = $this->input->post ();
		
		$data['graph_opd'] = "";
		$data['graph_cataract'] = "";			
		$data['graph_refraction'] = "";			
		$data['graph_spectacles'] = "";
		
		$this->load->view('common/header_login');
        $this->load->view('dashboard/dashboard_graph',$data);
	}	
	public function dashboard_graph_ueh()
	{		
		$data['mpr_summary_year'] = $this->Dashboard_model->getYearMprDetail();
		$data['mpr_summary_month'] = $this->Dashboard_model->getMonthMprDetail();
		$data['states'] = $this->Graph_model->getStates();
		$data['input']  = $this->input->post ();
		
		$data['graph_opd'] = "";
		$data['graph_cataract'] = "";			
		$data['graph_refraction'] = "";			
		$data['graph_spectacles'] = "";
		
		$this->load->view('common/header_login');
        $this->load->view('dashboard/dashboard_graph_ueh',$data);
	}	
	public function graph_filter_reh()
	{
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
		
			$project_name = "rural eye health";
			$module_name = "main entry";
			
			$combine_day = "01-".$this->input->post('month_data');  
			$month_data = date("Y-m-d",strtotime($combine_day));			
			
			$data['graph_opd'] = $this->graph_data($project_name,$module_name,"OPD/Screening",$month_data);
				
			$data['graph_cataract'] = $this->graph_data($project_name,$module_name,"cataract",$month_data);
				
			$data['graph_refraction'] = $this->graph_data_multiple_metric_id_refraction($project_name,$module_name,"refraction",$month_data);		
			
			$data['graph_spectacles'] = $this->graph_data_multiple_metric_id_spectacles($project_name,$module_name,"spectacles",$month_data);
				
			$this->load->view('common/header_login');
			$this->load->view('dashboard/dashboard_graph',$data);
			

		}
		else{
			redirect('dashboard_graph');
		}
	}
	public function graph_filter_ueh()
	{
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
		
			$project_name = "urban eye health";
			$module_name = "main entry";
			
			$combine_day = "01-".$this->input->post('month_data');  
			$month_data = date("Y-m-d",strtotime($combine_day));
			
			
			$data['graph_opd'] = $this->graph_data($project_name,$module_name,"OPD/Screening",$month_data);
			
			$data['graph_cataract'] = $this->graph_data($project_name,$module_name,"cataract",$month_data);
			
			$data['graph_refraction'] = $this->graph_data_multiple_metric_id_refraction($project_name,$module_name,"refraction",$month_data);		
			
			$data['graph_spectacles'] = $this->graph_data_multiple_metric_id_spectacles($project_name,$module_name,"spectacles",$month_data);
					
			$this->load->view('common/header_login');
			$this->load->view('dashboard/dashboard_graph_ueh',$data);
			

		}
		else{
			redirect('dashboard_graph_ueh');
		}
	}
	public function graph_data($project_name,$module_name,$section,$year)
	{

		$metric_ids_fetch_main = $this->Dashboard_model->getMetricId($project_name,$module_name,$section);		
		
		$data['graph'] = $this->Dashboard_model->graph_first1($year,$metric_ids_fetch_main);		
		$man = 0;
		$women = 0;
		$boys = 0;
		$girl = 0;
		foreach($data['graph'] as $key => $values) {
		$man += $values->ss_four_column_data_value_men;
		$women += $values->ss_four_column_data_value_women;
		$boys += $values->ss_four_column_data_value_boys;
		$girl += $values->ss_four_column_data_value_girls;
		}
		
		$data['totaldata']=($man+$women+$boys+$girl);
		 
        //State Wise
		$data['graph_state_main'] = $this->Dashboard_model->getStateWiseOpdMain1($year,$metric_ids_fetch_main);
		
		//Monthly
		$data['graph_monthly_main'] = $this->Dashboard_model->getMonthlyMain($year,$metric_ids_fetch_main);
		//var_dump($metric_ids_main); die;
		$module_name = "yearly target";
		
		$metric_ids_fetch_yearly = $this->Dashboard_model->getMetricId($project_name,$module_name,$section);
		$data['graph_yearly'] = $this->Dashboard_model->graph_first_yearly($year,$metric_ids_fetch_yearly);
		$selecteddate = $year;
		$data['selectedmonth'] =  date("m", strtotime($selecteddate)); 
	    $man1=$data['graph_yearly']->four_column_men1;
		$women1=$data['graph_yearly']->four_column_data_women1;
		$boys1=$data['graph_yearly']->four_column_data_boys1;
		$girl1=$data['graph_yearly']->four_column_data_girls1;
		$data['totaldatayearly'] = ($man1+$women1+$boys1+$girl1); 
		$data['totaldata1'] =  round(($data['totaldatayearly']/12)* $data['selectedmonth']);
	   	
		//State Wise
		$data['graph_state_yearly'] = $this->Dashboard_model->getStateWiseOpdYearly1($year,$metric_ids_fetch_yearly);		
		
		return $data;
	}
	public function graph_data_multiple_metric_id_refraction($project_name,$module_name,$section,$year)
	{
		
		$metric_ids_fetch_main = $this->Dashboard_model->getMetricId($project_name,$module_name,$section);
					
		
		// for the main entry 
		$data['graph'] = $this->Dashboard_model->graph_first1($year,$metric_ids_fetch_main);
			
		$man = 0;
		$women = 0;
		$boys = 0;
		$girl = 0;
		foreach($data['graph'] as $key => $values) {
		$man += $values->ss_four_column_data_value_men;
		$women += $values->ss_four_column_data_value_women;
		$boys += $values->ss_four_column_data_value_boys;
		$girl += $values->ss_four_column_data_value_girls;
		}
		
		$data['totaldata']=($man+$women+$boys+$girl);
		 
        //State Wise
		
		$data['graph_state_main'] = $this->Dashboard_model->getStateWiseOpdMain1($year,$metric_ids_fetch_main);
		
		//Monthly
		$data['graph_monthly_main'] = $this->Dashboard_model->getMonthlyMain($year,$metric_ids_fetch_main);		
		
		
		$module_name = "yearly target";
		
		$metric_ids_fetch_yearly = $this->Dashboard_model->getMetricId($project_name,$module_name,$section);		
		
		
		
		
		// for the yearly entry			
		$data['graph_yearly'] = $this->Dashboard_model->graph_first_yearly($year,$metric_ids_fetch_yearly);	
	    $selecteddate = $year;
		$data['selectedmonth'] =  date("m", strtotime($selecteddate)); 
		$man1=$data['graph_yearly']->four_column_men1;
		$women1=$data['graph_yearly']->four_column_data_women1;
		$boys1=$data['graph_yearly']->four_column_data_boys1;
		$girl1=$data['graph_yearly']->four_column_data_girls1;
		$data['totaldatayearly']=($man1+$women1+$boys1+$girl1); 
	   	$data['totaldata1'] =  round(($data['totaldatayearly']/12)* $data['selectedmonth']);
		//State Wise
		$data['graph_state_yearly'] = $this->Dashboard_model->getStateWiseOpdYearly1($year,$metric_ids_fetch_yearly);
		
		
		return $data;
	}
	public function graph_data_multiple_metric_id_spectacles($project_name,$module_name,$section,$year)
	{
		
		$metric_ids_fetch_main = $this->Dashboard_model->getMetricId($project_name,$module_name,$section);
		
		
		// for the main entry 
		$data['graph'] = $this->Dashboard_model->graph_first1($year,$metric_ids_fetch_main);
		$man = 0;
		$women = 0;
		$boys = 0;
		$girl = 0;
		foreach($data['graph'] as $key => $values) {
		$man += $values->ss_four_column_data_value_men;
		$women += $values->ss_four_column_data_value_women;
		$boys += $values->ss_four_column_data_value_boys;
		$girl += $values->ss_four_column_data_value_girls;
		}
		$data['totaldata']=($man+$women+$boys+$girl);
		 
        //State Wise
		$data['graph_state_main'] = $this->Dashboard_model->getStateWiseOpdMain1($year,$metric_ids_fetch_main);
		
		//Monthly
		$data['graph_monthly_main'] = $this->Dashboard_model->getMonthlyMain($year,$metric_ids_fetch_main);		
		
		$module_name = "yearly target";
		
		$metric_ids_fetch_yearly = $this->Dashboard_model->getMetricId($project_name,$module_name,$section);
		// for the yearly entry			
		$data['graph_yearly'] = $this->Dashboard_model->graph_first_yearly($year,$metric_ids_fetch_yearly);	
		 $selecteddate = $year;
		$data['selectedmonth'] =  date("m", strtotime($selecteddate)); 
	    $man1=$data['graph_yearly']->four_column_men1;
		$women1=$data['graph_yearly']->four_column_data_women1;
		$boys1=$data['graph_yearly']->four_column_data_boys1;
		$girl1=$data['graph_yearly']->four_column_data_girls1;
		$data['totaldatayearly']=($man1+$women1+$boys1+$girl1); 
	    $data['totaldata1'] =  round($data['totaldatayearly']/12)* $data['selectedmonth'];
		
		//State Wise
		$data['graph_state_yearly'] = $this->Dashboard_model->getStateWiseOpdYearly1($year,$metric_ids_fetch_yearly);		
		
		return $data;
	}

}
