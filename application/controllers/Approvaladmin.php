<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Approvaladmin extends CI_Controller {
	
	 public function __construct() {
        parent::__construct();       
		check_login();		
		$this->load->model('Approval_model');
    }
	
    public function index() {
		$created_on = server_date_time();
		$date_only = date("Y-m",strtotime($created_on));
		$create_date = $date_only."-01";
		$today_date = date("Y-m-d",strtotime($create_date));
		$data['summary_data'] = $this->Approval_model->getFilterSummaryadmin($today_date);
		//echo "<pre>"; print_R($data['summary_data']); die;
		$data['partner_data'] = $this->Approval_model->getAllPartner();
		$data['state_data'] = $this->Approval_model->getAllState();
		$data['district_data'] = $this->Approval_model->getAllDistrict();
		
		
		$data['include'] = "approvaladmin/approval_report_admin";
		$this->load->view('container_login1', $data);
    }
	public function filter_data_admin()
	{
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			
			if($this->input->post('month_from') && !empty($this->input->post('month_from')))
			{
			$combine_day = "01-".$this->input->post('month_from');
			$month_from = date("Y-m-d",strtotime($combine_day));
			}
			else if(empty($this->input->post('month_from')))
			{
				$month_from = "";
			}
			else{
				$month_from = "";
			}			
			$data['summary_data'] = $this->Approval_model->getFilterSummaryadmin($month_from);
		//	var_dump($data['summary_data']); 
			$data['csrfHash'] = $this->security->get_csrf_hash();
			if(!empty($data['summary_data']))
			{
				$data['success'] = 1;
			}else{
				$data['success']= 0;
			}			
			echo json_encode($data);
		}		
	}
}
