<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Reh_bcc extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Reh_bcc_model');
		$this->load->model('Reh_main_entry_model');
		check_login();
    }

    public function index() {
		$data['bcc_data'] = "";
		$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		$data['include'] = "reh/bcc";
		$this->load->view('container_login_dis', $data);
    }
	public function create()
	{$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
		
			$this->form_validation->set_rules('month_data','Month','required');
					
			if($this->form_validation->run()==true)
			{
				$partner_id = $_SESSION["partner_id"];
				$created_on = server_date_time();
				$ajax_id1 = $this->input->post('id_ajax');
				$block_name = $this->input->post('block_bcc');
				$activity_bcc = $this->input->post('activity_bcc');
				$sensitize_bcc = $this->input->post('sensitize_bcc');
				$sensitize_detail = $this->input->post('sensitize_detail');
				$participants_bcc = $this->input->post('participants_bcc');
				$topic_bcc = $this->input->post('topic_bcc');
				$combine_day = "01-".$this->input->post('month_data');
				$data_month = date("Y-m-d",strtotime($combine_day));
				if($this->input->post('id_ajax')=='')
				{
				$bcc_data = array('ss_partner_id'=>$partner_id,'ss_reh_bcc_block'=>$block_name,'ss_reh_bcc_activity_type'=>$activity_bcc,'ss_reh_bcc_sansitize'=>$sensitize_bcc,'ss_reh_bcc_sansitize_detail'=>$sensitize_detail,'ss_reh_bcc_participant_no'=>$participants_bcc,'ss_reh_bcc_topic'=>$topic_bcc,'ss_reh_bcc_month'=>$data_month,'ss_reh_bcc_created_on'=>$created_on);
				
				$creation_check = $this->Reh_bcc_model->createBcc($bcc_data);
				} else{
					$update_bcc_data = array('ss_partner_id'=>$partner_id,'ss_reh_bcc_block'=>$block_name,'ss_reh_bcc_activity_type'=>$activity_bcc,'ss_reh_bcc_sansitize'=>$sensitize_bcc,'ss_reh_bcc_sansitize_detail'=>$sensitize_detail,'ss_reh_bcc_participant_no'=>$participants_bcc,'ss_reh_bcc_topic'=>$topic_bcc,'ss_reh_bcc_month'=>$data_month,'ss_reh_bcc_created_on'=>$created_on);
				    $creation_check = $this->Reh_bcc_model->updateBcc($update_bcc_data,$ajax_id1);
				}
				
				$this->session->set_flashdata('success','Your data is successfully saved');
				redirect('filter_bcc_reh?month_from='.$_SESSION["month_date"]);
				//redirect('reh_bcc');
			}else{
				$data['include'] = "reh/bcc";
			$this->load->view('container_login_dis', $data);
			}
		}
	}
	public function filter_bcc_data()
	{   $userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		if($this->input->server('REQUEST_METHOD') === 'GET' and $this->input->get('month_from')!=null and preg_match('/^[0-9]{2}-[0-9]{4}\z/', $this->input->get('month_from')))
		{
			//$this->form_validation->set_rules('month_from','Month','required');
					
			//if($this->form_validation->run()==true)
			//{
				$combine_day = "01-".$this->input->get('month_from');
				$month_from = date("Y-m-d",strtotime($combine_day));
				
				
				$partner_id = $_SESSION["partner_id"];
				$data['bcc_data'] = $this->Reh_bcc_model->getBcc($month_from,$partner_id);
				
		//	}
			$data['include'] = "reh/bcc";
			$this->load->view('container_login_dis', $data);
		}else{
			redirect('reh_bcc');
		}
	}

   public function delete_bcc_reh()
	{ 
		if(!empty($this->uri->segment(2)))
        {
	     $id = base64_decode($this->uri->segment(2));
		 $this->Reh_bcc_model->deleteHehBcc($id);
	     redirect($_SERVER['HTTP_REFERER']);
		}
	}
	
	public function edit_bcc_reh()
	{ 
	     $id = base64_decode($this->input->post('id'));
		 $data=$this->Reh_bcc_model->editFetchData($id);
		 if(!empty($data))
		 {
			 $data2 = json_encode($data[0]);
			 echo $data2."$$$".$this->security->get_csrf_hash();
		 }
		 else
		 {
			 echo 0;
		 }
	}	
	
	
  
}
