<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Mpr extends CI_Controller {

    public function __construct()
	{
        parent::__construct();
		check_login();
        $this->load->model('Mpr_model');
		$this->load->model('Reh_main_entry_model');
		$this->load->library('metricdata');
		$this->load->library('common');
    }

	public function ajax_partner() 
	{  
		if($this->input->server('REQUEST_METHOD') === 'POST')
		        { 
			$options ="";
			$state_id=$this->input->post('state_name'); //exit;
		    $project_name=$this->input->post('project_name'); 
		
			//echo $state_id;
			if($state_id =="all_state")
			{
				$data=$this->Mpr_model->partner_fetch_all();
				
			}
			else if($state_id !="all_state" || $state_id !="" || $state_id !=0)
			{
				$data=$this->Mpr_model->partner_fetch($state_id,$project_name);
			}
			else
			{
				echo 0;
				exit;
				//$data=$this->Mpr_model->partner_fetch($state_id);
			}
			
			if(!empty($data))
		    {
				$options.="<option value=''>Select Partner</option>";
			foreach($data as $key=>$value)
			{
				$options.="<option value='".$value->ss_partners_id."'>".$value->ss_partners_name."</option>";
				
			}
			
			echo $options."|".$this->security->get_csrf_hash(); exit;
		}
		    
		//print_R($data); die;
	}
		//$data['include'] = "mpr/mpr_ie";
		//$this->load->view('container_login', $data);
    }
	
	public function demo() 
	{  
		if($this->input->server('REQUEST_METHOD') === 'POST'){
			
			$_SESSION["month_date"] = $this->input->post('param1'); //exit;
			echo $_SESSION["month_date"]."|".$this->security->get_csrf_hash(); exit;
	}
		//$data['include'] = "mpr/mpr_ie";
		//$this->load->view('container_login', $data);
    }
	
	public function demo_part() 
	{  
		if($this->input->server('REQUEST_METHOD') === 'POST'){
			$_SESSION['tab_id'] = 1;
	    	$_SESSION["partner_id"] = $this->input->post('param1'); 
			echo $_SESSION["partner_id"]."|".$this->security->get_csrf_hash(); exit;
	}
		//$data['include'] = "mpr/mpr_ie";
		//$this->load->view('container_login', $data);
    }
	
	
	public function mpr_reh()
	{
		$data['state_fetch']=$this->Mpr_model->state_fetch();
		      $data['partner_summary_data']="";
			  if($this->input->server('REQUEST_METHOD') === 'POST')
		        { 
			             date_default_timezone_set('Asia/Kolkata');
	    		    	 $state_name=$this->input->post('state_name');
			    	 	 $partner_id=$this->input->post('partner_name_post');  
			    	     $combine_day = "01-".$this->input->post('month_from1');
			    		 $combine_day_last = "01-".$this->input->post('month_from_last1');  
					  
				    $month_data = date("Y-m-d",strtotime($combine_day));  
					$month_data_last = date("Y-m-d",strtotime($combine_day_last)); 
				 	$year = date("Y",strtotime($combine_day)); 
					$project_name = "rural eye health";
					
			  
			  $data['partner_summary_data']=$this->Mpr_model->partner_summary_fatch($partner_id,$month_data);
              //echo "<pre>"; print_R($data['partner_summary_data']); die;
			  if(!empty($data['partner_summary_data']))
				   {
				  $module_name='main entry'; 
				  $abc = $this->Mpr_model->getAllMetricsData($project_name,$module_name,$year,1);
				   //echo "<pre>";print_R($abc); die; 
				  $module_name_yearly='yearly target'; 
				  $xyz = $this->Mpr_model->getAllMetricsData($project_name,$module_name_yearly,$year,2);
				//  echo "<pre>";print_R($xyz); die; 
				  $data['merge'] = $abc;
				  $data['merge2'] = $xyz;
				  $data['month_id'] = array($state_name,$month_data,$month_data_last,$partner_id);
				   }
				   else
				   { 
				  $module_name='main entry'; 
				  $abc = $this->Mpr_model->getAllMetricsData($project_name,$module_name,$year,1);
				  $module_name_yearly='yearly target';
				  $xyz = $this->Mpr_model->getAllMetricsData($project_name,$module_name_yearly,$year,2);
				  $data['merge'] = $abc;
				  $data['merge2'] = $xyz;
				  $data['month_id'] = array($state_name,$month_data,$month_data_last,$partner_id);
				   }
				}				
		$data['include'] = "mpr/mpr_reh";
		$this->load->view('container_login', $data);
	}
	
	
	
	public function mpr_reh_redirect()
	{         
	    //var_dump($this->input->post()); die;
		   date_default_timezone_set('Asia/Kolkata');
		      $data['state_fetch']=$this->Mpr_model->state_fetch();
		      //$data['partner_summary_data']= "";				
		       $month_from = base64_decode($this->uri->segment(2)); 
			   $data['post_month_from'] = $month_from; 
			   
			   $module_name='main entry';
						if($this->uri->segment(1) == "mpr_reh_redirect")
						{
							$project_name = "rural eye health";
						}
						else if($this->uri->segment(1) == "mpr_ueh_redirect")
						{
							$project_name = "urban eye health";
						}
						else if($this->uri->segment(1) == "mpr_social_redirect")
						{
							$project_name = "Social inclusion";
						}
						else if($this->uri->segment(1) == "mpr_education_redirect")
						{
							$project_name = "Inclusive education";
						}
						
				$partner_id = base64_decode($this->uri->segment(3));
				if($this->session->userdata('userinfo')['default_role'] == 6)//if partner
				{
					$data['session_partner_id'] = $partner_id;
					$check_mpr_summ_exist = $this->Reh_main_entry_model->checkMprExistArr($data['session_partner_id'],$project_name,$module_name,$month_from);//check mpr exist				
					if($check_mpr_summ_exist['status'] == 1 || $check_mpr_summ_exist['status'] == 4)
					{
						$data['mpr_id'] = $check_mpr_summ_exist['ss_mpr_summary_id'];
						$data['mpr_report_month'] = $check_mpr_summ_exist['ss_mpr_report_month'];
					}else
					{
						$data['mpr_id'] = "";
						$data['mpr_report_month'] = "";
					}
				}
				else
				{
					$data['session_partner_id'] = $partner_id;				
					$data['partners'] = $this->common->getPartners();
					$user_id = $this->session->userdata('userinfo')['user_id'];
					$data['comments'] = $this->common->getComments($user_id,$month_from,$data['session_partner_id'],$project_name,$module_name);
					$data['comments_pmpo'] = $this->common->getPmpoComments($data['session_partner_id'],$month_from,$project_name,$module_name);
				}
						$partner_info = $this->Mpr_model->getPartnerState($partner_id);
                        $state_name=$partner_info->ss_states_name;	
			    	     $combine_day = $month_from;
			    		 //$combine_day_last = "01-".$this->input->post('month_from_last1');  
				    $month_data = date("Y-m-d",strtotime($combine_day));  
					$month_data_last = ""; 
				 	$year = date("Y",strtotime($combine_day)); 
					//$project_name = "rural eye health";
                    ///////////////////////////////////////////////////					
			      $data['partner_summary_data']=$this->Mpr_model->partner_summary_fetch_redirect($partner_id,$month_data,$project_name);
				  if(!empty($data['partner_summary_data']) and $partner_id!="")
				   {				   
					  $abc = $this->Mpr_model->getAllMetricsData($project_name,$module_name,$year,1);
					  //echo "<pre>"; print_r($abc); die;
					  $module_name_yearly='yearly target';
					  $xyz = $this->Mpr_model->getAllMetricsData($project_name,$module_name_yearly,$year,2);
					  //echo "<pre>"; print_r($xyz); die;
					  $data['merge'] = $abc;
					  $data['merge2'] = $xyz;
					  $data['month_id'] = array($month_data,$partner_id);
					  $data['include'] = "mpr/mpr_reh_redirect";
					  $this->load->view('container_login', $data);
				   }
				   else
				   {
					
				   }
		       }
			public function mpr_partner_reh_redirect()
			{         
	    //var_dump($this->input->post()); die;
		   date_default_timezone_set('Asia/Kolkata');
		      $data['state_fetch']=$this->Mpr_model->state_fetch();
		      //$data['partner_summary_data']= "";				
		       $month_from = base64_decode($this->uri->segment(2)); 
				//var_dump($month_from); exit;
			   $data['post_month_from'] = $month_from; 
			   
			   $module_name='main entry';
						if($this->uri->segment(1) == "mpr_partner_reh_redirect")
						{
							$project_name = "rural eye health";
						}
						else if($this->uri->segment(1) == "mpr_partner_ueh_redirect")
						{
							$project_name = "urban eye health";
						}
						else if($this->uri->segment(1) == "mpr_partner_social_redirect")
						{
							$project_name = "Social inclusion";
						}
						else if($this->uri->segment(1) == "mpr_partner_education_redirect")
						{
							$project_name = "Inclusive education";
						}
						
				$partner_id = base64_decode($this->uri->segment(3));
				
				if($this->session->userdata('userinfo')['default_role'] == 6)//if partner
				{
					
					$data['session_partner_id'] = $partner_id;
					$check_mpr_summ_exist = $this->Reh_main_entry_model->checkMprExistArr($data['session_partner_id'],$project_name,$module_name,$month_from);//check mpr exist	
					
					if($check_mpr_summ_exist['status'] == 1 || $check_mpr_summ_exist['status'] == 4)
					{
						
						$data['mpr_id'] = $check_mpr_summ_exist['ss_mpr_summary_id'];
						$data['mpr_report_month'] = $check_mpr_summ_exist['ss_mpr_report_month'];
						//echo "<pre>"; print_r($data['mpr_id']); exit;
						
					}else
					{
						$data['mpr_id'] = "";
						$data['mpr_report_month'] = "";
					}
				}
				else
				{
					$data['session_partner_id'] = $partner_id;				
					$data['partners'] = $this->common->getPartners();
					$user_id = $this->session->userdata('userinfo')['user_id'];
					$data['comments'] = $this->common->getComments($user_id,$month_from,$data['session_partner_id'],$project_name,$module_name);
					$data['comments_pmpo'] = $this->common->getPmpoComments($data['session_partner_id'],$month_from,$project_name,$module_name);
				}
						$partner_info = $this->Mpr_model->getPartnerState($partner_id);
						//print_r($partner_info); exit;
                        $state_name=$partner_info->ss_states_name;	
			    	     $combine_day = $month_from;
						
			    		 //$combine_day_last = "01-".$this->input->post('month_from_last1');  
				    $month_data = date("Y-m-d",strtotime($combine_day));  
					$month_data_last = ""; 
				 	$year = date("Y",strtotime($combine_day)); 
					//$project_name = "rural eye health";
                    ///////////////////////////////////////////////////					
			      $data['partner_summary_data']=$this->Mpr_model->partner_summary_fetch_redirect($partner_id,$month_data,$project_name);
					
				  if(!empty($data['partner_summary_data']) and $partner_id!="")
				   {
					  $userid=$this->session->userdata('userinfo')['user_id'];
					  $abc = $this->Mpr_model->getAllMetricsData($project_name,$module_name,$year,1);
					  //echo "<pre>"; print_r($abc); die;
					  $module_name_yearly='yearly target';
					  $xyz = $this->Mpr_model->getAllMetricsData($project_name,$module_name_yearly,$year,2);
					  //echo "<pre>"; print_r($xyz); die;
					  $data['merge'] = $abc;
					  $data['merge2'] = $xyz;
					  $data['month_id'] = array($month_data,$partner_id, $check_mpr_summ_exist['status']);
					//echo "<pre>"; print_R(); die; 
					  $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
					  
					  $data['sections'] = $this->Reh_main_entry_model->getAllSections($project_name, $module_name,$year);
					  
					  if($this->session->userdata('userinfo')['state_name']=='West Bengal Sunderbans')
					  {
					  $data['metrics_data'] = $this->Reh_main_entry_model->getAllMetricData($project_name,$module_name,$year);
					  }
					  else 
					  {
					  $data['metrics_data'] = $this->Reh_main_entry_model->getAllMetricDataSunder($project_name,$module_name,$year);
					  } 
					  
			    $data['post_month_from'] = $month_data ; 
				$data['session_partner_id'] = $_SESSION["partner_id"];
				$check_mpr_summ_exist = $this->Reh_main_entry_model->checkMprExistArr($data['session_partner_id'],$project_name,$module_name,$data['post_month_from']);
				//echo "<pre>"; print_R($check_mpr_summ_exist); die;
				//check mpr exist				
				$data['status'] = $check_mpr_summ_exist['status'];
				
				$data['form_params'] = base64_encode($project_name.'-'.$module_name);
					  
					  //echo "<pre>"; print_r($data); exit;
					  $data['include'] = "mpr/mpr_partner_reh_redirect";
					  $this->load->view('container_login_dis', $data);
				   }
				   else
				   {
					
				   }
		       }
	
	       public function mpr_ueh()
	         { 
		     $data['state_fetch']=$this->Mpr_model->state_fetch();
			 
					 date_default_timezone_set('Asia/Kolkata');
					 $state_name=$this->input->post('state_name');
			 		 $partner_id=$this->input->post('partner_name_post');  
					 $combine_day = "01-".$this->input->post('month_from1');
					 $combine_day_last = "01-".$this->input->post('month_from_last1');  
					 
				    $month_data = date("Y-m-d",strtotime($combine_day));  
					$month_data_last = date("Y-m-d",strtotime($combine_day_last)); 
				 	$year = date("Y",strtotime($combine_day));
					$project_name = "urban eye health";
					$data['partner_summary_data']=$this->Mpr_model->partner_summary_fatch_ueh($partner_id,$month_data);
					//var_dump($data['partner_summary_data']); //die;
				   if(!empty($data['partner_summary_data']))
				   {  // echo "hello"; die;
					  $module_name='main entry'; 
					  $abc = $this->Mpr_model->getAllMetricsData($project_name,$module_name,$year,1);
					  $module_name_yearly='yearly target';
					  $xyz = $this->Mpr_model->getAllMetricsData($project_name,$module_name_yearly,$year,2);
					  $data['merge'] = $abc;
					  $data['merge2'] = $xyz;
					  $data['month_id'] = array($state_name,$month_data,$month_data_last,$partner_id);
					  //echo "<pre>"; print_R($data['month_id']); die;
				   }
                   else
				   {				   
					  $module_name='main entry'; 
					  $abc = $this->Mpr_model->getAllMetricsData($project_name,$module_name,$year,1);
					  $module_name_yearly='yearly target';
					  $xyz = $this->Mpr_model->getAllMetricsData($project_name,$module_name_yearly,$year,2);
					  $data['merge'] = $abc;
					  $data['merge2'] = $xyz;
					  $data['month_id'] = array($state_name,$month_data,$month_data_last,$partner_id);
				   }
		$data['include'] = "mpr/mpr_ueh";
		$this->load->view('container_login', $data);
	}
	
	public function social_inclusion() 
	{
		$data['state_fetch']=$this->Mpr_model->state_fetch();
		
		      $data['partner_summary_data']="";
			  if($this->input->server('REQUEST_METHOD') === 'POST')
		        { 
			             date_default_timezone_set('Asia/Kolkata');
	    		    	 $state_name=$this->input->post('state_name');
			    	 	 $partner_id=$this->input->post('partner_name_post');  
			    	     $combine_day = "01-".$this->input->post('month_from1');
			    		 $combine_day_last = "01-".$this->input->post('month_from_last1');  
					  
				    $month_data = date("Y-m-d",strtotime($combine_day));  
					$month_data_last = date("Y-m-d",strtotime($combine_day_last)); 
				 	$year = date("Y",strtotime($combine_day)); 
					$project_name = "Social inclusion";
					
			      $data['partner_summary_data']=$this->Mpr_model->partner_summary_fatch($partner_id,$month_data);	
                 // echo "<pre>"; print_r($data['partner_summary_data']); die;				  
				  if(!empty($data['partner_summary_data']))
				   {
				  $module_name='main entry'; 
				  $abc = $this->Mpr_model->getAllMetricsData($project_name,$module_name,$year,1);
				  // echo "<pre>";print_R($abc); die; 
				  $module_name_yearly='yearly target'; 
				  $xyz = $this->Mpr_model->getAllMetricsData($project_name,$module_name_yearly,$year,2);
				//  echo "<pre>";print_R($xyz); die; 
				  $data['merge'] = $abc;
				  $data['merge2'] = $xyz;
				  $data['month_id'] = array($state_name,$month_data,$month_data_last,$partner_id);
				   }
				   else
				   { 
				  $module_name='main entry'; 
				  $abc = $this->Mpr_model->getAllMetricsData($project_name,$module_name,$year,1);
				  $module_name_yearly='yearly target';
				  $xyz = $this->Mpr_model->getAllMetricsData($project_name,$module_name_yearly,$year,2);
				  $data['merge'] = $abc;
				  $data['merge2'] = $xyz;
				  $data['month_id'] = array($state_name,$month_data,$month_data_last,$partner_id);
				   }
				}				

		$data['include'] = "mpr/mpr_si";
		$this->load->view('container_login', $data);
    }
	
  public function inclusive_edu() 
	{
		$data['state_fetch']=$this->Mpr_model->state_fetch();
		      $data['partner_summary_data']="";
			  if($this->input->server('REQUEST_METHOD') === 'POST')
		        { 
			             date_default_timezone_set('Asia/Kolkata');
	    		    	 $state_name=$this->input->post('state_name');
			    	 	 $partner_id=$this->input->post('partner_name_post');  
			    	     $combine_day = "01-".$this->input->post('month_from1');
			    		 $combine_day_last = "01-".$this->input->post('month_from_last1');  
					  
				    $month_data = date("Y-m-d",strtotime($combine_day));  
					$month_data_last = date("Y-m-d",strtotime($combine_day_last)); 
				 	$year = date("Y",strtotime($combine_day)); 
					$project_name = "Inclusive education";
					
			      $data['partner_summary_data']=$this->Mpr_model->partner_summary_fatch($partner_id,$month_data);				  
				  if(!empty($data['partner_summary_data']))
				   {
				  $module_name='main entry'; 
				  $abc = $this->Mpr_model->getAllMetricsData($project_name,$module_name,$year,1);
				  $module_name_yearly='yearly target'; 
				  $xyz = $this->Mpr_model->getAllMetricsData($project_name,$module_name_yearly,$year,2);
				  $data['merge'] = $abc;
				  $data['merge2'] = $xyz;
				  $data['month_id'] = array($state_name,$month_data,$month_data_last,$partner_id);
				   }
				   else
				   { 
				  $module_name='main entry'; 
				  $abc = $this->Mpr_model->getAllMetricsData($project_name,$module_name,$year,1);
				  $module_name_yearly='yearly target';
				  $xyz = $this->Mpr_model->getAllMetricsData($project_name,$module_name_yearly,$year,2);
				  $data['merge'] = $abc;
				  $data['merge2'] = $xyz;
				  $data['month_id'] = array($state_name,$month_data,$month_data_last,$partner_id);
				   }
				}
		$data['include'] = "mpr/mpr_ie";
		$this->load->view('container_login', $data);
    }	

}
