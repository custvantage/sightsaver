<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Reh_advocacy extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Reh_advocacy_model');
		$this->load->model('Reh_main_entry_model');
		check_login();
    }

    public function index() {
		$data['advocacy_data'] = "";
		$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		$data['include'] = "reh/advocacy";
		$this->load->view('container_login_dis', $data);
    }
	public function create()
	{
		$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
		
			$this->form_validation->set_rules('month_data','Month','required');
					
			if($this->form_validation->run()==true)
			{
				$partner_id = $_SESSION["partner_id"];
				$created_on = server_date_time();
				$ajax_id1 = $this->input->post('id_ajax');
				$block_name = $this->input->post('advocacy_block');
				$event_type = $this->input->post('advocacy_event_type');
				$event_detail = $this->input->post('advocacy_event_detail');
				$meeting_purpose = $this->input->post('advocacy_meeting_purpose');
				$advocacy_prioritise = $this->input->post('advocacy_prioritise');
				$advocacy_inclusion_eh = $this->input->post('advocacy_inclusion_eh');			
				$advocacy_meeting_level = $this->input->post('advocacy_meeting_level');
				$advocacy_meeting_organised = $this->input->post('advocacy_meeting_organised');
				$advocacy_meeting_organised_detail = $this->input->post('advocacy_meeting_organised_detail');
				$advocacy_meeting_with = $this->input->post('advocacy_meeting_with');
				$advocacy_issue_discuss = $this->input->post('advocacy_issue_discuss');				
				$combine_day = "01-".$this->input->post('month_data');
				$data_month = date("Y-m-d",strtotime($combine_day));
				
				if($this->input->post('id_ajax')=='')
				{
				$advocacy_data = array('ss_partner_id'=>$partner_id,'ss_reh_advocacy_block'=>$block_name,'ss_reh_advocacy_event_type'=>$event_type,'ss_reh_advocacy_event_detail'=>$event_detail,'ss_reh_advocacy_meeting_purpose'=>$meeting_purpose,'ss_reh_advocacy_priorities'=>$advocacy_prioritise,'ss_reh_advocacy_inclusion_eye_health'=>$advocacy_inclusion_eh,'ss_reh_advocacy_meeting_level'=>$advocacy_meeting_level,'ss_reh_advocacy_meeting_organised'=>$advocacy_meeting_organised,'ss_reh_advocacy_meeting_organised_detail'=>$advocacy_meeting_organised_detail,'ss_reh_advocacy_meeting_with'=>$advocacy_meeting_with,'ss_reh_advocacy_issue'=>$advocacy_issue_discuss,'ss_reh_advocacy_month'=>$data_month,'ss_reh_advocacy_created_on'=>$created_on);
				
				$creation_check = $this->Reh_advocacy_model->createAdvocacy($advocacy_data);
				}
				else
				{
					$update_advocacy_data = array('ss_partner_id'=>$partner_id,'ss_reh_advocacy_block'=>$block_name,'ss_reh_advocacy_event_type'=>$event_type,'ss_reh_advocacy_event_detail'=>$event_detail,'ss_reh_advocacy_meeting_purpose'=>$meeting_purpose,'ss_reh_advocacy_priorities'=>$advocacy_prioritise,'ss_reh_advocacy_inclusion_eye_health'=>$advocacy_inclusion_eh,'ss_reh_advocacy_meeting_level'=>$advocacy_meeting_level,'ss_reh_advocacy_meeting_organised'=>$advocacy_meeting_organised,'ss_reh_advocacy_meeting_organised_detail'=>$advocacy_meeting_organised_detail,'ss_reh_advocacy_meeting_with'=>$advocacy_meeting_with,'ss_reh_advocacy_issue'=>$advocacy_issue_discuss,'ss_reh_advocacy_month'=>$data_month,'ss_reh_advocacy_created_on'=>$created_on);
				    $creation_check = $this->Reh_advocacy_model->updateAdvocacy($update_advocacy_data,$ajax_id1);
				}
				$this->session->set_flashdata('success','Your data is successfully saved');
                redirect('filter_advocacy_reh?month_from='.$_SESSION["month_date"]);
			}else{
				$data['include'] = "reh/advocacy";
			$this->load->view('container_login_dis', $data);
			}
		}
	}
	 public function filter_advocacy_data()
	 {     
	 $userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		if($this->input->server('REQUEST_METHOD') === 'GET' and $this->input->get('month_from')!=null and preg_match('/^[0-9]{2}-[0-9]{4}\z/', $this->input->get('month_from')))
		{
			
			//$this->form_validation->set_rules('month_from','Month','required');
					
			//if($this->form_validation->run()==true)
			//{
				$combine_day = "01-".$this->input->get('month_from');
				$month_from = date("Y-m-d",strtotime($combine_day));
				
				
				$partner_id = $_SESSION["partner_id"];
				$data['advocacy_data'] = $this->Reh_advocacy_model->getAdvocacy($month_from,$partner_id);
				//print_r($data['advocacy_data']); exit;
			//}
			
			$data['include'] = "reh/advocacy";
			$this->load->view('container_login_dis', $data);
		}
		else{
			redirect('reh_advocacy');
		}
	}
	
  public function delete_advocacy_reh()
	{ 
		if(!empty($this->uri->segment(2)))
        {
	     $id = base64_decode($this->uri->segment(2));
		 $this->Reh_advocacy_model->deleteHehAdvocacy($id);
	     redirect($_SERVER['HTTP_REFERER']);
		}
	}
	
	public function edit_advocacy_reh()
	{  
	     $id = base64_decode($this->input->post('id'));
		 $data=$this->Reh_advocacy_model->editFetchData($id);
		 if(!empty($data))
		 {
			 $data2 = json_encode($data[0]);
			 echo $data2."$$$".$this->security->get_csrf_hash();
		 }
		 else
		 {
			 echo 0;
		 }
	}
	
	
	
	
	
  
}
