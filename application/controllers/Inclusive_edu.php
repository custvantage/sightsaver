<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Inclusive_edu extends CI_Controller {

    public function __construct() {
        parent::__construct();
		check_login();	
		$this->load->model('Reh_main_entry_model');
        $this->load->model('Education_main_entry_model');
		$this->load->library('metricdata');
		$this->load->library('common');
		
    }
	 
	public function index() {
		//echo "hello"; die;
        if(!empty($this->uri->segment(2)))
        {						
            $getdata = base64_decode($this->uri->segment(2));
            $explodedata = explode('-',$getdata);
            $project_name = $explodedata[0];
            $module_name = $explodedata[1];
			$created_on = server_date_time();
			$current_year = date('Y',strtotime($created_on));		
            
            $data['sections'] = $this->Education_main_entry_model->getAllSections($project_name, $module_name,$current_year); 
           // var_dump($data['sections']); die;			
            
            $data['metrics_data'] = $this->Education_main_entry_model->getAllMetricData($project_name,$module_name,$current_year);
          //  print_R($data['metrics_data']); die;
			if($this->session->userdata('userinfo')['default_role'] != 6)//if pm/po
			{
				$data['partners'] = $this->common->getPartners();
			}
            $data['include'] = "inclusive_edu/ie_main_entry";
		    $this->load->view('container_login2', $data);
			
        }else
        {
            redirect('dashboard');
        }
    }
	
	
	
    public function main_entry_edu() 
	{
		//echo "hello"; die;
        if($this->input->server('REQUEST_METHOD') === 'POST')
		{	
			$getdata = base64_decode($this->uri->segment(2));
			$explodedata = explode('-',$getdata);
			$project_name = $explodedata[0];
			$module_name = $explodedata[1];
			
			$combine_day = "01-".$this->input->post('month_data');
			$data_month = date("Y-m-d",strtotime($combine_day));
			
		    $year = date('Y',strtotime($combine_day)); 
			
			$this->form_validation->set_rules('month_data','Month','required');
					
			if($this->form_validation->run()==true)
			{
				
				$created_on = server_date_time();
				$partner_id = $this->session->userdata['userinfo']['partner_id'];				
				
								
				//One column
				if($this->input->post('matric_id_one_column'))
				{
					$one_column_metric_ids = $this->input->post('matric_id_one_column');//post one column metric id
					$post_one_column_data = $this->input->post('one_column_val');//post one column value
					
					foreach($one_column_metric_ids as $key_metrics_one_column=>$one_column_metric_id)
					{
						$one_column_data[] = array('ss_metric_master_id'=>$one_column_metric_id,'ss_partner_id'=>$partner_id,'ss_one_column_data_value'=>$post_one_column_data[$key_metrics_one_column],'ss_one_column_data_month'=>$data_month,'ss_one_column_data_created_on'=>$created_on);
					}
				//echo "<pre>"; print_R($one_column_data); 
					$this->Education_main_entry_model->create_one_column($one_column_data);//One column data insert
					
				}
				
				//Four Column
				if($this->input->post('matric_id_four_column'))
				{
					$four_column_metric_ids = $this->input->post('matric_id_four_column');//post four column metric id
					$post_four_column_men_data = $this->input->post('four_column_men_val');//post four column men value
					$post_four_column_women_data = $this->input->post('four_column_women_val');//post four column women value
					$post_four_column_trans_data = $this->input->post('four_column_trans_val');//post four column transgender value
					$post_four_column_boy_data = $this->input->post('four_column_boy_val');//post four column boy value
					$post_four_column_girl_data = $this->input->post('four_column_girl_val');//post four column girl value
					
					foreach($four_column_metric_ids as $key_four_column=>$four_column_metric_id)
					{
						$four_column_data[] = array('ss_metric_master_id'=>$four_column_metric_id,'ss_partner_id'=>$partner_id,'ss_four_column_data_value_men'=>$post_four_column_men_data[$key_four_column],'ss_four_column_data_value_women'=>$post_four_column_women_data[$key_four_column],'ss_four_column_data_value_trans'=>$post_four_column_trans_data[$key_four_column],'ss_four_column_data_value_boys'=>$post_four_column_boy_data[$key_four_column],'ss_four_column_data_value_girls'=>$post_four_column_girl_data[$key_four_column],'ss_four_column_data_month'=>$data_month,'ss_four_column_data_created_on'=>$created_on);
					}//echo "<pre>"; print_R($four_column_data); 
					$this->Education_main_entry_model->create_four_column($four_column_data);//Four column data insert
				}
				
				//Eight column data
				if($this->input->post('matric_id_eight_column'))
				{
				$matric_id_eight_ids = $this->input->post('matric_id_eight_column');//post analysis metric id
				$eight_column_ele_sanctioned = $this->input->post('eight_column_ele_sanctioned');//post sanctioned
				$eight_column_ele_male = $this->input->post('eight_column_ele_male');//post male
				$eight_column_ele_female = $this->input->post('eight_column_ele_female');//post female
				$eight_column_ele_vacant = $this->input->post('eight_column_ele_vacant');//post vacant 
				$eight_column_sec_sanctioned = $this->input->post('eight_column_sec_sanctioned');//post sanctioned
				$eight_column_sec_male = $this->input->post('eight_column_sec_male');//post sanctioned
				$eight_column_sec_female = $this->input->post('eight_column_sec_female');//post sanctioned
				$eight_column_sec_vacant = $this->input->post('eight_column_sec_vacant');//post sanctioned
					
					foreach($matric_id_eight_ids as $key_eight=>$eight_column_ids)
					{
						$eight_column_data[] = array('ss_metric_master_id'=>$eight_column_ids,'ss_partner_id'=>$partner_id,'ss_elementory_sanctioned'=>$eight_column_ele_sanctioned[$key_eight],'ss_elementory_boys'=>$eight_column_ele_male[$key_eight],'ss_elementory_girls'=>$eight_column_ele_female[$key_eight],'ss_elementory_vacant'=>$eight_column_ele_vacant[$key_eight],'ss_secondary_sanctioned'=>$eight_column_sec_sanctioned[$key_eight],'ss_secondary_boys'=>$eight_column_sec_male[$key_eight],
						'ss_secondary_girls'=>$eight_column_sec_female[$key_eight],'ss_secondary_vacant'=>$eight_column_sec_vacant[$key_eight],'ss_data_month'=>$data_month,'ss_created_on'=>$created_on);
					} //echo "<pre>"; print_R($eight_column_data); 
					$this->Education_main_entry_model->create_eight_column($eight_column_data);//Eight time data 
				}
				
				//Mpr summary
				$partner_info = $this->Education_main_entry_model->getPartnerInfo($partner_id);//get partner info
				$check_mpr_summ_exist = $this->Education_main_entry_model->checkMprExist($partner_id,$project_name,$module_name,$data_month);//check mpr exist
				if(empty($check_mpr_summ_exist))
				{
					$mpr_summ_data = array('ss_mpr_summary_partner_id'=>$partner_id,'ss_mpr_project_name'=>$project_name,'ss_mpr_module_name'=>$module_name,'ss_mpr_start_date'=>$partner_info->ss_partners_created_on,'status'=>1,'ss_mpr_report_month'=>$data_month,'ss_mpr_modified_on'=>$created_on,'ss_mpr_created_on'=>$created_on);
					
					//create mpr summary
					$this->Education_main_entry_model->createMprSummary($mpr_summ_data);
				}else{
					$mpr_summ_update_data = array('ss_mpr_modified_on'=>$created_on);
					$mpr_summ_update_where_data = array('ss_mpr_summary_id'=>$check_mpr_summ_exist->ss_mpr_summary_id);
					//update mpr summary
					
					$this->Education_main_entry_model->updateMprSummary($mpr_summ_update_data,$mpr_summ_update_where_data);
				}
			}
			$data['sections'] = $this->Education_main_entry_model->getAllSections($project_name, $module_name,$year);
            
            $data['metrics_data'] = $this->Education_main_entry_model->getAllMetricData($project_name,$module_name, $year); $this->session->set_flashdata('success', 'Data save successfully');          
			$data['include'] = "inclusive_edu/ie_main_entry";
			 $this->load->view('container_login2', $data);
			
		}else{
			 redirect('ie_main_entry/'.$this->uri->segment(2));
		}
	}
	
	
public function manage()
	{		
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$getdata = base64_decode($this->uri->segment(2));
			$explodedata = explode('-',$getdata);
			$project_name = $explodedata[0];
			$module_name = $explodedata[1];
			$combine_day = "01-".$this->input->post('month_from');	
            $data['data1']=	$this->input->post('month_from'); 			
			$data['post_month_from'] = date("Y-m-d",strtotime($combine_day));
		    $year = date('Y',strtotime($combine_day)); 
			$data['mpr_report_month'] = $data['post_month_from'];			
			if($this->session->userdata('userinfo')['default_role'] == 6)//if partner
			{
				$data['session_partner_id'] = $this->session->userdata['userinfo']['partner_id'];
				$check_mpr_summ_exist = $this->Education_main_entry_model->checkMprExistArr($data['session_partner_id'],$project_name,$module_name,$data['post_month_from']);//check mpr exist	
                $data['status'] = $check_mpr_summ_exist['status'];						
				if($check_mpr_summ_exist['status'] == 1 || $check_mpr_summ_exist['status'] == 4)
				{
					$data['mpr_id'] = $check_mpr_summ_exist['ss_mpr_summary_id'];
					//$data['mpr_report_month'] = $check_mpr_summ_exist['ss_mpr_report_month'];
				}else{
					$data['mpr_id'] = "";
					//$data['mpr_report_month'] = "";
				}
			}else{
				$data['session_partner_id'] = $this->input->post('partner_name');				
				$data['partners'] = $this->common->getPartners();
				$user_id = $this->session->userdata('userinfo')['user_id'];
				$data['comments'] = $this->common->getComments($user_id,$data['post_month_from'],$data['session_partner_id'],$project_name,$module_name);
				$data['comments_pmpo'] = $this->common->getPmpoComments($data['session_partner_id'],$data['post_month_from'],$project_name,$module_name);
			}			
			$data['sections'] = $this->Education_main_entry_model->getAllSections($project_name, $module_name,$year);
		
			$data['metrics_data'] = $this->Education_main_entry_model->getAllMetricData($project_name,$module_name, $year);			
			
			$data['include'] = "inclusive_edu/manage_education_main_entry";
			$this->load->view('container_login2', $data);
		}
		else
		{
			$this->session->set_flashdata('success', 'Data save successfully');
			redirect('ie_main_entry/'.$this->uri->segment(2));
		}
	}
public function createpmpo_comment()
	{
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$this->form_validation->set_rules('comments','Comment','required');
			$this->form_validation->set_rules('partner_id','Partner Id','required');
			$data['csrfHash'] = $this->security->get_csrf_hash();
			if($this->form_validation->run()==true)
			{
				
				$comments = $this->input->post('comments');
				$partner_id = $this->input->post('partner_id');
				$month_from = $this->input->post('month_from');
				$user_id = $this->session->userdata('userinfo')['user_id'];
				$project_name = $this->input->post('project_name');
				$module_name = $this->input->post('module_name');
				$created_on = server_date_time();
				$status = 2;
				$check_mpr_summ_exist = $this->common->checkMprSubmit($partner_id,$project_name,$module_name,$month_from,$status);//check mpr exist				
				if(!empty($check_mpr_summ_exist))
				{				
					$comment_data = array('ss_user_id'=>$user_id,'ss_partner'=>$partner_id,'ss_month'=>$month_from,'project_name'=>$project_name,'module_name'=>$module_name,'comment'=>$comments,'created_on'=>$created_on);
					$this->common->createComments($comment_data);
					$data['partner_name'] = $this->common->partnerInfo($partner_id);
					$data['comments'] = $comments;
					$data['created_on'] = date('F j, Y',strtotime($created_on));
					$data['created_time'] = date('h:i A',strtotime($created_on));
					$data['success'] = 1;
				}else{
					$data['success'] = 0;
				}
				echo json_encode($data);
				
			}
		}
	}
	public function approve_reject()
	{
		//var_dump($this->input->post()); die;
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$this->form_validation->set_rules('month_from_approve','Month','required');
			$this->form_validation->set_rules('partner_id_approve','Partner Id','required');					
			if($this->form_validation->run()==true)
			{
				$partner_id = $this->input->post('partner_id_approve');
				$data_month = $this->input->post('month_from_approve');
				$project_name = $this->input->post('project_name');
				$module_name = $this->input->post('module_name');
				$reason = $this->input->post('reason');
				$approve_flag = $this->input->post('approve_flag');
				if($approve_flag == 1 && ($this->session->userdata('userinfo')['default_role']==4))
				{
					$status = 3;
					$check_status = 2;
				}
				if($approve_flag == 1 && ($this->session->userdata('userinfo')['default_role']==1))
				{
					$status = 5;
					$check_status = 3;
				}
				if($approve_flag == 0)
				{
					$status = 4;
				}
				$created_on = server_date_time();
					
				$check_mpr_summ_exist = $this->common->checkMprSubmit($partner_id,$project_name,$module_name,$data_month,$check_status);//check mpr exist
				//var_dump($check_mpr_summ_exist); 
				if(!empty($check_mpr_summ_exist))
				{					
					$mpr_summ_update_data = array('ss_mpr_modified_on'=>$created_on,'status'=>$status,'reason'=>$reason);
					$mpr_summ_update_where_data = array('ss_mpr_summary_id'=>$check_mpr_summ_exist->ss_mpr_summary_id);
					//update mpr summary
					
					$this->Reh_main_entry_model->updateMprSummary($mpr_summ_update_data,$mpr_summ_update_where_data);					
					$data['success'] = 1;				
				}else{
					$data['success'] = 0;
				}
				$data['csrfHash'] = $this->security->get_csrf_hash();
				echo json_encode($data);
			}
		}
	}
	
	public function submit_form_data_edu()
	{  
       if($this->input->server('REQUEST_METHOD') === 'POST')
		  { 
	       $partner_id=$this->session->userdata('userinfo')['partner_id'];
			$this->form_validation->set_rules('mpr_summary_id','Mpr Summary','required');			
			if($this->form_validation->run()==true)
			{
				$this->load->library('user_agent');				
				$created_on = server_date_time();
				 $mpr_id = $this->input->post('mpr_summary_id'); 
				 $month = $this->input->post('current_year');
			      $current_year = $this->input->post('current_year'); 
				  $current_year = date('Y',strtotime($current_year));
                 if(!empty($this->uri->segment(2)))
				   {						
					$getdata = base64_decode($this->uri->segment(2));
					$explodedata = explode('-',$getdata);
					$project_name = $explodedata[0];
					$module_name = $explodedata[1];
					 
					 //echo $partner_id; die;
		   $data['metrics_data_one'] = $this->Reh_main_entry_model->getAllMetricDataMprOne($project_name,$module_name,$current_year,$month);
		   //	echo "<pre>";	print_r($data['metrics_data_one']);  die;
				   $mpr_summ_update_data = array('ss_mpr_modified_on'=>$created_on,'status'=>2);
				   $mpr_summ_update_where_data = array('ss_mpr_summary_id'=>$mpr_id);
					 
					 foreach($data['metrics_data_one'] as $data_four)
					 {  
					  $four_column_metric_id=$data_four->master_matric_id;
				    $master_matric_depends_id=$data_four->master_matric_depends_id; 
					$data_matric_id=$data_four->data_matric_id;
					$ss_one_column_data_value=$data_four->ss_one_column_data_value;
					
					
					  if($data_four->ss_metric_master_active=='0' and $master_matric_depends_id!="")
					  { 
						     $data_four->ss_metric_master_active; 
			                 $data['four_column_metric_id_depend'] = $this->Reh_main_entry_model->getAllMetricDataMprOneDepends($month,$master_matric_depends_id); 
							 //echo "<pre>"; print_R($data['four_column_metric_id_depend']); die;

						if($master_matric_depends_id!='')	
						{			
                             $ss_one_column_data_value=$data['four_column_metric_id_depend'][0]->ss_one_column_data_value;
							 					
							$one_column_data[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
							'ss_mpr_partner_id'=>$partner_id,
							'ss_mpr_one_column_data_value'=>$ss_one_column_data_value,
							'ss_mpr_one_column_data_month'=>$month,
							'ss_mpr_one_column_data_created_on'=>$created_on);
						}
                            else
						{
						$one_column_data[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
						'ss_mpr_partner_id'=>$partner_id,
						'ss_mpr_one_column_data_value'=>'',
						'ss_mpr_one_column_data_month'=>$month,
						'ss_mpr_one_column_data_created_on'=>$created_on);
						}						
						//echo "<pre>"; print_R($four_column_data); die;
					}  
				     else 
					 {
						$ss_one_column_data_value=$data_four->ss_one_column_data_value;
	 
						$one_column_data[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
						'ss_mpr_partner_id'=>$partner_id,
						'ss_mpr_one_column_data_value'=>$ss_one_column_data_value,
						'ss_mpr_one_column_data_month'=>$month,
						'ss_mpr_one_column_data_created_on'=>$created_on);
					}  
				   }  
				  $this->Reh_main_entry_model->create_one_column_mpr($one_column_data);   //Four column data insert
				  //Four Column 
				   $data['metrics_data_four'] = $this->Reh_main_entry_model->getAllMetricDataMprFour($project_name,$module_name,$current_year,$month);
				  // echo "<pre>";print_R($data['metrics_data_four']); die;
				   foreach($data['metrics_data_four'] as $data_four)
					{   
				 	$four_column_metric_id=$data_four->master_matric_id;
				    $master_matric_depends_id=$data_four->master_matric_depends_id; 
					$data_matric_id=$data_four->data_matric_id;
					$ss_four_column_data_value_men=$data_four->ss_four_column_data_value_men;
					$ss_four_column_data_value_men=$data_four->ss_four_column_data_value_women;
					$ss_four_column_data_value_men=$data_four->ss_four_column_data_value_boys;
					$ss_four_column_data_value_men=$data_four->ss_four_column_data_value_girls;
					
					 if($data_four->ss_metric_master_active=='0' and $master_matric_depends_id!="")
					  { 
						     $data_four->ss_metric_master_active; 
			                 $data['four_column_metric_id_depend'] = $this->Reh_main_entry_model->getAllMetricDataMprFourDepends($month,$master_matric_depends_id); 
							 //echo "<pre>"; print_R($data['four_column_metric_id_depend']); die;

						if($master_matric_depends_id!='')	
						{			
                             $ss_four_column_data_value_men=$data['four_column_metric_id_depend'][0]->ss_four_column_data_value_men;
							 $ss_four_column_data_value_women=$data['four_column_metric_id_depend'][0]->ss_four_column_data_value_women;
							 $ss_four_column_data_value_boys=$data['four_column_metric_id_depend'][0]->ss_four_column_data_value_boys;
							 $ss_four_column_data_value_girls=$data['four_column_metric_id_depend'][0]->ss_four_column_data_value_girls;					
							$four_column_data[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
							'ss_mpr_partner_id'=>$partner_id,
							'ss_mpr_four_column_data_value_men'=>$ss_four_column_data_value_men,
							'ss_mpr_four_column_data_value_women'=>$ss_four_column_data_value_women,
							'ss_mpr_four_column_data_value_boys'=>$ss_four_column_data_value_boys,
							'ss_mpr_four_column_data_value_girls'=>$ss_four_column_data_value_girls,
							'ss_mpr_four_column_data_month'=>$month,
							'ss_mpr_four_column_data_created_on'=>$created_on);
						}
                            else
						{
						$four_column_data[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
						'ss_mpr_partner_id'=>$partner_id,
						'ss_mpr_four_column_data_value_men'=>'',
						'ss_mpr_four_column_data_value_women'=>'',
						'ss_mpr_four_column_data_value_boys'=>'',
						'ss_mpr_four_column_data_value_girls'=>'',
						'ss_mpr_four_column_data_month'=>$month,
						'ss_mpr_four_column_data_created_on'=>$created_on);
						}						
						//echo "<pre>"; print_R($four_column_data); die;
					}  
				     else 
					 {
						$ss_four_column_data_value_men=$data_four->ss_four_column_data_value_men;
						$ss_four_column_data_value_women=$data_four->ss_four_column_data_value_women;
						$ss_four_column_data_value_boys=$data_four->ss_four_column_data_value_boys;
						$ss_four_column_data_value_girls=$data_four->ss_four_column_data_value_girls;
							 
						$four_column_data[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
						'ss_mpr_partner_id'=>$partner_id,
						'ss_mpr_four_column_data_value_men'=>$ss_four_column_data_value_men,
						'ss_mpr_four_column_data_value_women'=>$ss_four_column_data_value_women,
						'ss_mpr_four_column_data_value_boys'=>$ss_four_column_data_value_boys,
						'ss_mpr_four_column_data_value_girls'=>$ss_four_column_data_value_girls,
						'ss_mpr_four_column_data_month'=>$month,
						'ss_mpr_four_column_data_created_on'=>$created_on);
					}  
					//echo "<pre>"; print_r($four_column_data); die;
				}
				$this->Reh_main_entry_model->create_four_column_mpr($four_column_data);//Four column data insert
				
				//Eight column data  
				
				$data['metrics_data_eight'] = $this->Reh_main_entry_model->getAllMetricDataMprEight($project_name,$module_name,$current_year,$month);
				  // echo "<pre>";print_R($data['metrics_data_four']); die;
				   foreach($data['metrics_data_eight'] as $data_eight)
					{   
				 	$eight_column_metric_id=$data_eight->master_matric_id;
				    $master_matric_depends_id=$data_eight->master_matric_depends_id; 
					$data_matric_id=$data_eight->data_matric_id;
					$ss_elementory_sanctioned=$data_eight->ss_elementory_sanctioned;
					$ss_elementory_boys=$data_eight->ss_elementory_boys;
					$ss_elementory_girls=$data_eight->ss_elementory_girls;
					$ss_elementory_vacant=$data_eight->ss_elementory_vacant;
					$ss_secondary_sanctioned=$data_eight->ss_secondary_sanctioned;
					$ss_secondary_boys=$data_eight->ss_secondary_boys;
					$ss_secondary_girls=$data_eight->ss_secondary_girls;
					$ss_secondary_vacant=$data_eight->ss_secondary_vacant;
					
					 if($data_eight->ss_metric_master_active=='0' and $master_matric_depends_id!="")
					  { 
						     $data_eight->ss_metric_master_active;
$data['eight_column_metric_id_depend'] = $this->Reh_main_entry_model->getAllMetricDataMprFourDepends($month,$master_matric_depends_id); 
							 //echo "<pre>"; print_R($data['four_column_metric_id_depend']); die;

						if($master_matric_depends_id!='')	
						{			
                             /* $ss_elementory_sanctioned=$data['eight_column_metric_id_depend']->ss_elementory_sanctioned;
							 $ss_elementory_boys=$data['eight_column_metric_id_depend']->ss_elementory_boys;
							 $ss_elementory_girls=$data['eight_column_metric_id_depend']->ss_elementory_girls;
							 $ss_elementory_vacant=$data['eight_column_metric_id_depend']->ss_elementory_vacant;	
							 $ss_secondary_sanctioned=$data['eight_column_metric_id_depend']->ss_secondary_sanctioned;
							 $ss_secondary_boys=$data['eight_column_metric_id_depend']->ss_secondary_boys;
							 $ss_secondary_girls=$data['eight_column_metric_id_depend']->ss_secondary_girls;
							 $ss_secondary_vacant=$data['eight_column_metric_id_depend']->ss_secondary_vacant;

							$eight_column_data[] = array('ss_mpr_metric_master_id'=>$eight_column_metric_id,
							'ss_mpr_metric_master_id'=>$eight_column_metric_id,
							'ss_mpr_partner_id'=>$partner_id,
							'ss_mpr_elementory_sanctioned'=>$ss_elementory_sanctioned,
							'ss_mpr_elementory_boys'=>$ss_elementory_boys,
							'ss_mpr_elementory_girls'=>$ss_elementory_girls,
							'ss_mpr_elementory_vacant'=>$ss_elementory_vacant,
							'ss_mpr_secondary_sanctioned'=>$ss_secondary_sanctioned,
							'ss_mpr_secondary_boys'=>$ss_secondary_boys,
							'ss_mpr_secondary_boys'=>$ss_secondary_girls,
							'ss_mpr_secondary_vacant'=>$ss_secondary_vacant,
							'ss_mpr_data_month'=>$month); */
						$ss_elementory_sanctioned=$data_eight->ss_elementory_sanctioned;
						$ss_elementory_boys=$data_eight->ss_elementory_boys;
						$ss_elementory_girls=$data_eight->ss_elementory_girls;
						$ss_elementory_vacant=$data_eight->ss_elementory_vacant;
						$ss_secondary_sanctioned=$data_eight->ss_secondary_sanctioned;
						$ss_secondary_boys=$data_eight->ss_secondary_boys;
						$ss_secondary_girls=$data_eight->ss_secondary_girls;
						$ss_secondary_vacant=$data_eight->ss_secondary_vacant;
						
							 
						$eight_column_data[] = array('ss_mpr_metric_master_id'=>$eight_column_metric_id,
						'ss_mpr_metric_master_id'=>$eight_column_metric_id,
						'ss_mpr_partner_id'=>$partner_id,
						    'ss_mpr_elementory_sanctioned'=>$ss_elementory_sanctioned,
							'ss_mpr_elementory_boys'=>$ss_elementory_boys,
							'ss_mpr_elementory_girls'=>$ss_elementory_girls,
							'ss_mpr_elementory_vacant'=>$ss_elementory_vacant,
							'ss_mpr_secondary_sanctioned'=>$ss_secondary_sanctioned,
							'ss_mpr_secondary_boys'=>$ss_secondary_boys,
							'ss_mpr_secondary_boys'=>$ss_secondary_girls,
							'ss_mpr_secondary_vacant'=>$ss_secondary_vacant,
							'ss_mpr_data_month'=>$month,
							'ss_mpr_created_on'=>$created_on);
							
						}
                            else
						{
						$eight_column_data[] = array('ss_mpr_metric_master_id'=>$eight_column_metric_id,
						'ss_mpr_metric_master_id'=>$eight_column_metric_id,
						'ss_mpr_partner_id'=>$partner_id,
						'ss_mpr_elementory_sanctioned'=>'',
							'ss_mpr_elementory_boys'=>'',
							'ss_mpr_elementory_girls'=>'',
							'ss_mpr_elementory_vacant'=>'',
							'ss_mpr_secondary_sanctioned'=>'',
							'ss_mpr_secondary_boys'=>'',
							'ss_mpr_secondary_boys'=>'',
							'ss_mpr_secondary_vacant'=>'',
							'ss_mpr_data_month'=>$month,
							'ss_mpr_created_on'=>$created_on);
						}						
					}  
				     else 
					 {
						$ss_elementory_sanctioned=$data_eight->ss_elementory_sanctioned;
						$ss_elementory_boys=$data_eight->ss_elementory_boys;
						$ss_elementory_girls=$data_eight->ss_elementory_girls;
						$ss_elementory_vacant=$data_eight->ss_elementory_vacant;
						$ss_secondary_sanctioned=$data_eight->ss_secondary_sanctioned;
						$ss_secondary_boys=$data_eight->ss_secondary_boys;
						$ss_secondary_girls=$data_eight->ss_secondary_girls;
						$ss_secondary_vacant=$data_eight->ss_secondary_vacant;
						
							 
						$eight_column_data[] = array('ss_mpr_metric_master_id'=>$eight_column_metric_id,
						'ss_mpr_metric_master_id'=>$eight_column_metric_id,
						'ss_mpr_partner_id'=>$partner_id,
						    'ss_mpr_elementory_sanctioned'=>$ss_elementory_sanctioned,
							'ss_mpr_elementory_boys'=>$ss_elementory_boys,
							'ss_mpr_elementory_girls'=>$ss_elementory_girls,
							'ss_mpr_elementory_vacant'=>$ss_elementory_vacant,
							'ss_mpr_secondary_sanctioned'=>$ss_secondary_sanctioned,
							'ss_mpr_secondary_boys'=>$ss_secondary_boys,
							'ss_mpr_secondary_boys'=>$ss_secondary_girls,
							'ss_mpr_secondary_vacant'=>$ss_secondary_vacant,
							'ss_mpr_data_month'=>$month,
							'ss_mpr_created_on'=>$created_on);
					}  
				}
				//echo "<pre>"; print_r($eight_column_data); die;
				$this->Reh_main_entry_model->create_eight_column_mpr($eight_column_data);//Four column data insert
			}	
          }	
			$this->Reh_main_entry_model->updateMprSummary($mpr_summ_update_data,$mpr_summ_update_where_data);			
			redirect($this->agent->referrer());
		}
	}
	
	
	public function edit_form_data_edu()
	{  
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{			
		
				$this->load->library('user_agent');
				if($this->input->post('update_form_data'))
				{
					$month_from = $this->input->post('mpr_summary_month');
					$this->update_form_data($month_from);
					redirect($this->agent->referrer());
				}
		}
	}
	public function update_form_data($month_from)
	{		
		$partner_id = $this->session->userdata['userinfo']['partner_id'];
		$getdata = base64_decode($this->uri->segment(2));
		$explodedata = explode('-',$getdata);
		$project_name = $explodedata[0];
		$module_name = $explodedata[1];
		$sections = $this->Reh_main_entry_model->getSectionId($project_name,$module_name);
		$year = date('Y',strtotime($month_from));
				
		if($this->input->post('mpr_summary_id'))
		{
		function get_section_id($sections)
		{
			return $sections->ss_section_layout_id;
		}
		$section_ids =  implode(array_map("get_section_id", $sections), ',');
		$metrics = $this->Reh_main_entry_model->getMetricId($section_ids,$year);
		function get_metric_id($metrics)
		{
			return $metrics->ss_metric_master_id;
		}
		$metrics_ids =  implode(array_map("get_metric_id", $metrics), ',');
		
		//Get one column id
		$get_onecolumn_ids = $this->Reh_main_entry_model->getOneColumnData($partner_id,$month_from,$metrics_ids);
		function get_one_column_id($get_onecolumn_ids)
		{
			return $get_onecolumn_ids->ss_one_column_data_id;
		}
		$one_column_ids =  implode(array_map("get_one_column_id", $get_onecolumn_ids), ',');
		
		//Get four column id
		$get_fourcolumn_ids = $this->Reh_main_entry_model->getFourColumnData($partner_id,$month_from,$metrics_ids);
		function get_four_column_id($get_fourcolumn_ids)
		{
			return $get_fourcolumn_ids->ss_four_column_data_id;
		}
		$four_column_ids =  implode(array_map("get_four_column_id", $get_fourcolumn_ids), ',');
		
		// Get eight column id
		$get_eightcolumn_ids = $this->Reh_main_entry_model->getEightColumnData($partner_id,$month_from,$metrics_ids);
		function get_eight_column_id($get_eightcolumn_ids)
		{
			return $get_eightcolumn_ids->ss_eight_data_id;
		}
		$eight_column_ids =  implode(array_map("get_eight_column_id", $get_eightcolumn_ids), ',');
		
		//Delete old data
		//one column data		
			$this->Reh_main_entry_model->deleteOneColumnData($one_column_ids);
		//four column data
			$this->Reh_main_entry_model->deleteFourColumnData($four_column_ids);
		//eight column data
			$this->Reh_main_entry_model->deleteEightColumnData($eight_column_ids);
		}
		
		
		
		
		//Insert old and new data
		$data_month = $this->input->post('mpr_summary_month');
				
		$created_on = server_date_time();
		
			//One column
		//	echo $this->input->post('matric_id_one_column'); die;
			if($this->input->post('matric_id_one_column'))
			{
				$one_column_metric_ids = $this->input->post('matric_id_one_column');//post one column metric id
				$post_one_column_data = $this->input->post('one_column_val');//post one column value
				
				foreach($one_column_metric_ids as $key_metrics_one_column=>$one_column_metric_id)
				{
					$one_column_data[] = array('ss_metric_master_id'=>$one_column_metric_id,'ss_partner_id'=>$partner_id,'ss_one_column_data_value'=>$post_one_column_data[$key_metrics_one_column],'ss_one_column_data_month'=>$data_month,'ss_one_column_data_created_on'=>$created_on);
				}
				$this->Reh_main_entry_model->create_one_column($one_column_data);//One column data insert
			}
			
			//Four Column
			if($this->input->post('matric_id_four_column'))
			{
				$four_column_metric_ids = $this->input->post('matric_id_four_column');//post four column metric id
				$post_four_column_men_data = $this->input->post('four_column_men_val');//post four column men value
				$post_four_column_women_data = $this->input->post('four_column_women_val');//post four column women value
				$post_four_column_trans_data = $this->input->post('four_column_trans_val');//post four column transgender value
				$post_four_column_boy_data = $this->input->post('four_column_boy_val');//post four column boy value
				$post_four_column_girl_data = $this->input->post('four_column_girl_val');//post four column girl value
				
				foreach($four_column_metric_ids as $key_four_column=>$four_column_metric_id)
				{
					$four_column_data[] = array('ss_metric_master_id'=>$four_column_metric_id,'ss_partner_id'=>$partner_id,'ss_four_column_data_value_men'=>$post_four_column_men_data[$key_four_column],'ss_four_column_data_value_women'=>$post_four_column_women_data[$key_four_column],'ss_four_column_data_value_trans'=>$post_four_column_trans_data[$key_four_column],'ss_four_column_data_value_boys'=>$post_four_column_boy_data[$key_four_column],'ss_four_column_data_value_girls'=>$post_four_column_girl_data[$key_four_column],'ss_four_column_data_month'=>$data_month,'ss_four_column_data_created_on'=>$created_on);
				}
				$this->Reh_main_entry_model->create_four_column($four_column_data);//Four column data insert
			}
			
			//Eightcolumn data 
			
			if($this->input->post('matric_id_eight_column'))
			{
				$eight_column_metric_ids = $this->input->post('matric_id_eight_column');//post eight column metric id
				$post_elementory_sanctioned_data = $this->input->post('eight_column_ele_sanctioned');//post four column men value
				$post_elementory_boys_data = $this->input->post('eight_column_ele_male');//post four column women value
				$post_elementory_girls_data = $this->input->post('eight_column_ele_female');//post four column transgender value
				$post_elementory_vacant_data = $this->input->post('eight_column_ele_vacant');//post four column boy value
				$post_secondary_sanctioned_data = $this->input->post('eight_column_sec_sanctioned');//post four column girl value
				$post_secondary_boys_data = $this->input->post('eight_column_sec_male');//post four column girl value
				$post_ss_secondary_girls_data = $this->input->post('eight_column_sec_female');//post four column girl value
				$post_secondary_vacant_data = $this->input->post('eight_column_sec_vacant');//post four column girl value
				
				foreach($eight_column_metric_ids as $key_eight_column=>$eight_column_metric_id)
				{
					$eight_column_data[] = array('ss_metric_master_id'=>$eight_column_metric_id,'ss_partner_id'=>$partner_id,'ss_elementory_sanctioned'=>$$post_elementory_sanctioned_data[$key_eight_column],'ss_elementory_boys'=>$post_elementory_boys_data[$key_eight_column],'ss_elementory_girls'=>$post_elementory_girls_data[$key_eight_column],'ss_elementory_vacant'=>$post_elementory_vacant_data[$key_eight_column],'ss_secondary_sanctioned'=>$post_secondary_sanctioned_data[$key_eight_column],'ss_secondary_boys'=>$post_secondary_boys_data[$key_eight_column],'ss_secondary_girls'=>$post_ss_secondary_girls_data[$key_eight_column],'ss_secondary_vacant'=>$post_secondary_vacant_data[$key_eight_column],'ss_data_month'=>$data_month,'ss_created_on'=>$created_on);
				}
				//echo "<pre>";print_R($eight_column_data); die; 
				$this->Reh_main_entry_model->create_eight_column($eight_column_data);//Eight column data insert
			}
			
			//Mpr summary
			$partner_info = $this->Reh_main_entry_model->getPartnerInfo($partner_id);//get partner info
			$check_mpr_summ_exist = $this->Reh_main_entry_model->checkMprExist($partner_id,$project_name,$module_name,$data_month);//check mpr exist			
			if(empty($check_mpr_summ_exist))
			{
				$mpr_summ_data = array('ss_mpr_summary_partner_id'=>$partner_id,'ss_mpr_project_name'=>$project_name,'ss_mpr_module_name'=>$module_name,'ss_mpr_start_date'=>$partner_info->ss_partners_created_on,'status'=>1,'ss_mpr_report_month'=>$data_month,'ss_mpr_modified_on'=>$created_on,'ss_mpr_created_on'=>$created_on);
				
				//create mpr summary
				$this->Reh_main_entry_model->createMprSummary($mpr_summ_data);
			}else{
				$mpr_summ_update_data = array('ss_mpr_modified_on'=>$created_on);
				$mpr_summ_update_where_data = array('ss_mpr_summary_id'=>$check_mpr_summ_exist->ss_mpr_summary_id);
				//update mpr summary
				
				$this->Reh_main_entry_model->updateMprSummary($mpr_summ_update_data,$mpr_summ_update_where_data);
			}
	}

}
