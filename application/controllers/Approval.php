<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Approval extends CI_Controller {
	
	 public function __construct() {
        parent::__construct();       
		check_login();		
		$this->load->model('Approval_model');
		 $this->load->model('Reh_main_entry_model');
    }
	
    public function index() {
		
		$created_on = server_date_time();
		$date_only = date("Y-m",strtotime($created_on));
		$create_date = $date_only."-01";
		$today_date = date("Y-m-d",strtotime($create_date));
		$data['summary_data'] = $this->Approval_model->getFilterSummary($today_date);
		$data['partner_data'] = $this->Approval_model->getAllPartner();
		$data['state_data'] = $this->Approval_model->getAllState();
		$data['district_data'] = $this->Approval_model->getAllDistrict();

		$data['include'] = "approval/approval_report";
		$this->load->view('container_login1', $data);
    }
	public function filter_data()
	{
		
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			
			if($this->input->post('month_from') && !empty($this->input->post('month_from')))
			{
			$combine_day = "01-".$this->input->post('month_from');
			$month_from = date("Y-m-d",strtotime($combine_day));
			}
			else if(empty($this->input->post('month_from')))
			{
				$month_from = "";
			}
			else{
				$month_from = "";
			}			
			$data['summary_data'] = $this->Approval_model->getFilterSummary($month_from);
			//echo "<pre>"; print_R($data['summary_data']); die;
			$data['csrfHash'] = $this->security->get_csrf_hash();
			if(!empty($data['summary_data']))
			{
				$data['success'] = 1;
			}else{
				$data['success']= 0;
			}			
			echo json_encode($data);
		}		
	}
	public function partner_mpr() {
		$partner_id = 0;//$_SESSION["partner_id"];
		$created_on = server_date_time();
		$userid=$this->session->userdata('userinfo')['user_id'];
		$date_only = date("Y-m",strtotime($created_on));
		$create_date = $date_only."-01";
		$today_date = date("Y-m-d",strtotime($create_date));
		$data['summary_data'] = $this->Approval_model->getPartnerFilterSummary($today_date, 0);
		$data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		//echo "<pre>"; print_r($data); exit;
		$data['include'] = "mpr/partner_report";
		$this->load->view('container_login_dis', $data);
    }
	public function partner_filter_data()
	{
		
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$partner_id = $this->input->post('partner');
			if($this->input->post('month_from') && !empty($this->input->post('month_from')))
			{
			$combine_day = "01-".$this->input->post('month_from');
			$month_from = date("Y-m-d",strtotime($combine_day));
			}
			else if(empty($this->input->post('month_from')))
			{
				$month_from = "";
			}
			else{
				$month_from = "";
			}			
			$data['summary_data'] = $this->Approval_model->getPartnerFilterSummary($month_from, $partner_id);
			//echo "<pre>"; print_R($data['summary_data']); die;
			$data['csrfHash'] = $this->security->get_csrf_hash();
			if(!empty($data['summary_data']))
			{
				$data['success'] = 1;
			}else{
				$data['success']= 0;
			}			
			echo json_encode($data);
		}		
	}
	
}
