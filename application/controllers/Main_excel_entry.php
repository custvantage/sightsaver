<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Main_excel_entry extends CI_Controller 
{

    public function __construct()
	 {
        parent::__construct();
        $this->load->model('Main_excel_entry_model');
     }
	
	 public function excelupload() 
	 {
			if(!empty($_FILES['upload_publish_file']['name']))
							{
								require_once("/library_excel/PHPExcel.php");
								$excel_file= $_FILES['upload_publish_file']['tmp_name'];
								$inputFileName = $excel_file; 
									try 
										{
										$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
										} 
									catch(Exception $e)
										{
										die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
										}
                                        require_once('library_excel/PHPExcel/IOFactory.php');

										$objReader = PHPExcel_IOFactory::createReader('Excel2007');
										$inputFileType = 'Excel2007';
										//$inputFileName = 'hello.xlsx';
										$sheetIndex = 0;
										$objReader = PHPExcel_IOFactory::createReader($inputFileType);
										$sheetnames = $objReader->listWorksheetNames($inputFileName);
										$objReader->setLoadSheetsOnly($sheetnames[$sheetIndex]);
										$objPHPExcel = $objReader->load($inputFileName);
										$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
										//var_dump($sheetData); die;
										// 1- for the OPD///////
									  $date1=0;
                                        $start=5;
											for ($row = $start; $row <=5; $row++) 
											{									
												$month = $objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue();
												$year = $objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue();
												$month1 = $monthname = date("m", strtotime($month));
									            $date1 = $year."-".$month1."-".'00'; 
											} 	
                                        //echo $date1; die;											
										$start=14;
											for ($row = $start; $row <= 17; $row++) 
											{
												$cal_data[] = array('ss_partner_id'=>1,
												'ss_metric_master_id'=>2,
												'ss_four_column_data_value_men'=>$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue(),
												'ss_four_column_data_value_women'=>$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue(),
												'ss_four_column_data_value_boys'=>$objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue(),
												'ss_four_column_data_value_girls'=>$objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue(),
												'ss_four_column_data_month'=>$date1,
												'ss_four_column_data_created_on'=>date('Y-m-d H:i:s'));	
											}	
											echo '<pre>'; print_R($cal_data); die;
										//2- for the Referral/ Reported (four column)///////
										$start=19;
											for ($row = $start; $row <= 24; $row++) 
											{
												$cal_data[] = array('ss_partner_id'=>1,
												'ss_metric_master_id'=>2,
												'ss_four_column_data_value_men'=>$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue(),
												'ss_four_column_data_value_women'=>$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue(),
												'ss_four_column_data_value_boys'=>$objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue(),
												'ss_four_column_data_value_girls'=>$objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue(),
												'ss_four_column_data_month'=>$date1,
												'ss_four_column_data_created_on'=>date('Y-m-d H:i:s'));	
											}
										// 3- for the Refraction (four column)///////
										$start=26;
											for ($row = $start; $row <= 31; $row++) 
											{
												$cal_data[] = array('ss_partner_id'=>1,
												'ss_metric_master_id'=>2,
												'ss_four_column_data_value_men'=>$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue(),
												'ss_four_column_data_value_women'=>$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue(),
												'ss_four_column_data_value_boys'=>$objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue(),
												'ss_four_column_data_value_girls'=>$objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue(),
												'ss_four_column_data_month'=>$date1,
												'ss_four_column_data_created_on'=>date('Y-m-d H:i:s'));	
											}
										//4- for the  Spectacles  (four column)///////////
										$start=33;
											for ($row = $start; $row <= 44; $row++) 
											{
												$cal_data[] = array('ss_partner_id'=>1,
												'ss_metric_master_id'=>2,
												'ss_four_column_data_value_men'=>$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue(),
												'ss_four_column_data_value_women'=>$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue(),
												'ss_four_column_data_value_boys'=>$objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue(),
												'ss_four_column_data_value_girls'=>$objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue(),
												'ss_four_column_data_month'=>$date1,
												'ss_four_column_data_created_on'=>date('Y-m-d H:i:s'));	
											}
										//5- for the Cataract  (four column)/////////////
										$start=46;
											for ($row = $start; $row <= 58; $row++) 
											{
												$cal_data[] = array('ss_partner_id'=>1,
												'ss_metric_master_id'=>2,
												'ss_four_column_data_value_men'=>$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue(),
												'ss_four_column_data_value_women'=>$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue(),
												'ss_four_column_data_value_boys'=>$objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue(),
												'ss_four_column_data_value_girls'=>$objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue(),
												'ss_four_column_data_month'=>$date1,
												'ss_four_column_data_created_on'=>date('Y-m-d H:i:s'));	
											}
										// 6- for the Glaucoma  (four column)///////////
										$start=60;
											for ($row = $start; $row <= 62; $row++) 
											{
												$cal_data[] = array('ss_partner_id'=>1,
												'ss_metric_master_id'=>2,
												'ss_four_column_data_value_men'=>$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue(),
												'ss_four_column_data_value_women'=>$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue(),
												'ss_four_column_data_value_boys'=>$objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue(),
												'ss_four_column_data_value_girls'=>$objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue(),
												'ss_four_column_data_month'=>$date1,
												'ss_four_column_data_created_on'=>date('Y-m-d H:i:s'));	
											}
										// 7- for the  Diabetic Retinopathy (DR)  (four column)/////
										$start=64;
											for ($row = $start; $row <= 67; $row++) 
											{
												$cal_data[] = array('ss_partner_id'=>1,
												'ss_metric_master_id'=>2,
												'ss_four_column_data_value_men'=>$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue(),
												'ss_four_column_data_value_women'=>$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue(),
												'ss_four_column_data_value_boys'=>$objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue(),
												'ss_four_column_data_value_girls'=>$objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue(),
												'ss_four_column_data_month'=>$date1,
												'ss_four_column_data_created_on'=>date('Y-m-d H:i:s'));	
											}

										// 8- for the Low Vision and irreversible blindness (four column)///////
										$start=69;
											for ($row = $start; $row <= 72; $row++) 
											{
												$cal_data[] = array('ss_partner_id'=>1,
												'ss_metric_master_id'=>2,
												'ss_four_column_data_value_men'=>$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue(),
												'ss_four_column_data_value_women'=>$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue(),
												'ss_four_column_data_value_boys'=>$objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue(),
												'ss_four_column_data_value_girls'=>$objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue(),
												'ss_four_column_data_month'=>$date1,
												'ss_four_column_data_created_on'=>date('Y-m-d H:i:s'));	
											}
										// 9- Any other treatment (four column)///////// 
										$start=74;
											for ($row = $start; $row <= 75; $row++) 
											{
												$cal_data[] = array('ss_partner_id'=>1,
												'ss_metric_master_id'=>2,
												'ss_four_column_data_value_men'=>$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue(),
												'ss_four_column_data_value_women'=>$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue(),
												'ss_four_column_data_value_boys'=>$objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue(),
												'ss_four_column_data_value_girls'=>$objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue(),
												'ss_four_column_data_month'=>$date1,
												'ss_four_column_data_created_on'=>date('Y-m-d H:i:s'));	
											}
										// 10- Please provide a brief analysis of the information given above in terms of (analysis)///////															
										 
										$start=77;
											for ($row = $start; $row <= 80; $row++) 
											{
												$analysis_data[] = array('ss_partner_id'=>1,
												'ss_metric_master_id'=>2,
												'ss_analysis_data_value'=>$objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue(),
												'ss_analysis_data_month'=>$date1,
												'ss_analysis_data_created_on'=>date('Y-m-d H:i:s'));	
											}

										// 11- System strengthening with Government (four column) ///// 
										$start=84;
											for ($row = $start; $row <= 91; $row++) 
											{
												$cal_data[] = array('ss_partner_id'=>1,
												'ss_metric_master_id'=>2,
												'ss_four_column_data_value_men'=>$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue(),
												'ss_four_column_data_value_women'=>$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue(),
												'ss_four_column_data_value_boys'=>$objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue(),
												'ss_four_column_data_value_girls'=>$objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue(),
												'ss_four_column_data_month'=>$date1,
												'ss_four_column_data_created_on'=>date('Y-m-d H:i:s'));	
											}
										// 12- Human Resource (Training / capacity building) (one column)//////
										$start=93;
											for ($row = $start; $row <= 106; $row++) 
											{
												$cal_one_data[] = array('ss_partner_id'=>1,
												'ss_metric_master_id'=>2,
												'ss_one_column_data_value'=>$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue(),
												'ss_one_column_data_month'=>$date1,
												'ss_one_column_data_created_on'=>date('Y-m-d H:i:s'));	
											}
										//  13- brief analysis of the information (Analysis) //////
										$start=108;
											for ($row = $start; $row <= 111; $row++) 
											{
												$analysis_data[] = array('ss_partner_id'=>1,
												'ss_metric_master_id'=>2,
												'ss_analysis_data_value'=>$objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue(),
												'ss_analysis_data_month'=>$date1,
												'ss_analysis_data_created_on'=>date('Y-m-d H:i:s'));	
											}
										//  14-Advocacy, networking, Liaison  (one column) //////////////
										$start=115;
											for ($row = $start; $row <= 118; $row++) 
											{
												$cal_one_data[] = array('ss_partner_id'=>1,
												'ss_metric_master_id'=>2,
												'ss_one_column_data_value'=>$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue(),
												'ss_one_column_data_month'=>$date1,
												'ss_one_column_data_created_on'=>date('Y-m-d H:i:s'));	
											}
										// 15- Please provide a brief analysis of the information given above in terms of  (Analysis)///////////
										$start=120;
											for ($row = $start; $row <= 123; $row++) 
											{
												$analysis_data[] = array('ss_partner_id'=>1,
												'ss_metric_master_id'=>2,
												'ss_analysis_data_value'=>$objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue(),
												'ss_analysis_data_month'=>$date1,
												'ss_analysis_data_created_on'=>date('Y-m-d H:i:s'));	
											}
										// 16- IEC/ BCC Activity   (one column)///
										$start=127;
											for ($row = $start; $row <= 136; $row++) 
											{
												$cal_one_data[] = array('ss_partner_id'=>1,
												'ss_metric_master_id'=>2,
												'ss_one_column_data_value'=>$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue(),
												'ss_one_column_data_month'=>$date1,
												'ss_one_column_data_created_on'=>date('Y-m-d H:i:s'));	
											}

										// 17-Please provide a brief analysis of the information given above in terms of (analysis) ///
										$start=138;
											for ($row = $start; $row <= 141; $row++) 
											{
											$analysis_data[] = array('ss_partner_id'=>1,
											'ss_metric_master_id'=>2,
											'ss_analysis_data_value'=>$objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue(),
											'ss_analysis_data_month'=>$date1,
											'ss_analysis_data_created_on'=>date('Y-m-d H:i:s'));	
											}
											
									//echo '<pre>'; print_R($cal_one_data); die;
									$this->Main_excel_entry_model->four_column_excel_upload($cal_data);
									$this->Main_excel_entry_model->analysis_excel_upload($analysis_data);
									$this->Main_excel_entry_model->one_column_excel_upload($cal_one_data);
							}          
			  $data['include'] = "excelupload";
			  $this->load->view('container_login', $data);
	} 
//end of the rural excel upload//////////


/*	public function excelupload() 
	 {
			if(!empty($_FILES['upload_publish_file']['name']))
							{
									require_once("/library_excel/PHPExcel.php");
									$excel_file= $_FILES['upload_publish_file']['tmp_name'];
								$inputFileName = $excel_file; 
									try 
										{
										$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
										} 
									catch(Exception $e)
										{
										die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
										}

									$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
									//var_dump($allDataInSheet); die;
                                   // $objPHPExcel->setActiveSheetIndex();
									//$arrayCount = count($allDataInSheet);// Here get total count of row in that Excel sheet
										// 1- for the OPD///////
									$date1=0;
                                        $start=5;
											for ($row = $start; $row <=5; $row++) 
											{									
												$month = $objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue();
												$year = $objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue();
												$month1 = $monthname = date("m", strtotime($month));
									            $date1 = $year."-".$month1."-".'00'; 
											} 	
                                        //echo $date1; die;											
										$start=14;
											for ($row = $start; $row <= 17; $row++) 
											{
												$cal_data[] = array('ss_partner_id'=>1,
												'ss_metric_master_id'=>2,
												'ss_four_column_data_value_men'=>$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue(),
												'ss_four_column_data_value_women'=>$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue(),
												'ss_four_column_data_value_boys'=>$objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue(),
												'ss_four_column_data_value_girls'=>$objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue(),
												'ss_four_column_data_month'=>$date1,
												'ss_four_column_data_created_on'=>date('Y-m-d H:i:s'));	
											}	
											//echo '<pre>'; print_R($cal_data); die;
										//2- for the Referral/ Reported (four column)///////
										$start=19;
											for ($row = $start; $row <= 24; $row++) 
											{
												$cal_data[] = array('ss_partner_id'=>1,
												'ss_metric_master_id'=>2,
												'ss_four_column_data_value_men'=>$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue(),
												'ss_four_column_data_value_women'=>$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue(),
												'ss_four_column_data_value_boys'=>$objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue(),
												'ss_four_column_data_value_girls'=>$objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue(),
												'ss_four_column_data_month'=>$date1,
												'ss_four_column_data_created_on'=>date('Y-m-d H:i:s'));	
											}
										// 3- for the Refraction (four column)///////
										$start=26;
											for ($row = $start; $row <= 31; $row++) 
											{
												$cal_data[] = array('ss_partner_id'=>1,
												'ss_metric_master_id'=>2,
												'ss_four_column_data_value_men'=>$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue(),
												'ss_four_column_data_value_women'=>$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue(),
												'ss_four_column_data_value_boys'=>$objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue(),
												'ss_four_column_data_value_girls'=>$objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue(),
												'ss_four_column_data_month'=>$date1,
												'ss_four_column_data_created_on'=>date('Y-m-d H:i:s'));	
											}
										//4- for the  Spectacles  (four column)///////////
										$start=33;
											for ($row = $start; $row <= 44; $row++) 
											{
												$cal_data[] = array('ss_partner_id'=>1,
												'ss_metric_master_id'=>2,
												'ss_four_column_data_value_men'=>$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue(),
												'ss_four_column_data_value_women'=>$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue(),
												'ss_four_column_data_value_boys'=>$objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue(),
												'ss_four_column_data_value_girls'=>$objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue(),
												'ss_four_column_data_month'=>$date1,
												'ss_four_column_data_created_on'=>date('Y-m-d H:i:s'));	
											}
										//5- for the Cataract  (four column)/////////////
										$start=46;
											for ($row = $start; $row <= 58; $row++) 
											{
												$cal_data[] = array('ss_partner_id'=>1,
												'ss_metric_master_id'=>2,
												'ss_four_column_data_value_men'=>$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue(),
												'ss_four_column_data_value_women'=>$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue(),
												'ss_four_column_data_value_boys'=>$objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue(),
												'ss_four_column_data_value_girls'=>$objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue(),
												'ss_four_column_data_month'=>$date1,
												'ss_four_column_data_created_on'=>date('Y-m-d H:i:s'));	
											}
										// 6- for the Glaucoma  (four column)///////////
										$start=60;
											for ($row = $start; $row <= 62; $row++) 
											{
												$cal_data[] = array('ss_partner_id'=>1,
												'ss_metric_master_id'=>2,
												'ss_four_column_data_value_men'=>$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue(),
												'ss_four_column_data_value_women'=>$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue(),
												'ss_four_column_data_value_boys'=>$objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue(),
												'ss_four_column_data_value_girls'=>$objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue(),
												'ss_four_column_data_month'=>$date1,
												'ss_four_column_data_created_on'=>date('Y-m-d H:i:s'));	
											}
										// 7- for the  Diabetic Retinopathy (DR)  (four column)/////
										$start=64;
											for ($row = $start; $row <= 67; $row++) 
											{
												$cal_data[] = array('ss_partner_id'=>1,
												'ss_metric_master_id'=>2,
												'ss_four_column_data_value_men'=>$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue(),
												'ss_four_column_data_value_women'=>$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue(),
												'ss_four_column_data_value_boys'=>$objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue(),
												'ss_four_column_data_value_girls'=>$objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue(),
												'ss_four_column_data_month'=>$date1,
												'ss_four_column_data_created_on'=>date('Y-m-d H:i:s'));	
											}

										// 8- for the Low Vision and irreversible blindness (four column)///////
										$start=69;
											for ($row = $start; $row <= 72; $row++) 
											{
												$cal_data[] = array('ss_partner_id'=>1,
												'ss_metric_master_id'=>2,
												'ss_four_column_data_value_men'=>$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue(),
												'ss_four_column_data_value_women'=>$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue(),
												'ss_four_column_data_value_boys'=>$objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue(),
												'ss_four_column_data_value_girls'=>$objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue(),
												'ss_four_column_data_month'=>$date1,
												'ss_four_column_data_created_on'=>date('Y-m-d H:i:s'));	
											}
										// 9- Any other treatment (four column)///////// 
										$start=74;
											for ($row = $start; $row <= 75; $row++) 
											{
												$cal_data[] = array('ss_partner_id'=>1,
												'ss_metric_master_id'=>2,
												'ss_four_column_data_value_men'=>$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue(),
												'ss_four_column_data_value_women'=>$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue(),
												'ss_four_column_data_value_boys'=>$objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue(),
												'ss_four_column_data_value_girls'=>$objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue(),
												'ss_four_column_data_month'=>$date1,
												'ss_four_column_data_created_on'=>date('Y-m-d H:i:s'));	
											}
										// 10- Please provide a brief analysis of the information given above in terms of (analysis)///////															
										 
										$start=77;
											for ($row = $start; $row <= 80; $row++) 
											{
												$analysis_data[] = array('ss_partner_id'=>1,
												'ss_metric_master_id'=>2,
												'ss_analysis_data_value'=>$objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue(),
												'ss_analysis_data_month'=>$date1,
												'ss_analysis_data_created_on'=>date('Y-m-d H:i:s'));	
											}

										// 11- System strengthening with Government (four column) ///// 
										$start=84;
											for ($row = $start; $row <= 91; $row++) 
											{
												$cal_data[] = array('ss_partner_id'=>1,
												'ss_metric_master_id'=>2,
												'ss_four_column_data_value_men'=>$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue(),
												'ss_four_column_data_value_women'=>$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue(),
												'ss_four_column_data_value_boys'=>$objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue(),
												'ss_four_column_data_value_girls'=>$objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue(),
												'ss_four_column_data_month'=>$date1,
												'ss_four_column_data_created_on'=>date('Y-m-d H:i:s'));	
											}
										// 12- Human Resource (Training / capacity building) (one column)//////
										$start=93;
											for ($row = $start; $row <= 106; $row++) 
											{
												$cal_one_data[] = array('ss_partner_id'=>1,
												'ss_metric_master_id'=>2,
												'ss_one_column_data_value'=>$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue(),
												'ss_one_column_data_month'=>$date1,
												'ss_one_column_data_created_on'=>date('Y-m-d H:i:s'));	
											}
										//  13- brief analysis of the information (Analysis) //////
										$start=108;
											for ($row = $start; $row <= 111; $row++) 
											{
												$analysis_data[] = array('ss_partner_id'=>1,
												'ss_metric_master_id'=>2,
												'ss_analysis_data_value'=>$objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue(),
												'ss_analysis_data_month'=>$date1,
												'ss_analysis_data_created_on'=>date('Y-m-d H:i:s'));	
											}
										//  14-Advocacy, networking, Liaison  (one column) //////////////
										$start=115;
											for ($row = $start; $row <= 118; $row++) 
											{
												$cal_one_data[] = array('ss_partner_id'=>1,
												'ss_metric_master_id'=>2,
												'ss_one_column_data_value'=>$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue(),
												'ss_one_column_data_month'=>$date1,
												'ss_one_column_data_created_on'=>date('Y-m-d H:i:s'));	
											}
										// 15- Please provide a brief analysis of the information given above in terms of  (Analysis)///////////
										$start=120;
											for ($row = $start; $row <= 123; $row++) 
											{
												$analysis_data[] = array('ss_partner_id'=>1,
												'ss_metric_master_id'=>2,
												'ss_analysis_data_value'=>$objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue(),
												'ss_analysis_data_month'=>$date1,
												'ss_analysis_data_created_on'=>date('Y-m-d H:i:s'));	
											}
										// 16- IEC/ BCC Activity   (one column)///
										$start=127;
											for ($row = $start; $row <= 136; $row++) 
											{
												$cal_one_data[] = array('ss_partner_id'=>1,
												'ss_metric_master_id'=>2,
												'ss_one_column_data_value'=>$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue(),
												'ss_one_column_data_month'=>$date1,
												'ss_one_column_data_created_on'=>date('Y-m-d H:i:s'));	
											}

										// 17-Please provide a brief analysis of the information given above in terms of (analysis) ///
										$start=138;
											for ($row = $start; $row <= 141; $row++) 
											{
											$analysis_data[] = array('ss_partner_id'=>1,
											'ss_metric_master_id'=>2,
											'ss_analysis_data_value'=>$objPHPExcel->getActiveSheet()->getCell('C'.$row)->getValue(),
											'ss_analysis_data_month'=>$date1,
											'ss_analysis_data_created_on'=>date('Y-m-d H:i:s'));	
											}
											
									//echo '<pre>'; print_R($cal_one_data); die;
									$this->Main_excel_entry_model->four_column_excel_upload($cal_data);
									$this->Main_excel_entry_model->analysis_excel_upload($analysis_data);
									$this->Main_excel_entry_model->one_column_excel_upload($cal_one_data);
							}          
			  $data['include'] = "excelupload";
			  $this->load->view('container_login', $data);
	} */

}
