<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Ueh_yearly_target extends CI_Controller {

    public function __construct() {
        parent::__construct();
		check_login();
        $this->load->model('Reh_main_entry_model');
		$this->load->library('metricdata');
		$this->load->library('common');
    }

    public function index() {
		
        if(!empty($this->uri->segment(2)))
        {						
            $getdata = base64_decode($this->uri->segment(2));
            $explodedata = explode('-',$getdata);
            $project_name = $explodedata[0];
            $module_name = $explodedata[1];
			$created_on = server_date_time();
			$current_year = date('Y',strtotime($created_on));
			
            $data['sections'] = $this->Reh_main_entry_model->getAllSections($project_name, $module_name,$current_year);
            
            $data['metrics_data'] = $this->Reh_main_entry_model->getAllMetricData($project_name,$module_name,$current_year);
            
			if($this->session->userdata('userinfo')['default_role'] != 6)//if pm/po
			{
				$data['partners'] = $this->common->getPartners();
			}
            $data['include'] = "ueh/ueh_yearly_target";
            $this->load->view('container_login', $data);
			
        }else
        {
            redirect('dashboard');
        }
    }
    public function create_ueh_yt_entry()
	{
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{	
			$getdata = base64_decode($this->uri->segment(2));
			$explodedata = explode('-',$getdata);
			$project_name = $explodedata[0];
			$module_name = $explodedata[1];
			
			$combine_day = "01-".$this->input->post('month_data');
			$data_month = date("Y-m-d",strtotime($combine_day));
			$year = date('Y',strtotime($combine_day));
			$data['partners'] = $this->common->getPartners();
			$partner_id1=$this->input->post('partner_name_h'); 
			$this->form_validation->set_rules('month_data','Month','required');
					
			if($this->form_validation->run()==true)
			{
				$created_on = server_date_time();
				$partner_id = $this->session->userdata['userinfo']['partner_id'];				
								
				//One column
				if($this->input->post('matric_id_one_column'))
				{
					$one_column_metric_ids = $this->input->post('matric_id_one_column');//post one column metric id
					$post_one_column_data = $this->input->post('one_column_val');//post one column value
					
					foreach($one_column_metric_ids as $key_metrics_one_column=>$one_column_metric_id)
					{
						$one_column_data[] = array('ss_metric_master_id'=>$one_column_metric_id,'ss_partner_id'=>$partner_id1,'ss_one_column_data_value'=>$post_one_column_data[$key_metrics_one_column],'ss_one_column_data_month'=>$data_month,'ss_one_column_data_created_on'=>$created_on);
					}
					$this->Reh_main_entry_model->create_one_column($one_column_data);//One column data insert
				}
				//Four Column
				if($this->input->post('matric_id_four_column'))
				{
					$four_column_metric_ids = $this->input->post('matric_id_four_column');//post four column metric id
					$post_four_column_men_data = $this->input->post('four_column_men_val');//post four column men value
					$post_four_column_women_data = $this->input->post('four_column_women_val');//post four column women value
					$post_four_column_trans_data = $this->input->post('four_column_trans_val');//post four column transgender value
					$post_four_column_boy_data = $this->input->post('four_column_boy_val');//post four column boy value
					$post_four_column_girl_data = $this->input->post('four_column_girl_val');//post four column girl value
					
					foreach($four_column_metric_ids as $key_four_column=>$four_column_metric_id)
					{
						$four_column_data[] = array('ss_metric_master_id'=>$four_column_metric_id,'ss_partner_id'=>$partner_id1,'ss_four_column_data_value_men'=>$post_four_column_men_data[$key_four_column],'ss_four_column_data_value_women'=>$post_four_column_women_data[$key_four_column],'ss_four_column_data_value_trans'=>$post_four_column_trans_data[$key_four_column],'ss_four_column_data_value_boys'=>$post_four_column_boy_data[$key_four_column],'ss_four_column_data_value_girls'=>$post_four_column_girl_data[$key_four_column],'ss_four_column_data_month'=>$data_month,'ss_four_column_data_created_on'=>$created_on);
					}
					$this->Reh_main_entry_model->create_four_column($four_column_data);//Four column data insert
				}
				
				//Analysis
				if($this->input->post('matric_id_analysis'))
				{
					$analysis_metric_ids = $this->input->post('matric_id_analysis');//post analysis metric id
					$post_analysis_data = $this->input->post('analysis_value');//post analysis value
					
					foreach($analysis_metric_ids as $key_analysis=>$analysis_metric_id)
					{
						$analysis_data[] = array('ss_metric_master_id'=>$analysis_metric_id,'ss_partner_id'=>$partner_id1,'ss_analysis_data_value'=>$post_analysis_data[$key_analysis],'ss_analysis_data_month'=>$data_month,'ss_analysis_data_created_on'=>$created_on);
					}
					$this->Reh_main_entry_model->create_analysis($analysis_data);//Analysis data insert
				}				
				
				//Mpr summary
				$partner_info = $this->Reh_main_entry_model->getPartnerInfo($partner_id1);//get partner info
				$check_mpr_summ_exist = $this->Reh_main_entry_model->checkMprExist($partner_id1,$project_name,$module_name,$data_month);//check mpr exist
				if(empty($check_mpr_summ_exist))
				{
					$mpr_summ_data = array('ss_mpr_summary_partner_id'=>$partner_id1,'ss_mpr_project_name'=>$project_name,'ss_mpr_module_name'=>$module_name,'ss_mpr_start_date'=>$partner_info->ss_partners_created_on,'status'=>1,'ss_mpr_report_month'=>$data_month,'ss_mpr_modified_on'=>$created_on,'ss_mpr_created_on'=>$created_on);
					
					//create mpr summary
					$this->Reh_main_entry_model->createMprSummary($mpr_summ_data);
				}else{
					$mpr_summ_update_data = array('ss_mpr_modified_on'=>$created_on);
					$mpr_summ_update_where_data = array('ss_mpr_summary_id'=>$check_mpr_summ_exist->ss_mpr_summary_id);
					//update mpr summary
					
					$this->Reh_main_entry_model->updateMprSummary($mpr_summ_update_data,$mpr_summ_update_where_data);
				}
			}
			$data['sections'] = $this->Reh_main_entry_model->getAllSections($project_name, $module_name,$year);
            
            $data['metrics_data'] = $this->Reh_main_entry_model->getAllMetricData($project_name,$module_name,$year);           
			
            $data['include'] = "ueh/ueh_yearly_target";
            $this->load->view('container_login', $data);
			
		}else{
			redirect('ueh_yearly_target/'.$this->uri->segment(2));
		}
	}
	public function manage()
	{
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$getdata = base64_decode($this->uri->segment(2));
			$explodedata = explode('-',$getdata);
			$project_name = $explodedata[0];
			$module_name = $explodedata[1];
			$combine_day = "01-".$this->input->post('month_from');			
			$data['post_month_from'] = date("Y-m-d",strtotime($combine_day));
			$year = date('Y',strtotime($combine_day));
			 $partner_id1=$this->input->post('partner_name'); 	
			$data['partners'] = $this->common->getPartners();
			$data['mpr_report_month'] = $data['post_month_from'];
			if($this->session->userdata('userinfo')['default_role'] == 4)//if partner
			{
				$data['session_partner_id'] = $partner_id1;
				$check_mpr_summ_exist = $this->Reh_main_entry_model->checkMprExistArr($data['session_partner_id'],$project_name,$module_name,$data['post_month_from']);//check mpr exist				
				if($check_mpr_summ_exist['status'] == 1 || $check_mpr_summ_exist['status'] == 4)
				{
					$data['mpr_id'] = $check_mpr_summ_exist['ss_mpr_summary_id'];
					//$data['mpr_report_month'] = $check_mpr_summ_exist['ss_mpr_report_month'];
				}else{
					$data['mpr_id'] = "";
					//$data['mpr_report_month'] = "";
				}
			}/* else{
				$data['session_partner_id'] = $this->input->post('partner_name');				
				$data['partners'] = $this->common->getPartners();
				$user_id = $this->session->userdata('userinfo')['user_id'];
				$data['comments'] = $this->common->getComments($user_id,$data['post_month_from'],$data['session_partner_id'],$project_name,$module_name);
				$data['comments_pmpo'] = $this->common->getPmpoComments($data['session_partner_id'],$data['post_month_from'],$project_name,$module_name);
			} */
			
			$data['sections'] = $this->Reh_main_entry_model->getAllSections($project_name, $module_name,$year);
		
			$data['metrics_data'] = $this->Reh_main_entry_model->getAllMetricData($project_name,$module_name,$year);			
			
			$data['include'] = "ueh/manage_ueh_yearly_target";
			$this->load->view('container_login', $data);
		}else{
			redirect('ueh_yearly_target/'.$this->uri->segment(2));
		}
	}
	
	
	
}
