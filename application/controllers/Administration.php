<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Administration extends CI_Controller 
{
    public function __construct()
	 {
        parent::__construct();
		check_login();
        $this->load->model('Administration_model');
     }
	 
    public function partner() 
	 {
		 $data['partner_data1']=$this->Administration_model->partner_data_fetch();
		 $data['district']=$this->Administration_model->district();
	     //var_dump($data['district']); die;
		 // load form helper and validation library
	//	$this->load->helper('form');
	//	$this->load->library('form_validation');
		  if($this->input->server('REQUEST_METHOD') === 'POST')
		    {      
	    $this->form_validation->set_rules('name', 'partner name', 'trim|required');
		$this->form_validation->set_rules('name', 'partner name', 'trim|required|is_unique[ss_partners.ss_partners_name]');	  
	    $this->form_validation->set_rules('districts', 'districts name', 'required');
			 if($this->form_validation->run()==true)
              {
			    date_default_timezone_set('Asia/Kolkata');
				$date1=date('Y-m-d H:i');
				$dis=explode('-',$this->input->post('districts'));
				
				$data1[] = array('ss_partners_name'=>$this->input->post('name').'('.$dis[1].')',
				'ss_partners_project_reh'=>$this->input->post('program1'),
				'ss_partners_project_ueh'=>$this->input->post('program2'),
				'ss_partners_project_si'=>$this->input->post('program3'),
				'ss_partners_project_ie'=>$this->input->post('program4'),
				'ss_partners_districts'=>$dis[0],
				'ss_partners_created_on'=>$date1);
			    	
				$this->Administration_model->partner_data($data1);
				$this->session->set_flashdata('success', 'Data save successfully');
				redirect("administration/partner");
		    }
		}
		
		  $data['include'] = "administration/partner";
		  $this->load->view('container_login', $data);
	 }
	 
	 public function districts() 
	 {
		  $data['state_data1']=$this->Administration_model->state_fetch();
		  $data['fetch_the_district']=$this->Administration_model->district_partner_fetch();
		   $this->form_validation->set_rules('district', 'district name', 'trim|required');
           $this->form_validation->set_rules('district', 'district name', 'trim|required|is_unique[ss_district.ss_district_name]');
		   $this->form_validation->set_rules('state', 'state name', 'required');
			 if($this->form_validation->run()==true)
              {
		    if($this->input->server('REQUEST_METHOD') === 'POST')
		        {
			    date_default_timezone_set('Asia/Kolkata');
				$date1=date('Y-m-d H:i');
				$data1[] = array('ss_district_name'=>$this->input->post('district'),
				'ss_states_ss_states_id'=>$this->input->post('state'),
				'ss_district_created_on'=>$date1);
				$this->Administration_model->district_insert($data1);
				$this->session->set_flashdata('success', 'Districts save successfully');
				redirect("administration/districts");
		        }
			  }      
		  $data['include'] = "administration/districts";
		  $this->load->view('container_login', $data);
	 }

/* public function generate_auto_pwd($chars_min = 4, $chars_max = 4, $use_upper_case = True, $include_numbers = True, $include_special_chars = False)
{
$length = rand($chars_min, $chars_max);
$selection = 'aeuoyibcdfghjklmnpqrstvwxz';
if ($include_numbers) 
{
$selection .= "0123456789";
}
if ($include_special_chars) {
$selection .= "!@#$%&";
}
$password = "";
for ($i = 0; $i < $length; $i++)
{
$current_letter = $use_upper_case ? (rand(0, 1) ? strtoupper($selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))];
$password .= $current_letter;
     }
     return $password;
} */
	 
function randomPassword() {
    $alphabet = 'abcdefghijklmnopqrstuvwxyz123456789';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}	

	 public function reporting_quarters() 
	 {
    
		  $data['include'] = "administration/reporting_quarters";
		  $this->load->view('container_login', $data);
	 }
	 
	  /* echo  date_default_timezone_set('Asia/Kolkata');
			    $date1=date('Y-m-d H:i'); 
				$data1[] = array('ss_user_fname'=>$this->input->post('firstname'),
				'ss_user_lname'=>$this->input->post('lastname'),
				'ss_user_email_id'=>$this->input->post('email'),
				'ss_user_role'=>$this->input->post('role'),
				'ss_partner_id'=>$this->input->post('partner_name'),
				'ss_user_status'=>$this->input->post('status'),
				'ss_user_created_on'=>$date1);
				 $this->Administration_model->partner_data($data1); */
				 
				 	///////////////////////////////////
/* public function email_register($message, $email_to, $subject) {
$this->load->library('email');

$config['protocol'] = 'smtp';
$config['smtp_host'] = 'smtpout.asia.secureserver.net';
$config['smtp_port'] = '80';
$config['smtp_user'] = 'shashi5july@gmail.com';
$config['smtp_pass'] = 'sanjana1983';
$config['charset'] = 'utf-8';
$config['mailtype'] = 'html';
$config['newline'] = "\r\n";

$this->email->initialize(
$config);
       
        $this->email->from('sightsavar.com', 'Sightsavar');
        $this->email->to('$email_to');
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->send();
		//echo $this->email->print_debugger();
		//die();
    } */
			 
   public function user() 
	 {  //echo "hello"; die;
		$data['role1']=$this->Administration_model->role();
		$data['partner_name1']=$this->Administration_model->partner_name();
		$data['user_name1']=$this->Administration_model->user_data_fetch();
		mail("shashi5july@gmail.com","My subject","hai");
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{  
			 $this->form_validation->set_rules('firstname', 'firstname', 'trim|required');
		     $this->form_validation->set_rules('lastname', 'lastname', 'required');
			 $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email|is_unique[ss_user.ss_user_email_id]');
			 $this->form_validation->set_rules('role', 'role', 'required');
			  //$this->form_validation->set_rules('partner_name', 'partner_name', 'required');
			 if($this->form_validation->run()==true)
              {
			    date_default_timezone_set('Asia/Kolkata');
			    $date1=date('Y-m-d H:i'); 
				$pass=$this->randomPassword();
				//$pass="SightSavers@1234";
				$pass1=md5($pass);
				
				$email=$this->input->post('email');
				
				$data1[] = array('ss_user_fname'=>$this->input->post('firstname'),
				'ss_user_lname'=>$this->input->post('lastname'),
				'ss_user_email_id'=>$this->input->post('email'),
				'ss_user_role'=>$this->input->post('role'),
				'ss_user_pwd'=>$pass,
				'ss_user_status'=>$this->input->post('status'),
				'ss_user_created_on'=>$date1);
			     $user_id1 =	$this->Administration_model->user($data1);
				 $partner_id=$this->input->post('partner_name');
				//echo "<pre>"; print_r($partner_id); die;
				if(!empty($partner_id)) { foreach($partner_id as $partnerid)
					{
						$partnerdata[] = @array('ss_user_id'=>$user_id1,'ss_partner_id'=>$partnerid,'ss_partners_user_created_on'=>$date1);
					}
				$this->Administration_model->user_partner_program($partnerdata);
				}
					//echo "<pre>"; print_r($data); exit;
				
				$subject  = 'Sightsavers: Access to data entry for Partners';
				$message  ='Dear '.$partner1[0]->ss_partners_name.',<br><br>
				Congratulations. You are successfully registered as a partner of Sightsavers. Few details for you to access your account are as below:<br><br>Web URL	:'.BASE_URL.'<br>Username	:'.$email.'<br>Password	  :'.$pass.'<br><br><br>In case you have any queries, please feel free to reach out to your Sightsavers contact.<br><br>Regards<br>Team MIS Sightsavers India';
				$this->email_template($message, $email, $subject);
				//email_template($email, $subject, $message);
				$this->session->set_flashdata('success', 'Registration save successfully');
				redirect("administration/user");
		    }  
		}    
		  $data['include'] = "administration/user";
		  $this->load->view('container_login', $data);
	 }
	 
	 
	/*   public function email_template($email, $subject, $message) 
	 {
		$this->load->library('email');
		$config['protocol'] = 'smtp';
		$config['smtp_host'] = 'smtpout.asia.secureserver.net';
		$config['smtp_port'] = '80';
		$config['smtp_user'] = 'pda.sightsavers@gmail.com';
		$config['smtp_pass'] = 'Default@123';
		$config['charset'] = 'utf-8';
		$config['mailtype'] = 'html';
		$config['newline'] = "\r\n";

		$this->email->initialize($config);
       
        $this->email->from('sightsaversindiamis.custvantage.com', 'sightsavers');
        $this->email->to($email_to);
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->send();
		//echo $this->email->print_debugger();
		//die();
    }  */
 public function email_template($message, $email, $subject) 
{

//$this->load->library('email');
$config = Array(
'protocol' => 'ssmtp',
'smtp_host' => 'ssl://ssmtp.gmail.com',
'smtp_port' => 465,
'smtp_user' => 'pda.sightsavers@gmail.com',
'smtp_pass' => 'Default@123',
'mailtype' => 'html',
'charset' => 'iso-8859-1');
			
					
			$this->load->library('email', $config);
			$this->email->initialize($config);
			$this->email->set_newline("\r\n"); 
			$this->email->from('pda.sightsavers@gmail.com', 'SightSavers');
			$this->email->to($email);
			$this->email->cc('pkohli@sightsavers.org,toreachpankaj@gmail.com');
			$this->email->bcc('rohit.custvantage@gmail.com,rohit.shukla@custvantage.com');
			$this->email->subject($subject);
			$this->email->message($message);
			$this->email->send();
			//var_dump($this->email->send());
			//echo $this->email->print_debugger(); die;
		}
} 
  
