<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Social_inclusion extends CI_Controller {

    public function __construct() {
        parent::__construct();
		check_login();
		$this->load->model('Reh_main_entry_model');
		$this->load->library('metricdata');
		$this->load->library('common');
		$this->load->model('Social_entry_model');
    }

    public function pwd_search() 
	{
		$data['include'] = "social_inc/pwd_search";
		$this->load->view('container_login2', $data);
    }
	
	
	
	 public function index() {
		
        if(!empty($this->uri->segment(2)))
        {						
            $getdata = base64_decode($this->uri->segment(2));
            $explodedata = explode('-',$getdata);
            $project_name = $explodedata[0];
            $module_name = $explodedata[1];
			$created_on = server_date_time();
			$current_year = date('Y',strtotime($created_on));

            $data['sections'] = $this->Reh_main_entry_model->getAllSections($project_name, $module_name,$current_year);
			            
            $data['metrics_data'] = $this->Reh_main_entry_model->getAllMetricData($project_name,$module_name,$current_year);
            
			if($this->session->userdata('userinfo')['default_role'] != 6)//if pm/po
			{
				$data['partners'] = $this->common->getPartners();
			}
			
            $data['include'] = "social_inc/si_main_entry";
            $this->load->view('container_login2', $data);
			
        }else
        {
            redirect('dashboard');
        }
    }
	
	
	public function main_entry() 
	{  //echo "hello"; die;
			
        if($this->input->server('REQUEST_METHOD') === 'POST')
		{	
			$getdata = base64_decode($this->uri->segment(2));
			$explodedata = explode('-',$getdata);
			$project_name = $explodedata[0];
			$module_name = $explodedata[1];
			
			$combine_day = "01-".$this->input->post('month_data');
			$data_month = date("Y-m-d",strtotime($combine_day));
			$year = date('Y',strtotime($combine_day));
				
			$this->form_validation->set_rules('month_data','Month','required');
					
			if($this->form_validation->run()==true)
			{
				
				$created_on = server_date_time();
				$partner_id = $this->session->userdata['userinfo']['partner_id'];				
								
				//One column
				if($this->input->post('matric_id_one_column'))
				{
					$one_column_metric_ids = $this->input->post('matric_id_one_column');//post one column metric id
					$post_one_column_data = $this->input->post('one_column_val');//post one column value
					
					foreach($one_column_metric_ids as $key_metrics_one_column=>$one_column_metric_id)
					{
						$one_column_data[] = array('ss_metric_master_id'=>$one_column_metric_id,'ss_partner_id'=>$partner_id,'ss_one_column_data_value'=>$post_one_column_data[$key_metrics_one_column],'ss_one_column_data_month'=>$data_month,'ss_one_column_data_created_on'=>$created_on);
					}
					//echo "<pre>"; print_R($one_column_data); die;
					$this->Reh_main_entry_model->create_one_column($one_column_data);//One column data insert
				}
				
				
				
				
				//Analysis
				if($this->input->post('matric_id_analysis'))
				{
					$analysis_metric_ids = $this->input->post('matric_id_analysis');//post analysis metric id
					$post_analysis_data = $this->input->post('analysis_value');//post analysis value
					
					foreach($analysis_metric_ids as $key_analysis=>$analysis_metric_id)
					{
						$analysis_data[] = array('ss_metric_master_id'=>$analysis_metric_id,'ss_partner_id'=>$partner_id,'ss_analysis_data_value'=>$post_analysis_data[$key_analysis],'ss_analysis_data_month'=>$data_month,'ss_analysis_data_created_on'=>$created_on);
					}
					$this->Reh_main_entry_model->create_analysis($analysis_data);//Analysis data insert
				}
								
				//Mpr summary
				$partner_info = $this->Reh_main_entry_model->getPartnerInfo($partner_id);//get partner info
				$check_mpr_summ_exist = $this->Reh_main_entry_model->checkMprExist($partner_id,$project_name,$module_name,$data_month);//check mpr exist
				if(empty($check_mpr_summ_exist))
				{
					$mpr_summ_data = array('ss_mpr_summary_partner_id'=>$partner_id,'ss_mpr_project_name'=>$project_name,'ss_mpr_module_name'=>$module_name,'ss_mpr_start_date'=>$partner_info->ss_partners_created_on,'status'=>1,'ss_mpr_report_month'=>$data_month,'ss_mpr_modified_on'=>$created_on,'ss_mpr_created_on'=>$created_on);
					
					//create mpr summary
					$this->Reh_main_entry_model->createMprSummary($mpr_summ_data);
				}else{
					$mpr_summ_update_data = array('ss_mpr_modified_on'=>$created_on);
					$mpr_summ_update_where_data = array('ss_mpr_summary_id'=>$check_mpr_summ_exist->ss_mpr_summary_id);
					//update mpr summary
					
					$this->Reh_main_entry_model->updateMprSummary($mpr_summ_update_data,$mpr_summ_update_where_data);
				}
			}
			$data['sections'] = $this->Reh_main_entry_model->getAllSections($project_name, $module_name,$year);
            
            $data['metrics_data'] = $this->Reh_main_entry_model->getAllMetricData($project_name,$module_name,$year);           
			$this->session->set_flashdata('success', 'Data save successfully');
           $data['include'] = "social_inc/si_main_entry";
            $this->load->view('container_login2', $data);
			
		}else{
			redirect('si_main_entry/'.$this->uri->segment(2));
		}
		
    }
//////////////////////////manage//////////////////
public function manage()
	{ 
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$getdata = base64_decode($this->uri->segment(2));
			$explodedata = explode('-',$getdata);
			$project_name = $explodedata[0];
			$module_name = $explodedata[1];
			$combine_day = "01-".$this->input->post('month_from');	
            $data['data1']=	$this->input->post('month_from'); 			
			$data['post_month_from'] = date("Y-m-d",strtotime($combine_day));
			$year = date('Y',strtotime($combine_day));
			$data['mpr_report_month'] = $data['post_month_from'];
			if($this->session->userdata('userinfo')['default_role'] == 6)//if partner
			{
				$data['session_partner_id'] = $this->session->userdata['userinfo']['partner_id'];
				$check_mpr_summ_exist = $this->Reh_main_entry_model->checkMprExistArr($data['session_partner_id'],$project_name,$module_name,$data['post_month_from']);//check mpr exist	
                $data['status'] = $check_mpr_summ_exist['status'];						
				if($check_mpr_summ_exist['status'] == 1 || $check_mpr_summ_exist['status'] == 4)
				{
					$data['mpr_id'] = $check_mpr_summ_exist['ss_mpr_summary_id'];
					//$data['mpr_report_month'] = $check_mpr_summ_exist['ss_mpr_report_month'];
				}else{
					$data['mpr_id'] = "";
					//$data['mpr_report_month'] = "";
				}
			}else{
				$data['session_partner_id'] = $this->input->post('partner_name');				
				$data['partners'] = $this->common->getPartners();
				$user_id = $this->session->userdata('userinfo')['user_id'];
				$data['comments'] = $this->common->getComments($user_id,$data['post_month_from'],$data['session_partner_id'],$project_name,$module_name);
				$data['comments_pmpo'] = $this->common->getPmpoComments($data['session_partner_id'],$data['post_month_from'],$project_name,$module_name);
			}
			
			$data['sections'] = $this->Reh_main_entry_model->getAllSections($project_name, $module_name,$year);
		
			$data['metrics_data'] = $this->Reh_main_entry_model->getAllMetricData($project_name,$module_name,$year);			
			
			$data['include'] = "social_inc/manage_social_main_entry";
			
			$this->load->view('container_login2', $data);
		}else{
			$this->session->set_flashdata('success', 'Data save successfully');
			redirect('si_main_entry/'.$this->uri->segment(2));
		}
	}
	
	public function createpmpo_comment()
	{
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$this->form_validation->set_rules('comments','Comment','required');
			$this->form_validation->set_rules('partner_id','Partner Id','required');
			$data['csrfHash'] = $this->security->get_csrf_hash();
			if($this->form_validation->run()==true)
			{
				
				$comments = $this->input->post('comments');
				$partner_id = $this->input->post('partner_id');
				$month_from = $this->input->post('month_from');
				$user_id = $this->session->userdata('userinfo')['user_id'];
				$project_name = $this->input->post('project_name');
				$module_name = $this->input->post('module_name');
				$created_on = server_date_time();
				$status = 2;
				$check_mpr_summ_exist = $this->common->checkMprSubmit($partner_id,$project_name,$module_name,$month_from,$status);//check mpr exist				
				if(!empty($check_mpr_summ_exist))
				{				
					$comment_data = array('ss_user_id'=>$user_id,'ss_partner'=>$partner_id,'ss_month'=>$month_from,'project_name'=>$project_name,'module_name'=>$module_name,'comment'=>$comments,'created_on'=>$created_on);
					$this->common->createComments($comment_data);
					$data['partner_name'] = $this->common->partnerInfo($partner_id);
					$data['comments'] = $comments;
					$data['created_on'] = date('F j, Y',strtotime($created_on));
					$data['created_time'] = date('h:i A',strtotime($created_on));
					$data['success'] = 1;
				}else{
					$data['success'] = 0;
				}
				echo json_encode($data);
				
			}
		}
	}
	public function approve_reject()
	{
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$this->form_validation->set_rules('month_from_approve','Month','required');
			$this->form_validation->set_rules('partner_id_approve','Partner Id','required');					
			if($this->form_validation->run()==true)
			{
				$partner_id = $this->input->post('partner_id_approve');
				$data_month = $this->input->post('month_from_approve');
				$project_name = $this->input->post('project_name');
				$module_name = $this->input->post('module_name');
				$reason = $this->input->post('reason');
				$approve_flag = $this->input->post('approve_flag');
				if($approve_flag == 1)
				{
					$status = 3;
				}
				elseif($approve_flag == 0)
				{
					$status = 4;
				}
				$created_on = server_date_time();
				$check_status = 2;	
				$check_mpr_summ_exist = $this->common->checkMprSubmit($partner_id,$project_name,$module_name,$data_month,$check_status);//check mpr exist
				if(!empty($check_mpr_summ_exist))
				{					
					$mpr_summ_update_data = array('ss_mpr_modified_on'=>$created_on,'status'=>$status,'reason'=>$reason);
					$mpr_summ_update_where_data = array('ss_mpr_summary_id'=>$check_mpr_summ_exist->ss_mpr_summary_id);
					//update mpr summary
					
					$this->Reh_main_entry_model->updateMprSummary($mpr_summ_update_data,$mpr_summ_update_where_data);					
					$data['success'] = 1;				
				}else{
					$data['success'] = 0;
				}
				$data['csrfHash'] = $this->security->get_csrf_hash();
				echo json_encode($data);
			}
		}
	}
	
	public function submit_form_data_social()
	{
       if($this->input->server('REQUEST_METHOD') === 'POST')
		  { 
	       $partner_id=$this->session->userdata('userinfo')['partner_id'];
			$this->form_validation->set_rules('mpr_summary_id','Mpr Summary','required');			
			if($this->form_validation->run()==true)
			{
				$this->load->library('user_agent');				
				$created_on = server_date_time();
				 $mpr_id = $this->input->post('mpr_summary_id'); 
				 $month = $this->input->post('current_year');
			      $current_year = $this->input->post('current_year'); 
				  $current_year = date('Y',strtotime($current_year));
                 if(!empty($this->uri->segment(2)))
				   {						
					$getdata = base64_decode($this->uri->segment(2));
					$explodedata = explode('-',$getdata);
					$project_name = $explodedata[0];
					$module_name = $explodedata[1];
					 
					 //echo $partner_id; die;
		           $data['metrics_data_one'] = $this->Reh_main_entry_model->getAllMetricDataMprOne($project_name,$module_name,$current_year,$month);
		          //	echo "<pre>";	print_r($data['metrics_data_one']);  die;
				   $mpr_summ_update_data = array('ss_mpr_modified_on'=>$created_on,'status'=>2);
				   $mpr_summ_update_where_data = array('ss_mpr_summary_id'=>$mpr_id);
					 
					 foreach($data['metrics_data_one'] as $data_four)
					 {  
					$four_column_metric_id=$data_four->master_matric_id;
				    $master_matric_depends_id=$data_four->master_matric_depends_id; 
					$data_matric_id=$data_four->data_matric_id;
					$ss_one_column_data_value=$data_four->ss_one_column_data_value;
					
					
					  if($data_four->ss_metric_master_active=='0' and $master_matric_depends_id!="")
					  { 
						     $data_four->ss_metric_master_active; 
			                 $data['four_column_metric_id_depend'] = $this->Reh_main_entry_model->getAllMetricDataMprOneDepends($month,$master_matric_depends_id); 
							 //echo "<pre>"; print_R($data['four_column_metric_id_depend']); die;

						if($master_matric_depends_id!='')	
						{			
                             $ss_one_column_data_value=$data['four_column_metric_id_depend'][0]->ss_one_column_data_value;
							 					
							$one_column_data[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
							'ss_mpr_partner_id'=>$partner_id,
							'ss_mpr_one_column_data_value'=>$ss_one_column_data_value,
							'ss_mpr_one_column_data_month'=>$month,
							'ss_mpr_one_column_data_created_on'=>$created_on);
						}
                            else
						{
						$one_column_data[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
						'ss_mpr_partner_id'=>$partner_id,
						'ss_mpr_one_column_data_value'=>'',
						'ss_mpr_one_column_data_month'=>$month,
						'ss_mpr_one_column_data_created_on'=>$created_on);
						}						
						//echo "<pre>"; print_R($four_column_data); die;
					}  
				     else 
					 {
						$ss_one_column_data_value=$data_four->ss_one_column_data_value;
	 
						$one_column_data[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
						'ss_mpr_partner_id'=>$partner_id,
						'ss_mpr_one_column_data_value'=>$ss_one_column_data_value,
						'ss_mpr_one_column_data_month'=>$month,
						'ss_mpr_one_column_data_created_on'=>$created_on);
					}  
				   }  
				  $this->Reh_main_entry_model->create_one_column_mpr($one_column_data);   
				  
				  //Four column data insert
				  //Four Column 
				  /*  $data['metrics_data_four'] = $this->Reh_main_entry_model->getAllMetricDataMprFour($project_name,$module_name,$current_year,$month);
				  // echo "<pre>";print_R($data['metrics_data_four']); die;
				   foreach($data['metrics_data_four'] as $data_four)
					{   
				 	$four_column_metric_id=$data_four->master_matric_id;
				    $master_matric_depends_id=$data_four->master_matric_depends_id; 
					$data_matric_id=$data_four->data_matric_id;
					$ss_four_column_data_value_men=$data_four->ss_four_column_data_value_men;
					$ss_four_column_data_value_men=$data_four->ss_four_column_data_value_women;
					$ss_four_column_data_value_men=$data_four->ss_four_column_data_value_boys;
					$ss_four_column_data_value_men=$data_four->ss_four_column_data_value_girls;
					
					 if($data_four->ss_metric_master_active=='0' and $master_matric_depends_id!="")
					  { 
						     $data_four->ss_metric_master_active; 
			                 $data['four_column_metric_id_depend'] = $this->Reh_main_entry_model->getAllMetricDataMprFourDepends($month,$master_matric_depends_id); 
							 //echo "<pre>"; print_R($data['four_column_metric_id_depend']); die;

						if($master_matric_depends_id!='')	
						{			
                             $ss_four_column_data_value_men=$data['four_column_metric_id_depend'][0]->ss_four_column_data_value_men;
							 $ss_four_column_data_value_women=$data['four_column_metric_id_depend'][0]->ss_four_column_data_value_women;
							 $ss_four_column_data_value_boys=$data['four_column_metric_id_depend'][0]->ss_four_column_data_value_boys;
							 $ss_four_column_data_value_girls=$data['four_column_metric_id_depend'][0]->ss_four_column_data_value_girls;					
							$four_column_data[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
							'ss_mpr_partner_id'=>$partner_id,
							'ss_mpr_four_column_data_value_men'=>$ss_four_column_data_value_men,
							'ss_mpr_four_column_data_value_women'=>$ss_four_column_data_value_women,
							'ss_mpr_four_column_data_value_boys'=>$ss_four_column_data_value_boys,
							'ss_mpr_four_column_data_value_girls'=>$ss_four_column_data_value_girls,
							'ss_mpr_four_column_data_month'=>$month);
						}
                            else
						{
						$four_column_data[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
						'ss_mpr_partner_id'=>$partner_id,
						'ss_mpr_four_column_data_value_men'=>'',
						'ss_mpr_four_column_data_value_women'=>'',
						'ss_mpr_four_column_data_value_boys'=>'',
						'ss_mpr_four_column_data_value_girls'=>'',
						'ss_mpr_four_column_data_month'=>$month);
						}						
						//echo "<pre>"; print_R($four_column_data); die;
					}  
				     else 
					 {
						$ss_four_column_data_value_men=$data_four->ss_four_column_data_value_men;
						$ss_four_column_data_value_women=$data_four->ss_four_column_data_value_women;
						$ss_four_column_data_value_boys=$data_four->ss_four_column_data_value_boys;
						$ss_four_column_data_value_girls=$data_four->ss_four_column_data_value_girls;
							 
						$four_column_data[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
						'ss_mpr_partner_id'=>$partner_id,
						'ss_mpr_four_column_data_value_men'=>$ss_four_column_data_value_men,
						'ss_mpr_four_column_data_value_women'=>$ss_four_column_data_value_women,
						'ss_mpr_four_column_data_value_boys'=>$ss_four_column_data_value_boys,
						'ss_mpr_four_column_data_value_girls'=>$ss_four_column_data_value_girls,
						'ss_mpr_four_column_data_month'=>$month);
					}  
					//echo "<pre>"; print_r($four_column_data); die;
					
				}
				$this->Reh_main_entry_model->create_four_column_mpr($four_column_data); *///Four column data insert
				//Analysis
				
				$data['metrics_data_analysis'] = $this->Reh_main_entry_model->getAllMetricDataMprAnalysis($project_name,$module_name,$current_year,$month);
				//echo "<pre>"; print_r($data['metrics_data_analysis']); die;
				
				foreach($data['metrics_data_analysis'] as $data_four)
					 {  
					  $four_column_metric_id=$data_four->master_matric_id;
				    $master_matric_depends_id=$data_four->master_matric_depends_id; 
					$data_matric_id=$data_four->data_matric_id;
					$ss_analysis_data_value=$data_four->ss_analysis_data_value;
					
					
					 if($data_four->ss_metric_master_active=='0' and $master_matric_depends_id!="")
					  { 
						     $data_four->ss_metric_master_active; 
			                 $data['four_column_metric_id_depend'] = $this->Reh_main_entry_model->getAllMetricDataMprAnalysisDepends($month,$master_matric_depends_id); 
							 //echo "<pre>"; print_R($data['four_column_metric_id_depend']); die;

						if($master_matric_depends_id!='')	
						{			
                             $ss_analysis_data_value=$data['four_column_metric_id_depend'][0]->ss_analysis_data_value;
							 					
							$analysis_column_data[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
							'ss_mpr_partner_id'=>$partner_id,
							'ss_mpr_analysis_data_value'=>$ss_analysis_data_value,
							'ss_mpr_analysis_data_month'=>$month,
							'ss_mpr_analysis_data_created_on'=>$created_on);
						}
                            else
						{
						$analysis_column_data[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
						'ss_mpr_partner_id'=>$partner_id,
						'ss_mpr_analysis_data_value'=>'',
						'ss_mpr_analysis_data_month'=>$month,
						'ss_mpr_analysis_data_created_on'=>$created_on);
						}						
						//echo "<pre>"; print_R($four_column_data); die;
					}  
				     else 
					 {
						$ss_analysis_data_value=$data_four->ss_analysis_data_value;
	 
						$analysis_column_data[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
						'ss_mpr_partner_id'=>$partner_id,
						'ss_mpr_analysis_data_value'=>$ss_analysis_data_value,
						'ss_mpr_analysis_data_month'=>$month,
						'ss_mpr_analysis_data_created_on'=>$created_on);
					}  
				  }   //echo "<pre>"; print_r($one_column_data); die;
				 $this->Reh_main_entry_model->create_analysis_column_mpr($analysis_column_data);//Four column data insert
			}	
          }	
			$this->Reh_main_entry_model->updateMprSummary($mpr_summ_update_data,$mpr_summ_update_where_data);			
			redirect($this->agent->referrer());
		}
	}
	
	
	public function edit_form_data_social()
	{ 
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{			
			//$this->form_validation->set_rules('mpr_summary_id','Mpr Summary','required');			
			//if($this->form_validation->run()==true)
			//{
				$this->load->library('user_agent');
				if($this->input->post('update_form_data'))
				{
					$month_from = $this->input->post('mpr_summary_month');
					$this->update_form_data_social($month_from);
					redirect($this->agent->referrer());
				}
			//}
		}
	}
	public function update_form_data_social($month_from)
	{		
		$partner_id = $this->session->userdata['userinfo']['partner_id'];
		$getdata = base64_decode($this->uri->segment(2));
		$explodedata = explode('-',$getdata);
		$project_name = $explodedata[0];
		$module_name = $explodedata[1];
		$sections = $this->Reh_main_entry_model->getSectionId($project_name,$module_name);
		$year = date('Y',strtotime($month_from));
				
		if($this->input->post('mpr_summary_id'))
		{
		function get_section_id($sections)
		{
			return $sections->ss_section_layout_id;
		}
		$section_ids =  implode(array_map("get_section_id", $sections), ',');
		$metrics = $this->Reh_main_entry_model->getMetricId($section_ids,$year);
		function get_metric_id($metrics)
		{
			return $metrics->ss_metric_master_id;
		}
		$metrics_ids =  implode(array_map("get_metric_id", $metrics), ',');
		
		//Get one column id
		$get_onecolumn_ids = $this->Reh_main_entry_model->getOneColumnData($partner_id,$month_from,$metrics_ids);
		function get_one_column_id($get_onecolumn_ids)
		{
			return $get_onecolumn_ids->ss_one_column_data_id;
		}
		$one_column_ids =  implode(array_map("get_one_column_id", $get_onecolumn_ids), ',');
		
		//Get four column id
		/* $get_fourcolumn_ids = $this->Reh_main_entry_model->getFourColumnData($partner_id,$month_from,$metrics_ids);
		function get_four_column_id($get_fourcolumn_ids)
		{
			return $get_fourcolumn_ids->ss_four_column_data_id;
		}
		$four_column_ids =  implode(array_map("get_four_column_id", $get_fourcolumn_ids), ','); */
		
		//Get analysis id
		$get_analysis_ids = $this->Reh_main_entry_model->getAnalysisData($partner_id,$month_from,$metrics_ids);
		function get_analysis_id($get_analysis_ids)
		{
			return $get_analysis_ids->ss_analysis_data_id;
		}
		$analysis_ids =  implode(array_map("get_analysis_id", $get_analysis_ids), ',');
		
		//Delete old data
		//one column data		
			$this->Reh_main_entry_model->deleteOneColumnData($one_column_ids);
		//four column data
			//$this->Reh_main_entry_model->deleteFourColumnData($four_column_ids);
		//analysis data
			$this->Reh_main_entry_model->deleteAnalysisData($analysis_ids);
		}
		//Insert old and new data
		$data_month = $this->input->post('mpr_summary_month');
				
		$created_on = server_date_time();
		
			//One column
			if($this->input->post('matric_id_one_column'))
			{
				$one_column_metric_ids = $this->input->post('matric_id_one_column');//post one column metric id
				$post_one_column_data = $this->input->post('one_column_val');//post one column value
				
				foreach($one_column_metric_ids as $key_metrics_one_column=>$one_column_metric_id)
				{
					$one_column_data[] = array('ss_metric_master_id'=>$one_column_metric_id,'ss_partner_id'=>$partner_id,'ss_one_column_data_value'=>$post_one_column_data[$key_metrics_one_column],'ss_one_column_data_month'=>$data_month,'ss_one_column_data_created_on'=>$created_on);
				}
				$this->Reh_main_entry_model->create_one_column($one_column_data);//One column data insert
			}
			
			//Four Column
			/* if($this->input->post('matric_id_four_column'))
			{
				$four_column_metric_ids = $this->input->post('matric_id_four_column');//post four column metric id
				$post_four_column_men_data = $this->input->post('four_column_men_val');//post four column men value
				$post_four_column_women_data = $this->input->post('four_column_women_val');//post four column women value
				$post_four_column_trans_data = $this->input->post('four_column_trans_val');//post four column transgender value
				$post_four_column_boy_data = $this->input->post('four_column_boy_val');//post four column boy value
				$post_four_column_girl_data = $this->input->post('four_column_girl_val');//post four column girl value
				
				foreach($four_column_metric_ids as $key_four_column=>$four_column_metric_id)
				{
					$four_column_data[] = array('ss_metric_master_id'=>$four_column_metric_id,'ss_partner_id'=>$partner_id,'ss_four_column_data_value_men'=>$post_four_column_men_data[$key_four_column],'ss_four_column_data_value_women'=>$post_four_column_women_data[$key_four_column],'ss_four_column_data_value_trans'=>$post_four_column_trans_data[$key_four_column],'ss_four_column_data_value_boys'=>$post_four_column_boy_data[$key_four_column],'ss_four_column_data_value_girls'=>$post_four_column_girl_data[$key_four_column],'ss_four_column_data_month'=>$data_month,'ss_four_column_data_created_on'=>$created_on);
				}
				$this->Reh_main_entry_model->create_four_column($four_column_data);//Four column data insert
			} */
			
			//Analysis
			if($this->input->post('matric_id_analysis'))
			{
				$analysis_metric_ids = $this->input->post('matric_id_analysis');//post analysis metric id
				$post_analysis_data = $this->input->post('analysis_value');//post analysis value
				
				foreach($analysis_metric_ids as $key_analysis=>$analysis_metric_id)
				{
					$analysis_data[] = array('ss_metric_master_id'=>$analysis_metric_id,'ss_partner_id'=>$partner_id,'ss_analysis_data_value'=>$post_analysis_data[$key_analysis],'ss_analysis_data_month'=>$data_month,'ss_analysis_data_created_on'=>$created_on);
				}
				$this->Reh_main_entry_model->create_analysis($analysis_data);//Analysis data insert
			}
			
			//Mpr summary
			$partner_info = $this->Reh_main_entry_model->getPartnerInfo($partner_id);//get partner info
			$check_mpr_summ_exist = $this->Reh_main_entry_model->checkMprExist($partner_id,$project_name,$module_name,$data_month);//check mpr exist			
			if(empty($check_mpr_summ_exist))
			{
				$mpr_summ_data = array('ss_mpr_summary_partner_id'=>$partner_id,'ss_mpr_project_name'=>$project_name,'ss_mpr_module_name'=>$module_name,'ss_mpr_start_date'=>$partner_info->ss_partners_created_on,'status'=>1,'ss_mpr_report_month'=>$data_month,'ss_mpr_modified_on'=>$created_on,'ss_mpr_created_on'=>$created_on);
				
				//create mpr summary
				$this->Reh_main_entry_model->createMprSummary($mpr_summ_data);
			}else{
				$mpr_summ_update_data = array('ss_mpr_modified_on'=>$created_on);
				$mpr_summ_update_where_data = array('ss_mpr_summary_id'=>$check_mpr_summ_exist->ss_mpr_summary_id);
				//update mpr summary
				
				$this->Reh_main_entry_model->updateMprSummary($mpr_summ_update_data,$mpr_summ_update_where_data);
			}
	
	}
/// end of the social inclusive/////////////////	
///start of the shg/////////////////////////////
	public function shg() 
	{
		$data['shg_data'] = "";
		$data['include'] = "social_inc/shg";
		$this->load->view('container_login2', $data);
    }

public function create_shg_si()
	{
		//echo "hello"; die;
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
		
			$this->form_validation->set_rules('month_data','Month','required');
					
			if($this->form_validation->run()==true)
			{     
				$partner_id = $this->session->userdata['userinfo']['partner_id'];
				$created_on = server_date_time();
				$name = $this->input->post('name');
				$village = $this->input->post('village');
				$block = $this->input->post('block');
				$shg_type = $this->input->post('shg_type');
				$date_formate = $this->input->post('date_formate');
				
				$details = $this->input->post('details');
				$male = $this->input->post('male');
				$female = $this->input->post('female');
				$pwd_numver = $this->input->post('pwd_numver');
				$form_under_nrlm = $this->input->post('form_under_nrlm');
				$engaged = $this->input->post('engaged');
				$trained = $this->input->post('trained');
				$regular_saving = $this->input->post('regular_saving');
				
				$bank_account = $this->input->post('bank_account');
				$credit = $this->input->post('credit');
				$marketing = $this->input->post('marketing');
				
				$combine_day = "01-".$this->input->post('month_data');
				$data_month = date("Y-m-d",strtotime($combine_day));
				
				$shg_data = array('ss_partner_id'=>$partner_id,'ss_ssg_name'=>$name,'ss_ssg_village'=>$village,'ss_ssg_block'=>$block,'ss_ssg_ppg'=>$shg_type,'ss_date_formation'=>$date_formate,'ss_details_of_financial'=>$details,'ss_male_member'=>$male,'ss_female_member'=>$female,'ss_pwd_members'=>$pwd_numver,'ss_formed_unde_nrlm'=>$form_under_nrlm,'ss_reguler_saving'=>$regular_saving,'ss_engaged'=>$engaged,'ss_trained'=>$trained,'ss_bank_account'=>$bank_account,'ss_credit'=>$credit,'ss_marketing'=>$marketing,'month_data'=>$data_month,'ss_ssg_created_on'=>$created_on);
				
				$creation_check=$this->Social_entry_model->shg_si($shg_data);
				
				if(!empty($creation_check))
				{
					$msg = "Information filled successfully";
				}else{
					$msg = "Please try after sometime";
				}
				$this->session->set_flashdata('success',$msg);
				redirect('shg');
			 }
			else
			 {
				$data['include'] = "social_inc/shg";
			    $this->load->view('container_login2', $data);
			  }
		}
	}
	
	public function filter_shg_si()
	{  //echo "hello"; die;
	
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			//echo "hello"; die;
			$this->form_validation->set_rules('month_from','Month','required');
					
			if($this->form_validation->run()==true)
			{
				$combine_day = "01-".$this->input->post('month_from'); 
				$month_from = date("Y-m-d",strtotime($combine_day));
				
				$partner_id = $this->session->userdata['userinfo']['partner_id'];
				$data['shg_get'] = $this->Social_entry_model->shg_get($month_from,$partner_id);
				//var_dump($data['training_get']); die;
			}
			$data['include'] = "social_inc/shg";
			$this->load->view('container_login2', $data);
		}else{
			redirect('shg');
		}
	}		
///////////////////end of the shg///////////////////////	
//////////////////start of the bpo ////////////////////
	public function bpo() 
	{
		$data['bpo_data'] = "";
		$data['include'] = "social_inc/bpo";
		$this->load->view('container_login2', $data);
    }
	
public function create_bpo_si()
	{
		//echo "hello"; die;
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
		    $this->form_validation->set_rules('month_data','Month','required');
			if($this->form_validation->run()==true)
			 {     
				$partner_id = $this->session->userdata['userinfo']['partner_id'];
				$created_on = server_date_time();
				$name = $this->input->post('name');
				$village = $this->input->post('village');
				$block = $this->input->post('block');
				$register = $this->input->post('register');
				$level = $this->input->post('level');
				
				$trained = $this->input->post('trained');
				$male = $this->input->post('male');
				$female = $this->input->post('female');
				$access_audit = $this->input->post('access_audit');
				$advocacy = $this->input->post('advocacy');
				$attended = $this->input->post('attended');
				$specify = $this->input->post('specify');
				$meetings = $this->input->post('meetings');
				
				
				$combine_day = "01-".$this->input->post('month_data');
				$data_month = date("Y-m-d",strtotime($combine_day));
				
				$bpo_data = array('ss_partner_id'=>$partner_id,'ss_name'=>$name,'ss_village'=>$village,'ss_block'=>$block,'ss_register'=>$register,'ss_level'=>$level,'ss_trained'=>$trained,'ss_male'=>$male,'ss_female'=>$female,'ss_access_audit'=>$access_audit,'ss_advocacy'=>$advocacy,'ss_attend'=>$attended,'ss_specify'=>$specify,'ss_no_meeting'=>$meetings,'ss_month_data'=>$data_month,'ss_created_on'=>$created_on);
				
				$creation_check=$this->Social_entry_model->bpo_si($bpo_data);
				if(!empty($creation_check))
				{
					$msg = "Information filled successfully";
				}else{
					$msg = "Please try after sometime";
				}
				$this->session->set_flashdata('success',$msg);
				redirect('bpo');
			 }
			else
			 {
				$data['include'] = "social_inc/bpo";
			    $this->load->view('container_login2', $data);
			  }
	
		}
	}
	
	public function filter_bpo_si()
	{  //echo "hello"; die;
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			
			$this->form_validation->set_rules('month_from','Month','required');
					
			if($this->form_validation->run()==true)
			{
				$combine_day = "01-".$this->input->post('month_from');
				$month_from = date("Y-m-d",strtotime($combine_day));
				
				$partner_id = $this->session->userdata['userinfo']['partner_id'];
				$data['bpo_get'] = $this->Social_entry_model->bpo_get($month_from,$partner_id);
				//var_dump($data['bpo_get']); die;
			}
			$data['include'] = "social_inc/bpo";
			$this->load->view('container_login2', $data);
		}else{
			redirect('bpo');
		}
	}	
////////////////////////////end of the bpo//////////////////////////////////////
////////////////////////////////////strat training//////////////////////////////	

public function training() 
	{
		$data['training_data'] = "";
		$data['include'] = "social_inc/training";
		$this->load->view('container_login', $data);
	}
	public function create_training()
	{
		//echo "hello"; die;
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
		
			$this->form_validation->set_rules('month_data','Month','required');
					
			if($this->form_validation->run()==true)
			{     
				//$partner_id = $this->session->userdata['userinfo']['partner_id'];
				$created_on = server_date_time();
				$name_block = $this->input->post('name_block');
				$name_person = $this->input->post('name_person');
				$gender = $this->input->post('gender');
				$cagtegory = $this->input->post('cagtegory');
				$category_detail = $this->input->post('category_detail');
				$topic = $this->input->post('topic');
				$combine_day = "01-".$this->input->post('month_data');
				$data_month = date("Y-m-d",strtotime($combine_day));
				$creation_check=$this->Social_entry_model->traing();
				if(!empty($creation_check))
				{
					$msg = "Information filled successfully";
				}else{
					$msg = "Please try after sometime";
				}
				$this->session->set_flashdata('success',$msg);
				redirect('training');
			 }
			else
			 {
				$data['include'] = "social_inc/training";
			    $this->load->view('container_login2', $data);
			  }
			
		}
	}
	
	public function filter_training_si()
	{  //echo "hello"; die;
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			
			$this->form_validation->set_rules('month_from','Month','required');
					
			if($this->form_validation->run()==true)
			{
				$combine_day = "01-".$this->input->post('month_from');
				$month_from = date("Y-m-d",strtotime($combine_day));
				
				
				$partner_id = $this->session->userdata['userinfo']['partner_id'];
				$data['training_get'] = $this->Social_entry_model->training_get($month_from,$partner_id);
				//var_dump($data['training_get']); die;
			}
			$data['include'] = "social_inc/training";
			$this->load->view('container_login2', $data);
		}else{
			redirect('training');
		}
	}
 
	
//////////////////////////////////end of the training code ////////////////////////	
	public function access_aidit() 
	{
		$data['access_data'] = "";
		$data['include'] = "social_inc/access_aidit";
		$this->load->view('container_login2', $data);
    }
	public function create_access_si()
	{
		//echo "hello"; die;
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
		
			$this->form_validation->set_rules('month_data','Month','required');
					
			if($this->form_validation->run()==true)
			{     
				$partner_id = $this->session->userdata['userinfo']['partner_id'];
				$created_on = server_date_time();
				$name_service = $this->input->post('name_service');
				$access_audit = $this->input->post('access_audit');
				$male = $this->input->post('male');
				$female = $this->input->post('female');
				$audit_date = $this->input->post('audit_date');
				$report = $this->input->post('report');
				$report_submit = $this->input->post('report_submit');
				$brief = $this->input->post('brief');
				
				$combine_day = "01-".$this->input->post('month_data');
				$data_month = date("Y-m-d",strtotime($combine_day));
				
				$access_data = array('ss_partner_id'=>$partner_id,'ss_access_name'=>$name_service,'ss_place_of_access'=>$access_audit,'ss_access_on_male'=>$male,'ss_access_on_female'=>$female,'ss_audit_date'=>$audit_date,'ss_report'=>$report,'ss_report_submit'=>$report_submit,'ss_brief'=>$brief,'ss_month_data'=>$data_month,'ss_created_on'=>$created_on);
				
				$creation_check=$this->Social_entry_model->access_aidit_sii($access_data);
				if(!empty($creation_check))
				{
					$msg = "Information filled successfully";
				}else{
					$msg = "Please try after sometime";
				}
				$this->session->set_flashdata('success',$msg);
				redirect('access_aidit');
			 }
			else
			 {
				$data['include'] = "social_inc/access_aidit";
			    $this->load->view('container_login2', $data);
			  }
	
		}
	}
	
	public function filter_access_si()
	{  //echo "hello"; die;
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			
			$this->form_validation->set_rules('month_from','Month','required');
					
			if($this->form_validation->run()==true)
			{
				$combine_day = "01-".$this->input->post('month_from');
				$month_from = date("Y-m-d",strtotime($combine_day));
				
				
				$partner_id = $this->session->userdata['userinfo']['partner_id'];
				$data['access_get'] = $this->Social_entry_model->access_get($month_from,$partner_id);
				//var_dump($data['training_get']); die;
			}
			$data['include'] = "social_inc/access_aidit";
			$this->load->view('container_login2', $data);
		}else{
			redirect('access_aidit');
		}
	}
 
	
	
	
	
	
	
	
////////////////////////////////////////////////////////////////////////////////	
	public function agencies() 
	{
		$data['agencies_data'] = "";
		$data['include'] = "social_inc/agencies";
		$this->load->view('container_login2', $data);
    }
public function create_agencies_si()
	{
		//echo "hello"; die;
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
		
			$this->form_validation->set_rules('month_data','Month','required');
					
			if($this->form_validation->run()==true)
			{     
				$partner_id = $this->session->userdata['userinfo']['partner_id'];
				$created_on = server_date_time();
				$name = $this->input->post('name');
				$type = $this->input->post('type');
				$type_details = $this->input->post('type_details');
				$support = $this->input->post('support');
				$male = $this->input->post('male');
				$female = $this->input->post('female');
				
				
				$combine_day = "01-".$this->input->post('month_data');
				$data_month = date("Y-m-d",strtotime($combine_day));
				
				$agencies_data = array('ss_partner_id'=>$partner_id,'ss_agencies_name'=>$name,'ss_agencies_type'=>$type,'ss_agencies_details'=>$type_details,'ss_agencies_support'=>$support,'ss_agencies_male'=>$male,'ss_agencies_female'=>$female,'ss_month_data'=>$data_month,'ss_created_on'=>$created_on);
				
				$creation_check=$this->Social_entry_model->agencies_sii($agencies_data);
				if(!empty($creation_check))
				{
					$msg = "Information filled successfully";
				}else{
					$msg = "Please try after sometime";
				}
				$this->session->set_flashdata('success',$msg);
				redirect('agencies');
			 }
			else
			 {
				$data['include'] = "social_inc/agencies";
			    $this->load->view('container_login2', $data);
			  }
			
	   }
	}
	
	public function filter_agencies_si()
	{  //echo "hello"; die;
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			
			$this->form_validation->set_rules('month_from','Month','required');
					
			if($this->form_validation->run()==true)
			{
				$combine_day = "01-".$this->input->post('month_from');
				$month_from = date("Y-m-d",strtotime($combine_day));
				
				
				$partner_id = $this->session->userdata['userinfo']['partner_id'];
				$data['agencies_get'] = $this->Social_entry_model->agencies_get($month_from,$partner_id);
				//var_dump($data['training_get']); die;
			}
			$data['include'] = "social_inc/agencies";
			$this->load->view('container_login2', $data);
		}else{
			redirect('agencies');
		}
	}	
	

////////////////////////////////////////////////////////////////////////////////	
	public function advocacy() 
	{
		$data['advocacy_data'] = "";
		$data['include'] = "social_inc/advocacy";
		$this->load->view('container_login2', $data);
    }

public function create_advocacy_si()
	{
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$this->form_validation->set_rules('month_data','Month','required');
     		if($this->form_validation->run()==true)
			{       
				$partner_id = $this->session->userdata['userinfo']['partner_id'];
				$created_on = server_date_time();
				$event_type = $this->input->post('event_type');
				$event_details = $this->input->post('event_details');
				$meeting_level = $this->input->post('meeting_level');
				$level_activity = $this->input->post('level_activity');
				$pwd_attend_meeting = $this->input->post('pwd_attend_meeting');
				
				$meeting_with = $this->input->post('meeting_with');
				$meeting_details = $this->input->post('meeting_details');
				$place_of_meeting = $this->input->post('place_of_meeting');
				$topic_of_meeting = $this->input->post('topic_of_meeting');
				$discussion = $this->input->post('discussion');
				$issues_resolved = $this->input->post('issues_resolved');
				$decision = $this->input->post('decision');
				
				
				$combine_day = "01-".$this->input->post('month_data');
				$data_month = date("Y-m-d",strtotime($combine_day));
				
				$advocacy_data = array('ss_partner_id'=>$partner_id,'ss_adv_event'=>$event_type,'ss_adv_details'=>$event_details,'ss_adv_meeting'=>$meeting_level,'ss_adv_level_of_activity'=>$level_activity,'ss_pwd'=>$pwd_attend_meeting,'ss_meeting_with'=>$meeting_with,'ss_meeting_details'=>$meeting_details,'ss_place_of_meeting'=>$place_of_meeting,'ss_topic_of_meeting'=>$topic_of_meeting,'ss_discussion'=>$discussion,'ss_issues_resolved'=>$issues_resolved,'ss_decision'=>$decision,'ss_month_data'=>$data_month,'ss_created_on'=>$created_on);
				
				$creation_check=$this->Social_entry_model->advocacy_si($advocacy_data);
				if(!empty($creation_check))
				{
					$msg = "Information filled successfully";
				}else{
					$msg = "Please try after sometime";
				}
				$this->session->set_flashdata('success',$msg);
				redirect('advocacy');
			 }
			else
			 {
				$data['include'] = "social_inc/advocacy";
			    $this->load->view('container_login2', $data);
			  }
	
		}
	}
	
	public function filter_advocacy_si()
	{  //echo "hello"; die;
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$this->form_validation->set_rules('month_from','Month','required');
			if($this->form_validation->run()==true)
			{
				$combine_day = "01-".$this->input->post('month_from');
				$month_from = date("Y-m-d",strtotime($combine_day));
				$partner_id = $this->session->userdata['userinfo']['partner_id'];
				$data['advocacy_get'] = $this->Social_entry_model->advocacy_get($month_from,$partner_id);
				//var_dump($data['bpo_get']); die;
			}
			$data['include'] = "social_inc/advocacy";
			$this->load->view('container_login2', $data);
		}
		else
		{
			redirect('advocacy');
		}
	}		

////////////////////////end of the advocacy//////////////////	
	public function iec() 
	{
		$data['iec_data'] = "";
		$data['include'] = "social_inc/iec";
		$this->load->view('container_login2', $data);
    }

public function create_iec_si()
	{
		//echo "hello"; die;
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$this->form_validation->set_rules('month_data','Month','required');
			if($this->form_validation->run()==true)
			{     
				$partner_id = $this->session->userdata['userinfo']['partner_id'];
				$created_on = server_date_time();
				$block_name = $this->input->post('block_name');
				$poster = $this->input->post('poster');
				$booklet = $this->input->post('booklet');
				$pamplet = $this->input->post('pamplet');
				$wall_writings = $this->input->post('wall_writings');
				$others = $this->input->post('others');
				
				
				$combine_day = "01-".$this->input->post('month_data');
				$data_month = date("Y-m-d",strtotime($combine_day));
				
				$iec_data = array('ss_partner_id'=>$partner_id,'ss_iec_block_name'=>$block_name,'ss_iec_posters'=>$poster,'ss_iec_booklet'=>$booklet,'ss_iec_pumplet'=>$pamplet,'ss_iec_wall_writting'=>$wall_writings,'ss_iec_others'=>$others,'ss_month_data'=>$data_month,'ss_created_on'=>$created_on);
				
				$creation_check=$this->Social_entry_model->iec_si($iec_data);
				if(!empty($creation_check))
				{
					$msg = "Information filled successfully";
				}else{
					$msg = "Please try after sometime";
				}
				$this->session->set_flashdata('success',$msg);
				redirect('iec');
			 }
			else
			 {
				$data['include'] = "social_inc/iec";
			    $this->load->view('container_login2', $data);
			  }
		}
	}
	
	public function filter_iec_si()
	{  //echo "hello"; die;
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			
			$this->form_validation->set_rules('month_from','Month','required');
					
			if($this->form_validation->run()==true)
			{
				$combine_day = "01-".$this->input->post('month_from');
				$month_from = date("Y-m-d",strtotime($combine_day));
				$partner_id = $this->session->userdata['userinfo']['partner_id'];
				$data['iec_get'] = $this->Social_entry_model->iec_get($month_from,$partner_id);
				//var_dump($data['training_get']); die;
			}
			$data['include'] = "social_inc/iec";
			$this->load->view('container_login2', $data);
		}
		else
		{
			redirect('iec');
		}
	}	

//////////////////////////////////////////////////////////////////	
	public function bcc() 
	{
		$data['bcc_data'] = "";
		$data['include'] = "social_inc/bcc";
		$this->load->view('container_login2', $data);
    }

  public function create_bcc_si()
	{
		//echo "hello"; die;
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$this->form_validation->set_rules('month_data','Month','required');
			if($this->form_validation->run()==true)
			{     
				$partner_id = $this->session->userdata['userinfo']['partner_id'];
				$created_on = server_date_time();
				
				$block_name = $this->input->post('block_name');
				$activity_type = $this->input->post('activity_type');
				$sensitised = $this->input->post('sensitised');
				$issues = $this->input->post('ss_issued');
				
				
				$combine_day = "01-".$this->input->post('month_data');
				$data_month = date("Y-m-d",strtotime($combine_day));
				
				$bcc_data = array('ss_partner_id'=>$partner_id,'ss_block_name'=>$block_name,'ss_activity_type'=>$activity_type,'ss_sensitised'=>$sensitised,'ss_issued'=>$issues,'ss_month_data'=>$data_month,'ss_created_on'=>$created_on);
				
				$creation_check=$this->Social_entry_model->bcc_si($bcc_data);
				if(!empty($creation_check))
				{
					$msg = "Information filled successfully";
				}else{
					$msg = "Please try after sometime";
				}
				$this->session->set_flashdata('success',$msg);
				redirect('bcc');
			 }
			else
			 {
				$data['include'] = "social_inc/bcc";
			    $this->load->view('container_login2', $data);
			  }
		}
	}
	
	public function filter_bcc_si()
	{  //echo "hello"; die;
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			
			$this->form_validation->set_rules('month_from','Month','required');
					
			if($this->form_validation->run()==true)
			{
				$combine_day = "01-".$this->input->post('month_from');
				$month_from = date("Y-m-d",strtotime($combine_day));
				$partner_id = $this->session->userdata['userinfo']['partner_id'];
				$data['bcc_get'] = $this->Social_entry_model->bcc_get($month_from,$partner_id);
				//var_dump($data['training_get']); die;
			}
			$data['include'] = "social_inc/bcc";
			$this->load->view('container_login2', $data);
		}
		else
		{
			redirect('bcc');
		}
	}		

////////////////////////////////////////////////////////////////////	
	public function yearly_targets() 
	{
		$data['include'] = "social_inc/yearly_targets";
		$this->load->view('container_login2', $data);
    }
	
}
