<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller {
	
	 public function __construct()
	 {
        parent::__construct();
        $this->load->model('Administration_model');
     }

    public function index() 
	{
		if($this->input->server('REQUEST_METHOD') === 'POST')
		    {      
				 $currentDate = date("d");
				 if($currentDate >= 6) {
					  
					 $_SESSION["month_date"] = date("m-Y");
					 
					 } else {
						 
						$_SESSION["month_date"] =  date("m-Y", strtotime("first day of previous month"));
						 }
				 $email=$this->input->post('email');
			     $passward=$this->input->post('passward'); 
			    $data1=$this->Administration_model->login($email ,$passward);
				
				if(!empty($data1)) 
				{
			    $server_date = server_date_time(); 
				//$user_id = $this->user_model->get_user_id_from_username($email);
				$session_data = array('user_id'=>$data1->ss_user_id,'partner_id'=>$data1->ss_partner_id,'partner_name'=>$data1->ss_partners_name,'email'=>$data1->ss_user_email_id,'fname'=>$data1->ss_user_fname,'reh'=>$data1->ss_partners_project_reh,'ueh'=>$data1->ss_partners_project_ueh,'social'=>$data1->ss_partners_project_si,'education'=>$data1->ss_partners_project_ie,'default_role'=>$data1->ss_user_role,'role_name'=>$data1->ss_role_name,'district'=>$data1->ss_district_name,'state_name'=>$data1->ss_states_name);
				//echo '<pre>'; print_R($session_data); die;
				$this->session->set_userdata('userinfo',$session_data);
				$server_date = server_date_time();
				$server_date1 =date('m-Y',strtotime($server_date));
				$this->session->set_userdata('monthdata',$server_date1);				 
				if($this->session->userdata('userinfo')['default_role']==6)
				{
			       redirect("dashboard");
				}
				else{
					redirect("dashboard_speedometer");				
				}
			    }
				else
				{
				unset($_SESSION["month_date"]);
				$this->session->set_flashdata('success', 'The username or password you have entered is incorrect');	
				redirect("login");
			   }
		}
		
		if(!$this->session->userdata('userinfo'))
		{
        $data['include'] = "login";
        $this->load->view('container', $data);
		} else {
				if($this->session->userdata('userinfo')['default_role']==6)
				{
			       redirect("dashboard");
				}
				else{
					redirect("dashboard_report");
				}
			   }
	   }		
	
			public function logout()
			{
				$this->session->sess_destroy();
				$this->session->set_flashdata('success', 'Lagout successfully');
				redirect('login');
			}

}
