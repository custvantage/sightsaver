<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Reh_vc extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Reh_vc_model');
		$this->load->model('Reh_main_entry_model');
		check_login();
    }

    public function index() {
		$data['vc_data'] = "";
		$data['include'] = "reh/vc";
		$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		$this->load->view('container_login_dis', $data);
    }
	public function create()
	{    
	$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$this->form_validation->set_rules('month_data','Month','required');
			if($this->form_validation->run()==true)
			{
				$partner_id = $_SESSION["partner_id"];
				$created_on = server_date_time();
				
				$id_ajax1=$this->input->post('id_ajax');
				$vc_name = $this->input->post('vc_name');
				$vc_open = $this->input->post('vc_open');
				$vc_support = $this->input->post('vc_support');
				
				$subsidized_male = $this->input->post('subsidized_male');
				$subsidized_female = $this->input->post('subsidized_female');
				$subsidized_trans = $this->input->post('subsidized_trans');
				$subsidized_boy = $this->input->post('subsidized_boy');
				$subsidized_girl = $this->input->post('subsidized_girl');
				
				$spectacles_male = $this->input->post('spectacles_male');
				$spectacles_female = $this->input->post('spectacles_female');
				$spectacles_trans = $this->input->post('spectacles_trans');
				$spectacles_boy = $this->input->post('spectacles_boy');
				$spectacles_girl = $this->input->post('spectacles_girl');
				
				$screen_male = $this->input->post('screen_male');
				$screen_female = $this->input->post('screen_female');
				$screen_trans = $this->input->post('screen_trans');
				$screen_boy = $this->input->post('screen_boy');
				$screen_girl = $this->input->post('screen_girl');
				
				$refra_male = $this->input->post('refra_male');
				$refra_female = $this->input->post('refra_female');
				$refra_trans = $this->input->post('refra_trans');
				$refra_boy = $this->input->post('refra_boy');
				$refra_girl = $this->input->post('refra_girl');
				
				$refra_pres_male = $this->input->post('refra_pres_male');
				$refra_pres_female = $this->input->post('refra_pres_female');
				$refra_pres_trans = $this->input->post('refra_pres_trans');
				$refra_pres_boy = $this->input->post('refra_pres_boy');
				$refra_pres_girl = $this->input->post('refra_pres_girl');
				
				$refra_disp_male = $this->input->post('refra_disp_male');
				$refra_disp_female = $this->input->post('refra_disp_female');
				$refra_disp_trans = $this->input->post('refra_disp_trans');
				$refra_disp_boy = $this->input->post('refra_disp_boy');
				$refra_disp_girl = $this->input->post('refra_disp_girl');
				
				$refer_refer_male = $this->input->post('refer_refer_male');
				$refer_refer_female = $this->input->post('refer_refer_female');
				$refer_refer_trans = $this->input->post('refer_refer_trans');
				$refer_refer_boy = $this->input->post('refer_refer_boy');
				$refer_refer_girl = $this->input->post('refer_refer_girl');
				
				$refer_cat_male = $this->input->post('refer_cat_male');
				$refer_cat_female = $this->input->post('refer_cat_female');
				$refer_cat_trans = $this->input->post('refer_cat_trans');
				$refer_cat_boy = $this->input->post('refer_cat_boy');
				$refer_cat_girl = $this->input->post('refer_cat_girl');				
				
				$combine_day = "01-".$this->input->post('month_data');
				$data_month = date("Y-m-d",strtotime($combine_day));
				
				if($this->input->post('id_ajax')=='')
				{
				$vc_data = array('ss_partner_id'=>$partner_id,'ss_vc_name'=>$vc_name,'ss_vc_open'=>$vc_open,'ss_vc_support'=>$vc_support,'ss_subsidized_male'=>$subsidized_male,'ss_subsidized_female'=>$subsidized_female,'ss_subsidized_trans'=>$subsidized_trans,'ss_subsidized_boy'=>$subsidized_boy,'ss_subsidized_girl'=>$subsidized_girl,
				
				'ss_spectacles_male'=>$spectacles_male,'ss_spectacles_female'=>$spectacles_female,'ss_spectacles_trans'=>$spectacles_trans,'ss_spectacles_boy'=>$spectacles_boy,'ss_spectacles_girl'=>$spectacles_girl,
				
				'ss_screen_male'=>$screen_male,'ss_screen_female'=>$screen_female,'ss_screen_trans'=>$screen_trans,'ss_screen_boy'=>$screen_boy,'ss_screen_girl'=>$screen_girl,				
				'ss_refra_male'=>$refra_male,'ss_refra_female'=>$refra_female,'ss_refra_trans'=>$refra_trans,'ss_refra_boy'=>$refra_boy,'ss_refra_girl'=>$refra_girl,
				
				'ss_refra_pres_male'=>$refra_pres_male,'ss_refra_pres_female'=>$refra_pres_female,'ss_refra_pres_trans'=>$refra_pres_trans,'ss_refra_pres_boy'=>$refra_pres_boy,'ss_refra_pres_girl'=>$refra_pres_girl,
				
				'ss_refra_disp_male'=>$refra_disp_male,'ss_refra_disp_female'=>$refra_disp_female,'ss_refra_disp_trans'=>$refra_disp_trans,'ss_refra_disp_boy'=>$refra_disp_boy,'ss_refra_disp_girl'=>$refra_disp_girl,
				
				'ss_refer_refer_male'=>$refer_refer_male,'ss_refer_refer_female'=>$refer_refer_female,'ss_refer_refer_trans'=>$refer_refer_trans,'ss_refer_refer_boy'=>$refer_refer_boy,'ss_refer_refer_girl'=>$refer_refer_girl,
				
				'ss_refer_cat_male'=>$refer_cat_male,'ss_refer_cat_female'=>$refer_cat_female,'ss_refer_cat_trans'=>$refer_cat_trans,'ss_refer_cat_boy'=>$refer_cat_boy,'ss_refer_cat_girl'=>$refer_cat_girl,
				
				'ss_reh_vc_month'=>$data_month,'ss_reh_vc_created_on'=>$created_on);
				
				$creation_check = $this->Reh_vc_model->createVc($vc_data);
				}
				else{
				$update_vc_data = array('ss_partner_id'=>$partner_id,'ss_vc_name'=>$vc_name,'ss_vc_open'=>$vc_open,'ss_vc_support'=>$vc_support,'ss_subsidized_male'=>$subsidized_male,'ss_subsidized_female'=>$subsidized_female,'ss_subsidized_trans'=>$subsidized_trans,'ss_subsidized_boy'=>$subsidized_boy,'ss_subsidized_girl'=>$subsidized_girl,
				
				'ss_spectacles_male'=>$spectacles_male,'ss_spectacles_female'=>$spectacles_female,'ss_spectacles_trans'=>$spectacles_trans,'ss_spectacles_boy'=>$spectacles_boy,'ss_spectacles_girl'=>$spectacles_girl,
				
				'ss_screen_male'=>$screen_male,'ss_screen_female'=>$screen_female,'ss_screen_trans'=>$screen_trans,'ss_screen_boy'=>$screen_boy,'ss_screen_girl'=>$screen_girl,				
				'ss_refra_male'=>$refra_male,'ss_refra_female'=>$refra_female,'ss_refra_trans'=>$refra_trans,'ss_refra_boy'=>$refra_boy,'ss_refra_girl'=>$refra_girl,
				
				'ss_refra_pres_male'=>$refra_pres_male,'ss_refra_pres_female'=>$refra_pres_female,'ss_refra_pres_trans'=>$refra_pres_trans,'ss_refra_pres_boy'=>$refra_pres_boy,'ss_refra_pres_girl'=>$refra_pres_girl,
				
				'ss_refra_disp_male'=>$refra_disp_male,'ss_refra_disp_female'=>$refra_disp_female,'ss_refra_disp_trans'=>$refra_disp_trans,'ss_refra_disp_boy'=>$refra_disp_boy,'ss_refra_disp_girl'=>$refra_disp_girl,
				
				'ss_refer_refer_male'=>$refer_refer_male,'ss_refer_refer_female'=>$refer_refer_female,'ss_refer_refer_trans'=>$refer_refer_trans,'ss_refer_refer_boy'=>$refer_refer_boy,'ss_refer_refer_girl'=>$refer_refer_girl,
				
				'ss_refer_cat_male'=>$refer_cat_male,'ss_refer_cat_female'=>$refer_cat_female,'ss_refer_cat_trans'=>$refer_cat_trans,'ss_refer_cat_boy'=>$refer_cat_boy,'ss_refer_cat_girl'=>$refer_cat_girl,
				'ss_reh_vc_month'=>$data_month,'ss_reh_vc_created_on'=>$created_on);
				
				$creation_check = $this->Reh_vc_model->updateVc($update_vc_data,$id_ajax1);	
				}
				
				$this->session->set_flashdata('success','Your data is successfully saved');
				//redirect('reh_vision_center');
				redirect('filter_vc_reh?month_from='.$_SESSION["month_date"]);
			}else{
				$data['include'] = "reh/vc";
			$this->load->view('container_login_dis', $data);
			}
		}
	}
	public function filter_vc_data()
	{
		$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		if($this->input->server('REQUEST_METHOD') === 'GET' and $this->input->get('month_from')!=null and preg_match('/^[0-9]{2}-[0-9]{4}\z/', $this->input->get('month_from')))
		{   
			//$this->form_validation->set_rules('month_from','Month','required');
			//if($this->form_validation->run()==true)
			//{
				$combine_day = "01-".$this->input->get('month_from');
				$month_from = date("Y-m-d",strtotime($combine_day));
				$partner_id = $_SESSION["partner_id"];
				$data['vc_data'] = $this->Reh_vc_model->getVc($month_from,$partner_id);
			//}
			$data['include'] = "reh/vc";
			$this->load->view('container_login_dis', $data);
			//exit();
		}else{
			redirect('reh_vision_center');
		}
	}
	
	public function delete_vc_reh()
	{ 
		if(!empty($this->uri->segment(2)))
        {
	     $id = base64_decode($this->uri->segment(2));
		 $this->Reh_vc_model->deleteHehVc($id);
	     redirect($_SERVER['HTTP_REFERER']);
		}
	}
	
	public function edit_vc_reh()
	{ 
	     $id = base64_decode($this->input->post('id'));
		 $data=$this->Reh_vc_model->editFetchData($id);
		 if(!empty($data))
		 {
			 $data2 = json_encode($data[0]);
			 echo $data2."$$$".$this->security->get_csrf_hash();
		 }
		 else
		 {
			 echo 0;
		 }
	}
	
	
	
	
	
  
}
