<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Reh_vhsnc extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Reh_vhsnc_model');
		$this->load->model('Reh_main_entry_model');
		check_login();
    }

    public function index() {
		$data['vhsnc_data'] = "";
		$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		$data['include'] = "reh/vhsnc";
		$this->load->view('container_login_dis', $data);
    }
	public function create()
	{  $userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
		
			$this->form_validation->set_rules('month_data','Month','required');
					
			if($this->form_validation->run()==true)
			{
				$partner_id = $_SESSION["partner_id"];
				$created_on = server_date_time();
				
				$ajax_id1 = $this->input->post('id_ajax');
				$vhsnc_name = $this->input->post('vhsnc_name');
				$meeting_date = date("Y-m-d",strtotime($this->input->post('meeting_date')));
				$meeting_attend_by = $this->input->post('meeting_attend_by_vhsnc');
				$issue_discuss = $this->input->post('issue_discuss');
				
				$combine_day = "01-".$this->input->post('month_data');
				$data_month = date("Y-m-d",strtotime($combine_day));
				
				if($this->input->post('id_ajax')=='')
				{
				$bcc_data = array('ss_partner_id'=>$partner_id,'ss_reh_vhsnc_name'=>$vhsnc_name,'ss_reh_vhsnc_meeting_date'=>$meeting_date,'ss_reh_vhsnc_meeting_attend_by'=>$meeting_attend_by,'ss_reh_vhsnc_issue_discussed'=>$issue_discuss,'ss_reh_vhsnc_month'=>$data_month,'ss_reh_vhsnc_created_on'=>$created_on);
				
				$creation_check = $this->Reh_vhsnc_model->createVhsnc($bcc_data);
				}
				else{
					$update_data = array('ss_partner_id'=>$partner_id,'ss_reh_vhsnc_name'=>$vhsnc_name,'ss_reh_vhsnc_meeting_date'=>$meeting_date,'ss_reh_vhsnc_meeting_attend_by'=>$meeting_attend_by,'ss_reh_vhsnc_issue_discussed'=>$issue_discuss,'ss_reh_vhsnc_month'=>$data_month,'ss_reh_vhsnc_created_on'=>$created_on);
				
				$creation_check = $this->Reh_vhsnc_model->updateVhsnc($update_data,$ajax_id1);
					
				}
				
				if(!empty($creation_check))
				{
					$msg = "Information filled successfully";
				}else{
					$msg = "Please try after sometime";
				}
				$this->session->set_flashdata('success',$msg);
				redirect('filter_vhsnc_reh?month_from='.$_SESSION["month_date"]);
			}else{
				$data['include'] = "reh/vhsnc";
			$this->load->view('container_login_dis', $data);
			}
		}
	}
	public function filter_vhsnc_data()
	{    $userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		if($this->input->server('REQUEST_METHOD') === 'GET' and $this->input->get('month_from')!=null and preg_match('/^[0-9]{2}-[0-9]{4}\z/', $this->input->get('month_from')))
		{
			$_SESSION['ABC'] = 1;
			/*$this->form_validation->set_rules('month_from','Month','required');
					
			if($this->form_validation->run()==true)
			{*/
				$combine_day = "01-".$this->input->get('month_from');
				$month_from = date("Y-m-d",strtotime($combine_day));
				
				
				$partner_id = $_SESSION["partner_id"];
				$data['vhsnc_data'] = $this->Reh_vhsnc_model->getVhsnc($month_from,$partner_id);
			//}
			$data['include'] = "reh/vhsnc";
			$this->load->view('container_login_dis', $data);
		}else{
			redirect('reh_vhsnc');
		}
	}
	public function delete_vhsnc_reh()
	{ 
		if(!empty($this->uri->segment(2)))
        {
	     $id = base64_decode($this->uri->segment(2));
		 $this->Reh_vhsnc_model->deleteHehVhsnc($id);
	     redirect($_SERVER['HTTP_REFERER']);
		}
	}
	
	public function edit_vhsnc_reh()
	{ 
	     $id = base64_decode($this->input->post('id'));
		 $data=$this->Reh_vhsnc_model->editFetchData($id);
		 if(!empty($data))
		 {
			 $data2 = json_encode($data[0]);
			 echo $data2."$$$".$this->security->get_csrf_hash();
		 }
		 else
		 {
			 echo 0;
		 }
	}
	
}
