<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Reh_main_entry extends CI_Controller {
    public function __construct() 
	{
        parent::__construct();
		check_login();		
		$this->load->library('session');
        $this->load->model('Reh_main_entry_model');
		$this->load->model('Main_excel_entry_model');
		$this->load->library('metricdata');
		$this->load->library('common');
    }

    public function index() 
	{ 
		$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid); 
        if(!empty($this->uri->segment(2)))
        {						
            $getdata = base64_decode($this->uri->segment(2));
            $explodedata = explode('-',$getdata);
            $project_name = $explodedata[0];
            $module_name = $explodedata[1];
			$created_on = server_date_time();
			$current_year = date('Y',strtotime($created_on));	
			
            $data['sections'] = $this->Reh_main_entry_model->getAllSections($project_name, $module_name,$current_year);    
		    if($this->session->userdata('userinfo')['state_name']=='West Bengal Sunderbans')
	    	{
            $data['metrics_data'] = $this->Reh_main_entry_model->getAllMetricData($project_name,$module_name,$current_year);
			}
			else 
			{
		    $data['metrics_data'] = $this->Reh_main_entry_model->getAllMetricDataSunder($project_name,$module_name,$current_year);
			}
            $this->session->userdata('userinfo')['district'];
			if($this->session->userdata('userinfo')['default_role'] != 6)//if pm/po
			{
				$data['partners'] = $this->common->getPartners();
			}
            $data['include'] = "reh/reh_main_entry";
            $this->load->view('container_login_dis', $data);
        }else
        {
            redirect('dashboard');
        }
    }
    public function create_reh_main_entry()
	{
		$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid); 
        if($this->input->server('REQUEST_METHOD') === 'POST')
		{	
			$getdata = base64_decode($this->uri->segment(2));
			$explodedata = explode('-',$getdata);
			$project_name = $explodedata[0];
			$module_name = $explodedata[1];
			$combine_day = "01-".$this->input->post('month_data');
			$data_month = date("Y-m-d",strtotime($combine_day));
			
			$year = date('Y',strtotime($combine_day));
			
			$this->form_validation->set_rules('month_data','Month','required');
					
			if($this->form_validation->run()==true)
			{
				
				$created_on = server_date_time();
				//$partner_id = $_SESSION["partner_id"];				
			    $partner_id = $_SESSION["partner_id"];
								
				//One column
				if($this->input->post('matric_id_one_column'))
				{
					$one_column_metric_ids = $this->input->post('matric_id_one_column');//post one column metric id
					$post_one_column_data = $this->input->post('one_column_val');//post one column value
					
					foreach($one_column_metric_ids as $key_metrics_one_column=>$one_column_metric_id)
					{
						$one_column_data[] = array('ss_metric_master_id'=>$one_column_metric_id,'ss_partner_id'=>$partner_id,'ss_one_column_data_value'=>$post_one_column_data[$key_metrics_one_column],'ss_one_column_data_month'=>$data_month,'ss_one_column_data_created_on'=>$created_on);
					}
					$this->Reh_main_entry_model->create_one_column($one_column_data);//One column data insert
				}
				
				//Four Column
				if($this->input->post('matric_id_four_column'))
				{
					$four_column_metric_ids = $this->input->post('matric_id_four_column');//post four column metric id
					$post_four_column_men_data = $this->input->post('four_column_men_val');//post four column men value
					$post_four_column_women_data = $this->input->post('four_column_women_val');//post four column women value
					$post_four_column_trans_data = $this->input->post('four_column_trans_val');//post four column transgender value
					$post_four_column_boy_data = $this->input->post('four_column_boy_val');//post four column boy value
					$post_four_column_girl_data = $this->input->post('four_column_girl_val');//post four column girl value
					
					foreach($four_column_metric_ids as $key_four_column=>$four_column_metric_id)
					{
						$four_column_data[] = array('ss_metric_master_id'=>$four_column_metric_id,'ss_partner_id'=>$partner_id,'ss_four_column_data_value_men'=>$post_four_column_men_data[$key_four_column],'ss_four_column_data_value_women'=>$post_four_column_women_data[$key_four_column],'ss_four_column_data_value_trans'=>$post_four_column_trans_data[$key_four_column],'ss_four_column_data_value_boys'=>$post_four_column_boy_data[$key_four_column],'ss_four_column_data_value_girls'=>$post_four_column_girl_data[$key_four_column],'ss_four_column_data_month'=>$data_month,'ss_four_column_data_created_on'=>$created_on);
					}
			  $this->Reh_main_entry_model->create_four_column($four_column_data);//Four column data insert
			  $metrics_data_insert=$this->Reh_main_entry_model->getAllMetricDataInsert($project_name,$module_name,$year);
			 
			 foreach($metrics_data_insert as $key_four_column_insert)
			  {
				$four_column_insert_data[] = array('ss_metric_master_id'=>$key_four_column_insert->ss_metric_master_id,'ss_partner_id'=>$partner_id,'ss_four_column_data_value_men'=>'','ss_four_column_data_value_women'=>'','ss_four_column_data_value_trans'=>'','ss_four_column_data_value_boys'=>'','ss_four_column_data_value_girls'=>'','ss_four_column_data_month'=>$data_month,'ss_four_column_data_created_on'=>$created_on);
				}
			   $this->Reh_main_entry_model->create_four_column($four_column_insert_data);//Four column data insert ended//////////
		   }
				
				//Analysis
				if($this->input->post('matric_id_analysis'))
				{
					$analysis_metric_ids = $this->input->post('matric_id_analysis');//post analysis metric id
					$post_analysis_data = $this->input->post('analysis_value');//post analysis value
					
					foreach($analysis_metric_ids as $key_analysis=>$analysis_metric_id)
					{
						$analysis_data[] = array('ss_metric_master_id'=>$analysis_metric_id,'ss_partner_id'=>$partner_id,'ss_analysis_data_value'=>$post_analysis_data[$key_analysis],'ss_analysis_data_month'=>$data_month,'ss_analysis_data_created_on'=>$created_on);
					}
					$this->Reh_main_entry_model->create_analysis($analysis_data);//Analysis data insert
				}
				
				//Mpr summary
				$partner_info = $this->Reh_main_entry_model->getPartnerInfo($partner_id);//get partner info
				$check_mpr_summ_exist = $this->Reh_main_entry_model->checkMprExist($partner_id,$project_name,$module_name,$data_month);//check mpr exist
				if(empty($check_mpr_summ_exist))
				{
					$mpr_summ_data = array('ss_mpr_summary_partner_id'=>$partner_id,'ss_mpr_project_name'=>$project_name,'ss_mpr_module_name'=>$module_name,'ss_mpr_start_date'=>$partner_info->ss_partners_created_on,'status'=>1,'ss_mpr_report_month'=>$data_month,'ss_mpr_modified_on'=>$created_on,'ss_mpr_created_on'=>$created_on);
					
					//create mpr summary
					$this->Reh_main_entry_model->createMprSummary($mpr_summ_data);
				}else{
					$mpr_summ_update_data = array('ss_mpr_modified_on'=>$created_on);
					$mpr_summ_update_where_data = array('ss_mpr_summary_id'=>$check_mpr_summ_exist->ss_mpr_summary_id);
					//update mpr summary
					
					$this->Reh_main_entry_model->updateMprSummary($mpr_summ_update_data,$mpr_summ_update_where_data);
				}
			}
			$data['sections'] = $this->Reh_main_entry_model->getAllSections($project_name, $module_name,$year);
            
            $data['metrics_data'] = $this->Reh_main_entry_model->getAllMetricData($project_name,$module_name, $year);
			$this->session->set_flashdata('success', 'Your data is successfully saved.');
			
			redirect('manage_reh_main_entry/'.$this->uri->segment(2).'?month_from='.$_SESSION["month_date"]);
           // $data['include'] = "reh/manage_reh_main_entry".$_SESSION["month_date"];
            $this->load->view('container_login_dis', $data);
			
		}else{
			
			 //$this->manage();  
		redirect('reh_main_entry/'.$this->uri->segment(2));
		}
	}
	public function manage()
	{	
	
	    $userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid); 	
		if($this->input->server('REQUEST_METHOD') == 'POST')
		{ 
            $month_from = $this->input->post('month_from');
 
			$getdata = base64_decode($this->uri->segment(2));
			$explodedata = explode('-',$getdata);
			$project_name = @$explodedata[0];
			$module_name = @$explodedata[1];
			
		    $combine_day = "01-".$month_from;	
		 	$data['post_month_from'] = date("Y-m-d",strtotime($combine_day)); 
			 $data['data1']=$month_from; 
			$year = date('Y',strtotime($combine_day));
			
			$data['mpr_report_month'] = $data['post_month_from'];			
			if($this->session->userdata('userinfo')['default_role'] == 6)//if partner
			{
				$data['session_partner_id'] = $_SESSION["partner_id"];
				$check_mpr_summ_exist = $this->Reh_main_entry_model->checkMprExistArr($data['session_partner_id'],$project_name,$module_name,$data['post_month_from']);
				//echo "<pre>"; print_R($check_mpr_summ_exist); die;
				//check mpr exist				
				$data['status'] = $check_mpr_summ_exist['status'];		
				if($check_mpr_summ_exist['status'] == 1 || $check_mpr_summ_exist['status'] == 4)
				{
					$data['mpr_id'] = $check_mpr_summ_exist['ss_mpr_summary_id'];
					//$data['mpr_report_month'] = $check_mpr_summ_exist['ss_mpr_report_month'];
				}else{
					$data['mpr_id'] = "";
					//$data['mpr_report_month'] = "";
				}
			}else{
				$data['session_partner_id'] = $_SESSION["partner_id"];				
				$data['partners'] = $this->common->getPartners();
				$user_id = $this->session->userdata('userinfo')['user_id'];
				$data['comments'] = $this->common->getComments($user_id,$data['post_month_from'],$data['session_partner_id'],$project_name,$module_name);
				$data['comments_pmpo'] = $this->common->getPmpoComments($data['session_partner_id'],$data['post_month_from'],$project_name,$module_name);
			}			
			$data['sections'] = $this->Reh_main_entry_model->getAllSections($project_name, $module_name,$year);
			
			if($this->session->userdata('userinfo')['state_name']=='West Bengal Sunderbans')
	    	{
            $data['metrics_data'] = $this->Reh_main_entry_model->getAllMetricData($project_name,$module_name,$year);
			}
			else 
			{
		    $data['metrics_data'] = $this->Reh_main_entry_model->getAllMetricDataSunder($project_name,$module_name,$year);
			} 
			
			//echo "<pre>"; print_R($data['metrics_data']); die; 
		   
			$data['include'] = "reh/manage_reh_main_entry";
			$this->load->view('container_login_dis', $data);
		}
		else
		{
			 $this->session->set_flashdata('success', 'Your data is successfully saved.');
			redirect('reh_main_entry/'.$this->uri->segment(2));
		}
	}
	public function excelupload_reh() 
	 {
		$getdata = base64_decode($this->uri->segment(2));
		$explodedata = explode('-',$getdata);
		$project_name = $explodedata[0];
		$module_name = $explodedata[1];
			if(!empty($_FILES['upload_publish_file']['name']))
							{
								require_once(EXCELDIR."PHPExcel.php");
								$excel_file= $_FILES['upload_publish_file']['tmp_name'];
								$inputFileName = $excel_file;
									try 
										{
										$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
										} 
									catch(Exception $e)
										{
										die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
										}
                                        require_once(EXCELDIR."PHPExcel/IOFactory.php");

										$objReader = PHPExcel_IOFactory::createReader('Excel2007');
										$inputFileType = 'Excel2007';
										//$inputFileName = 'hello.xlsx';
										$sheetIndex = 0;
										$objReader = PHPExcel_IOFactory::createReader($inputFileType);
										$sheetnames = $objReader->listWorksheetNames($inputFileName);
										$objReader->setLoadSheetsOnly($sheetnames[$sheetIndex]);
										$objPHPExcel = $objReader->load($inputFileName);
										$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
									  $date1=0;
                                        $start=5;
											for ($row = $start; $row <=5; $row++) 
											{									
												$month = $objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue();
												$year = $objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue();
												$month1 = $monthname = date("m", strtotime($month));
									            $date1 = $year."-".$month1."-".'01'; 
											} 	
																				
												$district_cover = $objPHPExcel->getActiveSheet()->getCell('P7')->getValue();
												$under_sightsaver = $objPHPExcel->getActiveSheet()->getCell('P8')->getValue();
												
                                 // start of the four column  /////       
								 $metric_map=$this->Main_excel_entry_model->metric_map_detail($year);
								 if(!empty($metric_map))
								 {
								 foreach($metric_map as $key=>$metric_map)
								 {
									$metric_id=$metric_map->ss_excel_metric_map_mertic_master_id; 
									$metric_ref=$metric_map->ss_excel_metric_map_excel_cell_ref;
									$metric_count=$metric_map->ss_excel_metric_map_field_count;
									$ref = substr("$metric_ref", 1, 2); 
									    							  
								  if($ref!=0 || $ref!="") 
								  {
									 $cal_data[] = array('ss_partner_id'=>$_SESSION["partner_id"],
									'ss_metric_master_id'=>$metric_id,
									'ss_four_column_data_value_men'=>$objPHPExcel->getActiveSheet()->getCell('L'."$ref")->getValue(),
									'ss_four_column_data_value_women'=>$objPHPExcel->getActiveSheet()->getCell('M'."$ref")->getValue(),
									'ss_four_column_data_value_boys'=>$objPHPExcel->getActiveSheet()->getCell('N'."$ref")->getValue(),
									'ss_four_column_data_value_girls'=>$objPHPExcel->getActiveSheet()->getCell('O'."$ref")->getValue(),
									'ss_four_column_data_month'=>$date1,
									'ss_four_column_data_created_on'=>server_date_time()); 	
									} 
								   }	
								} else{
									$cal_data[] = array('ss_partner_id'=>$this->session->userdata('userinfo')['partner_id'],
									'ss_metric_master_id'=>$metric_id,
									'ss_four_column_data_value_men'=>"",
									'ss_four_column_data_value_women'=>"",
									'ss_four_column_data_value_boys'=>"",
									'ss_four_column_data_value_girls'=>"",
									'ss_four_column_data_month'=>$date1,
									'ss_four_column_data_created_on'=>server_date_time());
								}
								
                                // end of the four column data ///
								// start of the one column data  ///
								$metric_map_one=$this->Main_excel_entry_model->metric_map_detail_one_column($year);
							   if(!empty($metric_map_one))
								{
								 foreach($metric_map_one as $key=>$metric_map_one)
								 {
									$metric_id=$metric_map_one->ss_excel_metric_map_mertic_master_id; 
									$metric_ref=$metric_map_one->ss_excel_metric_map_excel_cell_ref;
									$metric_count=$metric_map_one->ss_excel_metric_map_field_count;
									$ref = substr("$metric_ref", 1, 3); 
									    							  
								  if($ref!=0 || $ref!="") 
								  {
									 $cal_one_data[] = array('ss_partner_id'=>$_SESSION["partner_id"],
									'ss_metric_master_id'=>$metric_id,
									'ss_one_column_data_value'=>$objPHPExcel->getActiveSheet()->getCell('L'."$ref")->getValue(),
									'ss_one_column_data_month'=>$date1,
									'ss_one_column_data_created_on'=>server_date_time()); 	
								   }	
								} 
								}
								else{
									$cal_one_data[] = array('ss_partner_id'=>$_SESSION["partner_id"],
									'ss_metric_master_id'=>$metric_id,
									'ss_one_column_data_value'=>"",
									'ss_one_column_data_month'=>$date1,
									'ss_one_column_data_created_on'=>server_date_time()); 
								}
								// end of the one column  ////
								// start of the analysis///
								$metric_map_analysis=$this->Main_excel_entry_model->metric_map_detail_analysis($year);	
								 if(!empty($metric_map_analysis))
								{					
								 foreach($metric_map_analysis as $key=>$metric_map_analysis)
								 {
									$metric_id=$metric_map_analysis->ss_excel_metric_map_mertic_master_id; 
									$metric_ref=$metric_map_analysis->ss_excel_metric_map_excel_cell_ref;
									$metric_count=$metric_map_analysis->ss_excel_metric_map_field_count;
								//echo $metric_ref; die;
									$ref = substr("$metric_ref", 1, 3);
									  							  
								  if($ref!=0 || $ref!="") 
								  {
									 $analysis_data[] = array('ss_partner_id'=>$_SESSION["partner_id"],
												'ss_metric_master_id'=>$metric_id,
												'ss_analysis_data_value'=>$objPHPExcel->getActiveSheet()->getCell('C'."$ref")->getValue(),
												'ss_analysis_data_month'=>$date1,
												'ss_analysis_data_created_on'=>server_date_time());
								   }	
								}
								}
								else{
									$analysis_data[] = array('ss_partner_id'=>$_SESSION["partner_id"],
												'ss_metric_master_id'=>$metric_id,
												'ss_analysis_data_value'=>"",
												'ss_analysis_data_month'=>$date1,
												'ss_analysis_data_created_on'=>server_date_time());
								}

							$this->Main_excel_entry_model->four_column_excel_upload($cal_data);
							$this->Main_excel_entry_model->analysis_excel_upload($analysis_data);
							$this->Main_excel_entry_model->one_column_excel_upload($cal_one_data);
								
							$partner_info = $this->Reh_main_entry_model->getPartnerInfo($_SESSION["partner_id"]);
							$check_mpr_summ_exist = $this->Reh_main_entry_model->checkMprExist($_SESSION["partner_id"],$project_name,$module_name,$date1);
								if(empty($check_mpr_summ_exist))
								{
								$mpr_summery = array('ss_mpr_summary_partner_id'=>$_SESSION["partner_id"],
								'ss_mpr_project_name'=>$project_name,
								'ss_mpr_module_name'=>$module_name,
								'ss_mpr_report_month'=>$date1,
								'ss_mpr_district_cover_hospital'=>$district_cover,
								'ss_mpr_district_sightsaver'=>$under_sightsaver,
								'status'=>2,
								'ss_mpr_modified_on'=>server_date_time(),
								'ss_mpr_created_on'=>server_date_time()); 
								
								$this->Reh_main_entry_model->createMprSummary($mpr_summery);						
								}else{
									$mpr_summ_update_data = array('ss_mpr_modified_on'=>server_date_time());
									$mpr_summ_update_where_data = array('ss_mpr_summary_id'=>$check_mpr_summ_exist->ss_mpr_summary_id);
									$this->Reh_main_entry_model->updateMprSummary($mpr_summ_update_data,$mpr_summ_update_where_data);
								}
							}

            $data['sections'] = $this->Reh_main_entry_model->getAllSections($project_name, $module_name,$year);
            $data['metrics_data'] = $this->Reh_main_entry_model->getAllMetricData($project_name,$module_name,$year);
			$data['include'] = "reh/reh_main_entry";
			$this->load->view('container_login_dis', $data);
	}
	public function createpmpo_comment()
	{
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$this->form_validation->set_rules('comments','Comment','required');
			$this->form_validation->set_rules('partner_id','Partner Id','required');
			$data['csrfHash'] = $this->security->get_csrf_hash();
			if($this->form_validation->run()==true)
			{
				
				$comments = $this->input->post('comments');
				$partner_id = $this->input->post('partner_id');
				$month_from = $this->input->post('month_from');
				$user_id = $this->session->userdata('userinfo')['user_id'];
				$project_name = $this->input->post('project_name');
				$module_name = $this->input->post('module_name');
				$created_on = server_date_time();
				$status = 2;
				$check_mpr_summ_exist = $this->common->checkMprSubmit($partner_id,$project_name,$module_name,$month_from,$status);//check mpr exist				
				if(!empty($check_mpr_summ_exist))
				{				
					$comment_data = array('ss_user_id'=>$user_id,'ss_partner'=>$partner_id,'ss_month'=>$month_from,'project_name'=>$project_name,'module_name'=>$module_name,'comment'=>$comments,'created_on'=>$created_on);
					$this->common->createComments($comment_data);
					$data['partner_name'] = $this->common->partnerInfo($partner_id);
					$data['comments'] = $comments;
					$data['created_on'] = date('F j, Y',strtotime($created_on));
					$data['created_time'] = date('h:i A',strtotime($created_on));
					$data['success'] = 1;
				}else{
					$data['success'] = 0;
				}
				echo json_encode($data);
				
			}
		}
	}
	public function approve_reject()
	{
		//var_dump($this->input->post()); die;
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$this->form_validation->set_rules('month_from_approve','Month','required');
			$this->form_validation->set_rules('partner_id_approve','Partner Id','required');					
			if($this->form_validation->run()==true)
			{
				$partner_id = $this->input->post('partner_id_approve');
				$data_month = $this->input->post('month_from_approve');
				$project_name = $this->input->post('project_name');
				$module_name = $this->input->post('module_name');
				$reason = $this->input->post('reason');
				$approve_flag = $this->input->post('approve_flag');
				if($approve_flag == 1 && ($this->session->userdata('userinfo')['default_role']==4))
				{
					$status = 3;
					$check_status = 2;
				}
				if($approve_flag == 1 && ($this->session->userdata('userinfo')['default_role']==1))
				{
					$status = 5;
					$check_status = 3;
				}
				
				if($approve_flag == 0)
				{
					$status = 4;
					$check_status = 3;
				}
				if($approve_flag == 0)
				{
					$status = 4;
					$check_status = 2;
				}
				$created_on = server_date_time();
					
				$check_mpr_summ_exist = $this->common->checkMprSubmit($partner_id,$project_name,$module_name,$data_month,$check_status);//check mpr exist
				//var_dump($check_mpr_summ_exist); 
				if(!empty($check_mpr_summ_exist))
				{					
					$mpr_summ_update_data = array('ss_mpr_modified_on'=>$created_on,'ss_mpr_modified_by'=>$this->session->userdata('userinfo')['fname'] ." ".$this->session->userdata('lname')['user_id'],'status'=>$status,'reason'=>$reason);
					$mpr_summ_update_where_data = array('ss_mpr_summary_id'=>$check_mpr_summ_exist->ss_mpr_summary_id);
					//update mpr summary
					
					$this->Reh_main_entry_model->updateMprSummary($mpr_summ_update_data,$mpr_summ_update_where_data);					
					$data['success'] = 1;				
				}else{
					$data['success'] = 0;
				}
				$data['csrfHash'] = $this->security->get_csrf_hash();
				echo json_encode($data);
			}
		}
	}
	
	public function submit_form_data()
	{
       if($this->input->server('REQUEST_METHOD') === 'POST')
		  { 
	     $partner_id=$_SESSION["partner_id"]; 
			$this->form_validation->set_rules('mpr_summary_id','Mpr Summary','required');			
			if($this->form_validation->run()==true)
			{
				$this->load->library('user_agent');				
				$created_on = server_date_time();
				 $mpr_id = $this->input->post('mpr_summary_id'); 
				 $month = $this->input->post('current_year');
			      $current_year = $this->input->post('current_year'); 
				  $current_year = date('Y',strtotime($current_year));
				 
                 if(!empty($this->uri->segment(2)))
				   {						
					$getdata = base64_decode($this->uri->segment(2));
					$explodedata = explode('-',$getdata);
					$project_name = $explodedata[0];
					$module_name = $explodedata[1];
					if($project_name=='rural eye health')
					{
					 $this->db->query("call Generate_REH_MPR($partner_id,'$month')");
					}
					if($project_name=='urban eye health')
					{ //echo "hello"; echo $partner_id; echo $month; die;
					$this->db->query("call Generate_UEH_MPR($partner_id,'$month')");	
					}
					 
					 //echo $partner_id; die;
		   $data['metrics_data_one'] = $this->Reh_main_entry_model->getAllMetricDataMprOne($project_name,$module_name,$current_year,$month);
		   	//echo "<pre>";	print_r($data['metrics_data_one']);  die;
				   $mpr_summ_update_data = array('ss_mpr_modified_on'=>$created_on,'status'=>2);
				   $mpr_summ_update_where_data = array('ss_mpr_summary_id'=>$mpr_id);
					 
					 foreach($data['metrics_data_one'] as $data_four)
					 {  
					  $four_column_metric_id=$data_four->master_matric_id;
				    $master_matric_depends_id=$data_four->master_matric_depends_id; 
					$data_matric_id=$data_four->data_matric_id;
					$ss_one_column_data_value=$data_four->ss_one_column_data_value;
					
					
					  if($data_four->ss_metric_master_active=='0' and $master_matric_depends_id!="")
					  { 
						     $data_four->ss_metric_master_active; 
			                 $data['four_column_metric_id_depend'] = $this->Reh_main_entry_model->getAllMetricDataMprOneDepends($month,$master_matric_depends_id); 
							 //echo "<pre>"; print_R($data['four_column_metric_id_depend']); die;

						if($master_matric_depends_id!='')	
						{			
                            $ss_one_column_data_value=$data['four_column_metric_id_depend'][0]->ss_one_column_data_value;
							$one_column_data[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
							'ss_mpr_partner_id'=>$partner_id,
							'ss_mpr_one_column_data_value'=>$ss_one_column_data_value,
							'ss_mpr_one_column_data_month'=>$month);
						}
                            else
						{
						$one_column_data[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
						'ss_mpr_partner_id'=>$partner_id,
						'ss_mpr_one_column_data_value'=>'',
						'ss_mpr_one_column_data_month'=>$month);
						}						
						//echo "<pre>"; print_R($four_column_data); die;
					}  
				     else 
					 {
						$ss_one_column_data_value=$data_four->ss_one_column_data_value;
	 
						$one_column_data[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
						'ss_mpr_partner_id'=>$partner_id,
						'ss_mpr_one_column_data_value'=>$ss_one_column_data_value,
						'ss_mpr_one_column_data_month'=>$month);
					}  
				   } // echo "<pre>"; print_R($one_column_data); die;
				  $this->Reh_main_entry_model->create_one_column_mpr($one_column_data);   //Four column data insert
				  //Four Column 
				   $data['metrics_data_four'] = $this->Reh_main_entry_model->getAllMetricDataMprFour($project_name,$module_name,$current_year,$month);
				  // echo "<pre>";print_R($data['metrics_data_four']); die;
				   foreach($data['metrics_data_four'] as $data_four)
					{   
				 	$four_column_metric_id=$data_four->master_matric_id;
				    $master_matric_depends_id=$data_four->master_matric_depends_id; 
					$data_matric_id=$data_four->data_matric_id;
					$ss_four_column_data_value_men=$data_four->ss_four_column_data_value_men;
					$ss_four_column_data_value_men=$data_four->ss_four_column_data_value_women;
					$ss_four_column_data_value_men=$data_four->ss_four_column_data_value_boys;
					$ss_four_column_data_value_men=$data_four->ss_four_column_data_value_girls;
					
					 if($data_four->ss_metric_master_active=='0' and $master_matric_depends_id!="")
					  { 
						     $data_four->ss_metric_master_active; 
			                 $data['four_column_metric_id_depend'] = $this->Reh_main_entry_model->getAllMetricDataMprFourDepends($month,$master_matric_depends_id); 
							 //echo "<pre>"; print_R($data['four_column_metric_id_depend']); die;

						if($master_matric_depends_id!='')	
						{			
                             $ss_four_column_data_value_men=$data['four_column_metric_id_depend'][0]->ss_four_column_data_value_men;
							 $ss_four_column_data_value_women=$data['four_column_metric_id_depend'][0]->ss_four_column_data_value_women;
							 $ss_four_column_data_value_boys=$data['four_column_metric_id_depend'][0]->ss_four_column_data_value_boys;
							 $ss_four_column_data_value_girls=$data['four_column_metric_id_depend'][0]->ss_four_column_data_value_girls;					
							$four_column_data[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
							'ss_mpr_partner_id'=>$partner_id,
							'ss_mpr_four_column_data_value_men'=>$ss_four_column_data_value_men,
							'ss_mpr_four_column_data_value_women'=>$ss_four_column_data_value_women,
							'ss_mpr_four_column_data_value_boys'=>$ss_four_column_data_value_boys,
							'ss_mpr_four_column_data_value_girls'=>$ss_four_column_data_value_girls,
							'ss_mpr_four_column_data_month'=>$month);
						}
                            else
						{
						$four_column_data[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
						'ss_mpr_partner_id'=>$partner_id,
						'ss_mpr_four_column_data_value_men'=>'',
						'ss_mpr_four_column_data_value_women'=>'',
						'ss_mpr_four_column_data_value_boys'=>'',
						'ss_mpr_four_column_data_value_girls'=>'',
						'ss_mpr_four_column_data_month'=>$month);
						}						
						//echo "<pre>"; print_R($four_column_data); die;
					}  
				     else 
					 {
						$ss_four_column_data_value_men=$data_four->ss_four_column_data_value_men;
						$ss_four_column_data_value_women=$data_four->ss_four_column_data_value_women;
						$ss_four_column_data_value_boys=$data_four->ss_four_column_data_value_boys;
						$ss_four_column_data_value_girls=$data_four->ss_four_column_data_value_girls;
							 
						$four_column_data[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
						'ss_mpr_partner_id'=>$partner_id,
						'ss_mpr_four_column_data_value_men'=>$ss_four_column_data_value_men,
						'ss_mpr_four_column_data_value_women'=>$ss_four_column_data_value_women,
						'ss_mpr_four_column_data_value_boys'=>$ss_four_column_data_value_boys,
						'ss_mpr_four_column_data_value_girls'=>$ss_four_column_data_value_girls,
						'ss_mpr_four_column_data_month'=>$month);
					}  
					//echo "<pre>"; print_r($four_column_data); die;
					
				}
				$this->Reh_main_entry_model->create_four_column_mpr($four_column_data);
				//Four column data insert ended
				
				//Analysis
				$data['metrics_data_analysis'] = $this->Reh_main_entry_model->getAllMetricDataMprAnalysis($project_name,$module_name,$current_year,$month);
				//echo "<pre>"; print_r($data['metrics_data_analysis']); die;
				
				foreach($data['metrics_data_analysis'] as $data_four)
					 {  
					  $four_column_metric_id=$data_four->master_matric_id;
				    $master_matric_depends_id=$data_four->master_matric_depends_id; 
					$data_matric_id=$data_four->data_matric_id;
					$ss_analysis_data_value=$data_four->ss_analysis_data_value;
					
					
					 if($data_four->ss_metric_master_active=='0' and $master_matric_depends_id!="")
					  { 
						     $data_four->ss_metric_master_active; 
			                 $data['four_column_metric_id_depend'] = $this->Reh_main_entry_model->getAllMetricDataMprAnalysisDepends($month,$master_matric_depends_id); 
							 //echo "<pre>"; print_R($data['four_column_metric_id_depend']); die;

						if($master_matric_depends_id!='')	
						{			
                             $ss_analysis_data_value=$data['four_column_metric_id_depend'][0]->ss_analysis_data_value;
							 					
							$analysis_column_data[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
							'ss_mpr_partner_id'=>$partner_id,
							'ss_mpr_analysis_data_value'=>$ss_analysis_data_value,
							'ss_mpr_analysis_data_month'=>$month);
						}
                            else
						{
						$analysis_column_data[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
						'ss_mpr_partner_id'=>$partner_id,
						'ss_mpr_analysis_data_value'=>'',
						'ss_mpr_analysis_data_month'=>$month);
						}						
						//echo "<pre>"; print_R($four_column_data); die;
					}  
				     else 
					 {
						$ss_analysis_data_value=$data_four->ss_analysis_data_value;
	 
						$analysis_column_data[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
						'ss_mpr_partner_id'=>$partner_id,
						'ss_mpr_analysis_data_value'=>$ss_analysis_data_value,
						'ss_mpr_analysis_data_month'=>$month);
					}  
				  }   //echo "<pre>"; print_r($one_column_data); die;
				 $this->Reh_main_entry_model->create_analysis_column_mpr($analysis_column_data);//Four column data insert
			}	
          }	
			$this->Reh_main_entry_model->updateMprSummary($mpr_summ_update_data,$mpr_summ_update_where_data);	
			$this->session->set_flashdata('success', 'Your data is successfully submitted.');
			//redirect($this->agent->referrer());
		   $data11 = base64_decode($this->uri->segment(2));
			/*if($data11=='rural eye health-main entry')
			{
			redirect('reh_main_entry/'.$this->uri->segment(2));
			}
			else 
			{
			redirect('ueh_main_entry/'.$this->uri->segment(2));	
			}*/
			redirect($this->agent->referrer());
		}
		
	}
	
	
	public function edit_form_data()
	{
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{		 	
			//$this->form_validation->set_rules('mpr_summary_id','Mpr Summary','required');			
			//if($this->form_validation->run()==true)
			//{
				$this->load->library('user_agent');
				if($this->input->post('update_form_data'))
				{
					$month_from = $this->input->post('mpr_summary_month');
					if($this->input->post('request_type')!=null and $this->input->post('request_type') == 'analysis') {
					$this->update_form_data_analysis($month_from);
					}
					else {
						$this->update_form_data($month_from);
						}
					redirect($this->agent->referrer());
				}
			//}
		}
	}
	public function update_form_data($month_from)
	{		
		$partner_id = $_SESSION["partner_id"];
		$mpr_id = $this->input->post('mpr_summary_id');
		$getdata = base64_decode($this->uri->segment(2));
		$explodedata = explode('-',$getdata);
		$project_name = $explodedata[0];
		$module_name = $explodedata[1];
		$sections = $this->Reh_main_entry_model->getSectionId($project_name,$module_name);
		$year = date('Y',strtotime($month_from));
		
		$this->load->library('user_agent');				
		$created_on = server_date_time();
		$month = $month_from;
		//$current_year = $this->input->post('current_year'); 
		$current_year = date('Y',strtotime($month));
				
		if($this->input->post('mpr_summary_id'))
		{
		
					
		function get_section_id($sections)
		{
			return $sections->ss_section_layout_id;
		}
		$section_ids =  implode(array_map("get_section_id", $sections), ',');
		$metrics = $this->Reh_main_entry_model->getMetricId($section_ids,$year);
		function get_metric_id($metrics)
		{
			return $metrics->ss_metric_master_id;
		}
		$metrics_ids =  implode(array_map("get_metric_id", $metrics), ',');
		
		//Get one column id
		$get_onecolumn_ids = $this->Reh_main_entry_model->getOneColumnData($partner_id,$month_from,$metrics_ids);
		function get_one_column_id($get_onecolumn_ids)
		{
			return $get_onecolumn_ids->ss_one_column_data_id;
		}
		$one_column_ids =  implode(array_map("get_one_column_id", $get_onecolumn_ids), ',');
		
		//Get four column id
		$get_fourcolumn_ids = $this->Reh_main_entry_model->getFourColumnData($partner_id,$month_from,$metrics_ids);
		function get_four_column_id($get_fourcolumn_ids)
		{
			return $get_fourcolumn_ids->ss_four_column_data_id;
		}
		$four_column_ids =  implode(array_map("get_four_column_id", $get_fourcolumn_ids), ',');
		
		//Get analysis id
		
		/*$get_analysis_ids = $this->Reh_main_entry_model->getAnalysisData($partner_id,$month_from,$metrics_ids);
		
		function get_analysis_id($get_analysis_ids)
		{
			return $get_analysis_ids->ss_analysis_data_id;
		}
		$analysis_ids =  implode(array_map("get_analysis_id", $get_analysis_ids), ',');*/
		//echo($analysis_ids); exit;
		//Delete old data
		//one column data
		if($one_column_ids!= "") {		
			$this->Reh_main_entry_model->deleteOneColumnData($one_column_ids);
		}
		//four column data
		if($four_column_ids!= "") {
			$this->Reh_main_entry_model->deleteFourColumnData($four_column_ids);
		}
		//analysis data
		/*if($analysis_ids!= "") {
			$this->Reh_main_entry_model->deleteAnalysisData($analysis_ids);
		}*/
			
			
			
		}
		//Insert old and new data
		$data_month = $this->input->post('mpr_summary_month');
				
		$created_on = server_date_time();
		
			//One column
			if($this->input->post('matric_id_one_column'))
			{
				$one_column_metric_ids = $this->input->post('matric_id_one_column');//post one column metric id
				$post_one_column_data = $this->input->post('one_column_val');//post one column value
				
				foreach($one_column_metric_ids as $key_metrics_one_column=>$one_column_metric_id)
				{
					$one_column_data[] = array('ss_metric_master_id'=>$one_column_metric_id,'ss_partner_id'=>$partner_id,'ss_one_column_data_value'=>$post_one_column_data[$key_metrics_one_column],'ss_one_column_data_month'=>$data_month,'ss_one_column_data_created_on'=>$created_on);
				}
				$this->Reh_main_entry_model->create_one_column($one_column_data);//One column data insert
			}
			
			//Four Column
			if($this->input->post('matric_id_four_column'))
			{
				$four_column_metric_ids = $this->input->post('matric_id_four_column');//post four column metric id
				$post_four_column_men_data = $this->input->post('four_column_men_val');//post four column men value
				$post_four_column_women_data = $this->input->post('four_column_women_val');//post four column women value
				$post_four_column_adult_data = $this->input->post('four_column_adult_val');//post four column women value
				$post_four_column_trans_data = $this->input->post('four_column_trans_val');//post four column transgender value
				$post_four_column_boy_data = $this->input->post('four_column_boy_val');//post four column boy value
				$post_four_column_girl_data = $this->input->post('four_column_girl_val');//post four column girl value
				$post_four_column_child_data = $this->input->post('four_column_child_val');//post four column girl value
				
				foreach($four_column_metric_ids as $key_four_column=>$four_column_metric_id)
				{
					$four_column_data[] = array('ss_metric_master_id'=>$four_column_metric_id,'ss_partner_id'=>$partner_id,'ss_four_column_data_value_men'=>$post_four_column_men_data[$key_four_column],'ss_four_column_data_value_women'=>$post_four_column_women_data[$key_four_column],'ss_four_column_data_value_adult'=>$post_four_column_adult_data[$key_four_column],'ss_four_column_data_value_trans'=>$post_four_column_trans_data[$key_four_column],'ss_four_column_data_value_boys'=>$post_four_column_boy_data[$key_four_column],'ss_four_column_data_value_girls'=>$post_four_column_girl_data[$key_four_column],'ss_four_column_data_value_child'=>$post_four_column_child_data[$key_four_column],'ss_four_column_data_month'=>$data_month,'ss_four_column_data_created_on'=>$created_on);
				}
				$this->Reh_main_entry_model->create_four_column($four_column_data);
				
				//Four column data insert
				$metrics_data_insert=$this->Reh_main_entry_model->getAllMetricDataInsert($project_name,$module_name,$year);
               
			  foreach($metrics_data_insert as $key_four_column_insert)
			  {
				$four_column_insert_data[] = array('ss_metric_master_id'=>$key_four_column_insert->ss_metric_master_id,'ss_partner_id'=>$partner_id,'ss_four_column_data_value_men'=>'','ss_four_column_data_value_women'=>'','ss_four_column_data_value_adult'=>'','ss_four_column_data_value_trans'=>'','ss_four_column_data_value_boys'=>'','ss_four_column_data_value_girls'=>'','ss_four_column_data_value_child'=>'','ss_four_column_data_month'=>$data_month,'ss_four_column_data_created_on'=>$created_on);
			  }
			  $this->Reh_main_entry_model->create_four_column($four_column_insert_data);//Four 
			}
			
			//Analysis
			/*if($this->input->post('matric_id_analysis'))
			{
				$analysis_metric_ids = $this->input->post('matric_id_analysis');//post analysis metric id
				$post_analysis_data = $this->input->post('analysis_value');//post analysis value
				
				foreach($analysis_metric_ids as $key_analysis=>$analysis_metric_id)
				{
					$analysis_data[] = array('ss_metric_master_id'=>$analysis_metric_id,'ss_partner_id'=>$partner_id,'ss_analysis_data_value'=>$post_analysis_data[$key_analysis],'ss_analysis_data_month'=>$data_month,'ss_analysis_data_created_on'=>$created_on);
				}
				$this->Reh_main_entry_model->create_analysis($analysis_data);//Analysis data insert
			}*/
			
			//Mpr summary
			$partner_info = $this->Reh_main_entry_model->getPartnerInfo($partner_id);//get partner info
			$check_mpr_summ_exist = $this->Reh_main_entry_model->checkMprExist($partner_id,$project_name,$module_name,$data_month);//check mpr exist			
			if(empty($check_mpr_summ_exist))
			{
				$mpr_summ_data = array('ss_mpr_summary_partner_id'=>$partner_id,'ss_mpr_project_name'=>$project_name,'ss_mpr_module_name'=>$module_name,'ss_mpr_start_date'=>$partner_info->ss_partners_created_on,'status'=>1,'ss_mpr_report_month'=>$data_month,'ss_mpr_modified_on'=>$created_on,'ss_mpr_created_on'=>$created_on);
				
				//create mpr summary
				$this->Reh_main_entry_model->createMprSummary($mpr_summ_data);
			}else{
				$mpr_summ_update_data = array('ss_mpr_modified_on'=>$created_on);
				$mpr_summ_update_where_data = array('ss_mpr_summary_id'=>$check_mpr_summ_exist->ss_mpr_summary_id);
				//update mpr summary
				
				$this->Reh_main_entry_model->updateMprSummary($mpr_summ_update_data,$mpr_summ_update_where_data);
			}
			
			/*START*********/	
       if(!empty($this->uri->segment(2)))
				   {						
					$getdata = base64_decode($this->uri->segment(2));
					$explodedata = explode('-',$getdata);
					$project_name = $explodedata[0];
					$module_name = $explodedata[1];
					if($project_name=='rural eye health')
					{
					 $this->db->query("call Generate_REH_MPR($partner_id,'$month')");
					}
					if($project_name=='urban eye health')
					{ //echo "hello"; echo $partner_id; echo $month; die;
					$this->db->query("call Generate_UEH_MPR($partner_id,'$month')");	
					}
					 
					 //echo $partner_id; die;
		   $data['metrics_data_one'] = $this->Reh_main_entry_model->getAllMetricDataMprOne($project_name,$module_name,$current_year,$month);
		   	//echo "<pre>";	print_r($data['metrics_data_one']);  die;
				   $mpr_summ_update_data = array('ss_mpr_modified_on'=>$created_on,'status'=>2);
				   $mpr_summ_update_where_data = array('ss_mpr_summary_id'=>$mpr_id);
					 
					 foreach($data['metrics_data_one'] as $data_four)
					 {  
					  $four_column_metric_id=$data_four->master_matric_id;
				    $master_matric_depends_id=$data_four->master_matric_depends_id; 
					$data_matric_id=$data_four->data_matric_id;
					$ss_one_column_data_value=$data_four->ss_one_column_data_value;
					
					
					  if($data_four->ss_metric_master_active=='0' and $master_matric_depends_id!="")
					  { 
						     $data_four->ss_metric_master_active; 
			                 $data['four_column_metric_id_depend'] = $this->Reh_main_entry_model->getAllMetricDataMprOneDepends($month,$master_matric_depends_id); 
							 //echo "<pre>"; print_R($data['four_column_metric_id_depend']); die;

						if($master_matric_depends_id!='')	
						{			
                            $ss_one_column_data_value=$data['four_column_metric_id_depend'][0]->ss_one_column_data_value;
							$one_column_dataone[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
							'ss_mpr_partner_id'=>$partner_id,
							'ss_mpr_one_column_data_value'=>$ss_one_column_data_value,
							'ss_mpr_one_column_data_month'=>$month);
						}
                            else
						{
						$one_column_dataone[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
						'ss_mpr_partner_id'=>$partner_id,
						'ss_mpr_one_column_data_value'=>'',
						'ss_mpr_one_column_data_month'=>$month);
						}						
						//echo "<pre>"; print_R($four_column_data); die;
					}  
				     else 
					 {
						$ss_one_column_data_value=$data_four->ss_one_column_data_value;
	 
						$one_column_dataone[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
						'ss_mpr_partner_id'=>$partner_id,
						'ss_mpr_one_column_data_value'=>$ss_one_column_data_value,
						'ss_mpr_one_column_data_month'=>$month);
					}  
				   }  //echo "<pre>"; print_R($one_column_dataone); die;
				  $this->Reh_main_entry_model->create_one_column_mpr($one_column_dataone);   //Four column data insert
				  
				  //Four Column 
				   $data['metrics_data_four'] = $this->Reh_main_entry_model->getAllMetricDataMprFour($project_name,$module_name,$current_year,$month);
				  // echo "<pre>";print_R($data['metrics_data_four']); die;
				   foreach($data['metrics_data_four'] as $data_four)
					{   
				 	$four_column_metric_id=$data_four->master_matric_id;
				    $master_matric_depends_id=$data_four->master_matric_depends_id; 
					$data_matric_id=$data_four->data_matric_id;
					$ss_four_column_data_value_men=$data_four->ss_four_column_data_value_men;
					$ss_four_column_data_value_men=$data_four->ss_four_column_data_value_women;
				   	$ss_four_column_data_value_men=$data_four->ss_four_column_data_value_adult;
					$ss_four_column_data_value_men=$data_four->ss_four_column_data_value_trans;
					$ss_four_column_data_value_men=$data_four->ss_four_column_data_value_boys;
					$ss_four_column_data_value_men=$data_four->ss_four_column_data_value_girls;
					$ss_four_column_data_value_men=$data_four->ss_four_column_data_value_child;
					
					 if($data_four->ss_metric_master_active=='0' and $master_matric_depends_id!="")
					  { 
						     $data_four->ss_metric_master_active; 
			                 $data['four_column_metric_id_depend'] = $this->Reh_main_entry_model->getAllMetricDataMprFourDepends($month,$master_matric_depends_id); 
							 //echo "<pre>"; print_R($data['four_column_metric_id_depend']); die;

						if($master_matric_depends_id!='')	
						{			
                             $ss_four_column_data_value_men=$data['four_column_metric_id_depend'][0]->ss_four_column_data_value_men;
							 $ss_four_column_data_value_women=$data['four_column_metric_id_depend'][0]->ss_four_column_data_value_women;
							 $ss_four_column_data_value_adult=$data['four_column_metric_id_depend'][0]->ss_four_column_data_value_adult;
							$ss_four_column_data_value_trans=$data['four_column_metric_id_depend'][0]->ss_four_column_data_value_trans;
							 $ss_four_column_data_value_boys=$data['four_column_metric_id_depend'][0]->ss_four_column_data_value_boys;
							 $ss_four_column_data_value_girls=$data['four_column_metric_id_depend'][0]->ss_four_column_data_value_girls;
							 $ss_four_column_data_value_child=$data['four_column_metric_id_depend'][0]->ss_four_column_data_value_child;
							
							$four_column_dataone[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
							'ss_mpr_partner_id'=>$partner_id,
							'ss_mpr_four_column_data_value_men'=>$ss_four_column_data_value_men,
							'ss_mpr_four_column_data_value_women'=>$ss_four_column_data_value_women,
							'ss_mpr_four_column_data_value_adult'=>$ss_four_column_data_value_adult,
						    'ss_mpr_four_column_data_value_trans'=>$ss_four_column_data_value_trans,
							'ss_mpr_four_column_data_value_boys'=>$ss_four_column_data_value_boys,
							'ss_mpr_four_column_data_value_girls'=>$ss_four_column_data_value_girls,
							'ss_mpr_four_column_data_value_child'=>$ss_four_column_data_value_child,
							'ss_mpr_four_column_data_month'=>$month);
						}
                            else
						{
						$four_column_dataone[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
						'ss_mpr_partner_id'=>$partner_id,
						'ss_mpr_four_column_data_value_men'=>'',
						'ss_mpr_four_column_data_value_women'=>'',
						'ss_mpr_four_column_data_value_adult'=>'',
						'ss_mpr_four_column_data_value_trans'=>'',
						'ss_mpr_four_column_data_value_boys'=>'',
						'ss_mpr_four_column_data_value_girls'=>'',
						'ss_mpr_four_column_data_value_child'=>'',
						'ss_mpr_four_column_data_month'=>$month);
						}						
						//echo "<pre>"; print_R($four_column_dataone); die;
					}  
				     else 
					 {
						$ss_four_column_data_value_men=$data_four->ss_four_column_data_value_men;
						$ss_four_column_data_value_women=$data_four->ss_four_column_data_value_women;
						$ss_four_column_data_value_adult=$data_four->ss_four_column_data_value_adult;
						$ss_four_column_data_value_trans=$data_four->ss_four_column_data_value_trans;
						$ss_four_column_data_value_boys=$data_four->ss_four_column_data_value_boys;
						$ss_four_column_data_value_girls=$data_four->ss_four_column_data_value_girls;
						 $ss_four_column_data_value_child=$data_four->ss_four_column_data_value_child;
							 
						$four_column_dataone[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
						'ss_mpr_partner_id'=>$partner_id,
						'ss_mpr_four_column_data_value_men'=>$ss_four_column_data_value_men,
						'ss_mpr_four_column_data_value_women'=>$ss_four_column_data_value_women,
						'ss_mpr_four_column_data_value_adult'=>$ss_four_column_data_value_adult,
						'ss_mpr_four_column_data_value_trans'=>$ss_four_column_data_value_trans,
						'ss_mpr_four_column_data_value_boys'=>$ss_four_column_data_value_boys,
						'ss_mpr_four_column_data_value_girls'=>$ss_four_column_data_value_girls,
						'ss_mpr_four_column_data_value_child'=>$ss_four_column_data_value_child,
						'ss_mpr_four_column_data_month'=>$month);
					}  
					//echo "<pre>"; print_r($four_column_dataone); die;
					
				}
				 $this->Reh_main_entry_model->create_four_column_mpr($four_column_dataone);
				//Four column data insert ended
				
				/*//Analysis
				$data['metrics_data_analysis'] = $this->Reh_main_entry_model->getAllMetricDataMprAnalysis($project_name,$module_name,$current_year,$month);
				//echo "<pre>"; print_r($data['metrics_data_analysis']); die;
				
				foreach($data['metrics_data_analysis'] as $data_four)
					 {  
					  $four_column_metric_id=$data_four->master_matric_id;
				    $master_matric_depends_id=$data_four->master_matric_depends_id; 
					$data_matric_id=$data_four->data_matric_id;
					$ss_analysis_data_value=$data_four->ss_analysis_data_value;
					
					
					 if($data_four->ss_metric_master_active=='0' and $master_matric_depends_id!="")
					  { 
						     $data_four->ss_metric_master_active; 
			                 $data['four_column_metric_id_depend'] = $this->Reh_main_entry_model->getAllMetricDataMprAnalysisDepends($month,$master_matric_depends_id); 
							 //echo "<pre>"; print_R($data['four_column_metric_id_depend']); die;

						if($master_matric_depends_id!='')	
						{			
                             $ss_analysis_data_value=$data['four_column_metric_id_depend'][0]->ss_analysis_data_value;
							 					
							$analysis_column_data[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
							'ss_mpr_partner_id'=>$partner_id,
							'ss_mpr_analysis_data_value'=>$ss_analysis_data_value,
							'ss_mpr_analysis_data_month'=>$month);
						}
                            else
						{
						$analysis_column_data[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
						'ss_mpr_partner_id'=>$partner_id,
						'ss_mpr_analysis_data_value'=>'',
						'ss_mpr_analysis_data_month'=>$month);
						}						
						//echo "<pre>"; print_R($four_column_data); die;
					}  
				     else 
					 {
						$ss_analysis_data_value=$data_four->ss_analysis_data_value;
	 
						$analysis_column_data[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
						'ss_mpr_partner_id'=>$partner_id,
						'ss_mpr_analysis_data_value'=>$ss_analysis_data_value,
						'ss_mpr_analysis_data_month'=>$month);
					}  
				  }   //echo "<pre>"; print_r($one_column_data); die;
				 $this->Reh_main_entry_model->create_analysis_column_mpr($analysis_column_data);//Four column data insert*/
			}
					
	
	
	
	
	/*END*/
	}
	
	public function update_form_data_analysis($month_from)
	{		
		$partner_id = $_SESSION["partner_id"];
		$mpr_id = $this->input->post('mpr_summary_id');
		$getdata = base64_decode($this->uri->segment(2));
		$explodedata = explode('-',$getdata);
		$project_name = $explodedata[0];
		$module_name = $explodedata[1];
		$sections = $this->Reh_main_entry_model->getSectionId($project_name,$module_name);
		$year = date('Y',strtotime($month_from));
		
		$this->load->library('user_agent');				
		$created_on = server_date_time();
		$month = $month_from;
		//$current_year = $this->input->post('current_year'); 
		$current_year = date('Y',strtotime($month));
				
		if($this->input->post('mpr_summary_id'))
		{
		
					
		function get_section_id($sections)
		{
			return $sections->ss_section_layout_id;
		}
		$section_ids =  implode(array_map("get_section_id", $sections), ',');
		$metrics = $this->Reh_main_entry_model->getMetricId($section_ids,$year);
		function get_metric_id($metrics)
		{
			return $metrics->ss_metric_master_id;
		}
		$metrics_ids =  implode(array_map("get_metric_id", $metrics), ',');
		
		//Get analysis id
		
		$get_analysis_ids = $this->Reh_main_entry_model->getAnalysisData($partner_id,$month_from,$metrics_ids);
		
		function get_analysis_id($get_analysis_ids)
		{
			return $get_analysis_ids->ss_analysis_data_id;
		}
		$analysis_ids =  implode(array_map("get_analysis_id", $get_analysis_ids), ',');
		//echo($analysis_ids); exit;
		//Delete old data
		//one column data
		
		//analysis data
		if($analysis_ids!= "") {
			$this->Reh_main_entry_model->deleteAnalysisData($analysis_ids);
		}
			
			
			
		}
		//Insert old and new data
		$data_month = $this->input->post('mpr_summary_month');
				
		$created_on = server_date_time();
			
			//Analysis
			if($this->input->post('matric_id_analysis'))
			{
				$analysis_metric_ids = $this->input->post('matric_id_analysis');//post analysis metric id
				$post_analysis_data = $this->input->post('analysis_value');//post analysis value
				
				foreach($analysis_metric_ids as $key_analysis=>$analysis_metric_id)
				{
					$analysis_data[] = array('ss_metric_master_id'=>$analysis_metric_id,'ss_partner_id'=>$partner_id,'ss_analysis_data_value'=>$post_analysis_data[$key_analysis],'ss_analysis_data_month'=>$data_month,'ss_analysis_data_created_on'=>$created_on);
				}
				$this->Reh_main_entry_model->create_analysis($analysis_data);//Analysis data insert
			}
			
			//Mpr summary
			$partner_info = $this->Reh_main_entry_model->getPartnerInfo($partner_id);//get partner info
			$check_mpr_summ_exist = $this->Reh_main_entry_model->checkMprExist($partner_id,$project_name,$module_name,$data_month);//check mpr exist			
			if(empty($check_mpr_summ_exist))
			{
				$mpr_summ_data = array('ss_mpr_summary_partner_id'=>$partner_id,'ss_mpr_project_name'=>$project_name,'ss_mpr_module_name'=>$module_name,'ss_mpr_start_date'=>$partner_info->ss_partners_created_on,'status'=>1,'ss_mpr_report_month'=>$data_month,'ss_mpr_modified_on'=>$created_on,'ss_mpr_created_on'=>$created_on);
				
				//create mpr summary
				$this->Reh_main_entry_model->createMprSummary($mpr_summ_data);
			}else{
				$mpr_summ_update_data = array('ss_mpr_modified_on'=>$created_on);
				$mpr_summ_update_where_data = array('ss_mpr_summary_id'=>$check_mpr_summ_exist->ss_mpr_summary_id);
				//update mpr summary
				
				$this->Reh_main_entry_model->updateMprSummary($mpr_summ_update_data,$mpr_summ_update_where_data);
			}
			
			/*START*********/	
       if(!empty($this->uri->segment(2)))
				   {						
					$getdata = base64_decode($this->uri->segment(2));
					$explodedata = explode('-',$getdata);
					$project_name = $explodedata[0];
					$module_name = $explodedata[1];
					if($project_name=='rural eye health')
					{
					 $this->db->query("call Generate_REH_MPR($partner_id,'$month')");
					}
					if($project_name=='urban eye health')
					{ //echo "hello"; echo $partner_id; echo $month; die;
					$this->db->query("call Generate_UEH_MPR($partner_id,'$month')");	
					}
					
				
				//Analysis
				$data['metrics_data_analysis'] = $this->Reh_main_entry_model->getAllMetricDataMprAnalysis($project_name,$module_name,$current_year,$month);
				//echo "<pre>"; print_r($data['metrics_data_analysis']); die;
				
				foreach($data['metrics_data_analysis'] as $data_four)
					 {  
					  $four_column_metric_id=$data_four->master_matric_id;
				    $master_matric_depends_id=$data_four->master_matric_depends_id; 
					$data_matric_id=$data_four->data_matric_id;
					$ss_analysis_data_value=$data_four->ss_analysis_data_value;
					
					
					 if($data_four->ss_metric_master_active=='0' and $master_matric_depends_id!="")
					  { 
						     $data_four->ss_metric_master_active; 
			                 $data['four_column_metric_id_depend'] = $this->Reh_main_entry_model->getAllMetricDataMprAnalysisDepends($month,$master_matric_depends_id); 
							 //echo "<pre>"; print_R($data['four_column_metric_id_depend']); die;

						if($master_matric_depends_id!='')	
						{			
                             $ss_analysis_data_value=$data['four_column_metric_id_depend'][0]->ss_analysis_data_value;
							 					
							$analysis_column_data[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
							'ss_mpr_partner_id'=>$partner_id,
							'ss_mpr_analysis_data_value'=>$ss_analysis_data_value,
							'ss_mpr_analysis_data_month'=>$month);
						}
                            else
						{
						$analysis_column_data[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
						'ss_mpr_partner_id'=>$partner_id,
						'ss_mpr_analysis_data_value'=>'',
						'ss_mpr_analysis_data_month'=>$month);
						}						
						//echo "<pre>"; print_R($four_column_data); die;
					}  
				     else 
					 {
						$ss_analysis_data_value=$data_four->ss_analysis_data_value;
	 
						$analysis_column_data[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
						'ss_mpr_partner_id'=>$partner_id,
						'ss_mpr_analysis_data_value'=>$ss_analysis_data_value,
						'ss_mpr_analysis_data_month'=>$month);
					}  
				  }   //echo "<pre>"; print_r($one_column_data); die;
				 $this->Reh_main_entry_model->create_analysis_column_mpr($analysis_column_data);//Four column data insert
			}
					
	
	
	
	
	/*END*/
	}
}
