<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Reh_base_hospital extends CI_Controller {

    public function __construct() {
        parent::__construct();
		check_login();
        $this->load->model('Reh_base_hospital_model');
        $this->load->model('Reh_main_entry_model');		
    }

    public function index() {		
		$data['base_data'] = "";
		$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		$data['include'] = "reh/base_hospital";
		$this->load->view('container_login_dis', $data);
    }
	public function create()
	{ 
	$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
		
			$this->form_validation->set_rules('month_data','Month','required');
					
			if($this->form_validation->run()==true)
			{
				$partner_id = $_SESSION["partner_id"];
				$created_on = server_date_time();
				$id_ajax1=$this->input->post('id_ajax');
				$opd = $this->input->post('opd_reh');
				$patient_name = $this->input->post('patient_name');
			 	$opd_ssno = $this->input->post('opd_ssno');
				$kin_name = $this->input->post('kin_name');
				$village = $this->input->post('village');
				$block = $this->input->post('block');
				$district = $this->input->post('district');
				$age = $this->input->post('age');
				$gender = $this->input->post('gender');
				$ref_fr_by = $this->input->post('ref_fr_by');
				$eye_oper = $this->input->post('eye_oper');
				$pre_oper = $this->input->post('pre_oper');
				$post_oper = $this->input->post('post_oper');
				$surg_type = $this->input->post('surg_type');
				$surg_support = $this->input->post('surg_support');
				
				$combine_day = "01-".$this->input->post('month_data');
				$data_month = date("Y-m-d",strtotime($combine_day));
				if($this->input->post('id_ajax')=='')
				{
				$base_hospital_data = array('ss_partner_id'=>$partner_id,'ss_sno'=>$opd_ssno,'ss_opd_no'=>$opd,'ss_patient_name'=>$patient_name,'ss_kin_name'=>$kin_name,'ss_village'=>$village,'ss_block'=>$block,'ss_districts'=>$district,'ss_age'=>$age,'ss_gender'=>$gender,'ss_ref_from_ref_by'=>$ref_fr_by,'ss_eye_oper'=>$eye_oper,'ss_pre_oper'=>$pre_oper,'ss_post_oper'=>$post_oper,'ss_surgery_type'=>$surg_type,'ss_surgery_support'=>$surg_support,'ss_reh_base_month'=>$data_month,'ss_reh_base_created_on'=>$created_on);
				$creation_check = $this->Reh_base_hospital_model->createBaseHospital($base_hospital_data);
				}
				else
				{
					$base_hospital_data_update = array('ss_partner_id'=>$partner_id,'ss_sno'=>$opd_ssno,'ss_opd_no'=>$opd,'ss_patient_name'=>$patient_name,'ss_kin_name'=>$kin_name,'ss_village'=>$village,'ss_block'=>$block,'ss_districts'=>$district,'ss_age'=>$age,'ss_gender'=>$gender,'ss_ref_from_ref_by'=>$ref_fr_by,'ss_eye_oper'=>$eye_oper,'ss_pre_oper'=>$pre_oper,'ss_post_oper'=>$post_oper,'ss_surgery_type'=>$surg_type,'ss_surgery_support'=>$surg_support,'ss_reh_base_month'=>$data_month,'ss_reh_base_created_on'=>$created_on);
				 $creation_check = $this->Reh_base_hospital_model->UpdateBase($base_hospital_data_update,$id_ajax1);
				}
				$this->session->set_flashdata('success','OPD No:'.$opd.'  Your data is successfully saved');
				//redirect('reh_base_hospital');
				redirect('filter_base_reh?month_from='.$_SESSION["month_date"]);
			}else{
				$data['include'] = "reh/base_hospital";
			$this->load->view('container_login_dis', $data);
			}
		}
	}
	public function filter_base_data()
	{ 
		$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		 
		if($this->input->server('REQUEST_METHOD') === 'GET' and $this->input->get('month_from')!=null and preg_match('/^[0-9]{2}-[0-9]{4}\z/', $this->input->get('month_from')))
		{ 
			//$_SESSION['ABC'] = 1;
			//$this->form_validation->set_rules('month_from','Month','required');
			//if($this->form_validation->run()==true)
			//{
				$combine_day = "01-".$this->input->get('month_from');
				$month_from = date("Y-m-d",strtotime($combine_day));
				$partner_id = $_SESSION["partner_id"];
				$data['base_data'] = $this->Reh_base_hospital_model->getBaseHopital($month_from,$partner_id);
			//}
			$data['include'] = "reh/base_hospital";
			$this->load->view('container_login_dis', $data);
		}else{
			redirect('reh_base_hospital');
		}
	}
	
	public function excelupload()
	{		
		
		if(!empty($_FILES['upload_publish_file']['name']))
		{
			$excel_data="";
			$created_on = server_date_time();
			$combine_day = "01-".$this->input->post('month_data');
			$data_month = date("Y-m-d",strtotime($combine_day));
			$partner_id = $_SESSION["partner_id"];
		
			require_once(EXCELDIR."PHPExcel.php");
			$excel_file= $_FILES['upload_publish_file']['tmp_name'];
			$inputFileName = $excel_file;
				try 
					{
					$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
					} 
				catch(Exception $e)
					{
					die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
					}
					require_once(EXCELDIR."PHPExcel/IOFactory.php");

					$objReader = PHPExcel_IOFactory::createReader('Excel2007');
					$inputFileType = 'Excel2007';					
					$sheetIndex = 0;
					$objReader = PHPExcel_IOFactory::createReader($inputFileType);
					$sheetnames = $objReader->listWorksheetNames($inputFileName);
					$objReader->setLoadSheetsOnly($sheetnames[$sheetIndex]);
					$objPHPExcel = $objReader->load($inputFileName);
					$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);					
					$start=3;
					$end = $highestRow = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
						for ($row = $start; $row <=$end; $row++) 
						{									
							$excel_data[] = array('ss_partner_id'=>$partner_id,
							'ss_sno'=>$objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue(),					
							'ss_opd_no'=>$objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue(),
							'ss_patient_name'=>$objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue(),
							'ss_kin_name'=>$objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue(),
							'ss_village'=>$objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue(),
							'ss_block'=>$objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue(),
							'ss_districts'=>$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue(),
							'ss_age'=>$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue(),
							'ss_gender'=>$objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue(),
							'ss_ref_from_ref_by'=>$objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue(),
							'ss_eye_oper'=>$objPHPExcel->getActiveSheet()->getCell('P'.$row)->getValue(),
							'ss_pre_oper'=>$objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue(),
							'ss_post_oper'=>$objPHPExcel->getActiveSheet()->getCell('R'.$row)->getValue(),
							'ss_surgery_type'=>$objPHPExcel->getActiveSheet()->getCell('S'.$row)->getValue(),							
							'ss_surgery_support'=>$objPHPExcel->getActiveSheet()->getCell('T'.$row)->getValue(),							
							'ss_reh_base_month'=>$data_month,'ss_reh_base_created_on'=>$created_on);
						} 
			
					if(!empty($excel_data)) {
						$creation_check = $this->Reh_base_hospital_model->createBaseHospitalExcel($excel_data);
					}

				$data['base_data'] = $this->Reh_base_hospital_model->getBaseHopital($data_month,$partner_id);
			//	$this->session->set_flashdata('success','OPD No:'.for ($row = $start; $row <=$end; $row++) 
					//	{ $objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue() } .'  Your data is successfully saved');
				$data['include'] = "reh/base_hospital";
				$this->load->view('container_login_dis', $data);
		}else{
			redirect('reh_base_hospital');
		}
		
	}
	
	
	public function delete_base_reh()
	{ 
		if(!empty($this->uri->segment(2)))
        {
	     $id = base64_decode($this->uri->segment(2));
		 $this->Reh_base_hospital_model->deleteHehBase($id);
	     redirect($_SERVER['HTTP_REFERER']);
		}
	}
	
	public function edit_base_reh()
	{ 
	  //if($this->input->server('REQUEST_METHOD') === 'POST'){ 
	     $id = base64_decode($this->input->post('id'));
		 $data=$this->Reh_base_hospital_model->editFetchData($id);
		 //print_r($data); exit;
		 if(!empty($data))
		 {
			 $data2 = json_encode($data[0]);
			 echo $data2."$$$".$this->security->get_csrf_hash();
		 }
		 else{
			 
			 echo 0;
		 }
		 
		 //$data['include'] = "reh/edit_base_reh";
		  //$this->load->view('container_login', $data);
		//} 
     		
	}
	
	
	
	
	
  
}
