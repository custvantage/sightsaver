<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Reh_iec extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Reh_iec_model');
		$this->load->model('Reh_main_entry_model');
		check_login();
    }

    public function index() {
		$data['iec_data'] = "";
		$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		$data['include'] = "reh/iec";
		$this->load->view('container_login_dis', $data);
    }
	public function create()
	{
		$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
		
			$this->form_validation->set_rules('month_data','Month','required');
					
			if($this->form_validation->run()==true)
			{
				$partner_id = $_SESSION["partner_id"];
				$created_on = server_date_time();
				
				$ajax_id1 = $this->input->post('id_ajax');
				$block_name = $this->input->post('block_iec');
				$poster = $this->input->post('poster_no_iec');
				$booklet = $this->input->post('booklet_no_iec');
				$pamplet = $this->input->post('pamplet_no_iec');
				$wall = $this->input->post('wall_no_iec');
				$other = $this->input->post('other_no_iec');
				$person = $this->input->post('person_reach_iec');
				
				$combine_day = "01-".$this->input->post('month_data');
		    	$data_month = date("Y-m-d",strtotime($combine_day)); 
				
				if($this->input->post('id_ajax')=='')
				{
				$bcc_data = array('ss_partner_id'=>$partner_id,'ss_reh_iec_block'=>$block_name,'ss_reh_iec_posters'=>$poster,'ss_reh_iec_booklet'=>$booklet,'ss_reh_iec_pamplets'=>$pamplet,'ss_reh_iec_wall'=>$wall,'ss_reh_iec_others'=>$other,'ss_reh_through_iec'=>$person,'ss_reh_iec_month'=>$data_month,'ss_reh_iec_created_on'=>$created_on);
				
				$creation_check = $this->Reh_iec_model->createIec($bcc_data);
				}
				else{
						$update_iec_data = array('ss_partner_id'=>$partner_id,'ss_reh_iec_block'=>$block_name,'ss_reh_iec_posters'=>$poster,'ss_reh_iec_booklet'=>$booklet,'ss_reh_iec_pamplets'=>$pamplet,'ss_reh_iec_wall'=>$wall,'ss_reh_iec_others'=>$other,'ss_reh_through_iec'=>$person,'ss_reh_iec_month'=>$data_month,'ss_reh_iec_created_on'=>$created_on);
				        $creation_check = $this->Reh_iec_model->UpdateIec($update_iec_data,$ajax_id1);
				}
		     
			 // echo "<pre>";print_R($data['iec_data1']); die;
				$this->session->set_flashdata('success','Your data is successfully saved');
			   // $data['iec_data1'] = $this->Reh_iec_model->getIec($_SESSION["month_date"],$partner_id); 
				redirect('filter_iec_reh?month_from='.$_SESSION["month_date"]);
			}else{
				
			$data['include'] = "reh/iec";
			$this->load->view('container_login_dis', $data);
			}
		}
	}
	public function filter_iec_data()
	{  
    	$userid=$this->session->userdata('userinfo')['user_id'];
		
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		if($this->input->server('REQUEST_METHOD') === 'GET' and $this->input->get('month_from')!=null and preg_match('/^[0-9]{2}-[0-9]{4}\z/', $this->input->get('month_from')))
		{
			//$this->form_validation->set_rules('month_from','Month','required');
			
				 $combine_day = "01-".$this->input->get('month_from');
				 $month_from = date("Y-m-d",strtotime($combine_day));
				$partner_id = $_SESSION["partner_id"];
				$data['iec_data'] = $this->Reh_iec_model->getIec($month_from,$partner_id);
			$data['include'] = "reh/iec";
			$this->load->view('container_login_dis', $data);
		}else{
			redirect('reh_iec');
		}
	}

	public function delete_iec_reh()
	{ 
		if(!empty($this->uri->segment(2)))
        {
	     $id = base64_decode($this->uri->segment(2));
		 $this->Reh_iec_model->deleteHehIec($id);
	      redirect($_SERVER['HTTP_REFERER']);
		}
	}
	
	public function edit_iec_reh()
	{ 
	     $id = base64_decode($this->input->post('id'));
		 $data=$this->Reh_iec_model->editFetchData($id);
		 if(!empty($data))
		 {
			 $data2 = json_encode($data[0]);
			 echo $data2."$$$".$this->security->get_csrf_hash();
		 }
		 else
		 {
			 echo 0;
		 }
	}

}
