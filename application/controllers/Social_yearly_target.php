<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Social_yearly_target extends CI_Controller {

    public function __construct() {
        parent::__construct();
		check_login();
        $this->load->model('Reh_main_entry_model');
		$this->load->library('metricdata');
		$this->load->library('common');
    }

    public function index() {
		
        if(!empty($this->uri->segment(2)))
        {						
            $getdata = base64_decode($this->uri->segment(2));
            $explodedata = explode('-',$getdata);
            $project_name = $explodedata[0];
            $module_name = $explodedata[1];
			$created_on = server_date_time();
			$current_year = date('Y',strtotime($created_on));
			
            $data['sections'] = $this->Reh_main_entry_model->getAllSections($project_name, $module_name,$current_year);
            
            $data['metrics_data'] = $this->Reh_main_entry_model->getAllMetricData($project_name,$module_name,$current_year);
            
			if($this->session->userdata('userinfo')['default_role'] != 6)//if pm/po
			{
				$data['partners'] = $this->common->getPartners();
			}
            $data['include'] = "social_inc/yearly_targets";
            $this->load->view('container_login', $data);
			
        }else
        {
            redirect('dashboard');
        }
    }
    public function create_social_yt_entry()
	{ 
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{	
			$getdata = base64_decode($this->uri->segment(2));
			$explodedata = explode('-',$getdata);
			$project_name = $explodedata[0];
			$module_name = $explodedata[1];
			
			$combine_day = "01-".$this->input->post('month_data');
			$data_month = date("Y-m-d",strtotime($combine_day));
			$year = date('Y',strtotime($combine_day));
			
			$this->form_validation->set_rules('month_data','Month','required');
					
			if($this->form_validation->run()==true)
			{
				
				$created_on = server_date_time();
				$partner_id = $this->session->userdata['userinfo']['partner_id'];				
								
				//One column
				if($this->input->post('matric_id_one_column'))
				{
					$one_column_metric_ids = $this->input->post('matric_id_one_column');//post one column metric id
					$post_one_column_data = $this->input->post('one_column_val');//post one column value
					foreach($one_column_metric_ids as $key_metrics_one_column=>$one_column_metric_id)
					{
						$one_column_data[] = array('ss_metric_master_id'=>$one_column_metric_id,'ss_partner_id'=>$partner_id,'ss_one_column_data_value'=>$post_one_column_data[$key_metrics_one_column],'ss_one_column_data_month'=>$data_month,'ss_one_column_data_created_on'=>$created_on);
					}
					
					$this->Reh_main_entry_model->create_one_column($one_column_data);//One column data insert
				}
				/* //Four Column
				if($this->input->post('matric_id_four_column'))
				{
					$four_column_metric_ids = $this->input->post('matric_id_four_column');//post four column metric id
					$post_four_column_men_data = $this->input->post('four_column_men_val');//post four column men value
					$post_four_column_women_data = $this->input->post('four_column_women_val');//post four column women value
					$post_four_column_trans_data = $this->input->post('four_column_trans_val');//post four column transgender value
					$post_four_column_boy_data = $this->input->post('four_column_boy_val');//post four column boy value
					$post_four_column_girl_data = $this->input->post('four_column_girl_val');//post four column girl value
					
					foreach($four_column_metric_ids as $key_four_column=>$four_column_metric_id)
					{
						$four_column_data[] = array('ss_metric_master_id'=>$four_column_metric_id,'ss_partner_id'=>$partner_id,'ss_four_column_data_value_men'=>$post_four_column_men_data[$key_four_column],'ss_four_column_data_value_women'=>$post_four_column_women_data[$key_four_column],'ss_four_column_data_value_trans'=>$post_four_column_trans_data[$key_four_column],'ss_four_column_data_value_boys'=>$post_four_column_boy_data[$key_four_column],'ss_four_column_data_value_girls'=>$post_four_column_girl_data[$key_four_column],'ss_four_column_data_month'=>$data_month,'ss_four_column_data_created_on'=>$created_on);
					}
					$this->Reh_main_entry_model->create_four_column($four_column_data);//Four column data insert
				} */
				
				//Analysis
				/* if($this->input->post('matric_id_analysis'))
				{
					$analysis_metric_ids = $this->input->post('matric_id_analysis');//post analysis metric id
					$post_analysis_data = $this->input->post('analysis_value');//post analysis value
					
					foreach($analysis_metric_ids as $key_analysis=>$analysis_metric_id)
					{
						$analysis_data[] = array('ss_metric_master_id'=>$analysis_metric_id,'ss_partner_id'=>$partner_id,'ss_analysis_data_value'=>$post_analysis_data[$key_analysis],'ss_analysis_data_month'=>$data_month,'ss_analysis_data_created_on'=>$created_on);
					}
					$this->Reh_main_entry_model->create_analysis($analysis_data);//Analysis data insert
				}				
				 */
				//Mpr summary
				$partner_info = $this->Reh_main_entry_model->getPartnerInfo($partner_id);//get partner info
				$check_mpr_summ_exist = $this->Reh_main_entry_model->checkMprExist($partner_id,$project_name,$module_name,$data_month);//check mpr exist
				if(empty($check_mpr_summ_exist))
				{
					$mpr_summ_data = array('ss_mpr_summary_partner_id'=>$partner_id,'ss_mpr_project_name'=>$project_name,'ss_mpr_module_name'=>$module_name,'ss_mpr_start_date'=>$partner_info->ss_partners_created_on,'status'=>1,'ss_mpr_report_month'=>$data_month,'ss_mpr_modified_on'=>$created_on,'ss_mpr_created_on'=>$created_on);
					
					//create mpr summary
					$this->Reh_main_entry_model->createMprSummary($mpr_summ_data);
				}else{
					$mpr_summ_update_data = array('ss_mpr_modified_on'=>$created_on);
					$mpr_summ_update_where_data = array('ss_mpr_summary_id'=>$check_mpr_summ_exist->ss_mpr_summary_id);
					//update mpr summary
					
					$this->Reh_main_entry_model->updateMprSummary($mpr_summ_update_data,$mpr_summ_update_where_data);
				}
			}
			$data['sections'] = $this->Reh_main_entry_model->getAllSections($project_name, $module_name,$year);
            
            $data['metrics_data'] = $this->Reh_main_entry_model->getAllMetricData($project_name,$module_name,$year);           

			$data['include'] = "social_inc/yearly_targets";
            $this->load->view('container_login', $data);
			
		}else{
			redirect('yearly_targets/'.$this->uri->segment(2));
		}
	}
	
	public function manage()
	{
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$getdata = base64_decode($this->uri->segment(2));
			$explodedata = explode('-',$getdata);
			$project_name = $explodedata[0];
			$module_name = $explodedata[1];
			$combine_day = "01-".$this->input->post('month_from');			
			$data['post_month_from'] = date("Y-m-d",strtotime($combine_day));
			$year = date('Y',strtotime($combine_day));
			$data['mpr_report_month'] = $data['post_month_from'];
			if($this->session->userdata('userinfo')['default_role'] == 6)//if partner
			{
				$data['session_partner_id'] = $this->session->userdata['userinfo']['partner_id'];
				$check_mpr_summ_exist = $this->Reh_main_entry_model->checkMprExistArr($data['session_partner_id'],$project_name,$module_name,$data['post_month_from']);//check mpr exist				
				if($check_mpr_summ_exist['status'] == 1 || $check_mpr_summ_exist['status'] == 4)
				{
					$data['mpr_id'] = $check_mpr_summ_exist['ss_mpr_summary_id'];
					//$data['mpr_report_month'] = $check_mpr_summ_exist['ss_mpr_report_month'];
				}else{
					$data['mpr_id'] = "";
					//$data['mpr_report_month'] = "";
				}
			}else{
				$data['session_partner_id'] = $this->input->post('partner_name');				
				$data['partners'] = $this->common->getPartners();
				$user_id = $this->session->userdata('userinfo')['user_id'];
				$data['comments'] = $this->common->getComments($user_id,$data['post_month_from'],$data['session_partner_id'],$project_name,$module_name);
				$data['comments_pmpo'] = $this->common->getPmpoComments($data['session_partner_id'],$data['post_month_from'],$project_name,$module_name);
			}
			
			$data['sections'] = $this->Reh_main_entry_model->getAllSections($project_name, $module_name,$year);
		
			$data['metrics_data'] = $this->Reh_main_entry_model->getAllMetricData($project_name,$module_name,$year);			
			
			$data['include'] = "social_inc/manage_social_yearly_target";
			$this->load->view('container_login', $data);
		}else{
			redirect('yearly_targets/'.$this->uri->segment(2));
		}
	}
		public function submit_form_data_yearly()
	    {
          if($this->input->server('REQUEST_METHOD') === 'POST')
		  { 
	       $partner_id=$this->session->userdata('userinfo')['partner_id'];
			$this->form_validation->set_rules('mpr_summary_id','Mpr Summary','required');			
			if($this->form_validation->run()==true)
		    	{
				$this->load->library('user_agent');				
				$created_on = server_date_time();
				 $mpr_id = $this->input->post('mpr_summary_id'); 
				 $month = $this->input->post('current_year');
			      $current_year = $this->input->post('current_year'); 
				  $current_year = date('Y',strtotime($current_year));
                 if(!empty($this->uri->segment(2)))
				   {						
					$getdata = base64_decode($this->uri->segment(2));
					$explodedata = explode('-',$getdata);
					$project_name = $explodedata[0];
					$module_name = $explodedata[1];
					 
					 //echo $partner_id; die;
		          $data['metrics_data_one'] = $this->Reh_main_entry_model->getAllMetricDataMprOne($project_name,$module_name,$current_year,$month);
		          //	echo "<pre>";	print_r($data['metrics_data_one']);  die;
				   $mpr_summ_update_data = array('ss_mpr_modified_on'=>$created_on,'status'=>2);
				   $mpr_summ_update_where_data = array('ss_mpr_summary_id'=>$mpr_id);
					 
					 foreach($data['metrics_data_one'] as $data_four)
					 {  
						$four_column_metric_id=$data_four->master_matric_id;
						$master_matric_depends_id=$data_four->master_matric_depends_id; 
						$data_matric_id=$data_four->data_matric_id;
						$ss_one_column_data_value=$data_four->ss_one_column_data_value;

					  $one_column_data="";
					  if($data_four->ss_metric_master_active=='0' and $master_matric_depends_id!="")
					  { 
						     $data_four->ss_metric_master_active; 
			                 $data['four_column_metric_id_depend'] = $this->Reh_main_entry_model->getAllMetricDataMprOneDepends($month,$master_matric_depends_id); 
							 //echo "<pre>"; print_R($data['four_column_metric_id_depend']); die;
                       
						if($master_matric_depends_id!='')	
						{			
                             $ss_one_column_data_value=$data['four_column_metric_id_depend'][0]->ss_one_column_data_value;
							 					
							$one_column_data[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
							'ss_mpr_partner_id'=>$partner_id,
							'ss_mpr_one_column_data_value'=>$ss_one_column_data_value,
							'ss_mpr_one_column_data_month'=>$month);
						}
                            else
						{
						$one_column_data[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
						'ss_mpr_partner_id'=>$partner_id,
						'ss_mpr_one_column_data_value'=>'',
						'ss_mpr_one_column_data_month'=>$month);
						}						
					   }  
				     else 
					 {
						$ss_one_column_data_value=$data_four->ss_one_column_data_value;
						$one_column_data[] = array('ss_mpr_metric_master_id'=>$four_column_metric_id,
						'ss_mpr_partner_id'=>$partner_id,
						'ss_mpr_one_column_data_value'=>$ss_one_column_data_value,
						'ss_mpr_one_column_data_month'=>$month);
					}  
				   }  
				  $this->Reh_main_entry_model->create_one_column_mpr($one_column_data);   //Four column data insert
			}	
          }	
			$this->Reh_main_entry_model->updateMprSummary($mpr_summ_update_data,$mpr_summ_update_where_data);			
			redirect($this->agent->referrer());
		}
	}
	
	
	public function edit_form_data_yearly()
	{
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{			
			//$this->form_validation->set_rules('mpr_summary_id','Mpr Summary','required');			
			//if($this->form_validation->run()==true)
			//{
				$this->load->library('user_agent');
				if($this->input->post('update_form_data'))
				{
					$month_from = $this->input->post('mpr_summary_month');
					$this->update_form_data_yearly($month_from);
					redirect($this->agent->referrer());
				}
			//}
		}
	}
	public function update_form_data_yearly($month_from)
	{		
		$partner_id = $this->session->userdata['userinfo']['partner_id'];
		$getdata = base64_decode($this->uri->segment(2));
		$explodedata = explode('-',$getdata);
		$project_name = $explodedata[0];
		$module_name = $explodedata[1];
		$sections = $this->Reh_main_entry_model->getSectionId($project_name,$module_name);
		$year = date('Y',strtotime($month_from));
				
		if($this->input->post('mpr_summary_id'))
		{
		function get_section_id($sections)
		{
			return $sections->ss_section_layout_id;
		}
		$section_ids =  implode(array_map("get_section_id", $sections), ',');
		$metrics = $this->Reh_main_entry_model->getMetricId($section_ids,$year);
		function get_metric_id($metrics)
		{
			return $metrics->ss_metric_master_id;
		}
		$metrics_ids =  implode(array_map("get_metric_id", $metrics), ',');
		
		//Get one column id
		$get_onecolumn_ids = $this->Reh_main_entry_model->getOneColumnData($partner_id,$month_from,$metrics_ids);
		function get_one_column_id($get_onecolumn_ids)
		{
			return $get_onecolumn_ids->ss_one_column_data_id;
		}
		$one_column_ids =  implode(array_map("get_one_column_id", $get_onecolumn_ids), ',');
		
		/* //Get four column id
		$get_fourcolumn_ids = $this->Reh_main_entry_model->getFourColumnData($partner_id,$month_from,$metrics_ids);
		function get_four_column_id($get_fourcolumn_ids)
		{
			return $get_fourcolumn_ids->ss_four_column_data_id;
		}
		$four_column_ids =  implode(array_map("get_four_column_id", $get_fourcolumn_ids), ',');
		
		//Get analysis id
		$get_analysis_ids = $this->Reh_main_entry_model->getAnalysisData($partner_id,$month_from,$metrics_ids);
		function get_analysis_id($get_analysis_ids)
		{
			return $get_analysis_ids->ss_analysis_data_id;
		}
		$analysis_ids =  implode(array_map("get_analysis_id", $get_analysis_ids), ','); */
		
		//Delete old data
		//one column data		
			$this->Reh_main_entry_model->deleteOneColumnData($one_column_ids);
		//four column data
			//$this->Reh_main_entry_model->deleteFourColumnData($four_column_ids);
		//analysis data
			//$this->Reh_main_entry_model->deleteAnalysisData($analysis_ids);
		}
		//Insert old and new data
		$data_month = $this->input->post('mpr_summary_month');
				
		$created_on = server_date_time();
		
			//One column
			if($this->input->post('matric_id_one_column'))
			{
				$one_column_metric_ids = $this->input->post('matric_id_one_column');//post one column metric id
				$post_one_column_data = $this->input->post('one_column_val');//post one column value
				
				foreach($one_column_metric_ids as $key_metrics_one_column=>$one_column_metric_id)
				{
					$one_column_data[] = array('ss_metric_master_id'=>$one_column_metric_id,'ss_partner_id'=>$partner_id,'ss_one_column_data_value'=>$post_one_column_data[$key_metrics_one_column],'ss_one_column_data_month'=>$data_month,'ss_one_column_data_created_on'=>$created_on);
				}
				$this->Reh_main_entry_model->create_one_column($one_column_data);//One column data insert
			}
			
			//Four Column
			/* if($this->input->post('matric_id_four_column'))
			{
				$four_column_metric_ids = $this->input->post('matric_id_four_column');//post four column metric id
				$post_four_column_men_data = $this->input->post('four_column_men_val');//post four column men value
				$post_four_column_women_data = $this->input->post('four_column_women_val');//post four column women value
				$post_four_column_trans_data = $this->input->post('four_column_trans_val');//post four column transgender value
				$post_four_column_boy_data = $this->input->post('four_column_boy_val');//post four column boy value
				$post_four_column_girl_data = $this->input->post('four_column_girl_val');//post four column girl value
				
				foreach($four_column_metric_ids as $key_four_column=>$four_column_metric_id)
				{
					$four_column_data[] = array('ss_metric_master_id'=>$four_column_metric_id,'ss_partner_id'=>$partner_id,'ss_four_column_data_value_men'=>$post_four_column_men_data[$key_four_column],'ss_four_column_data_value_women'=>$post_four_column_women_data[$key_four_column],'ss_four_column_data_value_trans'=>$post_four_column_trans_data[$key_four_column],'ss_four_column_data_value_boys'=>$post_four_column_boy_data[$key_four_column],'ss_four_column_data_value_girls'=>$post_four_column_girl_data[$key_four_column],'ss_four_column_data_month'=>$data_month,'ss_four_column_data_created_on'=>$created_on);
				}
				$this->Reh_main_entry_model->create_four_column($four_column_data);//Four column data insert
			} */
			
			//Analysis
			/* if($this->input->post('matric_id_analysis'))
			{
				$analysis_metric_ids = $this->input->post('matric_id_analysis');//post analysis metric id
				$post_analysis_data = $this->input->post('analysis_value');//post analysis value
				
				foreach($analysis_metric_ids as $key_analysis=>$analysis_metric_id)
				{
					$analysis_data[] = array('ss_metric_master_id'=>$analysis_metric_id,'ss_partner_id'=>$partner_id,'ss_analysis_data_value'=>$post_analysis_data[$key_analysis],'ss_analysis_data_month'=>$data_month,'ss_analysis_data_created_on'=>$created_on);
				}
				$this->Reh_main_entry_model->create_analysis($analysis_data);//Analysis data insert
			} */
			
			//Mpr summary
			$partner_info = $this->Reh_main_entry_model->getPartnerInfo($partner_id);//get partner info
			$check_mpr_summ_exist = $this->Reh_main_entry_model->checkMprExist($partner_id,$project_name,$module_name,$data_month);//check mpr exist			
			if(empty($check_mpr_summ_exist))
			{
				$mpr_summ_data = array('ss_mpr_summary_partner_id'=>$partner_id,'ss_mpr_project_name'=>$project_name,'ss_mpr_module_name'=>$module_name,'ss_mpr_start_date'=>$partner_info->ss_partners_created_on,'status'=>1,'ss_mpr_report_month'=>$data_month,'ss_mpr_modified_on'=>$created_on,'ss_mpr_created_on'=>$created_on);
				
				//create mpr summary
				$this->Reh_main_entry_model->createMprSummary($mpr_summ_data);
			}else{
				$mpr_summ_update_data = array('ss_mpr_modified_on'=>$created_on);
				$mpr_summ_update_where_data = array('ss_mpr_summary_id'=>$check_mpr_summ_exist->ss_mpr_summary_id);
				//update mpr summary
				
				$this->Reh_main_entry_model->updateMprSummary($mpr_summ_update_data,$mpr_summ_update_where_data);
			}
	
	}
	
}
