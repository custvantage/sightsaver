<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ueh_main_entry extends CI_Controller {

    public function __construct() {
        parent::__construct();
		check_login();
        $this->load->model('Reh_main_entry_model');
		$this->load->model('Main_excel_entry_model');
		$this->load->library('metricdata');
		$this->load->library('common');
    }

    public function index() {
		$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
        if(!empty($this->uri->segment(2)))
        {						
            $getdata = base64_decode($this->uri->segment(2));
            $explodedata = explode('-',$getdata);
            $project_name = $explodedata[0];
            $module_name = $explodedata[1];
			$created_on = server_date_time();
			$current_year = date('Y',strtotime($created_on));
             
            $data['sections'] = $this->Reh_main_entry_model->getAllSectionsUeh($project_name, $module_name,$current_year);
            $data['metrics_data'] = $this->Reh_main_entry_model->getAllMetricData($project_name,$module_name,$current_year);
			if($this->session->userdata('userinfo')['default_role'] != 6)//if pm/po
			{
				$data['partners'] = $this->common->getPartners();
			}
            $data['include'] = "ueh/ueh_main_entry";
            $this->load->view('container_login_dis', $data);
			
        }else
        {
            redirect('dashboard');
        }
    }
     public function create_ueh_main_entry()
	{
		$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
        if($this->input->server('REQUEST_METHOD') === 'POST')
		{	
			$getdata = base64_decode($this->uri->segment(2));
			$explodedata = explode('-',$getdata);
			$project_name = $explodedata[0];
			$module_name = $explodedata[1];
			
			$combine_day = "01-".$this->input->post('month_data');
			$data_month = date("Y-m-d",strtotime($combine_day));
			$year = date('Y',strtotime($combine_day));
				
			$this->form_validation->set_rules('month_data','Month','required');
					
			if($this->form_validation->run()==true)
			{
				
				$created_on = server_date_time();
				$partner_id = $_SESSION["partner_id"];				
								
				//One column
				if($this->input->post('matric_id_one_column'))
				{
					$one_column_metric_ids = $this->input->post('matric_id_one_column');//post one column metric id
					$post_one_column_data = $this->input->post('one_column_val');//post one column value
					
					foreach($one_column_metric_ids as $key_metrics_one_column=>$one_column_metric_id)
					{
						$one_column_data[] = array('ss_metric_master_id'=>$one_column_metric_id,'ss_partner_id'=>$partner_id,'ss_one_column_data_value'=>$post_one_column_data[$key_metrics_one_column],'ss_one_column_data_month'=>$data_month,'ss_one_column_data_created_on'=>$created_on);
					}
				     $this->Reh_main_entry_model->create_one_column($one_column_data);//One column data insert
					 $metrics_data_insert_one=$this->Reh_main_entry_model->getOneMetricDataInsertUeh($project_name,$module_name,$year);
					 foreach($metrics_data_insert_one as $key_one_column_insert)
					  {
						$one_column_insert_data[] = array('ss_metric_master_id'=>$key_one_column_insert->ss_metric_master_id,'ss_partner_id'=>$partner_id,'ss_one_column_data_value'=>'','ss_one_column_data_month'=>$data_month,'ss_one_column_data_created_on'=>$created_on);  
					  }
					  $this->Reh_main_entry_model->create_one_column($one_column_insert_data);//One column data insert
				}
				
				//Four Column
				if($this->input->post('matric_id_four_column'))
				{
					$four_column_metric_ids = $this->input->post('matric_id_four_column');//post four column metric id
					$post_four_column_men_data = $this->input->post('four_column_men_val');//post four column men value
					$post_four_column_women_data = $this->input->post('four_column_women_val');//post four column women value
					$post_four_column_trans_data = $this->input->post('four_column_trans_val');//post four column transgender value
					$post_four_column_boy_data = $this->input->post('four_column_boy_val');//post four column boy value
					$post_four_column_girl_data = $this->input->post('four_column_girl_val');//post four column girl value
					
					foreach($four_column_metric_ids as $key_four_column=>$four_column_metric_id)
					{
						$four_column_data[] = array('ss_metric_master_id'=>$four_column_metric_id,'ss_partner_id'=>$partner_id,'ss_four_column_data_value_men'=>$post_four_column_men_data[$key_four_column],'ss_four_column_data_value_women'=>$post_four_column_women_data[$key_four_column],'ss_four_column_data_value_trans'=>$post_four_column_trans_data[$key_four_column],'ss_four_column_data_value_boys'=>$post_four_column_boy_data[$key_four_column],'ss_four_column_data_value_girls'=>$post_four_column_girl_data[$key_four_column],'ss_four_column_data_month'=>$data_month,'ss_four_column_data_created_on'=>$created_on);
					}
					$this->Reh_main_entry_model->create_four_column($four_column_data);//Four column data insert
					$metrics_data_insert=$this->Reh_main_entry_model->getAllMetricDataInsertUehFour($project_name,$module_name,$year);
					//echo "<pre>"; print_r($metrics_data_insert); die;
					 foreach($metrics_data_insert as $key_four_column_insert)
					  {
						$four_column_insert_data[] = array('ss_metric_master_id'=>$key_four_column_insert->ss_metric_master_id,'ss_partner_id'=>$partner_id,'ss_four_column_data_value_men'=>'','ss_four_column_data_value_women'=>'','ss_four_column_data_value_trans'=>'','ss_four_column_data_value_boys'=>'','ss_four_column_data_value_girls'=>'','ss_four_column_data_month'=>$data_month,'ss_four_column_data_created_on'=>$created_on);  
					  }
					  $this->Reh_main_entry_model->create_four_column($four_column_insert_data);//Four column data insert
				   }
				 
				//Analysis
				if($this->input->post('matric_id_analysis'))
				{
					$analysis_metric_ids = $this->input->post('matric_id_analysis');//post analysis metric id
					$post_analysis_data = $this->input->post('analysis_value');//post analysis value
					
					foreach($analysis_metric_ids as $key_analysis=>$analysis_metric_id)
					{
						$analysis_data[] = array('ss_metric_master_id'=>$analysis_metric_id,'ss_partner_id'=>$partner_id,'ss_analysis_data_value'=>$post_analysis_data[$key_analysis],'ss_analysis_data_month'=>$data_month,'ss_analysis_data_created_on'=>$created_on);
					}
					$this->Reh_main_entry_model->create_analysis($analysis_data);//Analysis data insert
				}
								
				//Mpr summary
				$partner_info = $this->Reh_main_entry_model->getPartnerInfo($partner_id);//get partner info
				$check_mpr_summ_exist = $this->Reh_main_entry_model->checkMprExist($partner_id,$project_name,$module_name,$data_month);//check mpr exist
				if(empty($check_mpr_summ_exist))
				{
					$mpr_summ_data = array('ss_mpr_summary_partner_id'=>$partner_id,'ss_mpr_project_name'=>$project_name,'ss_mpr_module_name'=>$module_name,'ss_mpr_start_date'=>$partner_info->ss_partners_created_on,'status'=>1,'ss_mpr_report_month'=>$data_month,'ss_mpr_modified_on'=>$created_on,'ss_mpr_created_on'=>$created_on);
					
					//create mpr summary
					$this->Reh_main_entry_model->createMprSummary($mpr_summ_data);
				}else{
					$mpr_summ_update_data = array('ss_mpr_modified_on'=>$created_on);
					$mpr_summ_update_where_data = array('ss_mpr_summary_id'=>$check_mpr_summ_exist->ss_mpr_summary_id);
					//update mpr summary
					
					$this->Reh_main_entry_model->updateMprSummary($mpr_summ_update_data,$mpr_summ_update_where_data);
				}
			}
			$data['sections'] = $this->Reh_main_entry_model->getAllSections($project_name, $module_name,$year);
            $data['metrics_data'] = $this->Reh_main_entry_model->getAllMetricData($project_name,$module_name,$year);           
			$this->session->set_flashdata('success', 'Your data is successfully saved.');
            $data['include'] = "ueh/ueh_main_entry";
            $this->load->view('container_login_dis', $data);
			
		}else{
			redirect('ueh_main_entry/'.$this->uri->segment(2));
		}
	}
	public function manage()
	{
		$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$getdata = base64_decode($this->uri->segment(2));
			$explodedata = explode('-',$getdata);
			$project_name = $explodedata[0];
			$module_name = $explodedata[1];
			$combine_day = "01-".$this->input->post('month_from');		
            $data['data1']=	$this->input->post('month_from'); 
			$data['post_month_from'] = date("Y-m-d",strtotime($combine_day));
			$year = date('Y',strtotime($combine_day));
			$data['mpr_report_month'] = $data['post_month_from'];
			if($this->session->userdata('userinfo')['default_role'] == 6)//if partner
			{
				$data['session_partner_id'] = $_SESSION["partner_id"];
				$check_mpr_summ_exist = $this->Reh_main_entry_model->checkMprExistArr($data['session_partner_id'],$project_name,$module_name,$data['post_month_from']);//check mpr exist	
                //echo "<pre>"; print_R($check_mpr_summ_exist); die;
                 $data['status'] = $check_mpr_summ_exist['status'];				
				if($check_mpr_summ_exist['status'] == 1 || $check_mpr_summ_exist['status'] == 4)
				{
					$data['mpr_id'] = $check_mpr_summ_exist['ss_mpr_summary_id'];
					
					//$data['mpr_report_month'] = $check_mpr_summ_exist['ss_mpr_report_month'];
				}else{
					$data['mpr_id'] = "";
					
					//$data['mpr_report_month'] = "";
				}
			}else{
				$data['session_partner_id'] = $this->input->post('partner_name');				
				$data['partners'] = $this->common->getPartners();
				$user_id = $this->session->userdata('userinfo')['user_id'];
				$data['comments'] = $this->common->getComments($user_id,$data['post_month_from'],$data['session_partner_id'],$project_name,$module_name);
				$data['comments_pmpo'] = $this->common->getPmpoComments($data['session_partner_id'],$data['post_month_from'],$project_name,$module_name);
			}
			
			$data['sections'] = $this->Reh_main_entry_model->getAllSectionsUeh($project_name, $module_name,$year);
			$data['metrics_data'] = $this->Reh_main_entry_model->getAllMetricData($project_name,$module_name,$year);			
			
			$data['include'] = "ueh/manage_ueh_main_entry";
			$this->load->view('container_login_dis', $data);
		}else{
			$this->session->set_flashdata('success', 'Your data is successfully saved.');
			redirect('ueh_main_entry/'.$this->uri->segment(2));
		}
	}
	public function excelupload_ueh() 
	{
		$getdata = base64_decode($this->uri->segment(2));
		$explodedata = explode('-',$getdata);
		$project_name = $explodedata[0];
		$module_name = $explodedata[1];
		if(!empty($_FILES['upload_publish_file']['name']))
					{
						require_once(EXCELDIR."PHPExcel.php");
						$excel_file= $_FILES['upload_publish_file']['tmp_name'];
						$inputFileName = $excel_file; 
							try 
								{
								$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
								} 
							catch(Exception $e)
								{
								die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
								}
								require_once(EXCELDIR."PHPExcel/IOFactory.php");

								$objReader = PHPExcel_IOFactory::createReader('Excel2007');
										$inputFileType = 'Excel2007';
										//$inputFileName = 'hello.xlsx';
										$sheetIndex = 0;
										$objReader = PHPExcel_IOFactory::createReader($inputFileType);
										$sheetnames = $objReader->listWorksheetNames($inputFileName);
										$objReader->setLoadSheetsOnly($sheetnames[$sheetIndex]);
										$objPHPExcel = $objReader->load($inputFileName);
										$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
										// 1- for the OPD///////
									  $date1=0;
                                        $start=5;
											for ($row = $start; $row <=5; $row++) 
											{									
											$month = $objPHPExcel->getActiveSheet()->getCell('D'.$row)->getValue();
												$year = $objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue();
											    $month1 = $monthname = date("m", strtotime($month)); 
									                $date1 = $year."-".$month1."-".'01';  
											} 	
											 // start of the four column  /////       
								 $metric_map_detail_ueh_four_column=$this->Main_excel_entry_model->metric_map_detail_ueh_four_column($year);
								// echo "<pre>"; print_R($metric_map_detail_ueh_one_column); die;
								 foreach($metric_map_detail_ueh_four_column as $key=>$metric_map_detail_ueh_four_column)
								 {
									$metric_id=$metric_map_detail_ueh_four_column->ss_excel_metric_map_mertic_master_id; 
									$metric_ref=$metric_map_detail_ueh_four_column->ss_excel_metric_map_excel_cell_ref;
									 $ref = substr("$metric_ref", 1, 3); 
															  
								  if($ref!=0 || $ref!="") 
								  {
									 $cal_data[] = array('ss_partner_id'=>$this->session->userdata('userinfo')['partner_id'],
									'ss_metric_master_id'=>$metric_id,
									'ss_four_column_data_value_men'=>$objPHPExcel->getActiveSheet()->getCell('L'."$ref")->getValue(),
									'ss_four_column_data_value_women'=>$objPHPExcel->getActiveSheet()->getCell('M'."$ref")->getValue(),
									'ss_four_column_data_value_boys'=>$objPHPExcel->getActiveSheet()->getCell('N'."$ref")->getValue(),
									'ss_four_column_data_value_girls'=>$objPHPExcel->getActiveSheet()->getCell('O'."$ref")->getValue(),
									'ss_four_column_data_month'=>$date1,
									'ss_four_column_data_created_on'=>server_date_time());	
								    
								   }	
								}
								
                                // end of the four column data ///
								// start of the one column data  ///
								$metric_map_one_ueh=$this->Main_excel_entry_model->metric_map_detail_ueh_one_column($year);
							
								 foreach($metric_map_one_ueh as $key=>$metric_map_one_ueh)
								 {
									$metric_id=$metric_map_one_ueh->ss_excel_metric_map_mertic_master_id; 
								    $metric_ref=$metric_map_one_ueh->ss_excel_metric_map_excel_cell_ref; 
									
									$ref = substr("$metric_ref", 1, 3); 
									    							  
								  if($ref!=0 || $ref!="") 
								  {
									 $cal_one_data[] = array('ss_partner_id'=>$this->session->userdata('userinfo')['partner_id'],
									'ss_metric_master_id'=>$metric_id,
									'ss_one_column_data_value'=>$objPHPExcel->getActiveSheet()->getCell('L'."$ref")->getValue(),
									'ss_one_column_data_month'=>$date1,
									'ss_one_column_data_created_on'=>server_date_time()); 	
								   }	
								} 
								
								// end of the one column  ////
								// start of the analysis///
								$metric_map_analysis_ueh=$this->Main_excel_entry_model->metric_map_detail_analysis_ueh($year);
								if(!empty($metric_map_analysis_ueh))
								{
								 foreach($metric_map_analysis_ueh as $key=>$metric_map_analysis_ueh)
								 {
									$metric_id=$metric_map_analysis_ueh->ss_excel_metric_map_mertic_master_id; 
									$metric_ref=$metric_map_analysis_ueh->ss_excel_metric_map_excel_cell_ref;
									
								
									$ref = substr("$metric_ref", 1, 3);
									  							  
								  if($ref!=0 || $ref!="") 
								  {
									 $analysis_data[] = array('ss_partner_id'=>$this->session->userdata('userinfo')['partner_id'],
												'ss_metric_master_id'=>$metric_id,
												'ss_analysis_data_value'=>$objPHPExcel->getActiveSheet()->getCell('C'."$ref")->getValue(),
												'ss_analysis_data_month'=>$date1,
												'ss_analysis_data_created_on'=>server_date_time());
								   }	
								}
								}else{
									 $analysis_data[] = array('ss_partner_id'=>$this->session->userdata('userinfo')['partner_id'],
												'ss_metric_master_id'=>$metric_id,
												'ss_analysis_data_value'=>"",
												'ss_analysis_data_month'=>$date1,
												'ss_analysis_data_created_on'=>server_date_time());
								}
								
						
                                        $this->Main_excel_entry_model->four_column_excel_upload($cal_data);
                                        $this->Main_excel_entry_model->analysis_excel_upload($analysis_data);
                                        $this->Main_excel_entry_model->one_column_excel_upload($cal_one_data);
						
							$partner_info = $this->Reh_main_entry_model->getPartnerInfo($this->session->userdata('userinfo')['partner_id']);
							$check_mpr_summ_exist = $this->Reh_main_entry_model->checkMprExist($this->session->userdata('userinfo')['partner_id'],$project_name,$module_name,$date1);
							if(empty($check_mpr_summ_exist))
							{
							$mpr_summery = array('ss_mpr_summary_partner_id'=>$this->session->userdata('userinfo')['partner_id'],
							'ss_mpr_project_name'=>$project_name,
							'ss_mpr_module_name'=>$module_name,
							'ss_mpr_report_month'=>$date1,							
							'status'=>2,
							'ss_mpr_modified_on'=>server_date_time(),
							'ss_mpr_created_on'=>server_date_time());
							$this->Reh_main_entry_model->createMprSummary($mpr_summery);						
							}else{
								$mpr_summ_update_data = array('ss_mpr_modified_on'=>server_date_time());
								$mpr_summ_update_where_data = array('ss_mpr_summary_id'=>$check_mpr_summ_exist->ss_mpr_summary_id);
								$this->Reh_main_entry_model->updateMprSummary($mpr_summ_update_data,$mpr_summ_update_where_data);
							}
						
					}
		$data['sections'] = $this->Reh_main_entry_model->getAllSections($project_name, $module_name,$year);
				
		$data['metrics_data'] = $this->Reh_main_entry_model->getAllMetricData($project_name,$module_name,$year);	
		
	  $data['include'] = "ueh/ueh_main_entry";
	  $this->load->view('container_login_dis', $data);
	}	
}
