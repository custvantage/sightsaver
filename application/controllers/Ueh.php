<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ueh extends CI_Controller {

    public function __construct() {
        parent::__construct();
		check_login();
		$this->load->model('Ueh_model');
		$this->load->model('Reh_main_entry_model');
	}

    public function ueh_vision_center() 
	{
		$data['vc_data'] = "";
		$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		$data['include'] = "ueh/ueh_vision_center";
		$this->load->view('container_login_dis', $data);
	}
	public function create_vc()
	{
		$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
		
			$this->form_validation->set_rules('month_data','Month','required');
					
			if($this->form_validation->run()==true)
			{
				$partner_id = $_SESSION["partner_id"];
				$created_on = server_date_time();
				
				$id_ajax = $this->input->post('id_ajax');
				$combine_day = "01-".$this->input->post('month_data');
				$data_month = date("Y-m-d",strtotime($combine_day));
				$vc_name = $this->input->post('vc_name');
				$open_days = $this->input->post('open_days');
				$vc_support = $this->input->post('vc_support');
				
				$screen_male = $this->input->post('screen_male');
				$screen_female = $this->input->post('screen_female');
				$screen_trans = $this->input->post('screen_trans');
				$screen_boy = $this->input->post('screen_boy');
				$screen_girl = $this->input->post('screen_girl');
				
				$refra_person_male = $this->input->post('refra_person_male');
				$refra_person_female = $this->input->post('refra_person_female');
				$refra_person_trans = $this->input->post('refra_person_trans');
				$refra_person_boy = $this->input->post('refra_person_boy');
				$refra_person_girl = $this->input->post('refra_person_girl');
				
				$refra_spectacles_male = $this->input->post('refra_spectacles_male');
				$refra_spectacles_female = $this->input->post('refra_spectacles_female');
				$refra_spectacles_trans = $this->input->post('refra_spectacles_trans');
				$refra_spectacles_boy = $this->input->post('refra_spectacles_boy');				
				$refra_spectacles_girl = $this->input->post('refra_spectacles_girl');
				
				$refra_dispensed_spectacles_male = $this->input->post('refra_dispensed_spectacles_male');
				$refra_dispensed_spectacles_female = $this->input->post('refra_dispensed_spectacles_female');
				$refra_dispensed_spectacles_trans = $this->input->post('refra_dispensed_spectacles_trans');
				$refra_dispensed_spectacles_boy = $this->input->post('refra_dispensed_spectacles_boy');
				$refra_dispensed_spectacles_girl = $this->input->post('refra_dispensed_spectacles_girl');
				
				$refra_purchage_male = $this->input->post('refra_purchage_male');
				$refra_purchage_female = $this->input->post('refra_purchage_female');
				$refra_purchage_trans = $this->input->post('refra_purchage_trans');
				$refra_purchage_boy = $this->input->post('refra_purchage_boy');
				$refra_purchage_girl = $this->input->post('refra_purchage_girl');
				
				$refer_persons_male = $this->input->post('refer_persons_male');
				$refer_persons_female = $this->input->post('refer_persons_female');
				$refer_persons_trans = $this->input->post('refer_persons_trans');
				$refer_persons_boy = $this->input->post('refer_persons_boy');
				$refer_persons_girl = $this->input->post('refer_persons_girl');
				
				$refer_cataract_male = $this->input->post('refer_cataract_male');
				$refer_cataract_female = $this->input->post('refer_cataract_female');
				$refer_cataract_trans = $this->input->post('refer_cataract_trans');
				$refer_cataract_boy = $this->input->post('refer_cataract_boy');
				$refer_cataract_girl = $this->input->post('refer_cataract_girl');
				
				$treat_male = $this->input->post('treat_male');
				$treat_female = $this->input->post('treat_female');
				$treat_trans = $this->input->post('treat_trans');
				$treat_boy = $this->input->post('treat_boy');
				$treat_girl = $this->input->post('treat_girl');
				
				$dr_diabetes_male = $this->input->post('dr_diabetes_male');
				$dr_diabetes_female = $this->input->post('dr_diabetes_female');
				$dr_diabetes_trans = $this->input->post('dr_diabetes_trans');
				$dr_diabetes_boy = $this->input->post('dr_diabetes_boy');
				$dr_diabetes_girl = $this->input->post('dr_diabetes_girl');
				
				$dr_dr_male = $this->input->post('dr_dr_male');
				$dr_dr_female = $this->input->post('dr_dr_female');
				$dr_dr_trans = $this->input->post('dr_dr_trans');
				$dr_dr_boy = $this->input->post('dr_dr_boy');
				$dr_dr_girl = $this->input->post('dr_dr_girl');
				
				$dr_methods_male = $this->input->post('dr_methods_male');
				$dr_methods_female = $this->input->post('dr_methods_female');
				$dr_methods_trans = $this->input->post('dr_methods_trans');
				$dr_methods_boy = $this->input->post('dr_methods_boy');
				$dr_methods_girl = $this->input->post('dr_methods_girl');
				
				if($this->input->post('id_ajax')=='')
				{
				$vc_data = array('ss_partner_id'=>$partner_id,'ss_vc_name'=>$vc_name,'ss_vc_open_days'=>$open_days,'ss_vc_support'=>$vc_support,'ss_screen_male'=>$screen_male,'ss_screen_female'=>$screen_female,'ss_screen_boy'=>$screen_boy,'ss_screen_trans'=>$screen_trans,'ss_screen_girl'=>$screen_girl,
				
				'ss_refra_person_male'=>$refra_person_male,'ss_refra_person_female'=>$refra_person_female,'ss_refra_person_trans'=>$refra_person_trans,'ss_refra_person_boy'=>$refra_person_boy,'ss_refra_person_girl'=>$refra_person_girl,
				
				'ss_refra_spectacles_male'=>$refra_spectacles_male,'ss_refra_spectacles_female'=>$refra_spectacles_female,'ss_refra_spectacles_trans'=>$refra_spectacles_trans,'ss_refra_spectacles_boy'=>$refra_spectacles_boy,'ss_refra_spectacles_girl'=>$refra_spectacles_girl,
				
				'ss_refra_dispensed_spectacles_male'=>$refra_dispensed_spectacles_male,'ss_refra_dispensed_spectacles_female'=>$refra_dispensed_spectacles_female,'ss_refra_dispensed_spectacles_trans'=>$refra_dispensed_spectacles_trans,'ss_refra_dispensed_spectacles_boy'=>$refra_dispensed_spectacles_boy,'ss_refra_dispensed_spectacles_girl'=>$refra_dispensed_spectacles_girl,
				
				'ss_refra_purchage_male'=>$refra_purchage_male,'ss_refra_purchage_female'=>$refra_purchage_female,'ss_refra_purchage_trans'=>$refra_purchage_trans,'ss_refra_purchage_boy'=>$refra_purchage_boy,'ss_refra_purchage_girls'=>$refra_purchage_girl,
								
				'ss_refer_persons_male'=>$refer_persons_male,'ss_refer_persons_female'=>$refer_persons_female,'ss_refer_persons_trans'=>$refer_persons_trans,'ss_refer_persons_boy'=>$refer_persons_boy,'ss_refer_persons_girl'=>$refer_persons_girl,
				
				'ss_refer_cataract_male'=>$refer_cataract_male,'ss_refer_cataract_female'=>$refer_cataract_female,'ss_refer_cataract_trans'=>$refer_cataract_trans,'ss_refer_cataract_boy'=>$refer_cataract_boy,'ss_refer_cataract_girl'=>$refer_cataract_girl,
				
				'ss_treat_male'=>$treat_male,'ss_treat_female'=>$treat_female,'ss_treat_trans'=>$treat_trans,'ss_treat_boy'=>$treat_boy,'ss_treat_girl'=>$treat_girl,
				
				'ss_dr_diabetes_male'=>$dr_diabetes_male,'ss_dr_diabetes_female'=>$dr_diabetes_female,'ss_dr_diabetes_trans'=>$dr_diabetes_trans,'ss_dr_diabetes_boy'=>$dr_diabetes_boy,'ss_dr_diabetes_girl'=>$dr_diabetes_girl,
				
				'ss_dr_dr_male'=>$dr_dr_male,'ss_dr_dr_female'=>$dr_dr_female,'ss_dr_dr_trans'=>$dr_dr_trans,'ss_dr_dr_boy'=>$dr_dr_boy,'ss_dr_dr_girl'=>$dr_dr_girl,
				
				'ss_methods_male'=>$dr_methods_male,'ss_methods_female'=>$dr_methods_female,'ss_dr_methods_trans'=>$dr_methods_trans,'ss_methods_boy'=>$dr_methods_boy,'ss_methods_girl'=>$dr_methods_girl,
				
				'ss_ueh_vc_month'=>$data_month,'ss_ueh_vc_created_on'=>$created_on);
				
				$creation_check = $this->Ueh_model->createVc($vc_data);
				}
				else{
					$update_vc_data = array('ss_partner_id'=>$partner_id,'ss_vc_name'=>$vc_name,'ss_vc_open_days'=>$open_days,'ss_vc_support'=>$vc_support,'ss_screen_male'=>$screen_male,'ss_screen_female'=>$screen_female,'ss_screen_boy'=>$screen_boy,'ss_screen_trans'=>$screen_trans,'ss_screen_girl'=>$screen_girl,
				
				'ss_refra_person_male'=>$refra_person_male,'ss_refra_person_female'=>$refra_person_female,'ss_refra_person_trans'=>$refra_person_trans,'ss_refra_person_boy'=>$refra_person_boy,'ss_refra_person_girl'=>$refra_person_girl,
				
				'ss_refra_spectacles_male'=>$refra_spectacles_male,'ss_refra_spectacles_female'=>$refra_spectacles_female,'ss_refra_spectacles_trans'=>$refra_spectacles_trans,'ss_refra_spectacles_boy'=>$refra_spectacles_boy,'ss_refra_spectacles_girl'=>$refra_spectacles_girl,
				
				'ss_refra_dispensed_spectacles_male'=>$refra_dispensed_spectacles_male,'ss_refra_dispensed_spectacles_female'=>$refra_dispensed_spectacles_female,'ss_refra_dispensed_spectacles_trans'=>$refra_dispensed_spectacles_trans,'ss_refra_dispensed_spectacles_boy'=>$refra_dispensed_spectacles_boy,'ss_refra_dispensed_spectacles_girl'=>$refra_dispensed_spectacles_girl,
				
				'ss_refra_purchage_male'=>$refra_purchage_male,'ss_refra_purchage_female'=>$refra_purchage_female,'ss_refra_purchage_trans'=>$refra_purchage_trans,'ss_refra_purchage_boy'=>$refra_purchage_boy,'ss_refra_purchage_girls'=>$refra_purchage_girl,
				
				'ss_refer_persons_male'=>$refer_persons_male,'ss_refer_persons_female'=>$refer_persons_female,'ss_refer_persons_trans'=>$refer_persons_trans,'ss_refer_persons_boy'=>$refer_persons_boy,'ss_refer_persons_girl'=>$refer_persons_girl,
				
				'ss_refer_cataract_male'=>$refer_cataract_male,'ss_refer_cataract_female'=>$refer_cataract_female,'ss_refer_cataract_trans'=>$refer_cataract_trans,'ss_refer_cataract_boy'=>$refer_cataract_boy,'ss_refer_cataract_girl'=>$refer_cataract_girl,
				
				'ss_treat_male'=>$treat_male,'ss_treat_female'=>$treat_female,'ss_treat_trans'=>$treat_trans,'ss_treat_boy'=>$treat_boy,'ss_treat_girl'=>$treat_girl,
				
				'ss_dr_diabetes_male'=>$dr_diabetes_male,'ss_dr_diabetes_female'=>$dr_diabetes_female,'ss_dr_diabetes_trans'=>$dr_diabetes_trans,'ss_dr_diabetes_boy'=>$dr_diabetes_boy,'ss_dr_diabetes_girl'=>$dr_diabetes_girl,
				
				'ss_dr_dr_male'=>$dr_dr_male,'ss_dr_dr_female'=>$dr_dr_female,'ss_dr_dr_trans'=>$dr_dr_trans,'ss_dr_dr_boy'=>$dr_dr_boy,'ss_dr_dr_girl'=>$dr_dr_girl,
				
				'ss_methods_male'=>$dr_methods_male,'ss_methods_female'=>$dr_methods_female,'ss_dr_methods_trans'=>$dr_methods_trans,'ss_methods_boy'=>$dr_methods_boy,'ss_methods_girl'=>$dr_methods_girl,
				
				'ss_ueh_vc_month'=>$data_month,'ss_ueh_vc_created_on'=>$created_on);
				
				$creation_check = $this->Ueh_model->updateVc($update_vc_data,$id_ajax);
					
				}
				
				
				if(!empty($creation_check))
				{
					$msg = "Information filled successfully";
				}else{
					$msg = "Please try after sometime";
				}
				$this->session->set_flashdata('success',$msg);
				redirect('filter_vc_ueh?month_from='.$this->input->post('month_data'));
			}else{
				$data['include'] = "ueh/ueh_vision_center";
			$this->load->view('container_login_dis', $data);
			}
		}
	}
	
	public function filter_vc_data()
	{   $userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		if($this->input->server('REQUEST_METHOD') === 'GET' and $this->input->get('month_from')!=null and preg_match('/^[0-9]{2}-[0-9]{4}\z/', $this->input->get('month_from')))
		{
			
			/*$this->form_validation->set_rules('month_from','Month','required');
					
			if($this->form_validation->run()==true)
			{*/
				$combine_day = "01-".$this->input->get('month_from');
				$month_from = date("Y-m-d",strtotime($combine_day));
				
				
				
				$partner_id = $_SESSION["partner_id"];
				$data['vc_data'] = $this->Ueh_model->getVcData($month_from,$partner_id);
				
			/*}*/
			$data['include'] = "ueh/ueh_vision_center";
			$this->load->view('container_login_dis', $data);
		}else{
			redirect('ueh_vision_center');
		}
	}

	public function delete_vc_ueh()
	{ 
		if(!empty($this->uri->segment(2)))
        {
	     $id = base64_decode($this->uri->segment(2));
		 $this->Ueh_model->deleteUehVc($id);
	     redirect('ueh_vision_center');
		}
	}
	
	public function edit_vc_ueh()
	{ 
	     $id = base64_decode($this->input->post('id'));
		 $data=$this->Ueh_model->editFetchData($id);
		 if(!empty($data))
		 {
			 $data2 = json_encode($data[0]);
			 echo $data2."$$$".$this->security->get_csrf_hash();
		 }
		 else
		 {
			 echo 0;
		 }
	}
	
	
	
	

///////////////// end of the vesion center code/////////////////////////////////

	
	public function ueh_base_hospital() 
	{
		$data['bh_data'] = "";
		$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		$data['include'] = "ueh/ueh_base_hospital";
		$this->load->view('container_login_dis', $data);
	}
	public function create_base_hospital()
	{
		$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
		
			$this->form_validation->set_rules('month_data','Month','required');
					
			if($this->form_validation->run()==true)
			{
				$partner_id = $_SESSION["partner_id"];
				$created_on = server_date_time();
				$id_ajax = $this->input->post('id_ajax');
				$combine_day = "01-".$this->input->post('month_data');
				$data_month = date("Y-m-d",strtotime($combine_day));
				$agency_name = $this->input->post('agency_name');
				
				$exam_opd_male = $this->input->post('exam_opd_male');
				$exam_opd_female = $this->input->post('exam_opd_female');
				$exam_opd_boy = $this->input->post('exam_opd_boy');
				$exam_opd_girl = $this->input->post('exam_opd_girl');
				
				$refer_vc_male = $this->input->post('refer_vc_male');
				$refer_vc_female = $this->input->post('refer_vc_female');
				$refer_vc_boy = $this->input->post('refer_vc_boy');
				$refer_vc_girl = $this->input->post('refer_vc_girl');
				
				$refer_camp_male = $this->input->post('refer_camp_male');
				$refer_camp_female = $this->input->post('refer_camp_female');
				$refer_camp_boy = $this->input->post('refer_camp_boy');
				$refer_camp_girl = $this->input->post('refer_camp_girl');
				
				$refer_walk_male = $this->input->post('refer_walk_male');
				$refer_walk_female = $this->input->post('refer_walk_female');
				$refer_walk_boy = $this->input->post('refer_walk_boy');
				$refer_walk_girl = $this->input->post('refer_walk_girl');
				
				$refra_refra_male = $this->input->post('refra_refra_male');
				$refra_refra_female = $this->input->post('refra_refra_female');
				$refra_refra_boy = $this->input->post('refra_refra_boy');
				$refra_refra_girl = $this->input->post('refra_refra_girl');	
				
				$refra_pres_male = $this->input->post('refra_pres_male');
				$refra_pres_female = $this->input->post('refra_pres_female');
				$refra_pres_boy = $this->input->post('refra_pres_boy');
				$refra_pres_girl = $this->input->post('refra_pres_girl');
				
				$refra_disp_male = $this->input->post('refra_disp_male');
				$refra_disp_female = $this->input->post('refra_disp_female');
				$refra_disp_boy = $this->input->post('refra_disp_boy');
				$refra_disp_girl = $this->input->post('refra_disp_girl');
				
				$refra_purc_male = $this->input->post('refra_purc_male');
				$refra_purc_female = $this->input->post('refra_purc_female');
				$refra_purc_boy = $this->input->post('refra_purc_boy');
				$refra_purc_girl = $this->input->post('refra_purc_girl');
				
				$treat_withoutsurg_male = $this->input->post('treat_withoutsurg_male');
				$treat_withoutsurg_female = $this->input->post('treat_withoutsurg_female');
				$treat_withoutsurg_boy = $this->input->post('treat_withoutsurg_boy');
				$treat_withoutsurg_girl = $this->input->post('treat_withoutsurg_girl');
				
				$treat_subdfree_male = $this->input->post('treat_subdfree_male');
				$treat_subdfree_female = $this->input->post('treat_subdfree_female');
				$treat_subdfree_boy = $this->input->post('treat_subdfree_boy');
				$treat_subdfree_girl = $this->input->post('treat_subdfree_girl');
				
				$treat_subd_male = $this->input->post('treat_subd_male');
				$treat_subd_female = $this->input->post('treat_subd_female');
				$treat_subd_boy = $this->input->post('treat_subd_boy');
				$treat_subd_girl = $this->input->post('treat_subd_girl');
				
				$treat_catara_male = $this->input->post('treat_catara_male');
				$treat_catara_female = $this->input->post('treat_catara_female');
				$treat_catara_boy = $this->input->post('treat_catara_boy');
				$treat_catara_girl = $this->input->post('treat_catara_girl');
								
				$treat_glau_male = $this->input->post('treat_glau_male');
				$treat_glau_female = $this->input->post('treat_glau_female');
				$treat_glau_boy = $this->input->post('treat_glau_boy');
				$treat_glau_girl = $this->input->post('treat_glau_girl');
				
				$treat_dr_male = $this->input->post('treat_dr_male');
				$treat_dr_female = $this->input->post('treat_dr_female');
				$treat_dr_boy = $this->input->post('treat_dr_boy');
				$treat_dr_girl = $this->input->post('treat_dr_girl');
				
				if($this->input->post('id_ajax')=='')
				{
				$bh_data = array('ss_partner_id'=>$partner_id,'ss_agency_name'=>$agency_name,
				
				'ss_exam_opd_male'=>$exam_opd_male,'ss_exam_opd_female'=>$exam_opd_female,'ss_exam_opd_boy'=>$exam_opd_boy,'ss_exam_opd_girl'=>$exam_opd_girl,
				
				'ss_refer_vc_male'=>$refer_vc_male,'ss_refer_vc_female'=>$refer_vc_female,'ss_refer_vc_boy'=>$refer_vc_boy,'ss_refer_vc_girl'=>$refer_vc_girl,
				
				'ss_refer_camp_male'=>$refer_camp_male,'ss_refer_camp_female'=>$refer_camp_female,'ss_refer_camp_boy'=>$refer_camp_boy,'ss_refer_camp_girl'=>$refer_camp_girl,
				
				'ss_refer_walk_male'=>$refer_walk_male,'ss_refer_walk_female'=>$refer_walk_female,'ss_refer_walk_boy'=>$refer_walk_boy,'ss_refer_walk_girl'=>$refer_walk_girl,
				
				'ss_refra_refra_male'=>$refra_refra_male,'ss_refra_refra_female'=>$refra_refra_female,'ss_refra_refra_boy'=>$refra_refra_boy,'ss_refra_refra_girl'=>$refra_refra_girl,
				
				'ss_refra_pres_male'=>$refra_pres_male,'ss_refra_pres_female'=>$refra_pres_female,'ss_refra_pres_boy'=>$refra_pres_boy,'ss_refra_pres_girl'=>$refra_pres_girl,
				
				'ss_refra_disp_male'=>$refra_disp_male,'ss_refra_disp_female'=>$refra_disp_female,'ss_refra_disp_boy'=>$refra_disp_boy,'ss_refra_disp_girl'=>$refra_disp_girl,
				
				'ss_refra_purc_male'=>$refra_purc_male,'ss_refra_purc_female'=>$refra_purc_female,'ss_refra_purc_boy'=>$refra_purc_boy,'ss_refra_purc_girl'=>$refra_purc_girl,
				
				'ss_ptreat_sur_male'=>$treat_withoutsurg_male,'ss_ptreat_sur_female'=>$treat_withoutsurg_female,'ss_ptreat_sur_boy'=>$treat_withoutsurg_boy,'ss_ptreat_sur_girl'=>$treat_withoutsurg_girl,
				
				'ss_treat_subdfree_male'=>$treat_subdfree_male,'ss_treat_subdfree_female'=>$treat_subdfree_female,'ss_treat_subdfree_boy'=>$treat_subdfree_boy,'ss_treat_subdfree_girl'=>$treat_subdfree_girl,
				
				'ss_treat_subd_male'=>$treat_subd_male,'ss_treat_subd_female'=>$treat_subd_female,'ss_treat_subd_boy'=>$treat_subd_boy,'ss_treat_subd_girl'=>$treat_subd_girl,
				
				'ss_treat_catara_male'=>$treat_catara_male,'ss_treat_catara_female'=>$treat_catara_female,'ss_treat_catara_boy'=>$treat_catara_boy,'ss_treat_catara_girl'=>$treat_catara_girl,
				
				'ss_treat_glau_male'=>$treat_glau_male,'ss_treat_glau_female'=>$treat_glau_female,'ss_treat_glau_boy'=>$treat_glau_boy,'ss_treat_glau_girl'=>$treat_glau_girl,
				
				'ss_treat_sur_male'=>$treat_dr_male,'ss_treat_sur_female'=>$treat_dr_female,'ss_treat_sur_boy'=>$treat_dr_boy,'ss_treat_sur_girl'=>$treat_dr_girl,
				
				'ss_ueh_base_month'=>$data_month,'ss_ueh_base_created_on'=>$created_on);
				//echo "<pre>"; print_R($bh_data); die;
				$creation_check = $this->Ueh_model->createBhHos($bh_data);
				}else
				{
					$update_bh_data = array('ss_partner_id'=>$partner_id,'ss_agency_name'=>$agency_name,
				
				'ss_exam_opd_male'=>$exam_opd_male,'ss_exam_opd_female'=>$exam_opd_female,'ss_exam_opd_boy'=>$exam_opd_boy,'ss_exam_opd_girl'=>$exam_opd_girl,
				
				'ss_refer_vc_male'=>$refer_vc_male,'ss_refer_vc_female'=>$refer_vc_female,'ss_refer_vc_boy'=>$refer_vc_boy,'ss_refer_vc_girl'=>$refer_vc_girl,
				
				'ss_refer_camp_male'=>$refer_camp_male,'ss_refer_camp_female'=>$refer_camp_female,'ss_refer_camp_boy'=>$refer_camp_boy,'ss_refer_camp_girl'=>$refer_camp_girl,
				
				'ss_refer_walk_male'=>$refer_walk_male,'ss_refer_walk_female'=>$refer_walk_female,'ss_refer_walk_boy'=>$refer_walk_boy,'ss_refer_walk_girl'=>$refer_walk_girl,
				
				'ss_refra_refra_male'=>$refra_refra_male,'ss_refra_refra_female'=>$refra_refra_female,'ss_refra_refra_boy'=>$refra_refra_boy,'ss_refra_refra_girl'=>$refra_refra_girl,
				
				'ss_refra_pres_male'=>$refra_pres_male,'ss_refra_pres_female'=>$refra_pres_female,'ss_refra_pres_boy'=>$refra_pres_boy,'ss_refra_pres_girl'=>$refra_pres_girl,
				
				'ss_refra_disp_male'=>$refra_disp_male,'ss_refra_disp_female'=>$refra_disp_female,'ss_refra_disp_boy'=>$refra_disp_boy,'ss_refra_disp_girl'=>$refra_disp_girl,
				
				'ss_refra_purc_male'=>$refra_purc_male,'ss_refra_purc_female'=>$refra_purc_female,'ss_refra_purc_boy'=>$refra_purc_boy,'ss_refra_purc_girl'=>$refra_purc_girl,
				
				'ss_ptreat_sur_male'=>$treat_withoutsurg_male,'ss_ptreat_sur_female'=>$treat_withoutsurg_female,'ss_ptreat_sur_boy'=>$treat_withoutsurg_boy,'ss_ptreat_sur_girl'=>$treat_withoutsurg_girl,
				
				'ss_treat_subdfree_male'=>$treat_subdfree_male,'ss_treat_subdfree_female'=>$treat_subdfree_female,'ss_treat_subdfree_boy'=>$treat_subdfree_boy,'ss_treat_subdfree_girl'=>$treat_subdfree_girl,
				
				'ss_treat_subd_male'=>$treat_subd_male,'ss_treat_subd_female'=>$treat_subd_female,'ss_treat_subd_boy'=>$treat_subd_boy,'ss_treat_subd_girl'=>$treat_subd_girl,
				
				'ss_treat_catara_male'=>$treat_catara_male,'ss_treat_catara_female'=>$treat_catara_female,'ss_treat_catara_boy'=>$treat_catara_boy,'ss_treat_catara_girl'=>$treat_catara_girl,
				
				'ss_treat_glau_male'=>$treat_glau_male,'ss_treat_glau_female'=>$treat_glau_female,'ss_treat_glau_boy'=>$treat_glau_boy,'ss_treat_glau_girl'=>$treat_glau_girl,
				
				'ss_treat_sur_male'=>$treat_dr_male,'ss_treat_sur_female'=>$treat_dr_female,'ss_treat_sur_boy'=>$treat_dr_boy,'ss_treat_sur_girl'=>$treat_dr_girl,
				
				'ss_ueh_base_month'=>$data_month,'ss_ueh_base_created_on'=>$created_on);
				//echo "<pre>"; print_R($bh_data); die;
				$creation_check = $this->Ueh_model->updateBhHos($update_bh_data,$id_ajax);
				}
				
				
				
				
				if(!empty($creation_check))
				{
					$msg = "Information filled successfully";
				}else{
					$msg = "Please try after sometime";
				}
				$this->session->set_flashdata('success',$msg);
				redirect('filter_bh_ueh?month_from='.$this->input->post('month_data'));
			}else{
				$data['include'] = "ueh/ueh_base_hospital";
			$this->load->view('container_login_dis', $data);
			}
		}
	}
	
	public function filter_bh_data()
	{   $userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		if($this->input->server('REQUEST_METHOD') === 'GET' and $this->input->get('month_from')!=null and preg_match('/^[0-9]{2}-[0-9]{4}\z/', $this->input->get('month_from')))
		{
			
			//$this->form_validation->set_rules('month_from','Month','required');
					
			/*if($this->form_validation->run()==true)
			{*/
				$combine_day = "01-".$this->input->get('month_from');
				$month_from = date("Y-m-d",strtotime($combine_day));
				
				
				
				$partner_id = $_SESSION["partner_id"];
				$data['bh_data'] = $this->Ueh_model->getBhData($month_from,$partner_id);
				
			/*}*/
			$data['include'] = "ueh/ueh_base_hospital";
			$this->load->view('container_login_dis', $data);
		}else{
			redirect('ueh_base_hospital');
		}
	}
	
	public function delete_base_ueh()
	{ 
		if(!empty($this->uri->segment(2)))
        {
	     $id = base64_decode($this->uri->segment(2));
		 $this->Ueh_model->deleteUehBase($id);
	     redirect('ueh_base_hospital');
		}
	}
	
	public function edit_base_ueh()
	{ 
	     $id = base64_decode($this->input->post('id'));
		 $data=$this->Ueh_model->editFetchDataBase($id);
		 if(!empty($data))
		 {
			 $data2 = json_encode($data[0]);
			 echo $data2."$$$".$this->security->get_csrf_hash();
		 }
		 else
		 {
			 echo 0;
		 }
	}
	
	
	
//////////////////////////end of the  base hospital ///////////////////
	
	public function ueh_outreach_camp() 
	{
		$data['oc_data'] = "";
		$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		$data['include'] = "ueh/ueh_outreach_camp";
		$this->load->view('container_login_dis', $data);
	}
	public function create_oc()
	{
		$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$this->form_validation->set_rules('month_data','Month','required');
			if($this->form_validation->run()==true)
			{
				$partner_id = $_SESSION["partner_id"];
				$created_on = server_date_time();
				$combine_day = "01-".$this->input->post('month_data');
				$data_month = date("Y-m-d",strtotime($combine_day));
				$id_ajax = $this->input->post('id_ajax');
				$name_cs = $this->input->post('name_cs');
				$camp_support = $this->input->post('camp_support');
				$fund_agency = $this->input->post('fund_agency');				
				$loc_type = $this->input->post('loc_type');
				$screen_male = $this->input->post('screen_male');
				$screen_female = $this->input->post('screen_female');
				$screen_boy = $this->input->post('screen_boy');
				$screen_girl = $this->input->post('screen_girl');
				$refra_refra_male = $this->input->post('refra_refra_male');
				$refra_refra_female = $this->input->post('refra_refra_female');
				$refra_refra_boy = $this->input->post('refra_refra_boy');
				$refra_refra_girl = $this->input->post('refra_refra_girl');
				$refra_pres_male = $this->input->post('refra_pres_male');
				$refra_pres_female = $this->input->post('refra_pres_female');
				$refra_pres_boy = $this->input->post('refra_pres_boy');
				$refra_pres_girl = $this->input->post('refra_pres_girl');
				$refra_disp_male = $this->input->post('refra_disp_male');
				$refra_disp_female = $this->input->post('refra_disp_female');
				$refra_disp_boy = $this->input->post('refra_disp_boy');
				$refra_disp_girl = $this->input->post('refra_disp_girl');				
				$refra_purc_male = $this->input->post('refra_purc_male');
				$refra_purc_female = $this->input->post('refra_purc_female');
				$refra_purc_child = $this->input->post('refra_purc_child');
				$refra_purc_girl = $this->input->post('refra_purc_girl');
				$refer_refer_male = $this->input->post('refer_refer_male');
				$refer_refer_female = $this->input->post('refer_refer_female');
				$refer_refer_boy = $this->input->post('refer_refer_boy');
				$refer_refer_girl = $this->input->post('refer_refer_girl');
				$refer_catar_male = $this->input->post('refer_catar_male');
				$refer_catar_female = $this->input->post('refer_catar_female');
				$refer_catar_boy = $this->input->post('refer_catar_boy');
				$refer_catar_girl = $this->input->post('refer_catar_girl');
				$treat_subdfree_male = $this->input->post('treat_subdfree_male');
				$treat_male = $this->input->post('treat_male');
				$treat_female = $this->input->post('treat_female');
				$treat_boy = $this->input->post('treat_boy');
				$treat_girl = $this->input->post('treat_girl');
				$dr_diabe_male = $this->input->post('dr_diabe_male');
				$dr_diabe_female = $this->input->post('dr_diabe_female');
				$dr_diabe_boy = $this->input->post('dr_diabe_boy');
				$dr_diabe_girl = $this->input->post('dr_diabe_girl');
				$dr_screen_male = $this->input->post('dr_screen_male');
				$dr_screen_female = $this->input->post('dr_screen_female');
				$dr_screen_boy = $this->input->post('dr_screen_boy');
				$dr_screen_girl = $this->input->post('dr_screen_girl');
				$dr_method_male = $this->input->post('dr_method_male');
				$dr_method_female = $this->input->post('dr_method_female');
				$dr_method_boy = $this->input->post('dr_method_boy');				
				$dr_method_girl = $this->input->post('dr_method_girl');
				
				if($this->input->post('id_ajax')=='')
				{
				$oc_data = array('ss_partner_id'=>$partner_id,'ss_name_cs'=>$name_cs,'ss_camp_support'=>$camp_support,'ss_fund_agency'=>$fund_agency,'ss_loc_type'=>$loc_type,'ss_screen_male'=>$screen_male,'ss_screen_female'=>$screen_female,'ss_screen_boy'=>$screen_boy,'ss_screen_girl'=>$screen_girl,'ss_refra_refra_male'=>$refra_refra_male,'ss_refra_refra_female'=>$refra_refra_female,'ss_refra_refra_boy'=>$refra_refra_boy,'ss_refra_refra_girl'=>$refra_refra_girl,'ss_refra_pres_male'=>$refra_pres_male,'ss_refra_pres_female'=>$refra_pres_female,'ss_refra_pres_boy'=>$refra_pres_boy,'ss_refra_pres_girl'=>$refra_pres_girl,'ss_refra_disp_male'=>$refra_disp_male,'ss_refra_disp_female'=>$refra_disp_female,'ss_refra_disp_boy'=>$refra_disp_boy,'ss_refra_disp_girl'=>$refra_disp_girl,'ss_refra_purc_male'=>$refra_purc_male,'ss_refra_purc_female'=>$refra_purc_female,'ss_refra_purc_child'=>$refra_purc_child,'ss_refra_purc_girl'=>$refra_purc_girl,'ss_refer_refer_male'=>$refer_refer_male,'ss_refer_refer_female'=>$refer_refer_female,'ss_refer_refer_boy'=>$refer_refer_boy,'ss_refer_refer_girl'=>$refer_refer_girl,'ss_refer_catar_male'=>$refer_catar_male,'ss_refer_catar_female'=>$refer_catar_female,'ss_refer_catar_boy'=>$refer_catar_boy,'ss_refer_catar_girl'=>$refer_catar_girl,'ss_treat_male'=>$treat_male,'ss_treat_female'=>$treat_female,'ss_treat_boy'=>$treat_boy,'ss_treat_girl'=>$treat_girl,'ss_dr_diabe_male'=>$dr_diabe_male,'ss_dr_diabe_female'=>$dr_diabe_female,'ss_dr_diabe_boy'=>$dr_diabe_boy,'ss_dr_diabe_girl'=>$dr_diabe_girl,'ss_dr_screen_male'=>$dr_screen_male,'ss_dr_screen_female'=>$dr_screen_female,'ss_dr_screen_boy'=>$dr_screen_boy,'ss_dr_screen_girl'=>$dr_screen_girl,'ss_dr_method_male'=>$dr_method_male,'ss_dr_method_female'=>$dr_method_female,'ss_dr_method_boy'=>$dr_method_boy,'ss_dr_method_girl'=>$dr_method_girl,'ss_ueh_month'=>$data_month,'ss_ueh_oc_created_on'=>$created_on);
				
				$creation_check = $this->Ueh_model->createOc($oc_data);
				}else
				{
					$update_oc_data = array('ss_partner_id'=>$partner_id,'ss_name_cs'=>$name_cs,'ss_camp_support'=>$camp_support,'ss_fund_agency'=>$fund_agency,'ss_loc_type'=>$loc_type,'ss_screen_male'=>$screen_male,'ss_screen_female'=>$screen_female,'ss_screen_boy'=>$screen_boy,'ss_screen_girl'=>$screen_girl,'ss_refra_refra_male'=>$refra_refra_male,'ss_refra_refra_female'=>$refra_refra_female,'ss_refra_refra_boy'=>$refra_refra_boy,'ss_refra_refra_girl'=>$refra_refra_girl,'ss_refra_pres_male'=>$refra_pres_male,'ss_refra_pres_female'=>$refra_pres_female,'ss_refra_pres_boy'=>$refra_pres_boy,'ss_refra_pres_girl'=>$refra_pres_girl,'ss_refra_disp_male'=>$refra_disp_male,'ss_refra_disp_female'=>$refra_disp_female,'ss_refra_disp_boy'=>$refra_disp_boy,'ss_refra_disp_girl'=>$refra_disp_girl,'ss_refra_purc_male'=>$refra_purc_male,'ss_refra_purc_female'=>$refra_purc_female,'ss_refra_purc_child'=>$refra_purc_child,'ss_refra_purc_girl'=>$refra_purc_girl,'ss_refer_refer_male'=>$refer_refer_male,'ss_refer_refer_female'=>$refer_refer_female,'ss_refer_refer_boy'=>$refer_refer_boy,'ss_refer_refer_girl'=>$refer_refer_girl,'ss_refer_catar_male'=>$refer_catar_male,'ss_refer_catar_female'=>$refer_catar_female,'ss_refer_catar_boy'=>$refer_catar_boy,'ss_refer_catar_girl'=>$refer_catar_girl,'ss_treat_male'=>$treat_male,'ss_treat_female'=>$treat_female,'ss_treat_boy'=>$treat_boy,'ss_treat_girl'=>$treat_girl,'ss_dr_diabe_male'=>$dr_diabe_male,'ss_dr_diabe_female'=>$dr_diabe_female,'ss_dr_diabe_boy'=>$dr_diabe_boy,'ss_dr_diabe_girl'=>$dr_diabe_girl,'ss_dr_screen_male'=>$dr_screen_male,'ss_dr_screen_female'=>$dr_screen_female,'ss_dr_screen_boy'=>$dr_screen_boy,'ss_dr_screen_girl'=>$dr_screen_girl,'ss_dr_method_male'=>$dr_method_male,'ss_dr_method_female'=>$dr_method_female,'ss_dr_method_boy'=>$dr_method_boy,'ss_dr_method_girl'=>$dr_method_girl,'ss_ueh_month'=>$data_month,'ss_ueh_oc_created_on'=>$created_on);
				
				$creation_check = $this->Ueh_model->updateOc($update_oc_data,$id_ajax);
				}
				if(!empty($creation_check))
				{
					$msg = "Information filled successfully";
				}else{
					$msg = "Please try after sometime";
				}
				$this->session->set_flashdata('success',$msg);
				redirect('filter_oc_ueh?month_from='.$this->input->post('month_data'));
			}else{
				$data['include'] = "ueh/ueh_outreach_camp";
			$this->load->view('container_login_dis', $data);
			}
		}
	}
	
	public function filter_oc_data()
	{   
	 $userid=$this->session->userdata('userinfo')['user_id'];
	    
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		if($this->input->server('REQUEST_METHOD') === 'GET' and $this->input->get('month_from')!=null and preg_match('/^[0-9]{2}-[0-9]{4}\z/', $this->input->get('month_from')))
		{
			
			/*$this->form_validation->set_rules('month_from','Month','required');
					
			if($this->form_validation->run()==true)
			{*/
				$combine_day = "01-".$this->input->get('month_from');
				$month_from = date("Y-m-d",strtotime($combine_day));
				
				$partner_id = $_SESSION["partner_id"];
				$data['oc_data'] = $this->Ueh_model->getOcData($month_from,$partner_id);
				
			//}
			$data['include'] = "ueh/ueh_outreach_camp";
			$this->load->view('container_login_dis', $data);
		}else{
			redirect('ueh_outreach_camp');
		}
	}
	public function delete_oc_ueh()
	{ 
		if(!empty($this->uri->segment(2)))
        {
	     $id = base64_decode($this->uri->segment(2));
		 $this->Ueh_model->deleteUehOc($id);
	     redirect('ueh_outreach_camp');
		}
	}
	
	public function edit_oc_ueh()
	{ 
	     $id = base64_decode($this->input->post('id'));
		 $data=$this->Ueh_model->editFetchDataOc($id);
		 if(!empty($data))
		 {
			 $data2 = json_encode($data[0]);
			 echo $data2."$$$".$this->security->get_csrf_hash();
		 }
		 else
		 {
			 echo 0;
		 }
	}

///////////////////end of the outreach code  ///////////////////////	
	public function ueh_training_entry() 
	{
		$data['training_data'] = "";
		$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		$data['include'] = "ueh/ueh_training_entry";
		$this->load->view('container_login_dis', $data);
	}
	
	public function create_training_ueh()
	{
		$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
		
			$this->form_validation->set_rules('month_data','Month','required');
					
			if($this->form_validation->run()==true)
			{  
				$partner_id = $_SESSION["partner_id"];
				$created_on = server_date_time();
				$id_ajax = $this->input->post('id_ajax');
				$cityname = $this->input->post('city_name');
				$person = $this->input->post('person');
				$gender = $this->input->post('gender');
				$person_category = $this->input->post('person_category');
				$person_category_detail = $this->input->post('person_category_detail');
				$location_activity = $this->input->post('location_activity');			
				$location_activity_detail = $this->input->post('location_activity_detail');
				$activity = $this->input->post('activity');
				$activity_detail = $this->input->post('activity_detail');
				$participant = $this->input->post('participant');
				$participant_detail = $this->input->post('participant_detail');
				$topic = $this->input->post('topic');
				$combine_day = "01-".$this->input->post('month_data');
				$data_month = date("Y-m-d",strtotime($combine_day));
				
				if($this->input->post('id_ajax')=='')
				{ 
				$training_data = array('ss_partner_id'=>$partner_id,'ss_ueh_city_name'=>$cityname,'ss_ueh_person'=>$person,'ss_ueh_gender'=>$gender,'ss_person_category'=>$person_category,'ss_person_category_detail'=>$person_category_detail,'ss_location_activity'=>$location_activity,'ss_location_activity_detail'=>$location_activity_detail,'ss_activity'=>$activity,'ss_activity_details'=>$activity_detail,'ss_participants_from'=>$participant,'ss_partcipants_from_detail'=>$participant_detail,'ss_topic'=>$topic,'ss_ueh_month'=>$data_month,'ss_ueh_training_created_on'=>$created_on);
				$creation_check = $this->Ueh_model->createTraining($training_data);
				}
				else{ 
					$update_training_data = array('ss_partner_id'=>$partner_id,'ss_ueh_city_name'=>$cityname,'ss_ueh_person'=>$person,'ss_ueh_gender'=>$gender,'ss_person_category'=>$person_category,'ss_person_category_detail'=>$person_category_detail,'ss_location_activity'=>$location_activity,'ss_location_activity_detail'=>$location_activity_detail,'ss_activity'=>$activity,'ss_activity_details'=>$activity_detail,'ss_participants_from'=>$participant,'ss_partcipants_from_detail'=>$participant_detail,'ss_topic'=>$topic,'ss_ueh_month'=>$data_month,'ss_ueh_training_created_on'=>$created_on);
				$creation_check = $this->Ueh_model->updateTraining($update_training_data,$id_ajax);
				}
				
				if(!empty($creation_check))
				{
					$msg = "Information filled successfully";
				}else{
					$msg = "Please try after sometime";
				}
				$this->session->set_flashdata('success',$msg);
				redirect('filter_training_data?month_from='.$this->input->post('month_data'));
			}else{ 
				$data['include'] = "ueh/ueh_training_entry";
			$this->load->view('container_login_dis', $data);
			}
		}
	}
	public function filter_training_data()
	{
		$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		//echo $this->input->get('month_from'); exit;
		if($this->input->server('REQUEST_METHOD') === 'GET' and $this->input->get('month_from')!=null and preg_match('/^[0-9]{2}-[0-9]{4}\z/', $this->input->get('month_from')))
		{
			//$this->form_validation->set_rules('month_from','Month','required');
			/*if($this->form_validation->run()==true)
			{  echo $this->input->get('month_from'); exit;*/ 
				$combine_day = "01-".$this->input->get('month_from');
				$month_from = date("Y-m-d",strtotime($combine_day));
				$partner_id = $_SESSION["partner_id"];
				$data['training_data'] = $this->Ueh_model->getTraining($month_from,$partner_id);
			/*}*/
			$data['include'] = "ueh/ueh_training_entry";
			$this->load->view('container_login_dis', $data);
		}else{
			redirect('ueh_training_entry');
		}
	}
	
  public function delete_training_ueh()
	{ 
		if(!empty($this->uri->segment(2)))
        {
	     $id = base64_decode($this->uri->segment(2));
		 $this->Ueh_model->deleteUehTraining($id);
	     redirect('ueh_training_entry');
		}
	}
	
	public function edit_training_ueh()
	{ 
	     $id = base64_decode($this->input->post('id'));
		 $data=$this->Ueh_model->editFetchDataTraining($id);
		 if(!empty($data))
		 {
			 $data2 = json_encode($data[0]);
			 echo $data2."$$$".$this->security->get_csrf_hash();
		 }
		 else
		 {
			 echo 0;
		 }
	}
		
	
	
	
	
	
	///// start of the advocacy  //////
	public function ueh_advocacy_entry() 
	{
		$data['advocacy_data'] = "";
		$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		$data['include'] = "ueh/ueh_advocacy_entry";
		$this->load->view('container_login_dis', $data);
	}
	public function create_advocacy_ueh()
	{
		$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		if($this->input->server('REQUEST_METHOD') === 'POST')
		  { 
			$this->form_validation->set_rules('month_data','Month','required');
			if($this->form_validation->run()==true)
			{   // echo "hello"; die;
		    	$partner_id = $_SESSION["partner_id"]; 
				$created_on = server_date_time();
				$id_ajax = $this->input->post('id_ajax');
				$ss_ueh_city_name = $this->input->post('city_name');
				$event_type = $this->input->post('event_type');
				$event_detail = $this->input->post('event_type_detail');
				$meeting_purpose = $this->input->post('meeting_purpose');
				$meeting_purpose_detail = $this->input->post('meeting_purpose_detail');
				$meeting_level = $this->input->post('meeting_level');			
				$meeting_held_with = $this->input->post('meeting_held_with');
				$meeting_held_with_detail = $this->input->post('meeting_held_with_detail');
				$topic_event = $this->input->post('topic_event');
				$combine_day = "01-".$this->input->post('month_data');
				$data_month = date("Y-m-d",strtotime($combine_day));
				if($this->input->post('id_ajax')=='')
				{ 
				$advocacy_data = array('ss_partner_id'=>$partner_id,'ss_reh_city_name'=>$ss_ueh_city_name,'ss_ueh_event_type'=>$event_type,'ss_ueh_event_detail'=>$event_detail,'ss_ueh_meeting_purpose'=>$meeting_purpose,'ss_meeting_purpose_detail'=>$meeting_purpose_detail,'ss_meeting_level'=>$meeting_level,'ss_meeting_held_with'=>$meeting_held_with,'ss_meeting_held_with_detail'=>$meeting_held_with_detail,'ss_topic_event'=>$topic_event,'ss_ueh_month'=>$data_month,'ss_ueh_created_on'=>$created_on);
				
				$creation_check = $this->Ueh_model->createAdvocacy($advocacy_data);
				}else
				{   
					$update_advocacy_data = array('ss_partner_id'=>$partner_id,'ss_reh_city_name'=>$ss_ueh_city_name,'ss_ueh_event_type'=>$event_type,'ss_ueh_event_detail'=>$event_detail,'ss_ueh_meeting_purpose'=>$meeting_purpose,'ss_meeting_purpose_detail'=>$meeting_purpose_detail,'ss_meeting_level'=>$meeting_level,'ss_meeting_held_with'=>$meeting_held_with,'ss_meeting_held_with_detail'=>$meeting_held_with_detail,'ss_topic_event'=>$topic_event,'ss_ueh_month'=>$data_month,'ss_ueh_created_on'=>$created_on);
				
				$creation_check = $this->Ueh_model->updateAdvocacy($update_advocacy_data,$id_ajax);
				}
				
				if(!empty($creation_check))
				{
					$msg = "Information filled successfully";
				}else{
					$msg = "Please try after sometime";
				}
				$this->session->set_flashdata('success',$msg);
				redirect('filter_advocacy_ueh?month_from='.$this->input->post('month_data'));
			}
			else
			{
			$data['include'] = "ueh/ueh_advocacy_entry";
			$this->load->view('container_login_dis', $data);
			}
		}
	} 
	public function filter_advocacy_ueh()
	{
		$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		if($this->input->server('REQUEST_METHOD') === 'GET' and $this->input->get('month_from')!=null and preg_match('/^[0-9]{2}-[0-9]{4}\z/', $this->input->get('month_from')))
		{
			$_SESSION['ABC'] = 1;
			
			/*$this->form_validation->set_rules('month_from','Month','required');
					
			if($this->form_validation->run()==true)
			{*/
				$combine_day = "01-".$this->input->get('month_from');
				$month_from = date("Y-m-d",strtotime($combine_day));
				
				
				$partner_id = $_SESSION["partner_id"];
				$data['advocacy_data'] = $this->Ueh_model->getAdvocacy($month_from,$partner_id);
				
			//}
			$data['include'] = "ueh/ueh_advocacy_entry";
			$this->load->view('container_login_dis', $data);
		}else{
			redirect('ueh_advocacy_entry');
		}
	}
	
	public function delete_advocacy_ueh()
	{ 
		if(!empty($this->uri->segment(2)))
        {
	     $id = base64_decode($this->uri->segment(2));
		 $this->Ueh_model->deleteUehAdvocay($id);
	    redirect($_SERVER['HTTP_REFERER']);
		}
	}
	
	public function edit_advocacy_ueh()
	{ 
	     $id = base64_decode($this->input->post('id'));
		 $data=$this->Ueh_model->editFetchDataAdvocacy($id);
		 if(!empty($data))
		 {
			 $data2 = json_encode($data[0]);
			 echo $data2."$$$".$this->security->get_csrf_hash();
		 }
		 else
		 {
			 echo 0;
		 }
	}
	
	
	
	
	/// end of the advocacy  ////////////
	
	
	
	
	/// start the Rural bcc  ///////////
	
	
	
	public function ueh_bcc_entry() 
	{
		$data['bcc_data'] = "";
		$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		$data['include'] = "ueh/ueh_bcc_entry";
		$this->load->view('container_login_dis', $data);
	}
	public function create_bcc_ueh()
	{
		$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
		
			$this->form_validation->set_rules('month_data','Month','required');
					
			if($this->form_validation->run()==true)
			{
				$partner_id = $_SESSION["partner_id"];
				$created_on = server_date_time();
				$id_ajax = $this->input->post('id_ajax');
				$city_name = $this->input->post('city_name');
				$activity_bcc = $this->input->post('act_awareness');
				$activity_detail = $this->input->post('act_detail');
				$sensitize_bcc = $this->input->post('sensitize');
				$sensitize_detail = $this->input->post('sensitize_detail');
				
				$participants_bcc = $this->input->post('participant');
				$topic_bcc = $this->input->post('topic');
				$combine_day = "01-".$this->input->post('month_data');
				$data_month = date("Y-m-d",strtotime($combine_day));
				if($this->input->post('id_ajax')=='')
				{
				$bcc_data = array('ss_partner_id'=>$partner_id,'ss_ueh_bcc_city_name'=>$city_name,'ss_ueh_bcc_activity_type'=>$activity_bcc,'ss_ueh_bcc_sansitize'=>$sensitize_bcc,'ss_activity_detail'=>$activity_detail,'ss_ueh_bcc_sansitize_detail'=>$sensitize_detail,'ss_ueh_bcc_participant_no'=>$participants_bcc,'ss_ueh_bcc_topic'=>$topic_bcc,'ss_ueh_bcc_month'=>$data_month,'ss_ueh_bcc_created_on'=>$created_on);
				//var_dump($bcc_data); die;
				$creation_check = $this->Ueh_model->createBcc($bcc_data);
				}
				else{
					$update_bcc_data = array('ss_partner_id'=>$partner_id,'ss_ueh_bcc_city_name'=>$city_name,'ss_ueh_bcc_activity_type'=>$activity_bcc,'ss_ueh_bcc_sansitize'=>$sensitize_bcc,'ss_activity_detail'=>$activity_detail,'ss_ueh_bcc_sansitize_detail'=>$sensitize_detail,'ss_ueh_bcc_participant_no'=>$participants_bcc,'ss_ueh_bcc_topic'=>$topic_bcc,'ss_ueh_bcc_month'=>$data_month,'ss_ueh_bcc_created_on'=>$created_on);
				
				$creation_check = $this->Ueh_model->updatebcc($update_bcc_data,$id_ajax);
				}
				if(!empty($creation_check))
				{
					$msg = "Information filled successfully";
				}else{
					$msg = "Please try after sometime";
				}
				$this->session->set_flashdata('success',$msg);
				redirect('filter_bcc_ueh?month_from='.$this->input->post('month_data'));
			}else{
				$data['include'] = "ueh/ueh_bcc_entry";
			$this->load->view('container_login_dis', $data);
			}
		}
	}
	
	public function filter_bcc_data()
	{
		$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		if($this->input->server('REQUEST_METHOD') === 'GET' and $this->input->get('month_from')!=null and preg_match('/^[0-9]{2}-[0-9]{4}\z/', $this->input->get('month_from')))
		{
			
			/*$this->form_validation->set_rules('month_from','Month','required');
					
			if($this->form_validation->run()==true)
			{*/
				$combine_day = "01-".$this->input->get('month_from');
				$month_from = date("Y-m-d",strtotime($combine_day));
				
				
				$partner_id = $_SESSION["partner_id"];
				$data['bcc_data'] = $this->Ueh_model->getBcc($month_from,$partner_id);
				
			//}
			$data['include'] = "ueh/ueh_bcc_entry";
			$this->load->view('container_login_dis', $data);
		}else{
			redirect('ueh_bcc_entry');
		}
	}
	
	public function delete_bcc_ueh()
	{ 
		if(!empty($this->uri->segment(2)))
        {
	     $id = base64_decode($this->uri->segment(2));
		 $this->Ueh_model->deleteUehBcc($id);
	     redirect($_SERVER['HTTP_REFERER']);
		}
	}
	
	public function edit_bcc_ueh()
	{ 
	     $id = base64_decode($this->input->post('id'));
		 $data=$this->Ueh_model->editFetchDataBcc($id);
		 if(!empty($data))
		 {
			 $data2 = json_encode($data[0]);
			 echo $data2."$$$".$this->security->get_csrf_hash();
		 }
		 else
		 {
			 echo 0;
		 }
	}
	
	
	
	
	
 
//// end of the Rural bcc   ////////	
/////// start the Rural iec  /////////	
  public function ueh_iec_entry()
	{ 
		$data['iec_data'] = "";
		$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		$data['include'] = "ueh/ueh_iec_entry";
		$this->load->view('container_login_dis', $data);
	}
	
	
   public function create_iec()
   {
	   $userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
	if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$this->form_validation->set_rules('month_data','Month','required');
			if($this->form_validation->run()==true)
			{
				$partner_id = $_SESSION["partner_id"];
				$created_on = server_date_time();
				$id_ajax = $this->input->post('id_ajax');
				$city_name = $this->input->post('city_name');
				$poster = $this->input->post('poster');
				$booklet = $this->input->post('booklet');
				$pamplet = $this->input->post('pamphlet');
				$wall = $this->input->post('wall');
				$other = $this->input->post('any');
				$person = $this->input->post('person_reach_iec');
				$combine_day = "01-".$this->input->post('month_data');
				$data_month = date("Y-m-d",strtotime($combine_day));
				if($this->input->post('id_ajax')=='')
				{ 
				$bcc_data = array('ss_partner_id'=>$partner_id,'ss_ueh_iec_city'=>$city_name,'ss_ueh_iec_posters'=>$poster,'ss_ueh_iec_booklet'=>$booklet,'ss_ueh_iec_pamplets'=>$pamplet,'ss_ueh_iec_wall'=>$wall,'ss_ueh_iec_others'=>$other,'ss_ueh_iec_month'=>$data_month,'ss_ueh_iec_created_on'=>$created_on);
				$creation_check = $this->Ueh_model->createIec($bcc_data);
				}else
				{
					$update_bcc_data = array('ss_partner_id'=>$partner_id,'ss_ueh_iec_city'=>$city_name,'ss_ueh_iec_posters'=>$poster,'ss_ueh_iec_booklet'=>$booklet,'ss_ueh_iec_pamplets'=>$pamplet,'ss_ueh_iec_wall'=>$wall,'ss_ueh_iec_others'=>$other,'ss_ueh_iec_month'=>$data_month,'ss_ueh_iec_created_on'=>$created_on);
					//echo "<pre>"; print_r($update_bcc_data); die;
				$creation_check = $this->Ueh_model->updateIec($update_bcc_data,$id_ajax);
				}
				if(!empty($creation_check))
				{
					$msg = "Information filled successfully";
				}else{
					$msg = "Please try after sometime";
				}
				$this->session->set_flashdata('success',$msg);
				redirect('filter_iec_ueh?month_from='.$this->input->post('month_data'));
			}else{
				$data['include'] = "ueh/ueh_iec_entry";
		        $this->load->view('container_login_dis', $data);
			}
		}   
   }
 

	public function filter_iec_data()
	{
		$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		if($this->input->server('REQUEST_METHOD') === 'GET' and $this->input->get('month_from')!=null and preg_match('/^[0-9]{2}-[0-9]{4}\z/', $this->input->get('month_from')))
		{

			/*$this->form_validation->set_rules('month_from','Month','required');
					
			if($this->form_validation->run()==true)
			{*/
				$combine_day = "01-".$this->input->get('month_from');
				$month_from = date("Y-m-d",strtotime($combine_day));
				
				
				$partner_id = $_SESSION["partner_id"];
				$data['iec_data'] = $this->Ueh_model->getIec($month_from,$partner_id);
				
			//}
			$data['include'] = "ueh/ueh_iec_entry";
			$this->load->view('container_login_dis', $data);
		}else{
			redirect('ueh_iec_entry');
		}
	}
	
	public function delete_iec_ueh()
	{ 
		if(!empty($this->uri->segment(2)))
        {
	     $id = base64_decode($this->uri->segment(2));
		 $this->Ueh_model->deleteUehIec($id);
	     redirect($_SERVER['HTTP_REFERER']);
		}
	}
	
	public function edit_iec_ueh()
	{ 
	     $id = base64_decode($this->input->post('id'));
		 $data=$this->Ueh_model->editFetchDataIec($id);
		 if(!empty($data))
		 {
			 $data2 = json_encode($data[0]);
			 echo $data2."$$$".$this->security->get_csrf_hash();
		 }
		 else
		 {
			 echo 0;
		 }
	}
	
	
	
	///   end of the rural iec ////
	///   start base hospital excel upload ///
	public function excelupload()
	{		
		
		if(!empty($_FILES['upload_publish_file']['name']))
		{
			$created_on = server_date_time();
			$combine_day = "01-".$this->input->post('month_data');
			$data_month = date("Y-m-d",strtotime($combine_day));
			$partner_id = $_SESSION["partner_id"];
		
			require_once(EXCELDIR."PHPExcel.php");
			$excel_file= $_FILES['upload_publish_file']['tmp_name'];
			$inputFileName = $excel_file;
				try 
					{
					$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
					} 
				catch(Exception $e)
					{
					die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
					}
					require_once(EXCELDIR."PHPExcel/IOFactory.php");

					$objReader = PHPExcel_IOFactory::createReader('Excel2007');
					$inputFileType = 'Excel2007';					
					$sheetIndex = 0;
					$objReader = PHPExcel_IOFactory::createReader($inputFileType);
					$sheetnames = $objReader->listWorksheetNames($inputFileName);
					$objReader->setLoadSheetsOnly($sheetnames[$sheetIndex]);
					$objPHPExcel = $objReader->load($inputFileName);
					$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);					
					$start=7;
					$end = $highestRow = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
						for ($row = $start; $row <=$end; $row++) 
						{									
							$excel_data[] = array('ss_partner_id'=>$partner_id,'ss_agency_name'=>$objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue(),
							'ss_exam_opd_male'=>$objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue(),
							'ss_exam_opd_female'=>$objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue(),
							'ss_exam_opd_boy'=>$objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue(),
							'ss_exam_opd_girl'=>$objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue(),
							
							'ss_refer_vc_male'=>$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue(),
							'ss_refer_vc_female'=>$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue(),
							'ss_refer_vc_boy'=>$objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue(),
							'ss_refer_vc_girl'=>$objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue(),
							
							'ss_refer_camp_male'=>$objPHPExcel->getActiveSheet()->getCell('P'.$row)->getValue(),
							'ss_refer_camp_female'=>$objPHPExcel->getActiveSheet()->getCell('Q'.$row)->getValue(),
							'ss_refer_camp_boy'=>$objPHPExcel->getActiveSheet()->getCell('R'.$row)->getValue(),
							'ss_refer_camp_girl'=>$objPHPExcel->getActiveSheet()->getCell('S'.$row)->getValue(),
							
							'ss_refer_walk_male'=>$objPHPExcel->getActiveSheet()->getCell('T'.$row)->getValue(),
							'ss_refer_walk_female'=>$objPHPExcel->getActiveSheet()->getCell('U'.$row)->getValue(),
							'ss_refer_walk_boy'=>$objPHPExcel->getActiveSheet()->getCell('V'.$row)->getValue(),
							'ss_refer_walk_girl'=>$objPHPExcel->getActiveSheet()->getCell('W'.$row)->getValue(),
							
							'ss_refra_refra_male'=>$objPHPExcel->getActiveSheet()->getCell('X'.$row)->getValue(),
							'ss_refra_refra_female'=>$objPHPExcel->getActiveSheet()->getCell('Y'.$row)->getValue(),
							'ss_refra_refra_boy'=>$objPHPExcel->getActiveSheet()->getCell('Z'.$row)->getValue(),
							'ss_refra_refra_girl'=>$objPHPExcel->getActiveSheet()->getCell('AA'.$row)->getValue(),

							'ss_refra_pres_male'=>$objPHPExcel->getActiveSheet()->getCell('AB'.$row)->getValue(),
							'ss_refra_pres_female'=>$objPHPExcel->getActiveSheet()->getCell('AC'.$row)->getValue(),
							'ss_refra_pres_boy'=>$objPHPExcel->getActiveSheet()->getCell('AD'.$row)->getValue(),
							'ss_refra_pres_girl'=>$objPHPExcel->getActiveSheet()->getCell('AE'.$row)->getValue(),
							
							'ss_refra_disp_male'=>$objPHPExcel->getActiveSheet()->getCell('AF'.$row)->getValue(),
							'ss_refra_disp_female'=>$objPHPExcel->getActiveSheet()->getCell('AG'.$row)->getValue(),
							'ss_refra_disp_boy'=>$objPHPExcel->getActiveSheet()->getCell('AH'.$row)->getValue(),
							'ss_refra_disp_girl'=>$objPHPExcel->getActiveSheet()->getCell('AI'.$row)->getValue(),
							
							'ss_refra_purc_male'=>$objPHPExcel->getActiveSheet()->getCell('AJ'.$row)->getValue(),
							'ss_refra_purc_female'=>$objPHPExcel->getActiveSheet()->getCell('AK'.$row)->getValue(),
							'ss_refra_purc_boy'=>$objPHPExcel->getActiveSheet()->getCell('AL'.$row)->getValue(),
							'ss_refra_purc_girl'=>$objPHPExcel->getActiveSheet()->getCell('AM'.$row)->getValue(),
							
							'ss_ptreat_sur_male'=>$objPHPExcel->getActiveSheet()->getCell('AN'.$row)->getValue(),
							'ss_ptreat_sur_female'=>$objPHPExcel->getActiveSheet()->getCell('AO'.$row)->getValue(),
							'ss_ptreat_sur_boy'=>$objPHPExcel->getActiveSheet()->getCell('AP'.$row)->getValue(),
							'ss_ptreat_sur_girl'=>$objPHPExcel->getActiveSheet()->getCell('AQ'.$row)->getValue(),
							
							'ss_treat_subdfree_male'=>$objPHPExcel->getActiveSheet()->getCell('AR'.$row)->getValue(),
							'ss_treat_subdfree_female'=>$objPHPExcel->getActiveSheet()->getCell('AS'.$row)->getValue(),
							'ss_treat_subdfree_boy'=>$objPHPExcel->getActiveSheet()->getCell('AT'.$row)->getValue(),
							'ss_treat_subdfree_girl'=>$objPHPExcel->getActiveSheet()->getCell('AU'.$row)->getValue(),
							
							'ss_treat_subd_male'=>$objPHPExcel->getActiveSheet()->getCell('AV'.$row)->getValue(),
							'ss_treat_subd_female'=>$objPHPExcel->getActiveSheet()->getCell('Aw'.$row)->getValue(),
							'ss_treat_subd_boy'=>$objPHPExcel->getActiveSheet()->getCell('AX'.$row)->getValue(),
							'ss_treat_subd_girl'=>$objPHPExcel->getActiveSheet()->getCell('AY'.$row)->getValue(),							
							
							'ss_treat_catara_male'=>$objPHPExcel->getActiveSheet()->getCell('AZ'.$row)->getValue(),
							'ss_treat_catara_female'=>$objPHPExcel->getActiveSheet()->getCell('BA'.$row)->getValue(),
							'ss_treat_catara_boy'=>$objPHPExcel->getActiveSheet()->getCell('BB'.$row)->getValue(),
							'ss_treat_catara_girl'=>$objPHPExcel->getActiveSheet()->getCell('BC'.$row)->getValue(),
							
							'ss_treat_glau_male'=>$objPHPExcel->getActiveSheet()->getCell('BD'.$row)->getValue(),
							'ss_treat_glau_female'=>$objPHPExcel->getActiveSheet()->getCell('BE'.$row)->getValue(),
							'ss_treat_glau_boy'=>$objPHPExcel->getActiveSheet()->getCell('BF'.$row)->getValue(),
							'ss_treat_glau_girl'=>$objPHPExcel->getActiveSheet()->getCell('BG'.$row)->getValue(),
							
							'ss_treat_sur_male'=>$objPHPExcel->getActiveSheet()->getCell('BH'.$row)->getValue(),
							'ss_treat_sur_female'=>$objPHPExcel->getActiveSheet()->getCell('BI'.$row)->getValue(),
							'ss_treat_sur_boy'=>$objPHPExcel->getActiveSheet()->getCell('BJ'.$row)->getValue(),
							'ss_treat_sur_girl'=>$objPHPExcel->getActiveSheet()->getCell('BK'.$row)->getValue(),						
							
							'ss_ueh_base_month'=>$data_month,'ss_ueh_base_created_on'=>$created_on);
						}

					$creation_check = $this->Ueh_model->createBaseHospitalExcel($excel_data);
				
				$data['bh_data'] = $this->Ueh_model->getBhData($data_month,$partner_id);
				$data['include'] = "ueh/ueh_base_hospital";
				$this->load->view('container_login_dis', $data);
		}else{
			redirect('ueh_base_hospital');
		}
	}
}
