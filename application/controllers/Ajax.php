<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Ajax extends CI_Controller {
	public function __construct() {
		parent::__construct ();
		check_login ();
		$this->load->model ( 'Graph_model' );
	}
	public function getDistrict() {
		$state_id = $this->input->post ( 'state_id' );
		if ($state_id != '') {
			$district = $this->Graph_model->getDistrict ( $state_id );
			echo '<option value="">--Select District--</option>';
			foreach ( $district as $arr ) {
				echo "<option value='" . $arr->ss_district_id . "'>" . $arr->ss_district_name . "</option>";
			}
		} else {
			echo '';
		}
	}
	public function getchartsfordashboard(){
		$this->loadchart1();
		$this->loadchart2();
		$this->loadchart3();
		$this->loadchart4();
	}
	public function loadchart1(){
		$data = $this->Graph_model->getuehchart ( 'OPD_Examined', 'no_of_person_examined' );
		$data1 = $this->Graph_model->getrehchart ( 'OPD_Examined', 'no_of_person_examined' );
		
		$actual=0;
		$target=0;
		$actual += intval ( ((isset ( $data ['actual_result']->opd_count ) && $data ['actual_result']->opd_count != '') ? $data ['actual_result']->opd_count : 0) );
		$target += intval ( ((isset ( $data ['target_result']->opd_count ) && $data ['target_result']->opd_count != '') ? $data ['target_result']->opd_count : 0) );
		
		$actual += intval ( ((isset ( $data1 ['actual_result']->opd_count ) && $data1 ['actual_result']->opd_count != '') ? $data1 ['actual_result']->opd_count : 0) );
		$target += intval ( ((isset ( $data1 ['target_result']->opd_count ) && $data1 ['target_result']->opd_count != '') ? $data1 ['target_result']->opd_count : 0) );

		
			$chart_data1=$target;
			$chart_data2=$actual;
			$y_axis='Target';
			$x_axis='Actual';
		
		echo "<script>Highcharts.chart('speedometer_1', Highcharts.merge(gaugeOptions, {
    yAxis: {
        min: 0,
        max: ".$chart_data1.",
        title: {
            text: '".$y_axis."'
        }
    },
	
    credits: {
        enabled: false
    },
	
    series: [{
        name: '".$x_axis."',
        data: [".$chart_data2."],
        dataLabels: {
	
        },
        tooltip: {
            valueSuffix: ' km/h'
        }
    }]
	
}));</script>";
	}
	
	
	public function loadchart2(){
		$data = $this->Graph_model->getuehchart ( 'Catract_Examined', 'no_of_person_examined' );
		$data1 = $this->Graph_model->getrehchart ('Catract_Examined', 'no_of_person_examined');
		
		$actual=0;
		$target=0;
		$actual += intval ( ((isset ( $data ['actual_result']->opd_count ) && $data ['actual_result']->opd_count != '') ? $data ['actual_result']->opd_count : 0) );
		$target += intval ( ((isset ( $data ['target_result']->opd_count ) && $data ['target_result']->opd_count != '') ? $data ['target_result']->opd_count : 0) );
		
		$actual += intval ( ((isset ( $data1 ['actual_result']->opd_count ) && $data1 ['actual_result']->opd_count != '') ? $data1 ['actual_result']->opd_count : 0) );
		$target += intval ( ((isset ( $data1 ['target_result']->opd_count ) && $data1 ['target_result']->opd_count != '') ? $data1 ['target_result']->opd_count : 0) );
		
		
		$chart_data1=$target;
		$chart_data2=$actual;
		$y_axis='Target';
		$x_axis='Actual';
		echo "<script>Highcharts.chart('speedometer_2', Highcharts.merge(gaugeOptions, {
    yAxis: {
        min: 0,
        max: ".$chart_data1.",
        title: {
            text: '".$y_axis."'
        }
    },
	
    credits: {
        enabled: false
    },
	
    series: [{
        name: '".$x_axis."',
        data: [".$chart_data2."],
        dataLabels: {
	
        },
        tooltip: {
            valueSuffix: ' km/h'
        }
    }]
	
}));</script>";
	}
	
	
	
	public function loadchart3(){
		$data = $this->Graph_model->getuehchart ( 'Refraction', 'no_of_person_examined' );
		$data1 = $this->Graph_model->getrehchart ( 'Refraction', 'no_of_person_examined' );
		
		$actual=0;
		$target=0;
		$actual += intval ( ((isset ( $data ['actual_result']->opd_count ) && $data ['actual_result']->opd_count != '') ? $data ['actual_result']->opd_count : 0) );
		$target += intval ( ((isset ( $data ['target_result']->opd_count ) && $data ['target_result']->opd_count != '') ? $data ['target_result']->opd_count : 0) );
		
		$actual += intval ( ((isset ( $data1 ['actual_result']->opd_count ) && $data1 ['actual_result']->opd_count != '') ? $data1 ['actual_result']->opd_count : 0) );
		$target += intval ( ((isset ( $data1 ['target_result']->opd_count ) && $data1 ['target_result']->opd_count != '') ? $data1 ['target_result']->opd_count : 0) );
		
		
		$chart_data1=$target;
		$chart_data2=$actual;
		$y_axis='Target';
		$x_axis='Actual';
		echo "<script>Highcharts.chart('speedometer_3', Highcharts.merge(gaugeOptions, {
    yAxis: {
        min: 0,
        max: ".$chart_data1.",
        title: {
            text: '".$y_axis."'
        }
    },
	
    credits: {
        enabled: false
    },
	
    series: [{
        name: '".$x_axis."',
        data: [".$chart_data2."],
        dataLabels: {
	
        },
        tooltip: {
            valueSuffix: ' km/h'
        }
    }]
	
}));</script>";
	}
	
	
	
	public function loadchart4(){
		$data = $this->Graph_model->getuehchart ( 'Spectacles', 'no_of_person_examined' );
		$data1 = $this->Graph_model->getrehchart ( 'Spectacles', 'no_of_person_examined' );
		
		$actual=0;
		$target=0;
		$actual += intval ( ((isset ( $data ['actual_result']->opd_count ) && $data ['actual_result']->opd_count != '') ? $data ['actual_result']->opd_count : 0) );
		$target += intval ( ((isset ( $data ['target_result']->opd_count ) && $data ['target_result']->opd_count != '') ? $data ['target_result']->opd_count : 0) );
		
		$actual += intval ( ((isset ( $data1 ['actual_result']->opd_count ) && $data1 ['actual_result']->opd_count != '') ? $data1 ['actual_result']->opd_count : 0) );
		$target += intval ( ((isset ( $data1 ['target_result']->opd_count ) && $data1 ['target_result']->opd_count != '') ? $data1 ['target_result']->opd_count : 0) );
		
		
		$chart_data1=$target;
		$chart_data2=$actual;
		$y_axis='Target';
		$x_axis='Actual';
		echo "<script>Highcharts.chart('speedometer_4', Highcharts.merge(gaugeOptions, {
    yAxis: {
        min: 0,
        max: ".$chart_data1.",
        title: {
            text: '".$y_axis."'
        }
    },
	
    credits: {
        enabled: false
    },
	
    series: [{
        name: '".$x_axis."',
        data: [".$chart_data2."],
        dataLabels: {
	
        },
        tooltip: {
            valueSuffix: ' km/h'
        }
    }]
	
}));</script>";
	}
	
	public function getcharts_ueh() {
		$this->chart_ueh_OPD_Examined_no_of_person_examined ();
		$this->chart_ueh_OPD_Examined_no_of_person_examined_state_wise ();
		$this->chart_ueh_OPD_Examined_no_of_person_examined_state_wise_trend ();
		$this->chart_ueh_cataract_Examined_no_of_person_examined ();
		$this->chart_ueh_cataract_Examined_no_of_person_examined_state_wise ();
		$this->chart_ueh_cataract_Examined_no_of_person_examined_state_wise_trend ();
		
		$this->chart_ueh_refraction_Examined_no_of_person_examined ();
		$this->chart_ueh_refraction_Examined_no_of_person_examined_state_wise ();
		$this->chart_ueh_refraction_Examined_no_of_person_examined_state_wise_trend ();
		
		$this->chart_ueh_Spectacles_Examined_no_of_person_examined ();
		$this->chart_ueh_Spectacles_Examined_no_of_person_examined_state_wise ();
		$this->chart_ueh_Spectacles_Examined_no_of_person_examined_state_wise_trend ();
	}
	
	// Spectacles
	function chart_ueh_Spectacles_Examined_no_of_person_examined_state_wise_trend() {
		$data = $this->Graph_model->getuehchart ( 'Spectacles', 'no_of_person_examined_state_wise_trend' );
		$actual_result = [ ];
		foreach ( $data ['actual_result'] as $arr ) {
			$actual_result [$arr->month] [$arr->ss_states_id] = $arr->opd_count;
		}
		$month_array = [ 
				'1' => 'Jan',
				'2' => 'Feb',
				'3' => 'Mar',
				'4' => 'Apr',
				'5' => 'May',
				'6' => 'Jun',
				'7' => 'Jul',
				'8' => 'Aug',
				'9' => 'Sept',
				'10' => 'Oct',
				'11' => 'Nov',
				'12' => 'Dec' 
		];
		$month = [ ];
		$state_array = [ ];
		$actual = [ ];
		for($i = 1; $i <= $data ['month']; $i ++) {
			foreach ( $data ['states'] as $arr ) {
				
				$actual [$arr->ss_states_name] [$i] = ((isset ( $actual_result [$i] [$arr->ss_states_id] )) ? $actual_result [$i] [$arr->ss_states_id] : 0);
			}
			$month [$i] = $month_array [$i];
		}
		$data_display = '';
		foreach ( $actual as $key => $val ) {
			$data_display .= "{
		    name: '" . $key . "',
		    		duration: 5000 ,
          			easing: 'easeOutBounce',
		    data: [" . implode ( ",", $val ) . "]
		    },";
		}
		echo '<script>';
		echo "Highcharts.chart('graph3_4', {
		    	chart: {
		    	type: 'area'
		    },
		    title: {
		    text: 'No. of persons disbursed spectacles trend'
		    },
		    xAxis: {
		    categories: ['" . implode ( "','", $month ) . "'],
		    title: {
		    text: null
		    }
		    },
		    yAxis: {
		    min: 0,
		    title: {
		    text: 'Opd Count',
		    align: 'high'
		    },
		    labels: {
		    overflow: 'justify'
		    }
		    },
		    plotOptions: {
		    bar: {
		    dataLabels: {
		    enabled: true
		    }
		    }
		    },
		    series: [" . $data_display . "]
		    });
    ";
		echo '</script>';
	}
	function chart_ueh_Spectacles_Examined_no_of_person_examined_state_wise() {
		$data = $this->Graph_model->getuehchart ( 'Spectacles', 'no_of_person_examined_state_wise' );
		$actual_result = [ ];
		foreach ( $data ['actual_result'] as $arr ) {
			$actual_result [$arr->ss_states_id] = $arr->opd_count;
		}
		$target_result = [ ];
		foreach ( $data ['target_result'] as $arr ) {
			$target_result [$arr->ss_states_id] = $arr->opd_count;
		}
		$state_array = [ ];
		$actual = [ ];
		$target = [ ];
		$acheived = [ ];
		foreach ( $data ['states'] as $arr ) {
			$state_array [$arr->ss_states_id] = $arr->ss_states_name;
			$actual [$arr->ss_states_id] = ((isset ( $actual_result [$arr->ss_states_id] )) ? $actual_result [$arr->ss_states_id] : 0);
			$target [$arr->ss_states_id] = ((isset ( $target_result [$arr->ss_states_id] )) ? $target_result [$arr->ss_states_id] : 0);
			$acheived [$arr->ss_states_id] = round ( $actual [$arr->ss_states_id] / (($target [$arr->ss_states_id] != 0) ? $target [$arr->ss_states_id] : 1) * 100, 2 );
		}
		echo '<script>';
		echo "Highcharts.chart('graph2_4', {
    	chart: {
    	type: 'column'
			    },
			    title: {
			    text: 'No. of persons disbursed spectacles statewise'
			    },
			    xAxis: {
			    categories: ['" . implode ( "','", $state_array ) . "'],
			    title: {
			    text: null
			    }
			    },
			    yAxis: {
			    min: 0,
			    title: {
			    text: 'Opd Count',
			    align: 'high'
			    },
			    labels: {
			    overflow: 'justify'
			    }
			    },
			    plotOptions: {
			    bar: {
			    dataLabels: {
			    enabled: true
			    }
			    }
			    },
			    series: [{
			    name: 'Actual',
			    		duration: 5000 ,
          			easing: 'easeOutBounce',
			    		
        type: 'column',
			    data: [" . implode ( ",", $actual ) . "]
			    },{
			    name: 'Target',
			    		duration: 5000 ,
			    		        type: 'spline',

          			easing: 'easeOutBounce',
			    data: [" . implode ( ",", $target ) . "]
			    }]
			    });
		    ";
		echo '</script>';
	}
	function chart_ueh_Spectacles_Examined_no_of_person_examined() {
		$data = $this->Graph_model->getuehchart ( 'Spectacles', 'no_of_person_examined' );
		$actual = $data ['actual_result']->opd_count;
		echo '<script>';
		$target = intval ( ((isset ( $data ['target_result']->opd_count ) && $data ['target_result']->opd_count != '') ? $data ['target_result']->opd_count : 0) );
		echo "Highcharts.chart('plain_graph_4', {
		chart: {
		type: 'bar'
	},
	title: {
	text: 'No. of persons disbursed spectacles refracted'
	},
	xAxis: {
	categories: ['Target', 'Actual'],
	title: {
	text: null
	}
	},
	yAxis: {
	min: 0,
	title: {
	text: 'Opd Count',
	align: 'high'
	},
	labels: {
	overflow: 'justify'
	}
	},
	plotOptions: {
	bar: {
	dataLabels: {
	enabled: true
	}
	}
	},
	series: [{
	name: 'Total',
	duration: 5000 ,
          			easing: 'easeOutBounce',
          
	data: [$actual, $target]
	}]
	});
	";
		echo '</script>';
	}
	
	// refratcion
	function chart_ueh_refraction_Examined_no_of_person_examined_state_wise_trend() {
		$data = $this->Graph_model->getuehchart ( 'Refraction', 'no_of_person_examined_state_wise_trend' );
		$actual_result = [ ];
		foreach ( $data ['actual_result'] as $arr ) {
			$actual_result [$arr->month] [$arr->ss_states_id] = $arr->opd_count;
		}
		$month_array = [ 
				'1' => 'Jan',
				'2' => 'Feb',
				'3' => 'Mar',
				'4' => 'Apr',
				'5' => 'May',
				'6' => 'Jun',
				'7' => 'Jul',
				'8' => 'Aug',
				'9' => 'Sept',
				'10' => 'Oct',
				'11' => 'Nov',
				'12' => 'Dec' 
		];
		$month = [ ];
		$state_array = [ ];
		$actual = [ ];
		for($i = 1; $i <= $data ['month']; $i ++) {
			foreach ( $data ['states'] as $arr ) {
				
				$actual [$arr->ss_states_name] [$i] = ((isset ( $actual_result [$i] [$arr->ss_states_id] )) ? $actual_result [$i] [$arr->ss_states_id] : 0);
			}
			$month [$i] = $month_array [$i];
		}
		$data_display = '';
		foreach ( $actual as $key => $val ) {
			$data_display .= "{
					name: '" . $key . "',
							duration: 5000 ,
          			easing: 'easeOutBounce',
          
		    data: [" . implode ( ",", $val ) . "]
					},";
		}
		echo '<script>';
		echo "Highcharts.chart('graph3_3', {
		    	chart: {
		    	type: 'area'
		    },
		    title: {
		    text: 'No. of persons refracted trend'
		    },
		    xAxis: {
		    categories: ['" . implode ( "','", $month ) . "'],
			    title: {
			    text: null
		    }
		    },
		    yAxis: {
		    min: 0,
		    title: {
		    text: 'Opd Count',
		    align: 'high'
		    },
		    labels: {
		    overflow: 'justify'
		    }
		    },
		    plotOptions: {
		    bar: {
		    dataLabels: {
		    enabled: true
		    }
		    }
		    },
		    series: [" . $data_display . "]
	});
	";
		echo '</script>';
	}
	function chart_ueh_refraction_Examined_no_of_person_examined_state_wise() {
		$data = $this->Graph_model->getuehchart ( 'Refraction', 'no_of_person_examined_state_wise' );
		$actual_result = [ ];
		foreach ( $data ['actual_result'] as $arr ) {
			$actual_result [$arr->ss_states_id] = $arr->opd_count;
		}
		$target_result = [ ];
		foreach ( $data ['target_result'] as $arr ) {
			$target_result [$arr->ss_states_id] = $arr->opd_count;
		}
		$state_array = [ ];
		$actual = [ ];
		$target = [ ];
		$acheived = [ ];
		foreach ( $data ['states'] as $arr ) {
			$state_array [$arr->ss_states_id] = $arr->ss_states_name;
			$actual [$arr->ss_states_id] = ((isset ( $actual_result [$arr->ss_states_id] )) ? $actual_result [$arr->ss_states_id] : 0);
			$target [$arr->ss_states_id] = ((isset ( $target_result [$arr->ss_states_id] )) ? $target_result [$arr->ss_states_id] : 0);
			$acheived [$arr->ss_states_id] = round ( $actual [$arr->ss_states_id] / (($target [$arr->ss_states_id] != 0) ? $target [$arr->ss_states_id] : 1) * 100, 2 );
		}
		echo '<script>';
		echo "Highcharts.chart('graph2_3', {
		chart: {
		type: 'column'
	},
			    title: {
			    text: 'No. of persons refracted statewise'
			    },
			    xAxis: {
			    categories: ['" . implode ( "','", $state_array ) . "'],
			    title: {
			    text: null
	}
	},
			yAxis: {
			min: 0,
			title: {
			    text: 'Opd Count',
			    align: 'high'
			    },
			    labels: {
			    overflow: 'justify'
			    }
			    },
			    plotOptions: {
			    bar: {
			    dataLabels: {
			    enabled: true
			    }
			    }
			    },
			    series: [{
			    name: 'Actual',
			    		duration: 5000 ,
          			easing: 'easeOutBounce',
			    		
        type: 'column',
       
          
			    data: [" . implode ( ",", $actual ) . "]
			    },{
			    name: 'Target',
			    		duration: 5000 ,
          			easing: 'easeOutBounce',
           type: 'spline',
			    data: [" . implode ( ",", $target ) . "]
	}]
	});
		    ";
		echo '</script>';
	}
	function chart_ueh_refraction_Examined_no_of_person_examined() {
		$data = $this->Graph_model->getuehchart ( 'Refraction', 'no_of_person_examined' );
		$actual = $data ['actual_result']->opd_count;
		echo '<script>';
		$target = intval ( ((isset ( $data ['target_result']->opd_count ) && $data ['target_result']->opd_count != '') ? $data ['target_result']->opd_count : 0) );
		$acheived = round ( $actual / (($target != 0) ? $target : 1) * 100, 2 );
		echo "Highcharts.chart('plain_graph_3', {
					chart: {
					type: 'bar'
	},
	title: {
	text: 'No. of persons refracteds'
	},
	xAxis: {
	categories: ['Target', 'Actual'],
	title: {
	text: null
	}
	},
	yAxis: {
	min: 0,
	title: {
	text: 'Opd Count',
	align: 'high'
	},
	labels: {
	overflow: 'justify'
	}
	},
	plotOptions: {
	bar: {
	dataLabels: {
	enabled: true
	}
	}
	},
	series: [{
	name: 'Total',
	duration: 5000 ,
          			easing: 'easeOutBounce',
          
	data: [$actual, $target]
	}]
	});
	";
		echo '</script>';
	}
	
	// CATARACT
	function chart_ueh_cataract_Examined_no_of_person_examined_state_wise_trend() {
		$data = $this->Graph_model->getuehchart ( 'Catract_Examined', 'no_of_person_examined_state_wise_trend' );
		$actual_result = [ ];
		foreach ( $data ['actual_result'] as $arr ) {
			$actual_result [$arr->month] [$arr->ss_states_id] = $arr->opd_count;
		}
		$month_array = [ 
				'1' => 'Jan',
				'2' => 'Feb',
				'3' => 'Mar',
				'4' => 'Apr',
				'5' => 'May',
				'6' => 'Jun',
				'7' => 'Jul',
				'8' => 'Aug',
				'9' => 'Sept',
				'10' => 'Oct',
				'11' => 'Nov',
				'12' => 'Dec' 
		];
		$month = [ ];
		$state_array = [ ];
		$actual = [ ];
		for($i = 1; $i <= $data ['month']; $i ++) {
			foreach ( $data ['states'] as $arr ) {
				
				$actual [$arr->ss_states_name] [$i] = ((isset ( $actual_result [$i] [$arr->ss_states_id] )) ? $actual_result [$i] [$arr->ss_states_id] : 0);
			}
			$month [$i] = $month_array [$i];
		}
		$data_display = '';
		foreach ( $actual as $key => $val ) {
			$data_display .= "{
											name: '" . $key . "',
													duration: 5000 ,
          			easing: 'easeOutBounce',
          
											data: [" . implode ( ",", $val ) . "]
	},";
		}
		echo '<script>';
		echo "Highcharts.chart('graph3_2', {
													chart: {
											type: 'area'
	},
											title: {
		    text: 'Cataract operations trend'
		    },
		    xAxis: {
		    categories: ['" . implode ( "','", $month ) . "'],
		    title: {
		    text: null
		    }
		    },
			    yAxis: {
			    min: 0,
		    title: {
		    text: 'Opd Count',
		    align: 'high'
		    },
		    labels: {
		    overflow: 'justify'
		    }
		    },
		    plotOptions: {
		    bar: {
		    dataLabels: {
		    enabled: true
		    }
		    }
		    },
		    series: [" . $data_display . "]
		    });
    ";
		echo '</script>';
	}
	function chart_ueh_cataract_Examined_no_of_person_examined_state_wise() {
		$data = $this->Graph_model->getuehchart ( 'Catract_Examined', 'no_of_person_examined_state_wise' );
		$actual_result = [ ];
		foreach ( $data ['actual_result'] as $arr ) {
			$actual_result [$arr->ss_states_id] = $arr->opd_count;
		}
		$target_result = [ ];
		foreach ( $data ['target_result'] as $arr ) {
			$target_result [$arr->ss_states_id] = $arr->opd_count;
		}
		$state_array = [ ];
		$actual = [ ];
		$target = [ ];
		$acheived = [ ];
		foreach ( $data ['states'] as $arr ) {
			$state_array [$arr->ss_states_id] = $arr->ss_states_name;
			$actual [$arr->ss_states_id] = ((isset ( $actual_result [$arr->ss_states_id] )) ? $actual_result [$arr->ss_states_id] : 0);
			$target [$arr->ss_states_id] = ((isset ( $target_result [$arr->ss_states_id] )) ? $target_result [$arr->ss_states_id] : 0);
			$acheived [$arr->ss_states_id] = round ( $actual [$arr->ss_states_id] / (($target [$arr->ss_states_id] != 0) ? $target [$arr->ss_states_id] : 1) * 100, 2 );
		}
		echo '<script>';
		echo "Highcharts.chart('graph2_2', {
			title: {
			    text: 'Cataract operations statewise'
			    },
			    xAxis: {
			    categories: ['" . implode ( "','", $state_array ) . "'],
			    title: {
			    text: null
			    }
											},
													yAxis: {
													min: 0,
			    title: {
			    text: 'Opd Count',
			    align: 'high'
			    },
			    labels: {
			    overflow: 'justify'
			    }
			    },
			    plotOptions: {
			    bar: {
			    dataLabels: {
			    enabled: true
			    }
			    }
			    },
			    series: [{
			    name: 'Actual',
			    		duration: 5000 ,
          			easing: 'easeOutBounce',
          
        type: 'column',
      
			    data: [" . implode ( ",", $actual ) . "]
			    },{
			    name: 'Target',
			    		duration: 5000 ,
          			easing: 'easeOutBounce',
            type: 'spline',
															data: [" . implode ( ",", $target ) . "]
			    }]
			    });
																	";
		echo '</script>';
	}
	function chart_ueh_cataract_Examined_no_of_person_examined() {
		$data = $this->Graph_model->getuehchart ( 'Catract_Examined', 'no_of_person_examined' );
		$actual = $data ['actual_result']->opd_count;
		echo '<script>';
		$target = intval ( ((isset ( $data ['target_result']->opd_count ) && $data ['target_result']->opd_count != '') ? $data ['target_result']->opd_count : 0) );
		$acheived = round ( $actual / (($target != 0) ? $target : 1) * 100, 2 );
		echo "Highcharts.chart('plain_graph_2', {
			chart: {
			type: 'bar'
		},
		title: {
		text: 'Cataract operations'
		},
		xAxis: {
		categories: ['Target', 'Actual'],
		title: {
		text: null
		}
		},
		yAxis: {
		min: 0,
		title: {
		text: 'Opd Count',
		align: 'high'
		},
		labels: {
		overflow: 'justify'
		}
		},
		plotOptions: {
		bar: {
		dataLabels: {
		enabled: true
		}
		}
		},
		series: [{
		name: 'Total',
		duration: 5000 ,
          			easing: 'easeOutBounce',
          
		data: [$actual, $target]
		}]
		});
		";
		echo '</script>';
	}
	
	// OPD
	function chart_ueh_OPD_Examined_no_of_person_examined_state_wise_trend() {
		$data = $this->Graph_model->getuehchart ( 'OPD_Examined', 'no_of_person_examined_state_wise_trend' );
		$actual_result = [ ];
		foreach ( $data ['actual_result'] as $arr ) {
			$actual_result [$arr->month] [$arr->ss_states_id] = $arr->opd_count;
		}
		$month_array = [ 
				'1' => 'Jan',
				'2' => 'Feb',
				'3' => 'Mar',
				'4' => 'Apr',
				'5' => 'May',
				'6' => 'Jun',
				'7' => 'Jul',
				'8' => 'Aug',
				'9' => 'Sept',
				'10' => 'Oct',
				'11' => 'Nov',
				'12' => 'Dec' 
		];
		$month = [ ];
		$state_array = [ ];
		$actual = [ ];
		for($i = 1; $i <= $data ['month']; $i ++) {
			foreach ( $data ['states'] as $arr ) {
				
				$actual [$arr->ss_states_name] [$i] = ((isset ( $actual_result [$i] [$arr->ss_states_id] )) ? $actual_result [$i] [$arr->ss_states_id] : 0);
			}
			$month [$i] = $month_array [$i];
		}
		$data_display = '';
		foreach ( $actual as $key => $val ) {
			$data_display .= "{
			name: '" . $key . "',
					duration: 5000 ,
          			easing: 'easeOutBounce',
          
			data: [" . implode ( ",", $val ) . "]
		    },";
		}
		echo '<script>';
		echo "Highcharts.chart('graph3_1', {
					chart: {
					type: 'area'
		    },
		    title: {
		    text: 'Number of Person examined'
		    },
		    xAxis: {
		    categories: ['" . implode ( "','", $month ) . "'],
		    title: {
			    text: null
			}
			},
			yAxis: {
		    min: 0,
		    title: {
		    text: 'Opd Count',
		    align: 'high'
		    },
		    labels: {
		    overflow: 'justify'
		    }
		    },
		    plotOptions: {
		    bar: {
		    dataLabels: {
		    enabled: true
		    }
		    }
		    },
		    series: [" . $data_display . "]
		    });
    ";
		echo '</script>';
	}
	function chart_ueh_OPD_Examined_no_of_person_examined_state_wise() {
		$data = $this->Graph_model->getuehchart ( 'OPD_Examined', 'no_of_person_examined_state_wise' );
		$actual_result = [ ];
		foreach ( $data ['actual_result'] as $arr ) {
			$actual_result [$arr->ss_states_id] = $arr->opd_count;
		}
		$target_result = [ ];
		foreach ( $data ['target_result'] as $arr ) {
			$target_result [$arr->ss_states_id] = $arr->opd_count;
		}
		$state_array = [ ];
		$actual = [ ];
		$target = [ ];
		$acheived = [ ];
		foreach ( $data ['states'] as $arr ) {
			$state_array [$arr->ss_states_id] = $arr->ss_states_name;
			$actual [$arr->ss_states_id] = ((isset ( $actual_result [$arr->ss_states_id] )) ? $actual_result [$arr->ss_states_id] : 0);
			$target [$arr->ss_states_id] = ((isset ( $target_result [$arr->ss_states_id] )) ? $target_result [$arr->ss_states_id] : 0);
			$acheived [$arr->ss_states_id] = round ( $actual [$arr->ss_states_id] / (($target [$arr->ss_states_id] != 0) ? $target [$arr->ss_states_id] : 1) * 100, 2 );
		}
		echo '<script>';
		echo "Highcharts.chart('graph2_1', {
				
		title: {
			    text: 'Number of Person examined'
			    },
			    xAxis: {
			    categories: ['" . implode ( "','", $state_array ) . "'],
			    title: {
			    text: null
			    }
		},
		yAxis: {
		min: 0,
		title: {
		text: 'Opd Count',
			    align: 'high'
			    },
			    labels: {
			    overflow: 'justify'
			    }
			    },
			    plotOptions: {
			    bar: {
			    dataLabels: {
			    enabled: true
			    }
			    }
			    },
			    series: [{
			    name: 'Actual',
			    		duration: 5000 ,
          			easing: 'easeOutBounce',
			    		
     			   type: 'column',
       
          
			    data: [" . implode ( ",", $actual ) . "]
			    },{
			    name: 'Target',
			    		duration: 5000 ,
          			easing: 'easeOutBounce',
          			 type: 'spline',
			    data: [" . implode ( ",", $target ) . "]
		}]
		});
				";
		echo '</script>';
	}
	function chart_ueh_OPD_Examined_no_of_person_examined() {
		$data = $this->Graph_model->getuehchart ( 'OPD_Examined', 'no_of_person_examined' );
		$actual = $data ['actual_result']->opd_count;
		echo '<script>';
		$target = intval ( ((isset ( $data ['target_result']->opd_count ) && $data ['target_result']->opd_count != '') ? $data ['target_result']->opd_count : 0) );
		$acheived = round ( $actual / (($target != 0) ? $target : 1) * 100, 2 );
		echo "Highcharts.chart('plain_graph_1', {
    	chart: {
					type: 'bar'
		},
		title: {
		text: 'Number of Person examined'
		},
		xAxis: {
		categories: ['Target', 'Actual'],
		title: {
		text: null
		}
		},
		yAxis: {
		min: 0,
		title: {
		text: 'Opd Count',
		align: 'high'
		},
		labels: {
		overflow: 'justify'
		}
		},
		plotOptions: {
		bar: {
		dataLabels: {
		enabled: true
		}
		}
		},
		series: [{
		name: 'Total',
		animation:{
          duration: 5000 ,
          easing: 'easeOutBounce',
          
        },
		data: [$actual, $target]
		}]
		});
		";
		echo '</script>';
	}
	public function getcharts_reh() {
		$this->chart_OPD_Examined_no_of_person_examined ();
		$this->chart_OPD_Examined_no_of_person_examined_state_wise ();
		$this->chart_OPD_Examined_no_of_person_examined_state_wise_trend ();
		
		$this->chart_cataract_Examined_no_of_person_examined ();
		$this->chart_cataract_Examined_no_of_person_examined_state_wise ();
		$this->chart_cataract_Examined_no_of_person_examined_state_wise_trend ();
		
		$this->chart_refraction_Examined_no_of_person_examined ();
		$this->chart_refraction_Examined_no_of_person_examined_state_wise ();
		$this->chart_refraction_Examined_no_of_person_examined_state_wise_trend ();
		
		$this->chart_Spectacles_Examined_no_of_person_examined ();
		$this->chart_Spectacles_Examined_no_of_person_examined_state_wise ();
		$this->chart_Spectacles_Examined_no_of_person_examined_state_wise_trend ();
	 }
	
	// Spectacles
	function chart_Spectacles_Examined_no_of_person_examined_state_wise_trend() {
		$data = $this->Graph_model->getrehchart ( 'Spectacles', 'no_of_person_examined_state_wise_trend' );
		$actual_result = [ ];
		foreach ( $data ['actual_result'] as $arr ) {
			$actual_result [$arr->month] [$arr->ss_states_id] = $arr->opd_count;
		}
		$month_array = [ 
				'1' => 'Jan',
				'2' => 'Feb',
				'3' => 'Mar',
				'4' => 'Apr',
				'5' => 'May',
				'6' => 'Jun',
				'7' => 'Jul',
				'8' => 'Aug',
				'9' => 'Sept',
				'10' => 'Oct',
				'11' => 'Nov',
				'12' => 'Dec' 
		];
		$month = [ ];
		$state_array = [ ];
		$actual = [ ];
		for($i = 1; $i <= $data ['month']; $i ++) {
			foreach ( $data ['states'] as $arr ) {
				
				$actual [$arr->ss_states_name] [$i] = ((isset ( $actual_result [$i] [$arr->ss_states_id] )) ? $actual_result [$i] [$arr->ss_states_id] : 0);
			}
			$month [$i] = $month_array [$i];
		}
		$data_display = '';
		foreach ( $actual as $key => $val ) {
			$data_display .= "{
		    name: '" . $key . "',
		    		duration: 5000 ,
          			easing: 'easeOutBounce',
          
		    data: [" . implode ( ",", $val ) . "]
		    },";
		}
		echo '<script>';
		echo "Highcharts.chart('graph3_4', {
		    	chart: {
		    	type: 'area'
		    },
		    title: {
		    text: 'No. of persons disbursed spectacles trend'
		    },
		    xAxis: {
		    categories: ['" . implode ( "','", $month ) . "'],
		    title: {
		    text: null
		    }
		    },
		    yAxis: {
		    min: 0,
		    title: {
		    text: 'Opd Count',
		    align: 'high'
		    },
		    labels: {
		    overflow: 'justify'
		    }
		    },
		    plotOptions: {
		    bar: {
		    dataLabels: {
		    enabled: true
		    }
		    }
		    },
		    series: [" . $data_display . "]
		    });
    ";
		echo '</script>';
	}
	function chart_Spectacles_Examined_no_of_person_examined_state_wise() {
		$data = $this->Graph_model->getrehchart ( 'Spectacles', 'no_of_person_examined_state_wise' );
		$actual_result = [ ];
		foreach ( $data ['actual_result'] as $arr ) {
			$actual_result [$arr->ss_states_id] = $arr->opd_count;
		}
		$target_result = [ ];
		foreach ( $data ['target_result'] as $arr ) {
			$target_result [$arr->ss_states_id] = $arr->opd_count;
		}
		$state_array = [ ];
		$actual = [ ];
		$target = [ ];
		$acheived = [ ];
		foreach ( $data ['states'] as $arr ) {
			$state_array [$arr->ss_states_id] = $arr->ss_states_name;
			$actual [$arr->ss_states_id] = ((isset ( $actual_result [$arr->ss_states_id] )) ? $actual_result [$arr->ss_states_id] : 0);
			$target [$arr->ss_states_id] = ((isset ( $target_result [$arr->ss_states_id] )) ? $target_result [$arr->ss_states_id] : 0);
			$acheived [$arr->ss_states_id] = round ( $actual [$arr->ss_states_id] / (($target [$arr->ss_states_id] != 0) ? $target [$arr->ss_states_id] : 1) * 100, 2 );
		}
		echo '<script>';
		echo "Highcharts.chart('graph2_4', {
    		    title: {
			    text: 'No. of persons disbursed spectacles statewise'
			    },
			    xAxis: {
			    categories: ['" . implode ( "','", $state_array ) . "'],
			    title: {
			    text: null
			    }
			    },
			    yAxis: {
			    min: 0,
			    title: {
			    text: 'Opd Count',
			    align: 'high'
			    },
			    labels: {
			    overflow: 'justify'
			    }
			    },
			    plotOptions: {
			    bar: {
			    dataLabels: {
			    enabled: true
			    }
			    }
			    },
			    series: [{
			    name: 'Actual',
			    		duration: 5000 ,
          			easing: 'easeOutBounce',
          type: 'column',
			    data: [" . implode ( ",", $actual ) . "]
			    },{
			    name: 'Target',
			    		duration: 5000 ,
          			easing: 'easeOutBounce',
          
				  type: 'spline',
			    		
			    data: [" . implode ( ",", $target ) . "]
			    }]
			    });
		    ";
		echo '</script>';
	}
	function chart_Spectacles_Examined_no_of_person_examined() {
		$data = $this->Graph_model->getrehchart ( 'Spectacles', 'no_of_person_examined' );
		$actual = $data ['actual_result']->opd_count;
		echo '<script>';
		$target = intval ( ((isset ( $data ['target_result']->opd_count ) && $data ['target_result']->opd_count != '') ? $data ['target_result']->opd_count : 0) );
		echo "Highcharts.chart('plain_graph_4', {
		chart: {
		type: 'bar'
	},
	title: {
	text: 'No. of persons disbursed spectacles refracted'
	},
	xAxis: {
	categories: ['Target', 'Actual'],
	title: {
	text: null
	}
	},
	yAxis: {
	min: 0,
	title: {
	text: 'Opd Count',
	align: 'high'
	},
	labels: {
	overflow: 'justify'
	}
	},
	plotOptions: {
	bar: {
	dataLabels: {
	enabled: true
	}
	}
	},
	series: [{
	name: 'Total',
	duration: 5000 ,
          			easing: 'easeOutBounce',
          
	data: [$actual, $target]
	}]
	});
	";
		echo '</script>';
	}
	
	// refratcion
	function chart_refraction_Examined_no_of_person_examined_state_wise_trend() {
		$data = $this->Graph_model->getrehchart ( 'Refraction', 'no_of_person_examined_state_wise_trend' );
		$actual_result = [ ];
		foreach ( $data ['actual_result'] as $arr ) {
			$actual_result [$arr->month] [$arr->ss_states_id] = $arr->opd_count;
		}
		$month_array = [ 
				'1' => 'Jan',
				'2' => 'Feb',
				'3' => 'Mar',
				'4' => 'Apr',
				'5' => 'May',
				'6' => 'Jun',
				'7' => 'Jul',
				'8' => 'Aug',
				'9' => 'Sept',
				'10' => 'Oct',
				'11' => 'Nov',
				'12' => 'Dec' 
		];
		$month = [ ];
		$state_array = [ ];
		$actual = [ ];
		for($i = 1; $i <= $data ['month']; $i ++) {
			foreach ( $data ['states'] as $arr ) {
				
				$actual [$arr->ss_states_name] [$i] = ((isset ( $actual_result [$i] [$arr->ss_states_id] )) ? $actual_result [$i] [$arr->ss_states_id] : 0);
			}
			$month [$i] = $month_array [$i];
		}
		$data_display = '';
		foreach ( $actual as $key => $val ) {
			$data_display .= "{
		    name: '" . $key . "',
		    		duration: 5000 ,
          			easing: 'easeOutBounce',
		    data: [" . implode ( ",", $val ) . "]
		    },";
		}
		echo '<script>';
		echo "Highcharts.chart('graph3_3', {
		    	chart: {
		    	type: 'area'
		    },
		    title: {
		    text: 'No. of persons refracted trend'
		    },
		    xAxis: {
		    categories: ['" . implode ( "','", $month ) . "'],
		    title: {
		    text: null
		    }
		    },
		    yAxis: {
		    min: 0,
		    title: {
		    text: 'Opd Count',
		    align: 'high'
		    },
		    labels: {
		    overflow: 'justify'
		    }
		    },
		    plotOptions: {
		    bar: {
		    dataLabels: {
		    enabled: true
		    }
		    }
		    },
		    series: [" . $data_display . "]
		    });
    ";
		echo '</script>';
	}
	function chart_refraction_Examined_no_of_person_examined_state_wise() {
		$data = $this->Graph_model->getrehchart ( 'Refraction', 'no_of_person_examined_state_wise' );
		$actual_result = [ ];
		foreach ( $data ['actual_result'] as $arr ) {
			$actual_result [$arr->ss_states_id] = $arr->opd_count;
		}
		$target_result = [ ];
		foreach ( $data ['target_result'] as $arr ) {
			$target_result [$arr->ss_states_id] = $arr->opd_count;
		}
		$state_array = [ ];
		$actual = [ ];
		$target = [ ];
		$acheived = [ ];
		foreach ( $data ['states'] as $arr ) {
			$state_array [$arr->ss_states_id] = $arr->ss_states_name;
			$actual [$arr->ss_states_id] = ((isset ( $actual_result [$arr->ss_states_id] )) ? $actual_result [$arr->ss_states_id] : 0);
			$target [$arr->ss_states_id] = ((isset ( $target_result [$arr->ss_states_id] )) ? $target_result [$arr->ss_states_id] : 0);
			$acheived [$arr->ss_states_id] = round ( $actual [$arr->ss_states_id] / (($target [$arr->ss_states_id] != 0) ? $target [$arr->ss_states_id] : 1) * 100, 2 );
		}
		echo '<script>';
		echo "Highcharts.chart('graph2_3', {
    	
			    title: {
			    text: 'No. of persons refracted statewise'
			    },
			    xAxis: {
			    categories: ['" . implode ( "','", $state_array ) . "'],
			    title: {
			    text: null
			    }
			    },
			    yAxis: {
			    min: 0,
			    title: {
			    text: 'Opd Count',
			    align: 'high'
			    },
			    labels: {
			    overflow: 'justify'
			    }
			    },
			    plotOptions: {
			    bar: {
			    dataLabels: {
			    enabled: true
			    }
			    }
			    },
			    series: [{
			    name: 'Actual',
			    		duration: 5000 ,
          			easing: 'easeOutBounce',
			    			type: 'column',
			    data: [" . implode ( ",", $actual ) . "]
			    },{
			    name: 'Target',
			    		duration: 5000 ,
          			easing: 'easeOutBounce',
			    		
				  type: 'spline',
			    	
			    data: [" . implode ( ",", $target ) . "]
			    }]
			    });
		    ";
		echo '</script>';
	}
	function chart_refraction_Examined_no_of_person_examined() {
		$data = $this->Graph_model->getrehchart ( 'Refraction', 'no_of_person_examined' );
		$actual = $data ['actual_result']->opd_count;
		echo '<script>';
		$target = intval ( ((isset ( $data ['target_result']->opd_count ) && $data ['target_result']->opd_count != '') ? $data ['target_result']->opd_count : 0) );
		$acheived = round ( $actual / (($target != 0) ? $target : 1) * 100, 2 );
		echo "Highcharts.chart('plain_graph_3', {
		chart: {
		type: 'bar'
	},
	title: {
	text: 'No. of persons refracteds'
	},
	xAxis: {
	categories: ['Target', 'Actual'],
	title: {
	text: null
	}
	},
	yAxis: {
	min: 0,
	title: {
	text: 'Opd Count',
	align: 'high'
	},
	labels: {
	overflow: 'justify'
	}
	},
	plotOptions: {
	bar: {
	dataLabels: {
	enabled: true
	}
	}
	},
	series: [{
	name: 'Total',
	duration: 5000 ,
          			easing: 'easeOutBounce',
          
	data: [$actual, $target]
	}]
	});
	";
		echo '</script>';
	}
	
	// CATARACT
	function chart_cataract_Examined_no_of_person_examined_state_wise_trend() {
		$data = $this->Graph_model->getrehchart ( 'Catract_Examined', 'no_of_person_examined_state_wise_trend' );
		$actual_result = [ ];
		foreach ( $data ['actual_result'] as $arr ) {
			$actual_result [$arr->month] [$arr->ss_states_id] = $arr->opd_count;
		}
		$month_array = [ 
				'1' => 'Jan',
				'2' => 'Feb',
				'3' => 'Mar',
				'4' => 'Apr',
				'5' => 'May',
				'6' => 'Jun',
				'7' => 'Jul',
				'8' => 'Aug',
				'9' => 'Sept',
				'10' => 'Oct',
				'11' => 'Nov',
				'12' => 'Dec' 
		];
		$month = [ ];
		$state_array = [ ];
		$actual = [ ];
		for($i = 1; $i <= $data ['month']; $i ++) {
			foreach ( $data ['states'] as $arr ) {
				
				$actual [$arr->ss_states_name] [$i] = ((isset ( $actual_result [$i] [$arr->ss_states_id] )) ? $actual_result [$i] [$arr->ss_states_id] : 0);
			}
			$month [$i] = $month_array [$i];
		}
		$data_display = '';
		foreach ( $actual as $key => $val ) {
			$data_display .= "{
		    name: '" . $key . "',
		    		duration: 5000 ,
          			easing: 'easeOutBounce',
          
		    data: [" . implode ( ",", $val ) . "]
		    },";
		}
		echo '<script>';
		echo "Highcharts.chart('graph3_2', {
		    	chart: {
		    	type: 'area'
		    },
		    title: {
		    text: 'Cataract operations trend'
		    },
		    xAxis: {
		    categories: ['" . implode ( "','", $month ) . "'],
		    title: {
		    text: null
		    }
		    },
		    yAxis: {
		    min: 0,
		    title: {
		    text: 'Opd Count',
		    align: 'high'
		    },
		    labels: {
		    overflow: 'justify'
		    }
		    },
		    plotOptions: {
		    bar: {
		    dataLabels: {
		    enabled: true
		    }
		    }
		    },
		    series: [" . $data_display . "]
		    });
    ";
		echo '</script>';
	}
	function chart_cataract_Examined_no_of_person_examined_state_wise() {
		$data = $this->Graph_model->getrehchart ( 'Catract_Examined', 'no_of_person_examined_state_wise' );
		$actual_result = [ ];
		foreach ( $data ['actual_result'] as $arr ) {
			$actual_result [$arr->ss_states_id] = $arr->opd_count;
		}
		$target_result = [ ];
		foreach ( $data ['target_result'] as $arr ) {
			$target_result [$arr->ss_states_id] = $arr->opd_count;
		}
		$state_array = [ ];
		$actual = [ ];
		$target = [ ];
		$acheived = [ ];
		foreach ( $data ['states'] as $arr ) {
			$state_array [$arr->ss_states_id] = $arr->ss_states_name;
			$actual [$arr->ss_states_id] = ((isset ( $actual_result [$arr->ss_states_id] )) ? $actual_result [$arr->ss_states_id] : 0);
			$target [$arr->ss_states_id] = ((isset ( $target_result [$arr->ss_states_id] )) ? $target_result [$arr->ss_states_id] : 0);
			$acheived [$arr->ss_states_id] = round ( $actual [$arr->ss_states_id] / (($target [$arr->ss_states_id] != 0) ? $target [$arr->ss_states_id] : 1) * 100, 2 );
		}
		echo '<script>';
		echo "Highcharts.chart('graph2_2', {
    	
			    title: {
			    text: 'Cataract operations statewise'
			    },
			    xAxis: {
			    categories: ['" . implode ( "','", $state_array ) . "'],
			    title: {
			    text: null
			    }
			    },
			    yAxis: {
			    min: 0,
			    title: {
			    text: 'Opd Count',
			    align: 'high'
			    },
			    labels: {
			    overflow: 'justify'
			    }
			    },
			    plotOptions: {
			    bar: {
			    dataLabels: {
			    enabled: true
			    }
			    }
			    },
			    series: [{
			    name: 'Actual',
			    		duration: 5000 ,
          			easing: 'easeOutBounce',
        			  type: 'column',
			    data: [" . implode ( ",", $actual ) . "]
			    },{
			    name: 'Target',
			    		duration: 5000 ,
          			easing: 'easeOutBounce',
          
				  type: 'spline',
			    		
			    data: [" . implode ( ",", $target ) . "]
			    }]
			    });
		    ";
		echo '</script>';
	}
	function chart_cataract_Examined_no_of_person_examined() {
		$data = $this->Graph_model->getrehchart ( 'Catract_Examined', 'no_of_person_examined' );
		$actual = $data ['actual_result']->opd_count;
		echo '<script>';
		$target = intval ( ((isset ( $data ['target_result']->opd_count ) && $data ['target_result']->opd_count != '') ? $data ['target_result']->opd_count : 0) );
		echo "Highcharts.chart('plain_graph_2', {
		chart: {
		type: 'bar'
	},
	title: {
	text: 'Cataract operations'
	},
	xAxis: {
	categories: ['Target', 'Actual'],
	title: {
	text: null
	}
	},
	yAxis: {
	min: 0,
	title: {
	text: 'Opd Count',
	align: 'high'
	},
	labels: {
	overflow: 'justify'
	}
	},
	plotOptions: {
	bar: {
	dataLabels: {
	enabled: true
	}
	}
	},
	series: [{
	name: 'Total',
	duration: 5000 ,
  	easing: 'easeOutBounce',
          
	data: [$actual, $target]
	}]
	});
	";
		echo '</script>';
	}
	
	// OPD
	function chart_OPD_Examined_no_of_person_examined_state_wise_trend() {
		$data = $this->Graph_model->getrehchart ( 'OPD_Examined', 'no_of_person_examined_state_wise_trend' );
		$actual_result = [ ];
		foreach ( $data ['actual_result'] as $arr ) {
			$actual_result [$arr->month] [$arr->ss_states_id] = $arr->opd_count;
		}
		$month_array = [ 
				'1' => 'Jan',
				'2' => 'Feb',
				'3' => 'Mar',
				'4' => 'Apr',
				'5' => 'May',
				'6' => 'Jun',
				'7' => 'Jul',
				'8' => 'Aug',
				'9' => 'Sept',
				'10' => 'Oct',
				'11' => 'Nov',
				'12' => 'Dec' 
		];
		$month = [ ];
		$state_array = [ ];
		$actual = [ ];
		for($i = 1; $i <= $data ['month']; $i ++) {
			foreach ( $data ['states'] as $arr ) {
				
				$actual [$arr->ss_states_name] [$i] = ((isset ( $actual_result [$i] [$arr->ss_states_id] )) ? $actual_result [$i] [$arr->ss_states_id] : 0);
			}
			$month [$i] = $month_array [$i];
		}
		$data_display = '';
		foreach ( $actual as $key => $val ) {
			$data_display .= "{
		    name: '" . $key . "',
		    		duration: 5000 ,
          			easing: 'easeOutBounce',
          
		    data: [" . implode ( ",", $val ) . "]
		    },";
		}
		echo '<script>';
		echo "Highcharts.chart('graph3_1', {
		    	chart: {
		    	type: 'area'
		    },
		    title: {
		    text: 'Number of Person examined'
		    },
		    xAxis: {
		    categories: ['" . implode ( "','", $month ) . "'],
		    title: {
		    text: null
		    }
		    },
		    yAxis: {
		    min: 0,
		    title: {
		    text: 'Opd Count',
		    align: 'high'
		    },
		    labels: {
		    overflow: 'justify'
		    }
		    },
		    plotOptions: {
		    bar: {
		    dataLabels: {
		    enabled: true
		    }
		    }
		    },
		    series: [" . $data_display . "]
		    });
    ";
		echo '</script>';
	}
	function chart_OPD_Examined_no_of_person_examined_state_wise_map() {
		$data = $this->Graph_model->getrehchart ( 'OPD_Examined', 'no_of_person_examined_state_wise' );
		$actual_result = [ ];
		foreach ( $data ['actual_result'] as $arr ) {
			$actual_result [$arr->ss_states_id] = $arr->opd_count;
		}
		$target_result = [ ];
		foreach ( $data ['target_result'] as $arr ) {
			$target_result [$arr->ss_states_id] = $arr->opd_count;
		}
		$state_array = [ ];
		$actual = [ ];
		$target = [ ];
		$acheived = [ ];
		$all_states=getStateCode();
		$data1=array();
		foreach ( $data ['states'] as $arr ) {
			if(in_array($arr->ss_states_name,$all_states)){
				$key=array_keys($all_states,$arr->ss_states_name);
				if(isset($key[0]))
				 $data1[]= '"'.$key[0].'": '.((isset ( $actual_result [$arr->ss_states_id] )) ? $actual_result [$arr->ss_states_id] : 0);
			}
			
		}
		echo '<script>
				var graph4_1_data = {
			  '.implode(",", $data1).'
			};';
		echo "
				$(function(){
    $('#graph4_1').vectorMap({map: 'in_mill',
    	series: {
    	    regions: [{
    	      values: graph4_1_data,
    	      scale: ['#C8EEFF', '#0071A4'],
    	      normalizeFunction: 'polynomial'
    	    }]
    	  },
	onRegionTipShow: function(e, el, code){
    el.html(el.html()+' (No. of person examined - '+graph4_1_data[code]+')');
  }
	});
				});
  
		    ";
		echo '</script>';
	}
	function chart_OPD_Examined_no_of_person_examined_state_wise() {
		$data = $this->Graph_model->getrehchart ( 'OPD_Examined', 'no_of_person_examined_state_wise' );
		$actual_result = [ ];
		foreach ( $data ['actual_result'] as $arr ) {
			$actual_result [$arr->ss_states_id] = $arr->opd_count;
		}
		$target_result = [ ];
		foreach ( $data ['target_result'] as $arr ) {
			$target_result [$arr->ss_states_id] = $arr->opd_count;
		}
		$state_array = [ ];
		$actual = [ ];
		$target = [ ];
		$acheived = [ ];
		foreach ( $data ['states'] as $arr ) {
			$state_array [$arr->ss_states_id] = $arr->ss_states_name;
			$actual [$arr->ss_states_id] = ((isset ( $actual_result [$arr->ss_states_id] )) ? $actual_result [$arr->ss_states_id] : 0);
			$target [$arr->ss_states_id] = ((isset ( $target_result [$arr->ss_states_id] )) ? $target_result [$arr->ss_states_id] : 0);
			$acheived [$arr->ss_states_id] = round ( $actual [$arr->ss_states_id] / (($target [$arr->ss_states_id] != 0) ? $target [$arr->ss_states_id] : 1) * 100, 2 );
		}
		echo '<script>';
		echo "Highcharts.chart('graph2_1', {
    	
			    title: {
			    text: 'Number of Person examined'
			    },
			    xAxis: {
			    categories: ['" . implode ( "','", $state_array ) . "'],
			    title: {
			    text: null
			    }
			    },
			    yAxis: {
			    min: 0,
			    title: {
			    text: 'Opd Count',
			    align: 'high'
			    },
			    labels: {
			    overflow: 'justify'
			    }
			    },
			    plotOptions: {
			    bar: {
			    dataLabels: {
			    enabled: true
			    }
			    }
			    },
			    series: [{
			    		
			    name: 'Actual',
			    duration: 5000 ,
          		easing: 'easeOutBounce',
          
        		type: 'column',
			    data: [" . implode ( ",", $actual ) . "]
			    },{
				  type: 'spline',
			    		
			    name: 'Target',
			   	duration: 5000 ,
          		easing: 'easeOutBounce',
          
			    data: [" . implode ( ",", $target ) . "]
			    }]
			    });
		    ";
		echo '</script>';
	}
	function chart_OPD_Examined_no_of_person_examined() {
		$data = $this->Graph_model->getrehchart ( 'OPD_Examined', 'no_of_person_examined' );
		 $actual = $data ['actual_result']->opd_count;
		echo '<script>';
		$target = intval ( ((isset ( $data ['target_result']->opd_count ) && $data ['target_result']->opd_count != '') ? $data ['target_result']->opd_count : 0) );
		echo "Highcharts.chart('plain_graph_1', {
    	chart: {
    	type: 'bar'
    	},
    	title: {
    	text: 'Number of Person examined'
    	},
    	xAxis: {
    	categories: [ 'Actual','Target'],
    	title: {
    	text: null
    	}
    	},
    	yAxis: {
    	min: 0,
    	title: {
    	text: 'Opd Count',
    	align: 'high'
    	},
    	labels: {
    	overflow: 'justify'
    	}
    	},
    	plotOptions: {
    	bar: {
    	dataLabels: {
    	enabled: true
    	}
    	}
    	},
    	series: [{
    	name: 'Total',
    	duration: 5000 ,
         easing: 'easeOutBounce',
          
    	data: [$actual, $target]
    	}]
    	});
    	";
		echo '</script>';
	}
}
