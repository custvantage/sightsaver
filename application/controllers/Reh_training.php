<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Reh_training extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Reh_training_model');
		$this->load->model('Reh_main_entry_model');
		check_login();
		//$this->load->library('pagination');
    }

    public function index() {
		$data['training_data'] = "";
		$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		$data['include'] = "reh/training_cb";
		$this->load->view('container_login_dis', $data);
    }
	public function create()
	{
		$userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
		
			$this->form_validation->set_rules('month_data','Month','required');
					
			if($this->form_validation->run()==true)
			{
				$partner_id = $_SESSION["partner_id"];
				$created_on = server_date_time();
				$ajax_id1 = $this->input->post('id_ajax');
				$serialno = $this->input->post('training_serialno');
				$block_name = $this->input->post('training_block');
				$person_name = $this->input->post('training_person');
				$gender = $this->input->post('training_gender');
				$designation = $this->input->post('training_designation');
				$designation_detail = $this->input->post('training_designation_detail');
				$loc_act = $this->input->post('training_loc_activity');			
				$loc_act_detail = $this->input->post('training_loc_activity_detail');
				$activity = $this->input->post('training_activity');
				$activity_detail = $this->input->post('training_activity_detail');
				$participant_from = $this->input->post('training_participant_from');
				$participant_from_detail = $this->input->post('training_participant_from_detail');
				$topic_cover = $this->input->post('training_topic_cover');
				$combine_day = "01-".$this->input->post('month_data');
				$data_month = date("Y-m-d",strtotime($combine_day));
				
				if($this->input->post('id_ajax')=='')
				{
				$training_data = array('ss_partner_id'=>$partner_id,'ss_sno'=>$serialno,'ss_reh_training_block'=>$block_name,'ss_reh_training_person'=>$person_name,'ss_reh_training_gender'=>$gender,'ss_reh_training_designation'=>$designation,'ss_reh_training_designation_detail'=>$designation_detail,'ss_reh_training_loc_activity'=>$loc_act,'ss_reh_training_loc_activity_detail'=>$loc_act_detail,'ss_reh_training_activity'=>$activity,'ss_reh_training_activity_detail'=>$activity_detail,'ss_reh_training_participant_from'=>$participant_from,'ss_reh_training_participant_from_detail'=>$participant_from_detail,'ss_reh_training_topic_cover'=>$topic_cover,'ss_reh_training_month'=>$data_month,'ss_reh_training_created_on'=>$created_on);
				
				$creation_check = $this->Reh_training_model->createTraining($training_data);
				} else 
				{
					$update_training_data = array('ss_partner_id'=>$partner_id,'ss_sno'=>$serialno,'ss_reh_training_block'=>$block_name,'ss_reh_training_person'=>$person_name,'ss_reh_training_gender'=>$gender,'ss_reh_training_designation'=>$designation,'ss_reh_training_designation_detail'=>$designation_detail,'ss_reh_training_loc_activity'=>$loc_act,'ss_reh_training_loc_activity_detail'=>$loc_act_detail,'ss_reh_training_activity'=>$activity,'ss_reh_training_activity_detail'=>$activity_detail,'ss_reh_training_participant_from'=>$participant_from,'ss_reh_training_participant_from_detail'=>$participant_from_detail,'ss_reh_training_topic_cover'=>$topic_cover,'ss_reh_training_month'=>$data_month,'ss_reh_training_created_on'=>$created_on);
				
				$creation_check = $this->Reh_training_model->updateTraining($update_training_data,$ajax_id1);
				}
				$this->session->set_flashdata('success','Your data is successfully saved');
				//redirect('reh_training');
				redirect('filter_training_reh?month_from='.$_SESSION["month_date"]);
			}else
			{
			$data['include'] = "reh/training_cb";
			$this->load->view('container_login_dis', $data);
			}
		}
	}
	public function filter_training_data()
	{   
	    $userid=$this->session->userdata('userinfo')['user_id'];
	    $data['dis']=$this->Reh_main_entry_model->user_wise_partner($userid);
		if($this->input->server('REQUEST_METHOD') === 'GET')
		{
			
			//$this->form_validation->set_rules('month_from','Month','required');
					
			//if($this->form_validation->run()==true)
			//{
				$combine_day = "01-".$this->input->get('month_from');
				$month_from = date("Y-m-d",strtotime($combine_day));

				$partner_id = $_SESSION["partner_id"];
				$data['training_data'] = $this->Reh_training_model->getTraining($month_from,$partner_id);
			//} 
			$data['include'] = "reh/training_cb";
			$this->load->view('container_login_dis', $data);
		}else
		{
			redirect('reh_training');
		}
	}
	
	public function delete_training_reh()
	{ 
		if(!empty($this->uri->segment(2)))
        {
	     $id = base64_decode($this->uri->segment(2));
		 $this->Reh_training_model->deleteHehTraining($id);
	     redirect('reh_training');
		}
	}
	
	public function edit_training_reh()
	{ 
	     $id = base64_decode($this->input->post('id'));
		 $data=$this->Reh_training_model->editFetchData($id);
		 if(!empty($data))
		 {
			 $data2 = json_encode($data[0]);
			 echo $data2."$$$".$this->security->get_csrf_hash();
		 }
		 else
		 {
			 echo 0;
		 }
	}
	/*=============================
		training cb excel upload
	===============================*/
	public function excelupload_traning_cb()
	{		
		
		if(!empty($_FILES['upload_publish_file']['name']))
		{
			$created_on = server_date_time();
			$combine_day = "01-".$this->input->post('month_data');
			$data_month = date("Y-m-d",strtotime($combine_day));
			$partner_id = $_SESSION["partner_id"];
		
			require_once(EXCELDIR."PHPExcel.php");
			$excel_file= $_FILES['upload_publish_file']['tmp_name'];
			$inputFileName = $excel_file;
				try 
					{
					$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
					} 
				catch(Exception $e)
					{
					die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
					}
					require_once(EXCELDIR."PHPExcel/IOFactory.php");

					$objReader = PHPExcel_IOFactory::createReader('Excel2007');
					$inputFileType = 'Excel2007';					
					$sheetIndex = 0;
					$objReader = PHPExcel_IOFactory::createReader($inputFileType);
					$sheetnames = $objReader->listWorksheetNames($inputFileName);
					$objReader->setLoadSheetsOnly($sheetnames[$sheetIndex]);
					$objPHPExcel = $objReader->load($inputFileName);
					$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);					
					$start=3;
					$end = $highestRow = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
						for ($row = $start; $row <=$end; $row++) 
						{									
							$excel_data[] = array('ss_partner_id'=>$partner_id,
							'ss_sno'=>$objPHPExcel->getActiveSheet()->getCell('A'.$row)->getValue(),					
							'ss_reh_training_block'=>$objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue(),
							'ss_reh_training_person'=>$objPHPExcel->getActiveSheet()->getCell('H'.$row)->getValue(),
							'ss_reh_training_gender'=>$objPHPExcel->getActiveSheet()->getCell('I'.$row)->getValue(),
							'ss_reh_training_designation'=>$objPHPExcel->getActiveSheet()->getCell('J'.$row)->getValue(),
							'ss_reh_training_loc_activity'=>$objPHPExcel->getActiveSheet()->getCell('K'.$row)->getValue(),
							'ss_reh_training_activity'=>$objPHPExcel->getActiveSheet()->getCell('L'.$row)->getValue(),
							'ss_reh_training_activity_detail'=>$objPHPExcel->getActiveSheet()->getCell('M'.$row)->getValue(),
							'ss_reh_training_participant_from'=>$objPHPExcel->getActiveSheet()->getCell('N'.$row)->getValue(),
							'ss_reh_training_participant_from_detail'=>$objPHPExcel->getActiveSheet()->getCell('O'.$row)->getValue(),
							'ss_reh_training_topic_cover'=>$objPHPExcel->getActiveSheet()->getCell('P'.$row)->getValue(),					
							'ss_reh_training_month'=>$data_month,'ss_reh_training_created_on'=>$created_on);
						} 
					if(!empty($excel_data)) {
					$creation_check = $this->Reh_training_model->createTrainingCBExcel($excel_data);
					}
				$data['training_data'] = $this->Reh_training_model->getTraining($data_month,$partner_id);
			//	$this->session->set_flashdata('success','OPD No:'.for ($row = $start; $row <=$end; $row++) 
					//	{ $objPHPExcel->getActiveSheet()->getCell('G'.$row)->getValue() } .'  Your data is successfully saved');
				$data['include'] = "reh/training_cb";
				$this->load->view('container_login_dis', $data);
		}else{
			redirect('reh_training');
		}
		
	}
}
