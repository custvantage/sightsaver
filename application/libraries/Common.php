<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
Class Common{
	
	public function __construct()
	{		
		$this->CI = & get_instance();
	}
	public function getPartners()
	{		
		$query = $this->CI->db->query("Select ss_partners_id,ss_partners_name From ss_partners");	
		return $query->result();
	}
	public function getComments($user_id,$month_from,$partner_id,$project_name,$module_name)
	{
		$query = $this->CI->db->query("Select comments.comment,DATE_FORMAT(comments.created_on,'%M %d, %Y')as created_date,DATE_FORMAT(comments.created_on,'%h:%i %p')as created_time,partner.ss_partners_name From ss_comments as comments INNER JOIN ss_partners as partner ON partner.ss_partners_id=comments.ss_partner Where comments.ss_user_id='".$user_id."' AND comments.ss_month='".$month_from."' AND comments.ss_partner='".$partner_id."' AND comments.project_name='".$project_name."' AND comments.module_name='".$module_name."'");	
		return $query->result();
	}
	public function createComments($data)
	{
		$query = $this->CI->db->insert('ss_comments',$data);	
		return 1;
	}
	public function partnerInfo($partner_id)
	{
		$query = $this->CI->db->query("Select ss_partners_name From ss_partners Where ss_partners_id='".$partner_id."'");	
		return $query->row('ss_partners_name');
	}
	public function getPmpoComments($partner_id,$month_from,$project_name,$module_name)
	{
		$query = $this->CI->db->query("Select comments.ss_comments_id,comments.comment,DATE_FORMAT(comments.created_on,'%d-%m-%Y %H:%i:%s')as created_on,partner.ss_partners_name From ss_comments as comments INNER JOIN ss_partners as partner ON partner.ss_partners_id=comments.ss_partner Where comments.ss_partner='".$partner_id."' AND comments.ss_month='".$month_from."' AND comments.project_name='".$project_name."' AND comments.module_name='".$module_name."'");	
		return $query->result();
	}
	public function checkMprSubmit($partner_id,$project_name,$module_name,$data_month,$status)
	{
		$query = $this->CI->db->query("Select ss_mpr_summary_id From ss_mpr_summary Where ss_mpr_summary_partner_id='".$partner_id."' AND ss_mpr_project_name='".$project_name."' AND ss_mpr_module_name='".$module_name."' AND ss_mpr_report_month='".$data_month."' AND status='".$status."'");
		return $query->row();
	}
	
}