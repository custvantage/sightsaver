<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
Class Metricdata{
	
	public function __construct()
	{		
		$this->CI = & get_instance();
	}
	public function getOneColVal($metric_master_id,$month_from,$partner_id)
	{		
		$query = $this->CI->db->query("Select oc.ss_one_column_data_value From ss_one_column_data as oc Where oc.ss_one_column_data_month = '".$month_from."' AND oc.ss_partner_id = '".$partner_id."' AND ss_metric_master_id='".$metric_master_id."'");	
		return $query->row('ss_one_column_data_value');
	}
	public function getFourColVal($metric_master_id,$month_from,$partner_id)
	{		
		$query = $this->CI->db->query("Select ss_four_column_data_value_men,ss_four_column_data_value_women,ss_four_column_data_value_adult,ss_four_column_data_value_trans,ss_four_column_data_value_boys,ss_four_column_data_value_girls,ss_four_column_data_value_child From ss_four_column_data Where ss_four_column_data_month = '".$month_from."' AND ss_partner_id = '".$partner_id."' AND ss_metric_master_id='".$metric_master_id."'");		
		return $query->row();
	}
	public function getAnalysisVal($metric_master_id,$month_from,$partner_id)
	{
		$query = $this->CI->db->query("Select ss_analysis_data_value From ss_analysis_data Where ss_analysis_data_month = '".$month_from."' AND ss_partner_id = '".$partner_id."' AND ss_metric_master_id='".$metric_master_id."'");
		return $query->row('ss_analysis_data_value');
	}
/////////////////////////////////////////////////////////////////////////////	
	
	public function getOneColVal_mpr_partner($metric_master_id,$month_from,$partner_id)
	{		
		$query = $this->CI->db->query("Select sum(oc.ss_mpr_one_column_data_value) as ss_mpr_one_column_data_value From ss_mpr_one_column_data as oc Where oc.ss_mpr_one_column_data_month = '".$month_from."' AND oc.ss_mpr_partner_id = '".$partner_id."' AND ss_mpr_metric_master_id='".$metric_master_id."'");	
		$query->row('ss_mpr_one_column_data_value');
		echo $this->CI->db->last_query(); exit;
		
	}
	
	
/////////////////////////////////////////////////////////////////////////////	
  public function getOneColVal_ampr($metric_master_id,$month_from,$partner_id)
	{		//echo $metric_master_id; echo $month_from; echo $partner_id; die;
	
		$query = $this->CI->db->query("Select oc.ss_mpr_one_column_data_value as ss_mpr_one_column_data_value  
From ss_mpr_one_column_data as oc 
Where oc.ss_mpr_one_column_data_month = '".$month_from."' 
AND oc.ss_mpr_partner_id = '".$partner_id."' 
AND oc.ss_mpr_metric_master_id='".$metric_master_id."'
AND oc.ss_mpr_one_column_data_id = (select max(oc1.ss_mpr_one_column_data_id) from ss_mpr_one_column_data as oc1 where oc1.ss_mpr_one_column_data_month = oc.ss_mpr_one_column_data_month
AND oc1.ss_mpr_partner_id =oc.ss_mpr_partner_id
AND oc1.ss_mpr_metric_master_id= oc.ss_mpr_metric_master_id)
order by oc.ss_mpr_metric_master_id");	
		return $query->row('ss_mpr_one_column_data_value');
	}
///yearly pda mpr///// one column	
	public function getOneColVal_ampr_yearly($metric_master_id,$month_from,$partner_id)
	{		
		$query = $this->CI->db->query("Select oc.ss_one_column_data_value From ss_one_column_data as oc Where oc.ss_one_column_data_month = '".$month_from."' AND oc.ss_partner_id = '".$partner_id."' AND ss_metric_master_id='".$metric_master_id."'");	
		return $query->row('ss_one_column_data_value');
	}
	//////////////end of the yearly pda mpr//// one column
	public function getFourColVal_ampr($metric_master_id,$month_from,$partner_id)
	{		
		$query = $this->CI->db->query("Select oc.ss_mpr_four_column_data_value_men as ss_mpr_four_column_data_value_men,oc.ss_mpr_four_column_data_value_women as ss_mpr_four_column_data_value_women,oc.ss_mpr_four_column_data_value_trans asss_mpr_four_column_data_value_trans,oc.ss_mpr_four_column_data_value_boys as ss_mpr_four_column_data_value_boys,oc.ss_mpr_four_column_data_value_girls as ss_mpr_four_column_data_value_girls From ss_mpr_four_column_data  as oc Where oc.ss_mpr_four_column_data_month = '".$month_from."' AND oc.ss_mpr_partner_id = '".$partner_id."' AND oc.ss_mpr_metric_master_id='".$metric_master_id."' AND oc.ss_mpr_four_column_data_id = (select max(oc1.ss_mpr_four_column_data_id) from ss_mpr_four_column_data as oc1 where oc1.ss_mpr_four_column_data_month = oc.ss_mpr_four_column_data_month
AND oc1.ss_mpr_partner_id =oc.ss_mpr_partner_id
AND oc1.ss_mpr_metric_master_id= oc.ss_mpr_metric_master_id)
order by oc.ss_mpr_four_column_data_id");		
		return $query->row();
	}
	////////////////// four col value pda mpr/// data fetch from ss_four column table
	public function getFourColVal_ampr_yearly($metric_master_id,$month_from,$partner_id)
	{		
		$query = $this->CI->db->query("Select ss_four_column_data_value_men ,ss_four_column_data_value_women, ss_four_column_data_value_adult, ss_four_column_data_value_trans,ss_four_column_data_value_boys,ss_four_column_data_value_girls, ss_four_column_data_value_child From  ss_four_column_data Where YEAR(ss_four_column_data_month) = '".$month_from."' AND ss_partner_id = '".$partner_id."' AND ss_metric_master_id='".$metric_master_id."'");		
		return $query->row();
	}
	/////////////// end of  the ///////////////////
	public function getEightColVal_ampr($metric_master_id,$month_from,$partner_id)
	{	
		$query = $this->CI->db->query("Select oc.ss_mpr_elementory_sanctioned as ss_mpr_elementory_sanctioned,oc.ss_mpr_elementory_boys as ss_mpr_elementory_boys,oc.ss_mpr_elementory_girls as ss_mpr_elementory_girls,oc.ss_mpr_elementory_vacant as ss_mpr_elementory_vacant,oc.ss_mpr_secondary_sanctioned as ss_mpr_secondary_sanctioned,oc.ss_mpr_secondary_boys as ss_mpr_secondary_boys,oc.ss_mpr_secondary_girls as ss_mpr_secondary_girls,oc.ss_mpr_secondary_vacant as ss_mpr_secondary_vacant From ss_mpr_eight_column_data as oc Where oc.ss_mpr_data_month = '".$month_from."' AND oc.ss_mpr_partner_id = '".$partner_id."' AND oc.ss_mpr_metric_master_id='".$metric_master_id."' AND oc.ss_mpr_metric_master_id='".$metric_master_id."' AND oc.ss_mpr_eight_data_id = (select max(oc1.ss_mpr_eight_data_id) from ss_mpr_eight_column_data as oc1 where oc1.ss_mpr_data_month = oc.ss_mpr_data_month
AND oc1.ss_mpr_partner_id =oc.ss_mpr_partner_id
AND oc1.ss_mpr_metric_master_id= oc.ss_mpr_metric_master_id)
order by oc.ss_mpr_eight_data_id");		
		return $query->row();
	}
	
	
	public function getAnalysisVal_ampr($metric_master_id,$month_from,$partner_id)
	{
		//"Select group_concat(ss_mpr_analysis_data_value SEPARATOR ',') as ss_mpr_analysis_data_value From ss_mpr_analysis_data Where ss_mpr_analysis_data_month = '".$month_from."' AND ss_mpr_partner_id = '".$partner_id."' AND ss_mpr_metric_master_id='".$metric_master_id."'"
		
		//$query = $this->CI->db->query("Select sum(ss_mpr_analysis_data_value) as ss_mpr_analysis_data_value From ss_mpr_analysis_data Where ss_mpr_analysis_data_month = '".$month_from."' AND ss_mpr_partner_id = '".$partner_id."' AND ss_mpr_metric_master_id='".$metric_master_id."'");
		$query = $this->CI->db->query("Select oc.ss_mpr_analysis_data_value as ss_mpr_analysis_data_value From ss_mpr_analysis_data as oc Where oc.ss_mpr_analysis_data_month = '".$month_from."' AND oc.ss_mpr_partner_id = '".$partner_id."' AND oc.ss_mpr_metric_master_id='".$metric_master_id."'  AND oc.ss_mpr_analysis_data_id = (select max(oc1.ss_mpr_analysis_data_id) from ss_mpr_analysis_data as oc1 where oc1.ss_mpr_analysis_data_month = oc.ss_mpr_analysis_data_month
AND oc1.ss_mpr_partner_id =oc.ss_mpr_partner_id
AND oc1.ss_mpr_metric_master_id= oc.ss_mpr_metric_master_id)
order by oc.ss_mpr_analysis_data_id");
		
		return $query->row('ss_mpr_analysis_data_value');
	}
	
	
	
	
	public function getEightColVal($metric_master_id,$month_from,$partner_id)
	{
		$query = $this->CI->db->query("Select ss_elementory_sanctioned,ss_elementory_boys,ss_elementory_girls,ss_elementory_vacant,ss_secondary_sanctioned,ss_secondary_boys,ss_secondary_girls,ss_secondary_vacant From ss_eight_data Where ss_data_month = '".$month_from."' AND ss_partner_id = '".$partner_id."' AND ss_metric_master_id='".$metric_master_id."'");
		return $query->row();
	}
	
	//$key,$month_id[0],$month_id[1],$month_id[2],$month_id[3];
	// for the one column 
	public function getOneColVal_mpr($metric_master_id = null,$state_name=null,$month_from = null,$month_from_last = null,$partner_id = null,$district_id = null)
	{	
	$q="";
	  if($state_name and $state_name !='all_state')
	  {
		  $q.=" and state.ss_states_name='".$state_name."' ";
	  }
	  if($district_id and $district_id !='')
	  {
		  $q.=" and dist.ss_district_id='".$district_id."' ";
	  }		  
	  if($month_from)
	  {
		  $q.=" and oc.ss_mpr_one_column_data_month >= '".$month_from."' ";
	  }
	  if($month_from_last)
	  {
		  $q.=" and oc.ss_mpr_one_column_data_month <= '".$month_from_last."' ";
	  }
	  if($partner_id)
	  {
		  $q.=" and oc.ss_mpr_partner_id = '".$partner_id."' ";
	  }
		  $query = $this->CI->db->query("Select sum(oc.ss_mpr_one_column_data_value) as ss_mpr_one_column_data_value From ss_mpr_one_column_data as oc 
		  left join ss_partners as part on part.ss_partners_id=oc.ss_mpr_partner_id left join ss_district as dist on part.ss_partners_districts=dist.ss_district_id left join ss_states as state on state.ss_states_id=dist.ss_states_ss_states_id Where 1 $q AND ss_mpr_metric_master_id='".$metric_master_id."'");	
		return $query->row('ss_mpr_one_column_data_value');
		}
		// for the yearly one column
		public function getOneColVal_yearly($metric_master_id = null,$state_name=null,$month_from = null,$month_from_last = null,$partner_id = null,$district_id = null)
	{	
	$q="";
	  if($state_name and $state_name !='all_state')
	  {
		  $q.=" and state.ss_states_name='".$state_name."' ";
	  }
	  if($district_id and $district_id !='')
	  {
		  $q.=" and dist.ss_district_id='".$district_id."' ";
	  }	  
	   if($month_from)
	  {
		  $q.=" and oc.ss_one_column_data_month >= '".$month_from."' ";
	  }
	  if($month_from_last)
	  {
		  $q.=" and oc.ss_one_column_data_month <= '".$month_from_last."' ";
	  }
	  if($partner_id)
	  {
		  $q.=" and oc.ss_partner_id = '".$partner_id."' ";
	  }
	       
		  $query = $this->CI->db->query("Select sum(oc.ss_one_column_data_value) as ss_one_column_data_value From ss_one_column_data as oc 
		  left join ss_partners as part on part.ss_partners_id=oc.ss_partner_id left join ss_district as dist on part.ss_partners_districts=dist.ss_district_id left join ss_states as state on state.ss_states_id=dist.ss_states_ss_states_id Where 1 $q AND ss_metric_master_id='".$metric_master_id."'");	
		return $query->row('ss_one_column_data_value');
		}
		
		
		
		// end of the yearly one column
	// for the four column
	public function getFourColVal_mpr($metric_master_id = null,$state_name=null,$month_from = null,$month_from_last = null,$partner_id = null,$district_id = null)
	{	
	$q="";
	  if($state_name and $state_name !='all_state')
	  {
		  $q.=" and state.ss_states_name='".$state_name."' ";
	  }
	  if($district_id and $district_id !='')
	  {
		  $q.=" and dist.ss_district_id='".$district_id."' ";
	  }	  
	   if($month_from)
	  {
		  $q.=" and four.ss_mpr_four_column_data_month >= '".$month_from."' ";
	  }
	  if($month_from_last)
	  {
		  $q.=" and four.ss_mpr_four_column_data_month <= '".$month_from_last."' ";
	  }
	  if($partner_id)
	  {
		  $q.=" and four.ss_mpr_partner_id = '".$partner_id."' ";
	  }
		  $query = $this->CI->db->query("Select sum(four.ss_mpr_four_column_data_value_men) as ss_mpr_four_column_data_value_men,sum(four.ss_mpr_four_column_data_value_women) as ss_mpr_four_column_data_value_women, sum(four.ss_mpr_four_column_data_value_adult) as ss_mpr_four_column_data_value_adult, sum(four.ss_mpr_four_column_data_value_trans) as ss_mpr_four_column_data_value_trans,sum(four.ss_mpr_four_column_data_value_boys) as ss_mpr_four_column_data_value_boys,sum(four.ss_mpr_four_column_data_value_girls) as ss_mpr_four_column_data_value_girls, sum(four.ss_mpr_four_column_data_value_child) as ss_mpr_four_column_data_value_child From ss_mpr_four_column_data as four 
		  left join ss_partners as part on part.ss_partners_id=four.ss_mpr_partner_id left join ss_district as dist on part.ss_partners_districts=dist.ss_district_id left join ss_states as state on state.ss_states_id=dist.ss_states_ss_states_id Where 1 $q AND four.ss_mpr_metric_master_id='".$metric_master_id."'");	
		  //echo $this->CI->db->last_query();
		  $data = $query->row();
		  //print_r($data); exit; 
		  //exit;
		  return $data;
	}
	
	//for the yearly
	// for the four column
	public function getFourColVal_yearly($metric_master_id = null,$state_name=null,$month_from = null,$month_from_last = null,$partner_id = null,$district_id = null)
	{	
	$q="";
	  if($state_name and $state_name !='all_state')
	  {
		  $q.=" and state.ss_states_name='".$state_name."' ";
	  }
	  if($district_id and $district_id !='')
	  {
		  $q.=" and dist.ss_district_id='".$district_id."' ";
	  }		  
	   if($month_from)
	  {
		  $q.=" and four.ss_four_column_data_month >= '".$month_from."' ";
	  }
	  if($month_from_last)
	  {
		  $q.=" and four.ss_four_column_data_month <= '".$month_from_last."' ";
	  }
	  if($partner_id)
	  {
		  $q.=" and four.ss_partner_id = '".$partner_id."' ";
	  }
		  $query = $this->CI->db->query("Select sum(four.ss_four_column_data_value_men) as ss_four_column_data_value_men,sum(four.ss_four_column_data_value_women) as ss_four_column_data_value_women, sum(four.ss_four_column_data_value_adult) as ss_four_column_data_value_adult, sum(four.ss_four_column_data_value_trans) as ss_four_column_data_value_trans,sum(four.ss_four_column_data_value_boys) as ss_four_column_data_value_boys,sum(four.ss_four_column_data_value_girls) as ss_four_column_data_value_girls, sum(four.ss_four_column_data_value_child) as ss_four_column_data_value_child From ss_four_column_data as four 
		  left join ss_partners as part on part.ss_partners_id=four.ss_partner_id left join ss_district as dist on part.ss_partners_districts=dist.ss_district_id left join ss_states as state on state.ss_states_id=dist.ss_states_ss_states_id Where 1 $q AND four.ss_metric_master_id='".$metric_master_id."'");	
		  //echo $this->CI->db->last_query();
		  $data = $query->row();
		  //print_r($data); exit; 
		  //exit;
		  return $data;
	 }
	//end of the yearly  four column 
	 
// for the eight column data
public function getEightColVal_mpr($metric_master_id = null,$state_name=null,$month_from = null,$month_from_last = null,$partner_id = null)
	{	
	$q="";
	  if($state_name)
	  {
		  $q.=" and state.ss_states_name='".$state_name."' ";
	  }	  
	   if($month_from)
	  {
		  $q.=" and eight.ss_mpr_data_month >= '".$month_from."' ";
	  }
	  if($month_from_last)
	  {
		  $q.=" and eight.ss_mpr_data_month <= '".$month_from_last."' ";
	  }
	  if($partner_id)
	  {
		  $q.=" and eight.ss_mpr_partner_id = '".$partner_id."' ";
	  }
		  $query = $this->CI->db->query("Select sum(eight.ss_mpr_elementory_sanctioned) as ss_mpr_elementory_sanctioned,sum(eight.ss_mpr_elementory_boys) as ss_mpr_elementory_boys,sum(eight.ss_mpr_elementory_girls) as ss_mpr_elementory_girls,sum(eight.ss_mpr_elementory_vacant) as ss_mpr_elementory_vacant,sum(eight.ss_mpr_secondary_sanctioned) as ss_mpr_secondary_sanctioned,sum(eight.ss_mpr_secondary_boys) as ss_mpr_secondary_boys,sum(eight.ss_mpr_secondary_girls) as ss_mpr_secondary_girls,sum(eight.ss_mpr_secondary_vacant) as ss_mpr_secondary_vacant From ss_mpr_eight_column_data as eight 
		  left join ss_partners as part on part.ss_partners_id=eight.ss_mpr_partner_id left join ss_district as dist on part.ss_partners_districts=dist.ss_district_id left join ss_states as state on state.ss_states_id=dist.ss_states_ss_states_id Where 1 $q AND eight.ss_mpr_metric_master_id='".$metric_master_id."'");	
		  $data = $query->row();
		  return $data;
	 }	 

	 // for the analysis column 
	public function getAnalysisVal_mpr($metric_master_id = null,$state_name=null,$month_from = null,$month_from_last = null,$partner_id = null,$district_id = null)
	{	
	$q="";
	  if($state_name and $state_name !='all_state')
	  {
		  $q.=" and state.ss_states_name='".$state_name."' ";
	  }
	  if($district_id and $district_id !='')
	  {
		  $q.=" and dist.ss_district_id='".$district_id."' ";
	  }	  
	   if($month_from)
	  {
		  $q.=" and analy.ss_mpr_analysis_data_month >= '".$month_from."' ";
	  }
	  if($month_from_last)
	  {
		  $q.=" and analy.ss_mpr_analysis_data_month <= '".$month_from_last."' ";
	  }
	  if($partner_id)
	  {
		  $q.=" and analy.ss_mpr_partner_id = '".$partner_id."' ";
	  }
		  $query = $this->CI->db->query("Select sum(analy.ss_mpr_analysis_data_value) as ss_mpr_analysis_data_value From ss_mpr_analysis_data as analy 
		  left join ss_partners as part on part.ss_partners_id=analy.ss_mpr_partner_id left join ss_district as dist on part.ss_partners_districts=dist.ss_district_id left join ss_states as state on state.ss_states_id=dist.ss_states_ss_states_id Where 1 $q AND ss_mpr_metric_master_id='".$metric_master_id."'");
		  //echo $this->CI->db->last_query();	
		  return $query->row('ss_mpr_analysis_data_value');
	  }
	
	
}