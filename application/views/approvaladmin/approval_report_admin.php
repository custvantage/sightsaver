<div class="row wrapper border-bottom white-bg page-heading ng-scope">
  <div class="col-lg-10">
    <h2>MPR Status</h2>
  </div>
</div>
<div class="wrapper wrapper-content animated fadeIn ng-scope" ng-init="init()">

  <div class="row">
    <div class="col-md-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <!--<h5>MPR Status</h5>-->
        </div>
        <div class="ibox-content">
			<?php echo form_open();?>
			<div class="row flex-element center-both">
				<div class="well well-sm main-entry-box">             
				  <div class="col-md-12">				
				   <input type="text" name="month_from" class="form-control datepicker" placeholder="Select Month" value="<?php $server_date = server_date_time(); echo date('m-Y',strtotime($server_date));?>" onchange="filter_data(this.value)">			   
				  </div>              
				</div>
			</div>
			<?php echo form_close();?>
          <div class="table-responsive">
          <table class="table" id="table_data_dashboard">
            <thead>
				<tr>
				<th>State</th>
				<th>District</th>
				<th>Partner</th>
				<th>Project</th>
				<th width="250">Status</th>
				<th width="200">Last Updated On</th>				
				</tr>
				</thead>
				           
            <tbody id="response">
			<?php if(!empty($summary_data)){ foreach($summary_data as $sdata){ ?>
            <tr ng-repeat="item in items track by item.id" class="ng-scope">
              <td class="ng-binding"><?php echo $sdata->ss_states_name;?></td>
              <td class="ng-binding"><?php echo $sdata->ss_district_name;?></td>
              <td class="ng-binding"><?php echo $sdata->ss_partners_name;?></td>
              <td class=""><?php if(strpos($sdata->ss_mpr_project_name, 'rural')) { ?>
			  <a href="#">REH</a>
			  <?php } elseif(strpos($sdata->ss_mpr_project_name, 'urban')){?>
			  <a href="#">UEH</a--><?php }?>
				</td>
              <td class="ng-binding">
			  <?php if($sdata->status == 1){ echo 'Inprogress';}
			  else if($sdata->status == 2){ echo 'Submitted';}
			  else if($sdata->status == 3){ echo 'Approved';}
			  else if($sdata->status == 4){ echo 'Rejected';}
			  else if($sdata->status == 5){ echo 'Closed';}
			  ?></td>
              <td class="ng-binding"><?php echo $sdata->last_modified;?></td>			  
            </tr>
			<?php } }else{?>
			<tr>
				<label id="no_data">No Data Found</label>
			</tr>
			<?php }?>
            </tbody>
          </table>
        </div>
        </div>
      </div>
    </div>

  </div>
</div>
<script>
function filter_data(month_data)
{
	if(month_data != "" )
	{
		var url = "<?php echo BASE_URL('approval_report_filter_admin');?>";				 
		var csrfHash = $("input[name=csrf_test_name]").val();
		 
		$.post(url,{<?php echo $this->security->get_csrf_token_name(); ?>: csrfHash,month_from:month_data} ,function(response){
				
			var htmldata = "";
			var status1 = "";
			var prj_name = "";
			$("input[name=csrf_test_name]").val(response.csrfHash);
			if(response.success ==1)
			{
				for(var i=0;i<response.summary_data.length;i++)
				{
					
					status1 = response.summary_data[i].status;
					if(status1 == 1)
					{status="Inprogress";}
					else if(status1 == 2)
					{status="Submitted";}
					else if(status1 == 3)
					{status = "Verified";}
					else if(status1 == 4)
					{status = "Rejected";}
					else if(status1 == 5)
					{status = "Approved";}
					
					prj_name = response.summary_data[i].ss_mpr_project_name;
					if(prj_name.indexOf("rural") >= 0)
					{ prjname = '<a href="<?php echo 'mpr_reh_redirect/';?>'+btoa(response.summary_data[i].ss_mpr_report_month)+'/'+btoa(response.summary_data[i].ss_mpr_summary_partner_id)+'">REH</a>' }
					else if(prj_name.indexOf("urban") >= 0)
					{ prjname = '<a href="<?php echo 'mpr_ueh_redirect/';?>'+btoa(response.summary_data[i].ss_mpr_report_month)+'/'+btoa(response.summary_data[i].ss_mpr_summary_partner_id)+'">UEH</a>'; }
				    else if(prj_name.indexOf("Social") >= 0)
					{ prjname = '<a href="<?php echo 'mpr_social_redirect/';?>'+btoa(response.summary_data[i].ss_mpr_report_month)+'/'+btoa(response.summary_data[i].ss_mpr_summary_partner_id)+'">SOC</a>'; }
				    else if(prj_name.indexOf("Inclusive") >= 0)
					{ prjname = '<a href="<?php echo 'mpr_education_redirect/';?>'+btoa(response.summary_data[i].ss_mpr_report_month)+'/'+btoa(response.summary_data[i].ss_mpr_summary_partner_id)+'">EDU</a>'; }
				 
				 htmldata += '<tr ng-repeat="item in items track by item.id" class="ng-scope"><td class="ng-binding">'+response.summary_data[i].ss_states_name+'</td><td class="ng-binding">'+response.summary_data[i].ss_district_name+'</td><td class="ng-binding">'+response.summary_data[i].ss_partners_name+'</td><td class="ng-binding">'+prjname+'</td><td class="ng-binding">'+status+'</td><td class="ng-binding">'+response.summary_data[i].last_modified+'</td></tr>';				 
				}							
				$("#response").html(htmldata);				
				
			}
			else{
				$("#response").html('<tr><label>No Data Found</label></tr>');
			}
		},"json");
	}
}
</script>