
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Sight Savers</title>
        <link href="<?php echo PUBLIC_URL; ?>css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo PUBLIC_URL; ?>font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo PUBLIC_URL; ?>css/animate.css" rel="stylesheet">
        <link href="<?php echo PUBLIC_URL; ?>css/style.css" rel="stylesheet">
        <link href="<?php echo PUBLIC_URL; ?>css/custom.css" rel="stylesheet">
        <link href="<?php echo PUBLIC_URL; ?>css/lc_switch.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo PUBLIC_URL; ?>css/codemirror.css">
        <link rel="stylesheet" href="<?php echo PUBLIC_URL; ?>css/custom.css">
		 <link href="<?php echo PUBLIC_URL; ?>css/bootstrap_datepicker.css" rel="stylesheet"> 
     	<link href="<?php echo PUBLIC_URL; ?>css/dataTables.bootstrap.min.css" rel="stylesheet">
    </head>
<body class="fixed-sidebar">

<div id="wrapper">

<nav class="navbar-admin navbar-static-side" role="navigation">
    <div class="sidebar-collapse">

      <ul side-navigation="" class="nav metismenu" id="side-menu">
      <li class="nav-header" style="padding: 20px;">
        <div class="profile-element">
          <img alt="image" width="100%" src="<?php echo PUBLIC_URL; ?>img/logo.png" src="<?php echo PUBLIC_URL; ?>assets/images/logo.png">
        </div>
        <div class="logo-element">
          SS
        </div>
      </li>
	 
 <?php 
  if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==6)  { ?> 
	   <li ng-class="{active: m.ie}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Data Entry</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" id="menhead123" ng-class="{in: m.ie}" aria-expanded="false">
	  
     </ul>
      </li>
		  <li class="active">
        	<a href="<?php echo BASE_URL.'partner_mpr';?>"><i class="fa fa-eye"></i> <span class="nav-label">Review MPR</span></a>
        
      	</li>
     <?php }  ?>
	 
	   <?php  if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==1 || $this->session->userdata('userinfo')['default_role']==5 || $this->session->userdata('userinfo')['default_role']==2 || $this->session->userdata('userinfo')['default_role']==3 || $this->session->userdata('userinfo')['default_role']==4)  { ?>
	  
	   <li ng-class="{active: m.dashboards}" >
		  <li class="active">
			<a href="<?php echo BASE_URL.'dashboard_speedometer';?>"><i class="fa fa-dashboard"></i> <span class="nav-label">Dashboard</span> </a>
			</li>
			<!--li><a href="<?php //echo BASE_URL.'dashboard';?>">Report</a></li-->
			  <li ng-class="{active: m.reports}">
				<a href=""><i class="fa fa-file-pdf-o"></i> <span class="nav-label">Graph</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse" ng-class="{in: m.reports}" aria-expanded="true">
				  <li  ><a href="#">Social Inclusion</a></li>
				  <li ><a  href="<?php echo BASE_URL.'dashboard_graph';?>">Rural Eye Health</a></li>
				  <li ><a  href="<?php echo BASE_URL.'dashboard_graph_ueh';?>">Urban Eye Health</a></li>
				  <li ><a  href="#">Inclusive Education</a></li>
				</ul>
			  </li>
		  </li>
			<li ng-class="{active: m.reports}" >
				<a href=""><i class="fa fa-file-pdf-o"></i> <span class="nav-label">MPR</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse" ng-class="{in: m.reports}" aria-expanded="true">
				  <li  ><a href="<?php echo BASE_URL.'mpr_si';?>">Social Inclusion</a></li>
				  <li ><a  href="<?php echo BASE_URL.'mpr_reh' ;?>">Rural Eye Health</a></li>
				  <li ><a  href="<?php echo BASE_URL.'mpr_ueh';?>">Urban Eye Health</a></li>
				  <li ><a  href="<?php echo BASE_URL.'mpr_ie';?>">Inclusive Education</a></li>
				</ul>
			</li>
			
			<li ng-class="{active: m.reports}" >
				<a href=""><i class="fa fa-file-pdf-o"></i> <span class="nav-label">Yearly Target</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse" ng-class="{in: m.reports}" aria-expanded="true">
     			 <li ><a  href="<?php echo BASE_URL.'reh_yearly_target/';echo base64_encode('rural eye health-yearly target');?>">REH Yearly Targets</a></li>
				   <li ><a  href="<?php echo BASE_URL.'ueh_yearly_target/';echo base64_encode('urban eye health-yearly target');?>">UEH Yearly Targets</a></li>
				</ul>
			</li>
			
		<?php if($this->session->userdata('userinfo')['default_role']==2 || $this->session->userdata('userinfo')['default_role']==3 || $this->session->userdata('userinfo')['default_role']==4) { ?>
		<li ng-class="{active: m.reports}" >
				<a href=""><i class="fa fa-file-pdf-o"></i> <span class="nav-label">Data approval</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse" ng-class="{in: m.reports}" aria-expanded="true">
				  <li  ><a href="<?php echo BASE_URL.'Approval';?>">Review MPR</a></li>				  
				</ul>
			</li>
		<?php }  ?>
		<?php  if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==1) { ?>
		<li ng-class="{active: m.reports}" >
				<a href=""><i class="fa fa-file-pdf-o"></i> <span class="nav-label">Data approval</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse" ng-class="{in: m.reports}" aria-expanded="true">
				  <li  ><a href="<?php echo BASE_URL.'Approvaladmin';?>">Review MPR</a></li>				  
				</ul>
			</li>
		<?php } ?>
		
		
		
	  
	  <li ng-class="{active: m.ie}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Data Entry</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.ie}" aria-expanded="false">
        <li ng-class="{active: m.ie}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Inclusive Education</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.ie}" aria-expanded="false">
          <li ><a href="<?php echo BASE_URL.'ie_main_entry/';echo base64_encode('Inclusive education-main entry'); ?>">IE Main Entry</a></li>
        </ul>
      </li>

      <li ng-class="{active: m.reh}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Rural Eye Health</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.reh}" aria-expanded="false">
          <li ><a  href="<?php echo BASE_URL.'reh_main_entry/';echo base64_encode('rural eye health-main entry');?>">REH Main Entry</a></li>
          <!--<li ><a  href="<?php //echo BASE_URL.'reh_refferal';?>">Referrals</a></li>-->
		  <li><a href="<?php echo BASE_URL.'reh_base_hospital';?>">REH Base Hospital</a></li>
          <li ><a  href="<?php echo BASE_URL.'reh_training';?>">REH Training | Capacity Building</a></li>
          <li ><a  href="<?php echo BASE_URL.'reh_advocacy';?>">REH Advocacy | Networking | Liasion</a></li>
          <li ><a  href="<?php echo BASE_URL.'reh_bcc';?>">REH BCC Entry</a></li>
          <li ><a  href="<?php echo BASE_URL.'reh_iec';?>">REH IEC Entry</a></li>
          <li ><a  href="<?php echo BASE_URL.'reh_vhsnc';?>">VHSNC Entry</a></li>
         <!-- <li ><a  href="javascript:void(0)">Errors in submission</a></li>  -->
        </ul>
      </li>
	  
      <li ng-class="{active: m.si}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Social Inclusion</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.si}" aria-expanded="false">
          <li ><a  href="<?php echo BASE_URL.'si_pwd_search';?>">SI PWD Search</a></li>
          <li ><a  href="<?php echo BASE_URL.'si_main_entry/';echo base64_encode('Social inclusion-main entry');?>">SI Main Entry</a></li>
          <li ><a  href="<?php echo BASE_URL.'shg';?>">SI SHG | PPG</a></li>
          <li ><a  href="<?php echo BASE_URL.'bpo';?>">SI BPO | DPO</a></li>
          <li ><a  href="<?php echo BASE_URL.'training';?>">SI Training Activity</a></li>
          <li ><a  href="<?php echo BASE_URL.'access_aidit';?>">SI Access Audit</a></li>
          <li ><a  href="<?php echo BASE_URL.'agencies';?>">SI Agencies Support</a></li>
          <li ><a  href="<?php echo BASE_URL.'advocacy';?>">SI Advocacy Activity</a></li>
          <li ><a  href="<?php echo BASE_URL.'iec';?>">SI IEC Activity</a></li>
          <li ><a  href="<?php echo BASE_URL.'bcc';?>">SI BCC Activity</a></li>
          <li ><a  href="<?php echo BASE_URL.'si_yearly_targets';?>">SI Yearly Targets</a></li>
         <!-- <li ><a  href="javascript:void(0)">Errors in PWD</a></li>  -->
        </ul>
      </li>
	  
	 
      <li ng-class="{active: m.ueh}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Urban Eye Health</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.ueh}" aria-expanded="false">
          <li ><a  href="<?php echo BASE_URL.'ueh_main_entry/';echo base64_encode('urban eye health-main entry');?>">Main Entry</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_vision_center';?>">Vision Center</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_base_hospital';?>">Base Hospital</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_outreach_camp';?>">Outreach Camp</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_training_entry';?>">Training | Capacity Building</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_advocacy_entry';?>">Advocacy | Networking | Liasion</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_bcc_entry';?>">BCC Entry</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_iec_entry';?>">IEC</a></li>
        </ul>
      </li>
        </ul>
      </li>

      <li ng-class="{active: m.admin}">
        <a href=""><i class="fa fa-gears"></i> <span class="nav-label">Administration</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.admin}" aria-expanded="false">
		  <li ><a  href="<?php echo BASE_URL.'Administration/';echo 'districts';?>">Districts</a></li>
          <li ><a  href="<?php echo BASE_URL.'Administration/';echo 'partner';?>">Partners</a></li>          
          <li ><a  href="<?php echo BASE_URL.'Administration/';echo 'user';?>">Users</a></li>
          <li ><a  href="<?php echo BASE_URL.'Administration/';echo 'reporting_quarters';?>">Reporting Quarters</a></li>
        </ul>
      </li>
	  <?php } if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==6) {} ?>
    </ul>
    </div>
</nav>
<div id="page-wrapper" class="gray-bg">
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary SeeMore2 " href="#"> <i class="fa fa-caret-left" aria-hidden="true"></i>	
           </a>

        </div>
		
        <ul class="nav navbar-top-links navbar-right">
	      <?php   if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==6) {  ?>
		   <li>
			<select class="form-control" id="selectId" onchange="part_fill(this.value);program_district(this.value);">
				<option value="">Select partner</option>
				<?php
                  if(!empty($dis)){
                    foreach($dis as $dist){ ?>
					<option value="<?php echo $dist->ss_partners_id ?>"<?php if($dist->ss_partners_id==$_SESSION["partner_id"])echo 'selected'; else echo ''; ?>><?php echo $dist->ss_partners_name; ?></option>
				  <?php } } ?>
				</select>
	      </li>
			
			<?php  }  ?>
          <li>
            <div class="text-muted welcome-message">
              <a ui-sref="main.profile" class="ng-binding"><span class="user-img"><img src="<?php echo PUBLIC_URL;?>img/default-user-icon-profile.png" alt=""></span> Hi <?php   echo $this->session->userdata('userinfo')['fname'] ?></a>
            </div>
          </li>
          <li>
            <a href="<?php echo BASE_URL.'logout';?>">
              <i class="fa fa-sign-out"></i> Log out
            </a>
          </li>
        </ul>
    </nav>
</div>

<div class="row wrapper border-bottom white-bg page-heading ng-scope">
  <div class="col-lg-10">
    <h2><?php echo @strtoupper($this->session->userdata['userinfo']['partner_name']); ?></h2>
  </div>
</div>

<div class="wrapper wrapper-content animated fadeIn ng-scope" ng-init="init()">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox">
        <div class="ibox-title">
          <h5>Urban Eye Health/ Base Hospital</h5>
        </div>
        <div class="ibox-content">
          <?php 
		  $attributes = array('id' => 'myform');?>
		  <form method="GET" action="<?php echo BASE_URL.'filter_bh_ueh'?>">
          <div class="row flex-element center-both">
            <div class="well well-sm main-entry-box entry-box95">
              
              <div class="col-md-4">
			 <?php
				if(!isset($_SESSION["month_date"])) {
				  $_SESSION["month_date"] = date("m-Y", strtotime("-1 months"));
				 }
				  ?>
               <input type="text" name="month_from" value="<?php echo $_SESSION["month_date"]; ?>" id="month_from" class="form-control datepicker" onchange="date_fill()">
			   <span class="text-danger ng-binding ng-hide" id="error_month" aria-hidden="true"><?php echo form_error('month_data');?>
				<?php echo form_error('month_from');?>
			   </span>
              </div>
              <div class="col-md-2">
                <button class="btn btn-sm btn-primary btn-block" type="submit">
                Get Data
              </button>
              </div>
			  </form>
			   <?php
				// $form_attrib = array('id'=>'excelupload_ueh_base');
				// echo form_open_multipart('excelupload_ueh_base',$form_attrib);  ?>
						<!-- <div class="col-md-4">
							<input type="file" name="upload_publish_file" class="form-control">
							<p class="ng-binding ng-hide" aria-hidden="true">Upload.xls and .xlsx file</p>
							<span class="text-danger ng-binding ng-hide" aria-hidden="true" id="error_file"></span>
						</div> 
						<div class="col-md-2">
							<button class="btn btn-sm btn-primary btn-block" onclick="submit_form_file();" type="button">
								Upload Data
							</button>
						</div> -->
				</div>
			</div>
			<input type="hidden" name="month_data"/> 
				<?php // echo form_close();  ?>       
          <div class="row m-t-lg" ng-show="hasData" aria-hidden="false" style="">
            <div class="col-md-12">
			<?php
            $attributes1 = array('id' => 'myform1');
			echo form_open('create_bh_ueh',$attributes1);?>
              <div class="panel panel-primary">
                <div class="panel-heading">
                  Base Hospital
                </div>
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
					   <input type="hidden"  name="id_ajax" id="id_ajax1">
                        <label>Name of the Supporting Agency</label>
                        <input type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="agency_name" ng-model="form.supporting_agency" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.supporting_agency" aria-hidden="true"></p>
                    </div>
                  </div>


                  <div class="tab-theme">
                    <div class="tabs-container">
                      <div class="ng-isolate-scope">

                        <ul class="nav nav-tabs" >
                        <li class="uib-tab nav-item ng-scope ng-isolate-scope active" >
                          <a href="#tab1" data-toggle="tab" class="nav-link ng-binding">Examination</a>
                        </li>
                        <li class="uib-tab nav-item ng-scope ng-isolate-scope" >
                          <a href="#tab2" data-toggle="tab" class="nav-link ng-binding">Referral</a>
                        </li>

                        <li class="uib-tab nav-item ng-scope ng-isolate-scope">
                          <a href="#tab3" data-toggle="tab" class="nav-link ng-binding">Refraction</a>
                        </li>

                        <li class="uib-tab nav-item ng-scope ng-isolate-scope">
                          <a href="#tab4" data-toggle="tab" class="nav-link ng-binding">Treatment</a>
                        </li>
                      </ul>

  <div class="tab-content">
    <!-- ngRepeat: tab in tabset.tabs -->
    <div class="tab-pane ng-scope active" id="tab1">

                          <div class="panel-body ng-scope">
                            <div ueh-base-hospital="" title="Total OPD" form="form.opd" errors="errors.opd" class="ng-isolate-scope"><div class="panel panel-default panel-tab-content">
  <div class="panel-heading ng-binding">
    Total OPD
  </div>
  <div class="panel-body" style="padding: 5px 20px 0px 20px;">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Adult</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Male</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="exam_opd_male" ng-model="form.male" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.male" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Female</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="exam_opd_female" ng-model="form.female" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.female" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Child</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Boy</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="exam_opd_boy" ng-model="form.boy" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.boy" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Girl</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="exam_opd_girl" ng-model="form.girl" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.girl" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
</div>
                          </div>

                        </div><!-- end ngRepeat: tab in tabset.tabs -->

                        <div class="tab-pane ng-scope" id="tab2">

                          <div class="panel-body ng-scope">
                            <div ueh-base-hospital="" title="Total number of persons referred from VC, reported at base hospital" form="form.base_from_vc" errors="errors.base_from_vc" class="ng-isolate-scope"><div class="panel panel-default panel-tab-content">
  <div class="panel-heading ng-binding">
    Total number of persons referred from VC, reported at base hospital
  </div>
  <div class="panel-body" style="padding: 5px 20px 0px 20px;">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Adult</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Male</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refer_vc_male" ng-model="form.male" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.male" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Female</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refer_vc_female" ng-model="form.female" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.female" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Child</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Boy</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refer_vc_boy" ng-model="form.boy" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.boy" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Girl</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refer_vc_girl" ng-model="form.girl" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.girl" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
</div>

                            <div ueh-base-hospital="" title="Total number of persons referred from Camp, reported at base hospital" form="form.base_from_camp" errors="errors.base_from_camp" class="ng-isolate-scope"><div class="panel panel-default panel-tab-content">
  <div class="panel-heading ng-binding">
    Total number of persons referred from Camp, reported at base hospital
  </div>
  <div class="panel-body" style="padding: 5px 20px 0px 20px;">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Adult</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Male</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refer_camp_male" ng-model="form.male" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.male" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Female</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refer_camp_female" ng-model="form.female" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.female" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Child</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Boy</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refer_camp_boy" ng-model="form.boy" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.boy" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Girl</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refer_camp_girl" ng-model="form.girl" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.girl" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
</div>

                            <div ueh-base-hospital="" title="Total number of persons Walk-in to the base hospital" form="form.base_walkin" errors="errors.base_walkin" class="ng-isolate-scope"><div class="panel panel-default panel-tab-content">
  <div class="panel-heading ng-binding">
    Total number of persons Walk-in to the base hospital
  </div>
  <div class="panel-body" style="padding: 5px 20px 0px 20px;">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Adult</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Male</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refer_walk_male" ng-model="form.male" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.male" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Female</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refer_walk_female" ng-model="form.female" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.female" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Child</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Boy</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refer_walk_boy" ng-model="form.boy" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.boy" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Girl</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refer_walk_girl" ng-model="form.girl" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.girl" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
</div>
                          </div>
                        </div><!-- end ngRepeat: tab in tabset.tabs -->
                        <div class="tab-pane ng-scope" id="tab3">

                          <div class="panel-body ng-scope">
                            <div ueh-base-hospital="" title="Total number of persons refracted" form="form.refracted" errors="errors.refracted" class="ng-isolate-scope"><div class="panel panel-default panel-tab-content">
  <div class="panel-heading ng-binding">
    Total number of persons refracted
  </div>
  <div class="panel-body" style="padding: 5px 20px 0px 20px;">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Adult</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Male</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refra_refra_male" ng-model="form.male" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.male" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Female</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refra_refra_female" ng-model="form.female" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.female" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Child</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Boy</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refra_refra_boy" ng-model="form.boy" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.boy" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Girl</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refra_refra_girl" ng-model="form.girl" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.girl" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
</div>

                            <div ueh-base-hospital="" title="Total number of persons prescribed spectacles" form="form.prescribed_spectacles" errors="errors.prescribed_spectacles" class="ng-isolate-scope"><div class="panel panel-default panel-tab-content">
  <div class="panel-heading ng-binding">
    Total number of persons prescribed spectacles
  </div>
  <div class="panel-body" style="padding: 5px 20px 0px 20px;">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Adult</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Male</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refra_pres_male" ng-model="form.male" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.male" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Female</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refra_pres_female" ng-model="form.female" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.female" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Child</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Boy</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refra_pres_boy" ng-model="form.boy" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.boy" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Girl</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refra_pres_girl" ng-model="form.girl" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.girl" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
</div>

                            <div ueh-base-hospital="" title="Total number of persons dispensed spectacles" form="form.dispensed_spectacles" errors="errors.dispensed_spectacles" class="ng-isolate-scope"><div class="panel panel-default panel-tab-content">
  <div class="panel-heading ng-binding">
    Total number of persons dispensed spectacles for free
  </div>
  <div class="panel-body" style="padding: 5px 20px 0px 20px;">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Adult</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Male</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refra_disp_male" ng-model="form.male" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.male" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Female</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refra_disp_female" ng-model="form.female" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.female" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Child</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Boy</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refra_disp_boy" ng-model="form.boy" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.boy" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Girl</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refra_disp_girl" ng-model="form.girl" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.girl" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
</div>

<div ueh-base-hospital="" title="Total number of persons dispensed spectacles" form="form.dispensed_spectacles" errors="errors.dispensed_spectacles" class="ng-isolate-scope"><div class="panel panel-default panel-tab-content">
  <div class="panel-heading ng-binding">
    Total number of persons purchased spectacles
  </div>
  <div class="panel-body" style="padding: 5px 20px 0px 20px;">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Adult</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Male</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refra_purc_male" ng-model="form.male" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.male" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Female</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refra_purc_female" ng-model="form.female" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.female" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Child</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Boy</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refra_purc_boy" ng-model="form.boy" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.boy" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Girl</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refra_purc_girl" ng-model="form.girl" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.girl" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
</div>



                          </div>
                        </div><!-- end ngRepeat: tab in tabset.tabs --><div class="tab-pane ng-scope" id="tab4">

                          <div class="panel-body ng-scope">
                            <div ueh-base-hospital="" title="Total number of persons treated without surgery" form="form.treated_without_surgery" errors="errors.treated_without_surgery" class="ng-isolate-scope"><div class="panel panel-default panel-tab-content">
  <div class="panel-heading ng-binding">
    Total number of persons treated without surgery
  </div>
  <div class="panel-body" style="padding: 5px 20px 0px 20px;">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Adult</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Male</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="treat_withoutsurg_male" ng-model="form.male" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.male" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Female</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="treat_withoutsurg_female" ng-model="form.female" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.female" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Child</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Boy</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="treat_withoutsurg_boy" ng-model="form.boy" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.boy" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Girl</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="treat_withoutsurg_girl" ng-model="form.girl" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.girl" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
</div>

                            
                            
                            <div ueh-base-hospital="" title="Surgeries Glaucoma" form="form.surgery_glaucoma" errors="errors.surgery_glaucoma" class="ng-isolate-scope"><div class="panel panel-default panel-tab-content">
  <div class="panel-heading ng-binding">
    Cataract free			
  </div>
  <div class="panel-body" style="padding: 5px 20px 0px 20px;">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Adult</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Male</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="treat_subdfree_male" ng-model="form.male" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.male" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Female</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="treat_subdfree_female" ng-model="form.female" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.female" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Child</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Boy</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="treat_subdfree_boy" ng-model="form.boy" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.boy" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Girl</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="treat_subdfree_girl" ng-model="form.girl" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.girl" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
</div>
                            <div ueh-base-hospital="" title="Surgeries DR" form="form.surgery_dr" errors="errors.surgery_dr" class="ng-isolate-scope"><div class="panel panel-default panel-tab-content">
  <div class="panel-heading ng-binding">
    Cataract Subsidised			
  </div>
  <div class="panel-body" style="padding: 5px 20px 0px 20px;">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Adult</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Male</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="treat_subd_male" ng-model="form.male" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.male" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Female</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="treat_subd_female" ng-model="form.female" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.female" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Child</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Boy</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="treat_subd_boy" ng-model="form.boy" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.boy" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Girl</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="treat_subd_girl" ng-model="form.girl" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.girl" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
</div>
<div ueh-base-hospital="" title="Of the total referred, Total number of persons referred for Cataract" form="form.referred_cataract" errors="errors.referred_cataract" class="ng-isolate-scope"><div class="panel panel-default panel-tab-content">
  <div class="panel-heading ng-binding">
    Cataract fully Paid			

  </div>
  <div class="panel-body" style="padding: 5px 20px 0px 20px;">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Adult</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Male</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="treat_catara_male" ng-model="form.male" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.male" aria-hidden="true" style=""></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Female</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="treat_catara_female" ng-model="form.female" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.female" aria-hidden="true" style=""></p>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Child</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Boy</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="treat_catara_boy" ng-model="form.boy" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.boy" aria-hidden="true" style=""></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Girl</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="treat_catara_girl" ng-model="form.girl" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.girl" aria-hidden="true" style=""></p>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
</div>
<div ueh-base-hospital="" title="Of the total referred, Total number of persons referred for Cataract" form="form.referred_cataract" errors="errors.referred_cataract" class="ng-isolate-scope"><div class="panel panel-default panel-tab-content">
  <div class="panel-heading ng-binding">
    Surgeries Glaucoma			
	  </div>
  <div class="panel-body" style="padding: 5px 20px 0px 20px;">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Adult</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Male</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="treat_glau_male" ng-model="form.male" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.male" aria-hidden="true" style=""></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Female</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="treat_glau_female" ng-model="form.female" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.female" aria-hidden="true" style=""></p>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Child</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Boy</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="treat_glau_boy" ng-model="form.boy" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.boy" aria-hidden="true" style=""></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Girl</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="treat_glau_girl" ng-model="form.girl" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.girl" aria-hidden="true" style=""></p>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
</div>
<div ueh-base-hospital="" title="Of the total referred, Total number of persons referred for Cataract" form="form.referred_cataract" errors="errors.referred_cataract" class="ng-isolate-scope"><div class="panel panel-default panel-tab-content">
  <div class="panel-heading ng-binding">
   Surgeries DR			

  </div>
  <div class="panel-body" style="padding: 5px 20px 0px 20px;">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Adult</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Male</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="treat_dr_male" ng-model="form.male" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.male" aria-hidden="true" style=""></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Female</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="treat_dr_female" ng-model="form.female" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.female" aria-hidden="true" style=""></p>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Child</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Boy</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="treat_dr_boy" ng-model="form.boy" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.boy" aria-hidden="true" style=""></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Girl</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="treat_dr_girl" ng-model="form.girl" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.girl" aria-hidden="true" style=""></p>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
</div>
</div>
</div><!-- end ngRepeat: tab in tabset.tabs -->
  </div>
</div>
                    </div>
                  </div>
                </div>
                <div class="panel-footer">
                  <div class="flex-element  align-bw">
                    <div class="footer-text">
                      <h5>Please review your changes before you save them. Changes once saved cannot be
                reverted.</h5>
                    </div>
					<input type="hidden" name="month_data"/>
                    <div class="save-btn">
                      <button class="btn  btn-primary" type="submit">
                          Save Changes
                      </button>
                    </div>
                  </div>
                </div>
              </div>
			  <?php echo form_close();?>
			  <?php if(!empty($bh_data)){ ?>
              <div class="table-responsive report-table">
                <table class="table table-bordered">
                  <thead>
                    <tr class="tableizer-firstrow">
                      <th colspan="80">Base Hospital Details</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr class="lead-bg">
                      <td>Name of the Supporting Agency</td>
                      <td rowspan="2" colspan="5">Total OPD</td>
                      <td rowspan="2" colspan="5">Total referred from VC, reported at base hospital</td>
                      <td rowspan="2" colspan="5">Total referred from Camp, reported at base hospital</td>
                      <td rowspan="2" colspan="5">Total Walk-in to the base hospital</td>
                      <td rowspan="2" colspan="5">Total refracted</td>
                      <td rowspan="2" colspan="5">Total prescribed spectacles</td>
					  <td rowspan="2" colspan="5">Total dispensed spectacles</td>
					  <td rowspan="2" colspan="5">Total number of persons purchased spectacles</td>		
			          <td rowspan="2" colspan="5">Total treated without surgery</td>
					  <td colspan="25">Total number of persons treated with
					  <td>Action1</td>
			<td>Action2</td>
					  </td>
                    </tr>
				    <tr class="lead-bg">
				   <td> </td>
				   
					
					<td colspan="5">Cataract free			
</td>
					<td colspan="5">Cataract Subsidised			
</td>
					<td colspan="5">Cataract fully Paid			
</td>
					<td colspan="5">Surgeries Glaucoma			
</td>
					<td colspan="5">Surgeries DR			
</td>
<td>&nbsp;</td>
			<td>&nbsp;</td>
				   </tr> 
                    <tr class="lead-bg">
                      <td>&nbsp;</td>
                      <td colspan="3">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="3">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="3">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="3">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="3">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="3">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="3">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="3">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="3">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="3">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="3">Adult</td>
                      <td colspan="2">Child</td>
					  <td colspan="3">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="3">Adult</td>
                      <td colspan="2">Child</td>
					  <td colspan="3">Adult</td>
                      <td colspan="2">Child</td>
                      <td>&nbsp;</td>
			          <td>&nbsp;</td>
                    </tr>
                    <tr class="lead-bg">
                      <td>&nbsp;</td>
                      <td width="73">Male</td>
                      <td width="44">Female</td>
					  <td width="44">TransGender</td>
                      <td width="32">Boys</td>
                      <td width="28">Girls</td>
                      <td width="56">Male</td>
                      <td width="44">Female</td>
					  <td width="44">TransGender</td>
                      <td width="32">Boys</td>
                      <td width="28">Girls</td>
                      <td width="56">Male</td>
                      <td width="44">Female</td>
					  <td width="44">TransGender</td>
                      <td width="32">Boys</td>
                      <td width="28">Girls</td>
                      <td width="49">Male</td>
                      <td width="44">Female</td>
					  <td width="44">TransGender</td>
                      <td width="32">Boys</td>
                      <td width="28">Girls</td>
                      <td width="62">Male</td>
                      <td width="44">Female</td>
					  <td width="44">TransGender</td>
                      <td width="32">Boys</td>
                      <td width="28">Girls</td>
                      <td width="68">Male</td>
                      <td width="44">Female</td>
					  <td width="44">TransGender</td>
                      <td width="32">Boys</td>
                      <td width="28">Girls</td>
                      <td width="64">Male</td>
                      <td width="44">Female</td>
					  <td width="44">TransGender</td>
                      <td width="32">Boys</td>
                      <td width="28">Girls</td>
                      <td width="61">Male</td>
                      <td width="44">Female</td>
					  <td width="44">TransGender</td>
                      <td width="32">Boys</td>
                      <td width="28">Girls</td>
                      <td width="60">Male</td>
                      <td width="44">Female</td>
					  <td width="44">TransGender</td>
                      <td width="32">Boys</td>
                      <td width="28">Girls</td>
                      <td width="61">Male</td>
                      <td width="44">Female</td>
					  <td width="44">TransGender</td>
                      <td width="32">Boys</td>
                      <td width="28">Girls</td>
                      <td width="60">Male</td>
                      <td width="44">Female</td>
					  <td width="44">TransGender</td>
                      <td width="32">Boys</td>
                      <td width="28">Girls</td>
					  <td width="60">Male</td>
                      <td width="44">Female</td>
					  <td width="44">TransGender</td>
                      <td width="32">Boys</td>
                      <td width="28">Girls</td>
					  <td width="60">Male</td>
                      <td width="44">Female</td>
					  <td width="44">TransGender</td>
                      <td width="32">Boys</td>
                      <td width="28">Girls</td>
					  <td width="60">Male</td>
                      <td width="44">Female</td>
					  <td width="44">TransGender</td>
                      <td width="32">Boys</td>
                      <td width="28">Girls</td>
					  <td>&nbsp;</td>
		            	<td>&nbsp;</td>
                    </tr>
                 <?php 
					foreach($bh_data as $bdata){?>
					<tr>
						<td><?php echo $bdata->ss_agency_name;?></td>
						
						<td><?php echo $bdata->ss_exam_opd_male;?></td>						
						<td><?php echo $bdata->ss_exam_opd_female;?></td>
						<td><?php echo "0";?></td>
						<td><?php echo $bdata->ss_exam_opd_boy;?></td>
						<td><?php echo $bdata->ss_exam_opd_girl;?></td>
						
						<td><?php echo $bdata->ss_refer_vc_male;?></td>
						<td><?php echo $bdata->ss_refer_vc_female;?></td>
						<td><?php echo "0";?></td>
						<td><?php echo $bdata->ss_refer_vc_boy;?></td>
						<td><?php echo $bdata->ss_refer_vc_girl;?></td>
						
						<td><?php echo $bdata->ss_refer_camp_male;?></td>
						<td><?php echo $bdata->ss_refer_camp_female;?></td>
						<td><?php echo "0";?></td>
						<td><?php echo $bdata->ss_refer_camp_boy;?></td>
						<td><?php echo $bdata->ss_refer_camp_girl;?></td>
						
						<td><?php echo $bdata->ss_refer_walk_male;?></td>
						<td><?php echo $bdata->ss_refer_walk_female;?></td>
						<td><?php echo "0";?></td>
						<td><?php echo $bdata->ss_refer_walk_boy;?></td>
						<td><?php echo $bdata->ss_refer_walk_girl;?></td>
						
						<td><?php echo $bdata->ss_refra_refra_male;?></td>
						<td><?php echo $bdata->ss_refra_refra_female;?></td>
						<td><?php echo "0";?></td>
						<td><?php echo $bdata->ss_refra_refra_boy;?></td>
						<td><?php echo $bdata->ss_refra_refra_girl;?></td>
						
						<td><?php echo $bdata->ss_refra_pres_male;?></td>
						<td><?php echo $bdata->ss_refra_pres_female;?></td>
						<td><?php echo "0";?></td>
						<td><?php echo $bdata->ss_refra_pres_boy;?></td>
						<td><?php echo $bdata->ss_refra_pres_girl;?></td>
						
						<td><?php echo $bdata->ss_refra_disp_male;?></td>
						<td><?php echo $bdata->ss_refra_disp_female;?></td>
						<td><?php echo "0";?></td>
						<td><?php echo $bdata->ss_refra_disp_boy;?></td>
						<td><?php echo $bdata->ss_refra_disp_girl;?></td>
						
						<td><?php echo $bdata->ss_refra_purc_male;?></td>
						<td><?php echo $bdata->ss_refra_purc_female;?></td>
						<td><?php echo "0";?></td>
						<td><?php echo $bdata->ss_refra_purc_boy;?></td>
						<td><?php echo $bdata->ss_refra_purc_girl;?></td>
						
						<td><?php echo $bdata->ss_ptreat_sur_male;?></td>
						<td><?php echo $bdata->ss_ptreat_sur_female;?></td>
						<td><?php echo "0";?></td>
						<td><?php echo $bdata->ss_ptreat_sur_boy;?></td>
						<td><?php echo $bdata->ss_ptreat_sur_girl;?></td>
						
						<td><?php echo $bdata->ss_treat_subdfree_male;?></td>
						<td><?php echo $bdata->ss_treat_subdfree_female;?></td>
						<td><?php echo "0";?></td>
						<td><?php echo $bdata->ss_treat_subdfree_boy;?></td>
						<td><?php echo $bdata->ss_treat_subdfree_girl;?></td>
						
						<td><?php echo $bdata->ss_treat_subd_male;?></td>						
						<td><?php echo $bdata->ss_treat_subd_female;?></td>
						<td><?php echo "0";?></td>
						<td><?php echo $bdata->ss_treat_subd_boy;?></td>
						<td><?php echo $bdata->ss_treat_subd_girl;?></td>
						
                        <td><?php echo $bdata->ss_treat_catara_male;?></td>
                        <td><?php echo $bdata->ss_treat_catara_female;?></td>
						<td><?php echo "0";?></td>
						<td><?php echo $bdata->ss_treat_catara_boy;?></td>
						<td><?php echo $bdata->ss_treat_catara_girl;?></td>
						
						<td><?php echo $bdata->ss_treat_glau_male;?></td>
						<td><?php echo $bdata->ss_treat_glau_female;?></td>
						<td><?php echo "0";?></td>
						<td><?php echo $bdata->ss_treat_glau_boy;?></td>
						<td><?php echo $bdata->ss_treat_glau_girl;?></td>
						
						<td><?php echo $bdata->ss_treat_sur_male;?></td>
						<td><?php echo $bdata->ss_treat_sur_female;?></td>
						<td><?php echo "0";?></td>
						<td><?php echo $bdata->ss_treat_sur_boy;?></td>
						<td><?php echo $bdata->ss_treat_sur_girl;?></td>
						<td><a onclick="return confirmDelete();" href="<?php echo BASE_URL.'delete_base_ueh/'.base64_encode($bdata->ss_ueh_base_id); ?>">Delete</a></td>
            <td><a href="javascript:void(0)" class="editThis" data-val="<?php echo base64_encode($bdata->ss_ueh_base_id); ?>">Edit</a></td>
						
					</tr>
				  <?php }?>
                  </tbody>
                </table>
              </div>
			  <?php } ?>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
<script src="<?php echo PUBLIC_URL; ?>js/jquery-2.1.1.js"></script>
<script>

$(".editThis").click(function(){
	var dataid = $(this).attr('data-val');
	var cct = $("input[name='csrf_test_name']").val();
	//alert(cct);
	//var cct = $.cookie("<?php echo $this->config->item("csrf_cookie_name"); ?>");
	post_data ={ id: dataid,<?php echo $this->security->get_csrf_token_name(); ?> : cct};
	if(dataid!=0 || dataid !="")
	{
    $.ajax({ 
            url: "<?php echo BASE_URL.'ueh/edit_base_ueh';?>",
            data: post_data,
            type: 'POST'
        }).done(function(responseData) { //alert(responseData); return false;
		
		var csrf_t = responseData.split("$$$");
		var obj = JSON.parse(csrf_t[0]);
       
		$("input[name='csrf_test_name']").val(csrf_t[1]);
	    
		$("input[name='month_from']").val(obj['date_month']);
		$("input[name='agency_name']").val(obj['ss_agency_name']);
	    		
		$("input[name='exam_opd_male']").val(obj['ss_exam_opd_male']);
		$("input[name='exam_opd_female']").val(obj['ss_exam_opd_female']);
		$("input[name='exam_opd_boy']").val(obj['ss_exam_opd_boy']);
		$("input[name='exam_opd_girl']").val(obj['ss_exam_opd_girl']);
		
		$("input[name='refer_vc_male']").val(obj['ss_refer_vc_male']);
		$("input[name='refer_vc_female']").val(obj['ss_refer_vc_female']);
		$("input[name='refer_vc_boy']").val(obj['ss_refer_vc_boy']);
		$("input[name='refer_vc_girl']").val(obj['ss_refer_vc_girl']);
		
		$("input[name='refer_camp_male']").val(obj['ss_refer_camp_male']);
		$("input[name='refer_camp_female']").val(obj['ss_refer_camp_female']);
		$("input[name='refer_camp_boy']").val(obj['ss_refer_camp_boy']);
		$("input[name='refer_camp_girl']").val(obj['ss_refer_camp_girl']);
		
		$("input[name='refer_walk_male']").val(obj['ss_refer_walk_male']);
		$("input[name='refer_walk_female']").val(obj['ss_refer_walk_female']);
		$("input[name='refer_walk_boy']").val(obj['ss_refer_walk_boy']);
		$("input[name='refer_walk_girl']").val(obj['ss_refer_walk_girl']);
		
		$("input[name='refra_refra_male']").val(obj['ss_refra_refra_male']);
		$("input[name='refra_refra_female']").val(obj['ss_refra_refra_female']);
		$("input[name='refra_refra_boy']").val(obj['ss_refra_refra_boy']);
		$("input[name='refra_refra_girl']").val(obj['ss_refra_refra_girl']);

		$("input[name='refra_pres_male']").val(obj['ss_refra_pres_male']);
		$("input[name='refra_pres_female']").val(obj['ss_refra_pres_female']);
		$("input[name='refra_pres_boy']").val(obj['ss_refra_pres_boy']);
		$("input[name='refra_pres_girl']").val(obj['ss_refra_pres_girl']);
		
		$("input[name='refra_disp_male']").val(obj['ss_refra_disp_male']);
		$("input[name='refra_disp_female']").val(obj['ss_refra_disp_female']);
		$("input[name='refra_disp_boy']").val(obj['ss_refra_disp_boy']);
		$("input[name='refra_disp_girl']").val(obj['ss_refra_disp_girl']);
		
		$("input[name='refra_purc_male']").val(obj['ss_refra_purc_male']);
		$("input[name='refra_purc_female']").val(obj['ss_refra_purc_female']);
		$("input[name='refra_purc_boy']").val(obj['ss_refra_purc_boy']);
		$("input[name='refra_purc_girl']").val(obj['ss_refra_purc_girl']);
		
		$("input[name='treat_withoutsurg_male']").val(obj['ss_ptreat_sur_male']);
		$("input[name='treat_withoutsurg_female']").val(obj['ss_ptreat_sur_female']);
		$("input[name='treat_withoutsurg_boy']").val(obj['ss_ptreat_sur_boy']);
		$("input[name='treat_withoutsurg_girl']").val(obj['ss_ptreat_sur_girl']);
		
		$("input[name='treat_subdfree_male']").val(obj['ss_treat_subdfree_male']);
		$("input[name='treat_subdfree_female']").val(obj['ss_treat_subdfree_female']);
		$("input[name='treat_subdfree_boy']").val(obj['ss_treat_subdfree_boy']);
		$("input[name='treat_subdfree_girl']").val(obj['ss_treat_subdfree_girl']);
		
		$("input[name='treat_subd_male']").val(obj['ss_treat_subd_male']);
		$("input[name='treat_subd_female']").val(obj['ss_treat_subd_female']);
		$("input[name='treat_subd_boy']").val(obj['ss_treat_subd_boy']);
		$("input[name='treat_subd_girl']").val(obj['ss_treat_subd_girl']);
		
		$("input[name='treat_catara_male']").val(obj['ss_treat_catara_male']);
		$("input[name='treat_catara_female']").val(obj['ss_treat_catara_female']);
		$("input[name='treat_catara_boy']").val(obj['ss_treat_catara_boy']);
		$("input[name='treat_catara_girl']").val(obj['ss_treat_catara_girl']);
		
		$("input[name='treat_glau_male']").val(obj['ss_treat_glau_male']);
		$("input[name='treat_glau_female']").val(obj['ss_treat_glau_female']);
		$("input[name='treat_glau_boy']").val(obj['ss_treat_glau_boy']);
		$("input[name='treat_glau_girl']").val(obj['ss_treat_glau_girl']);
		
		$("input[name='treat_dr_male']").val(obj['ss_treat_sur_male']);
		$("input[name='treat_dr_female']").val(obj['ss_treat_sur_female']);
		$("input[name='treat_dr_boy']").val(obj['ss_treat_sur_boy']);
		$("input[name='treat_dr_girl']").val(obj['ss_treat_sur_girl']);
		
		$("input[name='id_ajax']").val(obj['ss_ueh_base_id']);
		
		
        console.log('Done: ', responseData);
        }).fail(function() {
            console.log('Failed');
        });
	}
})

</script>

<script type="text/javascript">
    function confirmDelete() 
	{
        return confirm('Do you really wants to delete?');
    }
</script>

<script>
function submit_form_file()
{
	if($('input[name="upload_publish_file"]').val() == "")
	{
		$("#error_file").html("Please select file");
	}
	else if($('input[name="month_data"]').val() == "")
	{
		$("#error_month").html("Please select month");
	}
	else{
		$("#excelupload_ueh_base").submit();
	}
}
</script>