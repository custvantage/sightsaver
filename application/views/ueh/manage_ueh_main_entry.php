
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Sight Savers</title>
        <link href="<?php echo PUBLIC_URL; ?>css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo PUBLIC_URL; ?>font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo PUBLIC_URL; ?>css/animate.css" rel="stylesheet">
        <link href="<?php echo PUBLIC_URL; ?>css/style.css" rel="stylesheet">
        <link href="<?php echo PUBLIC_URL; ?>css/custom.css" rel="stylesheet">
        <link href="<?php echo PUBLIC_URL; ?>css/lc_switch.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo PUBLIC_URL; ?>css/codemirror.css">
        <link rel="stylesheet" href="<?php echo PUBLIC_URL; ?>css/custom.css">
		 <link href="<?php echo PUBLIC_URL; ?>css/bootstrap_datepicker.css" rel="stylesheet"> 
     	<link href="<?php echo PUBLIC_URL; ?>css/dataTables.bootstrap.min.css" rel="stylesheet">
    </head>
<body class="fixed-sidebar">

<div id="wrapper">
<input type="hidden" id="hidden_files_out" value="<?php echo BASE_URL.'ueh_main_entry/';echo base64_encode('urban eye health-main entry');?>">
<nav class="navbar-admin navbar-static-side" role="navigation">
    <div class="sidebar-collapse">

      <ul side-navigation="" class="nav metismenu" id="side-menu">
      <li class="nav-header" style="padding: 20px;">
        <div class="profile-element">
          <img alt="image" width="100%" src="<?php echo PUBLIC_URL; ?>img/logo.png" src="<?php echo PUBLIC_URL; ?>assets/images/logo.png">
        </div>
        <div class="logo-element">
          SS
        </div>
      </li>
	 
 <?php 
  if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==6)  { ?> 
	   <li ng-class="{active: m.ie}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Data Entry</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" id="menhead123" ng-class="{in: m.ie}" aria-expanded="false">
	  
     </ul>
      </li>
	  <li class="active">
        	<a href="<?php echo BASE_URL.'partner_mpr';?>"><i class="fa fa-eye"></i> <span class="nav-label">Review MPR</span></a>
        
      	</li>
     <?php }  ?>
	 
	   <?php  if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==1 || $this->session->userdata('userinfo')['default_role']==5 || $this->session->userdata('userinfo')['default_role']==2 || $this->session->userdata('userinfo')['default_role']==3 || $this->session->userdata('userinfo')['default_role']==4)  { ?>
	  
	   <li ng-class="{active: m.dashboards}" >
		  <li class="active">
			<a href="<?php echo BASE_URL.'dashboard_speedometer';?>"><i class="fa fa-dashboard"></i> <span class="nav-label">Dashboard</span> </a>
			</li>
			<!--li><a href="<?php //echo BASE_URL.'dashboard';?>">Report</a></li-->
			  <li ng-class="{active: m.reports}">
				<a href=""><i class="fa fa-file-pdf-o"></i> <span class="nav-label">Graph</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse" ng-class="{in: m.reports}" aria-expanded="true">
				  <li  ><a href="#">Social Inclusion</a></li>
				  <li ><a  href="<?php echo BASE_URL.'dashboard_graph';?>">Rural Eye Health</a></li>
				  <li ><a  href="<?php echo BASE_URL.'dashboard_graph_ueh';?>">Urban Eye Health</a></li>
				  <li ><a  href="#">Inclusive Education</a></li>
				</ul>
			  </li>
		  </li>
			<li ng-class="{active: m.reports}" >
				<a href=""><i class="fa fa-file-pdf-o"></i> <span class="nav-label">MPR</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse" ng-class="{in: m.reports}" aria-expanded="true">
				  <li  ><a href="<?php echo BASE_URL.'mpr_si';?>">Social Inclusion</a></li>
				  <li ><a  href="<?php echo BASE_URL.'mpr_reh' ;?>">Rural Eye Health</a></li>
				  <li ><a  href="<?php echo BASE_URL.'mpr_ueh';?>">Urban Eye Health</a></li>
				  <li ><a  href="<?php echo BASE_URL.'mpr_ie';?>">Inclusive Education</a></li>
				</ul>
			</li>
			
			<li ng-class="{active: m.reports}" >
				<a href=""><i class="fa fa-file-pdf-o"></i> <span class="nav-label">Yearly Target</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse" ng-class="{in: m.reports}" aria-expanded="true">
     			 <li ><a  href="<?php echo BASE_URL.'reh_yearly_target/';echo base64_encode('rural eye health-yearly target');?>">REH Yearly Targets</a></li>
				   <li ><a  href="<?php echo BASE_URL.'ueh_yearly_target/';echo base64_encode('urban eye health-yearly target');?>">UEH Yearly Targets</a></li>
				</ul>
			</li>
			
		<?php if($this->session->userdata('userinfo')['default_role']==2 || $this->session->userdata('userinfo')['default_role']==3 || $this->session->userdata('userinfo')['default_role']==4) { ?>
		<li ng-class="{active: m.reports}" >
				<a href=""><i class="fa fa-file-pdf-o"></i> <span class="nav-label">Data approval</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse" ng-class="{in: m.reports}" aria-expanded="true">
				  <li  ><a href="<?php echo BASE_URL.'Approval';?>">Review MPR</a></li>				  
				</ul>
			</li>
		<?php }  ?>
		<?php  if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==1) { ?>
		<li ng-class="{active: m.reports}" >
				<a href=""><i class="fa fa-file-pdf-o"></i> <span class="nav-label">Data approval</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse" ng-class="{in: m.reports}" aria-expanded="true">
				  <li  ><a href="<?php echo BASE_URL.'Approvaladmin';?>">Review MPR</a></li>				  
				</ul>
			</li>
		<?php } ?>
		
		
		
	  
	  <li ng-class="{active: m.ie}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Data Entry</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.ie}" aria-expanded="false">
        <li ng-class="{active: m.ie}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Inclusive Education</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.ie}" aria-expanded="false">
          <li ><a href="<?php echo BASE_URL.'ie_main_entry/';echo base64_encode('Inclusive education-main entry'); ?>">IE Main Entry</a></li>
        </ul>
      </li>

      <li ng-class="{active: m.reh}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Rural Eye Health</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.reh}" aria-expanded="false">
          <li ><a  href="<?php echo BASE_URL.'reh_main_entry/';echo base64_encode('rural eye health-main entry');?>">REH Main Entry</a></li>
          <!--<li ><a  href="<?php //echo BASE_URL.'reh_refferal';?>">Referrals</a></li>-->
		  <li><a href="<?php echo BASE_URL.'reh_base_hospital';?>">REH Base Hospital</a></li>
          <li ><a  href="<?php echo BASE_URL.'reh_training';?>">REH Training | Capacity Building</a></li>
          <li ><a  href="<?php echo BASE_URL.'reh_advocacy';?>">REH Advocacy | Networking | Liasion</a></li>
          <li ><a  href="<?php echo BASE_URL.'reh_bcc';?>">REH BCC Entry</a></li>
          <li ><a  href="<?php echo BASE_URL.'reh_iec';?>">REH IEC Entry</a></li>
          <li ><a  href="<?php echo BASE_URL.'reh_vhsnc';?>">VHSNC Entry</a></li>
         <!-- <li ><a  href="javascript:void(0)">Errors in submission</a></li>  -->
        </ul>
      </li>
	  
      <li ng-class="{active: m.si}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Social Inclusion</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.si}" aria-expanded="false">
          <li ><a  href="<?php echo BASE_URL.'si_pwd_search';?>">SI PWD Search</a></li>
          <li ><a  href="<?php echo BASE_URL.'si_main_entry/';echo base64_encode('Social inclusion-main entry');?>">SI Main Entry</a></li>
          <li ><a  href="<?php echo BASE_URL.'shg';?>">SI SHG | PPG</a></li>
          <li ><a  href="<?php echo BASE_URL.'bpo';?>">SI BPO | DPO</a></li>
          <li ><a  href="<?php echo BASE_URL.'training';?>">SI Training Activity</a></li>
          <li ><a  href="<?php echo BASE_URL.'access_aidit';?>">SI Access Audit</a></li>
          <li ><a  href="<?php echo BASE_URL.'agencies';?>">SI Agencies Support</a></li>
          <li ><a  href="<?php echo BASE_URL.'advocacy';?>">SI Advocacy Activity</a></li>
          <li ><a  href="<?php echo BASE_URL.'iec';?>">SI IEC Activity</a></li>
          <li ><a  href="<?php echo BASE_URL.'bcc';?>">SI BCC Activity</a></li>
          <li ><a  href="<?php echo BASE_URL.'si_yearly_targets';?>">SI Yearly Targets</a></li>
         <!-- <li ><a  href="javascript:void(0)">Errors in PWD</a></li>  -->
        </ul>
      </li>
	  
	 
      <li ng-class="{active: m.ueh}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Urban Eye Health</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.ueh}" aria-expanded="false">
          <li ><a  href="<?php echo BASE_URL.'ueh_main_entry/';echo base64_encode('urban eye health-main entry');?>">Main Entry</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_vision_center';?>">Vision Center</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_base_hospital';?>">Base Hospital</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_outreach_camp';?>">Outreach Camp</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_training_entry';?>">Training | Capacity Building</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_advocacy_entry';?>">Advocacy | Networking | Liasion</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_bcc_entry';?>">BCC Entry</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_iec_entry';?>">IEC</a></li>
        </ul>
      </li>
        </ul>
      </li>

      <li ng-class="{active: m.admin}">
        <a href=""><i class="fa fa-gears"></i> <span class="nav-label">Administration</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.admin}" aria-expanded="false">
		  <li ><a  href="<?php echo BASE_URL.'Administration/';echo 'districts';?>">Districts</a></li>
          <li ><a  href="<?php echo BASE_URL.'Administration/';echo 'partner';?>">Partners</a></li>          
          <li ><a  href="<?php echo BASE_URL.'Administration/';echo 'user';?>">Users</a></li>
          <li ><a  href="<?php echo BASE_URL.'Administration/';echo 'reporting_quarters';?>">Reporting Quarters</a></li>
        </ul>
      </li>
	  <?php } if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==6) {} ?>
    </ul>
    </div>
</nav>
<div id="page-wrapper" class="gray-bg">
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary SeeMore2 " href="#"> <i class="fa fa-caret-left" aria-hidden="true"></i>	
           </a>

        </div>
		
        <ul class="nav navbar-top-links navbar-right">
	      <?php   if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==6) {  ?>
		   <li>
			<select class="form-control" id="selectId" onchange="part_fill(this.value);program_district(this.value);">
				<option value="">Select partner</option>
				<?php
                  if(!empty($dis)){
                    foreach($dis as $dist){ ?>
					<option value="<?php echo $dist->ss_partners_id ?>"<?php if($dist->ss_partners_id==$_SESSION["partner_id"])echo 'selected'; else echo ''; ?>><?php echo $dist->ss_partners_name; ?></option>
				  <?php } } ?>
				</select>
	      </li>
			
			<?php  }  ?>
          <li>
            <div class="text-muted welcome-message">
              <a ui-sref="main.profile" class="ng-binding"><span class="user-img"><img src="<?php echo PUBLIC_URL;?>img/default-user-icon-profile.png" alt=""></span> Hi <?php   echo $this->session->userdata('userinfo')['fname'] ?></a>
            </div>
          </li>
          <li>
            <a href="<?php echo BASE_URL.'logout';?>">
              <i class="fa fa-sign-out"></i> Log out
            </a>
          </li>
        </ul>
    </nav>
</div>
<div class="row wrapper border-bottom white-bg page-heading ng-scope">
    <div class="col-lg-10">
        <h2><?php echo @strtoupper($this->session->userdata['userinfo']['partner_name']); ?></h2>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeIn ng-scope" ng-init="init()">
<?php if(isset($_SESSION['success']))
{ ?>
<div class="alert alert-success">
  <strong></strong> <?php echo $_SESSION['success'] ; ?>
</div>
<?php } ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <div class="row">
                        <div class="col-lg-6">
                            <h5>Urban Eye Health / Main Entry</h5>
                        </div>
						<?php if($this->session->userdata('userinfo')['default_role']==6) { ?>
                        <div class="col-lg-6 status_display">
                            <h4 >Status: <span><?php if($status==1) {echo "In progress";}if($status==""){echo "New";} if($status==2){echo "Submitted";}if($status==4){echo "Rejected";} if($status==5){echo "Closed";} ?></span></h4>
                        </div>
						<?php } ?> 
                    </div>
                </div>
                <div class="ibox-content">
					<?php
					if($this->session->userdata('userinfo')['default_role'] == 6) //if partner
					{
						$disable = "";
						echo form_open('manage_ueh_main_entry/'.$this->uri->segment(2));?>
                    <div class="row flex-element center-both">					
                        <div class="well well-sm main-entry-box">
                            <div class="col-md-3 text-right">
                                <label>Month:</label><label></label>
                            </div>
                            <div class="col-md-5">
							<?php /*?><?php
				          if(!isset($_SESSION["month_date"])) {
				          $_SESSION["month_date"] = date("m-Y", strtotime("-1 months"));
						  
				          }
				          ?><?php */?>
                            <input type="text" name="month_from" id="month_from" value="<?php echo $data1; ?>" class="form-control datepicker" onchange="date_fill()">
                         
                            </div>
                            <div class="col-md-4">
                                <button class="btn btn-sm btn-primary btn-block" type="submit">
                                    Get Data
                                </button>
                            </div>
                        </div>
						
                    </div>
					<div class="clear"></div>
						
					<?php echo form_close();?>
					<?php }else
					{ //if pm/po
					$disable = "disabled";
					?>
					<?php $form_attrib = array('id'=>'manage_ueh_main_entry');
						echo form_open('manage_ueh_main_entry/'.$this->uri->segment(2),$form_attrib);?>
					<div class="row flex-element center-both">
						<div class="well well-sm main-entry-box">
						  <div class="col-md-6">
							<select class="form-control" name="partner_name">
							  <option value="">Select partner</option>
							  <?php if(!empty($partners)){ foreach($partners as $partner){?>
							  <option value="<?php echo $partner->ss_partners_id;?>"><?php echo $partner->ss_partners_name;?></option>
							  <?php } }?>
							</select>
						  </div>
						  <div class="col-md-4">
								<input type="text" name="month_from" id="month_from" value="<?php echo $data1; ?>" class="form-control datepicker" onchange="date_fill()">
								<p class="text-danger ng-binding ng-hide" aria-hidden="true" id="error_month">
								<?php echo form_error('month_data');?></p>
						  </div>
						  <div class="col-md-2">
							<button class="btn btn-sm btn-primary btn-block" onclick="submit_form('manage_ueh_main_entry');" type="button">
								Get Data
							</button>
						  </div>
						</div>
					</div>
					<?php echo form_close();?>
					<?php } ?>

                    <div class="row" ng-show="hasData" aria-hidden="false" style="">
                        <div class="col-md-12">
                            <div class="tabs-container">
                                <div class="tabs-left ng-isolate-scope">
                                    <ul class="nav nav-tabs">
                                        <?php $tab=0;
                                        if (!empty($sections)) {
											
											 
		                                     if(isset($_SESSION['tab_id_ueh'])) { $tab = $_SESSION['tab_id_ueh']; }
		
                                            foreach ($sections as $key_section => $section) { if(!strstr(strtolower(trim($section->ss_section_layout_section_name)), 'analysis')) { if($tab == 0 and $key_section == 0) { $tab = $section->ss_section_layout_ss_section_layout_id ; } 
                                                ?>
                                                <li class="uib-tab nav-item ng-scope ng-isolate-scope<?php if ($section->ss_section_layout_ss_section_layout_id == $tab) { ?> active <?php } ?>">
                                                    <a href="#tab<?php echo $section->ss_section_layout_ss_section_layout_id; ?>" data-toggle="tab" class="nav-link ng-binding"  onClick="functionTab(<?php echo $section->ss_section_layout_ss_section_layout_id; ?>)" <?php if ($section->ss_section_layout_ss_section_layout_id == $tab) { ?> aria-expanded = "true" <?php } ?>><?php echo ucwords($section->ss_section_layout_section_name); ?></a>
                                                </li>
                                            <?php
                                            }
											}
                                        }
                                        ?>
                                    </ul>
									<?php echo form_open('edit_form_data/'.$this->uri->segment(2));	?>
                                    <div class="tab-content">
                                        <?php $section_id = ""; $j=0; //$counter=0;?>                                       
                                        <?php
                                        if (!empty($metrics_data)) { 

                                            foreach ($metrics_data as $key_section_metric => $section_metric) { if(!strstr(strtolower(trim($section_metric->ss_section_layout_section_name)), 'analysis')) {
												
											
												
                                                if ($section_id != $section_metric->ss_section_layout_ss_section_layout_id) {
                                                    if ($j != 0) { //$counter=0;
                                                        ?>
                                                        </tbody>
                                                        </table>
                                                    </div>
                                                    <!--input type="submit" name="submit<?php //if(!empty($sub) && $sub == 1){ echo 'submit';}else{ echo 'update';}?>" value="Submit"/-->
													<?php //if($k=0){ foreach($inserted_id as $insert_id){ ?>
													<!--input type="hidden" name="update_id[]" value="<?php //echo $insert_id->ss_one_column_data_id;?>"/--> 
													<?php //} $k++; }?>
                                                    <?php echo form_close();?>
                                                </div>
                                            </div>
                                         <?php } ?>
										
                                        <div class="tab-pane ng-scope <?= $section_metric->ss_section_layout_ss_section_layout_id?> <?php if ($section_metric->ss_section_layout_ss_section_layout_id == $tab) { ?> active <?php } ?>"   id="tab<?php echo $section_metric->ss_section_layout_ss_section_layout_id; ?>">

                                            <div class="panel-body ng-scope">
                                                <div class="bg-heading p-xs b-r-sm ng-scope"><i class="fa fa-arrow-right"></i><?php echo ucwords($section_metric->ss_section_layout_section_name); ?> </div>                                                       
                                                <h4 class="ng-scope">
            <?php echo $section_metric->ss_section_layout_analysis_description; ?>
                                                </h4>                                                
                                                <div class="table-responsive ng-scope">
                                                    <table class="table table-bordered">
                                                        <thead>

                                                            <tr>
                                                                <?php if ($section_metric->ss_section_layout_section_col_count == 1 && $section_metric->ss_section_layout_analysis_description == "") { ?>
                                                                    <td>Metric</td>       
                                                                    <td>Value</td>
            <?php }
            elseif ($section_metric->ss_section_layout_section_col_count == 4 && $section_metric->ss_section_layout_analysis_description == "") { ?>
                                                                    <td>Metric</td>       
                                                                    <td>Men</td>
                                                                    <td>Women</td>
																	<td>Transgender</td>
                                                                    <td>Boys</td>
                                                                    <td>Girls</td>
            <?php }
            elseif ($section_metric->ss_section_layout_analysis_description != "") { ?>
                                                                    <td>Question</td>       
                                                                    <td>Response</td>
            <?php } ?>
                                                            </tr>

                                                        </thead>
                                                        <tbody>

                                                            <tr>
            <?php if ($section_metric->ss_section_layout_section_col_count == 1 && $section_metric->ss_section_layout_analysis_description == "") { ?>
                                                                    <td><?php echo $section_metric->ss_metric_master_description; ?>
																	<input type="hidden" name="matric_id_one_column[]" value="<?php echo $section_metric->ss_metric_master_id;?>"/>																	
                                                                    </td>
                                                                    <td><input type="text" name="one_column_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $one_col_val = $this->metricdata->getOneColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($one_col_val)){ echo $one_col_val;}?>" <?php echo $disable;?> aria-invalid="false"></td>
            <?php }
            elseif ($section_metric->ss_section_layout_section_col_count == 4 && $section_metric->ss_section_layout_analysis_description == "") { ?>
                                                                    <td><?php echo $section_metric->ss_metric_master_description; ?>
																	<input type="hidden" name="matric_id_four_column[]" value="<?php echo $section_metric->ss_metric_master_id;?>"/>																	
                                                                    </td>

                                                                    <td><input type="text" name="four_column_men_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $four_col_data = $this->metricdata->getFourColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($four_col_data->ss_four_column_data_value_men)){ echo $four_col_data->ss_four_column_data_value_men;}?>" <?php echo $disable;?> aria-invalid="false"></td>
                                                                    <td><input type="text" name="four_column_women_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $four_col_data = $this->metricdata->getFourColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($four_col_data->ss_four_column_data_value_women)){ echo $four_col_data->ss_four_column_data_value_women;}?>" <?php echo $disable;?> aria-invalid="false"></td>
																	<td><input type="text" name="four_column_trans_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $four_col_data = $this->metricdata->getFourColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($four_col_data->ss_four_column_data_value_trans)){ echo $four_col_data->ss_four_column_data_value_trans;}?>" <?php echo $disable;?> aria-invalid="false"></td>
                                                                    <td><input type="text" name="four_column_boy_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $four_col_data = $this->metricdata->getFourColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($four_col_data->ss_four_column_data_value_boys)){ echo $four_col_data->ss_four_column_data_value_boys;}?>" <?php echo $disable;?> aria-invalid="false"></td>
                                                                    <td><input type="text" name="four_column_girl_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $four_col_data = $this->metricdata->getFourColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($four_col_data->ss_four_column_data_value_girls)){
																	echo $four_col_data->ss_four_column_data_value_girls;}?>" <?php echo $disable;?> aria-invalid="false"></td>
                                                            <?php }
                                                            elseif ($section_metric->ss_section_layout_analysis_description != "") { ?>
                                                                    <td class="col-md-3"><?php echo $section_metric->ss_metric_master_description; ?>
																	<input type="hidden" name="matric_id_analysis[]" value="<?php echo $section_metric->ss_metric_master_id;?>"/>																	
                                                                    </td>
                                                                    <td class="col-md-9"><textarea class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false" name="analysis_value[]" <?php echo $disable;?>><?php $analysis_data = $this->metricdata->getAnalysisVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($analysis_data)){
																	echo $analysis_data;}?></textarea></td>                
                                                                <?php } ?>
                                                            </tr>
																
                                                   
            <?php $section_id = $section_metric->ss_section_layout_ss_section_layout_id;
			$j++;			
        }
        else {
            ?>
			</tbody>
                                                      
                                                            <tr>
            <?php if ($section_metric->ss_section_layout_section_col_count == 1 && $section_metric->ss_section_layout_analysis_description == "") { ?>
                                                                    <td><?php echo $section_metric->ss_metric_master_description; ?>
																	<input type="hidden" name="matric_id_one_column[]" value="<?php echo $section_metric->ss_metric_master_id;?>"/>																	
                                                                    </td>
                                                                    <td><input type="text" name="one_column_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $one_col_val = $this->metricdata->getOneColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($one_col_val)){ echo $one_col_val;}?>" <?php echo $disable;?> aria-invalid="false"></td>

                                                                <?php }
                                                                elseif ($section_metric->ss_section_layout_section_col_count == 4 && $section_metric->ss_section_layout_analysis_description == "") { ?>
                                                                    <td><?php echo $section_metric->ss_metric_master_description; ?>
																	<input type="hidden" name="matric_id_four_column[]" value="<?php echo $section_metric->ss_metric_master_id;?>"/>																	
                                                                    </td>

                                                                    <td><input type="text" name="four_column_men_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $four_col_data = $this->metricdata->getFourColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($four_col_data->ss_four_column_data_value_men)){ echo $four_col_data->ss_four_column_data_value_men;}?>" <?php echo $disable;?> aria-invalid="false"></td>
                                                                    <td><input type="text" name="four_column_women_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $four_col_data = $this->metricdata->getFourColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($four_col_data->ss_four_column_data_value_women)){ echo $four_col_data->ss_four_column_data_value_women;}?>" <?php echo $disable;?> aria-invalid="false"></td>
																	<td><input type="text" name="four_column_trans_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $four_col_data = $this->metricdata->getFourColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($four_col_data->ss_four_column_data_value_trans)){ echo $four_col_data->ss_four_column_data_value_trans;}?>" <?php echo $disable;?> aria-invalid="false"></td>
                                                                    <td><input type="text" name="four_column_boy_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $four_col_data = $this->metricdata->getFourColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($four_col_data->ss_four_column_data_value_boys)){ echo $four_col_data->ss_four_column_data_value_boys;}?>" <?php echo $disable;?> aria-invalid="false"></td>
                                                                    <td><input type="text" name="four_column_girl_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $four_col_data = $this->metricdata->getFourColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($four_col_data->ss_four_column_data_value_girls)){
																	echo $four_col_data->ss_four_column_data_value_girls;}?>" <?php echo $disable;?> aria-invalid="false"></td>
            <?php }
            elseif ($section_metric->ss_section_layout_analysis_description != "") { ?>
                                                                    <td class="col-md-3"><?php echo $section_metric->ss_metric_master_description; ?>
																	<input type="hidden" name="matric_id_analysis[]" value="<?php echo $section_metric->ss_metric_master_id;?>"/>																	
                                                                    </td>
                                                                    <td class="col-md-9"><textarea class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="analysis_value[]" aria-invalid="false" <?php echo $disable;?>><?php $analysis_data = $this->metricdata->getAnalysisVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($analysis_data)){
																	echo $analysis_data;}?></textarea></td>
                                        <?php } ?>
                                                            </tr>
            <?php } } }
    ?> 
                                                </tbody>
                                            </table>
							
                                        </div>										
<?php echo form_close();?>
</div>
</div>
<?php } ?>

<?php if($this->session->userdata('userinfo')['default_role']==6) { 
if($status==1 || $status=="" || $status==4)  { 
if(!empty($metrics_data))
{   
?>
<?php  if(isset($mpr_id) && !empty($mpr_id) ){?>
<input type="hidden" name="mpr_summary_id" value="<?php echo $mpr_id;?>"/>
<?php }?>
<input type="hidden" name="mpr_summary_month" value="<?php echo $mpr_report_month;?>"/>
<div class="panel-body ng-scope">
	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-0 col-xs-0"></div>
		<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 text-right">		
		<input type="submit" class="btn btn-block btn-primary" name="update_form_data" value="Save"/>
		</div>	
<?php } echo form_close(); 
} 
else {}  
}
 else 
 {   
if(!empty($metrics_data))
{   
?>
<?php  if(isset($mpr_id) && !empty($mpr_id) ){?>
<input type="hidden" name="mpr_summary_id" value="<?php echo $mpr_id;?>"/>
<?php }?>
<input type="hidden" name="mpr_summary_month" value="<?php echo $mpr_report_month;?>"/>
<div class="panel-body ng-scope">
	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-0 col-xs-0"></div>
		<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 text-right">		
		<input type="submit" class="btn btn-block btn-primary" name="update_form_data" value="Save"/>				
    </div>	
<?php } echo form_close();
} ?>

<div class="extra_btn" style="display:none">
<?php 
if(isset($mpr_id) && !empty($mpr_id) && !empty($metrics_data))
{
echo form_open('submit_form_data/'.$this->uri->segment(2));	?>

		<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 text-right">
		<input type="hidden" name="mpr_summary_id" value="<?php echo $mpr_id;?>"/>	
        <input type="hidden" name="current_year" value="<?php echo $mpr_report_month;?>"/>
		
			<button class="btn btn-block btn-primary" onclick="return confirmDelete();" type="submit">
				Submit
			</button>
		</div>
	
<?php echo form_close(); } ?>
</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
<!---Comment Model----------------->
<div class="modal fade" id="comment-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    	<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header" align="center">
					<h3>Comments</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
					</button>
				</div>
                
                <!-- Begin # DIV Form -->
                <div id="div-forms">
                
                    <!-- Begin # Login Form -->
                    <?php $form_attrib = array('id'=>'comment-form');
					echo form_open();
					?>
		                <div class="modal-body" id="pmpo_modal_response">												
				    		<div class="popup-messages">								
    		                      <div class="direct-chat-messages" id="response_pmpo">
									<?php if(!empty($comments)){ foreach($comments as $comment){ ?>
                                        <div class="chat-box-single-line">
											<abbr class="timestamp"><?php echo $comment->created_date;?></abbr>
                                        </div>

										<div class="direct-chat-msg doted-border">
                                          <div class="direct-chat-info clearfix">
                                            <span class="direct-chat-name pull-left"><?php echo $comment->ss_partners_name;?></span>
                                          </div>
                                          
                                          <!--img alt="message user image" src="<?php //echo PUBLIC_URL;?>img/profile_small.jpg" class="direct-chat-img"-->
                                          <div class="direct-chat-text">
                                            <?php echo $comment->comment;?>
                                          </div>
                                          <div class="direct-chat-info clearfix">
                                            <span class="direct-chat-timestamp pull-right"><?php echo $comment->created_time;?></span>
                                          </div>
                                        </div>
									<?php } }?>
                                   </div>								                                    
                            </div>							
                            <div class="clearfix"></div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Type your Comment</label>
                                    <textarea class="form-control" maxlength="1000" id="pmpo_comments" name="pmpo_comments"></textarea>
                                </div>
                            </div>
                            <div class="clearfix"></div>
        		    	</div>
				        <div class="modal-footer">
                            
                            <div class="row">
                                
                                <div class="col-lf-offset-8 col-lg-4 col-md-offset-8 col-md-4">
                                    <button type="button" class="btn btn-primary btn-lg btn-block" onclick="comment_pmpo()">Submit Comment</button>
                                </div>
                            </div>
				    	    
				        </div>
					<input type="hidden" name="partner_id" id="partner_id" value="<?php echo $session_partner_id;?>"/>
                    <?php echo form_close();?>
                    <!-- End # Login Form -->
                 
                </div>
                <!-- End # DIV Form -->
                
			</div>
		</div>
	</div>
<!----------------PO/PM Comments Model---------------------->
<div class="modal fade" id="po_pm_comments">
<div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          <h3 class="modal-title">PO/PM Comments</h3>
        </div>
        <div class="modal-body">
          <table class="table table-striped" id="tblGrid">
            <thead id="tblHead">
              <tr>
                <th>Name</th>
                <th>Comments</th>
                <th class="text-right">Date</th>
              </tr>
            </thead>
            <tbody>
              <?php if(!empty($comments_pmpo)) { foreach($comments_pmpo as $key_comment => $comment_pmpo){?>
              <tr>
				<td><?php echo $comment_pmpo->ss_partners_name;?></td>
                <td>
					<a href="javascript:void(0)" onclick="show_full_comment(<?php echo $comment_pmpo->ss_comments_id;?>);">
					<?php if(strlen($comment_pmpo->comment) > 10)
					{
						$string = substr($comment_pmpo->comment,0,10).'...';
					}else{
						$string = $comment_pmpo->comment;
					}
					echo $string;
					?>
					</a>
					<input type="hidden" id="comment_<?php echo $comment_pmpo->ss_comments_id;?>" value="<?php echo $comment_pmpo->comment;?>"/>
				</td>
                <td class="text-right"><?php echo $comment_pmpo->created_on;?></td>
              </tr>
			<?php } }?>
            </tbody>
          </table>
          
		</div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary " data-dismiss="modal">Close</button>
        </div>
				
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
<!-----------------Approve Model---------------->
<div class="modal fade" id="approve-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    	<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header" align="center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h3 class="modal-title">Approve</h3>
				</div>
                
                <!-- Begin # DIV Form -->
                <div id="div-forms">
                
                    <!-- Begin # Login Form -->
                    <?php $form_attrib = array('id'=>'login-form');
					echo form_open();
					?>
		                <div class="modal-body">
				    		<div class="col-lg-12">
                                <div class="form-group">
                                    <h3>By Clicking on submit button, this data will be approved.</h3>
                                    
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Submit your feedback(if any)</label>
                                    <textarea class="form-control" id="approve_reason"></textarea>
                                </div>
                            </div>
                            <div class="clearfix"></div>
        		    	</div>
				        <div class="modal-footer">
                            
                            <div class="row">
                                
                                <div class="col-lf-offset-8 col-lg-4 col-md-offset-8 col-md-4">
                                    <button type="button" class="btn btn-primary btn-lg btn-block" onclick="approve();">Submit</button>
                                </div>
                            </div>
				    	    
				        </div>
					<input type="hidden" name="month_from_approve" id="month_from_approve" value="<?php echo $post_month_from;?>"/>	
                    <input type="hidden" name="partner_id_approve" id="partner_id_approve" value="<?php echo $session_partner_id;?>"/>
                    <?php echo form_close();?>
                    <!-- End # Login Form -->
                    
                </div>
                <!-- End # DIV Form -->
                
			</div>
		</div>
	</div>
<!-----------------Disapprove Model---------------->
<div class="modal fade" id="disapprove-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    	<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header" align="center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h3 class="modal-title">Disapprove</h3>
				</div>
                
                <!-- Begin # DIV Form -->
                <div id="div-forms">
                
                    <!-- Begin # Login Form -->
					<?php $form_attrib = array('id'=>'login-form');
					echo form_open();
					?>
		                <div class="modal-body">
				    		<div class="col-lg-12">
                                <div class="form-group">
                                    <h3>By Clicking on submit button, this data will be rejected.</h3>
                                    
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Submit your feedback(if any)</label>
                                    <textarea class="form-control" id="reject_reason"></textarea>
                                </div>
                            </div>
                            <div class="clearfix"></div>
        		    	</div>
				        <div class="modal-footer">
                            
                            <div class="row">
                                
                                <div class="col-lf-offset-8 col-lg-4 col-md-offset-8 col-md-4">
                                    <button type="button" class="btn btn-primary btn-lg btn-block" onclick="approve();">Submit</button>
                                </div>
                            </div>
				    	    
				        </div>
                    <?php echo form_close();?>
                    <!-- End # Login Form -->
                    
                </div>
                <!-- End # DIV Form -->
                
			</div>
		</div>
	</div>
<script>
function submit_form(form_name)
{
	if($("#month_from").val() == "")
	{
		$("#error_month").html("Please select month");
		$("#month_from").focus();
	}else{	
		$("#manage_ueh_main_entry").submit();		
	}
}
function comment_pmpo()
{
	var comments = $("#pmpo_comments").val();
	var partner_id = $("#partner_id").val();
	var month_from = $("#month_from_approve").val();
	var project_name = "urban eye health";
	var module_name = "main entry";
	if(partner_id != "" && comments != "")
	{
		var url = "<?php echo BASE_URL('create_pmpo_comment');?>";				 
		var csrfHash = $("input[name=csrf_test_name]").val();
		 
		$.post(url,{<?php echo $this->security->get_csrf_token_name(); ?>: csrfHash,comments:comments,partner_id:partner_id,month_from:month_from,project_name:project_name,module_name:module_name} ,function(response){
				
			var htmldata = "";
			var status = "";
			$("input[name=csrf_test_name]").val(response.csrfHash);
			if(response.success ==1)
			{
				htmldata += '<div class="chat-box-single-line"><abbr class="timestamp">'+response.created_on+'</abbr></div><div class="direct-chat-msg doted-border"><div class="direct-chat-info clearfix"><span class="direct-chat-name pull-left">'+response.partner_name+'</span></div><div class="direct-chat-text">'+response.comments+'</div><div class="direct-chat-info clearfix"><span class="direct-chat-timestamp pull-right">'+response.created_time+'</span></div></div>';
				
				
				$("#pmpo_comments").val("");
				$("#response_pmpo").append(htmldata);
				
			}else{
				$("#pmpo_comments").val("");
				alert("Data is not submitted");
			}			
		},"json");
	}
}
function approve()
{
	var month_from_approve = $("#month_from_approve").val();
	var partner_id_approve = $("#partner_id_approve").val();
	var project_name = "urban eye health";
	var module_name = "main entry";
	var reason = "";
	var approve_flag = "";
	if($("#approve_reason").val() != "")
	{
		reason = $("#approve_reason").val();
		approve_flag = 1;
	}
	else if($("#reject_reason").val() != "")
	{
		reason = $("#reject_reason").val();
		approve_flag = 0;
	}	
	
	if(partner_id_approve != "" && month_from_approve != "")
	{
		var url = "<?php echo BASE_URL('approval');?>";				 
		var csrfHash = $("input[name=csrf_test_name]").val();
		 
		$.post(url,{<?php echo $this->security->get_csrf_token_name(); ?>: csrfHash,month_from_approve:month_from_approve,partner_id_approve:partner_id_approve,project_name:project_name,module_name:module_name,reason:reason,approve_flag:approve_flag} ,function(response){
				
			var htmldata = "";
			var status = "";
			$("input[name=csrf_test_name]").val(response.csrfHash);
			if(response.success ==1)
			{
				if(approve_flag == 1)
				{
					$("#approve_reason").val("");
					alert("Successfully Approved");					
				}else{
					$("#reject_reason").val("");
					alert("Rejected");
				}				
			}else{
				$("#approve_reason").val("");
				$("#reject_reason").val("");
				alert("No data found");
			}			
		},"json");
	}
}

function show_full_comment(id)
{
	if(id != "")
	{
		$("#full_comments_show").html("");
		$("#full_comments_show").html($("#comment_"+id).val());
		$("#full_comments").modal({show: 'True'}); 
	}
}
</script>
<script src="http://localhost/sightsaver/public/js/jquery-2.1.1.js"></script>

<script type="text/javascript">
    function confirmDelete() 
	{
        return confirm('Once the data is submitted it cannot be edited would you like to submit it now?');
    }
</script><script>
function functionTab(id){
		if(id !=""){
        $.ajax({ 
        type: "POST", 
        url: "<?php echo BASE_URL.'Dashboard/tab_active';?>", 
        data: {id:id,section:'ueh'}, 
        success: function(result){ 
		
           }
         });
		}
	
}
</script>
