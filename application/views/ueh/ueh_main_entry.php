
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Sight Savers</title>
        <link href="<?php echo PUBLIC_URL; ?>css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo PUBLIC_URL; ?>font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo PUBLIC_URL; ?>css/animate.css" rel="stylesheet">
        <link href="<?php echo PUBLIC_URL; ?>css/style.css" rel="stylesheet">
        <link href="<?php echo PUBLIC_URL; ?>css/custom.css" rel="stylesheet">
        <link href="<?php echo PUBLIC_URL; ?>css/lc_switch.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo PUBLIC_URL; ?>css/codemirror.css">
        <link rel="stylesheet" href="<?php echo PUBLIC_URL; ?>css/custom.css">
		 <link href="<?php echo PUBLIC_URL; ?>css/bootstrap_datepicker.css" rel="stylesheet"> 
     	<link href="<?php echo PUBLIC_URL; ?>css/dataTables.bootstrap.min.css" rel="stylesheet">
    </head>
<body class="fixed-sidebar">

<div id="wrapper">

<nav class="navbar-admin navbar-static-side" role="navigation">
    <div class="sidebar-collapse">

      <ul side-navigation="" class="nav metismenu" id="side-menu">
      <li class="nav-header" style="padding: 20px;">
        <div class="profile-element">
          <img alt="image" width="100%" src="<?php echo PUBLIC_URL; ?>img/logo.png" src="<?php echo PUBLIC_URL; ?>assets/images/logo.png">
        </div>
        <div class="logo-element">
          SS
        </div>
      </li>
	 
 <?php 
  if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==6)  { ?> 
	   <li ng-class="{active: m.ie}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Data Entry</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" id="menhead123" ng-class="{in: m.ie}" aria-expanded="false">
	  
     </ul>
      </li>
		  <li class="active">
        	<a href="<?php echo BASE_URL.'partner_mpr';?>"><i class="fa fa-eye"></i> <span class="nav-label">Review MPR</span></a>
        
      	</li>
     <?php }  ?>
	 
	   <?php  if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==1 || $this->session->userdata('userinfo')['default_role']==5 || $this->session->userdata('userinfo')['default_role']==2 || $this->session->userdata('userinfo')['default_role']==3 || $this->session->userdata('userinfo')['default_role']==4)  { ?>
	  
	   <li ng-class="{active: m.dashboards}" >
		  <li class="active">
			<a href="<?php echo BASE_URL.'dashboard_speedometer';?>"><i class="fa fa-dashboard"></i> <span class="nav-label">Dashboard</span> </a>
			</li>
			<!--li><a href="<?php //echo BASE_URL.'dashboard';?>">Report</a></li-->
			  <li ng-class="{active: m.reports}">
				<a href=""><i class="fa fa-file-pdf-o"></i> <span class="nav-label">Graph</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse" ng-class="{in: m.reports}" aria-expanded="true">
				  <li  ><a href="#">Social Inclusion</a></li>
				  <li ><a  href="<?php echo BASE_URL.'dashboard_graph';?>">Rural Eye Health</a></li>
				  <li ><a  href="<?php echo BASE_URL.'dashboard_graph_ueh';?>">Urban Eye Health</a></li>
				  <li ><a  href="#">Inclusive Education</a></li>
				</ul>
			  </li>
		  </li>
			<li ng-class="{active: m.reports}" >
				<a href=""><i class="fa fa-file-pdf-o"></i> <span class="nav-label">MPR</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse" ng-class="{in: m.reports}" aria-expanded="true">
				  <li  ><a href="<?php echo BASE_URL.'mpr_si';?>">Social Inclusion</a></li>
				  <li ><a  href="<?php echo BASE_URL.'mpr_reh' ;?>">Rural Eye Health</a></li>
				  <li ><a  href="<?php echo BASE_URL.'mpr_ueh';?>">Urban Eye Health</a></li>
				  <li ><a  href="<?php echo BASE_URL.'mpr_ie';?>">Inclusive Education</a></li>
				</ul>
			</li>
			
			<li ng-class="{active: m.reports}" >
				<a href=""><i class="fa fa-file-pdf-o"></i> <span class="nav-label">Yearly Target</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse" ng-class="{in: m.reports}" aria-expanded="true">
     			 <li ><a  href="<?php echo BASE_URL.'reh_yearly_target/';echo base64_encode('rural eye health-yearly target');?>">REH Yearly Targets</a></li>
				   <li ><a  href="<?php echo BASE_URL.'ueh_yearly_target/';echo base64_encode('urban eye health-yearly target');?>">UEH Yearly Targets</a></li>
				</ul>
			</li>
			
		<?php if($this->session->userdata('userinfo')['default_role']==2 || $this->session->userdata('userinfo')['default_role']==3 || $this->session->userdata('userinfo')['default_role']==4) { ?>
		<li ng-class="{active: m.reports}" >
				<a href=""><i class="fa fa-file-pdf-o"></i> <span class="nav-label">Data approval</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse" ng-class="{in: m.reports}" aria-expanded="true">
				  <li  ><a href="<?php echo BASE_URL.'Approval';?>">Review MPR</a></li>				  
				</ul>
			</li>
		<?php }  ?>
		<?php  if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==1) { ?>
		<li ng-class="{active: m.reports}" >
				<a href=""><i class="fa fa-file-pdf-o"></i> <span class="nav-label">Data approval</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse" ng-class="{in: m.reports}" aria-expanded="true">
				  <li  ><a href="<?php echo BASE_URL.'Approvaladmin';?>">Review MPR</a></li>				  
				</ul>
			</li>
		<?php } ?>
		
		
		
	  
	  <li ng-class="{active: m.ie}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Data Entry</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.ie}" aria-expanded="false">
        <li ng-class="{active: m.ie}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Inclusive Education</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.ie}" aria-expanded="false">
          <li ><a href="<?php echo BASE_URL.'ie_main_entry/';echo base64_encode('Inclusive education-main entry'); ?>">IE Main Entry</a></li>
        </ul>
      </li>

      <li ng-class="{active: m.reh}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Rural Eye Health</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.reh}" aria-expanded="false">
          <li ><a  href="<?php echo BASE_URL.'reh_main_entry/';echo base64_encode('rural eye health-main entry');?>">REH Main Entry</a></li>
          <!--<li ><a  href="<?php //echo BASE_URL.'reh_refferal';?>">Referrals</a></li>-->
		  <li><a href="<?php echo BASE_URL.'reh_base_hospital';?>">REH Base Hospital</a></li>
          <li ><a  href="<?php echo BASE_URL.'reh_training';?>">REH Training | Capacity Building</a></li>
          <li ><a  href="<?php echo BASE_URL.'reh_advocacy';?>">REH Advocacy | Networking | Liasion</a></li>
          <li ><a  href="<?php echo BASE_URL.'reh_bcc';?>">REH BCC Entry</a></li>
          <li ><a  href="<?php echo BASE_URL.'reh_iec';?>">REH IEC Entry</a></li>
          <li ><a  href="<?php echo BASE_URL.'reh_vhsnc';?>">VHSNC Entry</a></li>
         <!-- <li ><a  href="javascript:void(0)">Errors in submission</a></li>  -->
        </ul>
      </li>
	  
      <li ng-class="{active: m.si}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Social Inclusion</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.si}" aria-expanded="false">
          <li ><a  href="<?php echo BASE_URL.'si_pwd_search';?>">SI PWD Search</a></li>
          <li ><a  href="<?php echo BASE_URL.'si_main_entry/';echo base64_encode('Social inclusion-main entry');?>">SI Main Entry</a></li>
          <li ><a  href="<?php echo BASE_URL.'shg';?>">SI SHG | PPG</a></li>
          <li ><a  href="<?php echo BASE_URL.'bpo';?>">SI BPO | DPO</a></li>
          <li ><a  href="<?php echo BASE_URL.'training';?>">SI Training Activity</a></li>
          <li ><a  href="<?php echo BASE_URL.'access_aidit';?>">SI Access Audit</a></li>
          <li ><a  href="<?php echo BASE_URL.'agencies';?>">SI Agencies Support</a></li>
          <li ><a  href="<?php echo BASE_URL.'advocacy';?>">SI Advocacy Activity</a></li>
          <li ><a  href="<?php echo BASE_URL.'iec';?>">SI IEC Activity</a></li>
          <li ><a  href="<?php echo BASE_URL.'bcc';?>">SI BCC Activity</a></li>
          <li ><a  href="<?php echo BASE_URL.'si_yearly_targets';?>">SI Yearly Targets</a></li>
         <!-- <li ><a  href="javascript:void(0)">Errors in PWD</a></li>  -->
        </ul>
      </li>
	  
	 
      <li ng-class="{active: m.ueh}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Urban Eye Health</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.ueh}" aria-expanded="false">
          <li ><a  href="<?php echo BASE_URL.'ueh_main_entry/';echo base64_encode('urban eye health-main entry');?>">Main Entry</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_vision_center';?>">Vision Center</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_base_hospital';?>">Base Hospital</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_outreach_camp';?>">Outreach Camp</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_training_entry';?>">Training | Capacity Building</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_advocacy_entry';?>">Advocacy | Networking | Liasion</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_bcc_entry';?>">BCC Entry</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_iec_entry';?>">IEC</a></li>
        </ul>
      </li>
        </ul>
      </li>

      <li ng-class="{active: m.admin}">
        <a href=""><i class="fa fa-gears"></i> <span class="nav-label">Administration</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.admin}" aria-expanded="false">
		  <li ><a  href="<?php echo BASE_URL.'Administration/';echo 'districts';?>">Districts</a></li>
          <li ><a  href="<?php echo BASE_URL.'Administration/';echo 'partner';?>">Partners</a></li>          
          <li ><a  href="<?php echo BASE_URL.'Administration/';echo 'user';?>">Users</a></li>
          <li ><a  href="<?php echo BASE_URL.'Administration/';echo 'reporting_quarters';?>">Reporting Quarters</a></li>
        </ul>
      </li>
	  <?php } if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==6) {} ?>
    </ul>
    </div>
</nav>
<div id="page-wrapper" class="gray-bg">
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary SeeMore2 " href="#"> <i class="fa fa-caret-left" aria-hidden="true"></i>	
           </a>

        </div>
		
        <ul class="nav navbar-top-links navbar-right">
	      <?php   if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==6) {  ?>
		   <li>
			<select class="form-control" id="selectId" onchange="part_fill(this.value);program_district(this.value);">
				<option value="">Select partner</option>
				<?php
                  if(!empty($dis)){
                    foreach($dis as $dist){ ?>
					<option value="<?php echo $dist->ss_partners_id ?>"<?php if($dist->ss_partners_id==$_SESSION["partner_id"])echo 'selected'; else echo ''; ?>><?php echo $dist->ss_partners_name; ?></option>
				  <?php } } ?>
				</select>
	      </li>
			
			<?php  }  ?>
          <li>
            <div class="text-muted welcome-message">
              <a ui-sref="main.profile" class="ng-binding"><span class="user-img"><img src="<?php echo PUBLIC_URL;?>img/default-user-icon-profile.png" alt=""></span> Hi <?php   echo $this->session->userdata('userinfo')['fname'] ?></a>
            </div>
          </li>
          <li>
            <a href="<?php echo BASE_URL.'logout';?>">
              <i class="fa fa-sign-out"></i> Log out
            </a>
          </li>
        </ul>
    </nav>
</div>


<div class="row wrapper border-bottom white-bg page-heading ng-scope">
    <div class="col-lg-10">
        <h2><?php echo @strtoupper($this->session->userdata['userinfo']['partner_name']); ?></h2>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeIn ng-scope" ng-init="init()">
<?php if(isset($_SESSION['success']))
{ ?>
<div class="alert alert-success">
  <strong></strong> <?php echo $_SESSION['success'] ; ?>
</div>
<?php } ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Urban Eye Health/Main Entry</h5>
                </div>
                <div class="ibox-content">
				<?php
					if($this->session->userdata('userinfo')['default_role'] == 6) //if partner
					{?>
				<?php
				$form_attrib = array('id'=>'manage_ueh_main_entry');
				echo form_open('manage_ueh_main_entry/'.$this->uri->segment(2),$form_attrib);?>
                    <div class="row flex-element center-both">
                        <div class="well well-sm main-entry-box ">
                            <div class="col-md-3 text-right">
                                <label>Month:</label> 
                            </div>
                            <div class="col-md-5">
							<?php
			                    if(!isset($_SESSION["month_date"]))
								  {
								     $_SESSION["month_date"] = date("m-Y", strtotime("-1 months"));
			                      } 
								  ?>
                                <input type="text" name="month_from"  id="month_from" value="<?php echo $_SESSION["month_date"]; ?>" class="form-control datepicker" onchange="date_fill()">
								<span class="text-danger ng-binding ng-hide" aria-hidden="true" id="error_month"><?php echo form_error('month_data');?></span>
                            </div>
                            <div class="col-md-4">
                                <button class="btn btn-sm btn-primary btn-block" id="ueh_submit" onclick="submit_form('manage_ueh_main_entry');" type="button">
                                    Get Data
                                </button>
                            </div>	
                            </div>							
					<?php echo form_close();?>
					<?php
					//$form_attrib = array('id'=>'excelupload_ueh');
					//echo form_open_multipart('excelupload_ueh/'.$this->uri->segment(2),$form_attrib);  ?>
                    
                    </div>
					<?php echo form_close(); ?>
					<?php }else{ //if pm/po ?>
					<?php $form_attrib = array('id'=>'manage_ueh_main_entry1');
						echo form_open('manage_ueh_main_entry/'.$this->uri->segment(2),$form_attrib);?>
					<div class="row flex-element center-both">
						<div class="well well-sm main-entry-box">
						  <div class="col-md-6">
							<select class="form-control" name="partner_name">
							  <option value="">Select partner</option>
							  <?php if(!empty($partners)){ foreach($partners as $partner){?>
							  <option value="<?php echo $partner->ss_partners_id;?>"><?php echo $partner->ss_partners_name;?></option>
							  <?php } }?>
							</select>
						  </div>
						  <div class="col-md-4">
								<input type="text" name="month_from" id="month_from" class="form-control datepicker" placeholder="Select month" onchange="date_fill()">
								<p class="text-danger ng-binding ng-hide" aria-hidden="true" id="error_month"><?php echo form_error('month_data');?></p>
						  </div>
						  <div class="col-md-2">
							<button class="btn btn-sm btn-primary btn-block" onclick="submit_form('manage_ueh_main_entry');" type="button">
								Get Data
							</button>
						  </div>
						</div>
					</div>
					<?php echo form_close();?>
					<?php } ?>
                    <div class="row" ng-show="hasData" aria-hidden="false" style="">
                        <div class="col-md-12">
                            <div class="tabs-container">
                                <div class="tabs-left ng-isolate-scope">
                                    <ul class="nav nav-tabs">
                                        <?php
                                        if (!empty($sections)) {
                                            foreach ($sections as $key_section => $section) {
                                                ?>
                                                <li class="uib-tab nav-item ng-scope ng-isolate-scope<?php if ($key_section == 0) { ?> active <?php } ?>">
                                                    <a href="#tab<?php echo $section->ss_section_layout_ss_section_layout_id; ?>" data-toggle="tab" class="nav-link ng-binding"><?php echo ucwords($section->ss_section_layout_section_name); ?></a>
                                                </li>
                                            <?php
                                            }
                                        }
                                        ?>
                                    </ul>
									<?php
									$form_attrib = array('id'=>'ueh_main_entry');
									echo form_open('create_ueh_main_entry/'.$this->uri->segment(2),$form_attrib);?>
                                    <div class="tab-content">
                                        <?php $section_id = ""; $j=0; //$k=0;?>                                       
                                        <?php
                                        if (!empty($metrics_data)) {

                                            foreach ($metrics_data as $key_section_metric => $section_metric) {
												
                                                if ($section_id != $section_metric->ss_section_layout_ss_section_layout_id) {
                                                    if ($j != 0) {
                                                        ?>
                                                        </tbody>
                                                        </table>
                                                    </div>
													<input type="hidden" name="month_data"/>
                                                </div>
                                            </div>
            <?php } ?>
										
                                        <div class="tab-pane ng-scope<?php if ($section_id == "") { ?> active<?php } ?>" id="tab<?php echo $section_metric->ss_section_layout_ss_section_layout_id; ?>">

                                            <div class="panel-body ng-scope">
                                                <div class="bg-heading p-xs b-r-sm ng-scope"><i class="fa fa-arrow-right"></i><?php echo ucwords($section_metric->ss_section_layout_section_name); ?> </div>                                                       
                                                <h4 class="ng-scope">
                                                <?php echo $section_metric->ss_section_layout_analysis_description; ?>
                                                </h4>                                                
                                                <div class="table-responsive ng-scope">
                                                    <table class="table table-bordered">
                                                        <thead>

                                                            <tr>
                                                                <?php if ($section_metric->ss_section_layout_section_col_count == 1 && $section_metric->ss_section_layout_analysis_description == "") { ?>
                                                                    <td>Metric</td>       
                                                                    <td>Value</td>
            <?php }
            elseif ($section_metric->ss_section_layout_section_col_count == 4 && $section_metric->ss_section_layout_analysis_description == "") { ?>
                                                                    <td>Metric</td>       
                                                                    <td>Men</td>
                                                                    <td>Women</td>
																	<td>Transgender</td>
                                                                    <td>Boys</td>
                                                                    <td>Girls</td>
            <?php }
            elseif ($section_metric->ss_section_layout_analysis_description != "") { ?>
                                                                    <td>Question</td>       
                                                                    <td>Response</td>
            <?php } ?>
                                                            </tr>

                                                        </thead>
                                                        <tbody>

                                                            <tr>
            <?php if ($section_metric->ss_section_layout_section_col_count == 1 && $section_metric->ss_section_layout_analysis_description == "") { ?>
                                                                    <td>
																	<table border="0" cellspacing="0" cellpadding="0">
																		<tbody>
																			<tr>
																				<td width="98%" valign="top"><?php echo $section_metric->ss_metric_master_description;  ?></td>
																				<td width="2%" valign="top">
																				<?php if(isset($section_metric->ss_tooltip_description) && $section_metric->ss_tooltip_description!=null) { ?>
																				<a href="#" data-toggle="tooltip" title="<?php echo $section_metric->ss_tooltip_description; ?>"><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a>  <?php } ?>
																				<input type="hidden" name="matric_id_one_column[]" value="<?php echo $section_metric->ss_metric_master_id;?>"/>
																				</td>
																			</tr>
																		</tbody>
																	</table>
																	
																																		
                                                                    </td>
                                                                    <td><input type="text" name="one_column_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php if(!empty($return_data) &&$return_data['layout_id'] == $section_metric->ss_section_layout_ss_section_layout_id){ $key_r = array_search($section_metric->ss_metric_master_id,$return_data['matric_id_one_column_r']); echo $return_data['one_col_val_r'][$key_r]; }?>" aria-invalid="false"></td>
            <?php }
            elseif ($section_metric->ss_section_layout_section_col_count == 4 && $section_metric->ss_section_layout_analysis_description == "") { ?>
                                                                    <td>
																	
																	<table border="0" cellspacing="0" cellpadding="0">
																		<tbody>
																			<tr>
																				<td width="98%" valign="top"><?php echo $section_metric->ss_metric_master_description;  ?></td>
																				<td width="2%" valign="top">
																				<?php if(isset($section_metric->ss_tooltip_description) && $section_metric->ss_tooltip_description!=null) { ?>
																				<a href="#" data-toggle="tooltip" title="<?php echo $section_metric->ss_tooltip_description; ?>"><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a>  <?php } ?>
																				<input type="hidden" name="matric_id_four_column[]" value="<?php echo $section_metric->ss_metric_master_id;?>"/>
																				</td>
																			</tr>
																		</tbody>
																	</table>
																																
                                                                    </td>

                                                                    <td><input type="text" name="four_column_men_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
                                                                    <td><input type="text" name="four_column_women_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
																	<td><input type="text" name="four_column_trans_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
                                                                    <td><input type="text" name="four_column_boy_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
                                                                    <td><input type="text" name="four_column_girl_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
                                                            <?php }
                                                            elseif ($section_metric->ss_section_layout_analysis_description != "") { ?>
                                                                    <td class="col-md-3">
																	<table border="0" cellspacing="0" cellpadding="0">
																		<tbody>
																			<tr>
																				<td width="98%" valign="top"><?php echo $section_metric->ss_metric_master_description;  ?></td>
																				<td width="2%" valign="top">
																				<?php if(isset($section_metric->ss_tooltip_description) && $section_metric->ss_tooltip_description!=null) { ?>
																				<a href="#" data-toggle="tooltip" title="<?php echo $section_metric->ss_tooltip_description; ?>"><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a>  <?php } ?>
																				<input type="hidden" name="matric_id_analysis[]" value="<?php echo $section_metric->ss_metric_master_id;?>"/>
																				</td>
																			</tr>
																		</tbody>
																	</table>
																																		
                                                                    </td>
                                                                    <td class="col-md-9"><textarea class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false" name="analysis_value[]"></textarea></td>                
                                                                <?php } ?>
                                                            </tr>
																
                                                   
            <?php $section_id = $section_metric->ss_section_layout_ss_section_layout_id;
			$j++;
        }
        else {
            ?>
			</tbody>
                                                      
                                                            <tr>
            <?php if ($section_metric->ss_section_layout_section_col_count == 1 && $section_metric->ss_section_layout_analysis_description == "") { ?>
                                                                    <td>
																	<table border="0" cellspacing="0" cellpadding="0">
																		<tbody>
																			<tr>
																				<td width="98%" valign="top"><?php echo $section_metric->ss_metric_master_description;  ?></td>
																				<td width="2%" valign="top">
																				<?php if(isset($section_metric->ss_tooltip_description) && $section_metric->ss_tooltip_description!=null) { ?>
																				<a href="#" data-toggle="tooltip" title="<?php echo $section_metric->ss_tooltip_description; ?>"><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a>  <?php } ?>
																				<input type="hidden" name="matric_id_one_column[]" value="<?php echo $section_metric->ss_metric_master_id;?>"/>
																				</td>
																			</tr>
																		</tbody>
																	</table>
																															
                                                                    </td>
                                                                    <td><input type="text" name="one_column_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>

                                                                <?php }
                                                                elseif ($section_metric->ss_section_layout_section_col_count == 4 && $section_metric->ss_section_layout_analysis_description == "") { ?>
                                                                    <td>
																	
																	<table border="0" cellspacing="0" cellpadding="0">
																		<tbody>
																			<tr>
																				<td width="98%" valign="top"><?php echo $section_metric->ss_metric_master_description;  ?></td>
																				<td width="2%" valign="top">
																				<?php if(isset($section_metric->ss_tooltip_description) && $section_metric->ss_tooltip_description!=null) { ?>
																				<a href="#" data-toggle="tooltip" title="<?php echo $section_metric->ss_tooltip_description; ?>"><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a>  <?php } ?>
																				<input type="hidden" name="matric_id_four_column[]" value="<?php echo $section_metric->ss_metric_master_id;?>"/>
																				</td>
																			</tr>
																		</tbody>
																	</table>
																																		
                                                                    </td>

                                                                    <td><input type="text" name="four_column_men_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
                                                                    <td><input type="text" name="four_column_women_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
																	<td><input type="text" name="four_column_trans_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
                                                                    <td><input type="text" name="four_column_boy_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
                                                                    <td><input type="text" name="four_column_girl_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
            <?php }
            elseif ($section_metric->ss_section_layout_analysis_description != "") { ?>
                                                                    <td class="col-md-3">
																	<table border="0" cellspacing="0" cellpadding="0">
																		<tbody>
																			<tr>
																				<td width="98%" valign="top"><?php echo $section_metric->ss_metric_master_description;  ?></td>
																				<td width="2%" valign="top">
																				<?php if(isset($section_metric->ss_tooltip_description) && $section_metric->ss_tooltip_description!=null) { ?>
																				<a href="#" data-toggle="tooltip" title="<?php echo $section_metric->ss_tooltip_description; ?>"><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a>  <?php } ?>
																				<input type="hidden" name="matric_id_analysis[]" value="<?php echo $section_metric->ss_metric_master_id;?>"/>
																				</td>
																			</tr>
																		</tbody>
																	</table>
																																		
                                                                    </td>
                                                                    <td class="col-md-9"><textarea class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="analysis_value[]" aria-invalid="false"></textarea>
																	</td>
                                                               <?php } ?>
                                                            </tr>
                                                          <?php } }
                                                         ?> 
                                                </tbody>
                                            </table>
                                        </div>
										<input type="hidden" name="month_data"/>										
                                    </div>
                                </div>
<?php } ?>
<?php
if($this->session->userdata('userinfo')['default_role'] == 6 && !empty($metrics_data)) //if partner
{?>
<div class="panel-body ng-scope">
	<div class="row">
		<div class="col-lg-2 col-lg-offset-10 col-md-2 col-md-offset-10 col-sm-4 col-sm-offset-4 text-right">
			<button class="btn btn-block btn-primary" onclick="submit_form('ueh_main_entry');" type="button">
				Save
			</button>
		</div>
	</div>
</div>
<?php }?>                      
<?php echo form_close();?>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</div>
</div>
</div>

<script>document.getElementById('manage_ueh_main_entry').submit();</script>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js">
</script>


 <!--<script>document.getElementById('manage_ueh_main_entry').submit();</script>-->

<?php
$server_date = server_date_time(); 
$date11=date('m-Y',strtotime($server_date));
?>
<script>
 var tadayc = "<?php echo $date11; ?>";
function submit_form(form_name)
{ // alert(tadayc);
	if($("#month_from").val() =="")
	{  
		$("#error_month").html("Please select month");
		$("#month_from").focus();
	}
	 else if($("#month_from").val() > tadayc)
	 {
		 $("#error_month").html("Please select less then equal to current month");
		 $("#month_from").focus();
	 }
	else{		
		if(form_name == "ueh_main_entry")
		{
		$("#ueh_main_entry").submit();
		}
		else if(form_name == "manage_ueh_main_entry")
		{
			$("#manage_ueh_main_entry").submit();
		}
	}
}
function submit_form_file()
{
	if($('input[name="upload_publish_file"]').val() == "")
	{
		$("#error_file").html("Please select file");
	}else{
		$("#excelupload_ueh").submit();
	}
}
</script>