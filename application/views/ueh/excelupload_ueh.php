
<div class="row wrapper border-bottom white-bg page-heading ng-scope">
    <div class="col-lg-10">
        <h2>Upload Excel File UEH</h2>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeIn ng-scope" ng-init="init()">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Main sheet</h5>
                </div>
                <div class="ibox-content">
				<?php  echo form_open_multipart('excelupload_ueh');  ?>
                    <div class="row flex-element center-both">
                        <div class="well well-sm main-entry-box">
                            <div class="col-md-4">
                                <input type="file" name="upload_publish_file" class="form-control">
                            </div>
                            <div class="col-md-2">
                                <button class="btn btn-sm btn-primary btn-block" type="submit" >
                                    submit
                                </button>
                            </div>
                        </div>
                    </div>
					<?php echo form_close();  ?>
                    <div class="row" ng-show="hasData" aria-hidden="false" style="">
                        <div class="col-md-12">
                            <div class="tabs-container">
                                <div class="tabs-left ng-isolate-scope">
                                    <ul class="nav nav-tabs">
                                        <?php if (!empty($sections)) {
                                            foreach ($sections as $key_section => $section) { ?>
                                                <li class="uib-tab nav-item ng-scope ng-isolate-scope<?php if ($key_section == 0) { ?> active <?php } ?>">
                                                    <a href="#tab<?php echo $key_section; ?>" data-toggle="tab" class="nav-link ng-binding"><?php echo ucwords($section->ss_section_layout_section_name); ?></a>
                                                </li>
    <?php }
} ?>
                                    </ul>
                                    <div class="tab-content">
                                        <!-- ngRepeat: tab in tabset.tabs -->
<?php if (!empty($metrics_data)) {
    foreach ($metrics_data as $key_metric => $metric_data) { ?>
                                                <div class="tab-pane ng-scope<?php if ($key_metric == 0) { ?> active<?php } ?>" id="tab<?php echo $key_metric; ?>">

                                                    <div class="panel-body ng-scope">
                                                        <!-- ngInclude: --><ng-include src="'app/reh/partials/main_entry/camps.html'" class="ng-scope"><div class="bg-heading p-xs b-r-sm ng-scope"><i class="fa fa-arrow-right"></i> <?php echo ucwords($metric_data->ss_section_layout_section_name); ?></div>
                                                                <?php if (!empty($metric_data->ss_section_layout_analysis_description)) { ?>
                                                                <h4 class="ng-scope">
                                                                    Please provide a brief analysis of the information for the following sections:
                                                                     <?php echo $metric_data->ss_section_layout_analysis_description; ?>
                                                                    <!--ul>
                                                                        <li>Vision Centres /Camps</li>
                                                                        <li>OPD/Screening</li>
                                                                        <li>Referral/ Reported</li>
                                                                        <li>Refraction</li>
                                                                        <li>Spectacles</li>
                                                                        <li>Cataract</li>
                                                                        <li>Glaucoma</li>
                                                                        <li>Diabetic Retinopathy (DR)</li>
                                                                        <li>Low Vision and irreversible blindness</li>
                                                                        <li>Any other treatment</li>
                                                                    </ul-->
                                                                </h4>
                                                                <?php } ?>
                                                            <span><?php
                                                        if (!empty($this->session->flashdata('success'))) {
                                                            echo $this->session->flashdata('success');
                                                        }
                                                        ?></span>
                                                          <?php echo form_open('reh_main_entry_vc_create'); ?>
                                                            <div class="table-responsive ng-scope">
                                                                <table class="table table-bordered">
                                                                    <thead>
                                                                        <tr>                                                                    
                                                                            <td>Metric</td>
        <?php if ($metric_data->ss_section_layout_section_col_count == 1) { ?>
                                                                                <td>Value</td>
                                                                            <?php }
                                                                            elseif ($metric_data->ss_section_layout_section_col_count == 4) { ?>
                                                                                <td>Men</td>
                                                                                <td>Women</td>
                                                                                <td>Boys</td>
                                                                                <td>Girls</td>
        <?php } ?>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php for($i=0;$i<=$metric_data->ss_section_layout_section_col_count;$i++){?>
                                                                        <tr>
                                                                            <td><?php echo $metric_data->ss_metric_master_description; ?><input type="hidden" name="vision_center_master_id[]" value="<?php //echo $vision_center_data->ss_metric_master_id;  ?>"/></td>
                                                                            <td><input type="number" name="vision_center_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
                                                                        </tr>
                                                                        <?php }?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <input type="submit" name="sub"/>
        <?php echo form_close(); ?>
                                                        </ng-include>
                                                    </div>
                                                </div><!-- end ngRepeat: tab in tabset.tabs -->
    <?php }
} ?>
                                        <!-- end ngRepeat: tab in tabset.tabs -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

