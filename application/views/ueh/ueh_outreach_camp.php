
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Sight Savers</title>
        <link href="<?php echo PUBLIC_URL; ?>css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo PUBLIC_URL; ?>font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo PUBLIC_URL; ?>css/animate.css" rel="stylesheet">
        <link href="<?php echo PUBLIC_URL; ?>css/style.css" rel="stylesheet">
        <link href="<?php echo PUBLIC_URL; ?>css/custom.css" rel="stylesheet">
        <link href="<?php echo PUBLIC_URL; ?>css/lc_switch.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo PUBLIC_URL; ?>css/codemirror.css">
        <link rel="stylesheet" href="<?php echo PUBLIC_URL; ?>css/custom.css">
		 <link href="<?php echo PUBLIC_URL; ?>css/bootstrap_datepicker.css" rel="stylesheet"> 
     	<link href="<?php echo PUBLIC_URL; ?>css/dataTables.bootstrap.min.css" rel="stylesheet">
    </head>
<body class="fixed-sidebar">

<div id="wrapper">

<nav class="navbar-admin navbar-static-side" role="navigation">
    <div class="sidebar-collapse">

      <ul side-navigation="" class="nav metismenu" id="side-menu">
      <li class="nav-header" style="padding: 20px;">
        <div class="profile-element">
          <img alt="image" width="100%" src="<?php echo PUBLIC_URL; ?>img/logo.png" src="<?php echo PUBLIC_URL; ?>assets/images/logo.png">
        </div>
        <div class="logo-element">
          SS
        </div>
      </li>
	 
 <?php 
  if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==6)  { ?> 
	   <li ng-class="{active: m.ie}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Data Entry</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" id="menhead123" ng-class="{in: m.ie}" aria-expanded="false">
	  
     </ul>
      </li>
		 <li class="active">
        	<a href="<?php echo BASE_URL.'partner_mpr';?>"><i class="fa fa-eye"></i> <span class="nav-label">Review MPR</span></a>
        
      	</li>
     <?php }  ?>
	 
	   <?php  if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==1 || $this->session->userdata('userinfo')['default_role']==5 || $this->session->userdata('userinfo')['default_role']==2 || $this->session->userdata('userinfo')['default_role']==3 || $this->session->userdata('userinfo')['default_role']==4)  { ?>
	  
	   <li ng-class="{active: m.dashboards}" >
		  <li class="active">
			<a href="<?php echo BASE_URL.'dashboard_speedometer';?>"><i class="fa fa-dashboard"></i> <span class="nav-label">Dashboard</span> </a>
			</li>
			<!--li><a href="<?php //echo BASE_URL.'dashboard';?>">Report</a></li-->
			  <li ng-class="{active: m.reports}">
				<a href=""><i class="fa fa-file-pdf-o"></i> <span class="nav-label">Graph</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse" ng-class="{in: m.reports}" aria-expanded="true">
				  <li  ><a href="#">Social Inclusion</a></li>
				  <li ><a  href="<?php echo BASE_URL.'dashboard_graph';?>">Rural Eye Health</a></li>
				  <li ><a  href="<?php echo BASE_URL.'dashboard_graph_ueh';?>">Urban Eye Health</a></li>
				  <li ><a  href="#">Inclusive Education</a></li>
				</ul>
			  </li>
		  </li>
			<li ng-class="{active: m.reports}" >
				<a href=""><i class="fa fa-file-pdf-o"></i> <span class="nav-label">MPR</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse" ng-class="{in: m.reports}" aria-expanded="true">
				  <li  ><a href="<?php echo BASE_URL.'mpr_si';?>">Social Inclusion</a></li>
				  <li ><a  href="<?php echo BASE_URL.'mpr_reh' ;?>">Rural Eye Health</a></li>
				  <li ><a  href="<?php echo BASE_URL.'mpr_ueh';?>">Urban Eye Health</a></li>
				  <li ><a  href="<?php echo BASE_URL.'mpr_ie';?>">Inclusive Education</a></li>
				</ul>
			</li>
			
			<li ng-class="{active: m.reports}" >
				<a href=""><i class="fa fa-file-pdf-o"></i> <span class="nav-label">Yearly Target</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse" ng-class="{in: m.reports}" aria-expanded="true">
     			 <li ><a  href="<?php echo BASE_URL.'reh_yearly_target/';echo base64_encode('rural eye health-yearly target');?>">REH Yearly Targets</a></li>
				   <li ><a  href="<?php echo BASE_URL.'ueh_yearly_target/';echo base64_encode('urban eye health-yearly target');?>">UEH Yearly Targets</a></li>
				</ul>
			</li>
			
		<?php if($this->session->userdata('userinfo')['default_role']==2 || $this->session->userdata('userinfo')['default_role']==3 || $this->session->userdata('userinfo')['default_role']==4) { ?>
		<li ng-class="{active: m.reports}" >
				<a href=""><i class="fa fa-file-pdf-o"></i> <span class="nav-label">Data approval</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse" ng-class="{in: m.reports}" aria-expanded="true">
				  <li  ><a href="<?php echo BASE_URL.'Approval';?>">Review MPR</a></li>				  
				</ul>
			</li>
		<?php }  ?>
		<?php  if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==1) { ?>
		<li ng-class="{active: m.reports}" >
				<a href=""><i class="fa fa-file-pdf-o"></i> <span class="nav-label">Data approval</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse" ng-class="{in: m.reports}" aria-expanded="true">
				  <li  ><a href="<?php echo BASE_URL.'Approvaladmin';?>">Review MPR</a></li>				  
				</ul>
			</li>
		<?php } ?>
		
		
		
	  
	  <li ng-class="{active: m.ie}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Data Entry</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.ie}" aria-expanded="false">
        <li ng-class="{active: m.ie}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Inclusive Education</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.ie}" aria-expanded="false">
          <li ><a href="<?php echo BASE_URL.'ie_main_entry/';echo base64_encode('Inclusive education-main entry'); ?>">IE Main Entry</a></li>
        </ul>
      </li>

      <li ng-class="{active: m.reh}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Rural Eye Health</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.reh}" aria-expanded="false">
          <li ><a  href="<?php echo BASE_URL.'reh_main_entry/';echo base64_encode('rural eye health-main entry');?>">REH Main Entry</a></li>
          <!--<li ><a  href="<?php //echo BASE_URL.'reh_refferal';?>">Referrals</a></li>-->
		  <li><a href="<?php echo BASE_URL.'reh_base_hospital';?>">REH Base Hospital</a></li>
          <li ><a  href="<?php echo BASE_URL.'reh_training';?>">REH Training | Capacity Building</a></li>
          <li ><a  href="<?php echo BASE_URL.'reh_advocacy';?>">REH Advocacy | Networking | Liasion</a></li>
          <li ><a  href="<?php echo BASE_URL.'reh_bcc';?>">REH BCC Entry</a></li>
          <li ><a  href="<?php echo BASE_URL.'reh_iec';?>">REH IEC Entry</a></li>
          <li ><a  href="<?php echo BASE_URL.'reh_vhsnc';?>">VHSNC Entry</a></li>
         <!-- <li ><a  href="javascript:void(0)">Errors in submission</a></li>  -->
        </ul>
      </li>
	  
      <li ng-class="{active: m.si}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Social Inclusion</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.si}" aria-expanded="false">
          <li ><a  href="<?php echo BASE_URL.'si_pwd_search';?>">SI PWD Search</a></li>
          <li ><a  href="<?php echo BASE_URL.'si_main_entry/';echo base64_encode('Social inclusion-main entry');?>">SI Main Entry</a></li>
          <li ><a  href="<?php echo BASE_URL.'shg';?>">SI SHG | PPG</a></li>
          <li ><a  href="<?php echo BASE_URL.'bpo';?>">SI BPO | DPO</a></li>
          <li ><a  href="<?php echo BASE_URL.'training';?>">SI Training Activity</a></li>
          <li ><a  href="<?php echo BASE_URL.'access_aidit';?>">SI Access Audit</a></li>
          <li ><a  href="<?php echo BASE_URL.'agencies';?>">SI Agencies Support</a></li>
          <li ><a  href="<?php echo BASE_URL.'advocacy';?>">SI Advocacy Activity</a></li>
          <li ><a  href="<?php echo BASE_URL.'iec';?>">SI IEC Activity</a></li>
          <li ><a  href="<?php echo BASE_URL.'bcc';?>">SI BCC Activity</a></li>
          <li ><a  href="<?php echo BASE_URL.'si_yearly_targets';?>">SI Yearly Targets</a></li>
         <!-- <li ><a  href="javascript:void(0)">Errors in PWD</a></li>  -->
        </ul>
      </li>
	  
	 
      <li ng-class="{active: m.ueh}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Urban Eye Health</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.ueh}" aria-expanded="false">
          <li ><a  href="<?php echo BASE_URL.'ueh_main_entry/';echo base64_encode('urban eye health-main entry');?>">Main Entry</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_vision_center';?>">Vision Center</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_base_hospital';?>">Base Hospital</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_outreach_camp';?>">Outreach Camp</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_training_entry';?>">Training | Capacity Building</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_advocacy_entry';?>">Advocacy | Networking | Liasion</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_bcc_entry';?>">BCC Entry</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_iec_entry';?>">IEC</a></li>
        </ul>
      </li>
        </ul>
      </li>

      <li ng-class="{active: m.admin}">
        <a href=""><i class="fa fa-gears"></i> <span class="nav-label">Administration</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.admin}" aria-expanded="false">
		  <li ><a  href="<?php echo BASE_URL.'Administration/';echo 'districts';?>">Districts</a></li>
          <li ><a  href="<?php echo BASE_URL.'Administration/';echo 'partner';?>">Partners</a></li>          
          <li ><a  href="<?php echo BASE_URL.'Administration/';echo 'user';?>">Users</a></li>
          <li ><a  href="<?php echo BASE_URL.'Administration/';echo 'reporting_quarters';?>">Reporting Quarters</a></li>
        </ul>
      </li>
	  <?php } if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==6) {} ?>
    </ul>
    </div>
</nav>
<div id="page-wrapper" class="gray-bg">
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary SeeMore2 " href="#"> <i class="fa fa-caret-left" aria-hidden="true"></i>	
           </a>

        </div>
		
        <ul class="nav navbar-top-links navbar-right">
	      <?php   if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==6) {  ?>
		   <li>
			<select class="form-control" id="selectId" onchange="part_fill(this.value);program_district(this.value);">
				<option value="">Select partner</option>
				<?php
                  if(!empty($dis)){
                    foreach($dis as $dist){ ?>
					<option value="<?php echo $dist->ss_partners_id ?>"<?php if($dist->ss_partners_id==$_SESSION["partner_id"])echo 'selected'; else echo ''; ?>><?php echo $dist->ss_partners_name; ?></option>
				  <?php } } ?>
				</select>
	      </li>
			
			<?php  }  ?>
          <li>
            <div class="text-muted welcome-message">
              <a ui-sref="main.profile" class="ng-binding"><span class="user-img"><img src="<?php echo PUBLIC_URL;?>img/default-user-icon-profile.png" alt=""></span> Hi <?php   echo $this->session->userdata('userinfo')['fname'] ?></a>
            </div>
          </li>
          <li>
            <a href="<?php echo BASE_URL.'logout';?>">
              <i class="fa fa-sign-out"></i> Log out
            </a>
          </li>
        </ul>
    </nav>
</div>

<div class="row wrapper border-bottom white-bg page-heading ng-scope">
  <div class="col-lg-10">
    <h2><?php echo @strtoupper($this->session->userdata['userinfo']['partner_name']); ?></h2>
  </div>
</div>

<div class="wrapper wrapper-content animated fadeIn ng-scope" ng-init="init()">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox">
        <div class="ibox-title">
          <h5>Urban Eye Health / Outreach Camp</h5>
        </div>
        <div class="ibox-content">
		<?php
    $attributes = array('id' => 'myform');?>
		<form method="GET" action="<?php echo BASE_URL.'filter_oc_ueh'?>">
          <div class="row flex-element center-both">
            <div class="well well-sm main-entry-box">
              <div class="col-md-3 text-right">
                    <label>Month:</label> 
                </div>
              <div class="col-md-5">
			  <?php
				if(!isset($_SESSION["month_date"])) {
				  $_SESSION["month_date"] = date("m-Y", strtotime("-1 months"));
				 }
				  ?>
               <input type="text" name="month_from" value="<?php echo $_SESSION["month_date"]; ?>" id="month_from" class="form-control datepicker" onchange="date_fill()">
			   <span class="text-danger ng-binding ng-hide" aria-hidden="true"><?php echo form_error('month_data');?>
				<?php echo form_error('month_from');?>
			   </span>
              </div>
              <div class="col-md-4">
                <button class="btn btn-sm btn-primary btn-block" type="submit">
                Get Data
              </button>
              </div>
            </div>
          </div>
		 </form>




          <div class="row " ng-show="hasData" aria-hidden="false" style="">
            <div class="col-md-12">
			<?php
            $attributes1 = array('id' => 'myform1');
			echo form_open('create_oc_ueh',$attributes1);?>
              <div class="panel panel-primary">
                <div class="panel-heading">
                  Outreach Camp
                </div>
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
					  <input type="hidden"  name="id_ajax" id="id_ajax1">
                        <label>Name of the Camp Site / location</label>
                        <input type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="name_cs" ng-model="form.location_name" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.location_name" aria-hidden="true"></p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Camp supported by</label>
                        
						<select class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="camp_support" id="camp_support-ajax" aria-invalid="false">
						<option value="">Select</option>
                          <option value="Baxter India" class="ng-binding ng-scope">Baxter India</option>
						  <option value="Central Coalfields Ltd" class="ng-binding ng-scope">Central Coalfields Ltd</option>
						  <option value="Coal India" class="ng-binding ng-scope">Coal India</option>
						  <option value="Dubai Duty Free" class="ng-binding ng-scope">Dubai Duty Free</option>
						  <option value="Essilor India" class="ng-binding ng-scope">Essilor India</option>
						  <option value="Fresh Leaf Foundation" class="ng-binding ng-scope">Fresh Leaf Foundation</option>
						  <option value="Fullerton India" class="ng-binding ng-scope">Fullerton India</option>
						  <option value="IDFC Bank" class="ng-binding ng-scope">IDFC Bank</option>
						  <option value="Johnson & Johnson" class="ng-binding ng-scope">Johnson & Johnson</option>
						  <option value="L&M" class="ng-binding ng-scope">L&M</option>
						  <option value="Larsen & Toubro" class="ng-binding ng-scope">Larsen & Toubro</option>
						  <option value="MECON Ltd" class="ng-binding ng-scope">MECON Ltd</option>
						  <option value="Meru" class="ng-binding ng-scope">Meru</option>
						  <option value="Novalis" class="ng-binding ng-scope">Novalis</option>
						  <option value="Oracle" class="ng-binding ng-scope">Oracle</option>
						  <option value="Piramal" class="ng-binding ng-scope">Piramal</option>
						  <option value="Qatar Foundation" class="ng-binding ng-scope">Qatar Foundation</option>
						  <option value="RAYBAN" class="ng-binding ng-scope">RAYBAN</option>
						  <option value="Rotork International - India" class="ng-binding ng-scope">Rotork International - India</option>
						  <option value="Rotork International - UK" class="ng-binding ng-scope">Rotork International - UK</option>
						  <option value="RPG" class="ng-binding ng-scope">RPG</option>
						  <option value="Steel Authority of India" class="ng-binding ng-scope">Steel Authority of India</option>
						  <option value="Sightsavers" class="ng-binding ng-scope">Sightsavers</option>
						  <option value="Standard Chartered" class="ng-binding ng-scope">Standard Chartered</option>
						  <option value="Titan Eye" class="ng-binding ng-scope">Titan Eye</option>
						  <option value="Vision Express" class="ng-binding ng-scope">Vision Express</option>
						  
                        </select>
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.supported_by" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Is this a special camp requested by funding agency</label>
                        <select class="form-control ng-pristine ng-untouched ng-valid ng-empty" id="fund_agency-ajax" name="fund_agency" ng-model="form.special_camp" aria-invalid="false"><option value="">Select</option>
                          <option value="yes">Yes</option>
                          <option value="no">No</option>
                        </select>
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.special_camp" aria-hidden="true"></p>
                    </div>
                  </div>
					<div class="row">
					<div class="col-md-6">
                      <div class="form-group">
                        <label>Type of location</label>
                        <select class="form-control ng-pristine ng-untouched ng-valid ng-empty" id="loc_type-ajax" name="loc_type" ng-model="form.special_camp" aria-invalid="false"><option value="">Select</option>
                          <option value="camps">Camps</option>
                          <option value="school">School</option>
                        </select>
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.special_camp" aria-hidden="true"></p>
                    </div>
					</div>

                  <div class="tab-theme">
                    <div class="tabs-container">
                      <div class="ng-isolate-scope">
                        <ul class="nav nav-tabs">
                        <li class="uib-tab nav-item ng-scope ng-isolate-scope active">
                          <a href="#tab1" data-toggle="tab" class="nav-link ng-binding">Screening</a>
                        </li>

                        <li  class="uib-tab nav-item ng-scope ng-isolate-scope" >
                          <a href="#tab2" data-toggle="tab" class="nav-link ng-binding">Refraction</a>
                        </li>

                        <li  class="uib-tab nav-item ng-scope ng-isolate-scope">
                          <a href="#tab3" data-toggle="tab" class="nav-link ng-binding">Referral</a>
                        </li>

                        <li class="uib-tab nav-item ng-scope ng-isolate-scope">
                          <a href="#tab4" data-toggle="tab" class="nav-link ng-binding">Treatment</a>
                        </li>

                        <li class="uib-tab nav-item ng-scope ng-isolate-scope">
                          <a href="#tab5" data-toggle="tab" class="nav-link ng-binding">DR</a>
                        </li>

                      </ul>
  <div class="tab-content">
    <!-- ngRepeat: tab in tabset.tabs -->
    <div class="tab-pane ng-scope active" id="tab1">

                          <div class="panel-body ng-scope">
                            <div ueh-base-hospital="" title="Total number persons screened" form="form.screened" errors="errors.screened" class="ng-isolate-scope"><div class="panel panel-default panel-tab-content">
  <div class="panel-heading ng-binding">
    Total number persons screened
  </div>
  <div class="panel-body" style="padding: 5px 20px 0px 20px;">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Adult</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Male</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="screen_male" ng-model="form.male" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.male" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Female</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="screen_female" ng-model="form.female" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.female" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Child</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Boy</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="screen_boy" ng-model="form.boy" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.boy" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Girl</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="screen_girl" ng-model="form.girl" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.girl" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
</div>
                          </div>
                        </div><!-- end ngRepeat: tab in tabset.tabs -->
                        <div class="tab-pane ng-scope" id="tab2">

                          <div class="panel-body ng-scope">
                            <div ueh-base-hospital="" title="Total number of persons refracted" form="form.refracted" errors="errors.refracted" class="ng-isolate-scope"><div class="panel panel-default panel-tab-content">
  <div class="panel-heading ng-binding">
    Total number of persons refracted
  </div>
  <div class="panel-body" style="padding: 5px 20px 0px 20px;">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Adult</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Male</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refra_refra_male" ng-model="form.male" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.male" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Female</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refra_refra_female" ng-model="form.female" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.female" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Child</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Boy</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refra_refra_boy" ng-model="form.boy" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.boy" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Girl</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refra_refra_girl" ng-model="form.girl" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.girl" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
</div>
                            <div ueh-base-hospital="" title="Total number of persons prescribed spectacles" form="form.prescribed_spectacles" errors="errors.prescribed_spectacles" class="ng-isolate-scope"><div class="panel panel-default panel-tab-content">
  <div class="panel-heading ng-binding">
    Total number of persons prescribed spectacles
  </div>
  <div class="panel-body" style="padding: 5px 20px 0px 20px;">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Adult</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Male</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refra_pres_male" ng-model="form.male" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.male" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Female</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refra_pres_female" ng-model="form.female" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.female" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Child</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Boy</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refra_pres_boy" ng-model="form.boy" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.boy" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Girl</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refra_pres_girl" ng-model="form.girl" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.girl" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
</div>
                            <div ueh-base-hospital="" title="Total number of persons dispensed spectacles" form="form.dispensed_spectacles" errors="errors.dispensed_spectacles" class="ng-isolate-scope"><div class="panel panel-default panel-tab-content">
  <div class="panel-heading ng-binding">
    Total number of persons dispensed spectacles for free
  </div>
  <div class="panel-body" style="padding: 5px 20px 0px 20px;">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Adult</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Male</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refra_disp_male" ng-model="form.male" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.male" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Female</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refra_disp_female" ng-model="form.female" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.female" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Child</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Boy</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refra_disp_boy" ng-model="form.boy" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.boy" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Girl</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refra_disp_girl" ng-model="form.girl" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.girl" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
</div>
<div ueh-base-hospital="" title="Total number of persons prescribed spectacles" form="form.prescribed_spectacles" errors="errors.prescribed_spectacles" class="ng-isolate-scope"><div class="panel panel-default panel-tab-content">
  <div class="panel-heading ng-binding">
    Total number of persons purchased spectacles
  </div>
  <div class="panel-body" style="padding: 5px 20px 0px 20px;">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Adult</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Male</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refra_purc_male" ng-model="form.male" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.male" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Female</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refra_purc_female" ng-model="form.female" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.female" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Child</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Boy</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refra_purc_child" ng-model="form.boy" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.boy" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Girl</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refra_purc_girl" ng-model="form.girl" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.girl" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
</div>


                          </div>
                        </div><!-- end ngRepeat: tab in tabset.tabs -->

                        <div class="tab-pane ng-scope" id="tab3">

                          <div class="panel-body ng-scope">
                            <div ueh-base-hospital="" title="Total number of persons referred" form="form.referred" errors="errors.referred" class="ng-isolate-scope"><div class="panel panel-default panel-tab-content">
  <div class="panel-heading ng-binding">
    Total number of persons referred
  </div>
  <div class="panel-body" style="padding: 5px 20px 0px 20px;">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Adult</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Male</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refer_refer_male" ng-model="form.male" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.male" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Female</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refer_refer_female" ng-model="form.female" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.female" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Child</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Boy</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refer_refer_boy" ng-model="form.boy" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.boy" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Girl</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refer_refer_girl" ng-model="form.girl" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.girl" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
</div>
                            <div ueh-base-hospital="" title="Of the total referred, Total number of persons referred for Cataract" form="form.referred_cataract" errors="errors.referred_cataract" class="ng-isolate-scope"><div class="panel panel-default panel-tab-content">
  <div class="panel-heading ng-binding">
    Of the total referred, Total number of persons referred for Cataract
  </div>
  <div class="panel-body" style="padding: 5px 20px 0px 20px;">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Adult</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Male</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refer_catar_male" ng-model="form.male" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.male" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Female</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refer_catar_female" ng-model="form.female" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.female" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Child</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Boy</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refer_catar_boy" ng-model="form.boy" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.boy" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Girl</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="refer_catar_girl" ng-model="form.girl" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.girl" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
</div>
                          </div>
                        </div><!-- end ngRepeat: tab in tabset.tabs -->

                        <div class="tab-pane ng-scope" id="tab4">

                          <div class="panel-body ng-scope">
                            <div ueh-base-hospital="" title="Total number of persons treated at camp site" form="form.treated" errors="errors.treated" class="ng-isolate-scope"><div class="panel panel-default panel-tab-content">
  <div class="panel-heading ng-binding">
    Total number of persons treated at camp site
  </div>
  <div class="panel-body" style="padding: 5px 20px 0px 20px;">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Adult</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Male</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="treat_male" ng-model="form.male" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.male" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Female</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="treat_female" ng-model="form.female" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.female" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Child</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Boy</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="treat_boy" ng-model="form.boy" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.boy" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Girl</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="treat_girl" ng-model="form.girl" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.girl" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
</div>
                          </div>
                        </div><!-- end ngRepeat: tab in tabset.tabs -->

                        <div class="tab-pane ng-scope" id="tab5">

                          <div class="panel-body ng-scope">
                            <div ueh-base-hospital="" title="Total number of persons tested for Diabetes" form="form.diabetes_tested" errors="errors.diabetes_tested" class="ng-isolate-scope"><div class="panel panel-default panel-tab-content">
  <div class="panel-heading ng-binding">
    Total number of persons tested for Diabetes
  </div>
  <div class="panel-body" style="padding: 5px 20px 0px 20px;">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Adult</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Male</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="dr_diabe_male" ng-model="form.male" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.male" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Female</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="dr_diabe_female" ng-model="form.female" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.female" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Child</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Boy</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="dr_diabe_boy" ng-model="form.boy" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.boy" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Girl</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="dr_diabe_girl" ng-model="form.girl" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.girl" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
</div>

                            <div ueh-base-hospital="" title="Total number of persons screened for DR" form="form.screened_dr" errors="errors.screened_dr" class="ng-isolate-scope"><div class="panel panel-default panel-tab-content">
  <div class="panel-heading ng-binding">
    Total number of persons screened for DR
  </div>
  <div class="panel-body" style="padding: 5px 20px 0px 20px;">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Adult</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Male</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="dr_screen_male" ng-model="form.male" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.male" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Female</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="dr_screen_female" ng-model="form.female" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.female" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Child</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Boy</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="dr_screen_boy" ng-model="form.boy" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.boy" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Girl</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="dr_screen_girl" ng-model="form.girl" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.girl" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
</div>

                            <div ueh-base-hospital="" title="Total number of persons screened for DR using innovative methods/technology/equipment" form="form.screened_dr_special" errors="errors.screened_dr_special" class="ng-isolate-scope"><div class="panel panel-default panel-tab-content">
  <div class="panel-heading ng-binding">
    Total number of persons screened for DR using innovative methods/technology/equipment
  </div>
  <div class="panel-body" style="padding: 5px 20px 0px 20px;">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Adult</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Male</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="dr_method_male" ng-model="form.male" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.male" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Female</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="dr_method_female" ng-model="form.female" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.female" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
          <div class="field-heading">
            <h4>Child</h4></div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Boy</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="dr_method_boy" ng-model="form.boy" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.boy" aria-hidden="true"></p>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Girl</label>
                <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="dr_method_girl" ng-model="form.girl" aria-invalid="false">
              </div>
              <p class="text-danger ng-binding ng-hide" ng-show="errors.girl" aria-hidden="true"></p>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
</div>
                          </div>
                        </div><!-- end ngRepeat: tab in tabset.tabs -->
  </div>
</div>
                    </div>
                  </div>


                </div>
                <div class="panel-footer">
                  <div class="flex-element  align-bw">
                    <div class="footer-text">
                      <h5>Please review your changes before you save them. Changes once saved cannot be
                reverted.</h5>
                    </div>
					<input type="hidden" name="month_data"/>
                    <div class="save-btn">
                      <button class="btn  btn-primary" type="submit">
                          Save Changes
                      </button>
                    </div>
                  </div>
                </div>
              </div>
			  <?php echo form_close();?>
			  <?php if(!empty($oc_data)){ ?>
              <div class="table-responsive report-table">
                <table class="table table-bordered">
                  <thead>
                    <tr class="tableizer-firstrow">
                      <th colspan="50" align="left">Outreach camp detail (please provide the consolidated numbers for each column)</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr class="label-bg">
                      <td width="200">&nbsp;</td>
					  <td width="200">&nbsp;</td>
                      <td colspan="4">Screening</td>
                      <td colspan="16">Refraction</td>
                      <td colspan="8">Referral</td>
                      <td colspan="4">Treatment</td>
                      <td colspan="12">DR</td>
                      <td colspan="4">Supported by</td>
					  
                    </tr>
                    <tr class="lead-bg">
                      <td>Name of the Camp site/ Location</td>
					  <td>Type of location</td>
                      <td colspan="4">Total number of person screened</td>
                      <td colspan="4">Total number of persons refracted</td>
                      <td colspan="4">Total number of persons prescribed spectacles</td>
                      <td colspan="4">Total number of persons dispensed spectacles</td>
					  <td colspan="4">Total number of persons purchased spectacles</td>
                      <td colspan="4">Total number of persons referred</td>
                      <td colspan="4">Of the total referred, Total number of persons referred for Cataract</td>
                      <td colspan="4">Total number of persons treated at camp site</td>
                      <td colspan="4">Total number of persons tested for Diabetes</td>
                      <td colspan="4">Total number of persons screened for DR</td>
                      <td colspan="4">Total number of persons screened for DR using innovative methods/technology/equipment</td>
                      <td width="32">Camp supported by</td>
                      <td width="34">Is this a special camp requested by funding agency</td>
					  <td>Action1</td>
			          <td>Action2</td>
                    </tr>
                    <tr class="lead-bg">
                      <td>&nbsp;</td>
					  <td>&nbsp;</td>
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                     <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
					  <td colspan="2"></td>
					  <td>&nbsp;</td>
			          <td>&nbsp;</td>
                    </tr>
                    <tr class="lead-bg">
                      <td>&nbsp;</td>
					  <td>&nbsp;</td>
                      <td width="31">Male</td>
                      <td width="44">Female</td>
                      <td width="31">Boys</td>
                      <td width="28">Girls</td>
                      <td width="31">Male</td>
                      <td width="44">Female</td>
                      <td width="31">Boys</td>
                      <td width="28">Girls</td>
                      <td width="31">Male</td>
                      <td width="44">Female</td>
                      <td width="31">Boys</td>
                      <td width="28">Girls</td>
                      <td width="31">Male</td>
                      <td width="44">Female</td>
                      <td width="31">Boys</td>
                      <td width="28">Girls</td>
                      <td width="31">Male</td>
                      <td width="44">Female</td>
                      <td width="31">Boys</td>
                      <td width="28">Girls</td>
                      <td width="31">Male</td>
                      <td width="44">Female</td>
                      <td width="31">Boys</td>
                      <td width="28">Girls</td>
                      <td width="31">Male</td>
                      <td width="44">Female</td>
                      <td width="31">Boys</td>
                      <td width="28">Girls</td>
                      <td width="31">Male</td>
                      <td width="44">Female</td>
                      <td width="31">Boys</td>
                      <td width="28">Girls</td>
                      <td width="31">Male</td>
                      <td width="44">Female</td>
                      <td width="31">Boys</td>
                      <td width="28">Girls</td>
                      <td width="31">Male</td>
                      <td width="44">Female</td>
                      <td width="31">Boys</td>
                      <td width="28">Girls</td>
                     <td width="31">Male</td>
                      <td width="44">Female</td>
                      <td width="31">Boys</td>
                      <td width="28">Girls</td>
					  <td width="31"></td>
                      <td width="28"></td>
					  <td>&nbsp;</td>
			          <td>&nbsp;</td>
                    </tr>
					<?php foreach($oc_data as $vdata){?>
					<tr class="label-bg">
						<td><?php echo $vdata->ss_name_cs;?></td>
						<td><?php echo $vdata->ss_loc_type;?></td>
						<td><?php echo $vdata->ss_screen_male;?></td>
						<td><?php echo $vdata->ss_screen_female;?></td>
						<td><?php echo $vdata->ss_screen_boy;?></td>
						<td><?php echo $vdata->ss_screen_girl;?></td>
						<td><?php echo $vdata->ss_refra_refra_male;?></td>
						<td><?php echo $vdata->ss_refra_refra_female;?></td>
						<td><?php echo $vdata->ss_refra_refra_boy;?></td>
						<td><?php echo $vdata->ss_refra_refra_girl;?></td>
						<td><?php echo $vdata->ss_refra_pres_male;?></td>
						<td><?php echo $vdata->ss_refra_pres_female;?></td>
						<td><?php echo $vdata->ss_refra_pres_boy;?></td>
						<td><?php echo $vdata->ss_refra_pres_girl;?></td>
						<td><?php echo $vdata->ss_refra_disp_male;?></td>
						<td><?php echo $vdata->ss_refra_disp_female;?></td>
						<td><?php echo $vdata->ss_refra_disp_boy;?></td>
						<td><?php echo $vdata->ss_refra_disp_girl;?></td>
						<td><?php echo $vdata->ss_refra_purc_male;?></td>
						<td><?php echo $vdata->ss_refra_purc_female;?></td>
						<td><?php echo $vdata->ss_refra_purc_child;?></td>
						<td><?php echo $vdata->ss_refra_purc_girl;?></td>
						<td><?php echo $vdata->ss_refer_refer_male;?></td>
						<td><?php echo $vdata->ss_refer_refer_female;?></td>
						<td><?php echo $vdata->ss_refer_refer_boy;?></td>
						<td><?php echo $vdata->ss_refer_refer_girl;?></td>
						<td><?php echo $vdata->ss_refer_catar_male;?></td>
						<td><?php echo $vdata->ss_refer_catar_female;?></td>
						<td><?php echo $vdata->ss_refer_catar_boy;?></td>
						<td><?php echo $vdata->ss_refer_catar_girl;?></td>
						<td><?php echo $vdata->ss_treat_male;?></td>
						<td><?php echo $vdata->ss_treat_female;?></td>
						<td><?php echo $vdata->ss_treat_boy;?></td>
						<td><?php echo $vdata->ss_treat_girl;?></td>
						<td><?php echo $vdata->ss_dr_diabe_male;?></td>
						<td><?php echo $vdata->ss_dr_diabe_female;?></td>
						<td><?php echo $vdata->ss_dr_diabe_boy;?></td>
						<td><?php echo $vdata->ss_dr_diabe_girl;?></td>
						<td><?php echo $vdata->ss_dr_screen_male;?></td>
						<td><?php echo $vdata->ss_dr_screen_female;?></td>						
						<td><?php echo $vdata->ss_dr_screen_boy;?></td>
						<td><?php echo $vdata->ss_dr_screen_girl;?></td>
						<td><?php echo $vdata->ss_dr_method_male;?></td>
						<td><?php echo $vdata->ss_dr_method_female;?></td>
						<td><?php echo $vdata->ss_dr_method_boy;?></td>
						<td><?php echo $vdata->ss_dr_method_girl;?></td>
                        <td><?php echo $vdata->ss_camp_support;?></td>						
						<td><?php echo $vdata->ss_fund_agency;?></td>	
                        <td><a onclick="return confirmDelete();" href="<?php echo BASE_URL.'delete_oc_ueh/'.base64_encode($vdata->ss_ueh_outreachcamp_id); ?>">Delete</a></td>
            <td><a href="javascript:void(0)" class="editThis" data-val="<?php echo base64_encode($vdata->ss_ueh_outreachcamp_id); ?>">Edit</a></td>						
					</tr>
				  <?php }?>
                   
                  </tbody>
                </table>
              </div>
			  <?php } ?>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
<script src="<?php echo PUBLIC_URL; ?>js/jquery-2.1.1.js"></script>
<script>

$(".editThis").click(function(){
	var dataid = $(this).attr('data-val');
	var cct = $("input[name='csrf_test_name']").val();
	//alert(cct);
	//var cct = $.cookie("<?php echo $this->config->item("csrf_cookie_name"); ?>");
	post_data ={ id: dataid,<?php echo $this->security->get_csrf_token_name(); ?> : cct};
	if(dataid!=0 || dataid !="")
	{
    $.ajax({ 
            url: "<?php echo BASE_URL.'ueh/edit_oc_ueh';?>",
            data: post_data,
            type: 'POST'
        }).done(function(responseData) { //alert(responseData); return false;
		
		var csrf_t = responseData.split("$$$");
		var obj = JSON.parse(csrf_t[0]);
       
		$("input[name='csrf_test_name']").val(csrf_t[1]);
	    
		$("input[name='month_from']").val(obj['date_month']);
		
		$("input[name='name_cs']").val(obj['ss_name_cs']);
		var camp_support_ajax = obj['ss_camp_support'];
		var fund_agency_ajax = obj['ss_fund_agency'];
		var loc_type_ajax = obj['ss_loc_type'];
		
	    		
		$("input[name='screen_male']").val(obj['ss_screen_male']);
		$("input[name='screen_female']").val(obj['ss_screen_female']);
		$("input[name='screen_boy']").val(obj['ss_screen_boy']);
		$("input[name='screen_girl']").val(obj['ss_screen_girl']);
		
		$("input[name='refra_refra_male']").val(obj['ss_refra_refra_male']);
		$("input[name='refra_refra_female']").val(obj['ss_refra_refra_female']);
		$("input[name='refra_refra_boy']").val(obj['ss_refra_refra_boy']);
		$("input[name='refra_refra_girl']").val(obj['ss_refra_refra_girl']);
		
		$("input[name='refra_pres_male']").val(obj['ss_refra_pres_male']);
		$("input[name='refra_pres_female']").val(obj['ss_refra_pres_female']);
		$("input[name='refra_pres_boy']").val(obj['ss_refra_pres_boy']);
		$("input[name='refra_pres_girl']").val(obj['ss_refra_pres_girl']);
		
		$("input[name='refra_disp_male']").val(obj['ss_refra_disp_male']);
		$("input[name='refra_disp_female']").val(obj['ss_refra_disp_female']);
		$("input[name='refra_disp_boy']").val(obj['ss_refra_disp_boy']);
		$("input[name='refra_disp_girl']").val(obj['ss_refra_disp_girl']);
		
		$("input[name='refra_purc_male']").val(obj['ss_refra_purc_male']);
		$("input[name='refra_purc_female']").val(obj['ss_refra_purc_female']);
		$("input[name='refra_purc_boy']").val(obj['ss_refra_purc_boy']);
		$("input[name='refra_purc_girl']").val(obj['ss_refra_purc_girl']);

		$("input[name='refer_refer_male']").val(obj['ss_refer_refer_male']);
		$("input[name='refer_refer_female']").val(obj['ss_refer_refer_female']);
		$("input[name='refer_refer_boy']").val(obj['ss_refer_refer_boy']);
		$("input[name='refer_refer_girl']").val(obj['ss_refer_refer_girl']);
		
		$("input[name='refer_catar_male']").val(obj['ss_refer_catar_male']);
		$("input[name='refer_catar_female']").val(obj['ss_refer_catar_female']);
		$("input[name='refer_catar_boy']").val(obj['ss_refer_catar_boy']);
		$("input[name='refer_catar_girl']").val(obj['ss_refer_catar_girl']);
		
		$("input[name='treat_male']").val(obj['ss_treat_male']);
		$("input[name='treat_female']").val(obj['ss_treat_female']);
		$("input[name='treat_boy']").val(obj['ss_treat_boy']);
		$("input[name='treat_girl']").val(obj['ss_treat_girl']);
		
		$("input[name='dr_diabe_male']").val(obj['ss_dr_diabe_male']);
		$("input[name='dr_diabe_female']").val(obj['ss_dr_diabe_female']);
		$("input[name='dr_diabe_boy']").val(obj['ss_dr_diabe_boy']);
		$("input[name='dr_diabe_girl']").val(obj['ss_dr_diabe_girl']);
		
		$("input[name='dr_screen_male']").val(obj['ss_dr_screen_male']);
		$("input[name='dr_screen_female']").val(obj['ss_dr_screen_female']);
		$("input[name='dr_screen_boy']").val(obj['ss_dr_screen_boy']);
		$("input[name='dr_screen_girl']").val(obj['ss_dr_screen_girl']);
		
		$("input[name='dr_method_male']").val(obj['ss_dr_method_male']);
		$("input[name='dr_method_female']").val(obj['ss_dr_method_female']);
		$("input[name='dr_method_boy']").val(obj['ss_dr_method_boy']);
		$("input[name='dr_method_girl']").val(obj['ss_dr_method_girl']);
		
		$("input[name='id_ajax']").val(obj['ss_ueh_outreachcamp_id']);
		
	// for fund_agency-ajax     
	$("#fund_agency-ajax option").each(function (key,value) { 
	   if(fund_agency_ajax==$(this).val())
	   {
		 $(this).prop('selected', true);  
	   }
	});  
	
	// camp_support-ajax     
	$("#camp_support-ajax option").each(function (key,value) { 
	   if(camp_support_ajax==$(this).val())
	   {
		 $(this).prop('selected', true);  
	   }
	});
	
	// for loc_type_ajax     
	$("#loc_type-ajax option").each(function (key,value) { 
	   if(loc_type_ajax==$(this).val())
	   {
		 $(this).prop('selected', true);  
	   }
	});
	
		
        console.log('Done: ', responseData);
        }).fail(function() {
            console.log('Failed');
        });
	}
})

</script>

<script type="text/javascript">
    function confirmDelete() 
	{
        return confirm('Do you really wants to delete?');
    }
</script>