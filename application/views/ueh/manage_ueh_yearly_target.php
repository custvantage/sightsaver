
<div class="row wrapper border-bottom white-bg page-heading ng-scope">
    <div class="col-lg-10">
        <h2><?php //echo @strtoupper($this->session->userdata['userinfo']['partner_name']); ?></h2>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeIn ng-scope" ng-init="init()">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <div class="row">
                        <div class="col-lg-6">
                            <h5>Urban Eye Health / Yearly Targets</h5>
                        </div>
						
                    </div>
                </div>
                <div class="ibox-content">
					
				
					<?php $form_attrib = array('id'=>'manage_ueh_yt_entry');
						echo form_open('manage_ueh_yt_entry/'.$this->uri->segment(2),$form_attrib);?>
					<div class="row flex-element center-both">
						<div class="well well-sm main-entry-box">
						  <div class="col-md-6">
							<select class="form-control" id="partner_name11" name="partner_name" onchange="partner_fill()">
							  <option value="">Select partner</option>
							  <?php if(!empty($partners)){ foreach($partners as $partner){?>
							  <option value="<?php echo $partner->ss_partners_id;?>"><?php echo $partner->ss_partners_name;?></option>
							  <?php } }?>
							</select>
						  </div>
						  <div class="col-md-4">
								<input type="text" name="month_from" id="month_from" class="form-control datepicker" onchange="date_fill()">
								<p class="text-danger ng-binding ng-hide" aria-hidden="true" id="error_month"><?php echo form_error('month_data');?></p>
						  </div>
						  <div class="col-md-2">
							<button class="btn btn-sm btn-primary btn-block" onclick="submit_form('manage_ueh_yt_entry');" type="button">
								Get Data
							</button>
						  </div>
						</div>
					</div>
					<?php echo form_close();?>
					

                    <div class="row" ng-show="hasData" aria-hidden="false" style="">
                        <div class="col-md-12">
                            <div class="tabs-container">
                                <div class="tabs-left ng-isolate-scope">
                                    <ul class="nav nav-tabs">
                                        <?php
                                        if (!empty($sections)) {
                                            foreach ($sections as $key_section => $section) {
                                                ?>
                                                <li class="uib-tab nav-item ng-scope ng-isolate-scope<?php if ($key_section == 0) { ?> active <?php } ?>">
                                                    <a href="#tab<?php echo $section->ss_section_layout_ss_section_layout_id; ?>" data-toggle="tab" class="nav-link ng-binding"><?php echo ucwords($section->ss_section_layout_section_name); ?></a>
                                                </li>
                                            <?php
                                            }
                                        }
                                        ?>
                                    </ul>
									<?php echo form_open('edit_form_data_reh_yearly/'.$this->uri->segment(2));	?>
                                    <div class="tab-content">
                                        <?php $section_id = ""; $j=0; //$counter=0;?>                                       
                                        <?php
                                        if (!empty($metrics_data)) {

                                            foreach ($metrics_data as $key_section_metric => $section_metric) {
												
                                                if ($section_id != $section_metric->ss_section_layout_ss_section_layout_id) {
                                                    if ($j != 0) { //$counter=0;
                                                        ?>
                                                        </tbody>
                                                        </table>
                                                    </div>
                                                   <input type="hidden" name="month_data"/>
													<input type="hidden" class="partner_name_h" name="partner_name_h"/>
                                                    <?php echo form_close();?>
                                                </div>
                                            </div>
                                         <?php } ?>
										
                                        <div class="tab-pane ng-scope<?php if ($section_id == "") { ?> active<?php } ?>" id="tab<?php echo $section_metric->ss_section_layout_ss_section_layout_id; ?>">

                                            <div class="panel-body ng-scope">
                                                <div class="bg-heading p-xs b-r-sm ng-scope"><i class="fa fa-arrow-right"></i><?php echo ucwords($section_metric->ss_section_layout_section_name); ?> </div>                                                       
                                                <h4 class="ng-scope">
                               <?php echo $section_metric->ss_section_layout_analysis_description; ?>
                                                </h4>                                                
                                                <div class="table-responsive ng-scope">
                                                    <table class="table table-bordered">
                                                        <thead>

                                                            <tr>
                                                                <?php if ($section_metric->ss_section_layout_section_col_count == 1 && $section_metric->ss_section_layout_analysis_description == "") { ?>
                                                                    <td>Metric</td>       
                                                                    <td>Value</td>
            <?php }
            elseif ($section_metric->ss_section_layout_section_col_count == 4 && $section_metric->ss_section_layout_analysis_description == "") { ?>
                                                                    <td>Metric</td>       
                                                                    <td>Men</td>
                                                                    <td>Women</td>
																	<td>Transgender</td>
                                                                    <td>Boys</td>
                                                                    <td>Girls</td>
            <?php }
            elseif ($section_metric->ss_section_layout_analysis_description != "") { ?>
                                                                    <td>Question</td>       
                                                                    <td>Response</td>
            <?php } ?>
                                                            </tr>

                                                        </thead>
                                                        <tbody>

                                                            <tr>
            <?php if ($section_metric->ss_section_layout_section_col_count == 1 && $section_metric->ss_section_layout_analysis_description == "") { ?>
                                                                    <td><?php echo $section_metric->ss_metric_master_description; ?>
																	<input type="hidden" name="matric_id_one_column[]" value="<?php echo $section_metric->ss_metric_master_id;?>"/>																	
                                                                    </td>
                                                                    <td><input type="text" name="one_column_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $one_col_val = $this->metricdata->getOneColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($one_col_val)){ echo $one_col_val;}?>"  aria-invalid="false"></td>
            <?php }
            elseif ($section_metric->ss_section_layout_section_col_count == 4 && $section_metric->ss_section_layout_analysis_description == "") { ?>
                                                                    <td><?php echo $section_metric->ss_metric_master_description; ?>
																	<input type="hidden" name="matric_id_four_column[]" value="<?php echo $section_metric->ss_metric_master_id;?>"/>																	
                                                                    </td>

                                                                    <td><input type="text" name="four_column_men_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $four_col_data = $this->metricdata->getFourColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($four_col_data->ss_four_column_data_value_men)){ echo $four_col_data->ss_four_column_data_value_men;}?>"  aria-invalid="false"></td>
                                                                    <td><input type="text" name="four_column_women_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $four_col_data = $this->metricdata->getFourColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($four_col_data->ss_four_column_data_value_women)){ echo $four_col_data->ss_four_column_data_value_women;}?>"  aria-invalid="false"></td>
																	<td><input type="text" name="four_column_trans_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $four_col_data = $this->metricdata->getFourColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($four_col_data->ss_four_column_data_value_trans)){ echo $four_col_data->ss_four_column_data_value_trans;}?>"  aria-invalid="false"></td>
                                                                    <td><input type="text" name="four_column_boy_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $four_col_data = $this->metricdata->getFourColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($four_col_data->ss_four_column_data_value_boys)){ echo $four_col_data->ss_four_column_data_value_boys;}?>"  aria-invalid="false"></td>
                                                                    <td><input type="text" name="four_column_girl_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $four_col_data = $this->metricdata->getFourColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($four_col_data->ss_four_column_data_value_girls)){
																	echo $four_col_data->ss_four_column_data_value_girls;}?>"  aria-invalid="false"></td>
                                                            <?php }
                                                            elseif ($section_metric->ss_section_layout_analysis_description != "") { ?>
                                                                    <td class="col-md-3"><?php echo $section_metric->ss_metric_master_description; ?>
																	<input type="hidden" name="matric_id_analysis[]" value="<?php echo $section_metric->ss_metric_master_id;?>"/>																	
                                                                    </td>
                                                                    <td class="col-md-9"><textarea class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false" name="analysis_value[]" ><?php $analysis_data = $this->metricdata->getAnalysisVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($analysis_data)){
																	echo $analysis_data;}?></textarea></td>                
                                                                <?php } ?>
                                                            </tr>
																
                                                   
            <?php $section_id = $section_metric->ss_section_layout_ss_section_layout_id;
			$j++;			
        }
        else {
            ?>
			</tbody>
                                                      
                                                            <tr>
            <?php if ($section_metric->ss_section_layout_section_col_count == 1 && $section_metric->ss_section_layout_analysis_description == "") { ?>
                                                                    <td><?php echo $section_metric->ss_metric_master_description; ?>
																	<input type="hidden" name="matric_id_one_column[]" value="<?php echo $section_metric->ss_metric_master_id;?>"/>																	
                                                                    </td>
                                                                    <td><input type="text" name="one_column_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $one_col_val = $this->metricdata->getOneColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($one_col_val)){ echo $one_col_val;}?>"  aria-invalid="false"></td>

                                                                <?php }
                                                                elseif ($section_metric->ss_section_layout_section_col_count == 4 && $section_metric->ss_section_layout_analysis_description == "") { ?>
                                                                    <td><?php echo $section_metric->ss_metric_master_description; ?>
																	<input type="hidden" name="matric_id_four_column[]" value="<?php echo $section_metric->ss_metric_master_id;?>"/>																	
                                                                    </td>

                                                                    <td><input type="text" name="four_column_men_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $four_col_data = $this->metricdata->getFourColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($four_col_data->ss_four_column_data_value_men)){ echo $four_col_data->ss_four_column_data_value_men;}?>"  aria-invalid="false"></td>
                                                                    <td><input type="text" name="four_column_women_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $four_col_data = $this->metricdata->getFourColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($four_col_data->ss_four_column_data_value_women)){ echo $four_col_data->ss_four_column_data_value_women;}?>" aria-invalid="false"></td>
																	<td><input type="text" name="four_column_trans_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $four_col_data = $this->metricdata->getFourColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($four_col_data->ss_four_column_data_value_trans)){ echo $four_col_data->ss_four_column_data_value_trans;}?>"  aria-invalid="false"></td>
                                                                    <td><input type="text" name="four_column_boy_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $four_col_data = $this->metricdata->getFourColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($four_col_data->ss_four_column_data_value_boys)){ echo $four_col_data->ss_four_column_data_value_boys;}?>"  aria-invalid="false"></td>
                                                                    <td><input type="text" name="four_column_girl_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $four_col_data = $this->metricdata->getFourColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($four_col_data->ss_four_column_data_value_girls)){
																	echo $four_col_data->ss_four_column_data_value_girls;}?>" aria-invalid="false"></td>
            <?php }
            elseif ($section_metric->ss_section_layout_analysis_description != "") { ?>
                                                                    <td class="col-md-3"><?php echo $section_metric->ss_metric_master_description; ?>
																	<input type="hidden" name="matric_id_analysis[]" value="<?php echo $section_metric->ss_metric_master_id;?>"/>																	
                                                                    </td>
                                                                    <td class="col-md-9"><textarea class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="analysis_value[]" aria-invalid="false" ><?php $analysis_data = $this->metricdata->getAnalysisVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($analysis_data)){
																	echo $analysis_data;}?></textarea></td>
                                        <?php } ?>
                                                            </tr>
            <?php } }
    ?> 
                                                </tbody>
                                            </table>
                                                    
                                        </div>
										
										<?php echo form_close();?>
                                    </div>
                                </div>
<?php } ?>
<?php if(!empty($metrics_data))
{?>
<?php if(isset($mpr_id) && !empty($mpr_id)){?>
<input type="hidden" name="mpr_summary_id" value="<?php echo $mpr_id;?>"/>
<?php }?>
<input type="hidden" name="mpr_summary_month" value="<?php echo $mpr_report_month;?>"/>
<div class="panel-body ng-scope">
	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-0 col-xs-0"></div>
		<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 text-right">		
			<input type="submit" class="btn btn-block btn-primary" name="update_form_data" value="Save"/>				
		</div>	
<?php } echo form_close();?>
<?php
if(isset($mpr_id) && !empty($mpr_id) && !empty($metrics_data))
{
echo form_open('submit_form_data/'.$this->uri->segment(2));	?>

		<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 text-right">
		<input type="hidden" name="mpr_summary_id" value="<?php echo $mpr_id;?>"/>
			<button class="btn btn-block btn-primary" type="submit">
				Submit
			</button>
		</div>
	</div>
</div>
<?php echo form_close(); }?> 
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</div>
</div>
</div>
<!---Comment Model----------------->
<div class="modal fade" id="comment-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    	<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header" align="center">
					<h3>Comments</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
					</button>
				</div>
                
                <!-- Begin # DIV Form -->
                <div id="div-forms">
                
                    <!-- Begin # Login Form -->
                    <?php $form_attrib = array('id'=>'comment-form');
					echo form_open();
					?>
		                <div class="modal-body" id="pmpo_modal_response">												
				    		<div class="popup-messages">								
    		                      <div class="direct-chat-messages" id="response_pmpo">
									<?php if(!empty($comments)){ foreach($comments as $comment){ ?>
                                        <div class="chat-box-single-line">
											<abbr class="timestamp"><?php echo $comment->created_date;?></abbr>
                                        </div>

										<div class="direct-chat-msg doted-border">
                                          <div class="direct-chat-info clearfix">
                                            <span class="direct-chat-name pull-left"><?php echo $comment->ss_partners_name;?></span>
                                          </div>
                                          
                                          <!--img alt="message user image" src="<?php //echo PUBLIC_URL;?>img/profile_small.jpg" class="direct-chat-img"-->
                                          <div class="direct-chat-text">
                                            <?php echo $comment->comment;?>
                                          </div>
                                          <div class="direct-chat-info clearfix">
                                            <span class="direct-chat-timestamp pull-right"><?php echo $comment->created_time;?></span>
                                          </div>
                                        </div>
									<?php } }?>
                                   </div>								                                    
                            </div>							
                            <div class="clearfix"></div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Type your Comment</label>
                                    <textarea class="form-control" maxlength="1000" id="pmpo_comments" name="pmpo_comments"></textarea>
                                </div>
                            </div>
                            <div class="clearfix"></div>
        		    	</div>
				        <div class="modal-footer">
                            
                            <div class="row">
                                
                                <div class="col-lf-offset-8 col-lg-4 col-md-offset-8 col-md-4">
                                    <button type="button" class="btn btn-primary btn-lg btn-block" onclick="comment_pmpo()">Submit Comment</button>
                                </div>
                            </div>
				    	    
				        </div>
					<input type="hidden" name="partner_id" id="partner_id" value="<?php echo $session_partner_id;?>"/>
                    <?php echo form_close();?>
                    <!-- End # Login Form -->
                    
                    
                    
                    
                    
                </div>
                <!-- End # DIV Form -->
                
			</div>
		</div>
	</div>
<!----------------PO/PM Comments Model---------------------->
<div class="modal fade" id="po_pm_comments">
<div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          <h3 class="modal-title">PO/PM Comments</h3>
        </div>
        <div class="modal-body">
          <table class="table table-striped" id="tblGrid">
            <thead id="tblHead">
              <tr>
                <th>Name</th>
                <th>Comments</th>
                <th class="text-right">Date</th>
              </tr>
            </thead>
            <tbody>
              <?php if(!empty($comments_pmpo)) { foreach($comments_pmpo as $key_comment => $comment_pmpo){?>
              <tr>
				<td><?php echo $comment_pmpo->ss_partners_name;?></td>
                <td>
					<a href="javascript:void(0)" onclick="show_full_comment(<?php echo $comment_pmpo->ss_comments_id;?>);">
					<?php if(strlen($comment_pmpo->comment) > 10)
					{
						$string = substr($comment_pmpo->comment,0,10).'...';
					}else{
						$string = $comment_pmpo->comment;
					}
					echo $string;
					?>
					</a>
					<input type="hidden" id="comment_<?php echo $comment_pmpo->ss_comments_id;?>" value="<?php echo $comment_pmpo->comment;?>"/>
				</td>
                <td class="text-right"><?php echo $comment_pmpo->created_on;?></td>
              </tr>
			<?php } }?>
            </tbody>
          </table>
          
		</div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary " data-dismiss="modal">Close</button>
        </div>
				
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
<!-----------------Approve Model---------------->
<div class="modal fade" id="approve-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    	<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header" align="center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h3 class="modal-title">Approve</h3>
				</div>
                
                <!-- Begin # DIV Form -->
                <div id="div-forms">
                
                    <!-- Begin # Login Form -->
                    <?php $form_attrib = array('id'=>'login-form');
					echo form_open();
					?>
		                <div class="modal-body">
				    		<div class="col-lg-12">
                                <div class="form-group">
                                    <h3>By Clicking on submit button, this data will be approved.</h3>
                                    
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Submit your feedback(if any)</label>
                                    <textarea class="form-control" id="approve_reason"></textarea>
                                </div>
                            </div>
                            <div class="clearfix"></div>
        		    	</div>
				        <div class="modal-footer">
                            
                            <div class="row">
                                
                                <div class="col-lf-offset-8 col-lg-4 col-md-offset-8 col-md-4">
                                    <button type="button" class="btn btn-primary btn-lg btn-block" onclick="approve();">Submit</button>
                                </div>
                            </div>
				    	    
				        </div>
					<input type="hidden" name="month_from_approve" id="month_from_approve" value="<?php echo $post_month_from;?>"/>	
                    <input type="hidden" name="partner_id_approve" id="partner_id_approve" value="<?php echo $session_partner_id;?>"/>
                    <?php echo form_close();?>
                    <!-- End # Login Form -->
                    
                    
                    
                    
                    
                </div>
                <!-- End # DIV Form -->
                
			</div>
		</div>
	</div>
<!-----------------Disapprove Model---------------->
<div class="modal fade" id="disapprove-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    	<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header" align="center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h3 class="modal-title">Disapprove</h3>
				</div>
                
                <!-- Begin # DIV Form -->
                <div id="div-forms">
                
                    <!-- Begin # Login Form -->
					<?php $form_attrib = array('id'=>'login-form');
					echo form_open();
					?>
		                <div class="modal-body">
				    		<div class="col-lg-12">
                                <div class="form-group">
                                    <h3>By Clicking on submit button, this data will be rejected.</h3>
                                    
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Submit your feedback(if any)</label>
                                    <textarea class="form-control" id="reject_reason"></textarea>
                                </div>
                            </div>
                            <div class="clearfix"></div>
        		    	</div>
				        <div class="modal-footer">
                            
                            <div class="row">
                                
                                <div class="col-lf-offset-8 col-lg-4 col-md-offset-8 col-md-4">
                                    <button type="button" class="btn btn-primary btn-lg btn-block" onclick="approve();">Submit</button>
                                </div>
                            </div>
				    	    
				        </div>
                    <?php echo form_close();?>
                    <!-- End # Login Form -->
                    
                    
                    
                    
                    
                </div>
                <!-- End # DIV Form -->
                
			</div>
		</div>
	</div>
<script>
function submit_form(form_name)
{
	if($("#month_from").val() == "")
	{
		$("#error_month").html("Please select month");
		$("#month_from").focus();
	}else{	
		$("#manage_ueh_yt_entry").submit();		
	}
}
function comment_pmpo()
{
	var comments = $("#pmpo_comments").val();
	var partner_id = $("#partner_id").val();
	var month_from = $("#month_from_approve").val();
	var project_name = "urban eye health";
	var module_name = "yearly target";
	if(partner_id != "" && comments != "")
	{
		var url = "<?php echo BASE_URL('create_pmpo_comment');?>";				 
		var csrfHash = $("input[name=csrf_test_name]").val();
		 
		$.post(url,{<?php echo $this->security->get_csrf_token_name(); ?>: csrfHash,comments:comments,partner_id:partner_id,month_from:month_from,project_name:project_name,module_name:module_name} ,function(response){
				
			var htmldata = "";
			var status = "";
			$("input[name=csrf_test_name]").val(response.csrfHash);
			if(response.success ==1)
			{
				htmldata += '<div class="chat-box-single-line"><abbr class="timestamp">'+response.created_on+'</abbr></div><div class="direct-chat-msg doted-border"><div class="direct-chat-info clearfix"><span class="direct-chat-name pull-left">'+response.partner_name+'</span></div><div class="direct-chat-text">'+response.comments+'</div><div class="direct-chat-info clearfix"><span class="direct-chat-timestamp pull-right">'+response.created_time+'</span></div></div>';
				
				
				$("#pmpo_comments").val("");
				$("#response_pmpo").append(htmldata);
				
			}else{
				$("#pmpo_comments").val("");
				alert("Data is not submitted");
			}			
		},"json");
	}
}
function approve()
{
	var month_from_approve = $("#month_from_approve").val();
	var partner_id_approve = $("#partner_id_approve").val();
	var project_name = "urban eye health";
	var module_name = "yearly target";
	var reason = "";
	var approve_flag = "";
	if($("#approve_reason").val() != "")
	{
		reason = $("#approve_reason").val();
		approve_flag = 1;
	}
	else if($("#reject_reason").val() != "")
	{
		reason = $("#reject_reason").val();
		approve_flag = 0;
	}	
	
	if(partner_id_approve != "" && month_from_approve != "")
	{
		var url = "<?php echo BASE_URL('approval');?>";				 
		var csrfHash = $("input[name=csrf_test_name]").val();
		 
		$.post(url,{<?php echo $this->security->get_csrf_token_name(); ?>: csrfHash,month_from_approve:month_from_approve,partner_id_approve:partner_id_approve,project_name:project_name,module_name:module_name,reason:reason,approve_flag:approve_flag} ,function(response){
				
			var htmldata = "";
			var status = "";
			$("input[name=csrf_test_name]").val(response.csrfHash);
			if(response.success ==1)
			{
				if(approve_flag == 1)
				{
					$("#approve_reason").val("");
					alert("Successfully Approved");					
				}else{
					$("#reject_reason").val("");
					alert("Rejected");
				}				
				
			}else{
				$("#approve_reason").val("");
				$("#reject_reason").val("");
				alert("No data found");
			}			
		},"json");
	}
}
</script>