<script>
        Highcharts.chart('plain_graph_1', {
    chart: {
        type: 'bar',
        height:200
    },
    title: {
        text: ''
    },
    
    xAxis: {
        type: 'category'
    },
     yAxis: {
            min: 0,

            title: {
                text: ''

            }

        },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            animation: {
                duration: 2000
            },
            borderWidth: 0,
            pointWidth: 10,
            pointStart: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y}%'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
    },

    series: [{
        type:'bar',
        name: 'Number of persons examined',
        colorByPoint: true,
        data: [{
            name: 'Target',
            y: 56.33
        }, {
            name: 'Actual',
            y: 24.03
        }, ]
    }],
});
    
    
    
    
    
    
    
    
        
		$(function () {
            var chart = Highcharts.chart('plain_graph_2', {
                colors: ['#ed7d31','#5b9bd5'],
                chart: {
                    
                    type: 'bar',
                    height:200
                },
                title: {
                    text: ''
                },

                subtitle: {
                    text: ''
                },
                xAxis: {
                        type: 'category'
                    },
                yAxis: {
                        min: 0,
                        
                        title: {
                            text: ''

                        }
                        
                    },
                legend: {
                    enabled: false
                },

        
                series: 
                [{
                    type:'bar',
                    name: 'Cataract operations',
                    colorByPoint: true,
                    data: [{
                        name: 'Target',
                        y: 158002
                    }, {
                        name: 'Actual',
                        y: 80442
                    }, ]
                }],
                plotOptions: {
                series: {
                    animation: {
                            duration: 2000
                        },
                    borderWidth: 0,
                    pointWidth: 10,
                    dataLabels: {
                        enabled: true
                    }
                }
            },


            });


        });
        $(function () {
            var chart = Highcharts.chart('plain_graph_3', {
                colors: ['#ed7d31','#5b9bd5'],
                chart: {
                    
                    type: 'bar',
                    height:200
                },
                title: {
                    text: ''
                },

                subtitle: {
                    text: ''
                },
                xAxis: {
                        type: 'category'
                    },
                yAxis: {
                        min: 0,
                        
                        title: {
                            text: ''

                        }
                        
                    },
                legend: {
                    enabled: false
                },

        
                series: 
                [{
                    type:'bar',
                    name: 'Cataract operations',
                    colorByPoint: true,
                    data: [{
                        name: 'Target',
                        y: 284858
                    }, {
                        name: 'Actual',
                        y: 329358
                    }, ]
                }],
                plotOptions: {
                series: {
                    animation: {
                            duration: 2000
                        },
                    borderWidth: 0,
                    pointWidth: 10,
                    dataLabels: {
                        enabled: true
                    }
                }
            },


            });


        });
		$(function () {
            var chart = Highcharts.chart('plain_graph_4', {
                colors: ['#ed7d31','#5b9bd5'],
                chart: {
                    
                    type: 'bar',
                    height:200
                },
                title: {
                    text: ''
                },

                subtitle: {
                    text: ''
                },
                xAxis: {
                        type: 'category'
                    },
                yAxis: {
                        min: 0,
                        
                        title: {
                            text: ''

                        }
                        
                    },
                legend: {
                    enabled: false
                },

        
                series: 
                [{
                    type:'bar',
                    name: 'Cataract operations',
                    colorByPoint: true,
                    data: [{
                        name: 'Target',
                        y: 146267
                    }, {
                        name: 'Actual',
                        y: 120670
                    }, ]
                }],
                plotOptions: {
                series: {
                    animation: {
                            duration: 2000
                        },
                    borderWidth: 0,
                    pointWidth: 10,
                    dataLabels: {
                        enabled: true
                    }
                }
            },


            });


        });
		$(function () {
            var chart = Highcharts.chart('plain_graph_5', {
                colors: ['#ed7d31','#5b9bd5'],
                chart: {
                    
                    type: 'bar',
                    height:200
                },
                title: {
                    text: ''
                },

                subtitle: {
                    text: ''
                },
                xAxis: {
                        type: 'category'
                    },
                yAxis: {
                        min: 0,
                        
                        title: {
                            text: ''

                        }
                        
                    },
                legend: {
                    enabled: false
                },

        
                series: 
                [{
                    type:'bar',
                    name: 'Cataract operations',
                    colorByPoint: true,
                    data: [{
                        name: 'Target',
                        y: 4247
                    }, {
                        name: 'Actual',
                        y: 25582
                    }, ]
                }],
                plotOptions: {
                series: {
                    animation: {
                            duration: 2000
                        },
                    borderWidth: 0,
                    pointWidth: 10,
                    dataLabels: {
                        enabled: true
                    }
                }
            },


            });


        });
		$(function () {
            var chart = Highcharts.chart('plain_graph_6', {
                colors: ['#ed7d31','#5b9bd5'],
                chart: {
                    
                    type: 'bar',
                    height:200
                },
                title: {
                    text: ''
                },

                subtitle: {
                    text: ''
                },
                xAxis: {
                        type: 'category'
                    },
                yAxis: {
                        min: 0,
                        
                        title: {
                            text: ''

                        }
                        
                    },
                legend: {
                    enabled: false
                },

        
                series: 
                [{
                    type:'bar',
                    name: 'Cataract operations',
                    colorByPoint: true,
                    data: [{
                        name: 'Target',
                        y: 4247
                    }, {
                        name: 'Actual',
                        y: 25582
                    }, ]
                }],
                plotOptions: {
                series: {
                    animation: {
                            duration: 2000
                        },
                    borderWidth: 0,
                    pointWidth: 10,
                    dataLabels: {
                        enabled: true
                    }
                }
            },


            });


        });
		
    </script>
    <script>
        $(function () {
            Highcharts.chart('graph2_1', {
                chart: {
                    zoomType: 'xy',
                    height:200
                },
                colors: ['#5b9bd5', '#ed7d31'],
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: [{
                    categories: ['Bihar', 'Chattisgarh', 'Delhi', 'Jharkhand', 'Madhya Pradesh', 'Odisha',
                        'Rajasthan', 'Utter Pradesh', 'West Bengal'],
                    crosshair: true
                }],
                yAxis: [{ 
                    min: 0,
                    labels: {
                        format: '{value}',
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    },
                    title: {
                        text: '',
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    }
                }, { min: 0,
                    title: {
                        text: '',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    labels: {
                        format: '{value}',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    opposite: true
                }],
                tooltip: {
                    shared: true
                },
                legend: {
                    layout: 'vertical',
                    align: 'left',
                    x: 120,
                    verticalAlign: 'bottom',
                    y: 100,
                    floating: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
                },
                series: [{
                    name: 'Actual',
                    type: 'column',
                    data: [50000, 0, 30000, 20000, 120000, 60000, 20000, 150000, 350000],
                    

                }, {
                    name: 'Target',
                    type: 'spline',
                    data: [70000, 0, 30000, 20000, 300000, 100000, 40000, 120000, 220000],
                    
                }],
                plotOptions: {
                series: {
                    animation: {
                            duration: 2000
                        },
                    borderWidth: 0,
                    pointWidth: 10,
                    dataLabels: {
                        enabled: true
                    }
                }
            }            });
        });
		
		$(function () {
            Highcharts.chart('graph2_2', {
                chart: {
                    zoomType: 'xy',
                    height:200
                },
                colors: ['#5b9bd5', '#ed7d31'],
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: [{
                    categories: ['Bihar', 'Chattisgarh', 'Delhi', 'Jharkhand', 'Madhya Pradesh', 'Odisha',
                        'Rajasthan', 'Utter Pradesh', 'West Bengal'],
                    crosshair: true
                }],
                yAxis: [{ 
                    min: 0,
                    labels: {
                        format: '{value}',
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    },
                    title: {
                        text: '',
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    }
                }, { min: 0,
                    title: {
                        text: '',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    labels: {
                        format: '{value}',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    opposite: true
                }],
                tooltip: {
                    shared: true
                },
                legend: {
                    layout: 'vertical',
                    align: 'left',
                    x: 120,
                    verticalAlign: 'bottom',
                    y: 100,
                    floating: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
                },
                series: [{
                    name: 'Actual',
                    type: 'column',
                    data: [7000, 0, 1000, 3500, 7000, 6000, 1500, 35000, 16000],
                    

                }, {
                    name: 'Target',
                    type: 'spline',
                    data: [14000, 0, 1000, 5000, 18000, 13000, 4000, 33000, 17000],
                    
                }],
                plotOptions: {
                series: {
                    animation: {
                            duration: 2000
                        },
                    borderWidth: 0,
                    pointWidth: 10,
                    dataLabels: {
                        enabled: true
                    }
                }
            }
            });
        });
		$(function () {
            Highcharts.chart('graph2_3', {
                chart: {
                    zoomType: 'xy',
                    height:200
                },
                colors: ['#5b9bd5', '#ed7d31'],
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: [{
                    categories: ['Bihar', 'Chattisgarh', 'Delhi', 'Jharkhand', 'Madhya Pradesh', 'Odisha',
                        'Rajasthan', 'Utter Pradesh', 'West Bengal'],
                    crosshair: true
                }],
                yAxis: [{ 
                    min: 0,
                    labels: {
                        format: '{value}',
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    },
                    title: {
                        text: '',
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    }
                }, { min: 0,
                    title: {
                        text: '',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    labels: {
                        format: '{value}',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    opposite: true
                }],
                tooltip: {
                    shared: true
                },
                legend: {
                    layout: 'vertical',
                    align: 'left',
                    x: 120,
                    verticalAlign: 'bottom',
                    y: 100,
                    floating: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
                },
                series: [{
                    name: 'Actual',
                    type: 'column',
                    data: [15000, 0, 10000, 12000, 9000, 20000, 12000, 8800, 8000],
                    

                }, {
                    name: 'Target',
                    type: 'spline',
                    data: [22000, 0, 10000, 6000, 6200, 10000, 10000, 2800, 4500],
                    
                }],
                plotOptions: {
                series: {
                    animation: {
                            duration: 2000
                        },
                    borderWidth: 0,
                    pointWidth: 10,
                    dataLabels: {
                        enabled: true
                    }
                }
            }
            });
        });
		
		$(function () {
            Highcharts.chart('graph2_4', {
                chart: {
                    zoomType: 'xy',
                    height:200
                },
                colors: ['#5b9bd5', '#ed7d31'],
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: [{
                    categories: ['Bihar', 'Chattisgarh', 'Delhi', 'Jharkhand', 'Madhya Pradesh', 'Odisha',
                        'Rajasthan', 'Utter Pradesh', 'West Bengal'],
                    crosshair: true
                }],
                yAxis: [{ 
                    min: 0,
                    labels: {
                        format: '{value}',
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    },
                    title: {
                        text: '',
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    }
                }, { min: 0,
                    title: {
                        text: '',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    labels: {
                        format: '{value}',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    opposite: true
                }],
                tooltip: {
                    shared: true
                },
                legend: {
                    layout: 'vertical',
                    align: 'left',
                    x: 120,
                    verticalAlign: 'bottom',
                    y: 100,
                    floating: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
                },
                series: [{
                    name: 'Actual',
                    type: 'column',
                    data: [4000, 0, 1400, 3800, 9000, 10000, 4000, 48000, 26000],
                    

                }, {
                    name: 'Target',
                    type: 'spline',
                    data: [18000, 0, 1500, 3200, 28000, 10000, 10000, 53000, 20000],
                    
                }],
                plotOptions: {
                series: {
                    animation: {
                            duration: 2000
                        },
                    borderWidth: 0,
                    pointWidth: 10,
                    dataLabels: {
                        enabled: true
                    }
                }
            }
            });
        });
		
		
		$(function () {
            Highcharts.chart('graph2_5', {
                chart: {
                    zoomType: 'xy',
                    height:200
                },
                colors: ['#5b9bd5', '#ed7d31'],
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: [{
                    categories: ['Bihar', 'Chattisgarh', 'Delhi', 'Jharkhand', 'Madhya Pradesh', 'Odisha',
                        'Rajasthan', 'Utter Pradesh', 'West Bengal'],
                    crosshair: true
                }],
                yAxis: [{ 
                    min: 0,
                    labels: {
                        format: '{value}',
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    },
                    title: {
                        text: '',
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    }
                }, { min: 0,
                    title: {
                        text: '',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    labels: {
                        format: '{value}',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    opposite: true
                }],
                tooltip: {
                    shared: true
                },
                legend: {
                    layout: 'vertical',
                    align: 'left',
                    x: 120,
                    verticalAlign: 'bottom',
                    y: 100,
                    floating: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
                },
                series: [{
                    name: 'Actual',
                    type: 'column',
                    data: [6200, 0, 0, 600, 15000, 1900, 0, 0, 0],
                    

                }, {
                    name: 'Target',
                    type: 'spline',
                    data: [13000, 0, 0, 1800, 8200, 2400, 0, 0, 0],
                    
                }],
                plotOptions: {
                series: {
                    animation: {
                            duration: 2000
                        },
                    borderWidth: 0,
                    pointWidth: 10,
                    dataLabels: {
                        enabled: true
                    }
                }
            }
            });
        });
		
		
		$(function () {
            Highcharts.chart('graph2_6', {
                chart: {
                    zoomType: 'xy',
                    height:200
                },
                colors: ['#5b9bd5', '#ed7d31'],
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: [{
                    categories: ['Bihar', 'Chattisgarh', 'Delhi', 'Jharkhand', 'Madhya Pradesh', 'Odisha',
                        'Rajasthan', 'Utter Pradesh', 'West Bengal'],
                    crosshair: true
                }],
                yAxis: [{ 
                    min: 0,
                    labels: {
                        format: '{value}',
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    },
                    title: {
                        text: '',
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    }
                }, { min: 0,
                    title: {
                        text: '',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    labels: {
                        format: '{value}',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    opposite: true
                }],
                tooltip: {
                    shared: true
                },
                legend: {
                    layout: 'vertical',
                    align: 'left',
                    x: 120,
                    verticalAlign: 'bottom',
                    y: 100,
                    floating: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
                },
                series: [{
                    name: 'Actual',
                    type: 'column',
                    data: [80, 100, 0, 450, 1450, 750, 180, 300, 50],
                    

                }, {
                    name: 'Target',
                    type: 'spline',
                    data: [900, 600, 0, 800, 1400, 400, 420, 700, 80],
                    
                }],
                plotOptions: {
                series: {
                    animation: {
                            duration: 2000
                        },
                    borderWidth: 0,
                    pointWidth: 10,
                    dataLabels: {
                        enabled: true
                    }
                }
            }
            });
        });
		
    </script>
    <script>
        
        $(function () {
            Highcharts.chart('graph3_1', {
                chart: {
                    height:300
                },
                colors: ['#5899d4', '#f0904f','#a3a3a3', '#ffbc00','#3f6ec2', '#66a739','#17538a', '#595959','#966f00'],
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                        'Jul', 'Aug', '(blank)']
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ''
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    valueSuffix: '	'
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },
                series: [{
                    name: 'Bihar',
                    data: [5000, 10000, 6000, 4000, 5000, 3000, 6000, 14000, 0]
                }, {
                    name: 'Chhattisgarh',
                    data: [0, 0, 0, 0, 0, 0, 0, 0, 0]
                }, {
                    name: 'Delhi',
                    data: [2000, 0, 3000, 6000, 2000, 1000, 4000, 2000, 0]
                }, {
                    name: 'Jharkhand',
                    data: [3000, 2000, 1500, 2000, 2500, 1800, 1200, 3000, 0]
                },
                        {
                    name: 'Madhya Pradesh',
                    data: [10000, 18000, 28000, 10000, 12000, 15000, 14000, 16000, 0]
                },
                        {
                    name: 'Odisha',
                    data: [12000, 6000, 4000, 5000, 2000, 2800, 3600, 2000, 0]
                },
                        {
                    name: 'Rajasthan',
                    data: [0, 0, 0, 1000, 2000, 3100, 4100, 5000, 0]
                },
                        {
                    name: 'Utter Pradesh',
                    data: [43000, 42000, 40000, 4000, 35000, 40000, 41000, 48000, 0]
                },
                        {
                    name: 'West Bengal',
                    data: [40000, 43000, 45000, 31000, 35000, 40000, 43000, 45000, 0]
                }],
                plotOptions: {
                series: {
                    animation: {
                            duration: 2000
                        },
                    borderWidth: 0,
                    pointWidth: 10
                }
            }
            });
        });
		
		$(function () {
            Highcharts.chart('graph3_2', {
                chart: {
                    height:300
                },
                colors: ['#5899d4', '#f0904f','#a3a3a3', '#ffbc00','#3f6ec2', '#66a739','#17538a', '#595959','#966f00'],
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                        'Jul', 'Aug', '(blank)']
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ''
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    valueSuffix: '	'
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },
                series: [{
                    name: 'Bihar',
                    data: [1200, 1300, 1100, 800, 900, 1000, 1000, 400, 0]
                }, {
                    name: 'Chhattisgarh',
                    data: [0, 0, 0, 0, 0, 0, 0, 0, 0]
                }, {
                    name: 'Delhi',
                    data: [2000, 0, 3000, 6000, 2000, 1000, 4000, 2000, 0]
                }, {
                    name: 'Jharkhand',
                    data: [3000, 2000, 1500, 2000, 2500, 1800, 1200, 3000, 0]
                },
                        {
                    name: 'Madhya Pradesh',
                    data: [1500, 3600, 2400, 1200, 1500, 2400, 800, 1400, 0]
                },
                        {
                    name: 'Odisha',
                    data: [600, 1400, 1700, 2100, 2500, 1300, 2900, 1400, 0]
                },
                        {
                    name: 'Rajasthan',
                    data: [0, 0, 0, 1000, 2000, 3100, 4100, 5000, 0]
                },
                        {
                    name: 'Utter Pradesh',
                    data: [7800, 7200, 3500, 6200, 2880, 2400, 2200, 3000, 0]
                },
                        {
                    name: 'West Bengal',
                    data: [1800, 1250, 1600, 800, 750, 900, 1300, 500, 0]
                }],
                plotOptions: {
                series: {
                    animation: {
                            duration: 2000
                        },
                    borderWidth: 0,
                    pointWidth: 10
                }
            }
            });
        });
		
		$(function () {
            Highcharts.chart('graph3_3', {
                chart: {
                    height:300
                },
                colors: ['#5899d4', '#f0904f','#a3a3a3', '#ffbc00','#3f6ec2', '#66a739','#17538a', '#595959','#966f00'],
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                        'Jul', 'Aug', '(blank)']
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ''
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    valueSuffix: '	'
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },
                series: [{
                    name: 'Bihar',
                    data: [1200, 1300, 1100, 800, 900, 1000, 1000, 400, 0]
                }, {
                    name: 'Chhattisgarh',
                    data: [0, 0, 0, 0, 0, 0, 0, 0, 0]
                }, {
                    name: 'Delhi',
                    data: [2000, 0, 3000, 6000, 2000, 1000, 4000, 2000, 0]
                }, {
                    name: 'Jharkhand',
                    data: [3000, 2000, 1500, 2000, 2500, 1800, 1200, 3000, 0]
                },
                        {
                    name: 'Madhya Pradesh',
                    data: [1500, 3600, 2400, 1200, 1500, 2400, 800, 1400, 0]
                },
                        {
                    name: 'Odisha',
                    data: [600, 1400, 1700, 2100, 2500, 1300, 2900, 1400, 0]
                },
                        {
                    name: 'Rajasthan',
                    data: [0, 0, 0, 1000, 2000, 3100, 4100, 5000, 0]
                },
                        {
                    name: 'Utter Pradesh',
                    data: [7800, 7200, 3500, 6200, 2880, 6800, 9800, 3000, 0]
                },
                        {
                    name: 'West Bengal',
                    data: [5000, 5200, 16000, 8000, 7500, 9000, 13000, 5000, 0]
                }],
                plotOptions: {
                series: {
                    animation: {
                            duration: 2000
                        },
                    borderWidth: 0,
                    pointWidth: 10
                }
            }
            });
        });
		
		$(function () {
            Highcharts.chart('graph3_4', {
                chart: {
                    height:300
                },
                colors: ['#5899d4', '#f0904f','#a3a3a3', '#ffbc00','#3f6ec2', '#66a739','#17538a', '#595959','#966f00'],
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                        'Jul', 'Aug', '(blank)']
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ''
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    valueSuffix: '	'
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },
                series: [{
                    name: 'Bihar',
                    data: [1200, 1300, 1100, 800, 900, 1000, 1000, 400, 0]
                }, {
                    name: 'Chhattisgarh',
                    data: [0, 0, 0, 0, 0, 0, 0, 0, 0]
                }, {
                    name: 'Delhi',
                    data: [2000, 0, 3000, 6000, 2000, 1000, 4000, 2000, 0]
                }, {
                    name: 'Jharkhand',
                    data: [3000, 2000, 1500, 2000, 2500, 1800, 1200, 3000, 0]
                },
                        {
                    name: 'Madhya Pradesh',
                    data: [1500, 3600, 2400, 1200, 1500, 2400, 800, 1400, 0]
                },
                        {
                    name: 'Odisha',
                    data: [600, 1400, 1700, 2100, 2500, 1300, 2900, 1400, 0]
                },
                        {
                    name: 'Rajasthan',
                    data: [0, 0, 0, 1000, 2000, 3100, 4100, 5000, 0]
                },
                        {
                    name: 'Utter Pradesh',
                    data: [7800, 7200, 3500, 6200, 2880, 6800, 5800, 3000, 0]
                },
                        {
                    name: 'West Bengal',
                    data: [5000, 5200, 8200, 6000, 7500, 4200, 2200, 1300, 0]
                }],
                plotOptions: {
                series: {
                    animation: {
                            duration: 2000
                        },
                    borderWidth: 0,
                    pointWidth: 10
                }
            }
            });
        });
		
		$(function () {
            Highcharts.chart('graph3_5', {
                chart: {
                    height:300
                },
                colors: ['#5899d4', '#f0904f','#a3a3a3', '#ffbc00','#3f6ec2', '#66a739','#17538a', '#595959','#966f00'],
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                        'Jul', 'Aug', '(blank)']
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ''
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    valueSuffix: '	'
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },
                series: [{
                    name: 'Bihar',
                    data: [1200, 1300, 1100, 800, 900, 1000, 1000, 400, 0]
                }, {
                    name: 'Chhattisgarh',
                    data: [0, 0, 0, 0, 0, 0, 0, 0, 0]
                }, {
                    name: 'Delhi',
                    data: [2000, 0, 3000, 6000, 2000, 1000, 4000, 2000, 0]
                }, {
                    name: 'Jharkhand',
                    data: [3000, 2000, 1500, 2000, 2500, 1800, 1200, 3000, 0]
                },
                        {
                    name: 'Madhya Pradesh',
                    data: [1500, 3600, 2400, 1200, 1500, 2400, 800, 1400, 0]
                },
                        {
                    name: 'Odisha',
                    data: [600, 1400, 1700, 2100, 2500, 1300, 2900, 1400, 0]
                },
                        {
                    name: 'Rajasthan',
                    data: [0, 0, 0, 1000, 2000, 3100, 4100, 5000, 0]
                },
                        {
                    name: 'Utter Pradesh',
                    data: [7800, 7200, 3500, 6200, 2880, 6800, 5800, 3000, 0]
                },
                        {
                    name: 'West Bengal',
                    data: [5000, 5200, 8200, 6000, 7500, 4200, 2200, 1300, 0]
                }],
                plotOptions: {
                series: {
                    animation: {
                            duration: 2000
                        },
                    borderWidth: 0,
                    pointWidth: 10
                }
            }
            });
        });
		
		$(function () {
            Highcharts.chart('graph3_6', {
                chart: {
                    height:300
                },
                colors: ['#5899d4', '#f0904f','#a3a3a3', '#ffbc00','#3f6ec2', '#66a739','#17538a', '#595959','#966f00'],
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                        'Jul', 'Aug', '(blank)']
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ''
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    valueSuffix: '	'
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },
                series: [{
                    name: 'Bihar',
                    data: [120, 130, 110, 80, 90, 100, 100, 400, 0]
                }, {
                    name: 'Chhattisgarh',
                    data: [0, 0, 0, 0, 0, 0, 0, 0, 0]
                }, {
                    name: 'Delhi',
                    data: [200, 0, 300, 600, 200, 100, 400, 200, 0]
                }, {
                    name: 'Jharkhand',
                    data: [300, 200, 150, 200, 250, 180, 120, 300, 0]
                },
                        {
                    name: 'Madhya Pradesh',
                    data: [150, 360, 240, 120, 150, 240, 80, 140, 0]
                },
                        {
                    name: 'Odisha',
                    data: [60, 140, 170, 210, 250, 130, 290, 140, 0]
                },
                        {
                    name: 'Rajasthan',
                    data: [0, 0, 0, 100, 200, 310, 410, 500, 0]
                },
                        {
                    name: 'Utter Pradesh',
                    data: [780, 720, 350, 620, 288, 680, 580, 300, 0]
                },
                        {
                    name: 'West Bengal',
                    data: [500, 520, 820, 600, 750, 420, 220, 130, 0]
                }],
                plotOptions: {
                series: {
                    animation: {
                            duration: 2000
                        },
                    borderWidth: 0,
                    pointWidth: 10
                    
                }
            }
            });
        });
		
    </script>
<script>
	function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('glyphicon-plus glyphicon-minus');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>