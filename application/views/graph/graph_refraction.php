<script>
$(function () {
			var chart = Highcharts.chart('plain_graph_3', {
			colors: ['#ed7d31', '#5b9bd5'],
			chart: {
				type: 'bar',
				height:300
			},
			title: {
				text: ''
			},			
			xAxis: {
			type: 'category'
			},
			yAxis: {
				min: 0,
				title: {
                text: ''
				}
			},			
			legend: {
			enabled: false
			},
			plotOptions: {
				series: {
					animation: {
						duration: 2000
					},
				borderWidth: 0,
				pointWidth: 10,
				pointStart: 0,
					dataLabels: {
						enabled: true,
						format: '{point.y}'
					}
				}
			},

			tooltip: {
				headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
				pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
			},
			
			series: [{
				type:'bar',
				name: 'Number of persons examined',
				colorByPoint: true,
				data: [{
					name: 'Target',
					y: <?php echo @$totaldata1;?>
				}, {
					name: 'Actual',
					y: <?php echo @$totaldata;?>
				}, ]
			}],

			});


			});



</script>
<script>
$(function () {
            Highcharts.chart('graph2_3', {
                chart: {
                    zoomType: 'xy',
                    height:300
                },
                colors: ['#5b9bd5', '#ed7d31'],
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: [{
                    categories: [<?php if($graph_state_main){ $state_name = ""; foreach($graph_state_main as $graph_state){ 
					$state_name .= "'".$graph_state->ss_states_name."'".",";
					}echo rtrim($state_name,","); }?>],
                    crosshair: true
                }],				
				yAxis: [{ 
                    min: 0,
                    labels: {
                        format: '{value}',
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    },
                    title: {
                        text: '',
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    }
                }, { min: 0,
                    title: {
                        text: '',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    labels: {
                        format: '{value}',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    opposite: true
                }],
                tooltip: {
                    shared: true
                },
                legend: {
                    layout: 'vertical',
                    align: 'left',
                    x: 120,
                    verticalAlign: 'bottom',
                    y: 100,
                    floating: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
                },				
				series: [{
                    name: 'Actual',
                    type: 'column',
                    data: [<?php if($graph_state_main){ $total_sum=""; foreach($graph_state_main as $graph_state){ 
					$total_sum .= ($graph_state->four_column_men + $graph_state->four_column_women + $graph_state->four_column_boys + $graph_state->four_column_girl).",";
					
					} echo rtrim($total_sum,","); }?>],
                    

                }, {
                    name: 'Target',
                    type: 'spline',
                    data: [<?php if($graph_state_yearly){  $total_sum_y=""; foreach($graph_state_yearly as $graph_state_y){ 
					$total_sum_y .= ($graph_state_y->four_column_men + $graph_state_y->four_column_women + $graph_state_y->four_column_boys + $graph_state_y->four_column_girl).",";
					
					} $total_sum_y =  rtrim($total_sum_y,",");  $total_sum_ys = explode(',',$total_sum_y); $totalfinal =""; foreach($total_sum_ys as $total) { $totalfinal.= round(($total/12)*$selectedmonth)."," ;}  echo rtrim($totalfinal,","); } else { echo 0; } ?>],
                    
                }],
				 plotOptions: {
                series: {
                    animation: {
                            duration: 2000
                        },
                    borderWidth: 0,
                    pointWidth: 10,
                    dataLabels: {
                        enabled: true
                    }
                }
            }
            });
        });
</script>
<script>
$(function () {
            Highcharts.chart('graph3_3', {
                chart: {
                    height:300
                },
                colors: ['#5899d4', '#f0904f','#a3a3a3', '#ffbc00','#3f6ec2', '#66a739','#17538a', '#595959','#966f00'],
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: ['January','February','March','April','May','June','July','August','September','October','November','December']
                },
				yAxis: {
                    min: 0,
                    title: {
                        text: ''
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    valueSuffix: '	'
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },                
				series: [<?php if($graph_monthly_main){ 
					$total_sum_monthly="";
					$full_month_name = array('January','February','March','April','May','June','July','August','September','October','November','December');
					$repeted_state = "";
					//$uni = arr_unique($graph_monthly_main);
					//$graph_monthly_main = $uni;
					
					foreach($graph_monthly_main as $key => $graph_monthly_m){
					
					if(count($graph_monthly_main) != $key){?>{ <?php } ?>name: <?php echo "'".$graph_monthly_m->ss_states_name."'";?>,
                    data: [ <?php 
						for($i=0;$i<12;$i++)
						//echo $graph_monthly_main[$key]->four_column_data_value_men.",";
						{
							if($graph_monthly_m->month == $full_month_name[$i])
							{
							//echo $graph_monthly_main[$key]->four_column_data_value_men.",";
							
						$total_sum_monthly .= ($graph_monthly_main[$key]->four_column_data_value_men + $graph_monthly_main[$key]->four_column_data_value_women + $graph_monthly_main[$key]->four_column_data_value_boys + $graph_monthly_main[$key]->four_column_data_value_girls).",";
						}else{
							$total_sum_monthly .= "0,";
						}
						echo $total_sum_monthly;
						$total_sum_monthly = "";
						}?> ]
                 }<?php if(count($graph_monthly_main) != $key){?>,<?php } } }?>  
				 ],
				plotOptions: {
					series: {
						animation: {
								duration: 2000
							},
						borderWidth: 0,
						pointWidth: 10
					}
				}
            });
        });
</script>