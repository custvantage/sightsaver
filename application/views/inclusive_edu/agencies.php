<div class="row wrapper border-bottom white-bg page-heading ng-scope">
  <div class="col-lg-10">
    <h2>Social Inclusion Agencies Support Entry</h2>
  </div>
</div>

<div class="wrapper wrapper-content animated fadeIn ng-scope" ng-init="init()">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox">
        <div class="ibox-title">
          <h5>Agencies Support Entry</h5>
        </div>
        <div class="ibox-content">
        <?php echo form_open('filter_agencies_si');?>
          <div class="row flex-element center-both">
            <div class="well well-sm main-entry-box">
              <div class="col-md-6">
                <label>Partner:</label> <label><?php echo @strtoupper($this->session->userdata['userinfo']['partner_name']); ?></label>
              </div>
              <div class="col-md-4">
               <input type="text" name="month_from" id="month_from" class="form-control datepicker" onchange="date_fill()">
			   <p class="text-danger ng-binding ng-hide" aria-hidden="true"><?php echo form_error('month_data');?>
				<?php echo form_error('month_from');?>
			   </p>
              </div>
              <div class="col-md-2">
                <button class="btn btn-sm btn-primary btn-block" type="submit">
                Get Data
              </button>
              </div>
            </div>
          </div>
          <?php echo form_close();?>
          
          

          <div class="row m-t-lg" ng-show="hasData" aria-hidden="false" style="">
            <div class="col-md-12">
              <div class="panel panel-primary">
                <div class="panel-heading">
                  Agencies Support Entry
                </div>
                <div class="panel-body">
                  <div class="row">
				  <?php echo form_open('create_agencies_si');?>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Agency Name</label> <input type="text" name="name" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.agency_name" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.agency_name" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Agency Type</label>
                        <select name="type" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.agency_type" aria-invalid="false"><option value="? undefined:undefined ?"></option>
                          <!-- ngRepeat: type in agency_types --><option ng-repeat="type in agency_types" value="Vocational Training" class="ng-binding ng-scope">Vocational Training</option><!-- end ngRepeat: type in agency_types --><option ng-repeat="type in agency_types" value="Employment Support" class="ng-binding ng-scope">Employment Support</option><!-- end ngRepeat: type in agency_types --><option ng-repeat="type in agency_types" value="Product Marketing" class="ng-binding ng-scope">Product Marketing</option><!-- end ngRepeat: type in agency_types --><option ng-repeat="type in agency_types" value="Skill Training" class="ng-binding ng-scope">Skill Training</option><!-- end ngRepeat: type in agency_types --><option ng-repeat="type in agency_types" value="Financial assistance" class="ng-binding ng-scope">Financial assistance</option><!-- end ngRepeat: type in agency_types --><option ng-repeat="type in agency_types" value="Conceptual training" class="ng-binding ng-scope">Conceptual training</option><!-- end ngRepeat: type in agency_types --><option ng-repeat="type in agency_types" value="Other" class="ng-binding ng-scope">Other</option><!-- end ngRepeat: type in agency_types -->
                        </select>
                        <p class="text-danger ng-binding ng-hide" ng-show="errors.agency_type" aria-hidden="true"></p>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Agency Type Details</label> <input type="text" name="type_details" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.agency_type_details" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.agency_type_details" aria-hidden="true"></p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Training / Support type</label>
                        <input type="text" name="support" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.training_type" aria-invalid="false">
                        <p class="text-danger ng-binding ng-hide" ng-show="errors.training_type" aria-hidden="true"></p>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Male PWDs trained</label>
                        <input type="number" name="male" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.trained_male" aria-invalid="false">
                        <p class="text-danger ng-binding ng-hide" ng-show="errors.trained_male" aria-hidden="true"></p>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Female PWDs trained</label>
                        <input type="number" name="female" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.trained_female" aria-invalid="false">
                        <p class="text-danger ng-binding ng-hide" ng-show="errors.trained_female" aria-hidden="true"></p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="panel-footer">
                  <div class="flex-element  align-bw">
                    <div class="footer-text">
                      <h5>Please review your changes before you save them. Changes once saved cannot be
                reverted.</h5>
                    </div>
                    <div class="save-btn">
					<input type="hidden" name="month_data"/>
                      <button class="btn  btn-primary" type="submit" ng-click="onSubmit();">
                          Save Changes
                      </button>
                    </div>
                  </div>
                </div>
              </div>
			 <?php echo form_close();?>
			 
			  <?php if(!empty($agencies_get)){ ?>
              <div class="table-responsive">
                <table class="table table-bordered">
                  <thead>
                    <tr>
					<th>Agency Name</th>
                    <th>Agency Type</th>
                    <th>Agency Type details</th>
                    <th>Training type</th>
                    <th>Male PWDs trained</th>
                    <th>Female PWDs trained</th>
                   
                  </tr>
				  </thead>
                  <tbody>
				   <?php foreach($agencies_get as $agencies_get1) { ?>
                 <tr>
					<td><?php echo $agencies_get1->ss_agencies_name;  ?></td>
                    <td><?php echo $agencies_get1->ss_agencies_type;  ?></td>
					<td><?php echo $agencies_get1->ss_agencies_details;  ?></td>
					<td><?php echo $agencies_get1->ss_agencies_support;  ?></td>
					<td><?php echo $agencies_get1->ss_agencies_male;  ?></td>
					<td><?php echo $agencies_get1->ss_agencies_female;  ?></td>
                   
                  </tr>
				   <?php } ?>
                  </tbody>
                </table>
              </div>
			  <?php } ?>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(){


        var textarea = document.getElementById("code1");

        // Wait until animation finished render container
        setTimeout(function(){

            CodeMirror.fromTextArea(textarea, {
                lineNumbers: true,
                matchBrackets: true,
                styleActiveLine: true
            });
        }, 500);

    });
</script>