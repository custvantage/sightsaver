<div class="row wrapper border-bottom white-bg page-heading ng-scope">
  <div class="col-lg-10">
    <h2>Social Inclusion Advocacy Entry</h2>
  </div>
</div>

<div class="wrapper wrapper-content animated fadeIn ng-scope" ng-init="init()">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox">
        <div class="ibox-title">
          <h5>Advocacy Entry</h5>
        </div>
        <div class="ibox-content">
        <?php echo form_open('filter_advocacy_si');?>
          <div class="row flex-element center-both">
            <div class="well well-sm main-entry-box">
              <div class="col-md-6">
                <label>Partner:</label> <label><?php echo @strtoupper($this->session->userdata['userinfo']['partner_name']); ?></label>
              </div>
              <div class="col-md-4">
               <input type="text" name="month_from" id="month_from" class="form-control datepicker" onchange="date_fill()">
			   <p class="text-danger ng-binding ng-hide" aria-hidden="true">
			    <?php echo form_error('month_data');?>
				<?php echo form_error('month_from');?>
			   </p>
              </div>
              <div class="col-md-2">
			 
                <button class="btn btn-sm btn-primary btn-block" type="submit">
                Get Data
              </button>
              </div>
            </div>
          </div>
         <?php echo form_close(); ?>
          

          <div class="row m-t-lg" ng-show="hasData" aria-hidden="false" style="">
            <div class="col-md-12">
			<?php echo form_open('create_advocacy_si');?>
              <div class="panel panel-primary">
                <div class="panel-heading">
                  Advocacy Entry
                </div>
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Event Type</label>
                        <select name="event_type" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.event_type" aria-invalid="false"><option value="? undefined:undefined ?"></option>
                          <!-- ngRepeat: type in event_types --><option ng-repeat="type in event_types" value="Advocacy" class="ng-binding ng-scope">Advocacy</option><!-- end ngRepeat: type in event_types --><option ng-repeat="type in event_types" value="Meeting" class="ng-binding ng-scope">Meeting</option><!-- end ngRepeat: type in event_types --><option ng-repeat="type in event_types" value="Campaign" class="ng-binding ng-scope">Campaign</option><!-- end ngRepeat: type in event_types --><option ng-repeat="type in event_types" value="Workshop" class="ng-binding ng-scope">Workshop</option><!-- end ngRepeat: type in event_types --><option ng-repeat="type in event_types" value="mainstreaming disability" class="ng-binding ng-scope">mainstreaming disability</option><!-- end ngRepeat: type in event_types --><option ng-repeat="type in event_types" value="other" class="ng-binding ng-scope">other</option><!-- end ngRepeat: type in event_types -->
                        </select>
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.event_type" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Event Type details</label>
                        <input name="event_details" type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.event_type_details" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.event_topic" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <label>Meeting level</label>
                      <select name="meeting_level" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.level" aria-invalid="false"><option value="? undefined:undefined ?"></option>
                        <!-- ngRepeat: l in levels --><option ng-repeat="l in levels" value="Block" class="ng-binding ng-scope">Block</option><!-- end ngRepeat: l in levels --><option ng-repeat="l in levels" value="District" class="ng-binding ng-scope">District</option><!-- end ngRepeat: l in levels --><option ng-repeat="l in levels" value="State" class="ng-binding ng-scope">State</option><!-- end ngRepeat: l in levels --><option ng-repeat="l in levels" value="National" class="ng-binding ng-scope">National</option><!-- end ngRepeat: l in levels -->
                      </select>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.level" aria-hidden="true"></p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Level of activity</label>
                        <select name="level_activity" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.activity_level" aria-invalid="false"><option value="? undefined:undefined ?"></option>
                          <!-- ngRepeat: l in activity_levels --><option ng-repeat="l in activity_levels" value="BPOs / DPOs" class="ng-binding ng-scope">BPOs / DPOs</option><!-- end ngRepeat: l in activity_levels --><option ng-repeat="l in activity_levels" value="PWDs" class="ng-binding ng-scope">PWDs</option><!-- end ngRepeat: l in activity_levels --><option ng-repeat="l in activity_levels" value="Individual" class="ng-binding ng-scope">Individual</option><!-- end ngRepeat: l in activity_levels -->
                        </select>
                        <p class="text-danger ng-binding ng-hide" ng-show="errors.activity_level" aria-hidden="true"></p>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Whether PWD attended meeting</label>
                        <select name="pwd_attend_meeting" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.pwd_attended" aria-invalid="false"><option value="? undefined:undefined ?"></option>
                          <option value="true">Yes</option>
                          <option value="false">No</option>
                        </select>
                        <p class="text-danger ng-binding ng-hide" ng-show="errors.pwd_attended" aria-hidden="true"></p>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Meeting with</label>
                        <select name="meeting_with" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.meeting_with" aria-invalid="false"><option value="? undefined:undefined ?"></option>
                          <!-- ngRepeat: l in meeting_withs --><option ng-repeat="l in meeting_withs" value="Government department" class="ng-binding ng-scope">Government department</option><!-- end ngRepeat: l in meeting_withs --><option ng-repeat="l in meeting_withs" value="Private company" class="ng-binding ng-scope">Private company</option><!-- end ngRepeat: l in meeting_withs --><option ng-repeat="l in meeting_withs" value="Public sector company" class="ng-binding ng-scope">Public sector company</option><!-- end ngRepeat: l in meeting_withs --><option ng-repeat="l in meeting_withs" value="Other" class="ng-binding ng-scope">Other</option><!-- end ngRepeat: l in meeting_withs -->
                        </select>
                        <p class="text-danger ng-binding ng-hide" ng-show="errors.meeting_with" aria-hidden="true"></p>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Meeting with details</label>
                        <input name="meeting_details" type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.meeting_with_details" aria-invalid="false">
                        <p class="text-danger ng-binding ng-hide" ng-show="errors.meeting_with_details" aria-hidden="true"></p>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Place of meeting</label>
                        <input name="place_of_meeting" type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.meeting_place" aria-invalid="false">
                        <p class="text-danger ng-binding ng-hide" ng-show="errors.meeting_place" aria-hidden="true"></p>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Topic of meeting</label>
                        <input name="topic_of_meeting" type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.meeting_topic" aria-invalid="false">
                        <p class="text-danger ng-binding ng-hide" ng-show="errors.meeting_topic" aria-hidden="true"></p>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Details of the discussion</label>
                        <input name="discussion" type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.discussion_details" aria-invalid="false">
                        <p class="text-danger ng-binding ng-hide" ng-show="errors.discussion_details" aria-hidden="true"></p>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Issues succesfully resolved</label>
                        <input name="issues_resolved" type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.issues_resolved" aria-invalid="false">
                        <p class="text-danger ng-binding ng-hide" ng-show="errors.issues_resolved" aria-hidden="true"></p>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Decision / Plan</label>
                        <input name="decision" type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.decision_plan" aria-invalid="false">
                        <p class="text-danger ng-binding ng-hide" ng-show="errors.decision_plan" aria-hidden="true"></p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="panel-footer">
                  <div class="flex-element  align-bw">
                    <div class="footer-text">
                      <h5>
                        Please review your changes before you save them. Changes once saved cannot be reverted.
                      </h5>
                    </div>
                    <div class="save-btn">
					 <input type="hidden" name="month_data"/>
                      <button class="btn  btn-primary" type="submit" ng-click="onSubmit();">
                          Save Changes
                      </button>
                    </div>
                  </div>
                </div>
              </div>
			   <?php echo form_close();?>
			   
			<?php if(!empty($advocacy_get)){ ?>
              <div class="table-responsive">
                <table class="table table-bordered">
                  <thead>
                    <tr><th>Event Type</th>
                    <th>Event Type details</th>
                    <th>Meeting Level</th>
                    <th>Activity Level</th>
                    <th>PWD attended?</th>
                    <th>Meeting With</th>
                    <th>Meeting With details</th>
                    <th>Meeting Place</th>
                    <th>Meeting Topic</th>
                    <th>Discussion details</th>
                    <th>Issues Resolved?</th>
                    <th>Decision / plan</th>
                    
                  </tr></thead>
                  <tbody>
				  <?php foreach($advocacy_get as $advocacy_get1) { ?>
                   <tr>
				    <td><?php echo $advocacy_get1->ss_adv_event;  ?></td>
					<td><?php echo $advocacy_get1->ss_adv_details;  ?></td>
					<td><?php echo $advocacy_get1->ss_adv_meeting;  ?></td>
					<td><?php echo $advocacy_get1->ss_adv_level_of_activity;  ?></td>
					<td><?php echo $advocacy_get1->ss_pwd;  ?></td>
					<td><?php echo $advocacy_get1->ss_meeting_with;  ?></td>
					<td><?php echo $advocacy_get1->ss_meeting_details;  ?></td>
					<td><?php echo $advocacy_get1->ss_place_of_meeting;  ?></td>
					<td><?php echo $advocacy_get1->ss_topic_of_meeting;  ?></td>
					<td><?php echo $advocacy_get1->ss_discussion;  ?></td>
					<td><?php echo $advocacy_get1->ss_issues_resolved;  ?></td>
					<td><?php echo $advocacy_get1->ss_decision;  ?></td>
					
				   </tr>
				  <?php } ?>
                  </tbody>
                </table>
              </div>
           <?php } ?>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(){


        var textarea = document.getElementById("code1");

        // Wait until animation finished render container
        setTimeout(function(){

            CodeMirror.fromTextArea(textarea, {
                lineNumbers: true,
                matchBrackets: true,
                styleActiveLine: true
            });
        }, 500);

    });
</script>