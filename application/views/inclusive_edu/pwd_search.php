<div class="row wrapper border-bottom white-bg page-heading ng-scope">
  <div class="col-lg-10">
    <h2>Social Inclusion/ PWD Detail</h2>
  </div>
</div>

<div class="wrapper wrapper-content animated fadeIn ng-scope" ng-init="init()">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox">
        <div class="ibox-title">
          <h5>Pwd Detail</h5>
        </div>
        <div class="ibox-content">
          <div class="row flex-element center-both">
            <div class="well well-sm main-entry-box">
              <div class="col-md-4">
               <select class="form-control">
               	<option>Select a quarter...</option>
               </select>
              </div>
				<div class="col-md-8">
                <input type="text" class="form-control ng-pristine ng-valid ng-empty ng-touched" placeholder="Filter PWD" ng-model="filter" ng-model-options="{ debounce: 300 }" ng-change="applyFilter()" aria-invalid="false" style="">
              </div>
            </div>
          </div>
          <div class="row m-t-lg" ng-show="items.length > 0" aria-hidden="false" style="">
            <div class="pwd-table table-sm">
              <div class="text-center">
                <h3 class="table-heading">List Of PWD</h3>
                <div class="table-responsive">
                  <table class="table table-bordered text-left">
                    <thead>
                      <tr>
                        <th>Unique Id</th>
                        <th>Name</th>
                        <th>Age</th>
                        <th>Gender</th>
                        <th>Village</th>
                        <th>Block</th>
                        <th width="150">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <!-- ngRepeat: item in items track by item.id --><tr ng-repeat="item in items track by item.id" class="ng-scope">
                        <td class="ng-binding">1092</td>
                        <td class="ng-binding">Aadam ali</td>
                        <td class="ng-binding">10</td>
                        <td class="ng-binding">Male</td>
                        <td class="ng-binding">Moti bazar</td>
                        <td class="ng-binding">Nimbahera</td>
                        <td>
                          <button class="btn btn-sm btn-primary btn-block btn-xs" ui-sref="si.pwd_sheet({id: item.id})" href="#/si/pwd_sheet/8945">Detail</button>
                        </td>
                      </tr><!-- end ngRepeat: item in items track by item.id --><tr ng-repeat="item in items track by item.id" class="ng-scope">
                        <td class="ng-binding">2312</td>
                        <td class="ng-binding">Aaidan charan</td>
                        <td class="ng-binding">70</td>
                        <td class="ng-binding">Male</td>
                        <td class="ng-binding">Arniya paliwal</td>
                        <td class="ng-binding">Nimbahera</td>
                        <td>
                          <button class="btn btn-sm btn-primary btn-block btn-xs" ui-sref="si.pwd_sheet({id: item.id})" href="#/si/pwd_sheet/10137">Detail</button>
                        </td>
                      </tr><!-- end ngRepeat: item in items track by item.id --><tr ng-repeat="item in items track by item.id" class="ng-scope">
                        <td class="ng-binding">3530</td>
                        <td class="ng-binding">Aajad singh rajput</td>
                        <td class="ng-binding">42</td>
                        <td class="ng-binding">Male</td>
                        <td class="ng-binding">Lunkhanda</td>
                        <td class="ng-binding">Nimbahera</td>
                        <td>
                          <button class="btn btn-sm btn-primary btn-block btn-xs" ui-sref="si.pwd_sheet({id: item.id})" href="#/si/pwd_sheet/11328">Detail</button>
                        </td>
                      </tr><!-- end ngRepeat: item in items track by item.id --><tr ng-repeat="item in items track by item.id" class="ng-scope">
                        <td class="ng-binding">1466</td>
                        <td class="ng-binding">Aakash gujar</td>
                        <td class="ng-binding">3</td>
                        <td class="ng-binding">Male</td>
                        <td class="ng-binding">Satkhanda</td>
                        <td class="ng-binding">Nimbahera</td>
                        <td>
                          <button class="btn btn-sm btn-primary btn-block btn-xs" ui-sref="si.pwd_sheet({id: item.id})" href="#/si/pwd_sheet/9316">Detail</button>
                        </td>
                      </tr><!-- end ngRepeat: item in items track by item.id --><tr ng-repeat="item in items track by item.id" class="ng-scope">
                        <td class="ng-binding">1056</td>
                        <td class="ng-binding">Aakash tripathi</td>
                        <td class="ng-binding">18</td>
                        <td class="ng-binding">Male</td>
                        <td class="ng-binding">Shiv colony</td>
                        <td class="ng-binding">Nimbahera</td>
                        <td>
                          <button class="btn btn-sm btn-primary btn-block btn-xs" ui-sref="si.pwd_sheet({id: item.id})" href="#/si/pwd_sheet/8908">Detail</button>
                        </td>
                      </tr><!-- end ngRepeat: item in items track by item.id --><tr ng-repeat="item in items track by item.id" class="ng-scope">
                        <td class="ng-binding">1631</td>
                        <td class="ng-binding">Aamin</td>
                        <td class="ng-binding">30</td>
                        <td class="ng-binding">Male</td>
                        <td class="ng-binding">Rani kheda</td>
                        <td class="ng-binding">Nimbahera</td>
                        <td>
                          <button class="btn btn-sm btn-primary btn-block btn-xs" ui-sref="si.pwd_sheet({id: item.id})" href="#/si/pwd_sheet/9476">Detail</button>
                        </td>
                      </tr><!-- end ngRepeat: item in items track by item.id --><tr ng-repeat="item in items track by item.id" class="ng-scope">
                        <td class="ng-binding">1191</td>
                        <td class="ng-binding">Aanid kha</td>
                        <td class="ng-binding">34</td>
                        <td class="ng-binding">Male</td>
                        <td class="ng-binding">Lota bheru coloni</td>
                        <td class="ng-binding">Nimbahera</td>
                        <td>
                          <button class="btn btn-sm btn-primary btn-block btn-xs" ui-sref="si.pwd_sheet({id: item.id})" href="#/si/pwd_sheet/9042">Detail</button>
                        </td>
                      </tr><!-- end ngRepeat: item in items track by item.id --><tr ng-repeat="item in items track by item.id" class="ng-scope">
                        <td class="ng-binding">3690</td>
                        <td class="ng-binding">Aannd brahman</td>
                        <td class="ng-binding">16</td>
                        <td class="ng-binding">Male</td>
                        <td class="ng-binding">Ochhadi</td>
                        <td class="ng-binding">Chittorgarh</td>
                        <td>
                          <button class="btn btn-sm btn-primary btn-block btn-xs" ui-sref="si.pwd_sheet({id: item.id})" href="#/si/pwd_sheet/11485">Detail</button>
                        </td>
                      </tr><!-- end ngRepeat: item in items track by item.id --><tr ng-repeat="item in items track by item.id" class="ng-scope">
                        <td class="ng-binding">3097</td>
                        <td class="ng-binding">Aarati</td>
                        <td class="ng-binding">8</td>
                        <td class="ng-binding">Female</td>
                        <td class="ng-binding">Jalam pura</td>
                        <td class="ng-binding">Chittorgarh</td>
                        <td>
                          <button class="btn btn-sm btn-primary btn-block btn-xs" ui-sref="si.pwd_sheet({id: item.id})" href="#/si/pwd_sheet/10898">Detail</button>
                        </td>
                      </tr><!-- end ngRepeat: item in items track by item.id --><tr ng-repeat="item in items track by item.id" class="ng-scope">
                        <td class="ng-binding">3903</td>
                        <td class="ng-binding">Aarati</td>
                        <td class="ng-binding">14</td>
                        <td class="ng-binding">Female</td>
                        <td class="ng-binding">Benipuriya</td>
                        <td class="ng-binding">Chittorgarh</td>
                        <td>
                          <button class="btn btn-sm btn-primary btn-block btn-xs" ui-sref="si.pwd_sheet({id: item.id})" href="#/si/pwd_sheet/11697">Detail</button>
                        </td>
                      </tr><!-- end ngRepeat: item in items track by item.id --><tr ng-repeat="item in items track by item.id" class="ng-scope">
                        <td class="ng-binding">1143</td>
                        <td class="ng-binding">Aarif</td>
                        <td class="ng-binding">25</td>
                        <td class="ng-binding">Male</td>
                        <td class="ng-binding">Nagarpalika</td>
                        <td class="ng-binding">Nimbahera</td>
                        <td>
                          <button class="btn btn-sm btn-primary btn-block btn-xs" ui-sref="si.pwd_sheet({id: item.id})" href="#/si/pwd_sheet/8995">Detail</button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

          

        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(){


        var textarea = document.getElementById("code1");

        // Wait until animation finished render container
        setTimeout(function(){

            CodeMirror.fromTextArea(textarea, {
                lineNumbers: true,
                matchBrackets: true,
                styleActiveLine: true
            });
        }, 500);

    });
</script>