<div class="row wrapper border-bottom white-bg page-heading ng-scope">
  <div class="col-lg-10">
    <h2><?php echo @strtoupper($this->session->userdata['userinfo']['partner_name']); ?></h2>
  </div>
</div>

<div class="wrapper wrapper-content animated fadeIn ng-scope" ng-init="init()">
<?php if(isset($_SESSION['success']))
{ ?>
<div class="alert alert-success">
  <strong></strong> <?php echo $_SESSION['success'] ; ?>
</div>
<?php } ?>
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox">
        <div class="ibox-title">
          <h5>Inclusive Education / IE Main Entry</h5>
        </div>
        <div class="ibox-content">
                <?php
					if($this->session->userdata('userinfo')['default_role'] == 6) //if partner
					   {						
						$form_attrib = array('id'=>'manage_education_main_entry');
						echo form_open('manage_education_main_entry/'.$this->uri->segment(2),$form_attrib);?>
						<div class="row flex-element center-both">					
							<div class="well well-sm main-entry-box ">
								<div class="col-md-3 text-right">
									<label>Month:</label> 
								</div>
								<div class="col-md-5">
								<?php 
                                 $server_date = server_date_time(); 
								 $date11=date('m-Y',strtotime($server_date));


								?>
									<input type="text" name="month_from" id="month_from" value="<?php echo $date11;  ?>" class="form-control datepicker" onchange="date_fill()">
									<span class="text-danger ng-binding ng-hide" aria-hidden="true" id="error_month"><?php echo form_error('month_data');?></span>
								</div>
								<div class="col-md-4">
									<button class="btn btn-sm btn-primary btn-block" onclick="submit_form('manage_education_main_entry');" type="button">
										Get Data
									</button>
								</div>
							</div>
						</div>
							
						<?php echo form_close();?>
					<?php } else { //if pm/po ?>
					<?php $form_attrib = array('id'=>'manage_education_main_entry');
						echo form_open('manage_education_main_entry/'.$this->uri->segment(2),$form_attrib);?>
					<div class="row flex-element center-both">
						<div class="well well-sm main-entry-box">
						  <div class="col-md-4">
							<select class="form-control" name="partner_name">
							  <option value="">Select partner</option>
							  <?php if(!empty($partners)){ foreach($partners as $partner){?>
							  <option value="<?php echo $partner->ss_partners_id;?>"><?php echo $partner->ss_partners_name;?></option>
							  <?php } }?>
							</select>
						  </div>
						  <div class="col-md-4">
								<input type="text" name="month_from" id="month_from" class="form-control datepicker" placeholder="Select month" onchange="date_fill()">
								<span class="text-danger ng-binding ng-hide" aria-hidden="true" id="error_month"><?php echo form_error('month_data');?></span>
						  </div>
						  <div class="col-md-3">
							<button class="btn btn-sm btn-primary btn-block" onclick="submit_form('manage_education_main_entry');" type="button">
								Get Data
							</button>
						  </div>
						</div>
					</div>
					<?php echo form_close();?>
					<?php } ?>
         
          <div class="row" ng-show="hasData" aria-hidden="false">
		  
            <div class="col-md-12">
              <div class="bg-heading p-xs b-r-sm"><i class="fa fa-arrow-right"></i> Basic Details</div>
              <!-- ngInclude: -->
<ng-include src="'app/ie/partials/main_entry/a1.html'" class="ng-scope">
<?php $form_attrib = array('id'=>'education_main_entry');
echo form_open('main_entry_edu/'.$this->uri->segment(2),$form_attrib); ?>
<div class="table-responsive ng-scope">
    <?php 
	$section_id = ""; $j=0;
	if (!empty($metrics_data)) {
	// print_R($metrics_data); die;
  foreach ($metrics_data as $key_section_metric => $section_metric) 
  {
	  if ($section_id != $section_metric->ss_section_layout_ss_section_layout_id) {
		if ($j != 0) {
			?>
			</tbody>
			</table>
			<input type="hidden" name="month_data"/>
		<?php } ?>
  <table class="table table-bordered">
    <thead>
      <tr>  <?php if ($section_metric->ss_section_layout_section_col_count == 1 && $section_metric->ss_section_layout_analysis_description == "") { ?><td colspan="9" ><?php echo ucwords($section_metric->ss_section_layout_section_name); ?></td> <?php } else if($section_metric->ss_section_layout_section_col_count == 4 && $section_metric->ss_section_layout_analysis_description == ""){ ?><td colspan="20" ><?php echo ucwords($section_metric->ss_section_layout_section_name); ?></td> <?php } else { ?><td colspan="20" ><?php echo ucwords($section_metric->ss_section_layout_section_name); ?></td><?php } ?>  </tr>
	  
	  
	  
	  <?php if ($section_metric->ss_section_layout_section_col_count == 1 && $section_metric->ss_section_layout_analysis_description == "") { ?>
        <tr><td>Metric</td>
        <td>Value</td></tr>
		<?php }
            elseif ($section_metric->ss_section_layout_section_col_count == 4 && $section_metric->ss_section_layout_analysis_description == "") { ?>
		<tr>
        <td colspan="12"></td>
        <td colspan="2">Elementary (class 1 to 8)</td>
        <td colspan="2">Secondary (class 9 to 12)</td></tr>
      <tr>
        <td colspan="12"></td>
		<td>Boys</td>
        <td>Girls</td>
        <td>Boys</td>
        <td>Girls</td> </tr>
      
	  <?php }
            elseif ($section_metric->ss_section_layout_section_col_count == 8 && $section_metric->ss_section_layout_analysis_description == "") { ?>
		
		<tr>
        <td colspan="8"></td>
        <td colspan="4">Elementary (class 1 to 8)</td>
        <td colspan="4">Secondary (class 9 to 12)</td>
      </tr>
      <tr>
        <td colspan="8"></td>
		<td>sanctioned</td>
        <td colspan="2">In place</td>
        <td>vacant</td>
        <td>sanctioned</td>
        <td colspan="2">In place</td>
        <td>vacant</td>
      </tr>
      <tr>
        <td colspan="8"></td>
		<td>&nbsp;</td>
        <td>Male</td>
        <td>Female</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>Male</td>
        <td>Female</td>
        <td>&nbsp;</td>
       </tr> 
	    <?php }  ?>
     
    </thead>
    <tbody>
      <tr>
	  <?php if ($section_metric->ss_section_layout_section_col_count == 1 && $section_metric->ss_section_layout_analysis_description == "") { ?>
        <td><?php echo $section_metric->ss_metric_master_description;  ?>
		<input type="hidden" name="matric_id_one_column[]" value="<?php echo $section_metric->ss_metric_master_id;?>"/></td>

		<td><input type="text" name="one_column_val[]"  class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false">
		</td>
		
		<?php }
            elseif ($section_metric->ss_section_layout_section_col_count == 4 && $section_metric->ss_section_layout_analysis_description == "") { ?>
		
        <td colspan="12"><label><?php echo $section_metric->ss_metric_master_description;  ?></label>
		<input type="hidden" name="matric_id_four_column[]" value="<?php echo $section_metric->ss_metric_master_id;?>"/></td>
		
		<td><input type="text" name="four_column_men_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
		
        <td><input type="text" name="four_column_women_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
		
        <td><input type="text" name="four_column_boy_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
		
        <td><input type="text" name="four_column_girl_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
     
	   <?php }
       elseif ($section_metric->ss_section_layout_section_col_count == 8 && $section_metric->ss_section_layout_analysis_description == "") { ?>
	   
        <td colspan="8"><?php echo $section_metric->ss_metric_master_description;  ?>
		<input type="hidden" name="matric_id_eight_column[]" value="<?php echo $section_metric->ss_metric_master_id;?>"/></td>
		
		<td><input type="text" name="eight_column_ele_sanctioned[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
		
        <td><input type="text" name="eight_column_ele_male[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
		
        <td><input type="text" name="eight_column_ele_female[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
		
        <td><input type="text" name="eight_column_ele_vacant[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
		
        <td><input type="text" name="eight_column_sec_sanctioned[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
		
        <td><input type="text" name="eight_column_sec_male[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
		
        <td><input type="text" name="eight_column_sec_female[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
		
        <td><input type="text" name="eight_column_sec_vacant[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
      
	   <?php } ?>
	   
	  </tr>

	  <?php $section_id = $section_metric->ss_section_layout_ss_section_layout_id;
			$j++;
        }
        else {
            ?>
			
      <tr>
	  <?php if ($section_metric->ss_section_layout_section_col_count == 1 && $section_metric->ss_section_layout_analysis_description == "") { ?>
        <td><?php echo $section_metric->ss_metric_master_description;  ?>
		<input type="hidden" name="matric_id_one_column[]" value="<?php echo $section_metric->ss_metric_master_id;?>"/></td>
        <td><input type="text" name="one_column_val[]"  class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false">
		
		</td>
		<?php }
            elseif ($section_metric->ss_section_layout_section_col_count == 4 && $section_metric->ss_section_layout_analysis_description == "") { ?>
		
        <td colspan="12"><label><?php echo $section_metric->ss_metric_master_description;  ?></label>
		<input type="hidden" name="matric_id_four_column[]" value="<?php echo $section_metric->ss_metric_master_id;?>"/></td>
        <td><input type="text" name="four_column_men_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
		
        <td><input type="text" name="four_column_women_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
		
        <td><input type="text" name="four_column_boy_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
		
        <td><input type="text" name="four_column_girl_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
     
	   <?php }
       elseif ($section_metric->ss_section_layout_section_col_count == 8 && $section_metric->ss_section_layout_analysis_description == "") { ?>
	   
        <td colspan="8"><?php echo $section_metric->ss_metric_master_description;  ?>
		<input type="hidden" name="matric_id_eight_column[]" value="<?php echo $section_metric->ss_metric_master_id;?>"/></td>
        <td><input type="text" name="eight_column_ele_sanctioned[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
		
        <td><input type="text" name="eight_column_ele_male[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
		
        <td><input type="text" name="eight_column_ele_female[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
		
        <td><input type="text" name="eight_column_ele_vacant[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
		
        <td><input type="text" name="eight_column_sec_sanctioned[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
		
        <td><input type="text" name="eight_column_sec_male[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
		
        <td><input type="text" name="eight_column_sec_female[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
		
        <td><input type="text" name="eight_column_sec_vacant[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
      
	   <?php } ?>
	  </tr>
	
		<?php  }}}  ?>
		</tbody>
     </table>
	 <input type="hidden" name="month_data"/>
</div>
</ng-include>
<?php
if($this->session->userdata('userinfo')['default_role'] == 6 && !empty($metrics_data)) //if partner
{?>
 <div class="row" ng-show="hasData" aria-hidden="false" style="">
            <div class="col-md-12">
              <div class="well well-sm flex-element align-bw mt-20 tab-header">
                <h5 class="well-badge">Please review your changes before you save them. Changes once saved cannot be
                  reverted.</h5>
                <button class="btn btn-sm btn-primary" type="submit" ng-click="onSubmit(main_entry_edu);">
                  Save Changes
                </button>
              </div>
            </div>
          </div>
         <?php } ?>
		  <?php echo form_close();?>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>

<script>
    $(document).ready(function(){

        var textarea = document.getElementById("code1");

        // Wait until animation finished render container
        setTimeout(function(){

            CodeMirror.fromTextArea(textarea, {
                lineNumbers: true,
                matchBrackets: true,
                styleActiveLine: true
            });
        }, 500);
    });
</script>
<script>
$(document).ready(function(){
   $("[data-toggle='tooltip']").tooltip();
});
</script>
<script>
function submit_form(form_name)
{
	if($("#month_from").val() == "")
	{
		$("#error_month").html("Please select month");
		$("#month_from").focus();
	}else{
		
		if(form_name == "main_entry_edu")
		{
		$("#education_main_entry").submit();
		}
		else if(form_name == "manage_education_main_entry")
		{
			$("#manage_education_main_entry").submit();
		}
	}
}
</script>