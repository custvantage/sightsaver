<div class="row wrapper border-bottom white-bg page-heading ng-scope">
  <div class="col-lg-10">
    <h2><?php echo @strtoupper($this->session->userdata['userinfo']['partner_name']); ?></h2>
  </div>
   
</div>

<div class="wrapper wrapper-content animated fadeIn ng-scope" ng-init="init()">
<?php if(isset($_SESSION['success']))
{ ?>
<div class="alert alert-success">
  <strong></strong> <?php echo $_SESSION['success'] ; ?>
</div>
<?php } ?>
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox">
        <div class="ibox-title">
			<div class="col-lg-6">
          		<h5>Inclusive Education / IE Main Entry</h5>
			</div>
			<?php if($this->session->userdata('userinfo')['default_role']==6) { ?>
                        <div class="col-lg-6 status_display">
                            <h4 >Status: <span><?php if($status==1) {echo "In progress";}if($status==""){echo "New";} if($status==2){echo "Submitted";}  ?></span></h4>
                        </div>
						<?php } ?>
        </div>
        <div class="ibox-content">
        <?php
					if($this->session->userdata('userinfo')['default_role'] == 6) //if partner
					{						
						$disable = "";
						echo form_open('manage_education_main_entry/'.$this->uri->segment(2));?>
						<div class="row flex-element center-both">					
							<div class="well well-sm main-entry-box ">
								<div class="col-md-3 text-right">
									<label>Month:</label>
								</div>
								<div class="col-md-5">
									<input type="text" name="month_from" id="month_from" value="<?php echo $data1; ?>" class="form-control datepicker" onchange="date_fill()">
									<span class="text-danger ng-binding ng-hide" aria-hidden="true" id="error_month">
									<?php echo form_error('month_data');?></span>
								</div>
								<div class="col-md-4">
									<button class="btn btn-sm btn-primary btn-block" type="submit">
										Get Data
									</button>
								</div>
							</div>
						</div>
							
						<?php echo form_close();?>
					
					<?php } else { //if pm/po ?>
					<?php 
					$disable = "disabled";
					$form_attrib = array('id'=>'manage_education_main_entry');
						echo form_open('manage_education_main_entry/'.$this->uri->segment(2),$form_attrib);?>
					<div class="row flex-element center-both">
						<div class="well well-sm main-entry-box">
						  <div class="col-md-5">
							<select class="form-control" name="partner_name">
							  <option value="">Select partner</option>
							  <?php if(!empty($partners)){ foreach($partners as $partner){?>
							  <option value="<?php echo $partner->ss_partners_id;?>"><?php echo $partner->ss_partners_name;?></option>
							  <?php } } ?>
							</select>
						  </div>
						  <div class="col-md-4">
								<input type="text" name="month_from" id="month_from" vlaue="<?php echo $data1; ?>" class="form-control datepicker" placeholder="Select month" onchange="date_fill()">
								<span class="text-danger ng-binding ng-hide" aria-hidden="true" id="error_month"><?php echo form_error('month_data');?></span>
						  </div>
						  <div class="col-md-3">
							<button class="btn btn-sm btn-primary btn-block" onclick="submit_form('manage_education_main_entry');" type="button">
								Get Data
							</button>
						  </div>
						</div>
					</div>
					<?php echo form_close();?>
					<?php } ?>
         <div class="space50"></div>
          <div class="row" ng-show="hasData" aria-hidden="false">
            <div class="col-md-12">
              <div class="bg-heading p-xs b-r-sm"><i class="fa fa-arrow-right"></i> Basic Details</div>
              <!-- ngInclude: -->
               <ng-include src="'app/ie/partials/main_entry/a1.html'" class="ng-scope">
<?php $form_attrib = array('id'=>'education_main_entry');
 echo form_open('edit_form_data_edu/'.$this->uri->segment(2),$form_attrib); ?>
<div class="table-responsive ng-scope">
    <?php 
	$section_id = ""; $j=0;
	if (!empty($metrics_data)) {
	// print_R($metrics_data); die;
  foreach ($metrics_data as $key_section_metric => $section_metric) 
  {
	  if ($section_id != $section_metric->ss_section_layout_ss_section_layout_id) {
		if ($j != 0) {
			?>
			</tbody>
			</table>
			<input type="hidden" name="month_data"/>
		<?php } ?>
  <table class="table table-bordered">
    <thead>
      <tr>  <?php if ($section_metric->ss_section_layout_section_col_count == 1 && $section_metric->ss_section_layout_analysis_description == "") { ?><td colspan="9" ><?php echo ucwords($section_metric->ss_section_layout_section_name); ?></td> <?php } else if($section_metric->ss_section_layout_section_col_count == 4 && $section_metric->ss_section_layout_analysis_description == ""){ ?><td colspan="20" ><?php echo ucwords($section_metric->ss_section_layout_section_name); ?></td> <?php } else { ?><td colspan="20" ><?php echo ucwords($section_metric->ss_section_layout_section_name); ?></td><?php } ?>  </tr>
	  
	  
	  
	  <?php if ($section_metric->ss_section_layout_section_col_count == 1 && $section_metric->ss_section_layout_analysis_description == "") { ?>
        <tr><td>Metric</td>
        <td>Value</td></tr>
		<?php }
            elseif ($section_metric->ss_section_layout_section_col_count == 4 && $section_metric->ss_section_layout_analysis_description == "") { ?>
		<tr>
        <td colspan="12"></td>
        <td colspan="2">Elementary (class 1 to 8)</td>
        <td colspan="2">Secondary (class 9 to 12)</td></tr>
      <tr>
        <td colspan="12"></td>
		<td>Boys</td>
        <td>Girls</td>
        <td>Boys</td>
        <td>Girls</td> </tr>
      
	  <?php }
            elseif ($section_metric->ss_section_layout_section_col_count == 8 && $section_metric->ss_section_layout_analysis_description == "") { ?>
		
		<tr>
        <td colspan="8"></td>
        <td colspan="4">Elementary (class 1 to 8)</td>
        <td colspan="4">Secondary (class 9 to 12)</td>
      </tr>
      <tr>
        <td colspan="8"></td>
		<td>sanctioned</td>
        <td colspan="2">In place</td>
        <td>vacant</td>
        <td>sanctioned</td>
        <td colspan="2">In place</td>
        <td>vacant</td>
      </tr>
      <tr>
        <td colspan="8"></td>
		<td>&nbsp;</td>
        <td>Male</td>
        <td>Female</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>Male</td>
        <td>Female</td>
        <td>&nbsp;</td>
       </tr> 
	    <?php }  ?>
     
    </thead>
    <tbody>
      <tr>
	  <?php if ($section_metric->ss_section_layout_section_col_count == 1 && $section_metric->ss_section_layout_analysis_description == "") { ?>
        <td><?php echo $section_metric->ss_metric_master_description;  ?>
		<input type="hidden" name="matric_id_one_column[]" value="<?php echo $section_metric->ss_metric_master_id;?>"/></td>
		<td><input type="text" name="one_column_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $one_col_val = $this->metricdata->getOneColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($one_col_val)){ echo $one_col_val;}?>" <?php echo $disable;?> aria-invalid="false"></td>
		
		<?php }
            elseif ($section_metric->ss_section_layout_section_col_count == 4 && $section_metric->ss_section_layout_analysis_description == "") { ?>
		
        <td colspan="12"><label><?php echo $section_metric->ss_metric_master_description;  ?></label>
		<input type="hidden" name="matric_id_four_column[]" value="<?php echo $section_metric->ss_metric_master_id;?>"/></td>
		
		<td><input type="text" name="four_column_men_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $four_col_data = $this->metricdata->getFourColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($four_col_data->ss_four_column_data_value_men)){ echo $four_col_data->ss_four_column_data_value_men;}?>" <?php echo $disable;?> aria-invalid="false"></td>
		
		<td><input type="text" name="four_column_women_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $four_col_data = $this->metricdata->getFourColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($four_col_data->ss_four_column_data_value_women)){ echo $four_col_data->ss_four_column_data_value_women;}?>" <?php echo $disable;?> aria-invalid="false"></td>
		
        <td><input type="text" name="four_column_boy_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $four_col_data = $this->metricdata->getFourColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($four_col_data->ss_four_column_data_value_boys)){ echo $four_col_data->ss_four_column_data_value_boys;}?>" <?php echo $disable;?> aria-invalid="false"></td>
		
        <td><input type="text" name="four_column_girl_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $four_col_data = $this->metricdata->getFourColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($four_col_data->ss_four_column_data_value_girls)){
		echo $four_col_data->ss_four_column_data_value_girls;}?>" <?php echo $disable;?> aria-invalid="false"></td>
     
	   <?php }
       elseif ($section_metric->ss_section_layout_section_col_count == 8 && $section_metric->ss_section_layout_analysis_description == "") { ?>
	   
        <td colspan="8"><?php echo $section_metric->ss_metric_master_description;  ?>
		<input type="hidden" name="matric_id_eight_column[]" value="<?php echo $section_metric->ss_metric_master_id;?>"/></td>
		
		
		<td><input type="text" name="eight_column_ele_sanctioned[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $eight_col_data = $this->metricdata->getEightColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($eight_col_data->ss_elementory_sanctioned)){ echo $eight_col_data->ss_elementory_sanctioned;}?>" <?php echo $disable;?> aria-invalid="false"></td>
		
		<td><input type="text" name="eight_column_ele_male[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $eight_col_data = $this->metricdata->getEightColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($eight_col_data->ss_elementory_boys)){ echo $eight_col_data->ss_elementory_boys;}?>" <?php echo $disable;?> aria-invalid="false"></td>
		
		<td><input type="text" name="eight_column_ele_female[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $eight_col_data = $this->metricdata->getEightColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($eight_col_data->ss_elementory_girls)){ echo $eight_col_data->ss_elementory_girls;}?>" <?php echo $disable;?> aria-invalid="false"></td>
		
        <td><input type="text" name="eight_column_ele_vacant[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $eight_col_data = $this->metricdata->getEightColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($eight_col_data->ss_elementory_vacant)){ echo $eight_col_data->ss_elementory_vacant;}?>" <?php echo $disable;?> aria-invalid="false"></td>
		
        <td><input type="text" name="eight_column_sec_sanctioned[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $eight_col_data = $this->metricdata->getEightColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($eight_col_data->ss_secondary_sanctioned)){ echo $eight_col_data->ss_secondary_sanctioned;}?>" <?php echo $disable;?> aria-invalid="false"></td>
		
       <td><input type="text" name="eight_column_sec_male[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $eight_col_data = $this->metricdata->getEightColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($eight_col_data->ss_secondary_boys)){ echo $eight_col_data->ss_secondary_boys;}?>" <?php echo $disable;?> aria-invalid="false"></td>
		
        <td><input type="text" name="eight_column_sec_female[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $eight_col_data = $this->metricdata->getEightColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($eight_col_data->ss_secondary_girls)){ echo $eight_col_data->ss_secondary_girls;}?>" <?php echo $disable;?> aria-invalid="false"></td>
		
		 <td><input type="text" name="eight_column_sec_vacant[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $eight_col_data = $this->metricdata->getEightColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($eight_col_data->ss_secondary_vacant)){ echo $eight_col_data->ss_secondary_vacant;}?>" <?php echo $disable;?> aria-invalid="false"></td>
		
	   <?php } ?>
	   
	  </tr>

	  <?php $section_id = $section_metric->ss_section_layout_ss_section_layout_id;
			$j++;
        }
        else {
            ?>
			
      <tr>
	  <?php if ($section_metric->ss_section_layout_section_col_count == 1 && $section_metric->ss_section_layout_analysis_description == "") { ?>
       <td><?php echo $section_metric->ss_metric_master_description;  ?>
		<input type="hidden" name="matric_id_one_column[]" value="<?php echo $section_metric->ss_metric_master_id;?>"/></td>
		
		<td><input type="text" name="one_column_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $one_col_val = $this->metricdata->getOneColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($one_col_val)){ echo $one_col_val;}?>" <?php echo $disable;?> aria-invalid="false"></td>
		<?php }
            elseif ($section_metric->ss_section_layout_section_col_count == 4 && $section_metric->ss_section_layout_analysis_description == "") { ?>
		
        <td colspan="12"><label><?php echo $section_metric->ss_metric_master_description;  ?></label>
		<input type="hidden" name="matric_id_four_column[]" value="<?php echo $section_metric->ss_metric_master_id;?>"/></td>
        
		<td><input type="text" name="four_column_men_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $four_col_data = $this->metricdata->getFourColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($four_col_data->ss_four_column_data_value_men)){ echo $four_col_data->ss_four_column_data_value_men;}?>" <?php echo $disable;?> aria-invalid="false"></td>
		
		<td><input type="text" name="four_column_women_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $four_col_data = $this->metricdata->getFourColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($four_col_data->ss_four_column_data_value_women)){ echo $four_col_data->ss_four_column_data_value_women;}?>" <?php echo $disable;?> aria-invalid="false"></td>
		
        <td><input type="text" name="four_column_boy_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $four_col_data = $this->metricdata->getFourColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($four_col_data->ss_four_column_data_value_boys)){ echo $four_col_data->ss_four_column_data_value_boys;}?>" <?php echo $disable;?> aria-invalid="false"></td>
		
        <td><input type="text" name="four_column_girl_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $four_col_data = $this->metricdata->getFourColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($four_col_data->ss_four_column_data_value_girls)){
		echo $four_col_data->ss_four_column_data_value_girls;}?>" <?php echo $disable;?> aria-invalid="false"></td>
     
	   <?php }
       elseif ($section_metric->ss_section_layout_section_col_count == 8 && $section_metric->ss_section_layout_analysis_description == "") { ?>
	   
        <td colspan="8"><?php echo $section_metric->ss_metric_master_description;  ?>
		<input type="hidden" name="matric_id_eight_column[]" value="<?php echo $section_metric->ss_metric_master_id;?>"/></td>
        
		<td><input type="text" name="eight_column_ele_sanctioned[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $eight_col_data = $this->metricdata->getEightColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($eight_col_data->ss_elementory_sanctioned)){ echo $eight_col_data->ss_elementory_sanctioned;}?>" <?php echo $disable;?> aria-invalid="false"></td>
		
		<td><input type="text" name="eight_column_ele_male[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $eight_col_data = $this->metricdata->getEightColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($eight_col_data->ss_elementory_boys)){ echo $eight_col_data->ss_elementory_boys;}?>" <?php echo $disable;?> aria-invalid="false"></td>
		
		<td><input type="text" name="eight_column_ele_female[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $eight_col_data = $this->metricdata->getEightColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($eight_col_data->ss_elementory_girls)){ echo $eight_col_data->ss_elementory_girls;}?>" <?php echo $disable;?> aria-invalid="false"></td>
		
        <td><input type="text" name="eight_column_ele_vacant[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $eight_col_data = $this->metricdata->getEightColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($eight_col_data->ss_elementory_vacant)){ echo $eight_col_data->ss_elementory_vacant;}?>" <?php echo $disable;?> aria-invalid="false"></td>
		
        <td><input type="text" name="eight_column_sec_sanctioned[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $eight_col_data = $this->metricdata->getEightColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($eight_col_data->ss_secondary_sanctioned)){ echo $eight_col_data->ss_secondary_sanctioned;}?>" <?php echo $disable;?> aria-invalid="false"></td>
		
       <td><input type="text" name="eight_column_sec_male[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $eight_col_data = $this->metricdata->getEightColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($eight_col_data->ss_secondary_boys)){ echo $eight_col_data->ss_secondary_boys;}?>" <?php echo $disable;?> aria-invalid="false"></td>
		
        <td><input type="text" name="eight_column_sec_female[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $eight_col_data = $this->metricdata->getEightColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($eight_col_data->ss_secondary_girls)){ echo $eight_col_data->ss_secondary_girls;}?>" <?php echo $disable;?> aria-invalid="false"></td>
		
		 <td><input type="text" name="eight_column_sec_vacant[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" value="<?php $eight_col_data = $this->metricdata->getEightColVal($section_metric->ss_metric_master_id,$post_month_from,$session_partner_id);if(!empty($eight_col_data->ss_secondary_vacant)){ echo $eight_col_data->ss_secondary_vacant;}?>" <?php echo $disable;?> aria-invalid="false"></td>
      
	    <?php } ?>
	    </tr>
		<?php  }}}  ?>
		</tbody>
     </table>
	 <input type="hidden" name="month_data"/>
</div>
<?php if($this->session->userdata('userinfo')['default_role']==6) { ?>
<?php if($status==1 || $status=="")  { ?>
<?php if(!empty($metrics_data))
{?>
<?php if(isset($mpr_id) && !empty($mpr_id))
{ ?>
<input type="hidden" name="mpr_summary_id" value="<?php echo $mpr_id;?>"/>
<?php } ?>
<input type="hidden" name="mpr_summary_month" value="<?php echo $mpr_report_month;?>"/>
<div class="panel-body ng-scope">
	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-0 col-xs-0"></div>
		<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 text-right">		
			<input type="submit" class="btn btn-block btn-primary" name="update_form_data" value="Save"/>				
		</div>	
<?php } echo form_close();
 } else {}  
 } else {  
 if(!empty($metrics_data))
{
if(isset($mpr_id) && !empty($mpr_id))
{ ?>
<input type="hidden" name="mpr_summary_id" value="<?php echo $mpr_id;?>"/>
<?php } ?>
<input type="hidden" name="mpr_summary_month" value="<?php echo $mpr_report_month;?>"/>
<div class="panel-body ng-scope">
	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-0 col-xs-0"></div>
		<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 text-right">		
			<input type="submit" class="btn btn-block btn-primary" name="update_form_data" value="Save"/>				
		</div>	
<?php } echo form_close(); } ?>



<?php
if(isset($mpr_id) && !empty($mpr_id) && !empty($metrics_data))
{
echo form_open('submit_form_data_edu/'.$this->uri->segment(2));	?>

		<div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 text-right">
		<input type="hidden" name="mpr_summary_id" value="<?php echo $mpr_id;?>"/>	
        <input type="hidden" name="current_year" value="<?php echo $mpr_report_month;?>"/>
		
		<button class="btn btn-block btn-primary" type="submit">
				Submit
		</button>
		</div>
	</div>
</div>
<?php echo form_close(); } ?> 
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script>
    $(document).ready(function(){

        var textarea = document.getElementById("code1");

        // Wait until animation finished render container
        setTimeout(function(){

            CodeMirror.fromTextArea(textarea, {
                lineNumbers: true,
                matchBrackets: true,
                styleActiveLine: true
            });
        }, 500);

    });
</script>
<script>
function submit_form(form_name)
{
	if($("#month_from").val() == "")
	{
		$("#error_month").html("Please select month");
		$("#month_from").focus();
	}
	else
	{	
		$("#manage_education_main_entry").submit();		
	}
}

function comment_pmpo()
{
	var comments = $("#pmpo_comments").val();
	var partner_id = $("#partner_id").val();
	var month_from = $("#month_from_approve").val();
	var project_name = "rural eye health";
	var module_name = "main entry";
	if(partner_id != "" && comments != "")
	{
		var url = "<?php echo BASE_URL('create_pmpo_comment');?>";				 
		var csrfHash = $("input[name=csrf_test_name]").val();
		 
		$.post(url,{<?php echo $this->security->get_csrf_token_name(); ?>: csrfHash,comments:comments,partner_id:partner_id,month_from:month_from,project_name:project_name,module_name:module_name} ,function(response){
				
			var htmldata = "";
			var status = "";
			$("input[name=csrf_test_name]").val(response.csrfHash);
			if(response.success ==1)
			{
				htmldata += '<div class="chat-box-single-line"><abbr class="timestamp">'+response.created_on+'</abbr></div><div class="direct-chat-msg doted-border"><div class="direct-chat-info clearfix"><span class="direct-chat-name pull-left">'+response.partner_name+'</span></div><div class="direct-chat-text">'+response.comments+'</div><div class="direct-chat-info clearfix"><span class="direct-chat-timestamp pull-right">'+response.created_time+'</span></div></div>';
				
				
				$("#pmpo_comments").val("");
				$("#response_pmpo").append(htmldata);
				
			}else{
				$("#pmpo_comments").val("");
				alert("Data is not submitted");
			}			
		},"json");
	}
}
function approve(approvestatus)
{
	var month_from_approve = $("#month_from_approve").val();
	var partner_id_approve = $("#partner_id_approve").val();
	var project_name = "rural eye health";
	var module_name = "main entry";
	var reason = "";
	var approve_flag = "";
	if($("#approve_reason").val() != "")
	{
		reason = $("#approve_reason").val();
		
	}
	else if($("#reject_reason").val() != "")
	{
		reason = $("#reject_reason").val();
		approve_flag = 0;
	}	
	if(approvestatus == "approve")
	{
		approve_flag = 1;
	}elseif(approvestatus == "reject")
	{
		approve_flag = 0;
	}
	if(partner_id_approve != "" && month_from_approve != "")
	{
		var url = "<?php echo BASE_URL('approval');?>";				 
		var csrfHash = $("input[name=csrf_test_name]").val();
		 
		$.post(url,{<?php echo $this->security->get_csrf_token_name(); ?>: csrfHash,month_from_approve:month_from_approve,partner_id_approve:partner_id_approve,project_name:project_name,module_name:module_name,reason:reason,approve_flag:approve_flag} ,function(response){
				
			var htmldata = "";
			var status = "";
			$("input[name=csrf_test_name]").val(response.csrfHash);
			if(response.success ==1)
			{
				if(approve_flag == 1)
				{
					$("#approve_reason").val("");
					alert("Successfully Approved");					
				}else{
					$("#reject_reason").val("");
					alert("Rejected");
				}				
			}else{
				$("#approve_reason").val("");
				$("#reject_reason").val("");
				alert("No data found");
			}			
		},"json");
	}
}

function show_full_comment(id)
{
	if(id != "")
	{
		$("#full_comments_show").html("");
		$("#full_comments_show").html($("#comment_"+id).val());
		$("#full_comments").modal({show: 'True'}); 
	}
}
</script>