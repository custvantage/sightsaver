<div class="row wrapper border-bottom white-bg page-heading ng-scope">
  <div class="col-lg-10">
    <h2>Rural Eye Health Referrals Entry</h2>
  </div>
</div>

<div class="wrapper wrapper-content animated fadeIn ng-scope" ng-init="init()">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox">
        <div class="ibox-title">
          <h5>Main sheet</h5>
        </div>
        <div class="ibox-content">
          <div class="row flex-element center-both">
            <div class="well well-sm main-entry-box">
              <div class="col-md-6">
                <select class="form-control" name="">
                  <option>Select or searh for partener</option>
                </select>
              </div>
              <div class="col-md-4">
               <input type="month" class="form-control">
              </div>
              <div class="col-md-2">
                <button class="btn btn-sm btn-primary btn-block" type="submit" ng-click="getData();">
                Get Data
              </button>
              </div>
            </div>
          </div>
          

          <div class="row m-t-lg" ng-show="hasData" aria-hidden="false" style="">
            <div class="col-md-12">
              <div class="panel panel-primary">
                <div class="panel-heading">
                  Referrals Entry
                </div>
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Block / Location Name</label> <input type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.location_name" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.location_name" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Village</label> <input type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.village" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.village" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Patient Name</label> <input type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.patient_name" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.patient_name" aria-hidden="true"></p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Age</label> <input type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.patient_age" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.patient_age" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Gender</label>
                        <select class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.gender" aria-invalid="false"><option value="? undefined:undefined ?"></option>
                          <option value="Male">Male</option>
                          <option value="Female">Female</option>
                        </select>
                        <p class="text-danger ng-binding ng-hide" ng-show="errors.gender" aria-hidden="true"></p>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Referred from</label>
                        <select class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.referred_from" aria-invalid="false"><option value="? undefined:undefined ?"></option>
                          <option value="Base Hospital">Base Hospital</option>
                          <option value="Camps">Camps</option>
                          <option value="CHC">CHC</option>
                          <option value="District hospital">District hospital</option>
                          <option value="Other">Other</option>
                          <option value="PHC">PHC</option>
                          <option value="Vision Centre">Vision Centre</option>
                        </select>
                        <p class="text-danger ng-binding ng-hide" ng-show="errors.referred_from" aria-hidden="true"></p>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Referred to</label>
                        <select class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.referred_to" aria-invalid="false"><option value="? undefined:undefined ?"></option>
                          <option value="Base Hospital">Base Hospital</option>
                          <option value="Camps">Camps</option>
                          <option value="CHC">CHC</option>
                          <option value="District hospital">District hospital</option>
                          <option value="Other">Other</option>
                          <option value="PHC">PHC</option>
                          <option value="Vision Centre">Vision Centre</option>
                        </select>
                        <p class="text-danger ng-binding ng-hide" ng-show="errors.referred_to" aria-hidden="true"></p>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Referred By Designation</label>
                        <select class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.referred_by_designation" aria-invalid="false"><option value="? undefined:undefined ?"></option>
                          <option value="ASHA">ASHA</option>
                          <option value="ANM">ANM</option>
                          <option value="AWW">AWW</option>
                          <option value="VHSNC member">VHSNC member</option>
                          <option value="Ophthalmic personnel">Ophthalmic personnel</option>
                          <option value="Other"></option>
                        </select>
                        <p class="text-danger ng-binding ng-hide" ng-show="errors.referred_by_designation" aria-hidden="true"></p>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Name of health functionary</label> <input type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.health_functionary_name" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.health_functionary_name" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Referred For</label>
                        <select class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.referred_for" aria-invalid="false"><option value="? undefined:undefined ?"></option>
                          <option value="Cataract">Cataract</option>
                          <option value="Refractive Error">Refractive Error</option>
                          <option value="Other">Other</option>
                        </select>
                        <p class="text-danger ng-binding ng-hide" ng-show="errors.referred_for" aria-hidden="true"></p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="panel-footer">
                  <div class="flex-element  align-bw">
                    <div class="footer-text">
                      <h5>Please review your changes before you save them. Changes once saved cannot be
                reverted.</h5>
                    </div>
                    <div class="save-btn">
                      <button class="btn  btn-primary" type="submit" ng-click="onSubmit();">
                          Save Changes
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              <div class="table-responsive">
              <table class="table table-bordered">
                <thead>
                  <tr><th>Block / Location</th>
                  <th>Village</th>
                  <th>Patient Name</th>
                  <th>Age</th>
                  <th>Gender</th>
                  <th>Referred from</th>
                  <th>Referred to</th>
                  <th>Functionary designation</th>
                  <th>Functionary Name</th>
                  <th>Referred for</th>
                  <th>Action</th>
                </tr></thead>
                <tbody>
                  <!-- ngRepeat: item in items track by item.id -->
                </tbody>
              </table>
            </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(){


        var textarea = document.getElementById("code1");

        // Wait until animation finished render container
        setTimeout(function(){

            CodeMirror.fromTextArea(textarea, {
                lineNumbers: true,
                matchBrackets: true,
                styleActiveLine: true
            });
        }, 500);

    });
</script>