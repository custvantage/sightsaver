
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Sight Savers</title>
        <link href="<?php echo PUBLIC_URL; ?>css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo PUBLIC_URL; ?>font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo PUBLIC_URL; ?>css/animate.css" rel="stylesheet">
        <link href="<?php echo PUBLIC_URL; ?>css/style.css" rel="stylesheet">
        <link href="<?php echo PUBLIC_URL; ?>css/custom.css" rel="stylesheet">
        <link href="<?php echo PUBLIC_URL; ?>css/lc_switch.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo PUBLIC_URL; ?>css/codemirror.css">
        <link rel="stylesheet" href="<?php echo PUBLIC_URL; ?>css/custom.css">
		 <link href="<?php echo PUBLIC_URL; ?>css/bootstrap_datepicker.css" rel="stylesheet"> 
     	<link href="<?php echo PUBLIC_URL; ?>css/dataTables.bootstrap.min.css" rel="stylesheet">
    </head>
<body class="fixed-sidebar">

<div id="wrapper">

<nav class="navbar-admin navbar-static-side" role="navigation">
    <div class="sidebar-collapse">

      <ul side-navigation="" class="nav metismenu" id="side-menu">
      <li class="nav-header" style="padding: 20px;">
        <div class="profile-element">
          <img alt="image" width="100%" src="<?php echo PUBLIC_URL; ?>img/logo.png" src="<?php echo PUBLIC_URL; ?>assets/images/logo.png">
        </div>
        <div class="logo-element">
          SS
        </div>
      </li>
	 
 <?php 
  if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==6)  { ?> 
	   <li ng-class="{active: m.ie}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Data Entry</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" id="menhead123" ng-class="{in: m.ie}" aria-expanded="false">
	  
     </ul>
      </li>
		  <li class="active">
        	<a href="<?php echo BASE_URL.'partner_mpr';?>"><i class="fa fa-eye"></i> <span class="nav-label">Review MPR</span></a>
        
      	</li>
     <?php }  ?>
	 
	   <?php  if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==1 || $this->session->userdata('userinfo')['default_role']==5 || $this->session->userdata('userinfo')['default_role']==2 || $this->session->userdata('userinfo')['default_role']==3 || $this->session->userdata('userinfo')['default_role']==4)  { ?>
	  
	   <li ng-class="{active: m.dashboards}" >
		  <li class="active">
			<a href="<?php echo BASE_URL.'dashboard_speedometer';?>"><i class="fa fa-dashboard"></i> <span class="nav-label">Dashboard</span> </a>
			</li>
			<!--li><a href="<?php //echo BASE_URL.'dashboard';?>">Report</a></li-->
			  <li ng-class="{active: m.reports}">
				<a href=""><i class="fa fa-file-pdf-o"></i> <span class="nav-label">Graph</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse" ng-class="{in: m.reports}" aria-expanded="true">
				  <li  ><a href="#">Social Inclusion</a></li>
				  <li ><a  href="<?php echo BASE_URL.'dashboard_graph';?>">Rural Eye Health</a></li>
				  <li ><a  href="<?php echo BASE_URL.'dashboard_graph_ueh';?>">Urban Eye Health</a></li>
				  <li ><a  href="#">Inclusive Education</a></li>
				</ul>
			  </li>
		  </li>
			<li ng-class="{active: m.reports}" >
				<a href=""><i class="fa fa-file-pdf-o"></i> <span class="nav-label">MPR</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse" ng-class="{in: m.reports}" aria-expanded="true">
				  <li  ><a href="<?php echo BASE_URL.'mpr_si';?>">Social Inclusion</a></li>
				  <li ><a  href="<?php echo BASE_URL.'mpr_reh' ;?>">Rural Eye Health</a></li>
				  <li ><a  href="<?php echo BASE_URL.'mpr_ueh';?>">Urban Eye Health</a></li>
				  <li ><a  href="<?php echo BASE_URL.'mpr_ie';?>">Inclusive Education</a></li>
				</ul>
			</li>
			
			<li ng-class="{active: m.reports}" >
				<a href=""><i class="fa fa-file-pdf-o"></i> <span class="nav-label">Yearly Target</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse" ng-class="{in: m.reports}" aria-expanded="true">
     			 <li ><a  href="<?php echo BASE_URL.'reh_yearly_target/';echo base64_encode('rural eye health-yearly target');?>">REH Yearly Targets</a></li>
				   <li ><a  href="<?php echo BASE_URL.'ueh_yearly_target/';echo base64_encode('urban eye health-yearly target');?>">UEH Yearly Targets</a></li>
				</ul>
			</li>
			
		<?php if($this->session->userdata('userinfo')['default_role']==2 || $this->session->userdata('userinfo')['default_role']==3 || $this->session->userdata('userinfo')['default_role']==4) { ?>
		<li ng-class="{active: m.reports}" >
				<a href=""><i class="fa fa-file-pdf-o"></i> <span class="nav-label">Data approval</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse" ng-class="{in: m.reports}" aria-expanded="true">
				  <li  ><a href="<?php echo BASE_URL.'Approval';?>">Review MPR</a></li>				  
				</ul>
			</li>
		<?php }  ?>
		<?php  if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==1) { ?>
		<li ng-class="{active: m.reports}" >
				<a href=""><i class="fa fa-file-pdf-o"></i> <span class="nav-label">Data approval</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse" ng-class="{in: m.reports}" aria-expanded="true">
				  <li  ><a href="<?php echo BASE_URL.'Approvaladmin';?>">Review MPR</a></li>				  
				</ul>
			</li>
		<?php } ?>
		
		
		
	  
	  <li ng-class="{active: m.ie}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Data Entry</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.ie}" aria-expanded="false">
        <li ng-class="{active: m.ie}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Inclusive Education</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.ie}" aria-expanded="false">
          <li ><a href="<?php echo BASE_URL.'ie_main_entry/';echo base64_encode('Inclusive education-main entry'); ?>">IE Main Entry</a></li>
        </ul>
      </li>

      <li ng-class="{active: m.reh}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Rural Eye Health</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.reh}" aria-expanded="false">
          <li ><a  href="<?php echo BASE_URL.'reh_main_entry/';echo base64_encode('rural eye health-main entry');?>">REH Main Entry</a></li>
          <!--<li ><a  href="<?php //echo BASE_URL.'reh_refferal';?>">Referrals</a></li>-->
		  <li><a href="<?php echo BASE_URL.'reh_base_hospital';?>">REH Base Hospital</a></li>
          <li ><a  href="<?php echo BASE_URL.'reh_training';?>">REH Training | Capacity Building</a></li>
          <li ><a  href="<?php echo BASE_URL.'reh_advocacy';?>">REH Advocacy | Networking | Liasion</a></li>
          <li ><a  href="<?php echo BASE_URL.'reh_bcc';?>">REH BCC Entry</a></li>
          <li ><a  href="<?php echo BASE_URL.'reh_iec';?>">REH IEC Entry</a></li>
          <li ><a  href="<?php echo BASE_URL.'reh_vhsnc';?>">VHSNC Entry</a></li>
         <!-- <li ><a  href="javascript:void(0)">Errors in submission</a></li>  -->
        </ul>
      </li>
	  
      <li ng-class="{active: m.si}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Social Inclusion</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.si}" aria-expanded="false">
          <li ><a  href="<?php echo BASE_URL.'si_pwd_search';?>">SI PWD Search</a></li>
          <li ><a  href="<?php echo BASE_URL.'si_main_entry/';echo base64_encode('Social inclusion-main entry');?>">SI Main Entry</a></li>
          <li ><a  href="<?php echo BASE_URL.'shg';?>">SI SHG | PPG</a></li>
          <li ><a  href="<?php echo BASE_URL.'bpo';?>">SI BPO | DPO</a></li>
          <li ><a  href="<?php echo BASE_URL.'training';?>">SI Training Activity</a></li>
          <li ><a  href="<?php echo BASE_URL.'access_aidit';?>">SI Access Audit</a></li>
          <li ><a  href="<?php echo BASE_URL.'agencies';?>">SI Agencies Support</a></li>
          <li ><a  href="<?php echo BASE_URL.'advocacy';?>">SI Advocacy Activity</a></li>
          <li ><a  href="<?php echo BASE_URL.'iec';?>">SI IEC Activity</a></li>
          <li ><a  href="<?php echo BASE_URL.'bcc';?>">SI BCC Activity</a></li>
          <li ><a  href="<?php echo BASE_URL.'si_yearly_targets';?>">SI Yearly Targets</a></li>
         <!-- <li ><a  href="javascript:void(0)">Errors in PWD</a></li>  -->
        </ul>
      </li>
	  
	 
      <li ng-class="{active: m.ueh}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Urban Eye Health</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.ueh}" aria-expanded="false">
          <li ><a  href="<?php echo BASE_URL.'ueh_main_entry/';echo base64_encode('urban eye health-main entry');?>">Main Entry</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_vision_center';?>">Vision Center</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_base_hospital';?>">Base Hospital</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_outreach_camp';?>">Outreach Camp</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_training_entry';?>">Training | Capacity Building</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_advocacy_entry';?>">Advocacy | Networking | Liasion</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_bcc_entry';?>">BCC Entry</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_iec_entry';?>">IEC</a></li>
        </ul>
      </li>
        </ul>
      </li>

      <li ng-class="{active: m.admin}">
        <a href=""><i class="fa fa-gears"></i> <span class="nav-label">Administration</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.admin}" aria-expanded="false">
		  <li ><a  href="<?php echo BASE_URL.'Administration/';echo 'districts';?>">Districts</a></li>
          <li ><a  href="<?php echo BASE_URL.'Administration/';echo 'partner';?>">Partners</a></li>          
          <li ><a  href="<?php echo BASE_URL.'Administration/';echo 'user';?>">Users</a></li>
          <li ><a  href="<?php echo BASE_URL.'Administration/';echo 'reporting_quarters';?>">Reporting Quarters</a></li>
        </ul>
      </li>
	  <?php } if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==6) {} ?>
    </ul>
    </div>
</nav>
<div id="page-wrapper" class="gray-bg">
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary SeeMore2 " href="#"> <i class="fa fa-caret-left" aria-hidden="true"></i>	
           </a>

        </div>
		
        <ul class="nav navbar-top-links navbar-right">
	      <?php   if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==6) {  ?>
		   <li>
			<select class="form-control" id="selectId" onchange="part_fill(this.value);program_district(this.value);">
				<option value="">Select partner</option>
				<?php
                  if(!empty($dis)){
                    foreach($dis as $dist){ ?>
					<option value="<?php echo $dist->ss_partners_id ?>"<?php if($dist->ss_partners_id==$_SESSION["partner_id"])echo 'selected'; else echo ''; ?>><?php echo $dist->ss_partners_name; ?></option>
				  <?php } } ?>
				</select>
	      </li>
			
			<?php  }  ?>
          <li>
            <div class="text-muted welcome-message">
              <a ui-sref="main.profile" class="ng-binding"><span class="user-img"><img src="<?php echo PUBLIC_URL;?>img/default-user-icon-profile.png" alt=""></span> Hi <?php   echo $this->session->userdata('userinfo')['fname'] ?></a>
            </div>
          </li>
          <li>
            <a href="<?php echo BASE_URL.'logout';?>">
              <i class="fa fa-sign-out"></i> Log out
            </a>
          </li>
        </ul>
    </nav>
</div>

<div class="row wrapper border-bottom white-bg page-heading ng-scope">
    <div class="col-lg-10">
        <h2><?php echo @strtoupper($this->session->userdata['userinfo']['partner_name']); ?></h2>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeIn ng-scope">
<?php if(isset($_SESSION['success']))
{ ?>
<div class="alert alert-success">
  <strong></strong> <?php echo $_SESSION['success'] ; ?>
</div>
<?php } ?>
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox">
		<div class="col-md-12 ibox-title">
			<div class="col-md-10">
				
				  <h5>Rural Eye Health / REH Training | Capacity Building</h5>
				
			</div>
			<div class="col-md-2">
				<a href="<?php echo BASE_URL.'Training&CB Format.xlsx' ;?>"><button class="btn btn-sm btn-primary btn-block" type="button">
					Download Tamplate
				</button></a>
			</div>
		</div>
        
        <div class="ibox-content">
		<?php
       //  $attributes = array('id' => 'myform');
	//	echo form_open('filter_training_reh',$attributes); ?>
		<form method="get" action="<?php echo BASE_URL.'filter_training_reh/'; ?>">
			<div class="row flex-element center-both">
            <div class="well well-sm main-entry-box entry-box95">
              <div class="col-md-4">
			   <?php
				if(!isset($_SESSION["month_date"])) {
				  $_SESSION["month_date"] = date("m-Y", strtotime("-1 months"));
				 }
				  ?>
               <input type="text" name="month_from" value="<?php echo $_SESSION["month_date"]; ?>" id="month_from" class="form-control datepicker" onchange="date_fill()">
			   <span class="text-danger ng-binding ng-hide" aria-hidden="true"><?php echo form_error('month_data');?>
				<?php echo form_error('month_from');?>
			   </span>
              </div>
              <div class="col-md-2">
                <button class="btn btn-sm btn-primary btn-block" type="submit">
                Get Data
              </button>
              </div>
            </form>
			<?php
				$form_attrib = array('id'=>'excelupload_reh_training_cb');
				echo form_open_multipart('excelupload_reh_training_cb',$form_attrib);  ?>
						<div class="col-md-4">
							<input type="file" name="upload_publish_file" class="form-control">
							<p class="ng-binding ng-hide" aria-hidden="true" style="font-size:9px;">You can upload the data using the template provided on the website</p>
							<span class="text-danger ng-binding ng-hide" aria-hidden="true" id="error_file"></span>
						</div> 
						<div class="col-md-2">
							<button class="btn btn-sm btn-primary btn-block" onclick="submit_form_file();" type="button">
								Upload Data
							</button>
						</div>
				</div>
			</div>
			<input type="hidden" name="month_data"/>
			<?php echo form_close();  ?>
			
			
			
          

          <div class="row " ng-show="hasData" aria-hidden="false" style="">
            <div class="col-md-12">
			<?php echo form_open('create_training_reh');?>
              <div class="panel panel-primary">
                <div class="panel-heading">
                  Training / CB Entry
                </div>				
                <div class="panel-body">
                  <div class="row">
					<div class="col-md-3">
                      <div class="form-group">
					   <input type="hidden"  name="id_ajax" id="id_ajax1">
					 	<label>S.No.</label>
						<input type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="training_serialno" aria-invalid="false">
                        
                      </div>
                      <p class="text-danger ng-binding ng-hide" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
					   	<label>Name of the Block / Location</label>
						<input type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="training_block" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Name of the Person</label> 
						<input type="text" name="training_person" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Gender</label>
                        <select class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="training_gender" id="training_gender-ajax" aria-invalid="false">
							<option value="">Select</option>
							<option value="Male">Male</option>
							<option value="Female">Female</option>
							<option value="Other">Other</option>
                        </select>
                        <p class="text-danger ng-binding ng-hide" aria-hidden="true"></p>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Designation</label>
                        <select class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="training_designation" id="training_designation-ajax" aria-invalid="false">
							<option value="">Select</option>
							<option value="Project Staff" class="ng-binding ng-scope">Project Staff</option>
							<option value="ANMs" class="ng-binding ng-scope">ANMs</option>
							<option value="ASHA Workers" class="ng-binding ng-scope">ASHA Workers</option>
							<option value="Opthalmic Assistants" class="ng-binding ng-scope">Opthalmic Assistants</option>
							<option value="Optometrists" class="ng-binding ng-scope">Optometrists</option>
							<option value="OT Assistants" class="ng-binding ng-scope">OT Assistants</option>
							<option value="Opthalmologist" class="ng-binding ng-scope">Opthalmologist</option>
							<option value="Anganwadi Workers" class="ng-binding ng-scope">Anganwadi Workers</option>
							<option value="Health Providers" class="ng-binding ng-scope">Health Providers</option>
							<option value="Low Vision Specialist" class="ng-binding ng-scope">Low Vision Specialist</option>
							<option value="Other Specialist" class="ng-binding ng-scope">Other Specialist</option>
							<option value="District Programme Manager (DPM)" class="ng-binding ng-scope">District Programme Manager (DPM)</option>
							<option value="State Level Officials" class="ng-binding ng-scope">State Level Officials</option>
							<option value="Community Health Volunteers" class="ng-binding ng-scope">Community Health Volunteers</option>
							<option value="CBO members" class="ng-binding ng-scope">CBO members</option>
							<option value="VHNC Members" class="ng-binding ng-scope">VHNC Members</option>
							<option value="Panchayat Representatives" class="ng-binding ng-scope">Panchayat Representatives</option>
							<option value="Vision Technician" class="ng-binding ng-scope">Vision Technician</option>
							<option value="Other" class="ng-binding ng-scope">Other</option>
                        </select>
                        <p class="text-danger ng-binding ng-hide" aria-hidden="true"></p>
                      </div>
                    </div>
                    <div class="col-md-3" style="display:none;">
                      <div class="form-group">
                        <label>If designation is other, please specify</label> 
						<input type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="training_designation_detail" aria-invalid="false">
                        <p class="text-danger ng-binding ng-hide" ng-show="errors.designation_details" aria-hidden="true"></p>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Location of the Activity</label>
                        <select class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="training_loc_activity" id="training_loc_activity-ajax" aria-invalid="false">
							<option value="">Select</option>
							<option value="Partner" class="ng-binding ng-scope">Partner</option>
							<option value="Private location" class="ng-binding ng-scope">Private location</option>
							<option value="Other NGO Hospital" class="ng-binding ng-scope">Other NGO Hospital</option>
							<option value="Government Hospital" class="ng-binding ng-scope">Government Hospital</option>
							<option ng-repeat="location in activity_locations" value="Other" class="ng-binding ng-scope">Other</option>
                        </select>
                        <p class="text-danger ng-binding ng-hide" aria-hidden="true"></p>
                      </div>
                    </div>
                    <div class="col-md-3" style="display:none">
                      <div class="form-group">
                        <label>If location is other, please specify</label>
                        <input type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="training_loc_activity_detail" aria-invalid="false">
                        <p class="text-danger ng-binding ng-hide" aria-hidden="true"></p>
                      </div>
                    </div>
				  	<div class="col-md-3">
                      <div class="form-group">
                        <label>Activity</label>
                        <select class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="training_activity" id="training_activity-ajax" aria-invalid="false">
							<option value="">Select</option>
							<option value="Training" class="ng-binding ng-scope">Training</option>
							<option value="Exposure Visit" class="ng-binding ng-scope">Exposure Visit</option>
							<option value="Other" class="ng-binding ng-scope">Other</option>
                        </select>
                        <p class="text-danger ng-binding ng-hide" aria-hidden="true"></p>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>If activity is other, please specify</label> 
						<input type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="training_activity_detail" aria-invalid="false">
                        <p class="text-danger ng-binding ng-hide" aria-hidden="true"></p>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Participants From</label>
                        <select class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="training_participant_from" id="training_participant_from-ajax" aria-invalid="false">
							<option value="">Select</option>
							<option value="Partner" class="ng-binding ng-scope">Partner</option>
							<option value="Private Hospital" class="ng-binding ng-scope">Private Hospital</option>
							<option value="NGO Hospitals" class="ng-binding ng-scope">NGO Hospitals</option>
							<option value="Govt. Hospital" class="ng-binding ng-scope">Govt. Hospital</option>
							<option value="NPCB Programme" class="ng-binding ng-scope">NPCB Programme</option>
							<option value="Blindness Control Programme" class="ng-binding ng-scope">Blindness Control Programme</option>
							<option value="NRHM" class="ng-binding ng-scope">NRHM</option>
							<option value="Other" class="ng-binding ng-scope">Other</option>
                        </select>
                        <p class="text-danger ng-binding ng-hide" aria-hidden="true"></p>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>If Participants is other, please specify</label>
                        <input type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="training_participant_from_detail" aria-invalid="false">
                        <p class="text-danger ng-binding ng-hide" aria-hidden="true"></p>
                      </div>
                    </div>
					 <div class="col-md-4">
							<div class="form-group">
								<label>Topic Covered under training or capacity building</label>
								<input type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="training_topic_cover" aria-invalid="false">
							</div>
						</div>
                  </div>
				   
                </div>
				
                <div class="panel-footer">
                  <div class="flex-element  align-bw">
                    <div class="footer-text">
                      <h5>Please review your changes before you save them. Changes once saved cannot be
                reverted.</h5>
                    </div>
					<input type="hidden" name="month_data"/>
                    <div class="save-btn">
                      <button class="btn  btn-primary" type="submit">
                          Save Changes
                      </button>
                    </div>
                  </div>
                </div>
              </div>
			  <?php echo form_close();?>
			  <?php if(!empty($training_data)){ ?>
              <div class="table-responsive">
              <table id="table_data" class="table table-bordered">
                <thead>
					<tr>
						<th>S. No.</th>
					  <th>Name of the Block / Location</th>
					  <th>Name of the Person</th>
					  <th>Gender</th>
					  <th>Designation</th>
					  <th>Location Of the Activity</th>
					  <th>Activity</th>
					  <th>If activity is other, please specify</th>
					  <th>Participants from</th>
					  <th>If Participants is other, please specify</th>
					  <th>Action1</th>
					  <th>Action2</th>
					</tr>
				</thead>
                <tbody>
					
				<?php  foreach($training_data as $tdata){?>
					<tr>
						<td><?php echo $tdata->ss_sno;?><input type="hidden" name="csrf_test_name1" value="<?php echo $this->security->get_csrf_hash(); ?>"></td>
						<td><?php echo $tdata->ss_reh_training_block;?></td>
						<td><?php echo $tdata->ss_reh_training_person;?></td>
						<td><?php echo $tdata->ss_reh_training_gender;?></td>
						<td><?php echo $tdata->ss_reh_training_designation;?></td>
						<td><?php echo $tdata->ss_reh_training_loc_activity;?></td>
						<td><?php echo $tdata->ss_reh_training_activity;?></td>
						<td><?php echo $tdata->ss_reh_training_activity_detail;?></td>
						<td><?php echo $tdata->ss_reh_training_participant_from;?></td>
						<td><?php echo $tdata->ss_reh_training_participant_from_detail;?></td>
						<td><a onclick="return confirmDelete();" href="<?php echo BASE_URL.'delete_training_reh/'.base64_encode($tdata->ss_reh_training_id); ?>">Delete</a></td>
            <td><a href="javascript:void(0)" class="editThis" data-val="<?php echo base64_encode($tdata->ss_reh_training_id); ?>">Edit</a></td>
					</tr>
				  <?php } ?>
                </tbody>
				<?php //echo $this->pagination->create_links(); ?>
              </table>
            </div>
			  <?php } ?>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
<script src="<?php echo PUBLIC_URL; ?>js/jquery-2.1.1.js"></script>
<script>

$(".editThis").click(function(){
	
	var dataid = $(this).attr('data-val');
	var cct = $("input[name='csrf_test_name']").val();
	//alert(cct);
	//var cct = $.cookie("<?php echo $this->config->item("csrf_cookie_name"); ?>");
	post_data ={ id: dataid,<?php echo $this->security->get_csrf_token_name(); ?> : cct};
	if(dataid!=0 || dataid !="")
	{
		
    $.ajax({ 
            url: "<?php echo BASE_URL.'reh_training/edit_training_reh';?>",
            data: post_data,
            type: 'POST'
        }).done(function(responseData) { //alert(responseData); return false;
		
		var csrf_t = responseData.split("$$$");
		var obj = JSON.parse(csrf_t[0]);
        
		$("input[name='csrf_test_name']").val(csrf_t[1]);
	    
		$("input[name='date_month']").val(obj['ss_reh_training_month']);
		
		$("input[name='training_serialno']").val(obj['ss_sno']);
		$("input[name='training_block']").val(obj['ss_reh_training_block']);
	    $("input[name='training_person']").val(obj['ss_reh_training_person']);
		var training_gender_ajax = obj['ss_reh_training_gender'];
		
		var training_designation_ajax = obj['ss_reh_training_designation'];
	    $("input[name='training_designation_detail']").val(obj['ss_reh_training_designation_detail']);
		var training_loc_activity_ajax = obj['ss_reh_training_loc_activity'];
		
		$("input[name='training_loc_activity_detail']").val(obj['ss_reh_training_loc_activity_detail']);
	    var training_activity_ajax = obj['ss_reh_training_activity'];
		$("input[name='training_activity_detail']").val(obj['ss_reh_training_activity_detail']);
		
		var training_participant_from_ajax = obj['ss_reh_training_participant_from'];
	    $("input[name='training_participant_from_detail']").val(obj['ss_reh_training_participant_from_detail']);
		$("input[name='training_topic_cover']").val(obj['ss_reh_training_topic_cover']);
		
		$("input[name='id_ajax']").val(obj['ss_reh_training_id']);
		
	  // for  training_gender_ajax  
	  $("#training_gender-ajax option").each(function (key,value) { 
		   if(training_gender_ajax==$(this).val())
		   {
			 $(this).prop('selected', true);  
		   }
	  });  
	  
	  // for  training_designation-ajax  
	  $("#training_designation-ajax option").each(function (key,value) { 
		   if(training_designation_ajax==$(this).val())
		   {
			 $(this).prop('selected', true);  
		   }
	  });  
	  
	  // for  training_designation-ajax  
	  $("#training_designation-ajax option").each(function (key,value) { 
		   if(training_designation_ajax==$(this).val())
		   {
			 $(this).prop('selected', true);  
		   }
	  }); 
       
	  // for  training_designation-ajax  
	  $("#training_loc_activity-ajax option").each(function (key,value) { 
		   if(training_loc_activity_ajax==$(this).val())
		   {
			 $(this).prop('selected', true);  
		   }
	  }); 
	  
	  // for  training_activity-ajax  
	  $("#training_activity-ajax option").each(function (key,value) { 
		   if(training_activity_ajax==$(this).val())
		   {
			 $(this).prop('selected', true);  
		   }
	  }); 
	  
	  // for  training_activity-ajax  
	  $("#training_participant_from-ajax option").each(function (key,value) { 
		   if(training_participant_from_ajax==$(this).val())
		   {
			 $(this).prop('selected', true);  
		   }
	  }); 
	
        console.log('Done: ', responseData);
        }).fail(function() {
            console.log('Failed');
        });
	}
})

</script>


<script type="text/javascript">
    function confirmDelete() 
	{
        return confirm('Do you really wants to delete?');
    }
</script>
<script>
function submit_form_file()
{
	if($('input[name="upload_publish_file"]').val() == "")
	{
		$("#error_file").html("Please select file");
	}
	else if($('input[name="month_data"]').val() == "")
	{
		$("#error_month").html("Please select month");
	}
	else{
		$("#excelupload_reh_training_cb").submit();
	}
}
</script>