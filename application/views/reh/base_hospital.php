
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Sight Savers</title>
        <link href="<?php echo PUBLIC_URL; ?>css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo PUBLIC_URL; ?>font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo PUBLIC_URL; ?>css/animate.css" rel="stylesheet">
        <link href="<?php echo PUBLIC_URL; ?>css/style.css" rel="stylesheet">
        <link href="<?php echo PUBLIC_URL; ?>css/custom.css" rel="stylesheet">
        <link href="<?php echo PUBLIC_URL; ?>css/lc_switch.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo PUBLIC_URL; ?>css/codemirror.css">
        <link rel="stylesheet" href="<?php echo PUBLIC_URL; ?>css/custom.css">
		 <link href="<?php echo PUBLIC_URL; ?>css/bootstrap_datepicker.css" rel="stylesheet"> 
     	<link href="<?php echo PUBLIC_URL; ?>css/dataTables.bootstrap.min.css" rel="stylesheet">
    </head>
<body class="fixed-sidebar">

<div id="wrapper">

<nav class="navbar-admin navbar-static-side" role="navigation">
    <div class="sidebar-collapse">

      <ul side-navigation="" class="nav metismenu" id="side-menu">
      <li class="nav-header" style="padding: 20px;">
        <div class="profile-element">
          <img alt="image" width="100%" src="<?php echo PUBLIC_URL; ?>img/logo.png" src="<?php echo PUBLIC_URL; ?>assets/images/logo.png">
        </div>
        <div class="logo-element">
          SS
        </div>
      </li>
	 
 <?php 
  if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==6)  { ?> 
	   <li ng-class="{active: m.ie}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Data Entry</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" id="menhead123" ng-class="{in: m.ie}" aria-expanded="false">
	  
     </ul>
      </li>
		 <li class="active">
        	<a href="<?php echo BASE_URL.'partner_mpr';?>"><i class="fa fa-eye"></i> <span class="nav-label">Review MPR</span></a>
        
      	</li>
     <?php }  ?>
	 
	   <?php  if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==1 || $this->session->userdata('userinfo')['default_role']==5 || $this->session->userdata('userinfo')['default_role']==2 || $this->session->userdata('userinfo')['default_role']==3 || $this->session->userdata('userinfo')['default_role']==4)  { ?>
	  
	   <li ng-class="{active: m.dashboards}" >
		  <li class="active">
			<a href="<?php echo BASE_URL.'dashboard_speedometer';?>"><i class="fa fa-dashboard"></i> <span class="nav-label">Dashboard</span> </a>
			</li>
			<!--li><a href="<?php //echo BASE_URL.'dashboard';?>">Report</a></li-->
			  <li ng-class="{active: m.reports}">
				<a href=""><i class="fa fa-file-pdf-o"></i> <span class="nav-label">Graph</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse" ng-class="{in: m.reports}" aria-expanded="true">
				  <li  ><a href="#">Social Inclusion</a></li>
				  <li ><a  href="<?php echo BASE_URL.'dashboard_graph';?>">Rural Eye Health</a></li>
				  <li ><a  href="<?php echo BASE_URL.'dashboard_graph_ueh';?>">Urban Eye Health</a></li>
				  <li ><a  href="#">Inclusive Education</a></li>
				</ul>
			  </li>
		  </li>
			<li ng-class="{active: m.reports}" >
				<a href=""><i class="fa fa-file-pdf-o"></i> <span class="nav-label">MPR</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse" ng-class="{in: m.reports}" aria-expanded="true">
				  <li  ><a href="<?php echo BASE_URL.'mpr_si';?>">Social Inclusion</a></li>
				  <li ><a  href="<?php echo BASE_URL.'mpr_reh' ;?>">Rural Eye Health</a></li>
				  <li ><a  href="<?php echo BASE_URL.'mpr_ueh';?>">Urban Eye Health</a></li>
				  <li ><a  href="<?php echo BASE_URL.'mpr_ie';?>">Inclusive Education</a></li>
				</ul>
			</li>
			
			<li ng-class="{active: m.reports}" >
				<a href=""><i class="fa fa-file-pdf-o"></i> <span class="nav-label">Yearly Target</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse" ng-class="{in: m.reports}" aria-expanded="true">
     			 <li ><a  href="<?php echo BASE_URL.'reh_yearly_target/';echo base64_encode('rural eye health-yearly target');?>">REH Yearly Targets</a></li>
				   <li ><a  href="<?php echo BASE_URL.'ueh_yearly_target/';echo base64_encode('urban eye health-yearly target');?>">UEH Yearly Targets</a></li>
				</ul>
			</li>
			
		<?php if($this->session->userdata('userinfo')['default_role']==2 || $this->session->userdata('userinfo')['default_role']==3 || $this->session->userdata('userinfo')['default_role']==4) { ?>
		<li ng-class="{active: m.reports}" >
				<a href=""><i class="fa fa-file-pdf-o"></i> <span class="nav-label">Data approval</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse" ng-class="{in: m.reports}" aria-expanded="true">
				  <li  ><a href="<?php echo BASE_URL.'Approval';?>">Review MPR</a></li>				  
				</ul>
			</li>
		<?php }  ?>
		<?php  if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==1) { ?>
		<li ng-class="{active: m.reports}" >
				<a href=""><i class="fa fa-file-pdf-o"></i> <span class="nav-label">Data approval</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse" ng-class="{in: m.reports}" aria-expanded="true">
				  <li  ><a href="<?php echo BASE_URL.'Approvaladmin';?>">Review MPR</a></li>				  
				</ul>
			</li>
		<?php } ?>
		
		
		
	  
	  <li ng-class="{active: m.ie}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Data Entry</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.ie}" aria-expanded="false">
        <li ng-class="{active: m.ie}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Inclusive Education</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.ie}" aria-expanded="false">
          <li ><a href="<?php echo BASE_URL.'ie_main_entry/';echo base64_encode('Inclusive education-main entry'); ?>">IE Main Entry</a></li>
        </ul>
      </li>

      <li ng-class="{active: m.reh}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Rural Eye Health</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.reh}" aria-expanded="false">
          <li ><a  href="<?php echo BASE_URL.'reh_main_entry/';echo base64_encode('rural eye health-main entry');?>">REH Main Entry</a></li>
          <!--<li ><a  href="<?php //echo BASE_URL.'reh_refferal';?>">Referrals</a></li>-->
		  <li><a href="<?php echo BASE_URL.'reh_base_hospital';?>">REH Base Hospital</a></li>
          <li ><a  href="<?php echo BASE_URL.'reh_training';?>">REH Training | Capacity Building</a></li>
          <li ><a  href="<?php echo BASE_URL.'reh_advocacy';?>">REH Advocacy | Networking | Liasion</a></li>
          <li ><a  href="<?php echo BASE_URL.'reh_bcc';?>">REH BCC Entry</a></li>
          <li ><a  href="<?php echo BASE_URL.'reh_iec';?>">REH IEC Entry</a></li>
          <li ><a  href="<?php echo BASE_URL.'reh_vhsnc';?>">VHSNC Entry</a></li>
         <!-- <li ><a  href="javascript:void(0)">Errors in submission</a></li>  -->
        </ul>
      </li>
	  
      <li ng-class="{active: m.si}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Social Inclusion</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.si}" aria-expanded="false">
          <li ><a  href="<?php echo BASE_URL.'si_pwd_search';?>">SI PWD Search</a></li>
          <li ><a  href="<?php echo BASE_URL.'si_main_entry/';echo base64_encode('Social inclusion-main entry');?>">SI Main Entry</a></li>
          <li ><a  href="<?php echo BASE_URL.'shg';?>">SI SHG | PPG</a></li>
          <li ><a  href="<?php echo BASE_URL.'bpo';?>">SI BPO | DPO</a></li>
          <li ><a  href="<?php echo BASE_URL.'training';?>">SI Training Activity</a></li>
          <li ><a  href="<?php echo BASE_URL.'access_aidit';?>">SI Access Audit</a></li>
          <li ><a  href="<?php echo BASE_URL.'agencies';?>">SI Agencies Support</a></li>
          <li ><a  href="<?php echo BASE_URL.'advocacy';?>">SI Advocacy Activity</a></li>
          <li ><a  href="<?php echo BASE_URL.'iec';?>">SI IEC Activity</a></li>
          <li ><a  href="<?php echo BASE_URL.'bcc';?>">SI BCC Activity</a></li>
          <li ><a  href="<?php echo BASE_URL.'si_yearly_targets';?>">SI Yearly Targets</a></li>
         <!-- <li ><a  href="javascript:void(0)">Errors in PWD</a></li>  -->
        </ul>
      </li>
	  
	 
      <li ng-class="{active: m.ueh}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Urban Eye Health</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.ueh}" aria-expanded="false">
          <li ><a  href="<?php echo BASE_URL.'ueh_main_entry/';echo base64_encode('urban eye health-main entry');?>">Main Entry</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_vision_center';?>">Vision Center</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_base_hospital';?>">Base Hospital</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_outreach_camp';?>">Outreach Camp</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_training_entry';?>">Training | Capacity Building</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_advocacy_entry';?>">Advocacy | Networking | Liasion</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_bcc_entry';?>">BCC Entry</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_iec_entry';?>">IEC</a></li>
        </ul>
      </li>
        </ul>
      </li>

      <li ng-class="{active: m.admin}">
        <a href=""><i class="fa fa-gears"></i> <span class="nav-label">Administration</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.admin}" aria-expanded="false">
		  <li ><a  href="<?php echo BASE_URL.'Administration/';echo 'districts';?>">Districts</a></li>
          <li ><a  href="<?php echo BASE_URL.'Administration/';echo 'partner';?>">Partners</a></li>          
          <li ><a  href="<?php echo BASE_URL.'Administration/';echo 'user';?>">Users</a></li>
          <li ><a  href="<?php echo BASE_URL.'Administration/';echo 'reporting_quarters';?>">Reporting Quarters</a></li>
        </ul>
      </li>
	  <?php } if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==6) {} ?>
    </ul>
    </div>
</nav>
<div id="page-wrapper" class="gray-bg">
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary SeeMore2 " href="#"> <i class="fa fa-caret-left" aria-hidden="true"></i>	
           </a>

        </div>
		
        <ul class="nav navbar-top-links navbar-right">
	      <?php   if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==6) {  ?>
		   <li>
			<select class="form-control" id="selectId" onchange="part_fill(this.value);program_district(this.value);">
				<option value="">Select partner</option>
				<?php
                  if(!empty($dis)){
                    foreach($dis as $dist){ ?>
					<option value="<?php echo $dist->ss_partners_id ?>"<?php if($dist->ss_partners_id==$_SESSION["partner_id"])echo 'selected'; else echo ''; ?>><?php echo $dist->ss_partners_name; ?></option>
				  <?php } } ?>
				</select>
	      </li>
			
			<?php  }  ?>
          <li>
            <div class="text-muted welcome-message">
              <a ui-sref="main.profile" class="ng-binding"><span class="user-img"><img src="<?php echo PUBLIC_URL;?>img/default-user-icon-profile.png" alt=""></span> Hi <?php   echo $this->session->userdata('userinfo')['fname'] ?></a>
            </div>
          </li>
          <li>
            <a href="<?php echo BASE_URL.'logout';?>">
              <i class="fa fa-sign-out"></i> Log out
            </a>
          </li>
        </ul>
    </nav>
</div>
<div class="row wrapper border-bottom white-bg page-heading ng-scope">
  <div class="col-lg-10">
    <h2><?php //echo @strtoupper($this->session->userdata['userinfo']['partner_name']); ?></h2>
  </div>
</div>

<div class="wrapper wrapper-content animated fadeIn ng-scope" ng-init="init()">
<?php if(isset($_SESSION['success']))
{ ?>
<div class="alert alert-success">
  <strong></strong> <?php echo $_SESSION['success'] ; ?>
</div>
<?php } ?>
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox">
			<div class="col-md-12 ibox-title">
			<div class="col-md-10">
				
				  <h5>Rural Eye Health / REH Base Hospital</h5>
				
			</div>
			<div class="col-md-2">
				<a href="<?php echo BASE_URL.'BaseHospitalTemplate_DD.xlsx' ;?>"><button class="btn btn-sm btn-primary btn-block" type="button">
					Download Tamplate
				</button></a>
			</div>
		</div>
		
        <div class="ibox-content">
		<?php 
		//$attributes = array('id' => 'myform');
		//echo form_open('filter_base_reh',$attributes);?>
		<form method="get" action="<?php echo BASE_URL.'filter_base_reh/'; ?>">
          <div class="row flex-element center-both">
            <div class="well well-sm main-entry-box entry-box95">
              <div class="col-md-4">
			   <?php
			  if(!isset($_SESSION["month_date"])) {
			 $server_date = server_date_time(); 
			 date('m-Y',strtotime($server_date));
			 $_SESSION["month_date"] = date('m-Y',strtotime($server_date));
			  }
				?> 
               <input type="text" name="month_from"  id="month_from" value="<?php echo $_SESSION["month_date"]; ?>" class="form-control datepicker" onchange="date_fill()">
			   <span class="text-danger ng-binding ng-hide" id="error_month" aria-hidden="true"><?php echo form_error('month_data');?>
				<?php echo form_error('month_from');?>
			   </span>
              </div>
              <div class="col-md-2">
                <button class="btn btn-sm btn-primary btn-block" type="submit">
                Get Data
              </button>
              </div>
            </form>
			<?php
				$form_attrib = array('id'=>'excelupload_reh_base');
				echo form_open_multipart('excelupload_reh_base',$form_attrib);  ?>
						<div class="col-md-4">
							<input type="file" name="upload_publish_file" class="form-control">
							<p class="ng-binding ng-hide" aria-hidden="true" style="font-size:9px;">You can upload the data using the template provided on the website</p>
							<span class="text-danger ng-binding ng-hide" aria-hidden="true" id="error_file"></span>
						</div> 
						<div class="col-md-2">
							<button class="btn btn-sm btn-primary btn-block" onclick="submit_form_file();" type="button">
								Upload Data
							</button>
						</div>
				</div>
			</div>
			<input type="hidden" name="month_data"/>
				<?php echo form_close();  ?>

          <div class="row " ng-show="hasData" aria-hidden="false" style="">
            <div class="col-md-12">
			<?php echo form_open('create_base_reh');?>
			<input type="hidden"  name="id_ajax" id="id_ajax1">
              <div class="panel panel-primary">
                <div class="panel-heading">
                  Base Hospital
                </div>
                <div class="panel-body">
                  <div class="row">
				 	<div class="col-md-4">
                      <div class="form-group">
                        <label>S. No.</label>
                        <input type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="opd_ssno" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>OPD No.</label>
                        <input type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="opd_reh" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Name of the Cataract Patient</label> <input type="text" name="patient_name" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" aria-hidden="true"></p>
                    </div>
                    
                  </div>

                  <div class="row">
					 <div class="col-md-4">
                      <div class="form-group">
                        <label>Next of Kin Name</label>
                        <input type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="kin_name" aria-invalid="false">
                        <p class="text-danger ng-binding ng-hide" aria-hidden="true"></p>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Village</label>
                        <input type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="village" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Block</label> <input type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="block" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" aria-hidden="true"></p>
                    </div>
                    
                  </div>

                  <div class="row">
					 <div class="col-md-4">
                      <div class="form-group">
                        <label>District</label>
                        <input type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="district" aria-invalid="false">
                        <p class="text-danger ng-binding ng-hide" aria-hidden="true"></p>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Age</label>
                        <input type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty number-only" name="age" aria-invalid="false" >
                      </div>
                      <p class="text-danger ng-binding ng-hide" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Gender</label>
                        <select class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="gender" id="gender-ajax" aria-invalid="false">
						<option value="">Select</option>
                          <option value="Male">Male</option>
                          <option value="Female">Female</option>
						  <option value="Other">Transgender</option>
                        </select>
                        <p class="text-danger ng-binding ng-hide" aria-hidden="true"></p>
                      </div>
                    </div>
                    
                  </div>

                  <div class="row">
					<div class="col-md-4">
                      <div class="form-group">
                        <label>Referred from or Referred by</label> 
						 <select class="form-control ng-pristine ng-untouched ng-valid ng-empty" id="ref_fr_by-ajax" name="ref_fr_by" aria-invalid="false">
						<option value="">Select</option>
                          <option value="Vision Center">Vision Center</option>
                          <option value="Outreach Camp">Outreach Camp</option>
						  <option value="NGO Staff">NGO Staff</option>
						  <option value="DR Camp">DR Camp</option>
						  <option value="Community health Sanitation and Nutrition Committee(VHSNC)">Community health Sanitation and Nutrition Committee(VHSNC)</option>
						  <option value="Not refered by anyone (Walk-in)">Not refered by anyone (Walk-in)</option>
                        </select>
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.person_name" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Eye Operated (Left Eye / Right Eye)</label>
                        <select class="form-control ng-pristine ng-untouched ng-valid ng-empty" id="eye_oper-ajax" name="eye_oper" aria-invalid="false">
						<option value="">Select</option>
                          <option value="Left Eye">Left Eye</option>
                          <option value="Right Eye">Right Eye</option>
						  <option value="Both Eyes">Both Eyes</option></select>
                      </div>
                      <p class="text-danger ng-binding ng-hide" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Pre Operative Visual Acuity (Left Eye / Right Eye)</label> 
						<select class="form-control ng-pristine ng-untouched ng-valid ng-empty" id="pre_oper-ajax" name="pre_oper" aria-invalid="false">
						<option value="">Select</option>
                          <option value="NOPL">NOPL</option>
                          <option value="PLP">PLP</option>
						  <option value="1/60">1/60</option>
						  <option value="2/60">2/60</option>
						   <option value="4/60">4/60</option>
						  <option value="5/60">5/60</option>
						  <option value="Below 6/20">Below 6/20</option>
						  <option value="3/60">3/60</option>
                          <option value="6/60">6/60</option>
						  <option value="6/36">6/36</option>
						  <option value="6/24">6/24</option>
						   <option value="6/18">6/18</option>
						  <option value="6/12">6/12</option>
						  <option value="Below 6/9">6/9</option>
						  <option value="Below 6/6">6/6</option>
						  
						  </select>
                      </div>
                      <p class="text-danger ng-binding ng-hide" aria-hidden="true"></p>
                    </div>
                   
                  </div>

                  <div class="row">
					  <div class="col-md-4">
                      <div class="form-group">
                        <label>Post Operative Visual Acuity (Left Eye / Right Eye)</label>
                        <select class="form-control ng-pristine ng-untouched ng-valid ng-empty" id="post_oper-ajax" name="post_oper" aria-invalid="false">
						<option value="">Select</option>
                          <option value="NOPL">NOPL</option>
                          <option value="PLP">PLP</option>
						  <option value="1/60">1/60</option>
						  <option value="2/60">2/60</option>
						   <option value="4/60">4/60</option>
						  <option value="5/60">5/60</option>
						  <option value="Below 6/20">Below 6/20</option>
						  <option value="3/60">3/60</option>
                          <option value="6/60">6/60</option>
						  <option value="6/36">6/36</option>
						  <option value="6/24">6/24</option>
						   <option value="6/18">6/18</option>
						  <option value="6/12">6/12</option>
						  <option value="6/9">6/9</option>
						  <option value="6/6">6/6</option>
						  </select>
                        <p class="text-danger ng-binding ng-hide" ng-show="errors.gender" aria-hidden="true"></p>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Type of surgery (Free/ Full Payment/ Subsidized)</label>
                        <select class="form-control ng-pristine ng-untouched ng-valid ng-empty" id="surg_type-ajax" name="surg_type" aria-invalid="false">
						<option value="">Select</option>
                          <option value="Free">Free</option>
                          <option value="Subsisiderzed">Subsisiderzed</option>
						  <option value="Paid">Paid</option> </select>
                      </div>
                      <p class="text-danger ng-binding ng-hide" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Surgery supported by</label> 
						<select class="form-control ng-pristine ng-untouched ng-valid ng-empty" id="surg_support-ajax" name="surg_support" aria-invalid="false">
						<option value="">Select</option>
                          <option value="Baxter India">Baxter India</option>
                          <option value="Central Coalfields Ltd">Central Coalfields Ltd</option>
						  <option value="Coal India">Coal India</option> 
						  <option value="Distict Blindness Control Society">Distict Blindness Control Society</option> 
						  <option value="Dubai Duty Free">Dubai Duty Free</option> 
						  <option value="Fresh Leaf Foundation">Fresh Leaf Foundation</option> 
						  <option value="Fullerton India">Fullerton India</option> 
						  <option value="IDFC Bank">IDFC Bank</option> 
						  <option value="Insurance Company">Insurance Company</option> 
						  <option value="Johnson & Johnson">Johnson & Johnson</option> 
						  <option value="Just Dial">Just Dial</option>
						  <option value="L&M">L&M</option>
						  <option value="Larsen & Toubro">Larsen & Toubro</option>
						  <option value="MECON Ltd">MECON Ltd</option>
						  <option value="Meru">Meru</option>
						  <option value="Novalis">Novalis</option>
						  <option value="Oracle">Oracle</option>
						  <option value="Piramal">Piramal</option>
						  <option value="Qatar Foundation">Qatar Foundation</option>
						  <option value="RAYBAN">RAYBAN</option>
						  <option value="Rotork International - India">Rotork International - India</option>
						  <option value="Rotork International - UK">Rotork International - UK</option>
						  <option value="RPG">RPG</option>
						  <option value="Sightsavers">Sightsavers</option>
						  <option value="Standard Chartered">Standard Chartered</option>
						  <option value="Steel Authority of India">Steel Authority of India</option>
						  <option value="Titan Eye">Titan Eye</option>
						  <option value="Vision Express">Vision Express</option>
						  <option value="Other">Other</option>
    					  </select>
                      </div>
                      <p class="text-danger ng-binding ng-hide" aria-hidden="true"></p>
                    </div>
                  </div>

                </div>
                <div class="panel-footer">
                  <div class="flex-element  align-bw">
                    <div class="footer-text">
                      <h5>Please review your changes before you save them. Changes once saved cannot be
                reverted.</h5>
                    </div>
					<input type="hidden" name="month_data"/>
                    <div class="save-btn">
                      <button class="btn  btn-primary" type="submit">
                          Save Changes
                      </button>
                    </div>
                  </div>
                </div>
              </div>
			  <?php echo form_close();?>
			  <?php if(!empty($base_data)){ ?>
              <div class="table-responsive">
              <table id="table_data" class="table table-bordered">
                <thead>
                  <tr>
				          <th>S.No.</th>
                  <th>OPD No.</th>
                  <th>Name of the Cataract Patient</th>
                  <th>Next of Kin Name </th>
                  <th>Village</th>
                  <th>Block</th>
                  <th>District</th>
                  <th>Age</th>
                  <th>Gender</th>
                  <th>Referred from or Referred by</th>
                  <th>Eye Operated (Left Eye / Right Eye)</th>
                  <th>Pre Operative Visual Acuity </th>
                  <th>Post Operative Visual Acuity </th>
                  <th>Type of surgery (Free/ Full Payment/ Subsidized)</th>
                  <th>Surgery supported by</th>
				  <th>Delete</th>
				  <th>Edit</th>
                </tr></thead>
                <tbody>
				
                  <?php foreach($base_data as $bdata){ ?>
				  <tr>
				  <td> <?php echo $bdata->ss_sno;?></td>
                  <td><?php echo $bdata->ss_opd_no;?></td>
                  <td><?php echo $bdata->ss_patient_name;?></td>
                  <td><?php echo $bdata->ss_kin_name;?></td>
                  <td><?php echo $bdata->ss_village;?></td>
                  <td><?php echo $bdata->ss_block;?></td>
                  <td><?php echo $bdata->ss_districts;?></td>
                  <td><?php echo $bdata->ss_age;?></td>
                  <td><?php echo $bdata->ss_gender;?></td>
                  <td><?php echo $bdata->ss_ref_from_ref_by;?></td>
                  <td><?php echo $bdata->ss_eye_oper;?></td>
                  <td><?php echo $bdata->ss_pre_oper;?></td>
                  <td><?php echo $bdata->ss_post_oper;?></td>
                  <td><?php echo $bdata->ss_surgery_type;?></td>
                  <td><?php echo $bdata->ss_surgery_support;?></td>
<td><a onclick="return confirmDelete();" href="<?php echo BASE_URL.'delete_base_reh/'.base64_encode($bdata->ss_reh_base_hospital_id);?>">Delete</a></td>
<td><a href="javascript:void(0)" class="editThis" data-val="<?php echo base64_encode($bdata->ss_reh_base_hospital_id); ?>">Edit</a></td>
                </tr>
				  <?php } ?>
                </tbody>
              </table>
            </div>
			  <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="<?php echo PUBLIC_URL; ?>js/jquery-2.1.1.js"></script>
<script>
        
       
	$(document).ready(function() {
			$(".number-only").keydown(function (e) {
					// Allow: backspace, delete, tab, escape, enter and .
					if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
							// Allow: Ctrl+A, Command+A
							(e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
							// Allow: home, end, left, right, down, up
							(e.keyCode >= 35 && e.keyCode <= 40)) {
									// let it happen, don't do anything
									return;
					}
					// Ensure that it is a number and stop the keypress
					if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
							e.preventDefault();
					}
			});
	});
        
</script>
<script>

$(".editThis").click(function(){
	var dataid = $(this).attr('data-val');
	var cct = $("input[name='csrf_test_name']").val();
	//alert(cct);
	//var cct = $.cookie("<?php echo $this->config->item("csrf_cookie_name"); ?>");
	post_data ={ id: dataid,<?php echo $this->security->get_csrf_token_name(); ?> : cct};
	if(dataid!=0 || dataid !="")
	{
    $.ajax({ 
            url: "<?php echo BASE_URL.'reh_base_hospital/edit_base_reh';?>",
            data: post_data,
            type: 'POST'
        }).done(function(responseData) { //alert(responseData); return false;
		
		var csrf_t = responseData.split("$$$");
		var obj = JSON.parse(csrf_t[0]);
        
		$("input[name='csrf_test_name']").val(csrf_t[1]);
	    //console.log(responseData); 
		//return false;
		$("input[name='opd_ssno']").val(obj['ss_sno']);
		$("input[name='date_month']").val(obj['ss_reh_base_month']);
	    $("input[name='opd_reh']").val(obj['ss_opd_no']);
		$("input[name='patient_name']").val(obj['ss_patient_name']);
		$("input[name='kin_name']").val(obj['ss_kin_name']);
		$("input[name='village']").val(obj['ss_village']);
		$("input[name='block']").val(obj['ss_block']);
		$("input[name='district']").val(obj['ss_districts']);
		$("input[name='age']").val(obj['ss_age']);
		var gender_ajax = obj['ss_gender'];
		var ref_fr_by_ajax = obj['ss_ref_from_ref_by'];
		var eye_oper_ajax = obj['ss_eye_oper'];
		var pre_oper_ajax = obj['ss_pre_oper'];
		var post_oper_ajax = obj['ss_post_oper'];
		var surg_type_ajax = obj['ss_surgery_type'];
		var surg_support_ajax = obj['ss_surgery_support'];
		 $("input[name='id_ajax']").val(obj['ss_reh_base_hospital_id']);
		
  // for gender     
	$("#gender-ajax option").each(function (key,value) { 
	   if(gender_ajax==$(this).val())
	   {
		 $(this).prop('selected', true);  
	   }
	 
	});  
// for ref_fr_by    
	$("#ref_fr_by-ajax option").each(function (key,value) { 
	   if(ref_fr_by_ajax==$(this).val())
	   {
		 $(this).prop('selected', true);  
	   }
	 
	});  
// for eye oper   
	$("#eye_oper-ajax option").each(function (key,value) { 
	   if(eye_oper_ajax==$(this).val())
	   {
		 $(this).prop('selected', true);  
	   }
	});	

// for eye oper   
	$("#pre_oper-ajax option").each(function (key,value) { 
	   if(pre_oper_ajax==$(this).val())
	   {
		 $(this).prop('selected', true);  
	   }
	});		
	
// for post oper   
	$("#post_oper-ajax option").each(function (key,value) { 
	   if(post_oper_ajax==$(this).val())
	   {
		 $(this).prop('selected', true);  
	   }
	});		
	
	// for surg_type    
	$("#surg_type-ajax option").each(function (key,value) { 
	   if(surg_type_ajax==$(this).val())
	   {
		 $(this).prop('selected', true);  
	   }
	});
	// for surg_support    
	$("#surg_support-ajax option").each(function (key,value) { 
	   if(surg_support_ajax==$(this).val())
	   {
		 $(this).prop('selected', true);  
	   }
	});

	//alert(responseData); return false;
            console.log('Done: ', responseData);
        }).fail(function() {
            console.log('Failed');
        });
	}
})


</script>

<script type="text/javascript">
    function confirmDelete() 
	{
        return confirm('Do you really wants to delete?');
    }
</script>


<script>
function submit_form_file()
{
	if($('input[name="upload_publish_file"]').val() == "")
	{
		$("#error_file").html("Please select file");
	}
	else if($('input[name="month_data"]').val() == "")
	{
		$("#error_month").html("Please select month");
	}
	else{
		$("#excelupload_reh_base").submit();
	}
}
</script>