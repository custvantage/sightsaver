
<div class="row wrapper border-bottom white-bg page-heading ng-scope">
    <div class="col-lg-10">
        <h2><?php // echo @strtoupper($this->session->userdata['userinfo']['partner_name']); ?></h2>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeIn ng-scope" ng-init="init()">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Rural Eye Health / REH Yearly Targets</h5>
                </div>
                <div class="ibox-content">
					<?php
					if($this->session->userdata('userinfo')['default_role'] == 6) //if partner
					{
					$form_attrib = array('id'=>'manage_reh_yt_entry');
					echo form_open('manage_reh_yt_entry/'.$this->uri->segment(2),$form_attrib);?>
                    <div class="row flex-element center-both">					
                        <div class="well well-sm main-entry-box">
                            <div class="col-md-3 text-right">
                                <label>Month:</label> 
                            </div>
                            <div class="col-md-5">
								<?php
								/* if(!isset($_SESSION["month_date"]))
							   {
								 $server_date = server_date_time(); 
							     date('m-Y',strtotime($server_date));
							     $_SESSION["month_date"] = date('m-Y',strtotime($server_date));
								} */
								  ?>
                                <input type="text" name="month_from" value="<?php //echo $_SESSION["month_date"]; ?>" id="month_from" class="form-control datepicker" onchange="date_fill()">
								<span class="text-danger ng-binding ng-hide" aria-hidden="true" id="error_month"><?php echo form_error('month_data');?></span>
                            </div>
                            <div class="col-md-4">
                                <button class="btn btn-sm btn-primary btn-block" onclick="submit_form('manage_reh_yt_entry');" type="button">
                                    Get Data
                                </button>
                            </div>
                        </div>						
                    </div>
					<?php echo form_close();?>
					<?php }else { //if pm/po ?>
					<?php $form_attrib = array('id'=>'manage_reh_yt_entry');
						echo form_open('manage_reh_yt_entry/'.$this->uri->segment(2),$form_attrib);?>
					<div class="row flex-element center-both">
						<div class="well well-sm main-entry-box">
						  <div class="col-md-6">
							<select class="form-control" id="partner_name11" name="partner_name" onchange="partner_fill()">
							  <option value="">Select partner</option>
							  <?php if(!empty($partners)){ foreach($partners as $partner){?>
							  <option value="<?php echo $partner->ss_partners_id;?>"><?php echo $partner->ss_partners_name;?></option>
							  <?php } }?>
							</select>
						  </div>
						  <div class="col-md-4">
								<input type="text" name="month_from" id="month_from" class="form-control datepicker" placeholder="Select month" onchange="date_fill()">
								<p class="text-danger ng-binding ng-hide" aria-hidden="true" id="error_month"><?php echo form_error('month_data');?></p>
						  </div>
						  <div class="col-md-2">
							<button class="btn btn-sm btn-primary btn-block" onclick="submit_form('manage_reh_yt_entry');" type="button">
								Get Data
							</button>
						  </div>
						</div>
					</div>
					<?php echo form_close();?>
					<?php } ?>
                    <div class="row" ng-show="hasData" aria-hidden="false" style="">
                        <div class="col-md-12">
                            <div class="tabs-container">
                                <div class="tabs-left ng-isolate-scope">
                                    <ul class="nav nav-tabs">
                                        <?php
                                        if (!empty($sections)) {
                                            foreach ($sections as $key_section => $section) {
                                                ?>
                                                <li class="uib-tab nav-item ng-scope ng-isolate-scope<?php if ($key_section == 0) { ?> active <?php } ?>">
                                                    <a href="#tab<?php echo $section->ss_section_layout_ss_section_layout_id; ?>" data-toggle="tab" class="nav-link ng-binding"><?php echo ucwords($section->ss_section_layout_section_name); ?></a>
                                                </li>
                                            <?php
                                            }
                                        }
                                        ?>
                                    </ul>
									<?php
									$form_attrib = array('id'=>'reh_yt_entry');
									echo form_open('create_reh_yt_entry/'.$this->uri->segment(2),$form_attrib);?>
                                    <div class="tab-content">
                                        <?php $section_id = ""; $j=0; //$k=0;?>                                       
                                        <?php
                                        if (!empty($metrics_data)) {

                                            foreach ($metrics_data as $key_section_metric => $section_metric) {
												
                                                if ($section_id != $section_metric->ss_section_layout_ss_section_layout_id) {
                                                    if ($j != 0) {
                                                        ?>
                                                        </tbody>
                                                        </table>
                                                    </div>
													<input type="hidden" name="month_data"/>
													<input type="hidden" class="partner_name_h" name="partner_name_h"/>
                                                   
                                                </div>
                                            </div>
                                          <?php } ?>
										
                                        <div class="tab-pane ng-scope<?php if ($section_id == "") { ?> active<?php } ?>" id="tab<?php echo $section_metric->ss_section_layout_ss_section_layout_id; ?>">

                                            <div class="panel-body ng-scope">
                                                <div class="bg-heading p-xs b-r-sm ng-scope"><i class="fa fa-arrow-right"></i><?php echo ucwords($section_metric->ss_section_layout_section_name); ?> </div>                                                       
                                                <h4 class="ng-scope">
            <?php echo $section_metric->ss_section_layout_analysis_description; ?>
                                                </h4>                                               
                                                <div class="table-responsive ng-scope">
                                                    <table class="table table-bordered">
                                                        <thead>

                                                            <tr>
                                                                <?php if ($section_metric->ss_section_layout_section_col_count == 1 && $section_metric->ss_section_layout_analysis_description == "") { ?>
                                                                    <td>Metric</td>       
                                                                    <td>Value</td>
            <?php }
            elseif ($section_metric->ss_section_layout_section_col_count == 4 && $section_metric->ss_section_layout_analysis_description == "") { ?>
                                                                    <td>Metric</td>       
                                                                    <td>Men</td>
                                                                    <td>Women</td>
																	<td>Transgender</td>
                                                                    <td>Boys</td>
                                                                    <td>Girls</td>
            <?php }
            elseif ($section_metric->ss_section_layout_analysis_description != "") { ?>
                                                                    <td>Question</td>       
                                                                    <td>Response</td>
            <?php } ?>
                                                            </tr>

                                                        </thead>
                                                        <tbody>

                                                            <tr>
            <?php if ($section_metric->ss_section_layout_section_col_count == 1 && $section_metric->ss_section_layout_analysis_description == "") { ?>
                                                                    <td><?php echo $section_metric->ss_metric_master_description; ?>
																	<input type="hidden" name="matric_id_one_column[]" value="<?php echo $section_metric->ss_metric_master_id;?>"/>																	
                                                                    </td>
                                                                    <td><input type="text" name="one_column_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
            <?php }
            elseif ($section_metric->ss_section_layout_section_col_count == 4 && $section_metric->ss_section_layout_analysis_description == "") { ?>
                                                                    <td><?php echo $section_metric->ss_metric_master_description; ?>
																	<input type="hidden" name="matric_id_four_column[]" value="<?php echo $section_metric->ss_metric_master_id;?>"/>																	
                                                                    </td>

                                                                    <td><input type="text" name="four_column_men_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
                                                                    <td><input type="text" name="four_column_women_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
																	<td><input type="text" name="four_column_trans_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
                                                                    <td><input type="text" name="four_column_boy_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
                                                                    <td><input type="text" name="four_column_girl_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
                                                            <?php }
                                                            elseif ($section_metric->ss_section_layout_analysis_description != "") { ?>
                                                                    <td class="col-md-3"><?php echo $section_metric->ss_metric_master_description; ?>
																	<input type="hidden" name="matric_id_analysis[]" value="<?php echo $section_metric->ss_metric_master_id;?>"/>																	
                                                                    </td>
                                                                    <td class="col-md-9"><textarea class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false" name="analysis_value[]"></textarea></td>                
                                                                <?php } ?>
                                                            </tr>
																
                                                   
            <?php $section_id = $section_metric->ss_section_layout_ss_section_layout_id;
			$j++;
        }
        else {
            ?>
			</tbody>
                                                      
                                                            <tr>
            <?php if ($section_metric->ss_section_layout_section_col_count == 1 && $section_metric->ss_section_layout_analysis_description == "") { ?>
                                                                    <td><?php echo $section_metric->ss_metric_master_description; ?>
																	<input type="hidden" name="matric_id_one_column[]" value="<?php echo $section_metric->ss_metric_master_id;?>"/>																	
                                                                    </td>
                                                                    <td><input type="text" name="one_column_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>

                                                                <?php }
                                                                elseif ($section_metric->ss_section_layout_section_col_count == 4 && $section_metric->ss_section_layout_analysis_description == "") { ?>
                                                                    <td><?php echo $section_metric->ss_metric_master_description; ?>
																	<input type="hidden" name="matric_id_four_column[]" value="<?php echo $section_metric->ss_metric_master_id;?>"/>																	
                                                                    </td>

                                                                    <td><input type="text" name="four_column_men_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
                                                                    <td><input type="text" name="four_column_women_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
																	<td><input type="text" name="four_column_trans_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
                                                                    <td><input type="text" name="four_column_boy_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
                                                                    <td><input type="text" name="four_column_girl_val[]" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
            <?php }
            elseif ($section_metric->ss_section_layout_analysis_description != "") { ?>
                                                                    <td class="col-md-3"><?php echo $section_metric->ss_metric_master_description; ?>
																	<input type="hidden" name="matric_id_analysis[]" value="<?php echo $section_metric->ss_metric_master_id;?>"/>																	
                                                                    </td>
                                                                    <td class="col-md-9"><textarea class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="analysis_value[]" aria-invalid="false"></textarea>
																	</td>
                                                               <?php } ?>
                                                            </tr>
                                                         <?php } }
                                                     ?> 
                                                </tbody>
                                            </table>
                                                    
                                        </div>
										<input type="hidden" name="month_data"/>
                                       <input type="hidden" class="partner_name_h" name="partner_name_h"/>										
                                        										
                                    </div>
                                </div>
<?php } ?>
<?php
if($this->session->userdata('userinfo')['default_role'] == 4 && !empty($metrics_data)) //if partner
{?>
<div class="panel-body ng-scope">
	<div class="row">
		<div class="col-lg-2 col-lg-offset-10 col-md-2 col-md-offset-10 col-sm-4 col-sm-offset-4 text-right">
			<button class="btn btn-block btn-primary" onclick="submit_form('reh_yt_entry');" type="button">
				Save
			</button>
		</div>
	</div>
</div>
<?php }?>                     
<?php echo form_close();?>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</div>
</div>
</div>
<script>
function submit_form(form_name)
{
	if($("#month_from").val() == "")
	{
		$("#error_month").html("Please select month");
		$("#month_from").focus();
	}else{		
		if(form_name == "reh_yt_entry")
		{
		$("#reh_yt_entry").submit();
		}
		else if(form_name == "manage_reh_yt_entry")
		{
			$("#manage_reh_yt_entry").submit();
		}
	}
}


</script>