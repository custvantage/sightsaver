<div class="row wrapper border-bottom white-bg page-heading ng-scope">
  <div class="col-lg-10">
    <h2>Rural Eye Health Yearly Targets</h2>
  </div>
</div>

<div class="wrapper wrapper-content animated fadeIn ng-scope" ng-init="init()">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox">
        <div class="ibox-title">
          <h5>Vhsnc Entry</h5>
        </div>
        <div class="ibox-content">
          <div class="row flex-element center-both">
            <div class="well well-sm main-entry-box">
              <div class="col-md-6">
                <select class="form-control" name="">
                  <option>Select or searh for partener</option>
                </select>
              </div>
              <div class="col-md-4">
               <input type="month" class="form-control">
              </div>
              <div class="col-md-2">
                <button class="btn btn-sm btn-primary btn-block" type="submit" ng-click="getData();">
                Get Data
              </button>
              </div>
            </div>
          </div>
          

          <div class="row" ng-show="hasData" aria-hidden="false" style="">
            <div class="col-md-12">
              <div class="tabs-container">
                <div class="tabs-left ng-isolate-scope">
  <ul class="nav nav-tabs">

                  <li  class="uib-tab nav-item ng-scope ng-isolate-scope active">
                    <a href="#tab1" data-toggle="tab" class="nav-link ng-binding">Vision Centers/Camps</a>
                  </li>
                  <li class="uib-tab nav-item ng-scope ng-isolate-scope">
                    <a href="#tab2" data-toggle="tab" class="nav-link ng-binding">OPD / Screening</a>
                  </li>
                  <li  class="uib-tab nav-item ng-scope ng-isolate-scope">
                    <a href="#tab3" data-toggle="tab" class="nav-link ng-binding">Referral</a>
                  </li>
                  <li class="uib-tab nav-item ng-scope ng-isolate-scope">
                    <a href="#tab4" data-toggle="tab" class="nav-link ng-binding">Refraction</a>
                  </li>
                  <li class="uib-tab nav-item ng-scope ng-isolate-scope">
                    <a href="#tab5" data-toggle="tab" class="nav-link ng-binding">Spectacles</a>
                  </li>
                  <li class="uib-tab nav-item ng-scope ng-isolate-scope">
                    <a href="#tab6" data-toggle="tab" class="nav-link ng-binding">Cataract</a>
                  </li>
                  <li class="uib-tab nav-item ng-scope ng-isolate-scope">
                    <a href="#tab7" data-toggle="tab" class="nav-link ng-binding" >Glaucoma</a>
                  </li>
                  <li class="uib-tab nav-item ng-scope ng-isolate-scope">
                    <a href="#tab8" data-toggle="tab" class="nav-link ng-binding">Diabetic Retinopathy</a>
                  </li>
                  <li class="uib-tab nav-item ng-scope ng-isolate-scope">
                    <a href="#tab9" data-toggle="tab" class="nav-link ng-binding">Low Vision / Irreversible blindness</a>
                  </li>
                  <li class="uib-tab nav-item ng-scope ng-isolate-scope">
                    <a href="#tab10" data-toggle="tab" class="nav-link ng-binding">Other Treatments</a>
                  </li>
                  <li class="uib-tab nav-item ng-scope ng-isolate-scope">
                    <a href="#tab12" data-toggle="tab" class="nav-link ng-binding">System Strengthening</a>
                  </li>
                  <li class="uib-tab nav-item ng-scope ng-isolate-scope">
                    <a href="#tab13" data-toggle="tab" class="nav-link ng-binding" >Human Resource</a>
                  </li>
                  <li class="uib-tab nav-item ng-scope ng-isolate-scope">
                    <a href="#tab15" data-toggle="tab" class="nav-link ng-binding">Advocacy / Networking / Liasioning</a>
                  </li>
                  <li  class="uib-tab nav-item ng-scope ng-isolate-scope">
                    <a href="#tab17" data-toggle="tab" class="nav-link ng-binding" >IEC / BCC Activity</a>
                  </li>
                </ul>
  <div class="tab-content">
    <!-- ngRepeat: tab in tabset.tabs -->

    <div class="tab-pane ng-scope active" id="tab1">

                    <div class="panel-body ng-scope">
                      <!-- ngInclude: --><ng-include src="'app/reh/partials/main_entry/camps.html'" class="ng-scope"><div class="bg-heading p-xs b-r-sm ng-scope"><i class="fa fa-arrow-right"></i> Vision Centres /Camps</div>
<div class="table-responsive ng-scope">
<table class="table table-bordered">
  <thead>
  <tr>
    <td>Metric</td>
    <td>Value</td>
    <td>Metric</td>
    <td>Value</td>
  </tr>
  </thead>
  <tbody>
  <tr>
    <td>Number of Vision Centres (VC) operational in the district Partner supported by Sightsavers</td>
    <td><input type="number" ng-model="form.camps.vc_sightsavers" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td>Number of Mobile Vision Centres operational in the district supported by Sightsavers</td>
    <td><input type="number" ng-model="form.camps.vc_mobile_sightsavers" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of Vision Centres (VC) operational in the district setup by Government</td>
    <td><input type="number" ng-model="form.camps.vc_govt" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td>Number of Mobile Vision Centres operational in the district setup by Government</td>
    <td><input type="number" ng-model="form.camps.vc_mobile_govt" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of Camps conducted during the month</td>
    <td><input type="number" ng-model="form.camps.conducted" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td></td>
    <td></td>
  </tr>
  </tbody>
</table>
</div>
</ng-include>
                    </div>
                  </div><!-- end ngRepeat: tab in tabset.tabs -->
                  
                  <div class="tab-pane ng-scope" id="tab2">

                    <div class="panel-body ng-scope">
                      <!-- ngInclude: --><ng-include src="'app/reh/partials/main_entry/screening.html'" class="ng-scope"><div class="bg-heading p-xs b-r-sm ng-scope"><i class="fa fa-arrow-right"></i> OPD / Screening</div>
<div class="table-responsive ng-scope">
<table class="table table-bordered">
  <thead>
  <tr>
    <td class="col-md-6">Metric</td>
    <td>Men</td>
    <td>Women</td>
    <td>Boys</td>
    <td>GIrls</td>
  </tr>
  </thead>
  <tbody>
  <tr>
    <td>Total OPD for the month (this should be the total OPD figures for the entire partner hospital across all
      districts irrespective whether it is supported by Sightsavers or not)
    </td>
    <td><input type="number" ng-model="form.screening.total.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.screening.total.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.screening.total.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.screening.total.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of Screening at Vision Centre for the programme</td>
    <td><input type="number" ng-model="form.screening.vc.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.screening.vc.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.screening.vc.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.screening.vc.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of Screening at Camps for the programme</td>
    <td><input type="number" ng-model="form.screening.camps.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.screening.camps.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.screening.camps.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.screening.camps.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of OPD at Secondary / tertiary Eye Care Centre for the Sightsavers supported programme district</td>
    <td><input type="number" ng-model="form.screening.secondary.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.screening.secondary.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.screening.secondary.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.screening.secondary.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  </tbody>
</table>
</div>
</ng-include>
                    </div>
                  </div><!-- end ngRepeat: tab in tabset.tabs -->
                  
                  <div class="tab-pane ng-scope" id="tab3">

                    <div class="panel-body ng-scope">
                      <!-- ngInclude: --><ng-include src="'app/reh/partials/main_entry/referral.html'" class="ng-scope"><div class="bg-heading p-xs b-r-sm ng-scope"><i class="fa fa-arrow-right"></i> Referral</div>
<div class="table-responsive ng-scope">
<table class="table table-bordered">
  <thead>
  <tr>
    <td class="col-md-6">Metric</td>
    <td>Men</td>
    <td>Women</td>
    <td>Boys</td>
    <td>GIrls</td>
  </tr>
  </thead>
  <tbody>
  <tr>
    <td>Number of persons referred from Vision Centre to base hospital</td>
    <td><input type="number" ng-model="form.referral.vc_to_base.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.referral.vc_to_base.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.referral.vc_to_base.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.referral.vc_to_base.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of persons referred from camps to base hospital</td>
    <td><input type="number" ng-model="form.referral.camp_to_base.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.referral.camp_to_base.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.referral.camp_to_base.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.referral.camp_to_base.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of persons referred to Govt. hospital/ other hospitals</td>
    <td><input type="number" ng-model="form.referral.other_hospital.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.referral.other_hospital.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.referral.other_hospital.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.referral.other_hospital.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of persons referred to Vision Centres by ASHA / ANM</td>
    <td><input type="number" ng-model="form.referral.asha_vc.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.referral.asha_vc.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.referral.asha_vc.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.referral.asha_vc.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of persons referred to camps by by ASHA / ANM</td>
    <td><input type="number" ng-model="form.referral.asha_camp.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.referral.asha_camp.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.referral.asha_camp.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.referral.asha_camp.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of persons referred directly to base hospital by ASHA/ANM</td>
    <td><input type="number" ng-model="form.referral.asha_base.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.referral.asha_base.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.referral.asha_base.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.referral.asha_base.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of persons referred directly to Govt. Hospital by ASHA/ANM</td>
    <td><input type="number" ng-model="form.referral.asha_govt.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.referral.asha_govt.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.referral.asha_govt.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.referral.asha_govt.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of persons reported at base hospital from Vision Centre</td>
    <td><input type="number" ng-model="form.referral.base_from_vc.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.referral.base_from_vc.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.referral.base_from_vc.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.referral.base_from_vc.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of persons reported at base hospital from camps</td>
    <td><input type="number" ng-model="form.referral.base_from_camp.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.referral.base_from_camp.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.referral.base_from_camp.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.referral.base_from_camp.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of persons reported at Govt. hospital/ other hospitals</td>
    <td><input type="number" ng-model="form.referral.other_hospital_reported.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.referral.other_hospital_reported.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.referral.other_hospital_reported.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.referral.other_hospital_reported.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>

  </tbody>
</table>
</div>
</ng-include>
                    </div>
                  </div>
                  
                  <div class="tab-pane ng-scope" id="tab4">

                    <div class="panel-body ng-scope">
                      <!-- ngInclude: --><ng-include src="'app/reh/partials/main_entry/refraction.html'" class="ng-scope"><div class="bg-heading p-xs b-r-sm ng-scope"><i class="fa fa-arrow-right"></i> Refraction</div>
<div class="table-responsive ng-scope">
<table class="table table-bordered">
  <thead>
  <tr>
    <td class="col-md-6">Metric</td>
    <td>Men</td>
    <td>Women</td>
    <td>Boys</td>
    <td>GIrls</td>
  </tr>
  </thead>
  <tbody>
  <tr>
    <td>Number of persons refracted at the Vision Centre</td>
    <td><input type="number" ng-model="form.refraction.vc.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.refraction.vc.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.refraction.vc.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.refraction.vc.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of persons refracted at the camps</td>
    <td><input type="number" ng-model="form.refraction.camp.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.refraction.camp.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.refraction.camp.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.refraction.camp.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of persons refracted at the base hospitals</td>
    <td><input type="number" ng-model="form.refraction.base.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.refraction.base.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.refraction.base.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.refraction.base.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of persons identified with Refractive Error (RE) at the Vision Centre</td>
    <td><input type="number" ng-model="form.refraction.vc_re.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.refraction.vc_re.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.refraction.vc_re.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.refraction.vc_re.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of persons identified with Refractive Error (RE) at the camps</td>
    <td><input type="number" ng-model="form.refraction.camp_re.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.refraction.camp_re.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.refraction.camp_re.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.refraction.camp_re.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of persons identified with RE at base hospitals</td>
    <td><input type="number" ng-model="form.refraction.base_re.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.refraction.base_re.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.refraction.base_re.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.refraction.base_re.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  </tbody>
</table>
</div>
</ng-include>
                    </div>
                  </div><!-- end ngRepeat: tab in tabset.tabs -->
                  
                  <div class="tab-pane ng-scope" id="tab5">

                    <div class="panel-body ng-scope">
                      <!-- ngInclude: --><ng-include src="'app/reh/partials/main_entry/spectacles.html'" class="ng-scope"><div class="bg-heading p-xs b-r-sm ng-scope"><i class="fa fa-arrow-right"></i> Spectacles</div>
<div class="table-responsive ng-scope">
<table class="table table-bordered">
  <thead>
  <tr>
    <td class="col-md-6">Metric</td>
    <td>Men</td>
    <td>Women</td>
    <td>Boys</td>
    <td>GIrls</td>
  </tr>
  </thead>
  <tbody>
  <tr>
    <td>Number of persons with RE, prescribed spectacles at the Vision Centre</td>
    <td><input type="number" ng-model="form.spectacles.vc_re.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.spectacles.vc_re.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.spectacles.vc_re.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.spectacles.vc_re.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of persons with RE, prescribed spectacles at the camps</td>
    <td><input type="number" ng-model="form.spectacles.camp_re.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.spectacles.camp_re.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.spectacles.camp_re.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.spectacles.camp_re.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of persons with RE, prescribed spectacles at base hospital</td>
    <td><input type="number" ng-model="form.spectacles.base_re.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.spectacles.base_re.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.spectacles.base_re.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.spectacles.base_re.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of persons dispensed spectacles free at Vision Centre</td>
    <td><input type="number" ng-model="form.spectacles.vc_free.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.spectacles.vc_free.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.spectacles.vc_free.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.spectacles.vc_free.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of persons dispensed spectacles free at camp</td>
    <td><input type="number" ng-model="form.spectacles.camp_free.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.spectacles.camp_free.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.spectacles.camp_free.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.spectacles.camp_free.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of persons dispensed spectacles free at base hospital</td>
    <td><input type="number" ng-model="form.spectacles.base_free.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.spectacles.base_free.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.spectacles.base_free.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.spectacles.base_free.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of persons prescribed spectacles who purchased spectacles at Vision Centre (subsidised rate)</td>
    <td><input type="number" ng-model="form.spectacles.vc_subsidised.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.spectacles.vc_subsidised.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.spectacles.vc_subsidised.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.spectacles.vc_subsidised.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of persons prescribed spectacles who purchased spectacles at camp (subsidised rate)</td>
    <td><input type="number" ng-model="form.spectacles.camp_subsidised.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.spectacles.camp_subsidised.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.spectacles.camp_subsidised.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.spectacles.camp_subsidised.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of persons prescribed spectacles who purchased spectacles at base hospital (subsidised rate)</td>
    <td><input type="number" ng-model="form.spectacles.base_subsidised.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.spectacles.base_subsidised.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.spectacles.base_subsidised.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.spectacles.base_subsidised.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of persons prescribed spectacles who purchased spectacles at Vision Centre (full rate)</td>
    <td><input type="number" ng-model="form.spectacles.vc_full_rate.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.spectacles.vc_full_rate.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.spectacles.vc_full_rate.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.spectacles.vc_full_rate.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of persons prescribed spectacles who purchased spectacles at camp (full rate)</td>
    <td><input type="number" ng-model="form.spectacles.camp_full_rate.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.spectacles.camp_full_rate.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.spectacles.camp_full_rate.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.spectacles.camp_full_rate.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of persons prescribed spectacles who purchased spectacles at base hospital (full rate)</td>
    <td><input type="number" ng-model="form.spectacles.base_full_rate.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.spectacles.base_full_rate.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.spectacles.base_full_rate.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.spectacles.base_full_rate.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  </tbody>
</table>
</div>
</ng-include>
                    </div>
                  </div><!-- end ngRepeat: tab in tabset.tabs -->
                  <div class="tab-pane ng-scope" id="tab6">

                    <div class="panel-body ng-scope">
                      <!-- ngInclude: --><ng-include src="'app/reh/partials/main_entry/cataract.html'" class="ng-scope"><div class="bg-heading p-xs b-r-sm ng-scope"><i class="fa fa-arrow-right"></i> Cataract</div>
<div class="table-responsive ng-scope">
<table class="table table-bordered">
  <thead>
  <tr>
    <td class="col-md-6">Metric</td>
    <td>Men</td>
    <td>Women</td>
    <td>Boys</td>
    <td>GIrls</td>
  </tr>
  </thead>
  <tbody>
  <tr>
    <td>Number of persons referred to base hospital by ASHA / ANM for cataract</td>
    <td><input type="number" ng-model="form.cataract.asha_to_base.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.asha_to_base.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.asha_to_base.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.asha_to_base.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of persons referred to VCs by ASHA / ANM for cataract</td>
    <td><input type="number" ng-model="form.cataract.asha_to_vc.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.asha_to_vc.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.asha_to_vc.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.asha_to_vc.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of persons referred to camp by ASHA / ANM for cataract</td>
    <td><input type="number" ng-model="form.cataract.asha_to_camp.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.asha_to_camp.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.asha_to_camp.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.asha_to_camp.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of persons identified with Cataract and referred to base hospital from VCs</td>
    <td><input type="number" ng-model="form.cataract.vc_to_base.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.vc_to_base.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.vc_to_base.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.vc_to_base.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of persons identified with Cataract and referred to base hospital from Camp</td>
    <td><input type="number" ng-model="form.cataract.camp_to_base.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.camp_to_base.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.camp_to_base.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.camp_to_base.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of persons identified with Cataract and referred by ASHA / ANM who reported to base hospital</td>
    <td><input type="number" ng-model="form.cataract.asha_to_base_reported.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.asha_to_base_reported.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.asha_to_base_reported.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.asha_to_base_reported.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of persons identified with cataract and referred from Vision Centre and camps who reported to base hospital</td>
    <td><input type="number" ng-model="form.cataract.vc_to_base_reported.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.vc_to_base_reported.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.vc_to_base_reported.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.vc_to_base_reported.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of Cataract surgeries undertaken at Sightsavers intervention District (by partner or other hospitals)</td>
    <td><input type="number" ng-model="form.cataract.num_surgeries_all.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.num_surgeries_all.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.num_surgeries_all.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.num_surgeries_all.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of Cataract surgeries undertaken by the partner</td>
    <td><input type="number" ng-model="form.cataract.num_surgeries_partner.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.num_surgeries_partner.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.num_surgeries_partner.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.num_surgeries_partner.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of Cataract surgeries undertaken by partner calculated after Attribution</td>
    <td><input type="number" ng-model="form.cataract.num_surgeries_partner_after_attribution.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.num_surgeries_partner_after_attribution.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.num_surgeries_partner_after_attribution.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.num_surgeries_partner_after_attribution.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of Cataract operations performed free of cost</td>
    <td><input type="number" ng-model="form.cataract.num_surgeries_free.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.num_surgeries_free.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.num_surgeries_free.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.num_surgeries_free.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of Cataract operations performed subsidised</td>
    <td><input type="number" ng-model="form.cataract.num_surgeries_subsidised.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.num_surgeries_subsidised.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.num_surgeries_subsidised.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.num_surgeries_subsidised.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of Cataract operations performed paid</td>
    <td><input type="number" ng-model="form.cataract.num_surgeries_paid.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.num_surgeries_paid.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.num_surgeries_paid.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.num_surgeries_paid.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of Cataract surgeries supported by Sightsavers</td>
    <td><input type="number" ng-model="form.cataract.num_surgeries_sightsavers.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.num_surgeries_sightsavers.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.num_surgeries_sightsavers.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.num_surgeries_sightsavers.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of Cataract surgeries supported by DBCS</td>
    <td><input type="number" ng-model="form.cataract.num_surgeries_dbcs.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.num_surgeries_dbcs.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.num_surgeries_dbcs.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.num_surgeries_dbcs.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of Cataract surgeries supported by other sources</td>
    <td><input type="number" ng-model="form.cataract.num_surgeries_other.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.num_surgeries_other.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.num_surgeries_other.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.num_surgeries_other.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of persons who came for the first follow-up after Cataract Operations to base hospital</td>
    <td><input type="number" ng-model="form.cataract.followups_at_base.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.followups_at_base.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.followups_at_base.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.cataract.followups_at_base.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  </tbody>
</table>
</div>
</ng-include>
                    </div>
                  </div><!-- end ngRepeat: tab in tabset.tabs -->
                  <div class="tab-pane ng-scope" id="tab7">

                    <div class="panel-body ng-scope">
                      <!-- ngInclude: --><ng-include src="'app/reh/partials/main_entry/glaucoma.html'" class="ng-scope"><div class="bg-heading p-xs b-r-sm ng-scope"><i class="fa fa-arrow-right"></i> Glaucoma</div>
<div class="table-responsive ng-scope">
<table class="table table-bordered">
  <thead>
  <tr>
    <td class="col-md-6">Metric</td>
    <td>Men</td>
    <td>Women</td>
    <td>Boys</td>
    <td>GIrls</td>
  </tr>
  </thead>
  <tbody>
  <tr>
    <td>Number of persons examined for Glaucoma</td>
    <td><input type="number" ng-model="form.glaucoma.exam.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.glaucoma.exam.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.glaucoma.exam.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.glaucoma.exam.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of persons identified with Glaucoma</td>
    <td><input type="number" ng-model="form.glaucoma.confirmed.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.glaucoma.confirmed.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.glaucoma.confirmed.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.glaucoma.confirmed.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of cases of Glaucoma treated with medication</td>
    <td><input type="number" ng-model="form.glaucoma.treated.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.glaucoma.treated.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.glaucoma.treated.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.glaucoma.treated.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of Glaucoma surgeries / procedure performed at Sightsavers intervention District</td>
    <td><input type="number" ng-model="form.glaucoma.surgery.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.glaucoma.surgery.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.glaucoma.surgery.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.glaucoma.surgery.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  </tbody>
</table>
</div>
</ng-include>
                    </div>
                  </div><!-- end ngRepeat: tab in tabset.tabs -->
                  <div class="tab-pane ng-scope" id="tab8">

                    <div class="panel-body ng-scope">
                      <!-- ngInclude: --><ng-include src="'app/reh/partials/main_entry/diabetic_retinopathy.html'" class="ng-scope"><div class="bg-heading p-xs b-r-sm ng-scope"><i class="fa fa-arrow-right"></i> Diabetic Retinopathy</div>
<div class="table-responsive ng-scope">
<table class="table table-bordered">
  <thead>
    <tr>
      <td class="col-md-6">Metric</td>
      <td>Men</td>
      <td>Women</td>
      <td>Boys</td>
      <td>GIrls</td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Number of persons examined for DR</td>
      <td><input type="number" ng-model="form.diabetic_retinopathy.exam.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
      <td><input type="number" ng-model="form.diabetic_retinopathy.exam.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
      <td><input type="number" ng-model="form.diabetic_retinopathy.exam.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
      <td><input type="number" ng-model="form.diabetic_retinopathy.exam.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    </tr>
    <tr>
      <td>Number of persons identified with DR</td>
      <td><input type="number" ng-model="form.diabetic_retinopathy.confirmed.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
      <td><input type="number" ng-model="form.diabetic_retinopathy.confirmed.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
      <td><input type="number" ng-model="form.diabetic_retinopathy.confirmed.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
      <td><input type="number" ng-model="form.diabetic_retinopathy.confirmed.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    </tr>
    <tr>
      <td>Number of laser procedure performed</td>
      <td><input type="number" ng-model="form.diabetic_retinopathy.treated.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
      <td><input type="number" ng-model="form.diabetic_retinopathy.treated.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
      <td><input type="number" ng-model="form.diabetic_retinopathy.treated.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
      <td><input type="number" ng-model="form.diabetic_retinopathy.treated.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    </tr>
    <tr>
      <td>Number of Vitrectomy surgery performed at Sightsavers intervention District</td>
      <td><input type="number" ng-model="form.diabetic_retinopathy.surgery.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
      <td><input type="number" ng-model="form.diabetic_retinopathy.surgery.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
      <td><input type="number" ng-model="form.diabetic_retinopathy.surgery.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
      <td><input type="number" ng-model="form.diabetic_retinopathy.surgery.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    </tr>
  </tbody>
</table>
</div>
</ng-include>
                    </div>
                  </div><!-- end ngRepeat: tab in tabset.tabs -->
                  <div class="tab-pane ng-scope" id="tab9">

                    <div class="panel-body ng-scope">
                      <!-- ngInclude: --><ng-include src="'app/reh/partials/main_entry/low_vision.html'" class="ng-scope"><div class="bg-heading p-xs b-r-sm ng-scope"><i class="fa fa-arrow-right"></i> Low Vision / Irreversible blindness</div>
<div class="table-responsive ng-scope">
<table class="table table-bordered">
  <thead>
    <tr>
      <td class="col-md-6">Metric</td>
      <td>Men</td>
      <td>Women</td>
      <td>Boys</td>
      <td>GIrls</td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Number of people identified with low vision ( visual acuity less than 6/18 and equal to or better than 3/60 in the
        better eye with the best correction)</td>
      <td><input type="number" ng-model="form.low_vision.total.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
      <td><input type="number" ng-model="form.low_vision.total.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
      <td><input type="number" ng-model="form.low_vision.total.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
      <td><input type="number" ng-model="form.low_vision.total.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    </tr>
    <tr>
      <td>Number of people received clinical low vision assessment</td>
      <td><input type="number" ng-model="form.low_vision.clinical.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
      <td><input type="number" ng-model="form.low_vision.clinical.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
      <td><input type="number" ng-model="form.low_vision.clinical.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
      <td><input type="number" ng-model="form.low_vision.clinical.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    </tr>
    <tr>
      <td>Number of people dispensed low vision devices</td>
      <td><input type="number" ng-model="form.low_vision.devices.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
      <td><input type="number" ng-model="form.low_vision.devices.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
      <td><input type="number" ng-model="form.low_vision.devices.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
      <td><input type="number" ng-model="form.low_vision.devices.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    </tr>
    <tr>
      <td>Number of people clinically diagnosed irreversible blind (VA
        &lt;3/60 in the better eye with the best possible correction)</td>
          <td><input type="number" ng-model="form.low_vision.ir_diagnosed.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
          <td><input type="number" ng-model="form.low_vision.ir_diagnosed.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
          <td><input type="number" ng-model="form.low_vision.ir_diagnosed.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
          <td><input type="number" ng-model="form.low_vision.ir_diagnosed.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    </tr>
  </tbody>
</table>
</div>
</ng-include>
                    </div>
                  </div><!-- end ngRepeat: tab in tabset.tabs -->
                  <div class="tab-pane ng-scope" id="tab10">

                    <div class="panel-body ng-scope">
                      <!-- ngInclude: --><ng-include src="'app/reh/partials/main_entry/other_treatment.html'" class="ng-scope"><div class="bg-heading p-xs b-r-sm ng-scope"><i class="fa fa-arrow-right"></i> Other treatments</div>
<div class="table-responsive ng-scope">
<table class="table table-bordered">
  <thead>
  <tr>
    <td class="col-md-6">Metric</td>
    <td>Men</td>
    <td>Women</td>
    <td>Boys</td>
    <td>GIrls</td>
  </tr>
  </thead>
  <tbody>
  <tr>
    <td>Number of any other treatment not included above (if any)</td>
    <td><input type="number" ng-model="form.other_treatment.treatment.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.other_treatment.treatment.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.other_treatment.treatment.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.other_treatment.treatment.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of any other surgery not included above (if any)</td>
    <td><input type="number" ng-model="form.other_treatment.surgery.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.other_treatment.surgery.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.other_treatment.surgery.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.other_treatment.surgery.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  </tbody>
</table>
</div>
</ng-include>
                    </div>
                  </div><!-- end ngRepeat: tab in tabset.tabs -->
                  
                  <!-- end ngRepeat: tab in tabset.tabs -->
                  
                  <div class="tab-pane ng-scope" id="tab12">

                    <div class="panel-body ng-scope">
                      <!-- ngInclude: --><ng-include src="'app/reh/partials/main_entry/system_strengthning.html'" class="ng-scope"><div class="bg-heading p-xs b-r-sm ng-scope"><i class="fa fa-arrow-right"></i> System strengthening with Government</div>
<div class="table-responsive ng-scope">
<table class="table table-bordered">
  <thead>
  <tr>
    <td class="col-md-6">Metric</td>
    <td>Men</td>
    <td>Women</td>
    <td>Boys</td>
    <td>GIrls</td>
  </tr>
  </thead>
  <tbody>
  <tr>
    <td>Total eye OPD in Government hospitals during the month (inclusive of District hospital)</td>
    <td><input type="number" ng-model="form.system_strengthening.hospital.opd.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.hospital.opd.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.hospital.opd.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.hospital.opd.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Total eye OPD in Government CHCs, PHCs, Camps and other facilities</td>
    <td><input type="number" ng-model="form.system_strengthening.camps.opd.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.camps.opd.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.camps.opd.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.camps.opd.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Total number of persons refracted in Government facilities during the month (District hospital)</td>
    <td><input type="number" ng-model="form.system_strengthening.hospital.refracted.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.hospital.refracted.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.hospital.refracted.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.hospital.refracted.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Total number of persons refracted in Government facilities during the month (CHCs, PHCs, Camps and other facilities)</td>
    <td><input type="number" ng-model="form.system_strengthening.camps.refracted.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.camps.refracted.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.camps.refracted.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.camps.refracted.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Total number of persons identified with Refractive Error (RE) in Government facilities during the month (District hospital)</td>
    <td><input type="number" ng-model="form.system_strengthening.hospital.re.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.hospital.re.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.hospital.re.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.hospital.re.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Total number of persons identified with Refractive Error (RE) in Government facilities during the month (CHCs, PHCs, Camps and other facilities)</td>
    <td><input type="number" ng-model="form.system_strengthening.camps.re.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.camps.re.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.camps.re.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.camps.re.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Total number of persons prescribed spectacles in Government facilities during the month (District hospital)</td>
    <td><input type="number" ng-model="form.system_strengthening.hospital.specs_prescribed.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.hospital.specs_prescribed.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.hospital.specs_prescribed.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.hospital.specs_prescribed.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Total number of persons prescribed spectacles in Government facilities during the month (CHCs, PHCs, Camps and other facilities)</td>
    <td><input type="number" ng-model="form.system_strengthening.camps.specs_prescribed.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.camps.specs_prescribed.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.camps.specs_prescribed.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.camps.specs_prescribed.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Total number of persons dispensed spectacles in Government facilities during the month (District hospital)</td>
    <td><input type="number" ng-model="form.system_strengthening.hospital.specs_dispensed.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.hospital.specs_dispensed.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.hospital.specs_dispensed.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.hospital.specs_dispensed.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Total number of persons dispensed spectacles in Government facilities during the month (CHCs, PHCs, Camps and other facilities)</td>
    <td><input type="number" ng-model="form.system_strengthening.camps.specs_dispensed.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.camps.specs_dispensed.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.camps.specs_dispensed.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.camps.specs_dispensed.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Total number of persons operated for cataract in Government facilities during the month (District hospital)</td>
    <td><input type="number" ng-model="form.system_strengthening.hospital.operated_cataract.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.hospital.operated_cataract.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.hospital.operated_cataract.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.hospital.operated_cataract.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Total number of persons operated for cataract in Government facilities during the month (CHCs, PHCs, Camps and other facilities)</td>
    <td><input type="number" ng-model="form.system_strengthening.camps.operated_cataract.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.camps.operated_cataract.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.camps.operated_cataract.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.camps.operated_cataract.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Total number of persons operated for glaucoma in Government facilities during the month (District hospital)</td>
    <td><input type="number" ng-model="form.system_strengthening.hospital.operated_glaucoma.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.hospital.operated_glaucoma.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.hospital.operated_glaucoma.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.hospital.operated_glaucoma.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Total number of persons operated for glaucoma in Government facilities during the month (CHCs, PHCs, Camps and other facilities)</td>
    <td><input type="number" ng-model="form.system_strengthening.camps.operated_glaucoma.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.camps.operated_glaucoma.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.camps.operated_glaucoma.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.camps.operated_glaucoma.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Total number of persons operated for Diabetic Retinopathy in Government facilities during the month (District hospital)</td>
    <td><input type="number" ng-model="form.system_strengthening.hospital.operated_dr.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.hospital.operated_dr.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.hospital.operated_dr.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.hospital.operated_dr.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Total number of persons operated for Diabetic Retinopathy in Government facilities during the month (CHCs, PHCs, Camps and other facilities)</td>
    <td><input type="number" ng-model="form.system_strengthening.camps.operated_dr.men" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.camps.operated_dr.women" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.camps.operated_dr.boys" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
    <td><input type="number" ng-model="form.system_strengthening.camps.operated_dr.girls" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  </tbody>
</table>
</div>
</ng-include>
                    </div>
                  </div><!-- end ngRepeat: tab in tabset.tabs -->
                  
                  <div class="tab-pane ng-scope" id="tab13">

                    <div class="panel-body ng-scope">
                      <!-- ngInclude: --><ng-include src="'app/reh/partials/main_entry/human_resource.html'" class="ng-scope"><div class="bg-heading p-xs b-r-sm ng-scope"><i class="fa fa-arrow-right"></i> Vision Centres /Camps</div>
<div class="table-responsive ng-scope">
<table class="table table-bordered">
  <thead>
  <tr>
    <td>Metric</td>
    <td>Value</td>
  </tr>
  </thead>
  <tbody>
  <tr>
    <td>Number of Ophthalmic staff trained in partner / private / other Non Government Organisation (NGO) hospitals (Ophthalmic Assistants, Optometrists (including those who provide LV services) , OT assistants)</td>
    <td><input type="number" ng-model="form.human_resource.optho_staff_trained.ngo" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of Ophthalmic staff trained in Government hospitals (Ophthalmic Assistants, Optometrists (including those who provide LV services) , OT assistants)</td>
    <td><input type="number" ng-model="form.human_resource.optho_staff_trained.govt" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of Govt. Ophthalmologist trained</td>
    <td><input type="number" ng-model="form.human_resource.optho_trained.govt" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of Non Govt. Ophthalmologist (Partner/Pvt./other NGO hospital) trained</td>
    <td><input type="number" ng-model="form.human_resource.optho_trained.ngo" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of ASHA workers trained</td>
    <td><input type="number" ng-model="form.human_resource.training.asha" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of ANMs  trained</td>
    <td><input type="number" ng-model="form.human_resource.training.anm" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of Anganwadi workers under ICDS trained</td>
    <td><input type="number" ng-model="form.human_resource.training.angwadi" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of other specialist trained</td>
    <td><input type="number" ng-model="form.human_resource.training.other_specialist" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of District Programme Managers (DPM) under NPCB programme trained</td>
    <td><input type="number" ng-model="form.human_resource.training.dpm_npcb" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of state level officials under the blindness control programme trained</td>
    <td><input type="number" ng-model="form.human_resource.training.state_officials" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of community health volunteers trained</td>
    <td><input type="number" ng-model="form.human_resource.training.community_health_volunteer" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of CBO member trained</td>
    <td><input type="number" ng-model="form.human_resource.training.cbo" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of VHNC members trained</td>
    <td><input type="number" ng-model="form.human_resource.training.vhnc" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of Project Staff Trained or on exposure visit</td>
    <td><input type="number" ng-model="form.human_resource.training.project_staff" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Any other category trained</td>
    <td><input type="number" ng-model="form.human_resource.training.other_category" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of training held of DPMs under NRHM</td>
    <td><input type="number" ng-model="form.human_resource.training.dpms" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of CMEs organised during the month</td>
    <td><input type="number" ng-model="form.human_resource.training.cmes" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  </tbody>
</table>
</div>
</ng-include>
                    </div>
                  </div><!-- end ngRepeat: tab in tabset.tabs -->
                  
                  <div class="tab-pane ng-scope" id="tab14">

                    <div class="panel-body ng-scope">
                      <!-- ngInclude: --><ng-include src="'app/reh/partials/main_entry/analysis2.html'" class="ng-scope"><div class="bg-heading p-xs b-r-sm ng-scope"><i class="fa fa-arrow-right"></i> Analysis 2</div>
<h4 class="ng-scope">
  Please provide a brief analysis of the information for the following sections:
  <ul>
    <li>System strengthening with Government</li>
    <li>Human Resource (Training / capacity building)</li>
  </ul>
</h4>
<div class="table-responsive ng-scope">
<table class="table table-bordered">
  <thead>
  <tr>
    <td>Question</td>
    <td>Response</td>
  </tr>
  </thead>
  <tbody>
  <tr>
    <td class="col-md-3">What worked well?</td>
    <td class="col-md-9"><textarea ng-model="form.analysis2.worked_well" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></textarea></td>
  </tr>
  <tr>
    <td class="col-md-3">What according to you was not satisfactory and you will like to see improved (give reasons for
      Variation against target)
    </td>
    <td class="col-md-9"><textarea ng-model="form.analysis2.not_satisfactory" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></textarea></td>
  </tr>
  <tr>
    <td class="col-md-3">What challenges were faced and how they were mitigated?</td>
    <td class="col-md-9"><textarea ng-model="form.analysis2.challenges" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></textarea></td>
  </tr>
  <tr>
    <td class="col-md-3">What support would you require in order to overcome the challenge?</td>
    <td class="col-md-9"><textarea ng-model="form.analysis2.support_required" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></textarea></td>
  </tr>
  </tbody>
</table>
</div>
</ng-include>
                    </div>
                  </div><!-- end ngRepeat: tab in tabset.tabs -->
                  
                  <div class="tab-pane ng-scope" id="tab15">

                    <div class="panel-body ng-scope">
                      <!-- ngInclude: --><ng-include src="'app/reh/partials/main_entry/advocacy.html'" class="ng-scope"><div class="bg-heading p-xs b-r-sm ng-scope"><i class="fa fa-arrow-right"></i> Vision Centres /Camps</div>
<div class="table-responsive ng-scope">
<table class="table table-bordered">
  <thead>
  <tr>
    <td>Metric</td>
    <td>Value</td>
  </tr>
  </thead>
  <tbody>
  <tr>
    <td>Number of meeting/workshop organized for prioritising eye health and fostering integration with wider health system (NRHM)</td>
    <td><input type="number" ng-model="form.advocacy.workshop_nrhm" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of meeting held for advocacy for inclusion of Eye health component in the General Health training</td>
    <td><input type="number" ng-model="form.advocacy.workshop_advocacy" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of meetings attended at the National/District/ State/block/village level Health Committees</td>
    <td><input type="number" ng-model="form.advocacy.health_committee" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of review meetings of the Government attended at National and state level</td>
    <td><input type="number" ng-model="form.advocacy.review_meeting" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of Village Health Sanitation &amp; Nutrition Committee meetings attended by Project staff (NGO/Hospital/Government/Other)</td>
    <td><input type="number" ng-model="form.advocacy.sanitation" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  </tbody>
</table>
</div>
</ng-include>
                    </div>
                  </div><!-- end ngRepeat: tab in tabset.tabs -->
                  <div class="tab-pane ng-scope" id="tab16">

                    <div class="panel-body ng-scope">
                      <!-- ngInclude: --><ng-include src="'app/reh/partials/main_entry/analysis3.html'" class="ng-scope"><div class="bg-heading p-xs b-r-sm ng-scope"><i class="fa fa-arrow-right"></i> Analysis 3</div>
<h4 class="ng-scope">
  Please provide a brief analysis of the information for the following sections:
  <ul>
    <li>Advocacy, networking, Liaison</li>
  </ul>
</h4>
<div class="table-responsive ng-scope">
<table class="table table-bordered">
  <thead>
  <tr>
    <td>Question</td>
    <td>Response</td>
  </tr>
  </thead>
  <tbody>
  <tr>
    <td class="col-md-3">What worked well?</td>
    <td class="col-md-9"><textarea ng-model="form.analysis3.worked_well" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></textarea></td>
  </tr>
  <tr>
    <td class="col-md-3">What according to you was not satisfactory and you will like to see improved (give reasons for
      Variation against target)
    </td>
    <td class="col-md-9"><textarea ng-model="form.analysis3.not_satisfactory" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></textarea></td>
  </tr>
  <tr>
    <td class="col-md-3">What challenges were faced and how they were mitigated?</td>
    <td class="col-md-9"><textarea ng-model="form.analysis3.challenges" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></textarea></td>
  </tr>
  <tr>
    <td class="col-md-3">What support would you require in order to overcome the challenge?</td>
    <td class="col-md-9"><textarea ng-model="form.analysis3.support_required" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></textarea></td>
  </tr>
  </tbody>
</table>
</div>
</ng-include>
                    </div>
                  </div><!-- end ngRepeat: tab in tabset.tabs -->
                  
                  <div class="tab-pane ng-scope" id="tab17">

                    <div class="panel-body ng-scope">
                      <!-- ngInclude: --><ng-include src="'app/reh/partials/main_entry/iec_bcc.html'" class="ng-scope"><div class="bg-heading p-xs b-r-sm ng-scope"><i class="fa fa-arrow-right"></i> IEC / BCC Activity</div>
<div class="table-responsive ng-scope">
<table class="table table-bordered">
  <thead>
  <tr>
    <td>Metric</td>
    <td>Value</td>
  </tr>
  </thead>
  <tbody>
  <tr>
    <td>Number of community level awareness events undertaken</td>
    <td><input type="number" ng-model="form.iec_bcc.awareness_events" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of community Based Institutions actively involved in eye health service demand generation</td>
    <td><input type="number" ng-model="form.iec_bcc.institutions_involved" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of community members sensitised on eye health issues</td>
    <td><input type="number" ng-model="form.iec_bcc.members_sensitised" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of IEC/ BCC Posters distributed</td>
    <td><input type="number" ng-model="form.iec_bcc.distribution.posters" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of IEC/ BCC Booklet/ Leaflets distributed</td>
    <td><input type="number" ng-model="form.iec_bcc.distribution.leaflets" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of IEC/ BCC Pamphlets distributed</td>
    <td><input type="number" ng-model="form.iec_bcc.distribution.pamphlets" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of IEC/ BCC Wall Writing distributed</td>
    <td><input type="number" ng-model="form.iec_bcc.distribution.wall_writing" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of IEC/ BCC Other materials distributed</td>
    <td><input type="number" ng-model="form.iec_bcc.distribution.other" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of health workers provided awareness towards importance of eye health.</td>
    <td><input type="number" ng-model="form.iec_bcc.awareness.health_workers" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of community leaders provided awareness towards importance of eye health</td>
    <td><input type="number" ng-model="form.iec_bcc.awareness.community_leaders" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  <tr>
    <td>Number of other provided Awareness / Sensitize</td>
    <td><input type="number" ng-model="form.iec_bcc.awareness.other" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></td>
  </tr>
  </tbody>
</table>
</div>
</ng-include>
                    </div>
                  </div><!-- end ngRepeat: tab in tabset.tabs -->
                  
                  <div class="tab-pane ng-scope" id="tab18">

                    <div class="panel-body ng-scope">
                      <!-- ngInclude: --><ng-include src="'app/reh/partials/main_entry/analysis4.html'" class="ng-scope"><div class="bg-heading p-xs b-r-sm ng-scope"><i class="fa fa-arrow-right"></i> Analysis 4</div>
<h4 class="ng-scope">
  Please provide a brief analysis of the information for the following sections:
  <ul>
    <li>IEC/ BCC Activity</li>
  </ul>
</h4>
<div class="table-responsive ng-scope">
<table class="table table-bordered">
  <thead>
  <tr>
    <td>Question</td>
    <td>Response</td>
  </tr>
  </thead>
  <tbody>
  <tr>
    <td class="col-md-3">What worked well?</td>
    <td class="col-md-9"><textarea ng-model="form.analysis4.worked_well" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></textarea></td>
  </tr>
  <tr>
    <td class="col-md-3">What according to you was not satisfactory and you will like to see improved (give reasons for
      Variation against target)
    </td>
    <td class="col-md-9"><textarea ng-model="form.analysis4.not_satisfactory" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></textarea></td>
  </tr>
  <tr>
    <td class="col-md-3">What challenges were faced and how they were mitigated?</td>
    <td class="col-md-9"><textarea ng-model="form.analysis4.challenges" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></textarea></td>
  </tr>
  <tr>
    <td class="col-md-3">What support would you require in order to overcome the challenge?</td>
    <td class="col-md-9"><textarea ng-model="form.analysis4.support_required" class="form-control ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false"></textarea></td>
  </tr>
  </tbody>
</table>
</div>
</ng-include>
                    </div>
                  </div><!-- end ngRepeat: tab in tabset.tabs -->
  </div>
</div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(){


        var textarea = document.getElementById("code1");

        // Wait until animation finished render container
        setTimeout(function(){

            CodeMirror.fromTextArea(textarea, {
                lineNumbers: true,
                matchBrackets: true,
                styleActiveLine: true
            });
        }, 500);

    });
</script>