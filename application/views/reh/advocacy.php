
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Sight Savers</title>
        <link href="<?php echo PUBLIC_URL; ?>css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo PUBLIC_URL; ?>font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo PUBLIC_URL; ?>css/animate.css" rel="stylesheet">
        <link href="<?php echo PUBLIC_URL; ?>css/style.css" rel="stylesheet">
        <link href="<?php echo PUBLIC_URL; ?>css/custom.css" rel="stylesheet">
        <link href="<?php echo PUBLIC_URL; ?>css/lc_switch.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo PUBLIC_URL; ?>css/codemirror.css">
        <link rel="stylesheet" href="<?php echo PUBLIC_URL; ?>css/custom.css">
		 <link href="<?php echo PUBLIC_URL; ?>css/bootstrap_datepicker.css" rel="stylesheet"> 
     	<link href="<?php echo PUBLIC_URL; ?>css/dataTables.bootstrap.min.css" rel="stylesheet">
    </head>
<body class="fixed-sidebar">

<div id="wrapper">

<nav class="navbar-admin navbar-static-side" role="navigation">
    <div class="sidebar-collapse">

      <ul side-navigation="" class="nav metismenu" id="side-menu">
      <li class="nav-header" style="padding: 20px;">
        <div class="profile-element">
          <img alt="image" width="100%" src="<?php echo PUBLIC_URL; ?>img/logo.png" src="<?php echo PUBLIC_URL; ?>assets/images/logo.png">
        </div>
        <div class="logo-element">
          SS
        </div>
      </li>
	 
 <?php 
  if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==6)  { ?> 
	   <li ng-class="{active: m.ie}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Data Entry</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" id="menhead123" ng-class="{in: m.ie}" aria-expanded="false">
	  
     </ul>
      </li>
		<li class="active">
        	<a href="<?php echo BASE_URL.'partner_mpr';?>"><i class="fa fa-eye"></i> <span class="nav-label">Review MPR</span></a>
        
      	</li>
     <?php }  ?>
	 
	   <?php  if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==1 || $this->session->userdata('userinfo')['default_role']==5 || $this->session->userdata('userinfo')['default_role']==2 || $this->session->userdata('userinfo')['default_role']==3 || $this->session->userdata('userinfo')['default_role']==4)  { ?>
	  
	   <li ng-class="{active: m.dashboards}" >
		  <li class="active">
			<a href="<?php echo BASE_URL.'dashboard_speedometer';?>"><i class="fa fa-dashboard"></i> <span class="nav-label">Dashboard</span> </a>
			</li>
			<!--li><a href="<?php //echo BASE_URL.'dashboard';?>">Report</a></li-->
			  <li ng-class="{active: m.reports}">
				<a href=""><i class="fa fa-file-pdf-o"></i> <span class="nav-label">Graph</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse" ng-class="{in: m.reports}" aria-expanded="true">
				  <li  ><a href="#">Social Inclusion</a></li>
				  <li ><a  href="<?php echo BASE_URL.'dashboard_graph';?>">Rural Eye Health</a></li>
				  <li ><a  href="<?php echo BASE_URL.'dashboard_graph_ueh';?>">Urban Eye Health</a></li>
				  <li ><a  href="#">Inclusive Education</a></li>
				</ul>
			  </li>
		  </li>
			<li ng-class="{active: m.reports}" >
				<a href=""><i class="fa fa-file-pdf-o"></i> <span class="nav-label">MPR</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse" ng-class="{in: m.reports}" aria-expanded="true">
				  <li  ><a href="<?php echo BASE_URL.'mpr_si';?>">Social Inclusion</a></li>
				  <li ><a  href="<?php echo BASE_URL.'mpr_reh' ;?>">Rural Eye Health</a></li>
				  <li ><a  href="<?php echo BASE_URL.'mpr_ueh';?>">Urban Eye Health</a></li>
				  <li ><a  href="<?php echo BASE_URL.'mpr_ie';?>">Inclusive Education</a></li>
				</ul>
			</li>
			
			<li ng-class="{active: m.reports}" >
				<a href=""><i class="fa fa-file-pdf-o"></i> <span class="nav-label">Yearly Target</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse" ng-class="{in: m.reports}" aria-expanded="true">
     			 <li ><a  href="<?php echo BASE_URL.'reh_yearly_target/';echo base64_encode('rural eye health-yearly target');?>">REH Yearly Targets</a></li>
				   <li ><a  href="<?php echo BASE_URL.'ueh_yearly_target/';echo base64_encode('urban eye health-yearly target');?>">UEH Yearly Targets</a></li>
				</ul>
			</li>
			
		<?php if($this->session->userdata('userinfo')['default_role']==2 || $this->session->userdata('userinfo')['default_role']==3 || $this->session->userdata('userinfo')['default_role']==4) { ?>
		<li ng-class="{active: m.reports}" >
				<a href=""><i class="fa fa-file-pdf-o"></i> <span class="nav-label">Data approval</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse" ng-class="{in: m.reports}" aria-expanded="true">
				  <li  ><a href="<?php echo BASE_URL.'Approval';?>">Review MPR</a></li>				  
				</ul>
			</li>
		<?php }  ?>
		<?php  if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==1) { ?>
		<li ng-class="{active: m.reports}" >
				<a href=""><i class="fa fa-file-pdf-o"></i> <span class="nav-label">Data approval</span><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse" ng-class="{in: m.reports}" aria-expanded="true">
				  <li  ><a href="<?php echo BASE_URL.'Approvaladmin';?>">Review MPR</a></li>				  
				</ul>
			</li>
		<?php } ?>
		
		
		
	  
	  <li ng-class="{active: m.ie}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Data Entry</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.ie}" aria-expanded="false">
        <li ng-class="{active: m.ie}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Inclusive Education</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.ie}" aria-expanded="false">
          <li ><a href="<?php echo BASE_URL.'ie_main_entry/';echo base64_encode('Inclusive education-main entry'); ?>">IE Main Entry</a></li>
        </ul>
      </li>

      <li ng-class="{active: m.reh}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Rural Eye Health</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.reh}" aria-expanded="false">
          <li ><a  href="<?php echo BASE_URL.'reh_main_entry/';echo base64_encode('rural eye health-main entry');?>">REH Main Entry</a></li>
          <!--<li ><a  href="<?php //echo BASE_URL.'reh_refferal';?>">Referrals</a></li>-->
		  <li><a href="<?php echo BASE_URL.'reh_base_hospital';?>">REH Base Hospital</a></li>
          <li ><a  href="<?php echo BASE_URL.'reh_training';?>">REH Training | Capacity Building</a></li>
          <li ><a  href="<?php echo BASE_URL.'reh_advocacy';?>">REH Advocacy | Networking | Liasion</a></li>
          <li ><a  href="<?php echo BASE_URL.'reh_bcc';?>">REH BCC Entry</a></li>
          <li ><a  href="<?php echo BASE_URL.'reh_iec';?>">REH IEC Entry</a></li>
          <li ><a  href="<?php echo BASE_URL.'reh_vhsnc';?>">VHSNC Entry</a></li>
         <!-- <li ><a  href="javascript:void(0)">Errors in submission</a></li>  -->
        </ul>
      </li>
	  
      <li ng-class="{active: m.si}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Social Inclusion</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.si}" aria-expanded="false">
          <li ><a  href="<?php echo BASE_URL.'si_pwd_search';?>">SI PWD Search</a></li>
          <li ><a  href="<?php echo BASE_URL.'si_main_entry/';echo base64_encode('Social inclusion-main entry');?>">SI Main Entry</a></li>
          <li ><a  href="<?php echo BASE_URL.'shg';?>">SI SHG | PPG</a></li>
          <li ><a  href="<?php echo BASE_URL.'bpo';?>">SI BPO | DPO</a></li>
          <li ><a  href="<?php echo BASE_URL.'training';?>">SI Training Activity</a></li>
          <li ><a  href="<?php echo BASE_URL.'access_aidit';?>">SI Access Audit</a></li>
          <li ><a  href="<?php echo BASE_URL.'agencies';?>">SI Agencies Support</a></li>
          <li ><a  href="<?php echo BASE_URL.'advocacy';?>">SI Advocacy Activity</a></li>
          <li ><a  href="<?php echo BASE_URL.'iec';?>">SI IEC Activity</a></li>
          <li ><a  href="<?php echo BASE_URL.'bcc';?>">SI BCC Activity</a></li>
          <li ><a  href="<?php echo BASE_URL.'si_yearly_targets';?>">SI Yearly Targets</a></li>
         <!-- <li ><a  href="javascript:void(0)">Errors in PWD</a></li>  -->
        </ul>
      </li>
	  
	 
      <li ng-class="{active: m.ueh}">
        <a href=""><i class="fa fa-eye"></i> <span class="nav-label">Urban Eye Health</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.ueh}" aria-expanded="false">
          <li ><a  href="<?php echo BASE_URL.'ueh_main_entry/';echo base64_encode('urban eye health-main entry');?>">Main Entry</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_vision_center';?>">Vision Center</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_base_hospital';?>">Base Hospital</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_outreach_camp';?>">Outreach Camp</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_training_entry';?>">Training | Capacity Building</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_advocacy_entry';?>">Advocacy | Networking | Liasion</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_bcc_entry';?>">BCC Entry</a></li>
          <li ><a  href="<?php echo BASE_URL.'ueh_iec_entry';?>">IEC</a></li>
        </ul>
      </li>
        </ul>
      </li>

      <li ng-class="{active: m.admin}">
        <a href=""><i class="fa fa-gears"></i> <span class="nav-label">Administration</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" ng-class="{in: m.admin}" aria-expanded="false">
		  <li ><a  href="<?php echo BASE_URL.'Administration/';echo 'districts';?>">Districts</a></li>
          <li ><a  href="<?php echo BASE_URL.'Administration/';echo 'partner';?>">Partners</a></li>          
          <li ><a  href="<?php echo BASE_URL.'Administration/';echo 'user';?>">Users</a></li>
          <li ><a  href="<?php echo BASE_URL.'Administration/';echo 'reporting_quarters';?>">Reporting Quarters</a></li>
        </ul>
      </li>
	  <?php } if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==6) {} ?>
    </ul>
    </div>
</nav>
<div id="page-wrapper" class="gray-bg">
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary SeeMore2 " href="#"> <i class="fa fa-caret-left" aria-hidden="true"></i>	
           </a>

        </div>
		
        <ul class="nav navbar-top-links navbar-right">
	      <?php   if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==6) {  ?>
		   <li>
			<select class="form-control" id="selectId" onchange="part_fill(this.value);program_district(this.value);">
				<option value="">Select partner</option>
				<?php
                  if(!empty($dis)){
                    foreach($dis as $dist){ ?>
					<option value="<?php echo $dist->ss_partners_id ?>"<?php if($dist->ss_partners_id==$_SESSION["partner_id"])echo 'selected'; else echo ''; ?>><?php echo $dist->ss_partners_name; ?></option>
				  <?php } } ?>
				</select>
	      </li>
			
			<?php  }  ?>
          <li>
            <div class="text-muted welcome-message">
              <a ui-sref="main.profile" class="ng-binding"><span class="user-img"><img src="<?php echo PUBLIC_URL;?>img/default-user-icon-profile.png" alt=""></span> Hi <?php   echo $this->session->userdata('userinfo')['fname'] ?></a>
            </div>
          </li>
          <li>
            <a href="<?php echo BASE_URL.'logout';?>">
              <i class="fa fa-sign-out"></i> Log out
            </a>
          </li>
        </ul>
    </nav>
</div>
<div class="row wrapper border-bottom white-bg page-heading ng-scope">
  <div class="col-lg-10">
    <h2><?php echo @strtoupper($this->session->userdata['userinfo']['partner_name']); ?></h2>
  </div>
</div>
<div class="wrapper wrapper-content animated fadeIn ng-scope" ng-init="init()">
<?php if(isset($_SESSION['success']))
{ ?>
<div class="alert alert-success">
  <strong></strong> <?php echo $_SESSION['success'] ; ?>
</div>
<?php } ?>
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox">
        <div class="ibox-title">
          <h5>Rural Eye Health / REH Advocacy | Networking | Liasion</h5>
        </div>
        <div class="ibox-content">
		
		<?php 
	//	$attributes = array('id' => 'myform');
		//echo form_open('filter_advocacy_reh',$attributes);?>
		<form method="get" action="<?php echo BASE_URL.'filter_advocacy_reh/'; ?>">
          <div class="row flex-element center-both">
            <div class="well well-sm main-entry-box">
              <div class="col-md-3 text-right">
                    <label>Month:</label> 
                </div>
              <div class="col-md-5">
			  	<?php
				if(!isset($_SESSION["month_date"])) {
				  $_SESSION["month_date"] = date("m-Y", strtotime("-1 months"));
				 }
				  ?>
               <input type="text" name="month_from" value="<?php echo $_SESSION["month_date"]; ?>" id="month_from" class="form-control datepicker" onchange="date_fill()">
			   <span class="text-danger ng-binding ng-hide" aria-hidden="true">
			   <?php // echo form_error('month_data'); ?>
				<?php echo form_error('month_from');?>
			   </span>
              </div>
              <div class="col-md-4">
                <button class="btn btn-sm btn-primary btn-block" type="submit">
                Get Data
              </button>
              </div>
            </div>
          </div>
         </form>

          <div class="row " ng-show="hasData" aria-hidden="false" style="">
            <div class="col-md-12">
			<?php echo form_open('create_advocacy_reh');?>
              <div class="panel panel-primary">
                <div class="panel-heading">
                  Advocacy / Networking / Liasion Entry
                </div>
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-4">
					<input type="hidden"  name="id_ajax" id="id_ajax1">
                      <div class="form-group">
                        <label>Block / Location Name</label>
						<input type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="advocacy_block" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Event Type</label>
                        <select class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="advocacy_event_type" id="adcocacy_event-ajax" aria-invalid="false">
						<option value="">Select</option>
                          <option value="Meeting" class="ng-binding ng-scope">Meeting</option>
						  <option value="Review Meeting" class="ng-binding ng-scope">Review Meeting</option>
						  <option value="Workshop" class="ng-binding ng-scope">Workshop</option>
						  <option value="Other" class="ng-binding ng-scope">Other</option>
                        </select>
                        <p class="text-danger ng-binding ng-hide" aria-hidden="true"></p>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Event type details</label>
						<input type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="advocacy_event_detail" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" aria-hidden="true"></p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Meeting Purpose</label>
						<input type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="advocacy_meeting_purpose" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Held to prioritise eye health?</label>
                        <select class="form-control ng-pristine ng-untouched ng-valid ng-empty" id="advocacy_prioritise-ajax" name="advocacy_prioritise" aria-invalid="false">
						<option value="">Select</option>
                          <option value="yes">Yes</option>
                          <option value="no">No</option>
                        </select>
                        <p class="text-danger ng-binding ng-hide" aria-hidden="true"></p>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Held for inclusion eye health component in general health training?</label>
                        <select class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="advocacy_inclusion_eh" id="advocacy_inclusion_eh-ajax" aria-invalid="false">
						<option value=""></option>
                          <option value="yes">Yes</option>
                          <option value="no">No</option>
                        </select>
                        <p class="text-danger ng-binding ng-hide" aria-hidden="true"></p>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Meeting level</label>
                        <select class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="advocacy_meeting_level" id="advocacy_meeting_level-ajax" aria-invalid="false">
						<option value="">Select</option>
                          <option value="Block" class="ng-binding ng-scope">Block</option>
						  <option value="Village" class="ng-binding ng-scope">Village</option>
						  <option value="District" class="ng-binding ng-scope">District</option>
						  <option value="State" class="ng-binding ng-scope">State</option>
						  <option value="National" class="ng-binding ng-scope">National</option>
                        </select>
                        <p class="text-danger ng-binding ng-hide" aria-hidden="true"></p>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Meeting organised by</label>
                        <select class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="advocacy_meeting_organised" id="advocacy_meeting_organised-ajax" aria-invalid="false">
						<option value="">Select</option>
                          <option value="Government" class="ng-binding ng-scope">Government</option>
						  <option value="Health Committees" class="ng-binding ng-scope">Health Committees</option>
						  <option value="Partner and Sightsavers" class="ng-binding ng-scope">Partner &amp; Sightsavers</option>
						  <option value="Other" class="ng-binding ng-scope">Other</option>
                        </select>
                        <p class="text-danger ng-binding ng-hide" aria-hidden="true"></p>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Meeting organised by details</label> 
						<input type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="advocacy_meeting_organised_detail" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" aria-hidden="true"></p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Meeting Held with</label> 
						<input type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="advocacy_meeting_with" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Issues discussed</label> 
						<input type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" name="advocacy_issue_discuss" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" aria-hidden="true"></p>
                    </div>
                  </div>

                </div>

                <div class="panel-footer">
                  <div class="flex-element  align-bw">
                    <div class="footer-text">
                      <h5>Please review your changes before you save them. Changes once saved cannot be
                reverted.</h5>
                    </div>
					<input type="hidden" name="month_data"/>
                    <div class="save-btn">
                      <button class="btn  btn-primary" type="submit">
                          Save Changes
                      </button>
                    </div>
                  </div>
                </div>

              </div>
			  <?php echo form_close();?>
              <div class="table-responsive">
			  <?php if(!empty($advocacy_data)){ ?>
                <table id="table_data" class="table table-bordered">
                  <thead>
                    <tr><th>Block / Location</th>
                    <th>Event Type</th>
                    <th>Event Type details</th>
                    <th>Meeting Purpose</th>
                    <th>Held for priority?</th>
                    <th>Held for inclusion?</th>
                    <th>Level</th>
                    <th>Organised by</th>
                    <th>Organised by details</th>
                    <th>Held with</th>
                    <th>Issues discussed</th>
                    <th>Action1</th>
					<th>Action2</th>
                  </tr></thead>
                  <tbody>
                    <?php foreach($advocacy_data as $tdata){?>
					<tr>
						<td><?php echo $tdata->ss_reh_advocacy_block;?></td>
						<td><?php echo $tdata->ss_reh_advocacy_event_type;?></td>
						<td><?php echo $tdata->ss_reh_advocacy_event_detail;?></td>
						<td><?php echo $tdata->ss_reh_advocacy_meeting_purpose;?></td>
						<td><?php echo $tdata->ss_reh_advocacy_priorities;?></td>
						<td><?php echo $tdata->ss_reh_advocacy_inclusion_eye_health;?></td>
						<td><?php echo $tdata->ss_reh_advocacy_meeting_level;?></td>
						<td><?php echo $tdata->ss_reh_advocacy_meeting_organised;?></td>
						<td><?php echo $tdata->ss_reh_advocacy_meeting_organised_detail;?></td>
						<td><?php echo $tdata->ss_reh_advocacy_meeting_with;?></td>
						<td><?php echo $tdata->ss_reh_advocacy_issue;?></td>
						<td><a onclick="return confirmDelete();" href="<?php echo BASE_URL.'delete_advocacy_reh/'.base64_encode($tdata->ss_reh_advocacy_id); ?>">Delete</a></td>
            <td><a href="javascript:void(0)" class="editThis" data-val="<?php echo base64_encode($tdata->ss_reh_advocacy_id); ?>">Edit</a></td>
					</tr>
				  <?php }?>
                  </tbody>
                </table>
			  <?php }?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="<?php echo PUBLIC_URL; ?>js/jquery-2.1.1.js"></script>
<script>

$(".editThis").click(function(){
	
	var dataid = $(this).attr('data-val');
	var cct = $("input[name='csrf_test_name']").val();
	//alert(cct);
	//var cct = $.cookie("<?php echo $this->config->item("csrf_cookie_name"); ?>");
	post_data ={ id: dataid,<?php echo $this->security->get_csrf_token_name(); ?> : cct};
	if(dataid!=0 || dataid !="")
	{
    $.ajax({ 
            url: "<?php echo BASE_URL.'reh_advocacy/edit_advocacy_reh';?>",
            data: post_data,
            type: 'POST'
        }).done(function(responseData) { //alert(responseData); return false;
		
		var csrf_t = responseData.split("$$$");
		var obj = JSON.parse(csrf_t[0]);
       
		$("input[name='csrf_test_name']").val(csrf_t[1]);
	    
		$("input[name='date_month']").val(obj['ss_reh_advocacy_month']);
		$("input[name='advocacy_block']").val(obj['ss_reh_advocacy_block']);
	    var adcocacy_event_ajax = obj['ss_reh_advocacy_event_type'];
		$("input[name='advocacy_event_detail']").val(obj['ss_reh_advocacy_event_detail']);
		$("input[name='advocacy_meeting_purpose']").val(obj['ss_reh_advocacy_meeting_purpose']);
		
		var advocacy_prioritise_ajax = obj['ss_reh_advocacy_priorities'];
		var advocacy_inclusion_eh_ajax = obj['ss_reh_advocacy_inclusion_eye_health'];
		var advocacy_meeting_level_ajax = obj['ss_reh_advocacy_meeting_level'];
		
	    var advocacy_meeting_organised_ajax = obj['ss_reh_advocacy_meeting_organised'];
		$("input[name='advocacy_meeting_organised_detail']").val(obj['ss_reh_advocacy_meeting_organised_detail']);
		$("input[name='advocacy_meeting_with']").val(obj['ss_reh_advocacy_meeting_with']);
	    $("input[name='advocacy_issue_discuss']").val(obj['ss_reh_advocacy_issue']);
		$("input[name='id_ajax']").val(obj['ss_reh_advocacy_id']);
		
	// for  adcocacy_event_ajax   
	$("#adcocacy_event-ajax option").each(function (key,value) { 
	   if(adcocacy_event_ajax==$(this).val())
	   {
		 $(this).prop('selected', true);  
	   }
	});  
	
	// for  advocacy_prioritise_ajax   
	$("#advocacy_prioritise-ajax option").each(function (key,value) { 
	   if(advocacy_prioritise_ajax==$(this).val())
	   {
		 $(this).prop('selected', true);  
	   }
	});
	
	// for  advocacy_inclusion_eh-ajax   
	$("#advocacy_inclusion_eh-ajax option").each(function (key,value) { 
	   if(advocacy_inclusion_eh_ajax==$(this).val())
	   {
		 $(this).prop('selected', true);  
	   }
	});
	
	// for advocacy_meeting_level-ajax   
	$("#advocacy_meeting_level-ajax option").each(function (key,value) { 
	   if(advocacy_meeting_level_ajax==$(this).val())
	   {
		 $(this).prop('selected', true);  
	   }
	});
	
	// for advocacy_meeting_organised-ajax   
	$("#advocacy_meeting_organised-ajax option").each(function (key,value) { 
	   if(advocacy_meeting_organised_ajax==$(this).val())
	   {
		 $(this).prop('selected', true);  
	   }
	});
	

        console.log('Done: ', responseData);
        }).fail(function() {
            console.log('Failed');
        });
	}
})

</script>


<script type="text/javascript">
    function confirmDelete() 
	{
        return confirm('Do you really wants to delete?');
    }
</script>

