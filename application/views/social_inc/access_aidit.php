<div class="row wrapper border-bottom white-bg page-heading ng-scope">
  <div class="col-lg-10">
    <h2><?php echo @strtoupper($this->session->userdata['userinfo']['partner_name']); ?></h2>
  </div>
</div>

<div class="wrapper wrapper-content animated fadeIn ng-scope" ng-init="init()">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox">
        <div class="ibox-title">
          <h5>Access Audit Entry</h5>
        </div>
        <div class="ibox-content">
          <?php echo form_open('filter_access_si');?>
          <div class="row flex-element center-both">
            <div class="well well-sm main-entry-box">
              <div class="col-md-3 text-right">
                <label>Month:</label> 
              </div>
              <div class="col-md-5"><?php
			                     if(!isset($_SESSION["month_date"])) {
								 $server_date = server_date_time(); 
							     date('m-Y',strtotime($server_date));
							     $_SESSION["month_date"] = date('m-Y',strtotime($server_date));
								 }
			?>
			  
               <input type="text" name="month_from" value="<?php echo $_SESSION["month_date"]; ?>" id="month_from" class="form-control datepicker" onchange="date_fill()">
			   <span class="text-danger ng-binding ng-hide" aria-hidden="true"><?php echo form_error('month_data');?>
				<?php echo form_error('month_from');?>
			   </span>
              </div>
              <div class="col-md-4">
			  
			  <input type="hidden" name="month_data"/>
                <button class="btn btn-sm btn-primary btn-block" type="submit">
                Get Data
              </button>
              </div>
            </div>
          </div>
          <?php echo form_close();?>
          

          <div class="row" ng-show="hasData" aria-hidden="false" style="">
            <div class="col-md-12">
			<?php echo form_open('create_access_si');?>
              <div class="panel panel-primary">
                <div class="panel-heading">
                  Access Audit Entry
                </div>
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Name of Organisation / Building / Infrastructure / Services / System</label>
              <input name="name_service" type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.name" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.name" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Place of Access Audit</label>
                        <select name="access_audit" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.place" aria-invalid="false"><option value="? undefined:undefined ?"></option>
                          <option value="Organization">Organization</option>
                          <option value="Public Building">Public Building</option>
                          <option value="Public Infrastructure">Public Infrastructure</option>
                          <option value="Website">Website</option>
                          <option value="Documents">Documents</option>
                        </select>
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.place" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Number of Male members who conducted access audit</label>
                        <input type="number" name="male" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.male_members" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.male_members" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Number of Female members who conducted access audit</label>
                        <input type="number" name="female" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.female_members" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.female_members" aria-hidden="true"></p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Audit Date</label>
                        <p class="">
                          <input name="audit_date" type="date" class="form-control"><!-- ngIf: isOpen -->

                        </p>
                          
    
                        
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.audit_date" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Report Prepared</label>
                        <select name="report" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.report_prepared" aria-invalid="false">
						<option value="? undefined:undefined ?"></option>
                          <option value="true">Yes</option>
                          <option value="false">No</option>
                        </select>
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.report_prepared" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Report submitted to organisation</label>
                        <select name="report_submit" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.report_submitted" aria-invalid="false">
						<option value="? undefined:undefined ?"></option>
                          <option value="true">Yes</option>
                          <option value="false">No</option>
                        </select>
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.report_submitted" aria-hidden="true"></p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Brief recommendations for Improvement</label>
                        <input type="text" name="brief" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.recommendations" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.recommendations" aria-hidden="true"></p>
                    </div>
                  </div>

                </div>
                <div class="panel-footer">
                  <div class="flex-element  align-bw">
                    <div class="footer-text">
                      <h5>Please review your changes before you save them. Changes once saved cannot be
                reverted.</h5>
                    </div>
                    <div class="save-btn">
					<input type="hidden" name="month_data"/>
                      <button class="btn  btn-primary" type="submit" ng-click="onSubmit();">
                          Save Changes
                      </button>
                    </div>
                  </div>
                </div>
              </div>
			   <?php echo form_close();?>
			   
			    <?php if(!empty($access_get)){ ?>
              <div class="table-responsive">
                <table class="table table-bordered">
                  <thead>
                    <tr><th>Name</th>
                    <th>Place of Audit</th>
                    <th>Male members who conducted access audit</th>
                    <th>Female members who conducted access audit</th>
                    <th>Audit Date</th>
                    <th>Report Prepared</th>
                    <th>Report submitted to organisation</th>
                    <th>Brief recommendations for Improvement</th>
                   
                  </tr></thead>
                  <tbody>
				    <?php foreach($access_get as $access_get1) { ?>
                    <tr>
					<td><?php echo $access_get1->ss_access_name;  ?></td>
                    <td><?php echo $access_get1->ss_place_of_access;  ?></td>
					<td><?php echo $access_get1->ss_access_on_male;  ?></td>
					<td><?php echo $access_get1->ss_access_on_female;  ?></td>
					<td><?php echo $access_get1->ss_audit_date;  ?></td>
					<td><?php echo $access_get1->ss_report;  ?></td>
					<td><?php echo $access_get1->ss_report_submit;  ?></td>
					<td><?php echo $access_get1->ss_brief;  ?></td>
                    </tr>
					<?php } ?>
                  </tbody>
                </table>
              </div>
				<?php }  ?>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(){


        var textarea = document.getElementById("code1");

        // Wait until animation finished render container
        setTimeout(function(){

            CodeMirror.fromTextArea(textarea, {
                lineNumbers: true,
                matchBrackets: true,
                styleActiveLine: true
            });
        }, 500);

    });
</script>