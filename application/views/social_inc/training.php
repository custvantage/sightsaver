<div class="row wrapper border-bottom white-bg page-heading ng-scope">
  <div class="col-lg-10">
    <h2><?php echo @strtoupper($this->session->userdata['userinfo']['partner_name']); ?></h2>
  </div>
</div>

<div class="wrapper wrapper-content animated fadeIn ng-scope" ng-init="init()">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox">
        <div class="ibox-title">
          <h5>Social Inclusion Training / SI Training Activity</h5>
        </div>
        <div class="ibox-content">
         <?php echo form_open('filter_training_si');?>
          <div class="row flex-element center-both">
            <div class="well well-sm main-entry-box">
              <div class="col-md-3 text-right">
                <label>Month:</label> 
              </div>
              <div class="col-md-5">
			   <?php
				 if(!isset($_SESSION["month_date"])) {
				 $server_date = server_date_time(); 
				 date('m-Y',strtotime($server_date));
				 $_SESSION["month_date"] = date('m-Y',strtotime($server_date));
				 }
				  ?>
               <input type="text" name="month_from" id="month_from" value="<?php echo $_SESSION["month_date"]; ?>" class="form-control datepicker" onchange="date_fill()">
			   <span class="text-danger ng-binding ng-hide" aria-hidden="true"><?php echo form_error('month_data');?>
				<?php echo form_error('month_from');?>
			   </span>
              </div>
              <div class="col-md-4">
                <button class="btn btn-sm btn-primary btn-block" type="submit">
                Get Data
              </button>
              </div>
            </div>
          </div>
          <?php echo form_close();?>
          

          <div class="row " ng-show="hasData" aria-hidden="false" style="">
            <div class="col-md-12">
			<?php echo form_open('create_training');?>
              <div class="panel panel-primary">
                <div class="panel-heading">
                  Training / Capacity Building Entry
                </div>
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Name of Block / Location</label> 
						<input type="text" name="name_block" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.location_name" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.location_name" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Name of Person</label>
						<input type="text" name="name_person" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.person_name" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.person_name" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Gender</label>
                       <select name="gender" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.gender" aria-invalid="false">
						<option value="? undefined:undefined ?"></option>
                          <option value="Male">Male</option>
                          <option value="Female">Female</option>
                        </select>
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.gender" aria-hidden="true"></p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Person Category</label>
                        <select name="cagtegory" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.person_category" aria-invalid="false"><option value="? undefined:undefined ?"></option>
                          <option value="Project Staff">Project Staff</option>
                          <option value="Rehabilitation Specialists">Rehabilitation Specialists</option>
                          <option value="Social Workers / CBR workers">Social Workers / CBR workers</option>
                          <option value="Community Health Worker">Community Health Worker</option>
                          <option value="Community Volunteers">Community Volunteers</option>
                          <option value="Community members">Community members</option>
                          <option value="Govt. Officials">Govt. Officials</option>
                          <option value="Other">Other</option>
                        </select>
                        <p class="text-danger ng-binding ng-hide" ng-show="errors.person_category" aria-hidden="true"></p>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Person Category Details (If category is Other)</label>
						<input type="text" name="category_detail" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.person_category_details" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.person_category_details" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Topics Covered</label> 
						<input type="text" name="topic" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.topics" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.topics" aria-hidden="true"></p>
                    </div>
                  </div>
                </div>
                <div class="panel-footer">
                  <div class="flex-element  align-bw">
                    <div class="footer-text">
                      <h5>Please review your changes before you save them. Changes once saved cannot be
                        reverted.</h5>
                    </div>
                    <div class="save-btn">
					<input type="hidden" name="month_data"/>
                      <button class="btn  btn-primary" type="submit" ng-click="onSubmit();">
                          Save Changes
                      </button>
                    </div>
                  </div>
                </div>
              </div>
			   <?php echo form_close();?>
			    <?php if(!empty($training_get)){ ?>
              <div class="table-responsive">
                <table class="table table-bordered">
                  <thead>
                    <tr><th>Name of Block / Location</th>
                    <th>Name of Person</th>
                    <th>Gender</th>
                    <th>Person Category</th>
                    <th>Person Category Details</th>
                    <th>Topics Covered</th>
                    
                  </tr></thead>
                  <tbody>
				  <?php foreach($training_get as $training) { ?>
                     <tr>
					 <td></td>
                    <td><?php echo $training->ss_training_name_block;  ?></td>
                    <td><?php echo $training->ss_training_name;  ?></td>
                    <td><?php echo $training->ss_training_gender;  ?></td>
                    <td><?php echo $training->ss_training_category_details;  ?></td>
                    <td><?php echo $training->ss_training_topic_covers;  ?></td>
                    
					</tr>
				  <?php  } ?>
                  </tbody>
                </table>
              </div>
				<?php  } ?>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(){


        var textarea = document.getElementById("code1");

        // Wait until animation finished render container
        setTimeout(function(){

            CodeMirror.fromTextArea(textarea, {
                lineNumbers: true,
                matchBrackets: true,
                styleActiveLine: true
            });
        }, 500);

    });
</script>