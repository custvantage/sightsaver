<div class="row wrapper border-bottom white-bg page-heading ng-scope">
  <div class="col-lg-10">
    <h2><?php echo @strtoupper($this->session->userdata['userinfo']['partner_name']); ?></h2>
  </div>
</div>

<div class="wrapper wrapper-content animated fadeIn ng-scope" ng-init="init()">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox">
        <div class="ibox-title">
          <h5>Social Inclusion / SI SHG | PPG</h5>
        </div>
        <div class="ibox-content">
          <?php echo form_open('filter_shg_si');?>
          <div class="row flex-element center-both">
            <div class="well well-sm main-entry-box">
              <div class="col-md-3 text-right">
                <label>Month:</label> 
              </div>
              <div class="col-md-5">
			  <?php
			                     if(!isset($_SESSION["month_date"])) {
								 $server_date = server_date_time(); 
							     date('m-Y',strtotime($server_date));
							     $_SESSION["month_date"] = date('m-Y',strtotime($server_date));
								 }
								  ?>
               <input type="text" name="month_from" value="<?php echo $_SESSION["month_date"]; ?>" id="month_from" class="form-control datepicker" onchange="date_fill()">
			   
			   <span class="text-danger ng-binding ng-hide" aria-hidden="true"><?php echo form_error('month_data');?>
				<?php echo form_error('month_from');?>
			   </span>
              </div>
              <div class="col-md-4">
			  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
			  <input type="hidden" name="month_data"/>
                <button class="btn btn-sm btn-primary btn-block" type="submit">
                Get Data
              </button>
              </div>
            </div>
          </div>
          <?php echo form_close();?>
          

          <div class="row " ng-show="hasData" aria-hidden="false" style="">
            <div class="col-md-12">
			<?php echo form_open('create_shg_si');?>
              <div class="panel panel-primary">
                <div class="panel-heading">
                  SHG Entry
                </div>
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Name</label> <input name="name" type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.name" aria-invalid="false">
						<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.name" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Village</label> <input name="village" type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.village" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.village" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Block</label> <input name="block" type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.block" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.block" aria-hidden="true"></p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>SHG / PPG</label>
                        <select name="shg_type" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.shg_or_ppg" aria-invalid="false"><option value="? undefined:undefined ?"></option>
                          <option value="SHG">
                            SHG
                          </option>
                          <option value="PPG">
                            PPG
                          </option>
                        </select>
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.shg_or_ppg" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Date of formation</label>
                        <p class="">
                          <input type="date" name="date_formate" class="form-control">

                        </p>
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.formation_date" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Details of financial institution linked to</label>
						<input name="details" type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.financial_institution_details" aria-invalid="false">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Male members</label> <input name="male" type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.total_members_male" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.total_members_male" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Female members</label> <input name="female" type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.total_members_female" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.total_members_female" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>PWD members</label> <input name="pwd_numver" type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.pwd_members" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.pwd_members" aria-hidden="true"></p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Formed under NRLM</label>
                        <select name="form_under_nrlm" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.formed_under_nrlm" aria-invalid="false"><option value="? undefined:undefined ?"></option>
                          <option value="true">Yes</option>
                          <option value="false">No</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Engaged in Livelihood activity</label>
                        <select name="engaged" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.engaged_livelihood" aria-invalid="false"><option value="? undefined:undefined ?"></option>
                          <option value="true">Yes</option>
                          <option value="false">No</option> 
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Trained on SHG Operations &amp; management</label>
                        <select name="trained" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.trained" aria-invalid="false"><option value="? undefined:undefined ?"></option>
                          <option value="true">Yes</option>
                          <option value="false">No</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Bank account?</label>
                        <select name="bank_account" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.bank_account" aria-invalid="false"><option value="? undefined:undefined ?"></option>
                          <option value="true">Yes</option>
                          <option value="false">No</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Regular savings?</label>
                        <select name="regular_saving" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.regular_savings" aria-invalid="false"><option value="? undefined:undefined ?"></option>
                          <option value="true">Yes</option>
                          <option value="false">No</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Credit Linkage?</label>
                        <select name="credit" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.credit_linkage" aria-invalid="false"><option value="? undefined:undefined ?"></option>
                          <option value="true">Yes</option>
                          <option value="false">No</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Marketing products in local / larger markets</label>
                        <select name="marketing" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.marketing_products" aria-invalid="false"><option value="? undefined:undefined ?"></option>
                          <option value="true">Yes</option>
                          <option value="false">No</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="panel-footer">
                  <div class="flex-element  align-bw">
                    <div class="footer-text">
                      <h5>Please review your changes before you save them. Changes once saved cannot be
                reverted.</h5>
                    </div>
                    <div class="save-btn">
					<input type="hidden" name="month_data"/>
                      <button class="btn  btn-primary" type="submit" ng-click="onSubmit();">
                          Save Changes
                      </button>
                    </div>
                  </div>
                </div>
              </div>
    		  <?php echo form_close();?>
			  
			   <?php if(!empty($shg_get)){ ?>
              <div class="table-responsive">
                <table class="table table-bordered">
                  <thead>
                    <tr>
					<th>Name</th>
                    <th>Village</th>
                    <th>Block</th>
                    <th>SHG / PPG</th>
                    <th>Date of formation</th>
                   
                    <th>Details of financial institution</th>
                    <th>Male members</th>
                    <th>Female members</th>
                    <th>PWD members</th>
                    <th>Formed under NRLM</th>
                    <th>Livelihood activity?</th>
                    <th>Bank account?</th>
                    <th>Regular savings?</th>
                    <th>Credit linkage?</th>
                    <th>Trained?</th>
                    <th>Marketing products</th>
                    
                  </tr>
				  </thead>
                  <tbody>
				  <?php foreach($shg_get as $shg_get1) { ?>
                   <tr>
				   <td><?php echo $shg_get1->ss_ssg_name;  ?></td>
				   <td><?php echo $shg_get1->ss_ssg_village;  ?></td>
				   <td><?php echo $shg_get1->ss_ssg_block;  ?></td>
				   <td><?php echo $shg_get1->ss_ssg_ppg;  ?></td>
				   <td><?php echo $shg_get1->ss_date_formation;  ?></td>
				   
				   <td><?php echo $shg_get1->ss_details_of_financial;  ?></td>
				   <td><?php echo $shg_get1->ss_male_member;  ?></td>
				   <td><?php echo $shg_get1->ss_female_member;  ?></td>
				   <td><?php echo $shg_get1->ss_pwd_members;  ?></td>
				   <td><?php echo $shg_get1->ss_formed_unde_nrlm;  ?></td>
				   <td><?php echo $shg_get1->ss_engaged;  ?></td>
				   <td><?php echo $shg_get1->ss_trained;  ?></td>
				   <td><?php echo $shg_get1->ss_bank_account;  ?></td>
				   <td><?php echo $shg_get1->ss_reguler_saving;  ?></td>
				   <td><?php echo $shg_get1->ss_credit;  ?></td>
				   <td><?php echo $shg_get1->ss_marketing;  ?></td>
				   </tr>
				  <?php } ?>
                  </tbody>
                </table>
              </div>
			   <?php } ?>

            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(){


        var textarea = document.getElementById("code1");

        // Wait until animation finished render container
        setTimeout(function(){

            CodeMirror.fromTextArea(textarea, {
                lineNumbers: true,
                matchBrackets: true,
                styleActiveLine: true
            });
        }, 500);

    });
</script>