<div class="row wrapper border-bottom white-bg page-heading ng-scope">
  <div class="col-lg-10">
    <h2><?php echo @strtoupper($this->session->userdata['userinfo']['partner_name']); ?></h2>
  </div>
</div>

<div class="wrapper wrapper-content animated fadeIn ng-scope" ng-init="init()">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox">
        <div class="ibox-title">
          <h5>Social Inclusion / SI IEC Activity</h5>
        </div>
        <div class="ibox-content">
          <?php echo form_open('filter_iec_si');?>
          <div class="row flex-element center-both">
            <div class="well well-sm main-entry-box">
              <div class="col-md-3 text-right">
                <label>Month:</label> 
              </div>
              <div class="col-md-5">
			   <?php
			                     if(!isset($_SESSION["month_date"])) {
								 $server_date = server_date_time(); 
							     date('m-Y',strtotime($server_date));
							     $_SESSION["month_date"] = date('m-Y',strtotime($server_date));
								 }
								  ?>
               <input type="text" name="month_from" id="month_from" value="<?php echo $_SESSION["month_date"]; ?>" class="form-control datepicker" onchange="date_fill()">
			   <span class="text-danger ng-binding ng-hide" aria-hidden="true">
			    <?php echo form_error('month_data');?>
				<?php echo form_error('month_from');?>
			   </span>
              </div>
              <div class="col-md-4">
			 
                <button class="btn btn-sm btn-primary btn-block" type="submit">
                Get Data
              </button>
              </div>
            </div>
          </div>
         <?php echo form_close(); ?>
          

          <div class="row " ng-show="hasData" aria-hidden="false" style="">
            <div class="col-md-12">
			<?php echo form_open('create_iec_si');?>
              <div class="panel panel-primary">
                <div class="panel-heading">
                  IEC Entry
                </div>
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Block name</label> <input name="block_name" type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.block_name" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.block_name" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Posters distributed</label> <input name="poster" type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.posters" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.posters" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Booklet / Leaflet distributed</label> <input name="booklet" type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.booklet_leaflet" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.booklet_leaflet" aria-hidden="true"></p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Pamplets distributed</label> <input name="pamplet" type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.pamphlets" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.pamphlets" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Wall writings</label> <input name="wall_writings" type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.wall_writings" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.wall_writings" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Others</label> <input name="others" type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.others" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.others" aria-hidden="true"></p>
                    </div>
                  </div>
                </div>
                <div class="panel-footer">
                  <div class="flex-element  align-bw">
                    <div class="footer-text">
                      <h5>Please review your changes before you save them. Changes once saved cannot be
                reverted.</h5>
                    </div>
                    <div class="save-btn">
					 <input type="hidden" name="month_data"/>
                      <button class="btn  btn-primary" type="submit" ng-click="onSubmit();">
                          Save Changes
                      </button>
                    </div>
                  </div>
                </div>
              </div>
			  <?php echo form_close();?>
			  
			  <?php if(!empty($iec_get)){ ?>
              <div class="table-responsive">
                <table class="table table-bordered">
                  <thead>
                    <tr><th>Block Name</th>
                    <th>Posters</th>
                    <th>Booklet / Leaflet</th>
                    <th>Pamphlets</th>
                    <th>Wall writings</th>
                    <th>Others</th>

                  </tr></thead>
                  <tbody>
				  <?php foreach($iec_get as $iec_get1) { ?>
                    <tr>
					 <td><?php echo $iec_get1->ss_iec_block_name;  ?></td>
					 <td><?php echo $iec_get1->ss_iec_posters;  ?></td>
					 <td><?php echo $iec_get1->ss_iec_booklet;  ?></td>
					 <td><?php echo $iec_get1->ss_iec_pumplet;  ?></td>
					 <td><?php echo $iec_get1->ss_iec_wall_writting;  ?></td>
					 <td><?php echo $iec_get1->ss_iec_others;  ?></td>
					</tr>
				  <?php  } ?>
                </table>
              </div>
			  <?php } ?>

            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(){


        var textarea = document.getElementById("code1");

        // Wait until animation finished render container
        setTimeout(function(){

            CodeMirror.fromTextArea(textarea, {
                lineNumbers: true,
                matchBrackets: true,
                styleActiveLine: true
            });
        }, 500);

    });
</script>