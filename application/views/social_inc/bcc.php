<div class="row wrapper border-bottom white-bg page-heading ng-scope">
  <div class="col-lg-10">
    <h2><?php echo @strtoupper($this->session->userdata['userinfo']['partner_name']); ?></h2>
  </div>
</div>

<div class="wrapper wrapper-content animated fadeIn ng-scope" ng-init="init()">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox">
        <div class="ibox-title">
          <h5>Social Inclusion / SI BCC Activity</h5>
        </div>
        <div class="ibox-content">
        <?php echo form_open('filter_bcc_si');?>
          <div class="row flex-element center-both">
            <div class="well well-sm main-entry-box">
              <div class="col-md-3 text-right">
                <label>Month:</label> 
              </div>
              <div class="col-md-5">
			  <?php
			                     if(!isset($_SESSION["month_date"])) {
								 $server_date = server_date_time(); 
							     date('m-Y',strtotime($server_date));
							     $_SESSION["month_date"] = date('m-Y',strtotime($server_date));
								 }
								 ?>
               <input type="text" name="month_from" id="month_from" value="<?php echo $_SESSION["month_date"]; ?>" class="form-control datepicker" onchange="date_fill()">
			   <span class="text-danger ng-binding ng-hide" aria-hidden="true">
			    <?php echo form_error('month_data');?>
				<?php echo form_error('month_from');?>
			   </span>
              </div>
              <div class="col-md-4">
			 
                <button class="btn btn-sm btn-primary btn-block" type="submit">
                Get Data
              </button>
              </div>
            </div>
          </div>
         <?php echo form_close(); ?>
          

          <div class="row " ng-show="hasData" aria-hidden="false" style="">
            <div class="col-md-12">
			<?php echo form_open('create_bcc_si');?>
              <div class="panel panel-primary">
                <div class="panel-heading">
                  BCC Entry
                </div>
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Block name</label> <input name="block_name" type="text" class="form-control ng-pristine ng-valid ng-empty ng-touched" ng-model="form.block_name" aria-invalid="false" style="">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.block_name" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Activity Type</label>
                        <select name="activity_type" class="form-control ng-pristine ng-valid ng-empty ng-touched" ng-model="form.activity_type" aria-invalid="false" style=""><option value="? undefined:undefined ?"></option>
                          <!-- ngRepeat: l in activity_types --><option ng-repeat="l in activity_types" value="Community Level Meeting" class="ng-binding ng-scope">Community Level Meeting</option><!-- end ngRepeat: l in activity_types --><option ng-repeat="l in activity_types" value="Sensitisation Workshop / Meeting" class="ng-binding ng-scope">Sensitisation Workshop / Meeting</option><!-- end ngRepeat: l in activity_types --><option ng-repeat="l in activity_types" value="Other" class="ng-binding ng-scope">Other</option><!-- end ngRepeat: l in activity_types -->
                        </select>
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.activity_type" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Who was sensitised?</label> <input name="sensitised" type="text" class="form-control ng-pristine ng-valid ng-empty ng-touched" ng-model="form.who_sensitised" aria-invalid="false" style="">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.who_sensitised" aria-hidden="true"></p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Issues covered</label> <input name="ss_issued" type="text" class="form-control ng-pristine ng-valid ng-empty ng-touched" ng-model="form.issues_covered" aria-invalid="false" style="">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.issues_covered" aria-hidden="true"></p>
                    </div>
                  </div>
                </div>
                <div class="panel-footer">
                  <div class="flex-element  align-bw">
                    <div class="footer-text">
                      <h5>Please review your changes before you save them. Changes once saved cannot be
                reverted.</h5>
                    </div>
                    <div class="save-btn">
					<input type="hidden" name="month_data"/>
                      <button class="btn  btn-primary" type="submit" ng-click="onSubmit();">
                          Save Changes
                      </button>
                    </div>
                  </div>
                </div>
              </div>
			  <?php echo form_close();?>
			  <?php if(!empty($bcc_get)){ ?>
              <div class="table-responsive">
                <table class="table table-bordered">
                  <thead>
                    <tr><th>Block Name</th>
                    <th>Activity Type</th>
                    <th>Who was sensitised?</th>
                    <th>Issues covered</th>
                   </tr></thead>
                  <tbody>
				  <?php foreach($bcc_get as $bcc_get1) { ?>
                   <tr>
				  <td><?php echo $bcc_get1->ss_block_name;  ?></td>
				  <td><?php echo $bcc_get1->ss_activity_type;  ?></td>
				  <td><?php echo $bcc_get1->ss_sensitised;  ?></td>
				  <td><?php echo $bcc_get1->ss_issued;  ?></td>
				   </tr>
				  <?php } ?>
                  </tbody>
                </table>
              </div>
			  <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(){


        var textarea = document.getElementById("code1");

        // Wait until animation finished render container
        setTimeout(function(){

            CodeMirror.fromTextArea(textarea, {
                lineNumbers: true,
                matchBrackets: true,
                styleActiveLine: true
            });
        }, 500);

    });
</script>