<div class="row wrapper border-bottom white-bg page-heading ng-scope">
  <div class="col-lg-10">
    <h2><?php echo @strtoupper($this->session->userdata['userinfo']['partner_name']); ?></h2>
  </div>
</div>

<div class="wrapper wrapper-content animated fadeIn ng-scope" ng-init="init()">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox">
        <div class="ibox-title">
          <h5>Social Inclusion / SI BPO | DPO</h5>
        </div>
        <div class="ibox-content">
         <?php echo form_open('filter_bpo_si');?>
          <div class="row flex-element center-both">
            <div class="well well-sm main-entry-box">
              <div class="col-md-3 text-right">
                <label>Month:</label> 
              </div>
              <div class="col-md-5">
			                     <?php
			                     if(!isset($_SESSION["month_date"])) {
								 $server_date = server_date_time(); 
							     date('m-Y',strtotime($server_date));
							     $_SESSION["month_date"] = date('m-Y',strtotime($server_date));
								 }
								 ?>
               <input type="text" name="month_from" id="month_from" value="<?php echo $_SESSION["month_date"]; ?>" class="form-control datepicker" onchange="date_fill()">
			   <span class="text-danger ng-binding ng-hide" aria-hidden="true"><?php echo form_error('month_data');?>
				<?php echo form_error('month_from');?>
			   </span>
              </div>
              <div class="col-md-4">
			  <input type="hidden" name="month_data"/>
                <button class="btn btn-sm btn-primary btn-block" type="submit">
                Get Data
              </button>
              </div>
            </div>
          </div>
          <?php echo form_close();?>
          

          <div class="row " ng-show="hasData" aria-hidden="false" style="">
            <div class="col-md-12">
			<?php echo form_open('create_bpo_si');?>
              <div class="panel panel-primary">
                <div class="panel-heading">
                  SHG Entry
                </div>
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Name</label> <input name="name" type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.name" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.name" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Village</label> <input name="village" type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.village" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.village" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Block</label> <input name="block" type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.block" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.block" aria-hidden="true"></p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Registered under societies act?</label>
                        <select name="register" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.registered_societies_act" aria-invalid="false"><option value="? undefined:undefined ?"></option>
                          <option value="true">Yes</option>
                          <option value="false">No</option>
                        </select>
                        <p class="text-danger ng-binding ng-hide" ng-show="errors.registered_societies_act" aria-hidden="true"></p>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Level of BPO / DPO</label>
                        <select name="level" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.level" aria-invalid="false"><option value="? undefined:undefined ?"></option>
                          <option value="District">District</option>
                          <option value="Block">Block</option>
                        </select>
                        <p class="text-danger ng-binding ng-hide" ng-show="errors.level" aria-hidden="true"></p>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Trained on disability laws, rights &amp; entitlement</label>
                        <select name="trained" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.trained_disability_laws" aria-invalid="false"><option value="? undefined:undefined ?"></option>
                          <option value="true">Yes</option>
                          <option value="false">No</option>
                        </select>
                        <p class="text-danger ng-binding ng-hide" ng-show="errors.trained_disability_laws" aria-hidden="true"></p>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Male members</label> <input name="male" type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.total_members_male" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.total_members_male" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Female members</label> <input name="female" type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.total_members_female" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.total_members_female" aria-hidden="true"></p>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Number of members trained in conducting access audit</label>
                        <input name="access_audit" type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.members_trained_audit" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.members_trained_audit" aria-hidden="true"></p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Advocacy agenda &amp; plans for one year?</label>
                        <select name="advocacy" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.advocacy_agenda" aria-invalid="false"><option value="? undefined:undefined ?"></option>
                          <option value="true">Yes</option>
                          <option value="false">No</option>
                        </select>
                        <p class="text-danger ng-binding ng-hide" ng-show="errors.advocacy_agenda" aria-hidden="true"></p>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Attended training on advocacy or capacity building?</label>
                        <select name="attended" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.training_advocacy_capacity" aria-invalid="false"><option value="? undefined:undefined ?"></option>
                          <option value="true">Yes</option>
                          <option value="false">No</option>
                        </select>
                        <p class="text-danger ng-binding ng-hide" ng-show="errors.training_advocacy_capacity" aria-hidden="true"></p>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Please specify any other training attended</label>
                        <input name="specify" type="text" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.other_training_attended" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.other_training_attended" aria-hidden="true"></p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Number of meetings conducted this quarter</label>
                        <input name="meetings" type="number" class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.meetings_in_quarter" aria-invalid="false">
                      </div>
                      <p class="text-danger ng-binding ng-hide" ng-show="errors.meetings_in_quarter" aria-hidden="true"></p>
                    </div>
                  </div>

                </div>
                <div class="panel-footer">
                  <div class="flex-element  align-bw">
                    <div class="footer-text">
                      <h5>Please review your changes before you save them. Changes once saved cannot be
                       reverted.</h5>
                    </div>
                    <div class="save-btn">
					 <input type="hidden" name="month_data"/>
                      <button class="btn  btn-primary" type="submit" ng-click="onSubmit();">
                          Save Changes
                      </button>
                    </div>
                  </div>
                </div>
              </div>
			  <?php echo form_close();?>
			  
			   <?php
               
			   if(!empty($bpo_get)){ ?>
              <div class="table-responsive">
                <table class="table table-bordered">
                  <thead>
                    <tr><th>Name</th>
                    <th>Village</th>
                    <th>Block</th>
                    <th>Societies act registered</th>
                    <th>Level</th>
                    <th>Other training attended</th>
                    <th>Meetings in quarter</th>
                    <th>Male members</th>
                    <th>Female members</th>
                    <th>Members trained in conducting access audit</th>
                    <th>Trained in disability laws</th>
                    <th>Advocacy agenda for year</th>
                    <th>Attended training on advocacy</th>
                   
                  </tr></thead>
                  <tbody>
				   <?php foreach($bpo_get as $bpo_get1) { ?>
                   <tr>
				   <td><?php echo $bpo_get1->ss_name;  ?></td>
				   <td><?php echo $bpo_get1->ss_village;  ?></td>
				   <td><?php echo $bpo_get1->ss_block;  ?></td>
				   <td><?php echo $bpo_get1->ss_register;  ?></td>
				   <td><?php echo $bpo_get1->ss_level;  ?></td>
				   <td><?php echo $bpo_get1->ss_trained;  ?></td>
				   <td><?php echo $bpo_get1->ss_male;  ?></td>
				   <td><?php echo $bpo_get1->ss_female;  ?></td>
				   <td><?php echo $bpo_get1->ss_access_audit;  ?></td>
				   <td><?php echo $bpo_get1->ss_advocacy;  ?></td>
				   <td><?php echo $bpo_get1->ss_attend;  ?></td>
				   <td><?php echo $bpo_get1->ss_specify;  ?></td>
				   <td><?php echo $bpo_get1->ss_no_meeting;  ?></td>
				   </tr>
				   <?php } ?>
                  </tbody>
                </table>
              </div>
			   <?php } ?>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(){


        var textarea = document.getElementById("code1");

        // Wait until animation finished render container
        setTimeout(function(){

            CodeMirror.fromTextArea(textarea, {
                lineNumbers: true,
                matchBrackets: true,
                styleActiveLine: true
            });
        }, 500);

    });
</script>