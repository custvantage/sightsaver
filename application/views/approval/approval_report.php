<div class="row wrapper border-bottom white-bg page-heading ng-scope">
  <div class="col-lg-10">
    <h2>MPR Status</h2>
  </div>
</div>
<div class="wrapper wrapper-content animated fadeIn ng-scope" ng-init="init()">

  <div class="row">
    <div class="col-md-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
        <!--  <h5>MPR Status</h5>  -->
        </div>
        <div class="ibox-content">
			<?php echo form_open();?>
			<div class="row flex-element center-both">
				<div class="well well-sm main-entry-box">             
				  <div class="col-md-12">				
				   <input type="text" name="month_from" class="form-control datepicker" placeholder="Select Month" value="<?php $server_date = server_date_time(); echo date('m-Y',strtotime($server_date));?>" onchange="filter_data(this.value)">			   
				  </div>              
				</div>
			</div>
			<?php echo form_close();?>
          <div class="table-responsive">
          <table class="table" id="table_data_dashboard">
            <thead>
				<tr>
				<th>State</th>
				<th>District</th>
				<th>Partner</th>
				<th>Project</th>
				<th width="250">Status</th>
				<th width="200">Last Updated On</th>				
				</tr>
				</thead>
				<!--<tfoot>
				<tr>
				<th>State</th>
				<th>District</th>
				<th>Partner</th>
				<th>Project</th>
				<th width="250">Status</th>
				<th width="200">Last Updated On</th>				
				</tr>
				</tfoot>  -->         
            <tbody id="response">
			<?php if(!empty($summary_data)){ foreach($summary_data as $sdata){ ?>
            <tr ng-repeat="item in items track by item.id" class="ng-scope">
              <td class="ng-binding"><?php echo $sdata->ss_states_name;?></td>
              <td class="ng-binding"><?php echo $sdata->ss_district_name;?></td>
              <td class="ng-binding"><?php echo $sdata->ss_partners_name;?></td>
              <td class=""><?php if(strpos($sdata->ss_mpr_project_name, 'rural')) { ?>
			  <a href="#">REH</a>
			  <?php } elseif(strpos($sdata->ss_mpr_project_name, 'urban')){?>
			  <a href="#">UEH</a>
			  <?php } elseif(strpos($sdata->ss_mpr_project_name, 'Social')){?>
			  <a href="#">SOC</a>
			  <?php } elseif(strpos($sdata->ss_mpr_project_name, 'Inclusive')){?>
			  <a href="#">EDU</a><?php }?>
				</td>
              <td class="ng-binding"><?php if($sdata->status == 1){ echo 'Inprogress';}
			  else if($sdata->status == 2){ echo 'Submitted';}
			  else if($sdata->status == 3){ echo 'Approved';}
			  else if($sdata->status == 4){ echo 'Rejected';}
			  else if($sdata->status == 5){ echo 'Closed';}
			  ?></td>
              <td class="ng-binding"><?php echo $sdata->last_modified;?></td>			  
            </tr>
			<?php } }else{?>
			<tr>
				<label id="no_data">No Data Found</label>
			</tr>
			<?php }?>
            </tbody>
          </table>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>
