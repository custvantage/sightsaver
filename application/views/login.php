<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<body class="fixed-sidebar">

    <div class="login-main ng-scope" ng-init="init();">
        <div class="col-md-6 col-sm-6 hidden-xs image-block ">
            <div class="bgimage-caption text-center">
                <h2 class="font-bold ng-binding"></h2>
                <h3>If you do not have a login,</h3>
                <p>please contact your system administrator who will help you create an account.</p>
                <hr>
                <p class="text-muted text-center">
                    <button class="btn btn-default btn-sm">Contact Mr. Administrator to get started!</button>
                </p>
            </div>
        </div>

        <div class="col-md-6 col-sm-6">
            <div class="form-block">
                <div class="form-block-inner">
<?php  echo form_open('login', array( 'id' => 'registrationform', 'class' => 'm-t ng-valid-email ng-dirty ng-valid-parse ng-valid ng-valid-required', 'role' => 'form','style' => ''));  ?>
                    
                        <div class="login-block">
                            <a href="index.html"><img src="<?php echo PUBLIC_URL; ?>img/logo-dark.png" alt=""></a>
                            <h2 class="margin-bottom-30">Login To Your Account</h2>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon rounded-left"><i class="fa fa-user" aria-hidden="true"></i></span>
                                    <input type="email" name="email" id="email1" value="<?php echo set_value('email')?>" class="form-control " placeholder="Username" >
									<span id="email1_error" style="color:red"></span>
                                </div>

                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon rounded-left"><i class="fa fa-lock" aria-hidden="true"></i></span>
                                    <input type="password" name="passward" id="passward1" value="<?php echo set_value('passward')?>" class="form-control " placeholder="Password" >  <span class="input-group-addon rounded-left padding_right_9px"> <a href="javascript:void(0)" onclick="change_passwordtype();"> <i class="fa fa-eye" aria-hidden="true"></i> </a> </span>
									<span id="passward1_error" style="color:red"></span>
                                </div>
                            </div>
                           <?php if(isset($_SESSION['success']))
							{ ?>
							<div class="alert alert-success">
							<strong></strong> <?php echo $_SESSION['success'] ; ?>
							</div>
							<?php } ?>
							<button class="btn btn-primary btn-block btn-lg m-b" id="submit1" type="button">
							<strong>Login</strong></button>
                            <a href="#">
                              <!--  <small>Forgot password?</small>  -->
                            </a>

                            <p class="text-muted text-center">
                                <small>Do not have an account?</small>
                            </p>
                            <div class="copyright-block text-center">
                                <p>© Copyright 2016-2017 <a href="#">Sightsavers</a>. All Rights Reserved.</p>
                            </div>
                        </div>
                    <?php echo form_close();  ?>
                </div>
            </div>
        </div>
    </div>
	
<script src="<?php echo PUBLIC_URL; ?>js/jquery-2.1.1.js"></script>
<script>
 			$("#submit1").click (function()
			{
				var flag = true;
				var name_pattern = /^[a-zA-Z][a-zA-Z)(.\' ]+$/;
				var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				//var num_pattern1 = /^[0-9][0-9)(.\']+$/;
				var space=/^[a-zA-Z0-9]+$/;
				var num_pattern = /^(?!.*-.*-.*-)(?=(?:\d{8,10}$)|(?:(?=.{0,9}$)[^-]*-[^-]*$)|(?:(?=.{10,12}$)[^-]*-[^-]*-[^-]*$)  )[\d-]+$/;

if($('#email1').val()=="")
	{
		//alert('email'); 
		$('#email1_error').html('Please enter your  email address');
		$('#email1').focus();
		flag = 'false';
		return false;
	}
	 else if(!filter.test($('#email1').val()))
	{
		$('#email1_error').html('Please enter your valid email address');
		$('#email1_id').focus();
		flag = 'false';
		return false;
	} 
	else
	{
		$('#email1_error').html('');
	} 
	
							
if($('#passward1').val() == "")
		{
			$('#passward1_error').html('Please enter you passward');
			$('#passward1').focus();
			flag = 'false';
			return false;
		}

		else
		{
			$('#passward1_error').html('');
		}
			

	
 if(flag=="false")
	{
		return false;
	}
	else
	{
		$('#registrationform').submit();
	}
});
function change_passwordtype()
{
	if($("#passward1").attr("type") == "password")
	{
		$("#passward1").attr("type","text");		
	}else{
		$("#passward1").attr("type","password");
	}
	$("#passward1").focus();
}
</script>	
	
	
	