
<div class="row wrapper border-bottom white-bg page-heading ng-scope">
  <div class="col-lg-10">
    <h2>Partners</h2>
  </div>
</div>

<div class="wrapper wrapper-content animated fadeIn ng-scope" ng-init="init()">
<?php if(isset($_SESSION['success']))
{ ?>
<div class="alert alert-success">
  <strong></strong> <?php echo $_SESSION['success'] ; ?>
</div>
<?php } ?>
<?php  echo form_open('partner');  ?>
  <div class="row">
    <div class="col-md-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Add Partner</h5>
        </div>
        <div class="ibox-content ">
          <div class="row center-block mb-20">
            <div class="col-md-4">
              <label>Partner Name</label>
              <input type="text" ng-model="form.name" name="name" <?php   if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==5 || $this->session->userdata('userinfo')['default_role']==7 )  { echo "disabled"; } ?>  id="name1" value="<?php echo set_value('name')?>" class="form-control ng-pristine ng-valid ng-empty ng-touched" placeholder="Enter Partner Name" width="100%" aria-invalid="false" style="">
			  <span id="name1_error" style="color:red"><?php echo form_error('name');?></span>
            </div>
            <div class="col-md-4">
              <label>District</label>
			 
              <select name="districts" id="districts1" <?php   if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==5 || $this->session->userdata('userinfo')['default_role']==7 )  { echo "disabled"; } ?>  value="<?php echo set_value('districts')?>" class="form-control">
                <option value="">-select District-</option>
				<?php
                 if(isset($district) && $district!=null)
				 {
				foreach($district as $district1)
				{ 
				?> 
                <option value="<?php echo $district1->ss_district_id; ?>-<?php echo $district1->ss_district_name; ?>"><?php echo $district1->ss_district_name; ?>
				</option>
				
				 <?php }  ?>
				 
			<?php } ?>
              </select>
			  <span id="districts1_error" style="color:red"><?php echo form_error('districts');?></span>
            </div>
			<div class="col-md-4">
              <label>District</label>
			 
              <input type="text" disabled id="district_view" class="form-control">
            </div>
          </div>

          <div class="row center-block">
            <div class="col-md-12">
              <label>Program</label>
              <div class="well well-sm clearfix">
                <div class="col-md-2">
                  <div class="checkbox checkbox-primary">
                    <label><input class="styled" name="program1" <?php   if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==5 || $this->session->userdata('userinfo')['default_role']==7 )  { echo "disabled"; } ?>  id="program11" type="checkbox" value="1" checked="">
                      <span> Rural Eye Health</span>
                    </label>
                  </div>

                </div>

                <div class="col-md-2">
                  <div class="checkbox checkbox-primary">
                    <label><input class="styled" name="program2" <?php   if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==5 || $this->session->userdata('userinfo')['default_role']==7 )  { echo "disabled"; } ?>  id="program12" value="1" type="checkbox" >
                      <span> Urban Eye Health</span>
                    </label>
                  </div>

                </div>

                <div class="col-md-2">
                  <div class="checkbox checkbox-primary">
                    <label><input class="styled" name="program3" <?php   if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==5 || $this->session->userdata('userinfo')['default_role']==7 )  { echo "disabled"; } ?>  id="program13" value="1" type="checkbox">
                      <span> Inclusive Education</span>
                    </label>
                  </div>

                </div>
                <div class="col-md-2">
                  <div class="checkbox checkbox-primary">
                    <label><input class="styled" name="program4" <?php   if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==5 || $this->session->userdata('userinfo')['default_role']==7 )  { echo "disabled"; } ?>  id="program14" value="1" type="checkbox">
                      <span> Social Inclusion</span>
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>
		  <span id="program11_error" style="color:red"></span>
          <hr>
          <div class="text-center">
            <button class="btn btn-sm btn-primary" <?php   if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==5 || $this->session->userdata('userinfo')['default_role']==7 )  { echo "disabled"; } ?>  id="submit_regi112222" type="submit" ng-click="addItem(form);">
              <strong>Add Partner</strong>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php echo form_close();  ?>
  <div class="row">
    <div class="col-md-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Partners list</h5>
        </div>
        <div class="ibox-content">
          <div class="table-responsive">
          <table id="table_data" class="table table-bordered">
            <thead>
            <tr>
              <th>Id</th>
              <th>Partner Name</th>
              <th>District</th>
              <th>Program</th>
            </tr>
            </thead>
            <tbody>
			<?php 
			//var_dump($partner_data1); die;
             if(isset($partner_data1) && $partner_data1!=null)
			 {
			 foreach($partner_data1 as $partner_data12) {  ?> 
            <tr ng-repeat="item in partners track by item.id" class="ng-scope">
              <td class="ng-binding"><?php echo  $partner_data12->ss_partners_id; ?></td>
			  <td class="ng-binding"><?php echo  $partner_data12->ss_partners_name; ?></td>
              <td class="ng-binding"><?php echo  $partner_data12->ss_district_name; ?></td>
              <td class="ng-binding">
			  <?php $a="";
			  if($partner_data12->ss_partners_project_reh==1)
			  {
				  $a .= 'Rural Eye Health'.",";
  			  }
			  if($partner_data12->ss_partners_project_ueh==1)
			  {
				   $a .= 'Urban Eye Health'.",";
			  }
			  if($partner_data12->ss_partners_project_si==1)
			  {
				   $a .= 'Inclusive Education'.",";
			  }
			  if($partner_data12->ss_partners_project_ie==1)
			  {
				   $a .= 'Social Inclusion'.",";
			  }
			   echo substr($a,0,-1);

				  ?></td>
            </tr>
			 <?php    }  }   ?>
            </tbody>
          </table>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>



<script src="<?php echo PUBLIC_URL; ?>js/jquery-2.1.1.js"></script>
 <script>
 			$("#submit_regi112222").click (function()
			{
				var flag = true;
				var name_pattern = /^[a-zA-Z0-9$() -]*$/;
				//var name_pattern = /^[a-zA-Z][a-zA-Z)(.\' ]+$/;
				//var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				//var num_pattern1 = /^[0-9][0-9)(.\']+$/;
				//var space=/^[a-zA-Z0-9]+$/;
				//var num_pattern = /^(?!.*-.*-.*-)(?=(?:\d{8,10}$)|(?:(?=.{0,9}$)[^-]*-[^-]*$)|(?:(?=.{10,12}$)[^-]*-[^-]*-[^-]*$)  )[\d-]+$/;
				
				if($('#name1').val() == "")
				{
					$('#name1_error').html('Please Enter your Partner Name');
					$('#name1').focus();
					flag = 'false';
					return false;
				}
				else if(!name_pattern.test($('#name1').val()))
				{
			$('#name1_error').html('Special character and numbers are not allowed');
			$('#name1').focus();
			flag = 'false';
			return false;
			}
				else
				{
					$('#name1_error').html('');
				}

		      if($('#districts1').val() == "")
				{
					$('#districts1_error').html('Please Enter your District');
					$('#districts1').focus();
					flag = 'false';
					return false;
				}
			  else
				{
					$('#districts1_error').html('');
				}		
	 if($('#program11').val() == "0")
				{
					$('#program11_error').html('Please checked at list one');
					$('#program11').focus();
					flag = 'false';
					return false;
				}
			  else
				{
					$('#program11_error').html('');
				}			
				

    if(flag=="false")
	{
		return false;
	}
	else
	{
		$('#registrationform').submit();
	}
				
});

</script>	
 
