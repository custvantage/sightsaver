
<div class="row wrapper border-bottom white-bg page-heading ng-scope">
  <div class="col-lg-10">
    <h2>Users</h2>
  </div>
</div>

<div class="wrapper wrapper-content animated fadeIn ng-scope" ng-init="init()">
<?php if(isset($_SESSION['success']))
{ ?>
<div class="alert alert-success">
  <strong></strong> <?php echo $_SESSION['success'] ; ?>
</div>
<?php } ?>

<?php  echo form_open('user');  ?>
  <div class="row">
    <div class="col-md-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Add User</h5>
        </div>
        <div class="ibox-content">
          <div class="row center-block mb-15">
            <div class="col-md-3">
              <label>First Name</label>
              <input type="text" ng-model="form.name" name="firstname" id="firstname1" <?php   if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==5 || $this->session->userdata('userinfo')['default_role']==7 )  { echo "disabled"; } ?>  value="<?php echo set_value('firstname')?>" class="form-control ng-pristine ng-valid ng-empty ng-touched" placeholder="Enter First Name" width="100%" aria-invalid="false" style="">
			  <span id="firstname1_error" style="color:red"><?php echo form_error('firstname');?></span>
            </div>
            <div class="col-md-4">
              <label>Last Name</label>
              <input type="text" ng-model="form.username" value="<?php echo set_value('lastname')?>" name="lastname" <?php   if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==5 || $this->session->userdata('userinfo')['default_role']==7 )  { echo "disabled"; } ?>  id="lastname1" class="form-control ng-pristine ng-valid ng-empty ng-touched" placeholder="Enter Last Name" width="100%" aria-invalid="false" style="">
			  <span id="lastname1_error" style="color:red"><?php echo form_error('lastname');?></span>
            </div>
            <div class="col-md-5">
              <label>Email Id</label>
              <input type="email" ng-model="form.roles" value="<?php echo set_value('email')?>" name="email" <?php   if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==5 || $this->session->userdata('userinfo')['default_role']==7 )  { echo "disabled"; } ?>  id="email1" class="form-control ng-pristine ng-valid ng-empty ng-touched" placeholder="Enter Email Id" width="100%" aria-invalid="false" style="">
			  <span id="email1_error" style="color:red"><?php echo form_error('email');?></span>
            </div>
          </div>

          <div class="row center-block">
            <div class="col-md-5">
              <label>Role</label>
              <select name="role" id="role1" <?php   if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==5 || $this->session->userdata('userinfo')['default_role']==7 )  { echo "disabled"; } ?>  value="<?php echo set_value('role')?>" class="form-control">
                <option value="">-select Role-</option>
				<?php if(isset($role1) && $role1!=null)
				{ foreach($role1 as $role)
					{  ?>
                <option value="<?php echo $role->ss_role_id; ?>"><?php echo $role->ss_role_name; ?></option>
				<?php }}  ?>
              </select>
			  <span id="role1_error" style="color:red"><?php echo form_error('role');?></span>
            </div>
            <div id="partner_name11" class="col-md-5 bootstrap_multiselct">
              <label>Partner</label>
              <select id="partner_name1" name="partner_name[]" <?php   if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==5 || $this->session->userdata('userinfo')['default_role']==7 )  { echo "disabled"; } ?>  value="<?php echo set_value('partner_name')?>"  class="form-control" multiple="multiple">
                <option value="">-select Partner-</option>
				<?php if(isset($partner_name1) &&  $partner_name1!=null) 
				{ foreach($partner_name1 as $partner_n) { ?> 
                <option value="<?php echo $partner_n->ss_partners_id;  ?>"><?php echo $partner_n->ss_partners_name;  ?></option>
				<?php }} ?>
              </select>
			   <span id="partner_name1_error" style="color:red"><?php echo form_error('partner_name');?></span>
            </div>
            <div class="col-md-2">
              <label>Account Status</label>
              <div class="checkbox checkbox-primary">
                <label><input class="styled" name="status"  <?php   if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==5 || $this->session->userdata('userinfo')['default_role']==7 )  { echo "disabled"; } ?>  value="1" type="checkbox" checked="">
                  <span> Active</span>
                </label>
              </div>
            </div>
          </div>
          <hr>
          <div class="text-center">
            <button class="btn btn-sm btn-primary" <?php   if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==5 || $this->session->userdata('userinfo')['default_role']==7 )  { echo "disabled"; } ?>  type="submit" id="submit" ng-click="addItem(form);">
              <strong>Add User</strong>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
   <?php echo form_close();  ?>
  <div class="row">
    <div class="col-md-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Users list</h5>
        </div>
        <div class="ibox-content">
          <div class="table-responsive">
          <table id="table_data" class="table table-bordered">
            <thead>
            <tr>
              <th>Id</th>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Email Id</th>
              <th>Role</th>
              <th>Partner</th>
              <th>Account status</th>
            </tr>
			
            </thead>
            <tbody>
            <!-- ngRepeat: item in items track by item.id -->
			<?php if(isset($user_name1) && $user_name1!=null)  {
              foreach($user_name1 as $user_nam) {
 			?>
			<tr ng-repeat="item in items track by item.id" class="ng-scope">
              <td class="ng-binding"><?php echo $user_nam->ss_user_id;  ?></td>
              <td class="ng-binding"><?php echo $user_nam->ss_user_fname;  ?></td>
              <td class="ng-binding"><?php echo $user_nam->ss_user_lname;  ?></td>
              <td class="ng-binding"><?php echo $user_nam->ss_user_email_id;  ?></td>
              <td class="ng-binding"><?php echo $user_nam->ss_role_name;  ?></td>
              <td class="ng-binding"><?php echo $user_nam->ss_partners_name;  ?></td>
              <td class="ng-binding"><?php if($user_nam->ss_user_status==1){echo "Active";} else { echo "Inactive";}  ?></td>
            </tr>
			<?php }}  ?>
            </tbody>
          </table>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="<?php echo PUBLIC_URL; ?>js/jquery-2.1.1.js"></script>
<script>
 			$("#submit").click (function()
			{
				var flag = true;
				var name_pattern = /^[a-zA-Z][a-zA-Z)(.\' ]+$/;
				var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				var num_pattern1 = /^[0-9][0-9)(.\']+$/;
				var space=/^[a-zA-Z0-9]+$/;
				var num_pattern = /^(?!.*-.*-.*-)(?=(?:\d{8,10}$)|(?:(?=.{0,9}$)[^-]*-[^-]*$)|(?:(?=.{10,12}$)[^-]*-[^-]*-[^-]*$)  )[\d-]+$/;
				
							
		if($('#firstname1').val() == "")
				{
					$('#firstname1_error').html('Please enter you first name');
					$('#firstname1').focus();
					flag = 'false';
					return false;
				}
				else if(!name_pattern.test($('#firstname1').val()))
				{
			$('#firstname1_error').html('Special character and numbers are not allowed');
			$('#firstname1').focus();
			flag = 'false';
			return false;
			}
				else
				{
					$('#firstname1_error').html('');
				}
				
				if($('#lastname1').val() == "")
				{
					$('#lastname1_error').html('Please enter you last name');
					$('#lastname1').focus();
					flag = 'false';
					return false;
				}
				else if(!name_pattern.test($('#lastname1').val()))
				{
			$('#lastname1_error').html('Special character and numbers are not allowed');
			$('#lastname1').focus();
			flag = 'false';
			return false;
			}
				else
				{
					$('#lastname1_error').html('');
				}
				
if($('#email1').val()=="")
	{
		//alert('email'); 
		$('#email1_error').html('Please enter your  email address');
		$('#email1').focus();
		flag = 'false';
		return false;
	}
	 else if(!filter.test($('#email1').val()))
	{
		$('#email1_error').html('Please enter your valid email address');
		$('#email1_id').focus();
		flag = 'false';
		return false;
	} 
	else
	{
		$('#email1_error').html('');
	} 
	
	
	
	if($('#role1').val()=="")
	{
		//alert('number'); 
		$('#role1_error').html('Please enter your role');
		$('#role1').focus();
		flag = 'false';
		return false;
	}
	else
	{
		$('#role1_error').html('');
	}
				
 /* if($('#partner_name1').val()=="")
	{
		//alert('number'); 
		$('#partner_name1_error').html('Please enter your partner name');
		$('#partner_name1').focus();
		flag = 'false';
		return false;
	}
	else
	{
		$('#partner_name1_error').html('');
	}	 */			

	
 if(flag=="false")
	{
		return false;
	}
	else
	{
		$('#registrationform').submit();
	}
});
</script>	

<script>
$(function() {
	 $('#partner_name11').hide(); 
  $('#role1').change(function(){
	
	if($('#role1').val()=='6')
	{   //alert(groupVal);
		$('#partner_name11').show();
	}
	else
	{
		$('#partner_name11').hide();
	}
    
  });
 
});
</script>
