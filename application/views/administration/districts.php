

<div class="row wrapper border-bottom white-bg page-heading ng-scope">
  <div class="col-lg-10">
    <h2>Districts</h2>
  </div>
</div>

<div class="wrapper wrapper-content animated fadeIn ng-scope" ng-init="init()">
<?php if(isset($_SESSION['success']))
{ ?>
<div class="alert alert-success">
  <strong></strong> <?php echo $_SESSION['success'] ; ?>
</div>
<?php } ?>

<?php  echo form_open('districts');  ?>
  <div class="row">
    <div class="col-md-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Add district</h5>
        </div>
        <div class="ibox-content">
          <div class="row center-block">
            <div class="col-md-3">
              <input type="text" ng-model="form.name" name="district" id="district1" <?php   if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==5 || $this->session->userdata('userinfo')['default_role']==7)  { echo "disabled"; } ?> 

			  class="form-control ng-pristine ng-valid ng-empty ng-touched" placeholder="Enter district name" width="100%" aria-invalid="false" style="">
			  <span id="district1_error" style="color:red"><?php echo form_error('district');?></span>
            </div>
            <div class="col-md-3">
              <select name="state" id="state1" <?php   if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==5 || $this->session->userdata('userinfo')['default_role']==7 )  { echo "disabled"; } ?>  class="form-control">
              	<option value="">Select a state...</option>
				<?php if(isset($state_data1) && $state_data1!=null){
                  foreach($state_data1 as $state)	{ ?>
				<option value="<?php echo $state->ss_states_id; ?>"><?php echo $state->ss_states_name;  ?></option>
				<?php  }} ?>
              </select>
			  <span id="state1_error" style="color:red"><?php echo form_error('state');?></span>
            </div>
            <div class="col-md-2">
              <button class="btn btn-sm btn-primary btn-block" <?php   if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==5 || $this->session->userdata('userinfo')['default_role']==7 )  { echo "disabled"; } ?> type="submit" id="submit1" ng-click="addItem(form);">
                <strong>Add District</strong>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
 <?php echo form_close();  ?>
  <div class="row">
    <div class="col-md-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Districts list</h5>
        </div>
        <div class="ibox-content">
          <table id="table_data" class="table table-bordered">
            <thead>
            <tr>
              
              <th>District Name</th>
              <th>State Name</th>
            </tr>
            </thead>
            <tbody>
            <!-- ngRepeat: item in districts track by item.id -->
			<?php if(isset($fetch_the_district) && $fetch_the_district!=null) { 
            foreach($fetch_the_district as $fetch_the_dis)  { ?>
			<tr ng-repeat="item in districts track by item.id" class="ng-scope">
              <td class="ng-binding"><?php echo $fetch_the_dis->ss_district_name; ?></td>
              <td class="ng-binding"><?php echo $fetch_the_dis->ss_states_name; ?></td>
            </tr>
			<?php }}   ?>
            </tbody>
          </table>

        </div>
      </div>
    </div>
  </div>
</div>
<script src="<?php echo PUBLIC_URL; ?>js/jquery-2.1.1.js"></script>
<script>
 			$("#submit1").click (function()
			{
				var flag = true;
				var name_pattern = /^[a-zA-Z][a-zA-Z)(.\' ]+$/;
				//var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				//var num_pattern1 = /^[0-9][0-9)(.\']+$/;
				//var space=/^[a-zA-Z0-9]+$/;
				//var num_pattern = /^(?!.*-.*-.*-)(?=(?:\d{8,10}$)|(?:(?=.{0,9}$)[^-]*-[^-]*$)|(?:(?=.{10,12}$)[^-]*-[^-]*-[^-]*$)  )[\d-]+$/;
				
				if($('#district1').val() == "")
				{
					$('#district1_error').html('Please Enter your District Name');
					$('#district1').focus();
					flag = 'false';
					return false;
				}
				else if(!name_pattern.test($('#district1').val()))
				{
			$('#district1_error').html('Special character and numbers are not allowed');
			$('#district1').focus();
			flag = 'false';
			return false;
			}
				else
				{
					$('#district1_error').html('');
				}

		      if($('#state1').val() == "")
				{
					$('#state1_error').html('Please select your State');
					$('#state1').focus();
					flag = 'false';
					return false;
				}
			  else
				{
					$('#state1_error').html('');
				}		

   
				
});

</script>	


