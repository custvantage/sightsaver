

<div class="row wrapper border-bottom white-bg page-heading ng-scope">
  <div class="col-lg-10">
    <h2>Create a Reporting Quarter</h2>
  </div>
</div>

<div class="wrapper wrapper-content animated fadeIn ng-scope" ng-init="init()">
  <div class="row">
    <div class="col-md-12">
      <div class="ibox">
        <div class="ibox-title">
          <h5>Create a Reporting Quarter</h5>
        </div>
        <div class="ibox-content">
          <form role="form" class="ng-pristine ng-valid ng-valid-date">

            <div class="row flex-element center-both">
              <div class="well well-sm main-entry-box">
                <div class="clearfix">
                  <div class="col-md-6">
                    <label>From</label>
                    <div >
                      <input type="month" class="form-control">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <label>Year</label>
                    <select class="form-control ng-pristine ng-untouched ng-valid ng-empty" ng-model="form.year" aria-invalid="false"><option value="? undefined:undefined ?"></option>
                      <option value="2016">2016</option>
                      <option value="2017">2017</option>
                    </select>
                  </div>
                  <div class="col-md-2">
                    <label>&nbsp;</label>
                    <button class="btn btn-sm btn-primary btn-block" type="submit" ng-click="addItem(form)">
                      Save
                  </button>
                  </div>
                </div>

                <div class="form-discription">
                  When a new reporting quarter is added the PWD database from the previous quarter is copied over to the new quarter. Please
                  ensure that the PWD database of the previous quarter is closed before adding the next quarter.
                </div>
              </div>
            </div>




          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Reporting Quarters</h5>
        </div>
        <div class="ibox-content">
          <div class="table-responsive">
            <table class="table">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>From</th>
                  <th>To</th>
                  <th>Year</th>
                  <th>Status</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                <!-- ngRepeat: item in items track by item.id --><tr ng-repeat="item in items track by item.id" class="ng-scope">
                  <td class="ng-binding">1</td>
                  <td class="ng-binding">Apr, 2016</td>
                  <td class="ng-binding">Jun, 2016</td>
                  <td class="ng-binding">2016</td>
                  <td ng-show="item.is_open" aria-hidden="true" class="ng-hide">Open for updates</td>
                  <td ng-show="!item.is_open" aria-hidden="false" class="">Closed for updates</td>
                  <td ng-show="item.is_open" aria-hidden="true" class="ng-hide"><a ng-click="closeQuarter(item.id)">Close Quarter</a></td>
                  <td ng-show="!item.is_open" aria-hidden="false" class=""><a ng-click="openQuarter(item.id)">Open Quarter</a></td>
                </tr><!-- end ngRepeat: item in items track by item.id --><tr ng-repeat="item in items track by item.id" class="ng-scope">
                  <td class="ng-binding">2</td>
                  <td class="ng-binding">Jul, 2016</td>
                  <td class="ng-binding">Sep, 2016</td>
                  <td class="ng-binding">2016</td>
                  <td ng-show="item.is_open" aria-hidden="true" class="ng-hide">Open for updates</td>
                  <td ng-show="!item.is_open" aria-hidden="false" class="">Closed for updates</td>
                  <td ng-show="item.is_open" aria-hidden="true" class="ng-hide"><a ng-click="closeQuarter(item.id)">Close Quarter</a></td>
                  <td ng-show="!item.is_open" aria-hidden="false" class=""><a ng-click="openQuarter(item.id)">Open Quarter</a></td>
                </tr><!-- end ngRepeat: item in items track by item.id -->
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>

