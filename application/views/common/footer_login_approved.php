<div class="footer" >

    <div>
        <strong>Copyright</strong> sightsavers &copy; 2015-2016
    </div>
</div>
</div>
</div>

    <!-- Mainly scripts -->

	 <script src="<?php echo PUBLIC_URL; ?>js/jquery-2.1.1.js"></script>
	
    <script src="<?php echo PUBLIC_URL; ?>js/bootstrap.min.js"></script>
    <script src="<?php echo PUBLIC_URL; ?>js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo PUBLIC_URL; ?>js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo PUBLIC_URL; ?>js/inspinia.js"></script>
    <script src="<?php echo PUBLIC_URL; ?>js/plugins/pace/pace.min.js"></script>

    <script src="<?php echo PUBLIC_URL; ?>js/codemirror.js"></script>
    <script src="<?php echo PUBLIC_URL; ?>js/codescript.js"></script>
	<script src="<?php echo PUBLIC_URL; ?>js/bootstrap_datepicker.js"></script>
  	<script src="<?php echo PUBLIC_URL; ?>js/jquery.dataTables.min.js"></script>
	<script src="<?php echo PUBLIC_URL; ?>js/dataTables.bootstrap.min.js"></script>

<script>
function filter_data(month_data)
{
	//alert(month_data);
	if(month_data != "" )
	{
		var url = "<?php echo BASE_URL('approval_report_filter');?>";				 
		var csrfHash = $("input[name=csrf_test_name]").val();
		 
		$.post(url,{<?php echo $this->security->get_csrf_token_name(); ?>: csrfHash,month_from:month_data} ,function(response){
				
			var htmldata = "";
			var status1 = "";
			var prj_name = "";
			var createdby = "";
			$("input[name=csrf_test_name]").val(response.csrfHash);
			if(response.success ==1)
			{  //console.log(response); return false;
				for(var i=0;i<response.summary_data.length;i++)
				{   
				   createdby = response.summary_data[i].ss_mpr_modified_by;
				   console.log(createdby);
					status1 = response.summary_data[i].status;
					
					if(status1 == 1)
					{
						status="Inprogress";
						}
					else if(status1 == 2)
					{
						status="Submitted";
						}
					else if(status1 == 3)
					{
						status = "Approved";
						}
					else if(status1 == 4)
					{
						status = "Rejected";
						}
					else if(status1 == 5)
					{
						status = "Closed";
						}
					else {
						status = ""; 
						}
					
					if(createdby !="" && status1 == 3){
						status = "Approved by ".concat(createdby);
					}
					if(createdby !="" && status1 == 4){
						status = "Rejected by ".concat(createdby);
						 }
					if(createdby !="" && status1 == 5){
						status = "Closed by ".concat(createdby);
					}
					
					prj_name = response.summary_data[i].ss_mpr_project_name;
				//	prj_module = response.summary_data[i].ss_mpr_module_name;
					if(prj_name.indexOf("rural") >= 0)
					{ prjname = '<a href="<?php echo 'mpr_reh_redirect/';?>'+btoa(response.summary_data[i].ss_mpr_report_month)+'/'+btoa(response.summary_data[i].ss_mpr_summary_partner_id)+'">REH</a>' }
					else if(prj_name.indexOf("urban") >= 0)
					{ prjname = '<a href="<?php echo 'mpr_ueh_redirect/';?>'+btoa(response.summary_data[i].ss_mpr_report_month)+'/'+btoa(response.summary_data[i].ss_mpr_summary_partner_id)+'">UEH</a>'; }
				     else if(prj_name.indexOf("Social") >= 0)
					{ prjname = '<a href="<?php echo 'mpr_social_redirect/';?>'+btoa(response.summary_data[i].ss_mpr_report_month)+'/'+btoa(response.summary_data[i].ss_mpr_summary_partner_id)+'">SOC</a>'; }
				    else if(prj_name.indexOf("Inclusive") >= 0)
					{ prjname = '<a href="<?php echo 'mpr_education_redirect/';?>'+btoa(response.summary_data[i].ss_mpr_report_month)+'/'+btoa(response.summary_data[i].ss_mpr_summary_partner_id)+'">EDU</a>'; }
				     
				
				 htmldata += '<tr ng-repeat="item in items track by item.id" class="ng-scope"><td class="ng-binding">'+response.summary_data[i].ss_states_name+'</td><td class="ng-binding">'+response.summary_data[i].ss_district_name+'</td><td class="ng-binding">'+response.summary_data[i].ss_partners_name+'</td><td class="ng-binding">'+prjname+'</td><td class="ng-binding">'+status+'</td><td class="ng-binding">'+response.summary_data[i].last_modified+'</td></tr>';				 
				}							
				$("#response").html(htmldata);				
				
			}
			else{
				$("#response").html('<tr><label>No Data Found</label></tr>');
			}
		},"json");
	}
}
</script>
<script>
	$(document).ready(function(){
		$('.page-heading').addClass('hidden-print');
		$('hr').addClass('hidden-print');
		$('.print-btn').on('click', function(){
			
			window.print();
			
		});
	})
</script>
<script>

function partner_filter_data(month_data)
{
	
	//alert(month_data);
	if(month_data != "" )
	{
		
		var url = "<?php echo BASE_URL('partner_report_filter');?>";				 
		var csrfHash = $("input[name=csrf_test_name]").val();
		var partner_id = <?php echo $_SESSION["partner_id"]; ?>;
		 
		$.post(url,{<?php echo $this->security->get_csrf_token_name(); ?>: csrfHash,month_from:month_data,partner:partner_id} ,function(response){
				
			var htmldata = "";
			var status1 = "";
			var prj_name = "";
			$("input[name=csrf_test_name]").val(response.csrfHash);
			
			if(response.success ==1)
			{
				//alert(response.summary_data.length);
				for(var i=0;i<response.summary_data.length;i++)
				{
					status1 = response.summary_data[i].status;
					if(status1 == 1)
					{status="Inprogress";}
					else if(status1 == 2)
					{status="Submitted";}
					else if(status1 == 3)
					{status = "Approved";}
					else if(status1 == 4)
					{status = "Rejected";}
					else if(status1 == 5)
					{status = "Closed";}
					
					prj_name = response.summary_data[i].ss_mpr_project_name;
				//	prj_module = response.summary_data[i].ss_mpr_module_name;
					if(prj_name.indexOf("rural") >= 0)
					{ prjname = '<a href="<?php echo 'mpr_reh_redirect/';?>'+btoa(response.summary_data[i].ss_mpr_report_month)+'/'+btoa(response.summary_data[i].ss_mpr_summary_partner_id)+'">REH</a>' }
					else if(prj_name.indexOf("urban") >= 0)
					{ prjname = '<a href="<?php echo 'mpr_ueh_redirect/';?>'+btoa(response.summary_data[i].ss_mpr_report_month)+'/'+btoa(response.summary_data[i].ss_mpr_summary_partner_id)+'">UEH</a>'; }
				     else if(prj_name.indexOf("Social") >= 0)
					{ prjname = '<a href="<?php echo 'mpr_social_redirect/';?>'+btoa(response.summary_data[i].ss_mpr_report_month)+'/'+btoa(response.summary_data[i].ss_mpr_summary_partner_id)+'">SOC</a>'; }
				    else if(prj_name.indexOf("Inclusive") >= 0)
					{ prjname = '<a href="<?php echo 'mpr_education_redirect/';?>'+btoa(response.summary_data[i].ss_mpr_report_month)+'/'+btoa(response.summary_data[i].ss_mpr_summary_partner_id)+'">EDU</a>'; }
				     
				
				 htmldata += '<tr ng-repeat="item in items track by item.id" class="ng-scope"><td class="ng-binding">'+response.summary_data[i].ss_states_name+'</td><td class="ng-binding">'+response.summary_data[i].ss_district_name+'</td><td class="ng-binding">'+response.summary_data[i].ss_partners_name+'</td><td class="ng-binding">'+prjname+'</td><td class="ng-binding">'+status+'</td><td class="ng-binding">'+response.summary_data[i].last_modified+'</td></tr>';				 
				}							
				$("#response").html(htmldata);				
				
			}
			else{
				$("#response").html('<tr><label>No Data Found</label></tr>');
			}
		},"json");
	}
}
</script>
	<script>
		$(document).ready(function(){
			$('.table-bordered tbody tr td').each(function(){
				if($(this).children('input[type= "text"]').length >0 || $(this).children('textarea').length >0)
				{
					$(this).addClass('bg_diff');
				}
				
			});

		})
	</script>
	<script>
        $(function(){
           $('.datepicker').datepicker({
              format: "mm-yyyy",
            viewMode: "months", 
            minViewMode: "months"
            });
			$('.datepickerday').datepicker({
              format: "dd-mm-yyyy",
            viewMode: "date", 
            minViewMode: "date"
            });
        });
    </script>
	<script>
		$(document).ready(function(){
			$('#table_data').DataTable();
		});
		
	</script>
	
	<script>
	
	$('.SeeMore2').on('click', function(){
	var $this = $(this);
	$this.toggleClass('SeeMore2');
	if($(this).find($(".fa")).hasClass('fa-caret-left'))
	{
	$(this).find($(".fa")).removeClass('fa-caret-left').addClass('fa-caret-right');

	} else {
	$(this).find($(".fa")).removeClass('fa-caret-right').addClass('fa-caret-left');
	}
	});
</script>


	<script>
		$(document).ready(function() {
			$.extend( true, $.fn.dataTable.defaults, {
				"ordering": false
			} );
			$('#table_data_dashboard thead th').each( function () {
				var title = $(this).text();
				if(title == 'Last Updated On')
				{
					$(this).html( '<input type="text" placeholder="'+title+'" disabled />' );
				}
				else
				{
					$(this).html( '<input type="text" placeholder="Search '+title+'"  />' );
				}
				
			} );
		 
			// DataTable
			//var table = $('#table_data_dashboard').DataTable({bFilter: false, bInfo: false});//is for hide datatable search box
			var table = $('#table_data_dashboard').DataTable();
			// Apply the search
			table.columns().every( function () {
				var that = this;
		 
				$( 'input', this.header() ).on( 'keyup change', function () {
					if ( that.search() !== this.value ) {
						that
							.search( this.value )
							.draw();
					}
				} );
			} ); 
		} );
	</script>
	
    <script>
    $(document).ready(function(){
        var textarea = document.getElementById("code1");
        // Wait until animation finished render container
        setTimeout(function(){
            CodeMirror.fromTextArea(textarea, {
                lineNumbers: true,
                matchBrackets: true,
                styleActiveLine: true
            });
        }, 500);	 
    });	
</script>
	<script>
  $(document).ready(function(){
     
    var current_url = window.location.href;
	var ibox_title= $.trim($('.ibox-title h5').text().substr($('.ibox-title h5').text().lastIndexOf("/")+1));
     var hidden_url= $('#hidden_files_out').val(); 
      
 $(".nav-second-level li a").each(function(){
     var urltext= $.trim($(this).text());
    
     if($(this).attr("href") == current_url)
     {
		  
		 if($(this).parents('li').length== 3)
		 {
			  $('#side-menu li').removeClass('active');
		  $('.nav-second-level').removeClass('in');
		 $(".nav-second-level li").removeClass('active');
		 $(this).parent().parent().parent().parent().addClass("in");
		 $(this).parent().parent().addClass("in");
		 $(this).parent().parent().parent().addClass("active");
		 $(this).parent().addClass("active");
		 }
		else{ 
		  $('#side-menu li').removeClass('active');
		  $('.nav-second-level').removeClass('in');
		 $(".nav-second-level li").removeClass('active');
		 $(this).parent().parent().addClass("in");
		 $(this).parent().parent().parent().addClass("active");
		 $(this).parent().addClass("active");
		 }
     }
     
     else if(ibox_title === urltext)
         {
             if($(this).parents('li').length== 3)
		 {
			  $('#side-menu li').removeClass('active');
              $('.nav-second-level').removeClass('in');
             $(".nav-second-level li").removeClass('active');
             $(this).parent().parent().parent().parent().addClass("in");
             $(this).parent().parent().addClass("in");
             $(this).parent().parent().parent().addClass("active");
             $(this).parent().addClass("active");
             }
            else{ 
              $('#side-menu li').removeClass('active');
              $('.nav-second-level').removeClass('in');
             $(".nav-second-level li").removeClass('active');
             $(this).parent().parent().addClass("in");
             $(this).parent().parent().parent().addClass("active");
             $(this).parent().addClass("active");
             }
         }
	
    }); 
  });
  </script>
<script>
	$(document).ready(function() {
	   $('input[name="select_user"]').on('click', function() {
		   
		   if($(this).attr('id') == 'partnerwise') {
				$('#display_default').show(); 
				$('#display_states').hide();
				$('#display_years').hide();
		   }

		   else if($(this).attr('id') == 'state_wise') {
				$('#display_default').hide(); 
				$('#display_states').show();
				$('#display_years').hide();   
		   }
		   else{
			   $('#display_default').hide(); 
				$('#display_states').hide();
				$('#display_years').show();
		   }
	   });
	});
</script>




