
<div class="footer" >

    <div>
        <strong>Copyright</strong> sightsavers &copy; 2015-2016
    </div>
</div>
</div>
</div>
    <!-- Mainly scripts -->
	 <script src="<?php echo PUBLIC_URL; ?>js/jquery-2.1.1.js"></script>
    <script src="<?php echo PUBLIC_URL; ?>js/bootstrap.min.js"></script>
    <script src="<?php echo PUBLIC_URL; ?>js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo PUBLIC_URL; ?>js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo PUBLIC_URL; ?>js/inspinia.js"></script>
    <script src="<?php echo PUBLIC_URL; ?>js/plugins/pace/pace.min.js"></script>

    <script src="<?php echo PUBLIC_URL; ?>js/codemirror.js"></script>
    <script src="<?php echo PUBLIC_URL; ?>js/codescript.js"></script>
	<script src="<?php echo PUBLIC_URL; ?>js/bootstrap_datepicker.js"></script>
  	<script src="<?php echo PUBLIC_URL; ?>js/jquery.dataTables.min.js"></script>
	<script src="<?php echo PUBLIC_URL; ?>js/dataTables.bootstrap.min.js"></script>

<script src="<?php echo PUBLIC_URL; ?>js/bootstrap-multiselect.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
	
    $(document).on('change', '#selectId', function(){ 
      var selectId = $(this).val(); 
	  var partner_id = "<?php echo $this->session->userdata('userinfo')['partner_id'];?>";
	 // alert(selectId);
     
      $.ajax({ 
        type: "POST", 
        url: "<?php echo BASE_URL.'Dashboard/program_district';?>", 
        data: {id:selectId,partner_id1:partner_id}, 
        success: function(result){ 
		
		//alert(result.ss_partners_project_reh);
		$("#menhead123").addClass('in');
          $("#menhead123").html(result);
		  $('#side-menu').off();
			$('#side-menu').metisMenu();
        }
      });
    });
  });
</script>
<script>
	$(document).ready(function(){
		$('#districts1').on('change', function(){
			$('#district_view').val($('#districts1 option:selected').text());
		});
		$('#partner_name1').multiselect({
			selectAllValue: 'multiselect-all',
			enableCaseInsensitiveFiltering: true,
			enableFiltering: true,
			maxHeight: '300',
			buttonWidth: '235'
		});
	})
</script>
<script>
	$(document).ready(function(){
		$('.page-heading').addClass('hidden-print');
		$('.print-btn').on('click', function(){
			
			window.print();
			
		});
	})
</script>
	<script>
	
		$(document).ready(function(){
			$('.table-bordered tbody tr td').each(function(){
				if($(this).children('input[type= "text"]').length >0 || $(this).children('textarea').length >0)
				{
					$(this).addClass('bg_diff');
				}
				
			});

		})
	</script>
	<script>
        $(function(){
           $('.datepicker').datepicker({
              format: "mm-yyyy",
            viewMode: "months", 
            minViewMode: "months"
            });
			$('.datepickerday').datepicker({
              format: "dd-mm-yyyy",
            viewMode: "date", 
            minViewMode: "date"
            });
        });
    </script>
	<script>
		$(document).ready(function(){
			$('#table_data').DataTable();
		});
		$(document).ready( function () {
$('#table_data_dashboard').DataTable();
} );
	</script>
	
	<script>
	
	$('.SeeMore2').on('click', function(){
	var $this = $(this);
	$this.toggleClass('SeeMore2');
	if($(this).find($(".fa")).hasClass('fa-caret-left'))
	{
	$(this).find($(".fa")).removeClass('fa-caret-left').addClass('fa-caret-right');

	} else {
	$(this).find($(".fa")).removeClass('fa-caret-right').addClass('fa-caret-left');
	}
	});
</script>


	<script>
		$(document).ready(function() {
			$.extend( true, $.fn.dataTable.defaults, {
				"ordering": false
			} );
			$('#table_data_dashboard thead th').each( function () {
				var title = $(this).text();
				if(title == 'Last Updated On')
				{
					$(this).html( '<input type="text" placeholder="'+title+'" disabled />' );
				}
				else
				{
					$(this).html( '<input type="text" placeholder="Search '+title+'"  />' );
				}
				
			} );
		 
			// DataTable
			//var table = $('#table_data_dashboard').DataTable({bFilter: false, bInfo: false});//is for hide datatable search box
			/*var table = $('#table_data_dashboard').DataTable();
			// Apply the search
			table.columns().every( function () {
				var that = this;
		 
				$( 'input', this.header() ).on( 'keyup change', function () {
					if ( that.search() !== this.value ) {
						that
							.search( this.value )
							.draw();
					}
				} );
			} ); */
		} );
	</script>
	
    <script>
    $(document).ready(function(){
        var textarea = document.getElementById("code1");
        // Wait until animation finished render container
        setTimeout(function(){
            CodeMirror.fromTextArea(textarea, {
                lineNumbers: true,
                matchBrackets: true,
                styleActiveLine: true
            });
        }, 500);	 
    });	
</script>
	<script>
  $(document).ready(function(){
     
    var current_url = window.location.href;
	var ibox_title= $.trim($('.ibox-title h5').text().substr($('.ibox-title h5').text().lastIndexOf("/")+1));
     var hidden_url= $('#hidden_files_out').val(); 
      
 $(".nav-second-level li a").each(function(){
     var urltext= $.trim($(this).text());
    
     if($(this).attr("href") == current_url)
     {
		  
		 if($(this).parents('li').length== 3)
		 {
			  $('#side-menu li').removeClass('active');
		  $('.nav-second-level').removeClass('in');
		 $(".nav-second-level li").removeClass('active');
		 $(this).parent().parent().parent().parent().addClass("in");
		 $(this).parent().parent().addClass("in");
		 $(this).parent().parent().parent().addClass("active");
		 $(this).parent().addClass("active");
		 }
		else{ 
		  $('#side-menu li').removeClass('active');
		  $('.nav-second-level').removeClass('in');
		 $(".nav-second-level li").removeClass('active');
		 $(this).parent().parent().addClass("in");
		 $(this).parent().parent().parent().addClass("active");
		 $(this).parent().addClass("active");
		 }
     }
     
     else if(ibox_title === urltext)
         {
             if($(this).parents('li').length== 3)
		 {
			  $('#side-menu li').removeClass('active');
              $('.nav-second-level').removeClass('in');
             $(".nav-second-level li").removeClass('active');
             $(this).parent().parent().parent().parent().addClass("in");
             $(this).parent().parent().addClass("in");
             $(this).parent().parent().parent().addClass("active");
             $(this).parent().addClass("active");
             }
            else{ 
              $('#side-menu li').removeClass('active');
              $('.nav-second-level').removeClass('in');
             $(".nav-second-level li").removeClass('active');
             $(this).parent().parent().addClass("in");
             $(this).parent().parent().parent().addClass("active");
             $(this).parent().addClass("active");
             }
         }
	
    }); 
  });
  </script>
<script>
	$(document).ready(function() {
	   $('input[name="select_user"]').on('click', function() {
		   
		   if($(this).attr('id') == 'partnerwise') {
				$('#display_default').show(); 
				$('#display_states').hide();
				$('#display_years').hide();
		   }

		   else if($(this).attr('id') == 'state_wise') {
				$('#display_default').hide(); 
				$('#display_states').show();
				$('#display_years').hide();   
		   }
		   else{
			   $('#display_default').hide(); 
				$('#display_states').hide();
				$('#display_years').show();
		   }
	   });
	});
</script>




