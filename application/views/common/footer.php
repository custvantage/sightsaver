<!-- Mainly scripts -->



<script src="<?php echo PUBLIC_URL; ?>js/jquery-2.1.1.js"></script>
<script src="<?php echo PUBLIC_URL; ?>js/bootstrap.min.js"></script>
<script src="<?php echo PUBLIC_URL; ?>js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo PUBLIC_URL; ?>js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo PUBLIC_URL; ?>js/inspinia.js"></script>
<script src="<?php echo PUBLIC_URL; ?>js/plugins/pace/pace.min.js"></script>

<script src="<?php echo PUBLIC_URL; ?>js/codemirror.js"></script>
<script src="<?php echo PUBLIC_URL; ?>js/codescript.js"></script>

<script>
    $(document).ready(function () {
        var textarea = document.getElementById("code1");
        // Wait until animation finished render container
            setTimeout(function () {
            CodeMirror.fromTextArea(textarea, {
                lineNumbers: true,
                matchBrackets: true,
                styleActiveLine: true
            });
        }, 500);
    });
</script> 

</body>
</html>