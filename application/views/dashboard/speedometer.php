
<div class="row wrapper border-bottom white-bg page-heading ng-scope">
  <div class="col-lg-10">
    <h2>Dashboard - MPR Compliance</h2>
  </div>
</div>
<div class="space15"></div>

<?php $form_attrib=array('id'=>'filter-form'); 
echo form_open('dashboard_graph',$form_attrib);?>

<div class="row flex-element center-both">					
                        <div class="well well-sm ">
                        	<table class="table">
                        		<tr>
                        			<td>
		                       			 	<select name="quaterly_type" class="form-control" onchange="getquaterdom()">
		                                		<option value="monthly" <?php echo ((isset($input['quaterly_type']) && $input['quaterly_type']=='monthly')?'selected':'')?>>Monthly</option>
		                                		<option value="quaterly"  <?php echo ((isset($input['quaterly_type']) && $input['quaterly_type']=='quaterly')?'selected':'')?>>Quaterly</option>
		                                	</select>
                        				</td>
                        				
                        			<td>
		                       			 <select name="quater" class="form-control">
		                                	<option value="1" <?php echo ((isset($input['quater']) && $input['quater']=='1')?'selected':'')?>>1st Quater</option>
		                                	<option value="2" <?php echo ((isset($input['quater']) && $input['quater']=='2')?'selected':'')?>>2nd Quater</option>
		                                	<option value="3" <?php echo ((isset($input['quater']) && $input['quater']=='3')?'selected':'')?>>3rd Quater</option>
		                                	<option value="4" <?php echo ((isset($input['quater']) && $input['quater']=='4')?'selected':'')?>>4th Quater</option>
		                                </select>
                        			
		                       			 <select name="month" class="form-control">
		                                	<option value="1" <?php echo ((isset($input['month']) && $input['month']=='1')?'selected':'')?>>Jan</option>
		                                	<option value="2" <?php echo ((isset($input['month']) && $input['month']=='2')?'selected':'')?>>Feb</option>
		                                	<option value="3" <?php echo ((isset($input['month']) && $input['month']=='3')?'selected':'')?>>Mar</option>
		                                	<option value="4" <?php echo ((isset($input['month']) && $input['month']=='4')?'selected':'')?>>Apr</option>
		                                	<option value="5" <?php echo ((isset($input['month']) && $input['month']=='5')?'selected':'')?>>May</option>
		                                	<option value="6" <?php echo ((isset($input['month']) && $input['month']=='6')?'selected':'')?>>Jun</option>
		                                	<option value="7" <?php echo ((isset($input['month']) && $input['month']=='7')?'selected':'')?>>Jul</option>
		                                	<option value="8" <?php echo ((isset($input['month']) && $input['month']=='8')?'selected':'')?>>Aug</option>
		                                	<option value="9" <?php echo ((isset($input['month']) && $input['month']=='9')?'selected':'')?>>Sept</option>
		                                	<option value="10" <?php echo ((isset($input['month']) && $input['month']=='10')?'selected':'')?>>Oct</option>
		                                	<option value="11" <?php echo ((isset($input['month']) && $input['month']=='11')?'selected':'')?>>Nov</option>
		                                	<option value="12" <?php echo ((isset($input['month']) && $input['month']=='12')?'selected':'')?>>Dec</option>
		                                </select>
                        				</td>
                        				<td>
                        					<select name="year" class="form-control">
                        						<?php foreach ($mpr_summary_year as $arr){?>
                        							<option value="<?php echo $arr->report_year ?>" <?php echo ((isset($input['year']) && $input['year']==$arr->report_year)?'selected':'')?>><?php echo $arr->report_year ?></option>
                        						<?php }?>
                        					</select>
                        				</td>
                        				<td>
                        					<select name="states" class="form-control" onchange="getdistrict()">
                        							<option value="">--Select State--</option>
                        					
                        						<?php foreach ($states as $arr){?>
                        							<option value="<?php echo $arr->ss_states_id ?>" <?php echo ((isset($input['states']) && $input['states']==$arr->ss_states_id)?'selected':'')?>><?php echo $arr->ss_states_name ?></option>
                        						<?php }?>
                        					</select>
                        				</td>
                        				<td>
                        					<select name="districts" class="form-control">
                        						<option value="">--Select District--</option>
                        					
                        					</select>
                        				</td>
                        				<td>
                        				<button class="btn btn-sm btn-primary btn-block" type="submit">
                                    Get Data
                                </button>
                                </td>
                        		</tr>
                        	</table>
							
                        </div>						
                    </div>
					<input type="hidden" name="month_data"/>
					<?php echo form_close();?>
	
<div class="wrapper wrapper-content animated fadeIn ng-scope graphs_section" ng-init="init()">

	<!--<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true"> -->
		
		
			
			
<div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Rural Eye Health & Urban Eye Health</a>
      </h4>
    </div>
    <div id="collapse1" class="panel-collapse collapse in">
      <div class="panel-body">
	  
			
	  
		<!--Rural eye health-->
		<div class="row">
            
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title text-center">
                            <a role="button"  href="javascript:void(0)" aria-expanded="true" aria-controls="collapseOne">

                               OPD
                            </a>
                        </h4>
                    </div>
                    <div class="panel-collapse" >
                        <div class="panel-body">
                              <div class="row">

                                <div class="col-lg-12">
                                  
                                    
                                      <div class="row ">
                                        <div class="col-md-12">
                                           <div id="speedometer_1" style="width: 400px; height: 200px;"></div>
                                        </div>
                                        </div>

                                  
                                </div>

                              </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title text-center">
                            <a role="button"  href="javascript:void(0)" aria-expanded="true" aria-controls="collapseOne">

                               Cataract
                            </a>
                        </h4>
                    </div>
                    <div class="panel-collapse" >
                        <div class="panel-body">
                              <div class="row">

                                <div class="col-lg-12">
                                  
                                    
                                      <div class="row ">
                                        <div class="col-md-12">
                                           <div id="speedometer_2" style="width: 400px; height: 200px;"></div>
                                        </div>
                                        </div>

                                </div>


                              </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title text-center">
                            <a role="button"  href="javascript:void(0)" aria-expanded="true" aria-controls="collapseOne">

                                Refraction 
                            </a>
                        </h4>
                    </div>
                    <div class="panel-collapse" >
                        <div class="panel-body">
                              <div class="row">

                                <div class="col-lg-12">

                                      <div class="row ">
                                        <div class="col-md-12">
                                           <div id="speedometer_3" style="width: 400px; height: 200px;"></div>
                                        </div>
                                        </div>

                                </div>

                              </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title text-center">
                            <a role="button"  href="javascript:void(0)" aria-expanded="true" aria-controls="collapseOne">

                                Spectacles
                            </a>
                        </h4>
                    </div>
                    <div class="panel-collapse" >
                        <div class="panel-body">
                              <div class="row">

                                <div class="col-lg-12">
                                    
                                      <div class="row ">
                                        <div class="col-md-12">
                                           <div id="speedometer_4" style="width: 400px; height: 200px;"></div>
                                        </div>
                                      </div>

                                </div>
                                
                              </div>
                        </div>
                    </div>
                </div>
            </div>
			
        </div>
		
		
		
	  </div>
    </div>
  </div>
  
 
</div> 
			
   
    <!--</div>-->
</div>

<!----php code-->
<div id="g_script"></div>

<script src="<?php echo PUBLIC_URL; ?>js/jquery-2.1.1.js"></script>
<script src="<?php echo PUBLIC_URL; ?>js/bootstrap.min.js"></script>
<script src="<?php echo PUBLIC_URL; ?>js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo PUBLIC_URL; ?>js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo PUBLIC_URL; ?>js/inspinia.js"></script>
<script src="<?php echo PUBLIC_URL; ?>js/plugins/pace/pace.min.js"></script>

<script src="<?php echo PUBLIC_URL; ?>js/codemirror.js"></script>
<script src="<?php echo PUBLIC_URL; ?>js/codescript.js"></script>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>

<script src="https://code.highcharts.com/modules/solid-gauge.js"></script>

<script>

var gaugeOptions = {

    chart: {
        type: 'solidgauge'
    },

    title: null,

    pane: {
        center: ['50%', '85%'],
        size: '140%',
        startAngle: -90,
        endAngle: 90,
        background: {
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
            innerRadius: '60%',
            outerRadius: '100%',
            shape: 'arc'
        }
    },

    tooltip: {
        enabled: false
    },

    // the value axis
    yAxis: {
        stops: [
            [0.1, '#55BF3B'], // green
            [0.5, '#DDDF0D'], // yellow
            [0.9, '#DF5353'] // red
        ],
        lineWidth: 0,
        minorTickInterval: null,
        tickAmount: 2,
        title: {
            y: -70
        },
        labels: {
            y: 16
        }
    },

    plotOptions: {
        solidgauge: {
            dataLabels: {
                y: 5,
                borderWidth: 0,
                useHTML: true
            }
        }
    }
};

getdistrict();
function getdistrict(){
	$.post('getdistrict','state_id='+$('select[name="states"]').val(),function(data){
		$('select[name="districts"]').html(data);
	});
}

getcharts('filter-form');
function getcharts(formid){
	var form=document.getElementById(formid);
	$.post('getcharts_dash',$(form).serialize(),function(data){
		   $("#g_script").html(data);
		})
}

</script>
    <script>
getquaterdom();
function  getquaterdom(){
	var quaterly_type=$("select[name='quaterly_type']").val();
	if(quaterly_type=='monthly'){
		$("select[name='month']").show();
		$("select[name='quater']").hide();
	}
	else{

		$("select[name='quater']").show();
		$("select[name='month']").hide();
		}
}
</script>



