
<div class="row wrapper border-bottom white-bg page-heading ng-scope">
  <div class="col-lg-10">
    <h2>Urban Eye Health</h2>
  </div>
</div>
<div class="space15"></div>
<?php if($this->input->post('month_from')) {?>
<input type="hidden" value="<?php echo BASE_URL.'dashboard_graph';?>" id="hidden_files_out">
<?php }?>
<?php $form_attrib=array('id'=>'filter-form'); 
echo form_open('dashboard_graph',$form_attrib);?>

<div class="row flex-element center-both">					
                        <div class="well well-sm ">
                        	<table class="table">
                        		<tr>
                        			<td>
		                       			 	<select name="quaterly_type" class="form-control" onchange="getquaterdom()">
		                                		<option value="monthly" <?php echo ((isset($input['quaterly_type']) && $input['quaterly_type']=='monthly')?'selected':'')?>>Monthly</option>
		                                		<option value="quaterly"  <?php echo ((isset($input['quaterly_type']) && $input['quaterly_type']=='quaterly')?'selected':'')?>>Quaterly</option>
		                                	</select>
                        				</td>
                        				
                        			<td>
		                       			 <select name="quater" class="form-control">
		                                	<option value="1" <?php echo ((isset($input['quater']) && $input['quater']=='1')?'selected':'')?>>1st Quater</option>
		                                	<option value="2" <?php echo ((isset($input['quater']) && $input['quater']=='2')?'selected':'')?>>2nd Quater</option>
		                                	<option value="3" <?php echo ((isset($input['quater']) && $input['quater']=='3')?'selected':'')?>>3rd Quater</option>
		                                	<option value="4" <?php echo ((isset($input['quater']) && $input['quater']=='4')?'selected':'')?>>4th Quater</option>
		                                </select>
                        			
		                       			 <select name="month" class="form-control">
		                                	<option value="1" <?php echo ((isset($input['month']) && $input['month']=='1')?'selected':'')?>>Jan</option>
		                                	<option value="2" <?php echo ((isset($input['month']) && $input['month']=='2')?'selected':'')?>>Feb</option>
		                                	<option value="3" <?php echo ((isset($input['month']) && $input['month']=='3')?'selected':'')?>>Mar</option>
		                                	<option value="4" <?php echo ((isset($input['month']) && $input['month']=='4')?'selected':'')?>>Apr</option>
		                                	<option value="5" <?php echo ((isset($input['month']) && $input['month']=='5')?'selected':'')?>>May</option>
		                                	<option value="6" <?php echo ((isset($input['month']) && $input['month']=='6')?'selected':'')?>>Jun</option>
		                                	<option value="7" <?php echo ((isset($input['month']) && $input['month']=='7')?'selected':'')?>>Jul</option>
		                                	<option value="8" <?php echo ((isset($input['month']) && $input['month']=='8')?'selected':'')?>>Aug</option>
		                                	<option value="9" <?php echo ((isset($input['month']) && $input['month']=='9')?'selected':'')?>>Sept</option>
		                                	<option value="10" <?php echo ((isset($input['month']) && $input['month']=='10')?'selected':'')?>>Oct</option>
		                                	<option value="11" <?php echo ((isset($input['month']) && $input['month']=='11')?'selected':'')?>>Nov</option>
		                                	<option value="12" <?php echo ((isset($input['month']) && $input['month']=='12')?'selected':'')?>>Dec</option>
		                                </select>
                        				</td>
                        				<td>
                        					<select name="year" class="form-control">
                        						<?php foreach ($mpr_summary_year as $arr){?>
                        							<option value="<?php echo $arr->report_year ?>" <?php echo ((isset($input['year']) && $input['year']==$arr->report_year)?'selected':'')?>><?php echo $arr->report_year ?></option>
                        						<?php }?>
                        					</select>
                        				</td>
                        				<td>
                        					<select name="states" class="form-control" onchange="getdistrict()">
                        							<option value="">--Select State--</option>
                        					
                        						<?php foreach ($states as $arr){?>
                        							<option value="<?php echo $arr->ss_states_id ?>" <?php echo ((isset($input['states']) && $input['states']==$arr->ss_states_id)?'selected':'')?>><?php echo $arr->ss_states_name ?></option>
                        						<?php }?>
                        					</select>
                        				</td>
                        				<td>
                        					<select name="districts" class="form-control">
                        						<option value="">--Select District--</option>
                        					
                        					</select>
                        				</td>
                        				<td>
                        				<button class="btn btn-sm btn-primary btn-block" type="submit">
                                    Get Data
                                </button>
                                </td>
                        		</tr>
                        	</table>
							
                        </div>						
                    </div>
					<input type="hidden" name="month_data"/>
					<?php echo form_close();?>
<div class="wrapper wrapper-content animated fadeIn ng-scope graphs_section" ng-init="init()">

	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button"  href="javascript:void(0)" aria-expanded="true" aria-controls="collapseOne">
                        
                        OPD/Persons Examined
                    </a>
                </h4>
            </div>
            <div class="panel-collapse" >
                <div class="panel-body">
                
			<div class="panel with-nav-tabs panel-default">
                <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1default" data-toggle="tab">No. of persons examined</a></li>
                            <li><a href="#tab2default" data-toggle="tab">No. of persons examined statewise</a></li>
                            <li><a href="#tab3default" data-toggle="tab">No. of persons examined trend</a></li>
                        </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab1default">
                        			   <div id="plain_graph_1"></div>
						
                        </div>
                        <div class="tab-pane fade" id="tab2default">
                        	<div id="graph2_1"></div>
                        </div>
                        <div class="tab-pane fade" id="tab3default">
                        	<div id="graph3_1"></div>
                      	</div>
                    </div>
                </div>
            </div>                
                      
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
                <h4 class="panel-title">
                    <a role="button"  href="javascript:void(0)" aria-expanded="true" aria-controls="collapseOne">
                        
                       Cataract operations
                    </a>
                    
                </h4>
            </div>
            <div  class="panel-collapse " >
                <div class="panel-body">
                        
                        	<div class="panel with-nav-tabs panel-default">
                <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1default2" data-toggle="tab">Cataract operations</a></li>
                            <li><a href="#tab2default2" data-toggle="tab">Cataract operations statewise</a></li>
                            <li><a href="#tab3default2" data-toggle="tab">Cataract operations trend</a></li>
                        </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab1default2">
                        			   <div id="plain_graph_2"></div>
						
                        </div>
                        <div class="tab-pane fade" id="tab2default2">
                        	<div id="graph2_2"></div>
                        </div>
                        <div class="tab-pane fade" id="tab3default2">
                        	<div id="graph3_2" ></div>
                      	</div>
                    </div>
                </div>
            </div>       
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingThree">
                <h4 class="panel-title">
                     <a role="button"  href="javascript:void(0)" aria-expanded="true" aria-controls="collapseOne">
                        
                       Refraction
                    </a>
                    
                </h4>
            </div>
            <div class="panel-collapse ">
                <div class="panel-body">
                            
			<div class="panel with-nav-tabs panel-default">
                <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1default" data-toggle="tab">No. of persons refracted</a></li>
                            <li><a href="#tab2default" data-toggle="tab">No. of persons refracted statewise</a></li>
                            <li><a href="#tab3default" data-toggle="tab">No. of persons refracted trend</a></li>
                        </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab1default">
                        			   <div id="plain_graph_3"></div>
						
                        </div>
                        <div class="tab-pane fade" id="tab2default">
                        	<div id="graph2_3"></div>
                        </div>
                        <div class="tab-pane fade" id="tab3default">
                        	<div id="graph3_3"></div>
                      	</div>
                    </div>
                </div>
            </div>    
                      
                </div>
            </div>
        </div>
		<div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingFour">
                <h4 class="panel-title">
                    <a role="button"  href="javascript:void(0)" aria-expanded="true" aria-controls="collapseOne">
                        
                       Spectacles
                    </a>
                    
                </h4>
            </div>
            <div class="panel-collapse ">
                <div class="panel-body">
 <div class="panel with-nav-tabs panel-default">
                <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1default" data-toggle="tab">No. of persons disbursed spectacles refracted</a></li>
                            <li><a href="#tab2default" data-toggle="tab">No. of persons disbursed spectacles statewise</a></li>
                            <li><a href="#tab3default" data-toggle="tab">No. of persons disbursed spectacles trend</a></li>
                        </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab1default">
                        			   <div id="plain_graph_4"></div>
						
                        </div>
                        <div class="tab-pane fade" id="tab2default">
                        	<div id="graph2_4"></div>
                        </div>
                        <div class="tab-pane fade" id="tab3default">
                        	<div id="graph3_4"></div>
                      	</div>
                    </div>
                </div>
            </div>    
                      
                </div>
            </div>
        </div>
		<div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingFive">
                <h4 class="panel-title">
                    <a role="button"  href="javascript:void(0)" aria-expanded="true" aria-controls="collapseOne">
                        
                       Inclusive Education
                    </a>
                    
                </h4>
            </div>
            <div class="panel-collapse " >
                <div class="panel-body">
                      
 <div class="panel with-nav-tabs panel-default">
                <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1default" data-toggle="tab">No. of children currently in primary & secondary</a></li>
                            <li><a href="#tab2default" data-toggle="tab">No. of children currently in primary & secondary statewise</a></li>
                            <li><a href="#tab3default" data-toggle="tab">children currently in primary & secondary trend</a></li>
                        </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab1default">
                        			   <div id="plain_graph_5"></div>
						
                        </div>
                        <div class="tab-pane fade" id="tab2default">
                        	<div id="graph2_5"></div>
                        </div>
                        <div class="tab-pane fade" id="tab3default">
                        	<div id="graph3_5"></div>
                      	</div>
                    </div>
                </div>
            </div> 
                      
                      
                </div>
            </div>
        </div>
		<div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingSix">
                <h4 class="panel-title">
                    <a role="button"  href="javascript:void(0)" aria-expanded="true" aria-controls="collapseOne">
                        
                       Trainings
                    </a>
                    
                </h4>
            </div>
            <div class="panel-collapse " >
                <div class="panel-body">

                
                
                 <div class="panel with-nav-tabs panel-default">
                <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1default" data-toggle="tab">No disabled people attended trainings</a></li>
                            <li><a href="#tab2default" data-toggle="tab">No disabled people attended trainings statewise</a></li>
                            <li><a href="#tab3default" data-toggle="tab">No disabled people attended trainings trend</a></li>
                        </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab1default">
                        			   <div id="plain_graph_6"></div>
						
                        </div>
                        <div class="tab-pane fade" id="tab2default">
                        	<div id="graph2_6"></div>
                        </div>
                        <div class="tab-pane fade" id="tab3default">
                        	<div id="graph3_6"></div>
                      	</div>
                    </div>
                </div>
            </div> 
                
                
                
                
                
                
                
                </div>
            </div>
        </div>
    </div>
</div>
<div id="g_script"></div>
<script src="<?php echo PUBLIC_URL; ?>js/jquery-2.1.1.js"></script>
<script src="<?php echo PUBLIC_URL; ?>js/bootstrap.min.js"></script>
<script src="<?php echo PUBLIC_URL; ?>js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo PUBLIC_URL; ?>js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom & plugin javascript -->
<script src="<?php echo PUBLIC_URL; ?>js/inspinia.js"></script>
<script src="<?php echo PUBLIC_URL; ?>js/plugins/pace/pace.min.js"></script>

<script src="<?php echo PUBLIC_URL; ?>js/codemirror.js"></script>
<script src="<?php echo PUBLIC_URL; ?>js/codescript.js"></script>
<script src="<?php echo PUBLIC_URL; ?>js/bootstrap_datepicker.js"></script>
<script src="<?php echo PUBLIC_URL?>/js/highcharts.js"></script>
<script src="<?php echo PUBLIC_URL?>/js/highchart_theme.js"></script>
<script>
getdistrict();
function getdistrict(){
	$.post('getdistrict','state_id='+$('select[name="states"]').val(),function(data){
		$('select[name="districts"]').html(data);
	});
}

getcharts('filter-form');
function getcharts(formid){
	var form=document.getElementById(formid);
	$.post('getcharts_ueh',$(form).serialize(),function(data){
		   $("#g_script").html(data);
		})
}
$(function(){
$(".nav-tabs").on('shown.bs.tab',function(){
	$(this).closest(".panel").find("[data-highcharts-chart]").each(function () {
	    var highChart = Highcharts.charts[$(this).data('highchartsChart')];
	    var highChartCont = $(highChart.container).parent();
	    highChart.setSize(highChartCont.width(), highChartCont.height());
	    highChart.hasUserSize = undefined;
	});
		
});
})

</script>
    <script>
getquaterdom();
function  getquaterdom(){
	var quaterly_type=$("select[name='quaterly_type']").val();
	if(quaterly_type=='monthly'){
		$("select[name='month']").show();
		$("select[name='quater']").hide();
	}
	else{

		$("select[name='quater']").show();
		$("select[name='month']").hide();
		}
}
</script>