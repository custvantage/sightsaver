<div class="row wrapper border-bottom white-bg page-heading ng-scope">
  <div class="col-lg-10">
    <h2>Approve / Reject</h2>
  </div>
</div>
<?php  //if(isset($month_id)) { echo "<pre>";print_r($month_id); } ?>
<div class="wrapper wrapper-content animated fadeIn ng-scope" ng-init="init()">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox">
    
        <div class="ibox-content">
			<div class="row">				
				<?php //echo form_open('mpr_reh');?>
					<div class="row">
						<div class="col-lg-6">
							  <h5>Main Report</h5>
						</div>
						<div class="clearfix"></div>
						<?php if(isset($partner_summary_data) && !empty($partner_summary_data)) 
	                       {   if($partner_summary_data->status == 2 && $partner_summary_data->ss_mpr_module_name == 'main entry'){
	                           ?>     
						<?php if($this->session->userdata('userinfo')['default_role']==2 || $this->session->userdata('userinfo')['default_role']==3 || $this->session->userdata('userinfo')['default_role']==4) { ?>
						<div class="col-lg-2 text-right">
                            <button class="btn btn-sm btn-primary btn-block" type="button" data-toggle="modal" data-target="#approve-modal">
                                    Approve
                                </button>
                        </div>
                        <div class="col-lg-2 text-right">
                            <button class="btn btn-sm btn-primary btn-block" type="button" data-toggle="modal" data-target="#disapprove-modal">
                                    Disapprove
                                </button>
                        </div>
						<div class="col-lg-2 text-right">
                            <button class="btn btn-sm btn-primary btn-block" type="button" data-toggle="modal" data-target="#po_pm_comments">
                                    View PO/PM Comments
                                </button>
                        </div>
						
						<?php } if($this->session->userdata('userinfo')['default_role'] == 5)
						{ //if pm/po ?>
                        <div class="col-lg-2 text-right">
                            <button class="btn btn-sm btn-primary btn-block" type="button" data-toggle="modal" data-target="#comment-modal">
                                    Comments
                                </button>
                        </div>
						   <?php } } else {} }  ?>
						
						<?php if(isset($partner_summary_data) && !empty($partner_summary_data)) 
	                       {   if($partner_summary_data->status == 3) {
	                           ?> 
						<?php  if(($this->session->userdata('userinfo')) && $this->session->userdata('userinfo')['default_role']==1) { ?>
						<div class="col-lg-2 text-right">
                            <button class="btn btn-sm btn-primary btn-block" type="button" data-toggle="modal" data-target="#approve-modal">
                                    Approve
                                </button>
                        </div>
                        <div class="col-lg-2 text-right">
                            <button class="btn btn-sm btn-primary btn-block" type="button" data-toggle="modal" data-target="#disapprove-modal">
                                    Disapprove
                                </button>
                        </div>
						   <?php }} else {}  } ?>
						
					</div>
				 <div class="clearfix"></div>			
		  </div>
		  <hr>
		  <div class="clearfix"></div>
		   <?php //echo form_close();?>
	
	<?php if(isset($partner_summary_data) && !empty($partner_summary_data)) 
	      {    
	       ?>    
          <div class="row m-t-lg" ng-show="hasData">
            <div class="report-table" id="print-data">
              <div class="text-center">
		  <h3 class="table-heading"><?php if($partner_summary_data->ss_mpr_project_name=='rural eye health') {echo "RURAL EYE HEALTH PROGRAMME"; }
           if($partner_summary_data->ss_mpr_project_name=='urban eye health') {echo "URBAN EYE HEALTH PROGRAMME"; }
           if($partner_summary_data->ss_mpr_project_name=='Social inclusion') {echo "Social inclusion"; }	
           if($partner_summary_data->ss_mpr_project_name=='Inclusive education') {echo "Inclusive education"; }  ?> </h3>
                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td colspan="11">Monthly Partner Reporting Format
                        <button class="btn btn-default btn-xs pull-right text-default print-btn" ng-click="print()"><i class="fa fa-print"></i> Print</button>
                      </td>
                    </tr>
					    
                    <tr>  
                      <td class="label-bg"><label>Organisation Name:</label></td>
                      <td><?php if(isset($partner_summary_data) && $partner_summary_data!=null) { echo $partner_summary_data->ss_partners_name; } ?></td>
                      <td class="label-bg" colspan="2"><label>Name of the State:</label></td>
                      <td colspan="4"><?php if(isset($partner_summary_data) && $partner_summary_data!=null) { echo $partner_summary_data->ss_states_name; } ?></td>
                    </tr>
                    <tr>
                      <td class="label-bg"><label>District Name:</label></td>
                      <td><?php if(isset($partner_summary_data) && $partner_summary_data!=null) { echo $partner_summary_data->ss_district_name; } ?></td>
                      <td class="label-bg" colspan="2"><label>Reporting Month:</label></td>
                      <td colspan="4"><?php if(isset($partner_summary_data) && $partner_summary_data!=null) { echo date('M', strtotime($partner_summary_data->ss_mpr_report_month)); } ?></td>
                    </tr>
                    <tr>
                      <td class="label-bg"><label>Programme Start Date</label></td>
                      <td><?php if(isset($partner_summary_data) && $partner_summary_data!=null) {  } ?></td>
                      <td class="label-bg" colspan="2"><label>Reporting Year:</label></td>
                      <td colspan="4"><?php if(isset($partner_summary_data) && $partner_summary_data!=null) { echo date('Y', strtotime($partner_summary_data->ss_mpr_report_month)); } ?></td>
                    </tr>
                    <tr>
                      <td class="label-bg"><label>Demo/Scale up (to be filled by Sightsavers Staff)</label></td>
                      <td><?php if(isset($partner_summary_data) && $partner_summary_data!=null) { } ?></td>
                      <td colspan="2" class="label-bg"><label>District Budget (to be filled by Sightsavers Staff)</label></td>
                      <td colspan="4"><?php if(isset($partner_summary_data) && $partner_summary_data!=null) { 
					   } ?></td>
                    </tr>
                    <tr>
                      <td class="label-bg"><label>I. Total Number of districts covered by the hospital / organisation</label></td>
                      <td colspan="9"><?php if(isset($partner_summary_data) && $partner_summary_data!=null) {   
					  echo $partner_summary_data->ss_mpr_district_cover_hospital; } ?></td>
                    </tr>
                    <tr>
                      <td class="label-bg"><label>II. Total number of districts covered under Sightsavers programme</label></td>
                      <td colspan="9"><?php if(isset($partner_summary_data) && $partner_summary_data!=null) {   
					  echo $partner_summary_data->ss_mpr_district_sightsaver; } ?></td>
                    </tr>
                  </table>
                </div>
            
			   <?php 
				 
			   $section_id = "";
			   if(isset($merge))
			   { 
				   foreach($merge as $key_metrics => $metrics)
				   {   
				 if($metrics->ss_section_layout_section_col_count == 1 && $metrics->ss_section_layout_analysis_description == ""){  ?>
                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td><?php echo $metrics->ss_section_layout_section_name;?></td>
                      <td width="150">Yearly Targets</td>
                      <td width="150">This month</td>
                    </tr>
					
					 <?php 
					 
					 $description=explode('#', $metrics->ss_metric_master_description);
					 $metric_id_one=explode('#', $metrics->ss_metric_master_id);
					 $data_detail=array_combine($metric_id_one,$description);
					 foreach($data_detail as $key => $metric_dis)
					 {
					  ?>
                    <tr>
                      <td><?php echo($metric_dis); ?></td>
                      <td> - </td>
					   <?php // echo "<pre>"; print_R($month_id); die;  ?>
					  
                      <td><?php $one_col_val = $this->metricdata->getOneColVal_ampr($key,$month_id[0],$month_id[1]);
					 
					  if(!empty($one_col_val)){ echo $one_col_val;}?></td>
					
                    </tr>
					   <?php }  ?>
                  </table>
                </div>
					 <?php } else if($metrics->ss_section_layout_section_col_count == 4 && $metrics->ss_section_layout_analysis_description == ""){?>

                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td rowspan="3" colspan="4"><?php echo $metrics->ss_section_layout_section_name;?></td>
                      <td colspan="4">Yearly Targets</td>
                      <td colspan="4">Achievement this Month</td>
                    </tr>
                    <tr class="lead-bg">
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                    </tr>
                    <tr class="lead-bg">
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                    </tr>
					 <?php 
					 $description=explode('#', $metrics->ss_metric_master_description);
					 $metric_id_one=explode('#', $metrics->ss_metric_master_id);
					 if(!empty($merge2))
						 {
						 $metric_id_yearly=explode('#', $merge2[$key_metrics]->ss_metric_master_id);
						 $description_yearly=explode('#', $merge2[$key_metrics]->ss_metric_master_description);
						 }
					else
						   {
							$metric_id_yearly=array();
							$description_yearly =array();
						   }
					 
					 $all_keys = array_merge($metric_id_one,$metric_id_yearly);
					 $all_desc = array_merge($description,$description_yearly);
					 $data_detail=array_combine($metric_id_one,$description);
					 $newArray = array_keys($data_detail);
					
					foreach($data_detail as $key1 => $metric_dis_four)
						{
							$key2 = array_search($key1, $newArray); 
							echo "<tr>";
							echo '<td colspan="4">'.$metric_dis_four.'</td>';
							
					  ?>
                      
                      <td><?php if(!empty($metric_id_yearly)) { $four_col_data = $this->metricdata->getFourColVal_ampr($metric_id_yearly[$key2],$month_id[0],$month_id[1]);if(!empty($four_col_data->ss_mpr_four_column_data_value_men)){ echo $four_col_data->ss_mpr_four_column_data_value_men;} else { echo "-";}}else { echo "-";}?></td>
                      <td><?php if(!empty($metric_id_yearly)) { $four_col_data = $this->metricdata->getFourColVal_ampr($metric_id_yearly[$key2],$month_id[0],$month_id[1]);if(!empty($four_col_data->ss_mpr_four_column_data_value_women)){ echo $four_col_data->ss_mpr_four_column_data_value_women;} else { echo "-";}}else { echo "-";}?></td>
                      <td><?php if(!empty($metric_id_yearly)) { $four_col_data = $this->metricdata->getFourColVal_ampr($metric_id_yearly[$key2],$month_id[0],$month_id[1]);if(!empty($four_col_data->ss_mpr_four_column_data_value_boys)){ echo $four_col_data->ss_mpr_four_column_data_value_boys;} else { echo "-";}}else { echo "-";}?></td>
                      <td><?php if(!empty($metric_id_yearly)) { $four_col_data = $this->metricdata->getFourColVal_ampr($metric_id_yearly[$key2],$month_id[0],$month_id[1]);if(!empty($four_col_data->ss_mpr_four_column_data_value_girls)){ echo $four_col_data->ss_mpr_four_column_data_value_girls;} else { echo "-";}}else { echo "-";}?></td>
							
					 
                      <td><?php   $four_col_data = $this->metricdata->getFourColVal_ampr($key1,$month_id[0],$month_id[1]); 
					  if(!empty($four_col_data->ss_mpr_four_column_data_value_men)){ echo $four_col_data->ss_mpr_four_column_data_value_men;} else { echo "-";}?></td>
                      <td><?php $four_col_data = $this->metricdata->getFourColVal_ampr($key1,$month_id[0],$month_id[1]);if(!empty($four_col_data->ss_mpr_four_column_data_value_women)){ echo $four_col_data->ss_mpr_four_column_data_value_women;} else { echo "-";}?></td>
                      <td><?php $four_col_data = $this->metricdata->getFourColVal_ampr($key1,$month_id[0],$month_id[1]);if(!empty($four_col_data->ss_mpr_four_column_data_value_boys)){ echo $four_col_data->ss_mpr_four_column_data_value_boys;} else { echo "-";}?></td>
                      <td><?php $four_col_data = $this->metricdata->getFourColVal_ampr($key1,$month_id[0],$month_id[1]);if(!empty($four_col_data->ss_mpr_four_column_data_value_girls)){ echo $four_col_data->ss_mpr_four_column_data_value_girls;} else { echo "-";}?></td>
                    </tr>
						<?php } ?>
                  </table>
                </div>
		<!--///////////////////////////////////////////////////////////////////////////////////start of the eight column data-->		
				 <?php } else if($metrics->ss_section_layout_section_col_count == 8 && $metrics->ss_section_layout_analysis_description == ""){?>

                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td rowspan="4" colspan="4"><?php echo $metrics->ss_section_layout_section_name;?></td>
                     <td colspan="8">Yearly Targets</td>
                      <td colspan="8">Achievement this Month</td>
                    </tr>
						  <tr>
							<td colspan="8"></td>
							<td colspan="4">Elementary (class 1 to 8)</td>
							<td colspan="4">Secondary (class 9 to 12)</td>
						  </tr>
						  <tr>
							<td colspan="8"></td>
							<td>sanctioned</td>
							<td colspan="2">In place</td>
							<td>vacant</td>
							<td>sanctioned</td>
							<td colspan="2">In place</td>
							<td>vacant</td>
						  </tr>
						  <tr>
							<td colspan="8"></td>
							<td>&nbsp;</td>
							<td>Male</td>
							<td>Female</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>Male</td>
							<td>Female</td>
							<td>&nbsp;</td>
						   </tr> 
					 <?php 
					 $description=explode('#', $metrics->ss_metric_master_description);
					 $metric_id_one=explode('#', $metrics->ss_metric_master_id);
					 if(!empty($merge2))
						 {
						 $metric_id_yearly=explode('#', $merge2[$key_metrics]->ss_metric_master_id);
						 $description_yearly=explode('#', $merge2[$key_metrics]->ss_metric_master_description);
						 }
					else
						   {
							$metric_id_yearly=array();
							$description_yearly =array();
						   }
					 
					 $all_keys = array_merge($metric_id_one,$metric_id_yearly);
					 $all_desc = array_merge($description,$description_yearly);
					 $data_detail=array_combine($metric_id_one,$description);
					 $newArray = array_keys($data_detail);
					
					
					foreach($data_detail as $key1 => $metric_dis_four)
						{
							$key2 = array_search($key1, $newArray); 
							echo "<tr>";
							echo '<td colspan="8">'.$metric_dis_four.'</td>';
							
					  ?>
					  
					  
					  <td><?php if(!empty($metric_id_yearly)) { $four_col_data = $this->metricdata->getFourColVal_ampr($metric_id_yearly[$key2],$month_id[0],$month_id[1]);if(!empty($four_col_data->ss_mpr_four_column_data_value_men)){ echo $four_col_data->ss_mpr_four_column_data_value_men;} else { echo "-";}}else { echo "-";}?></td>
                      <td><?php if(!empty($metric_id_yearly)) { $four_col_data = $this->metricdata->getFourColVal_ampr($metric_id_yearly[$key2],$month_id[0],$month_id[1]);if(!empty($four_col_data->ss_mpr_four_column_data_value_women)){ echo $four_col_data->ss_mpr_four_column_data_value_women;} else { echo "-";}}else { echo "-";}?></td>
                      <td><?php if(!empty($metric_id_yearly)) { $four_col_data = $this->metricdata->getFourColVal_ampr($metric_id_yearly[$key2],$month_id[0],$month_id[1]);if(!empty($four_col_data->ss_mpr_four_column_data_value_boys)){ echo $four_col_data->ss_mpr_four_column_data_value_boys;} else { echo "-";}}else { echo "-";}?></td>
                      <td><?php if(!empty($metric_id_yearly)) { $four_col_data = $this->metricdata->getFourColVal_ampr($metric_id_yearly[$key2],$month_id[0],$month_id[1]);if(!empty($four_col_data->ss_mpr_four_column_data_value_girls)){ echo $four_col_data->ss_mpr_four_column_data_value_girls;} else { echo "-";}}else { echo "-";}?></td>
                      <td>
					  
					  <?php  $eight_col_data = $this->metricdata->getEightColVal_ampr($key1,$month_id[0],$month_id[1]); 
					  if(!empty($eight_col_data->ss_mpr_elementory_sanctioned)){ echo $eight_col_data->ss_mpr_elementory_sanctioned;} else { echo "-";}?>
					  </td>
                      <td>
					  <?php $eight_col_data = $this->metricdata->getEightColVal_ampr($key1,$month_id[0],$month_id[1]);if(!empty($eight_col_data->ss_mpr_elementory_boys)){ echo $eight_col_data->ss_mpr_elementory_boys;} else { echo "-";}?>
					  </td>
                      <td>
					  <?php $eight_col_data = $this->metricdata->getEightColVal_ampr($key1,$month_id[0],$month_id[1]);if(!empty($eight_col_data->ss_mpr_elementory_girls)){ echo $eight_col_data->ss_mpr_elementory_girls;} else { echo "-";}?>
					  </td>
                      <td>
					  <?php $eight_col_data = $this->metricdata->getEightColVal_ampr($key1,$month_id[0],$month_id[1]);if(!empty($eight_col_data->ss_mpr_elementory_vacant)){ echo $eight_col_data->ss_mpr_elementory_vacant;} else { echo "-";}?>
					  </td>
					  
					  <td>
					  <?php   $eight_col_data = $this->metricdata->getEightColVal_ampr($key1,$month_id[0],$month_id[1]); 
					  if(!empty($eight_col_data->ss_mpr_secondary_sanctioned)){ echo $eight_col_data->ss_mpr_secondary_sanctioned;} else { echo "-";}?>
					  </td>
                      <td>
					  <?php $eight_col_data = $this->metricdata->getEightColVal_ampr($key1,$month_id[0],$month_id[1]);if(!empty($eight_col_data->ss_mpr_secondary_boys)){ echo $eight_col_data->ss_mpr_secondary_boys;} else { echo "-";}?>
					  </td>
                      <td>
					  <?php $eight_col_data = $this->metricdata->getEightColVal_ampr($key1,$month_id[0],$month_id[1]);if(!empty($eight_col_data->ss_mpr_secondary_girls)){ echo $eight_col_data->ss_mpr_secondary_girls;} else { echo "-";}?>
					  </td>
                      <td>
					  <?php $eight_col_data = $this->metricdata->getEightColVal_ampr($key1,$month_id[0],$month_id[1]);if(!empty($eight_col_data->ss_mpr_secondary_vacant)){ echo $eight_col_data->ss_mpr_secondary_vacant;} else { echo "-";}?>
					  </td>
                    </tr>
						<?php } ?>
                  </table>
                </div>
				
			<!--///////////////////////////////////////////////////////////////////////////////////end of the code of eight column data-->	
					 <?php } else if($metrics->ss_section_layout_analysis_description != ""){?>
               

                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td colspan="2"><?php echo $metrics->ss_section_layout_section_name;?></td>
                    </tr>
					 <?php 
					 $description_analysis=explode('#', $metrics->ss_metric_master_description);
                     $metric_id_analisys=explode('#', $metrics->ss_metric_master_id);
					 $data_detail_analysis=array_combine($metric_id_analisys,$description_analysis);
					 foreach($data_detail_analysis as $key => $metric_analysis)
						{
					  ?>
                    <tr>
                      <td class="label-bg"><?php echo $metric_analysis;?></td>
                      <td width="400"><?php $analysis_data = $this->metricdata->getAnalysisVal_ampr($key,$month_id[0],$month_id[1]);if(!empty($analysis_data)){ echo $analysis_data;}?></td>
                    </tr>
						<?php } ?>

                  </table>
                </div>
					 <?php // } $section_id = $metrics->ss_section_layout_ss_section_layout_id; }else{?>
						 
		  <?php }  } ?>
                   <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td>Feedback Section (this section is to be filled in by Sightsavers programme staff based on the information
                        and analysis given above)</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                </div>


                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                      <td class="lead-bg" width="400">Details of case studies/anecdotes: </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="lead-bg" width="400">Details of case studies/anecdotes: </td>
                      <td>&nbsp;</td>
                    </tr>

                  </table>
                </div>

                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                      <td height="60">&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="label-bg">Signature</td>
                      <td class="label-bg">Signature</td>
                      <td class="label-bg">Signature</td>
                    </tr>
                    <tr>
                      <td class="lead-bg"><label>Project Coordinator</label></td>
                      <td class="lead-bg"> <label>Ophthalmologist</label></td>
                      <td class="lead-bg"><label>Project Director</label></td>
                    </tr>
                  </table>
                </div>
	   <?php }  } else { ?>
	
                 <div class="row m-t-lg" ng-show="hasData">
            <div class="report-table" id="print-data">
              <div class="text-center">
                <h3 class="table-heading">RURAL EYE HEALTH PROGRAMME</h3>
                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td colspan="11">Monthly Partner Reporting Format
                        <button class="btn btn-default btn-xs pull-right text-default print-btn" ng-click="print()"><i class="fa fa-print"></i> Print</button>
                      </td>
                    </tr>
					    
                    <tr>  
                      <td class="label-bg"><label>Organisation Name:</label></td>
                      <td></td>
                      <td class="label-bg" colspan="2"><label>Name of the State:</label></td>
                      <td colspan="4"></td>
                    </tr>
                    <tr>
                      <td class="label-bg"><label>District Name:</label></td>
                      <td></td>
                      <td class="label-bg" colspan="2"><label>Reporting Month:</label></td>
                      <td colspan="4"></td>
                    </tr>
                    <tr>
                      <td class="label-bg"><label>Programme Start Date</label></td>
                      <td></td>
                      <td class="label-bg" colspan="2"><label>Reporting Year:</label></td>
                      <td colspan="4"></td>
                    </tr>
                    <tr>
                      <td class="label-bg"><label>Demo/Scale up (to be filled by Sightsavers Staff)</label></td>
                      <td></td>
                      <td colspan="2" class="label-bg"><label>District Budget (to be filled by Sightsavers Staff)</label></td>
                      <td colspan="4"></td>
                    </tr>
                    <tr>
                      <td class="label-bg"><label>I. Total Number of districts covered by the hospital / organisation</label></td>
                      <td colspan="9"><?php if(isset($partner_summary_data) && $partner_summary_data!=null) {   
					  echo $partner_summary_data->ss_mpr_district_cover_hospital; } ?></td>
                    </tr>
                    <tr>
                      <td class="label-bg"><label>II. Total number of districts covered under Sightsavers programme</label></td>
                      <td colspan="9"><?php if(isset($partner_summary_data) && $partner_summary_data!=null) {   
					  echo $partner_summary_data->ss_mpr_district_sightsaver; } ?></td>
                    </tr>
                   </table>
                </div>

                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td>1.Vision Centres /Camps</td>
                      <td width="150">Yearly Targets</td>
                      <td width="150">This month</td>
                    </tr>
                    <tr>
                      <td>a. Number of Vision Centres (VC) operational in the district Partner supported by Sightsavers</td>
                      <td> - </td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>b. Number of Mobile Vision Centres operational in the district supported by Sightsavers</td>
                      <td> - </td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>c. Number of Vision Centres (VC) operational in the district setup by Government</td>
                      <td> - </td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>d. Number of Mobile Vision Centres operational in the district setup by Government</td>
                      <td> - </td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>e. Number of Camps conducted during the month</td>
                      <td>&nbsp;</td>
                      <td></td>
                    </tr>
					
                  </table>
                </div>


                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td rowspan="3" colspan="4">2. OPD/Screening</td>
                      <td colspan="4">Yearly Targets</td>
                      <td colspan="4">Achievement this Month</td>
                    </tr>
                    <tr class="lead-bg">
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                    </tr>
                    <tr class="lead-bg">
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                    </tr>
                    <tr>
                      <td colspan="4">a. Total OPD for the month (this should be the total OPD figures for the entire partner hospital across
                        all districts irrespective whether it is supported by Sightsavers or not)</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">b. Number of Screening at Vision Centre for the programme</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">c. Number of Screening at Camps for the programme</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> </td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">d. Number of OPD at Secondary / tertiary Eye Care Centre for the Sightsavers supported programme district
                      </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                  </table>
                </div>

                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td rowspan="3" colspan="4">3. Referral/ Reported</td>
                      <td colspan="4">Yearly Targets</td>
                      <td colspan="4">Achievement this Month</td>
                    </tr>
                    <tr class="lead-bg">
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                    </tr>
                    <tr class="lead-bg">
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                    </tr>
                    <tr>
                      <td colspan="4">a. Number of persons referred from Vision Centre to base hospital</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">b. Number of persons referred from camps to base hospital</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">c. Number of persons referred to Govt. hospital/ other hospitals</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">d. Total number of referrals undertaken by ASHA/ANM during the month</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                    </tr>
                    <tr>
                      <td colspan="4">d1. Number of persons referred to Vision Centres by ASHA / ANM</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">d2. Number of persons referred to camps by by ASHA / ANM</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">d3. Number of persons referred directly to base hospital by ASHA/ANM </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">d4. Number of persons referred directly to Govt. Hospital by ASHA/ANM</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">e. Number of persons reported at base hospital from Vision Centre</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">f. Number of persons reported at base hospital from camps</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">g. Number of persons reported at Govt. hospital/ other hospitals</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                  </table>
                </div>


                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td rowspan="3" colspan="4">4. Refraction</td>
                      <td colspan="4">Yearly Targets</td>
                      <td colspan="4">Achievement this Month</td>
                    </tr>
                    <tr class="lead-bg">
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                    </tr>
                    <tr class="lead-bg">
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                    </tr>
                    <tr>
                      <td colspan="4">a. Total Number of refraction undertaken</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>-</td>
                      <td>-</td>
                      <td>-</td>
                      <td>-</td>
                    </tr>
                    <tr>
                      <td colspan="4">a1. Number of persons refracted at the Vision Centre</td>
                      <td>  </td>
                      <td>  </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">a2. Number of persons refracted at the camps</td>
                      <td>  </td>
                      <td>  </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">a3. Number of persons refracted at the base hospitals</td>
                      <td>  </td>
                      <td>  </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">b. Number of persons identified with Refractive Error (RE) at the Vision Centre</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">c. Number of persons identified with Refractive Error (RE) at the camps</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">d. Number of persons identified with RE at base hospitals</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                  </table>
                </div>



                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td rowspan="3" colspan="4">5. Spectacles</td>
                      <td colspan="4">Yearly Targets</td>
                      <td colspan="4">Achievement this Month</td>
                    </tr>
                    <tr class="lead-bg">
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                    </tr>
                    <tr class="lead-bg">
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                    </tr>
                    <tr>
                      <td colspan="4">a. Number of persons with RE, prescribed spectacles at the Vision Centre</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">b. Number of persons with RE, prescribed spectacles at the camps</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">c. Number of persons with RE, prescribed spectacles at base hospital</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">d. Number of persons dispensed spectacles free at Vision Centre</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">e. Number of persons dispensed spectacles free at camp</td>
                      <td>  </td>
                      <td>  </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">f. Number of persons dispensed spectacles free at base hospital</td>
                      <td>  </td>
                      <td>  </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">g. Number of persons prescribed spectacles who purchased spectacles at Vision Centre (subsidised rate)</td>
                      <td>  </td>
                      <td>  </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">h. Number of persons prescribed spectacles who purchased spectacles at camp (subsidised rate)</td>
                      <td>  </td>
                      <td>  </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">i. Number of persons prescribed spectacles who purchased spectacles at base hospital (subsidised rate)</td>
                      <td> </td>
                      <td>  </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">j. Number of persons prescribed spectacles who purchased spectacles at Vision Centre (full rate)</td>
                      <td> </td>
                      <td>  </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">k. Number of persons prescribed spectacles who purchased spectacles at camp (full rate)</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">l. Number of persons prescribed spectacles who purchased spectacles at base hospital (full rate)</td>
                      <td>  </td>
                      <td>  </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                  </table>
                </div>



                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td rowspan="3" colspan="4">6. Cataract</td>
                      <td colspan="4">Yearly Targets</td>
                      <td colspan="4">Achievement this Month</td>
                    </tr>
                    <tr class="lead-bg">
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                    </tr>
                    <tr class="lead-bg">
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                    </tr>
                    <tr>
                      <td colspan="4">a. Number of persons referred to base hospital by ASHA / ANM for cataract </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">b. Number of persons referred to VCs by ASHA / ANM for cataract </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">c. Number of persons referred to camp by ASHA / ANM for cataract </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">d. Number of persons identified with Cataract and referred to base hospital from VCs</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">e. Number of persons identified with Cataract and referred to base hospital from Camp</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">f. Number of persons identified with Cataract and referred by ASHA / ANM who reported to base hospital</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">g. Number of persons identified with cataract and referred from Vision Centre and camps who reported
                        to base hospital</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">h. Number of Cataract surgeries undertaken at Sightsavers intervention District (by partner or other
                        hospitals)
                      </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">i. Number of Cataract surgeries undertaken by the partner</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">j. Number of Cataract surgeries undertaken by partner calculated after Attribution</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">k. Number of Cataract operations performed free of cost</td>
                      <td>  </td>
                      <td>  </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">l. Number of Cataract operations performed subsidised</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">m. Number of Cataract operations performed paid</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">n. Number of Cataract surgeries performed paid calculated after Attribution</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                    </tr>
                    <tr>
                      <td colspan="4">o. Number of Cataract surgeries supported by Sightsavers</td>
                      <td>  </td>
                      <td>  </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">p. Number of Cataract surgeries supported by DBCS</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">q. Number of Cataract surgeries supported by other sources</td>
                      <td>  </td>
                      <td>  </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">r. Number of persons who came for the first follow-up after Cataract Operations to base hospital</td>
                      <td> </td>
                      <td> </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                  </table>
                </div>


                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td rowspan="3" colspan="4">7. Glaucoma </td>
                      <td colspan="4">Yearly Targets</td>
                      <td colspan="4">Achievement this Month</td>
                    </tr>
                    <tr class="lead-bg">
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                    </tr>
                    <tr class="lead-bg">
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                    </tr>
                    <tr>
                      <td colspan="4">a. Number of persons examined for Glaucoma</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">b. Number of persons identified with Glaucoma</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">c. Number of cases of Glaucoma treated with medication</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">d. Number of Glaucoma surgeries / procedure performed at Sightsavers intervention District</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                  </table>
                </div>



                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td rowspan="3" colspan="4">8. Diabetic Retinopathy (DR)</td>
                      <td colspan="4">Yearly Targets</td>
                      <td colspan="4">Achievement this Month</td>
                    </tr>
                    <tr class="lead-bg">
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                    </tr>
                    <tr class="lead-bg">
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                    </tr>
                    <tr>
                      <td colspan="4">a. Number of persons examined for DR </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">b. Number of persons identified with DR </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">c. Number of laser procedure performed</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">d. Number of Vitrectomy surgery performed at Sightsavers intervention District</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>}</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                </div>


                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td rowspan="3" colspan="4">9. Low Vision and irreversible blindness</td>
                      <td colspan="4">Yearly Targets</td>
                      <td colspan="4">Achievement this Month</td>
                    </tr>
                    <tr class="lead-bg">
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                    </tr>
                    <tr class="lead-bg">
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                    </tr>
                    <tr>
                      <td colspan="4">a. Number of people identified with low vision ( visual acuity less than 6/18 and equal to or better
                        than 3/60 in the better eye with the best correction)</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">b. Number of people received clinical low vision assessment</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">c. Number of people dispensed low vision devices</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">d. Number of people clinically diagnosed irreversible blind (VA &lt;3/60 in the better eye with the
                        best possible correction)</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                </div>


                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td rowspan="3" colspan="4">10. Any other treatment </td>
                      <td colspan="4">Yearly Targets</td>
                      <td colspan="4">Achievement this Month</td>
                    </tr>
                    <tr class="lead-bg">
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                    </tr>
                    <tr class="lead-bg">
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                    </tr>
                    <tr>
                      <td colspan="4">a. Number of any other treatment not included above (if any)</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">b. Number of any other surgery not included above (if any)</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                </div>

                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                      <td class="lead-bg" colspan="2">Please provide a brief analysis of the information given above in terms of</td>
                    </tr>
                    <tr>
                      <td class="label-bg">1. what worked well</td>
                      <td width="400">&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="label-bg">2. what according to you was not satisfactory and you will like to see improved (give reasons for Variation
                        against target)</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="label-bg">3. What challenges were faced and how they were mitigated</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="label-bg">4. what support would you require in order to overcome the challenge</td>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                </div>

                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                      <td class="lead-bg">Feedback Section (this section is to be filled in by Sightsavers programme staff based on the information
                        and analysis given above)</td>
                    </tr>
                    <tr>
                      <td>&nbsp;

                      </td>
                    <tr>
                  </table>
                </div>

                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td rowspan="3" colspan="4">11. System strengthening with Government</td>
                      <td colspan="4">Yearly Targets</td>
                      <td colspan="4">Achievement this Month</td>
                    </tr>
                    <tr class="lead-bg">
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                    </tr>
                    <tr class="lead-bg">
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                    </tr>
                    <tr>
                      <td colspan="4">a. Total eye OPD in Government hospitals during the month (inclusive of District hospital)</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">b. Total eye OPD in Government CHCs, PHCs, Camps and other facilities</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">c. Total number of persons refracted in Government facilities during the month (District hospital)</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">d. Total number of persons refracted in Government facilities during the month (CHCs, PHCs, Camps and
                        other facilities)</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">e. Total number of persons identified with Refractive Error (RE) in Government facilities during the
                        month (District hospital)</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">f. Total number of persons identified with Refractive Error (RE) in Government facilities during the
                        month (CHCs, PHCs, Camps and other facilities)</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">g. Total number of persons prescribed spectacles in Government facilities during the month (District
                        hospital)
                      </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">h. Total number of persons prescribed spectacles in Government facilities during the month (CHCs, PHCs,
                        Camps and other facilities)</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">i. Total number of persons dispensed spectacles in Government facilities during the month (District
                        hospital)
                      </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">j. Total number of persons dispensed spectacles in Government facilities during the month (CHCs, PHCs,
                        Camps and other facilities)</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">k. Total number of persons operated for cataract in Government facilities during the month (District
                        hospital)
                      </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">l. Total number of persons operated for cataract in Government facilities during the month (CHCs, PHCs,
                        Camps and other facilities)</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">m. Total number of persons operated for glaucoma in Government facilities during the month (District
                        hospital)
                      </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">n. Total number of persons operated for glaucoma in Government facilities during the month (CHCs, PHCs,
                        Camps and other facilities)</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">o. Total number of persons operated for Diabetic Retinopathy in Government facilities during the month
                        (District hospital)</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">p. Total number of persons operated for Diabetic Retinopathy in Government facilities during the month
                        (CHCs, PHCs, Camps and other facilities)</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                </div>


                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td>12. Human Resource (Training / capacity building)</td>
                      <td width="150">Yearly Targets</td>
                      <td width="150">This Month</td>
                    </tr>
                    <tr>
                      <td>a. Number of Ophthalmic staff trained in partner / private / other Non Government Organisation (NGO)
                        hospitals (Ophthalmic Assistants, Optometrists (including those who provide LV services) , OT assistants)</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>b. Number of Ophthalmic staff trained in Government hospitals (Ophthalmic Assistants, Optometrists
                        (including those who provide LV services) , OT assistants)</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>c. Number of Govt. Ophthalmologist trained</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>d. Number of Non Govt. Ophthalmologist (Partner/Pvt./other NGO hospital) trained</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>e. Number of ASHA workers trained</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>f. Number of ANMs trained</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>g. Number of Anganwadi workers under ICDS trained</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>h. Number of other specialist trained</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>i. Number of District Programme Managers (DPM) under NPCB programme trained </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>j. Number of state level officials under the blindness control programme trained</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>k. Number of community health volunteers trained</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>k. Number of CBO member trained</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>k. Number of VHNC members trained</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>l. Number of Project Staff Trained or on exposure visit</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>m. Any other category trained</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>n. Number of training held of DPMs under NRHM</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>o. Number of CMEs organised during the month</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                </div>


                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td colspan="2">Please provide a brief analysis of the information given above in terms of</td>
                    </tr>
                    <tr>
                      <td class="label-bg">1. what worked well</td>
                      <td width="400">&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="label-bg">2. what according to you was not satisfactory and you will like to see improved (give reasons for Variation
                        against target)</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="label-bg">3. What challenges were faced and how they were mitigated</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="label-bg">4. what support would you require in order to overcome the challenge</td>
                      <td>&nbsp;</td>
                    </tr>

                  </table>
                </div>

                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                      <td class="lead-bg">Feedback Section (this section is to be filled in by Sightsavers programme staff based on the information
                        and analysis given above)</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                </div>

                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">

                    <tr class="lead-bg">
                      <td>13. Advocacy, networking, Liaison</td>
                      <td width="150">Yearly Targets</td>
                      <td width="150">This Month</td>
                    </tr>
                    <tr>
                      <td>a. Number of meeting/workshop organized for prioritising eye health and fostering integration with
                        wider health system (NRHM)</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>b. Number of meeting held for advocacy for inclusion of Eye health component in the General Health
                        training
                      </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>c. Number of meetings attended at the National/District/ State/block/village level Health Committees</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>d. Number of review meetings of the Government attended at National and state level</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>e. Number of Village Health Sanitation &amp; Nutrition Committee meetings attended by Project staff
                        (NGO/Hospital/Government/Other)
                      </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                </div>

                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td>Please provide a brief analysis of the information given above in terms of</td>
                      <td width="400">&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="label-bg">1. what worked well</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="label-bg">2. what according to you was not satisfactory and you will like to see improved (give reasons for Variation
                        against target)</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="label-bg">3. What challenges were faced and how they were mitigated</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="label-bg">4. what support would you require in order to overcome the challenge</td>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                </div>


                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td>Feedback Section (this section is to be filled in by Sightsavers programme staff based on the information
                        and analysis given above)</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                </div>

                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td>14. IEC/ BCC Activity</td>
                      <td width="150">Yearly Targets</td>
                      <td width="150">This Month</td>
                    </tr>
                    <tr>
                      <td>a. Number of community level awareness events undertaken</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>b. Number of community Based Institutions actively involved in eye health service demand generation</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>c. Number of community members sensitised on eye health issues</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>d. Number of IEC/ BCC material Distributed</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>d1. Posters</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>d2. Booklet/ Leaflets</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>d3. Pamphlets</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>d4. Wall Writing </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>d5. Any other</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>e. Number of health workers provided awareness towards importance of eye health.</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>f. Number of community leaders provided awareness towards importance of eye health.</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>g. Number of other provided Awareness / Sensitize</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                </div>


                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td colspan="2">Please provide a brief analysis of the information given above in terms of</td>
                    </tr>
                    <tr>
                      <td class="label-bg">1. what worked well</td>
                      <td width="400">&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="label-bg">2. what according to you was not satisfactory and you will like to see improved (give reasons for Variation
                        against target)</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="label-bg">3. What challenges were faced and how they were mitigated</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="label-bg">4. what support would you require in order to overcome the challenge</td>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                </div>


                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td>Feedback Section (this section is to be filled in by Sightsavers programme staff based on the information
                        and analysis given above)</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                </div>


                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                      <td class="lead-bg" width="400">Details of case studies/anecdotes: </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="lead-bg" width="400">Details of case studies/anecdotes: </td>
                      <td>&nbsp;</td>
                    </tr>

                  </table>
                </div>

                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                      <td height="60">&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="label-bg">Signature</td>
                      <td class="label-bg">Signature</td>
                      <td class="label-bg">Signature</td>
                    </tr>
                    <tr>
                      <td class="lead-bg"><label>Project Coordinator</label></td>
                      <td class="lead-bg"> <label>Ophthalmologist</label></td>
                      <td class="lead-bg"><label>Project Director</label></td>
                    </tr>
                  </table>
                </div>				

		<?php  } ?>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<!----------------Modal--------------------------------->  
<!---Comment Model----------------->
<div class="modal fade" id="comment-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    	<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header" align="center">
					<h3>Comments</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
					</button>
				</div>
                
                <!-- Begin # DIV Form -->
                <div id="div-forms">
                
                    <!-- Begin # Login Form -->
                    <?php $form_attrib = array('id'=>'comment-form');
					echo form_open();
					?>
		                <div class="modal-body" id="pmpo_modal_response">												
				    		<div class="popup-messages">								
    		                      <div class="direct-chat-messages" id="response_pmpo">
									<?php if(!empty($comments)){ foreach($comments as $comment){ ?>
                                        <div class="chat-box-single-line">
											<abbr class="timestamp"><?php echo $comment->created_date;?></abbr>
                                        </div>

										<div class="direct-chat-msg doted-border">
                                          <div class="direct-chat-info clearfix">
                                            <span class="direct-chat-name pull-left"><?php echo $comment->ss_partners_name;?></span>
                                          </div>
                                          
                                          <!--img alt="message user image" src="<?php //echo PUBLIC_URL;?>img/profile_small.jpg" class="direct-chat-img"-->
                                          <div class="direct-chat-text">
                                            <?php echo $comment->comment;?>
                                          </div>
                                          <div class="direct-chat-info clearfix">
                                            <span class="direct-chat-timestamp pull-right"><?php echo $comment->created_time;?></span>
                                          </div>
                                        </div>
									<?php } }?>
                                   </div>								                                    
                            </div>							
                            <div class="clearfix"></div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Type your Comment</label>
                                    <textarea class="form-control" maxlength="1000" id="pmpo_comments" name="pmpo_comments"></textarea>
                                </div>
                            </div>
                            <div class="clearfix"></div>
        		    	</div>
				        <div class="modal-footer">
                            
                            <div class="row">
                                
                                <div class="col-lf-offset-8 col-lg-4 col-md-offset-8 col-md-4">
                                    <button type="button" class="btn btn-primary btn-lg btn-block" onclick="comment_pmpo()">Submit Comment</button>
                                </div>
                            </div>
				    	    
				        </div>
					<input type="hidden" name="partner_id" id="partner_id" value="<?php echo $session_partner_id;?>"/>
                    <?php echo form_close();?>
                    <!-- End # Login Form -->
                    
                    
                    
                    
                    
                </div>
                <!-- End # DIV Form -->
                
			</div>
		</div>
	</div>
<!----------------PO/PM Comments Model---------------------->
<div class="modal fade" id="po_pm_comments">
<div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          <h3 class="modal-title">PO/PM Comments</h3>
        </div>
        <div class="modal-body">
          <table class="table table-striped" id="tblGrid">
            <thead id="tblHead">
              <tr>
                <th>Name</th>
                <th>Comments</th>
                <th class="text-right">Date</th>
              </tr>
            </thead>
            <tbody>
			<?php if(!empty($comments_pmpo)) { foreach($comments_pmpo as $key_comment => $comment_pmpo){?>
              <tr>
				<td><?php echo $comment_pmpo->ss_partners_name;?></td>
                <td>
					<a href="javascript:void(0)" onclick="show_full_comment(<?php echo $comment_pmpo->ss_comments_id;?>);">
					<?php if(strlen($comment_pmpo->comment) > 10)
					{
						$string = substr($comment_pmpo->comment,0,10).'...';
					}else{
						$string = $comment_pmpo->comment;
					}
					echo $string;
					?>
					</a>
					<input type="hidden" id="comment_<?php echo $comment_pmpo->ss_comments_id;?>" value="<?php echo $comment_pmpo->comment;?>"/>
				</td>
                <td class="text-right"><?php echo $comment_pmpo->created_on;?></td>
              </tr>
			<?php } }?>
            </tbody>
          </table>
          
		</div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary " data-dismiss="modal">Close</button>
        </div>
				
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
<!-----------------Approve Model---------------->
<div class="modal fade" id="approve-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    	<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header" align="center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h3 class="modal-title">Approve</h3>
				</div>
                
                <!-- Begin # DIV Form -->
                <div id="div-forms">
                
                    <!-- Begin # Login Form -->
                    <?php $form_attrib = array('id'=>'login-form');
					echo form_open();
					?>
		                <div class="modal-body">
				    		<div class="col-lg-12">
                                <div class="form-group">
                                    <h3>By Clicking on submit button, this data will be approved.</h3>
                                    
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Submit your feedback(if any)</label>
                                    <textarea class="form-control" id="approve_reason"></textarea>
                                </div>
                            </div>
                            <div class="clearfix"></div>
        		    	</div>
				        <div class="modal-footer">
                            <div class="row">
                                <div class="col-lf-offset-8 col-lg-4 col-md-offset-8 col-md-4">
                                    <button type="button" class="btn btn-primary btn-lg btn-block" onclick="approve('approve');">Submit</button>
                                </div>
                            </div>
				    	    
				        </div>
					<input type="hidden" name="month_from_approve" id="month_from_approve" value="<?php echo $post_month_from;?>"/>	
                    <input type="hidden" name="partner_id_approve" id="partner_id_approve" value="<?php echo $session_partner_id;?>"/>
                    <?php echo form_close();?>
                    <!-- End # Login Form -->
                    
                    
                    
                    
                    
                </div>
                <!-- End # DIV Form -->
                
			</div>
		</div>
	</div>
<!-----------------Disapprove Model---------------->
<div class="modal fade" id="disapprove-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    	<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header" align="center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h3 class="modal-title">Disapprove</h3>
				</div>
                
                <!-- Begin # DIV Form -->
                <div id="div-forms">
                
                    <!-- Begin # Login Form -->
					<?php $form_attrib = array('id'=>'login-form');
					echo form_open();
					?>
		                <div class="modal-body">
				    		<div class="col-lg-12">
                                <div class="form-group">
                                    <h3>By Clicking on submit button, this data will be rejected.</h3>
                                    
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Submit your feedback(if any)</label>
                                    <textarea class="form-control" id="reject_reason"></textarea>
                                </div>
                            </div>
                            <div class="clearfix"></div>
        		    	</div>
				        <div class="modal-footer">
                            
                            <div class="row">
                                
                                <div class="col-lf-offset-8 col-lg-4 col-md-offset-8 col-md-4">
                                    <button type="button" class="btn btn-primary btn-lg btn-block" onclick="approve('reject');">Submit</button>
                                </div>
                            </div>
				    	    
				        </div>
                    <?php echo form_close();?>
                    <!-- End # Login Form -->
              
                </div>
                <!-- End # DIV Form -->
                
			</div>
		</div>
	</div>
<!----------------------------Full Comment Modal----------------->
<div class="modal fade" id="full_comments">
<div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          <h3 class="modal-title">PO/PM Comments</h3>
        </div>
        <div class="modal-body" id="full_comments_show">
                    
		</div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary " data-dismiss="modal">Close</button>
        </div>
				
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script src="https://sightsaversindiamis.custvantage.com/public/js/jquery-2.1.1.js"></script>
<script>

$("#sel1").change(function(){
	var matchvalue = $(this).val();
	var cct = $("input[name='csrf_test_name']").val();
	//alert(cct);
	//var cct = $.cookie("<?php echo $this->config->item("csrf_cookie_name"); ?>");
	post_data ={ state_name: matchvalue,<?php echo $this->security->get_csrf_token_name(); ?> : cct, project_name :'rural eye health'};
	if(matchvalue!=0 || matchvalue !="")
	{
    $.ajax({ 
            url: "<?php echo BASE_URL.'mpr/ajax_partner';?>",
            data: post_data,
            type: 'POST'
        }).done(function(responseData) { //alert(responseData); return false;
		var res = responseData.split('|');
		$("input[name='csrf_test_name']").val(res[1]);
if(res[0]==0)
{
	alert("Invalid request.Please select options properly.");
	return false;
}
else{
	$("#partner1").html("");
	$("#partner1").append(res[0]);
}	
		
		//alert(responseData); return false;
            console.log('Done: ', responseData);
        }).fail(function() {
            console.log('Failed');
        });
	}
});

</script>

<script>
 			$("#submit123").click (function()
			
			{
				var flag = true;
				 var name_pattern = /^[a-zA-Z][a-zA-Z)(.\' ]+$/;
				var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				var num_pattern1 = /^[0-9][0-9)(.\']+$/;
				var space=/^[a-zA-Z0-9]+$/;
				var num_pattern = /^(?!.*-.*-.*-)(?=(?:\d{8,10}$)|(?:(?=.{0,9}$)[^-]*-[^-]*$)|(?:(?=.{10,12}$)[^-]*-[^-]*-[^-]*$)  )[\d-]+$/;
				
				if($('#title11').val() == "")
				{
					$('#title11_error').html('Please select The Title!');
					$('#title11').focus();
					flag = 'false';
					return false;
				}
				else
				{
					$('#title11_error').html('');
				}
				
				if($('#firstname').val() == "")
				{
					$('#firstname_error').html('Please Enter you First Name!');
					$('#firstname').focus();
					flag = 'false';
					return false;
				}
				else if(!name_pattern.test($('#firstname').val()))
				{
			$('#firstname_error').html('Special character and numbers are not allowed');
			$('#firstname').focus();
			flag = 'false';
			return false;
			}
				else
				{
					$('#firstname_error').html('');
				}
				
				if($('#lastname').val() == "")
				{
					$('#lastname_error').html('Please Enter you Last Name!');
					$('#lastname').focus();
					flag = 'false';
					return false;
				}
				else if(!name_pattern.test($('#lastname').val()))
				{
			$('#lastname_error').html('Special character and numbers are not allowed');
			$('#lastname').focus();
			flag = 'false';
			return false;
			}
				else
				{
					$('#lastname_error').html('');
				}
				 if($('#email_id').val()=="")
	{
	
			
 if(flag=="false")
	{
		return false;
	}
	else
	{
		$('#submit123').submit();
	}
}
			});

function comment_pmpo()
{	
	var comments = $("#pmpo_comments").val();
	var partner_id = $("#partner_id").val();
	var month_from = $("#month_from_approve").val();
	<?php if($this->uri->segment(1) == "mpr_reh_redirect"){?>
	var project_name = "rural eye health";
	<?php } elseif($this->uri->segment(1) == "mpr_ueh_redirect"){?>
	var project_name = "urban eye health";
	
	<?php } elseif($this->uri->segment(1) == "mpr_social_redirect"){?>
	var project_name = "Social inclusion";
	
	<?php } elseif($this->uri->segment(1) == "mpr_education_redirect"){?>
	var project_name = "Inclusive education";
	<?php } ?>
	
	var module_name = "main entry";
	if(partner_id != "" && comments != "")
	{
		var url = "<?php echo BASE_URL('create_pmpo_comment');?>";				 
		var csrfHash = $("input[name=csrf_test_name]").val();
		 
		$.post(url,{<?php echo $this->security->get_csrf_token_name(); ?>: csrfHash,comments:comments,partner_id:partner_id,month_from:month_from,project_name:project_name,module_name:module_name} ,function(response){
				
			var htmldata = "";
			var status = "";
			$("input[name=csrf_test_name]").val(response.csrfHash);
			if(response.success ==1)
			{
				htmldata += '<div class="chat-box-single-line"><abbr class="timestamp">'+response.created_on+'</abbr></div><div class="direct-chat-msg doted-border"><div class="direct-chat-info clearfix"><span class="direct-chat-name pull-left">'+response.partner_name+'</span></div><div class="direct-chat-text">'+response.comments+'</div><div class="direct-chat-info clearfix"><span class="direct-chat-timestamp pull-right">'+response.created_time+'</span></div></div>';
				
				
				$("#pmpo_comments").val("");
				$("#response_pmpo").append(htmldata);
			}else{
				$("#pmpo_comments").val("");
				alert("Data is not submitted");
			}			
		},"json");
	}
}
function approve(approvestatus)
{
	var month_from_approve = $("#month_from_approve").val();
	var partner_id_approve = $("#partner_id_approve").val();
	<?php if($this->uri->segment(1) == "mpr_reh_redirect"){?>
	var project_name = "rural eye health";
	<?php } elseif($this->uri->segment(1) == "mpr_ueh_redirect"){?>
	var project_name = "urban eye health";
	<?php } elseif($this->uri->segment(1) == "mpr_social_redirect"){?>
	var project_name = "Social inclusion";
	<?php } elseif($this->uri->segment(1) == "mpr_education_redirect"){?>
	var project_name = "Inclusive education";
	<?php }?>
	var module_name = "main entry";
	var reason = "";
	var approve_flag = "";
	if($("#approve_reason").val() != "")
	{
		reason = $("#approve_reason").val();		
	}
	else if($("#reject_reason").val() != "")
	{
		reason = $("#reject_reason").val();		
	}	
	if(approvestatus == "approve")
	{
		approve_flag = 1;
	}else if(approvestatus == "reject")
	{
		approve_flag = 0;
		 
	} 
	if(partner_id_approve != "" && month_from_approve != "")
	{
		var url = "<?php echo BASE_URL('approval');?>";				 
		var csrfHash = $("input[name=csrf_test_name]").val();
		
		//$.post(url,{<?php echo $this->security->get_csrf_token_name(); ?>:csrfHash,month_from_approve:month_from_approve,partner_id_approve:partner_id_approve,project_name:project_name,module_name:module_name,reason:reason,approve_flag:approve_flag} ,function(response){
		//$.post(url,{month_from_approve:month_from_approve,partner_id_approve:partner_id_approve,project_name:project_name,module_name:module_name,reason:reason,approve_flag:approve_flag} ,function(response){
		$.post(url,{<?php echo $this->security->get_csrf_token_name(); ?>: csrfHash,month_from_approve:month_from_approve,partner_id_approve:partner_id_approve,project_name:project_name,module_name:module_name,reason:reason,approve_flag:approve_flag} ,function(response){
	
			var htmldata = "";
			var status = "";
			$("input[name=csrf_test_name]").val(response.csrfHash);

			if(response.success == 1)
			{
				if(approve_flag == 1)
				{
					$("#approve_reason").val("");
					alert("Successfully Approved");					
				}else if(approve_flag == 0){
					$("#reject_reason").val("");
					alert("Rejected");
				}				
			}else{
				$("#approve_reason").val("");
				$("#reject_reason").val("");
				alert("No data found");
			}			
		},"json");
	}
}
function show_full_comment(id)
{
	if(id != "")
	{
		$("#full_comments_show").html("");
		$("#full_comments_show").html($("#comment_"+id).val());
		$("#full_comments").modal({show: 'True'}); 
	}
}

</script>