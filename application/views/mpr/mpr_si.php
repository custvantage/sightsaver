<div class="row wrapper border-bottom white-bg page-heading ng-scope">
  <div class="col-lg-10">
    <h2>Social Enclusion</h2>
  </div>
</div>
<?php  //if(isset($month_id)) { echo "<pre>";print_r($month_id); } ?>
<div class="wrapper wrapper-content animated fadeIn ng-scope" ng-init="init()">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox">
    
        <div class="ibox-content">
			<div class="row">
				<div class="col-lg-12">
				<?php $form_attr = array('id'=>'mpr_si'); echo form_open('mpr_si',$form_attr);?>
					<div class="row">
						<div class="col-lg-12">
							  <h5>Main Report</h5>
						</div>
						<div class="clearfix"></div>
						<div class="col-lg-3">
								<div class="form-group">
								  <select class="form-control" name="state_name" id="sel1" onchange="getPartner();">
									<option value="" selected="true" disabled="disabled">Select State</option>
									<optgroup>
										<option value="all_state">All States Data</option>
									</optgroup>
									<optgroup>
									<?php if(isset($state_fetch) && $state_fetch!=null)
									{
										foreach($state_fetch as $state_fet)
										{   ?>
										<option value="<?php  echo $state_fet->ss_states_name;?>"><?php  echo $state_fet->ss_states_name;  ?></option>
									<?php   }} ?>
									</optgroup>
									
								  </select>
								  <p class="text-danger ng-binding ng-hide" aria-hidden="true" id="state_error"></p>
								  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />

						
								</div>
						</div>
						<div class="col-lg-3">
							
								<div class="form-group">
								  <select class="form-control" name="partner_name_post" id="partner1">
									<option  selected="true" disabled="disabled">Select Partner</option>
									<optgroup>
										<option>All Partners Data</option>
									</optgroup>
									<optgroup>
									
									</optgroup>
									
								  </select>
								</div>
							
						</div>
						<div class="col-lg-4">
							<div class="row">
								<div class="col-lg-6">
								<input type="text" name="month_from1" id="month_from" class="form-control datepicker" placeholder="Start Date" onchange="date_fill()">
								<p class="text-danger ng-binding ng-hide" aria-hidden="true" id="month_from_error"><?php echo form_error('month_data');?></p>
								</div>
								<div class="col-lg-6">
								<input type="text" name="month_from_last1" id="month_from_last" class="form-control datepicker" placeholder="Start Date Last" onchange="date_fill()">
								<p class="text-danger ng-binding ng-hide" aria-hidden="true" id="month_from_last_error"></p>
								</div>
							</div>
						</div>
						<div class="col-md-2">
			 
							<button class="btn btn-sm btn-primary btn-block" type="button" id="submit123" onclick="submit_form();">
							Get Data
						  </button>
						</div>					
					</div>
				 <div class="clearfix"></div>
			</div>
		  </div>
		  <hr>
		  <div class="clearfix"></div>
		   <?php echo form_close();?>
	
	<?php if(isset($partner_summary_data) && !empty($partner_summary_data)) 
	      {   
	       ?>
          <div class="row m-t-lg" ng-show="hasData">
            <div class="report-table" id="print-data">
              <div class="text-center">
                <h3 class="table-heading">RURAL EYE HEALTH PROGRAMME</h3>
                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td colspan="11">Monthly Partner Reporting Format
                        <button class="btn btn-default btn-xs pull-right text-default print-btn" ng-click="print()"><i class="fa fa-print"></i> Print</button>
                      </td>
                    </tr>
					    
                    <tr>  
                      <td class="label-bg"><label>Organisation Name:</label></td>
                      <td><?php if(isset($partner_summary_data) && $partner_summary_data!=null) { echo $partner_summary_data->ss_partners_name; } ?></td>
                      <td class="label-bg" colspan="2"><label>Name of the State:</label></td>
                      <td colspan="4"><?php if(isset($partner_summary_data) && $partner_summary_data!=null) { echo $partner_summary_data->ss_states_name; } ?></td>
                    </tr>
                    <tr>
                      <td class="label-bg"><label>District Name:</label></td>
                      <td><?php if(isset($partner_summary_data) && $partner_summary_data!=null) { echo $partner_summary_data->ss_district_name; } ?></td>
                      <td class="label-bg" colspan="2"><label>Reporting Month:</label></td>
                      <td colspan="4"><?php if(isset($partner_summary_data) && $partner_summary_data!=null) { echo date('M', strtotime($partner_summary_data->ss_mpr_report_month)); } ?></td>
                    </tr>
                    <tr>
                      <td class="label-bg"><label>Programme Start Date</label></td>
                      <td><?php if(isset($partner_summary_data) && $partner_summary_data!=null) {  } ?></td>
                      <td class="label-bg" colspan="2"><label>Reporting Year:</label></td>
                      <td colspan="4"><?php if(isset($partner_summary_data) && $partner_summary_data!=null) { echo date('Y', strtotime($partner_summary_data->ss_mpr_report_month)); } ?></td>
                    </tr>
                    <tr>
                      <td class="label-bg"><label>Demo/Scale up (to be filled by Sightsavers Staff)</label></td>
                      <td><?php if(isset($partner_summary_data) && $partner_summary_data!=null) { } ?></td>
                      <td colspan="2" class="label-bg"><label>District Budget (to be filled by Sightsavers Staff)</label></td>
                      <td colspan="4"><?php if(isset($partner_summary_data) && $partner_summary_data!=null) { 
					   } ?></td>
                    </tr>
                    <tr>
                      <td class="label-bg"><label>I. Total Number of districts covered by the hospital / organisation</label></td>
                      <td colspan="9"><?php if(isset($partner_summary_data) && $partner_summary_data!=null) {   
					  echo $partner_summary_data->ss_mpr_district_cover_hospital; } ?></td>
                    </tr>
                    <tr>
                      <td class="label-bg"><label>II. Total number of districts covered under Sightsavers programme</label></td>
                      <td colspan="9"><?php if(isset($partner_summary_data) && $partner_summary_data!=null) {   
					  echo $partner_summary_data->ss_mpr_district_sightsaver; } ?></td>
                    </tr>
                  </table>
                </div>
            
			   <?php 
				 }
			   $section_id = "";
			   if(isset($merge) and !empty($merge))
			   { 
				   foreach($merge as $key_metrics => $metrics)
				   {   
				 if($metrics->ss_section_layout_section_col_count == 1 && $metrics->ss_section_layout_analysis_description == ""){  ?>
                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td><?php echo $metrics->ss_section_layout_section_name;?></td>
                      <td width="150">Yearly Targets</td>
                      <td width="150">This month</td>
                    </tr>
					
					 <?php 
					 $description=explode('#', $metrics->ss_metric_master_description);
					 $metric_id_one=explode('#', $metrics->ss_metric_master_id);
					 $data_detail=array_combine($metric_id_one,$description);
					 foreach($data_detail as $key => $metric_dis)
					 {
					  ?>
                    <tr>
                      <td><?php echo($metric_dis); ?></td>
                      <td> - </td>
					  
					  
                      <td><?php $one_col_val = $this->metricdata->getOneColVal_mpr($key,$month_id[0],$month_id[1],$month_id[2],$month_id[3]);
					  if(!empty($one_col_val)){ echo $one_col_val;}?></td>
					  </tr>
					   <?php }  ?>
                  </table>
                </div>
					 <?php } else if($metrics->ss_section_layout_section_col_count == 4 && $metrics->ss_section_layout_analysis_description == ""){?>

                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td rowspan="3" colspan="4"><?php echo $metrics->ss_section_layout_section_name;?></td>
                      <td colspan="4">Yearly Targets</td>
                      <td colspan="4">Achievement this Month</td>
                    </tr>
                    <tr class="lead-bg">
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                    </tr>
                    <tr class="lead-bg">
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                    </tr>
					 <?php 
					 $description=explode('#', $metrics->ss_metric_master_description);
					 $metric_id_one=explode('#', $metrics->ss_metric_master_id);
					 
						if(!empty($merge2))
						{
							$metric_id_yearly=explode('#', $merge2[$key_metrics]->ss_metric_master_id);
							$description_yearly=explode('#', $merge2[$key_metrics]->ss_metric_master_description);
						}
						else
					    {
							$metric_id_yearly=array();
							$description_yearly =array();
					    }
				 
					 $all_keys = array_merge($metric_id_one,$metric_id_yearly);
					 $all_desc = array_merge($description,$description_yearly);
					 $data_detail=array_combine($metric_id_one,$description);
					 $newArray = array_keys($data_detail);
					 
					foreach($data_detail as $key1 => $metric_dis_four)
						{
							$key2 = array_search($key1, $newArray); 
							echo "<tr>";
							echo '<td colspan="4">'.$metric_dis_four.'</td>';
							
					  ?>
                      
                      <td><?php if(!empty($metric_id_yearly)) { $four_col_data = $this->metricdata->getFourColVal_mpr($metric_id_yearly[$key2],$month_id[0],$month_id[1],$month_id[2],$month_id[3]);if(!empty($four_col_data->ss_mpr_four_column_data_value_men)){ echo $four_col_data->ss_mpr_four_column_data_value_men;} else { echo "-";}}else { echo "-";}?></td>
                      <td><?php if(!empty($metric_id_yearly)) { $four_col_data = $this->metricdata->getFourColVal_mpr($metric_id_yearly[$key2],$month_id[0],$month_id[1],$month_id[2],$month_id[3]);if(!empty($four_col_data->ss_mpr_four_column_data_value_women)){ echo $four_col_data->ss_mpr_four_column_data_value_women;} else { echo "-";}}else { echo "-";}?></td>
                      <td><?php if(!empty($metric_id_yearly)) { $four_col_data = $this->metricdata->getFourColVal_mpr($metric_id_yearly[$key2],$month_id[0],$month_id[1],$month_id[2],$month_id[3]);if(!empty($four_col_data->ss_mpr_four_column_data_value_boys)){ echo $four_col_data->ss_mpr_four_column_data_value_boys;} else { echo "-";}}else { echo "-";}?></td>
                      <td><?php if(!empty($metric_id_yearly)) { $four_col_data = $this->metricdata->getFourColVal_mpr($metric_id_yearly[$key2],$month_id[0],$month_id[1],$month_id[2],$month_id[3]);if(!empty($four_col_data->ss_mpr_four_column_data_value_girls)){ echo $four_col_data->ss_mpr_four_column_data_value_girls;} else { echo "-";}}else { echo "-";}?></td>
							
					 
                      <td ><?php   $four_col_data = $this->metricdata->getFourColVal_mpr($key1,$month_id[0],$month_id[1],$month_id[2],$month_id[3]); 
					  if(!empty($four_col_data->ss_mpr_four_column_data_value_men)){ echo $four_col_data->ss_mpr_four_column_data_value_men;} else { echo "-";}?></td>
                      <td><?php $four_col_data = $this->metricdata->getFourColVal_mpr($key1,$month_id[0],$month_id[1],$month_id[2],$month_id[3]);if(!empty($four_col_data->ss_mpr_four_column_data_value_women)){ echo $four_col_data->ss_mpr_four_column_data_value_women;} else { echo "-";}?></td>
                      <td><?php $four_col_data = $this->metricdata->getFourColVal_mpr($key1,$month_id[0],$month_id[1],$month_id[2],$month_id[3]);if(!empty($four_col_data->ss_mpr_four_column_data_value_boys)){ echo $four_col_data->ss_mpr_four_column_data_value_boys;} else { echo "-";}?></td>
                      <td><?php $four_col_data = $this->metricdata->getFourColVal_mpr($key1,$month_id[0],$month_id[1],$month_id[2],$month_id[3]);if(!empty($four_col_data->ss_mpr_four_column_data_value_girls)){ echo $four_col_data->ss_mpr_four_column_data_value_girls;} else { echo "-";}?></td>
					
                    </tr>
						<?php } ?>
                  </table>
                </div>
					 <?php } else if($metrics->ss_section_layout_analysis_description != ""){?>
               

                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td colspan="2"><?php echo $metrics->ss_section_layout_section_name;?></td>
                    </tr>
					 <?php 
					 $description_analysis=explode('#', $metrics->ss_metric_master_description);
                     $metric_id_analisys=explode('#', $metrics->ss_metric_master_id);
					 $data_detail_analysis=array_combine($metric_id_analisys,$description_analysis);
					 foreach($data_detail_analysis as $key => $metric_analysis)
						{
					  ?>
                    <tr>
                      <td class="label-bg"><?php echo $metric_analysis;?></td>
                      <td width="400"><?php $analysis_data = $this->metricdata->getAnalysisVal_mpr($key,$month_id[0],$month_id[1],$month_id[2],$month_id[3]);if(!empty($analysis_data)){ echo $analysis_data;}?></td>
                    </tr>
						<?php } ?>

                  </table>
                </div>
					 <?php // } $section_id = $metrics->ss_section_layout_ss_section_layout_id; }else{?>
						 
					<?php } } ?>
                   <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td>Feedback Section (this section is to be filled in by Sightsavers programme staff based on the information
                        and analysis given above)</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                </div>


                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                      <td class="lead-bg" width="400">Details of case studies/anecdotes: </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="lead-bg" width="400">Details of case studies/anecdotes: </td>
                      <td>&nbsp;</td>
                    </tr>

                  </table>
                </div>

                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                      <td height="60">&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="label-bg">Signature</td>
                      <td class="label-bg">Signature</td>
                      <td class="label-bg">Signature</td>
                    </tr>
                    <tr>
                      <td class="lead-bg"><label>Project Coordinator</label></td>
                      <td class="lead-bg"> <label>Ophthalmologist</label></td>
                      <td class="lead-bg"><label>Project Director</label></td>
                    </tr>
                  </table>
                </div>
	   <?php } else {  ?>
	
                 <div class="row m-t-lg" ng-show="hasData">
            <div class="report-table" id="print-data">
              <div class="text-center">
                <h3 class="table-heading">RURAL EYE HEALTH PROGRAMME</h3>
                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td colspan="11">Monthly Partner Reporting Format
                        <button class="btn btn-default btn-xs pull-right text-default print-btn" ng-click="print()"><i class="fa fa-print"></i> Print</button>
                      </td>
                    </tr>
					    
                    <tr>  
                      <td class="label-bg"><label>Organisation Name:</label></td>
                      <td></td>
                      <td class="label-bg" colspan="2"><label>Name of the State:</label></td>
                      <td colspan="4"></td>
                    </tr>
                    <tr>
                      <td class="label-bg"><label>District Name:</label></td>
                      <td></td>
                      <td class="label-bg" colspan="2"><label>Reporting Month:</label></td>
                      <td colspan="4"></td>
                    </tr>
                    <tr>
                      <td class="label-bg"><label>Programme Start Date</label></td>
                      <td></td>
                      <td class="label-bg" colspan="2"><label>Reporting Year:</label></td>
                      <td colspan="4"></td>
                    </tr>
                    <tr>
                      <td class="label-bg"><label>Demo/Scale up (to be filled by Sightsavers Staff)</label></td>
                      <td></td>
                      <td colspan="2" class="label-bg"><label>District Budget (to be filled by Sightsavers Staff)</label></td>
                      <td colspan="4"></td>
                    </tr>
                    <tr>
                      <td class="label-bg"><label>I. Total Number of districts covered by the hospital / organisation</label></td>
                      <td colspan="9"><?php if(isset($partner_summary_data) && $partner_summary_data!=null) {   
					  echo $partner_summary_data->ss_mpr_district_cover_hospital; } ?></td>
                    </tr>
                    <tr>
                      <td class="label-bg"><label>II. Total number of districts covered under Sightsavers programme</label></td>
                      <td colspan="9"><?php if(isset($partner_summary_data) && $partner_summary_data!=null) {   
					  echo $partner_summary_data->ss_mpr_district_sightsaver; } ?></td>
                    </tr>
</table>
                </div>

                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td>1.Vision Centres /Camps</td>
                      <td width="150">Yearly Targets</td>
                      <td width="150">This month</td>
                    </tr>
                    <tr>
                      <td>a. Number of Vision Centres (VC) operational in the district Partner supported by Sightsavers</td>
                      <td> - </td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>b. Number of Mobile Vision Centres operational in the district supported by Sightsavers</td>
                      <td> - </td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>c. Number of Vision Centres (VC) operational in the district setup by Government</td>
                      <td> - </td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>d. Number of Mobile Vision Centres operational in the district setup by Government</td>
                      <td> - </td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>e. Number of Camps conducted during the month</td>
                      <td>&nbsp;</td>
                      <td></td>
                    </tr>
					
                  </table>
                </div>


                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td rowspan="3" colspan="4">2. OPD/Screening</td>
                      <td colspan="4">Yearly Targets</td>
                      <td colspan="4">Achievement this Month</td>
                    </tr>
                    <tr class="lead-bg">
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                    </tr>
                    <tr class="lead-bg">
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                    </tr>
                    <tr>
                      <td colspan="4">a. Total OPD for the month (this should be the total OPD figures for the entire partner hospital across
                        all districts irrespective whether it is supported by Sightsavers or not)</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">b. Number of Screening at Vision Centre for the programme</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">c. Number of Screening at Camps for the programme</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> </td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">d. Number of OPD at Secondary / tertiary Eye Care Centre for the Sightsavers supported programme district
                      </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                  </table>
                </div>

                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td rowspan="3" colspan="4">3. Referral/ Reported</td>
                      <td colspan="4">Yearly Targets</td>
                      <td colspan="4">Achievement this Month</td>
                    </tr>
                    <tr class="lead-bg">
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                    </tr>
                    <tr class="lead-bg">
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                    </tr>
                    <tr>
                      <td colspan="4">a. Number of persons referred from Vision Centre to base hospital</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">b. Number of persons referred from camps to base hospital</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">c. Number of persons referred to Govt. hospital/ other hospitals</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">d. Total number of referrals undertaken by ASHA/ANM during the month</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                    </tr>
                    <tr>
                      <td colspan="4">d1. Number of persons referred to Vision Centres by ASHA / ANM</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">d2. Number of persons referred to camps by by ASHA / ANM</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">d3. Number of persons referred directly to base hospital by ASHA/ANM </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">d4. Number of persons referred directly to Govt. Hospital by ASHA/ANM</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">e. Number of persons reported at base hospital from Vision Centre</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">f. Number of persons reported at base hospital from camps</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">g. Number of persons reported at Govt. hospital/ other hospitals</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                  </table>
                </div>


                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td rowspan="3" colspan="4">4. Refraction</td>
                      <td colspan="4">Yearly Targets</td>
                      <td colspan="4">Achievement this Month</td>
                    </tr>
                    <tr class="lead-bg">
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                    </tr>
                    <tr class="lead-bg">
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                    </tr>
                    <tr>
                      <td colspan="4">a. Total Number of refraction undertaken</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>-</td>
                      <td>-</td>
                      <td>-</td>
                      <td>-</td>
                    </tr>
                    <tr>
                      <td colspan="4">a1. Number of persons refracted at the Vision Centre</td>
                      <td>  </td>
                      <td>  </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">a2. Number of persons refracted at the camps</td>
                      <td>  </td>
                      <td>  </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">a3. Number of persons refracted at the base hospitals</td>
                      <td>  </td>
                      <td>  </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">b. Number of persons identified with Refractive Error (RE) at the Vision Centre</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">c. Number of persons identified with Refractive Error (RE) at the camps</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">d. Number of persons identified with RE at base hospitals</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                  </table>
                </div>



                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td rowspan="3" colspan="4">5. Spectacles</td>
                      <td colspan="4">Yearly Targets</td>
                      <td colspan="4">Achievement this Month</td>
                    </tr>
                    <tr class="lead-bg">
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                    </tr>
                    <tr class="lead-bg">
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                    </tr>
                    <tr>
                      <td colspan="4">a. Number of persons with RE, prescribed spectacles at the Vision Centre</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">b. Number of persons with RE, prescribed spectacles at the camps</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">c. Number of persons with RE, prescribed spectacles at base hospital</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">d. Number of persons dispensed spectacles free at Vision Centre</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">e. Number of persons dispensed spectacles free at camp</td>
                      <td>  </td>
                      <td>  </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">f. Number of persons dispensed spectacles free at base hospital</td>
                      <td>  </td>
                      <td>  </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">g. Number of persons prescribed spectacles who purchased spectacles at Vision Centre (subsidised rate)</td>
                      <td>  </td>
                      <td>  </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">h. Number of persons prescribed spectacles who purchased spectacles at camp (subsidised rate)</td>
                      <td>  </td>
                      <td>  </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">i. Number of persons prescribed spectacles who purchased spectacles at base hospital (subsidised rate)</td>
                      <td> </td>
                      <td>  </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">j. Number of persons prescribed spectacles who purchased spectacles at Vision Centre (full rate)</td>
                      <td> </td>
                      <td>  </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">k. Number of persons prescribed spectacles who purchased spectacles at camp (full rate)</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">l. Number of persons prescribed spectacles who purchased spectacles at base hospital (full rate)</td>
                      <td>  </td>
                      <td>  </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                  </table>
                </div>



                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td rowspan="3" colspan="4">6. Cataract</td>
                      <td colspan="4">Yearly Targets</td>
                      <td colspan="4">Achievement this Month</td>
                    </tr>
                    <tr class="lead-bg">
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                    </tr>
                    <tr class="lead-bg">
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                    </tr>
                    <tr>
                      <td colspan="4">a. Number of persons referred to base hospital by ASHA / ANM for cataract </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">b. Number of persons referred to VCs by ASHA / ANM for cataract </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">c. Number of persons referred to camp by ASHA / ANM for cataract </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">d. Number of persons identified with Cataract and referred to base hospital from VCs</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">e. Number of persons identified with Cataract and referred to base hospital from Camp</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">f. Number of persons identified with Cataract and referred by ASHA / ANM who reported to base hospital</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">g. Number of persons identified with cataract and referred from Vision Centre and camps who reported
                        to base hospital</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">h. Number of Cataract surgeries undertaken at Sightsavers intervention District (by partner or other
                        hospitals)
                      </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">i. Number of Cataract surgeries undertaken by the partner</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">j. Number of Cataract surgeries undertaken by partner calculated after Attribution</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">k. Number of Cataract operations performed free of cost</td>
                      <td>  </td>
                      <td>  </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">l. Number of Cataract operations performed subsidised</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">m. Number of Cataract operations performed paid</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">n. Number of Cataract surgeries performed paid calculated after Attribution</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                    </tr>
                    <tr>
                      <td colspan="4">o. Number of Cataract surgeries supported by Sightsavers</td>
                      <td>  </td>
                      <td>  </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">p. Number of Cataract surgeries supported by DBCS</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">q. Number of Cataract surgeries supported by other sources</td>
                      <td>  </td>
                      <td>  </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">r. Number of persons who came for the first follow-up after Cataract Operations to base hospital</td>
                      <td> </td>
                      <td> </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                  </table>
                </div>


                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td rowspan="3" colspan="4">7. Glaucoma </td>
                      <td colspan="4">Yearly Targets</td>
                      <td colspan="4">Achievement this Month</td>
                    </tr>
                    <tr class="lead-bg">
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                    </tr>
                    <tr class="lead-bg">
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                    </tr>
                    <tr>
                      <td colspan="4">a. Number of persons examined for Glaucoma</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">b. Number of persons identified with Glaucoma</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">c. Number of cases of Glaucoma treated with medication</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">d. Number of Glaucoma surgeries / procedure performed at Sightsavers intervention District</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                  </table>
                </div>



                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td rowspan="3" colspan="4">8. Diabetic Retinopathy (DR)</td>
                      <td colspan="4">Yearly Targets</td>
                      <td colspan="4">Achievement this Month</td>
                    </tr>
                    <tr class="lead-bg">
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                    </tr>
                    <tr class="lead-bg">
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                    </tr>
                    <tr>
                      <td colspan="4">a. Number of persons examined for DR </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">b. Number of persons identified with DR </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="4">c. Number of laser procedure performed</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td></td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">d. Number of Vitrectomy surgery performed at Sightsavers intervention District</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>}</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                </div>


                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td rowspan="3" colspan="4">9. Low Vision and irreversible blindness</td>
                      <td colspan="4">Yearly Targets</td>
                      <td colspan="4">Achievement this Month</td>
                    </tr>
                    <tr class="lead-bg">
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                    </tr>
                    <tr class="lead-bg">
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                    </tr>
                    <tr>
                      <td colspan="4">a. Number of people identified with low vision ( visual acuity less than 6/18 and equal to or better
                        than 3/60 in the better eye with the best correction)</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">b. Number of people received clinical low vision assessment</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">c. Number of people dispensed low vision devices</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">d. Number of people clinically diagnosed irreversible blind (VA &lt;3/60 in the better eye with the
                        best possible correction)</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                </div>


                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td rowspan="3" colspan="4">10. Any other treatment </td>
                      <td colspan="4">Yearly Targets</td>
                      <td colspan="4">Achievement this Month</td>
                    </tr>
                    <tr class="lead-bg">
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                    </tr>
                    <tr class="lead-bg">
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                    </tr>
                    <tr>
                      <td colspan="4">a. Number of any other treatment not included above (if any)</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">b. Number of any other surgery not included above (if any)</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                </div>

                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                      <td class="lead-bg" colspan="2">Please provide a brief analysis of the information given above in terms of</td>
                    </tr>
                    <tr>
                      <td class="label-bg">1. what worked well</td>
                      <td width="400">&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="label-bg">2. what according to you was not satisfactory and you will like to see improved (give reasons for Variation
                        against target)</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="label-bg">3. What challenges were faced and how they were mitigated</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="label-bg">4. what support would you require in order to overcome the challenge</td>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                </div>

                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                      <td class="lead-bg">Feedback Section (this section is to be filled in by Sightsavers programme staff based on the information
                        and analysis given above)</td>
                    </tr>
                    <tr>
                      <td>&nbsp;

                      </td>
                    <tr>
                  </table>
                </div>

                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td rowspan="3" colspan="4">11. System strengthening with Government</td>
                      <td colspan="4">Yearly Targets</td>
                      <td colspan="4">Achievement this Month</td>
                    </tr>
                    <tr class="lead-bg">
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                      <td colspan="2">Adult</td>
                      <td colspan="2">Child</td>
                    </tr>
                    <tr class="lead-bg">
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                      <td>Men</td>
                      <td>Women</td>
                      <td>Boys</td>
                      <td>Girls</td>
                    </tr>
                    <tr>
                      <td colspan="4">a. Total eye OPD in Government hospitals during the month (inclusive of District hospital)</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">b. Total eye OPD in Government CHCs, PHCs, Camps and other facilities</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">c. Total number of persons refracted in Government facilities during the month (District hospital)</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">d. Total number of persons refracted in Government facilities during the month (CHCs, PHCs, Camps and
                        other facilities)</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">e. Total number of persons identified with Refractive Error (RE) in Government facilities during the
                        month (District hospital)</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">f. Total number of persons identified with Refractive Error (RE) in Government facilities during the
                        month (CHCs, PHCs, Camps and other facilities)</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">g. Total number of persons prescribed spectacles in Government facilities during the month (District
                        hospital)
                      </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">h. Total number of persons prescribed spectacles in Government facilities during the month (CHCs, PHCs,
                        Camps and other facilities)</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">i. Total number of persons dispensed spectacles in Government facilities during the month (District
                        hospital)
                      </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">j. Total number of persons dispensed spectacles in Government facilities during the month (CHCs, PHCs,
                        Camps and other facilities)</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">k. Total number of persons operated for cataract in Government facilities during the month (District
                        hospital)
                      </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">l. Total number of persons operated for cataract in Government facilities during the month (CHCs, PHCs,
                        Camps and other facilities)</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">m. Total number of persons operated for glaucoma in Government facilities during the month (District
                        hospital)
                      </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">n. Total number of persons operated for glaucoma in Government facilities during the month (CHCs, PHCs,
                        Camps and other facilities)</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">o. Total number of persons operated for Diabetic Retinopathy in Government facilities during the month
                        (District hospital)</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">p. Total number of persons operated for Diabetic Retinopathy in Government facilities during the month
                        (CHCs, PHCs, Camps and other facilities)</td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                </div>


                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td>12. Human Resource (Training / capacity building)</td>
                      <td width="150">Yearly Targets</td>
                      <td width="150">This Month</td>
                    </tr>
                    <tr>
                      <td>a. Number of Ophthalmic staff trained in partner / private / other Non Government Organisation (NGO)
                        hospitals (Ophthalmic Assistants, Optometrists (including those who provide LV services) , OT assistants)</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>b. Number of Ophthalmic staff trained in Government hospitals (Ophthalmic Assistants, Optometrists
                        (including those who provide LV services) , OT assistants)</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>c. Number of Govt. Ophthalmologist trained</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>d. Number of Non Govt. Ophthalmologist (Partner/Pvt./other NGO hospital) trained</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>e. Number of ASHA workers trained</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>f. Number of ANMs trained</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>g. Number of Anganwadi workers under ICDS trained</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>h. Number of other specialist trained</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>i. Number of District Programme Managers (DPM) under NPCB programme trained </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>j. Number of state level officials under the blindness control programme trained</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>k. Number of community health volunteers trained</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>k. Number of CBO member trained</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>k. Number of VHNC members trained</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>l. Number of Project Staff Trained or on exposure visit</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>m. Any other category trained</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>n. Number of training held of DPMs under NRHM</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>o. Number of CMEs organised during the month</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                </div>


                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td colspan="2">Please provide a brief analysis of the information given above in terms of</td>
                    </tr>
                    <tr>
                      <td class="label-bg">1. what worked well</td>
                      <td width="400">&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="label-bg">2. what according to you was not satisfactory and you will like to see improved (give reasons for Variation
                        against target)</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="label-bg">3. What challenges were faced and how they were mitigated</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="label-bg">4. what support would you require in order to overcome the challenge</td>
                      <td>&nbsp;</td>
                    </tr>

                  </table>
                </div>

                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                      <td class="lead-bg">Feedback Section (this section is to be filled in by Sightsavers programme staff based on the information
                        and analysis given above)</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                </div>

                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">

                    <tr class="lead-bg">
                      <td>13. Advocacy, networking, Liaison</td>
                      <td width="150">Yearly Targets</td>
                      <td width="150">This Month</td>
                    </tr>
                    <tr>
                      <td>a. Number of meeting/workshop organized for prioritising eye health and fostering integration with
                        wider health system (NRHM)</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>b. Number of meeting held for advocacy for inclusion of Eye health component in the General Health
                        training
                      </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>c. Number of meetings attended at the National/District/ State/block/village level Health Committees</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>d. Number of review meetings of the Government attended at National and state level</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>e. Number of Village Health Sanitation &amp; Nutrition Committee meetings attended by Project staff
                        (NGO/Hospital/Government/Other)
                      </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                </div>

                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td>Please provide a brief analysis of the information given above in terms of</td>
                      <td width="400">&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="label-bg">1. what worked well</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="label-bg">2. what according to you was not satisfactory and you will like to see improved (give reasons for Variation
                        against target)</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="label-bg">3. What challenges were faced and how they were mitigated</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="label-bg">4. what support would you require in order to overcome the challenge</td>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                </div>


                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td>Feedback Section (this section is to be filled in by Sightsavers programme staff based on the information
                        and analysis given above)</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                </div>

                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td>14. IEC/ BCC Activity</td>
                      <td width="150">Yearly Targets</td>
                      <td width="150">This Month</td>
                    </tr>
                    <tr>
                      <td>a. Number of community level awareness events undertaken</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>b. Number of community Based Institutions actively involved in eye health service demand generation</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>c. Number of community members sensitised on eye health issues</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>d. Number of IEC/ BCC material Distributed</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>d1. Posters</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>d2. Booklet/ Leaflets</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>d3. Pamphlets</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>d4. Wall Writing </td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>d5. Any other</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>e. Number of health workers provided awareness towards importance of eye health.</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>f. Number of community leaders provided awareness towards importance of eye health.</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>g. Number of other provided Awareness / Sensitize</td>
                      <td> - </td>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                </div>


                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td colspan="2">Please provide a brief analysis of the information given above in terms of</td>
                    </tr>
                    <tr>
                      <td class="label-bg">1. what worked well</td>
                      <td width="400">&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="label-bg">2. what according to you was not satisfactory and you will like to see improved (give reasons for Variation
                        against target)</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="label-bg">3. What challenges were faced and how they were mitigated</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="label-bg">4. what support would you require in order to overcome the challenge</td>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                </div>


                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr class="lead-bg">
                      <td>Feedback Section (this section is to be filled in by Sightsavers programme staff based on the information
                        and analysis given above)</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                </div>


                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                      <td class="lead-bg" width="400">Details of case studies/anecdotes: </td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="lead-bg" width="400">Details of case studies/anecdotes: </td>
                      <td>&nbsp;</td>
                    </tr>

                  </table>
                </div>

                <div class="table-responsive">
                  <table class="table table-bordered table-condensed text-left" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                      <td height="60">&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="label-bg">Signature</td>
                      <td class="label-bg">Signature</td>
                      <td class="label-bg">Signature</td>
                    </tr>
                    <tr>
                      <td class="lead-bg"><label>Project Coordinator</label></td>
                      <td class="lead-bg"> <label>Ophthalmologist</label></td>
                      <td class="lead-bg"><label>Project Director</label></td>
                    </tr>
                  </table>
                </div>				

		<?php  } ?>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<script src="<?php echo PUBLIC_URL; ?>js/jquery-2.1.1.js"></script>
<script>

function getPartner(){
	var matchvalue = $("#sel1").val();
	var cct = $("input[name='csrf_test_name']").val();
	//alert(cct);
	//var cct = $.cookie("<?php echo $this->config->item("csrf_cookie_name"); ?>");
	post_data ={ state_name: matchvalue,<?php echo $this->security->get_csrf_token_name(); ?> : cct, project_name :'Social inclusion'};
	if(matchvalue!=0 || matchvalue !="")
	{
    $.ajax({ 
            url: "<?php echo BASE_URL.'mpr/ajax_partner';?>",
            data: post_data,
            type: 'POST'
        }).done(function(responseData) { //alert(responseData); return false;
		var res = responseData.split('|');
		$("input[name='csrf_test_name']").val(res[1]);
if(res[0]==0)
{
	alert("Invalid request.Please select options properly.");
	return false;
}
else{
	$("#partner1").html("");
	$("#partner1").append(res[0]);
}	
		
		//alert(responseData); return false;
            console.log('Done: ', responseData);
        }).fail(function() {
            console.log('Failed');
        });
	}
}

</script>

<script>
 			function submit_form()			
			{
				var month_from = $("#month_from").val();
				var month_to = $("#month_from_last").val();
				var getYearFrom = month_from.split("-");
				var getYearTo = month_to.split("-");
				if($("#sel1").val() == null)
				{					
					$("#state_error").html("Select State");
				}
				else if($("#month_from").val() == "")
				{
					$("#month_from_error").html("Select month from");
				}
				else if($("#month_from_last").val() == "")
				{
					$("#month_from_last_error").html("Select month to");
				}
				else if(getYearFrom[1] != getYearTo[1])
				{
					$("#month_from_last_error").html("Selected from year and to year should be same year");
				}
				else{
					$("#mpr_si").submit();
				}
				
			}
</script>



