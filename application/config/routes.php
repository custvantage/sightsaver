<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//$route['default_controller'] = 'welcome';
$route['default_controller'] = 'login';

//Dashboard Routes
$route['dashboard'] = 'dashboard';
$route['dashboard_graph'] = 'dashboard/dashboard_graph';
$route['dashboard_speedometer'] = 'dashboard/dashboard_speedometer';
$route['dashboard_graph_ueh'] = 'dashboard/dashboard_graph_ueh';
$route['filter_data_graph_reh'] = 'dashboard/graph_filter_reh';
$route['filter_data_graph_ueh'] = 'dashboard/graph_filter_ueh';


//Approval
$route['Approval'] = 'Approval';
$route['approval_report_filter'] = 'Approval/filter_data';
$route['Approvaladmin'] = 'Approvaladmin';
$route['approval_report_filter_admin'] = 'Approvaladmin/filter_data_admin';


//Rular Routes
$route['excelupload_reh/:any'] = 'reh_main_entry/excelupload_reh';
$route['reh_main_entry/:any'] = 'reh_main_entry';
$route['edit_form_data/:any'] = 'reh_main_entry/edit_form_data';
$route['create_reh_main_entry/:any'] = 'reh_main_entry/create_reh_main_entry';
$route['manage_reh_main_entry/:any'] = 'reh_main_entry/manage';
$route['create_pmpo_comment'] = 'reh_main_entry/createpmpo_comment';
$route['approval'] = 'reh_main_entry/approve_reject';
$route['submit_form_data/:any'] = 'reh_main_entry/submit_form_data';

$route['reh_refferal'] = 'reh/refferal';

$route['reh_base_hospital'] = 'reh_base_hospital';
$route['create_base_reh'] = 'reh_base_hospital/create';
$route['filter_base_reh'] = 'reh_base_hospital/filter_base_data';
$route['excelupload_reh_base'] = 'reh_base_hospital/excelupload';
$route['delete_base_reh/:any'] = 'reh_base_hospital/delete_base_reh';
$route['edit_base_reh/:any'] = 'reh_base_hospital/edit_base_reh';

$route['reh_vision_center'] = 'reh_vc';
$route['create_vc_reh'] = 'reh_vc/create';
$route['filter_vc_reh'] = 'reh_vc/filter_vc_data';
$route['delete_vc_reh/:any'] = 'reh_vc/delete_vc_reh';
$route['edit_vc_reh/:any'] = 'reh_vc/edit_vc_reh';

$route['reh_training'] = 'reh_training';
$route['create_training_reh'] = 'reh_training/create';
$route['excelupload_reh_training_cb'] = 'reh_training/excelupload_traning_cb';
$route['filter_training_reh'] = 'reh_training/filter_training_data';
$route['delete_training_reh/:any'] = 'reh_training/delete_training_reh';
$route['edit_training_reh/:any'] = 'reh_training/edit_training_reh';


$route['reh_advocacy'] = 'reh_advocacy';
$route['create_advocacy_reh'] = 'reh_advocacy/create';
$route['filter_advocacy_reh'] = 'reh_advocacy/filter_advocacy_data';
$route['delete_advocacy_reh/:any'] = 'reh_advocacy/delete_advocacy_reh';
$route['edit_advocacy_reh/:any'] = 'reh_advocacy/edit_advocacy_reh';



$route['reh_bcc'] = 'reh_bcc';
$route['create_bcc_reh'] = 'reh_bcc/create';
$route['filter_bcc_reh'] = 'reh_bcc/filter_bcc_data';
$route['delete_bcc_reh/:any'] = 'reh_bcc/delete_bcc_reh';
$route['edit_bcc_reh/:any'] = 'reh_bcc/edit_bcc_reh';


$route['reh_iec'] = 'reh_iec';
$route['create_iec_reh'] = 'reh_iec/create';
$route['filter_iec_reh'] = 'reh_iec/filter_iec_data';
$route['delete_iec_reh/:any'] = 'reh_iec/delete_iec_reh';
$route['edit_iec_reh/:any'] = 'reh_iec/edit_iec_reh';

$route['reh_vhsnc'] = 'reh_vhsnc';
$route['create_vhsnc_reh'] = 'reh_vhsnc/create';
$route['filter_vhsnc_reh'] = 'reh_vhsnc/filter_vhsnc_data';
$route['delete_vhsnc_reh/:any'] = 'reh_vhsnc/delete_vhsnc_reh';
$route['edit_vhsnc_reh/:any'] = 'reh_vhsnc/edit_vhsnc_reh';


$route['reh_yearly_target/:any'] = 'reh_yearly_target';
$route['create_reh_yt_entry/:any'] = 'reh_yearly_target/create_reh_yt_entry';
$route['manage_reh_yt_entry/:any'] = 'reh_yearly_target/manage';
$route['edit_form_data_reh_yearly/:any'] = 'reh_yearly_target/edit_form_data_reh_yearly';
$route['submit_form_data_reh_yearly/:any'] = 'reh_yearly_target/submit_form_data_reh_yearly';



//Urban Routes
$route['excelupload_ueh/:any'] = 'ueh_main_entry/excelupload_ueh';
$route['ueh_main_entry/:any'] = 'ueh_main_entry';
$route['create_ueh_main_entry/:any'] = 'ueh_main_entry/create_ueh_main_entry';
$route['manage_ueh_main_entry/:any'] = 'ueh_main_entry/manage';

$route['ueh_vision_center'] = 'ueh/ueh_vision_center';
$route['create_vc_ueh'] = 'ueh/create_vc';
$route['filter_vc_ueh'] = 'ueh/filter_vc_data';
$route['delete_vc_ueh/:any'] = 'ueh/delete_vc_ueh';
$route['edit_vc_ueh/:any'] = 'ueh/edit_vc_ueh';

$route['ueh_base_hospital'] = 'ueh/ueh_base_hospital';
$route['create_bh_ueh'] = 'ueh/create_base_hospital';
$route['filter_bh_ueh'] = 'ueh/filter_bh_data';
$route['excelupload_ueh_base'] = 'ueh/excelupload';
$route['delete_base_ueh/:any'] = 'ueh/delete_base_ueh';
$route['edit_base_ueh/:any'] = 'ueh/edit_base_ueh';

$route['ueh_outreach_camp'] = 'ueh/ueh_outreach_camp';
$route['create_oc_ueh'] = 'ueh/create_oc';
$route['filter_oc_ueh'] = 'ueh/filter_oc_data';
$route['delete_oc_ueh/:any'] = 'ueh/delete_oc_ueh';
$route['edit_oc_ueh/:any'] = 'ueh/edit_oc_ueh';


$route['ueh_training_entry'] = 'ueh/ueh_training_entry';
$route['create_training_ueh'] = 'ueh/create_training_ueh';
$route['filter_training_data'] = 'ueh/filter_training_data';
$route['delete_training_ueh/:any'] = 'ueh/delete_training_ueh';
$route['edit_training_ueh/:any'] = 'ueh/edit_training_ueh';

$route['ueh_advocacy_entry'] = 'ueh/ueh_advocacy_entry';
$route['filter_advocacy_ueh'] = 'ueh/filter_advocacy_ueh';
$route['create_advocacy_ueh'] = 'ueh/create_advocacy_ueh';
$route['delete_advocacy_ueh/:any'] = 'ueh/delete_advocacy_ueh';
$route['edit_advocacy_ueh/:any'] = 'ueh/edit_advocacy_ueh';

$route['ueh_bcc_entry'] = 'ueh/ueh_bcc_entry';
$route['create_bcc_ueh'] = 'ueh/create_bcc_ueh';
$route['filter_bcc_ueh'] = 'ueh/filter_bcc_data';
$route['delete_bcc_ueh/:any'] = 'ueh/delete_bcc_ueh';
$route['edit_bcc_ueh/:any'] = 'ueh/edit_bcc_ueh';

$route['ueh_iec_entry'] = 'ueh/ueh_iec_entry';
$route['create_iec'] = 'ueh/create_iec';
$route['filter_iec_ueh'] = 'ueh/filter_iec_data';
$route['delete_iec_ueh/:any'] = 'ueh/delete_iec_ueh';
$route['edit_iec_ueh/:any'] = 'ueh/edit_iec_ueh';

$route['ueh_yearly_target/:any'] = 'ueh_yearly_target';
$route['create_ueh_yt_entry/:any'] = 'ueh_yearly_target/create_ueh_yt_entry';
$route['manage_ueh_yt_entry/:any'] = 'ueh_yearly_target/manage';


$route['404_override'] = '';
$route['translate_uri_dashes'] = TRUE;
//Administration
$route['partner'] = 'Administration/partner';
$route['districts'] = 'Administration/districts';
$route['reporting_quarters'] = 'Administration/reporting_quarters';
$route['user'] = 'Administration/user';
$route['logout'] = 'login/logout';


// Mpr Routes
$route['mpr_reh'] = 'Mpr/mpr_reh';
$route['mpr_reh_redirect/:any/:any'] = 'Mpr/mpr_reh_redirect';
$route['mpr_ueh'] = 'Mpr/mpr_ueh';
$route['mpr_ueh_redirect/:any/:any'] = 'Mpr/mpr_reh_redirect';
$route['mpr_si'] = 'Mpr/social_inclusion';
$route['mpr_social_redirect/:any/:any'] = 'Mpr/mpr_reh_redirect';
$route['mpr_ie'] = 'Mpr/inclusive_edu';
$route['mpr_education_redirect/:any/:any'] = 'Mpr/mpr_reh_redirect';

$route['mpr_partner_reh'] = 'Mpr/mpr_reh';
$route['mpr_partner_reh_redirect/:any/:any'] = 'Mpr/mpr_partner_reh_redirect';
$route['mpr_partner_ueh'] = 'Mpr/mpr_ueh';
$route['mpr_partner_ueh_redirect/:any/:any'] = 'Mpr/mpr_partner_reh_redirect';
$route['mpr_partner_si'] = 'Mpr/social_inclusion';
$route['mpr_partner_social_redirect/:any/:any'] = 'Mpr/mpr_partner_reh_redirect';
$route['mpr_partner_ie'] = 'Mpr/inclusive_edu';
$route['mpr_partner_education_redirect/:any/:any'] = 'Mpr/mpr_partner_reh_redirect';

// Inclusive Education ROUTES

$route['ie_main_entry/:any'] = 'Inclusive_edu';   
$route['edit_form_data_edu/:any'] = 'Inclusive_edu/edit_form_data_edu';
$route['main_entry_edu/:any'] = 'Inclusive_edu/main_entry_edu';
$route['manage_education_main_entry/:any'] = 'Inclusive_edu/manage';
//$route['create_pmpo_comment'] = 'reh_main_entry/createpmpo_comment';
//$route['approval'] = 'reh_main_entry/approve_reject';
$route['submit_form_data_edu/:any'] = 'Inclusive_edu/submit_form_data_edu';


// Social Inclusion
$route['si_main_entry/:any'] = 'Social_inclusion';
$route['main_entry/:any'] = 'Social_inclusion/main_entry';
//$route['main_entry/:any'] = 'Social_inclusion/main_entry';
$route['manage_social_main_entry/:any'] = 'Social_inclusion/manage';
$route['edit_form_data_social/:any'] = 'Social_inclusion/edit_form_data_social';
$route['submit_form_data_social/:any'] = 'Social_inclusion/submit_form_data_social';


$route['si_pwd_search'] = 'Social_inclusion/pwd_search';

$route['shg'] = 'Social_inclusion/shg';
$route['create_shg_si'] = 'Social_inclusion/create_shg_si';
$route['filter_shg_si'] = 'Social_inclusion/filter_shg_si';

$route['bpo'] = 'Social_inclusion/bpo';
$route['create_bpo_si'] = 'Social_inclusion/create_bpo_si';
$route['filter_bpo_si'] = 'Social_inclusion/filter_bpo_si';

$route['training'] = 'Social_inclusion/training';
$route['create_training'] = 'Social_inclusion/create_training';
$route['filter_training_si'] = 'Social_inclusion/filter_training_si';

$route['access_aidit'] = 'Social_inclusion/access_aidit';
$route['create_access_si'] = 'Social_inclusion/create_access_si';
$route['filter_access_si'] = 'Social_inclusion/filter_access_si';

$route['agencies'] = 'Social_inclusion/agencies';
$route['create_agencies_si'] = 'Social_inclusion/create_agencies_si';
$route['filter_agencies_si'] = 'Social_inclusion/filter_agencies_si';

$route['advocacy'] = 'Social_inclusion/advocacy';
$route['create_advocacy_si'] = 'Social_inclusion/create_advocacy_si';
$route['filter_advocacy_si'] = 'Social_inclusion/filter_advocacy_si';

$route['iec'] = 'Social_inclusion/iec';
$route['create_iec_si'] = 'Social_inclusion/create_iec_si';
$route['filter_iec_si'] = 'Social_inclusion/filter_iec_si';

$route['bcc'] = 'Social_inclusion/bcc';
$route['create_bcc_si'] = 'Social_inclusion/create_bcc_si';
$route['filter_bcc_si'] = 'Social_inclusion/filter_bcc_si';

$route['si_yearly_targets/:any'] = 'Social_inclusion';
$route['create_social_yt_entry/:any'] = 'Social_inclusion/create_social_yt_entry';
$route['manage_social_yt_entry/:any'] = 'Social_inclusion/manage';

$route['yearly_targets/:any'] = 'Social_yearly_target';
$route['create_social_yt_entry/:any'] = 'Social_yearly_target/create_social_yt_entry';
$route['manage_social_yt_entry/:any'] = 'Social_yearly_target/manage';
$route['edit_form_data_yearly/:any'] = 'Social_yearly_target/edit_form_data_yearly';
$route['submit_form_data_yearly/:any'] = 'Social_yearly_target/submit_form_data_yearly';

$route['partner_mpr'] = 'Approval/partner_mpr';
$route['partner_report_filter'] = 'Approval/partner_filter_data';
$route['getdistrict'] = 'Ajax/getDistrict';
$route['getcharts_reh'] = 'Ajax/getcharts_reh';
$route['getcharts_ueh'] = 'Ajax/getcharts_ueh';

$route['getcharts_dash'] = 'Ajax/getchartsfordashboard';

