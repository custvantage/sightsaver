ALTER TABLE `ss_states` ADD `status` VARCHAR(50) NOT NULL AFTER `ss_statescol`;
UPDATE ss_states SET status='Inactive';
UPDATE `ss_states` SET `status` = 'Active' WHERE `ss_states`.`ss_states_id` = 3;
UPDATE `ss_states` SET `status` = 'Active' WHERE `ss_states`.`ss_states_id` = 17;
UPDATE `ss_states` SET `status` = 'Active' WHERE `ss_states`.`ss_states_id` = 1;
UPDATE `ss_states` SET `status` = 'Active' WHERE `ss_states`.`ss_states_id` = 4;
UPDATE `ss_states` SET `status` = 'Active' WHERE `ss_states`.`ss_states_id` = 6;
UPDATE `ss_states` SET `status` = 'Active' WHERE `ss_states`.`ss_states_id` = 8;
UPDATE `ss_states` SET `status` = 'Active' WHERE `ss_states`.`ss_states_id` = 9;
UPDATE `ss_states` SET `status` = 'Active' WHERE `ss_states`.`ss_states_id` = 2;
UPDATE `ss_states` SET `status` = 'Active' WHERE `ss_states`.`ss_states_id` = 12;
UPDATE `ss_states` SET `status` = 'Active' WHERE `ss_states`.`ss_states_id` = 37;

